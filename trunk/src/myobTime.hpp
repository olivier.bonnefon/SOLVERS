/************************************************************************
  			myobtime.hpp 
**************************************************************************/
#include <stdio.h>
#include "sys/time.h"
#include <iostream>
using namespace std;


#ifndef MYOBTIME_H
#define MYOBTIME_H
// Class myobTime
// 
// 
class myobTime {
public:
  myobTime();
  void start();
  void stop();
  void reset();
  void setName(char const *Name);
  void print(ostream& os = cout);
  virtual ~myobTime();
  long value(){return mCumul;};
  
protected:
private:
  long mCall;
  timeval mStart;
  long mCumul;
  char mName[128];
  bool mIsRunning;
};



#define MYOB_TIMER_POW0 0
#define MYOB_TIMER_POW1 1
#define MYOB_TIMER_POW2 2
#define MYOB_TIMER_POW3 3
#define MYOB_TIMER_POW4 4
#define MYOB_TIMER_POW5 5
#define MYOB_TIMER_POW6 6
#define MYOB_TIMER_POW7 7
#define MYOB_TIMER_POW8 8
#define MYOB_TIMER_POW9 9
#define MYOB_TIMER_POW10 10
#define MYOB_TIMER_POW11 11
#define MYOB_TIMER_2 12
#define MYOB_TIMER_WNOCAST 13
#define MYOB_TIMER_WCAST 14
#define MYOB_TIMER_MERGE 15
#define MYOB_TIMER_SORTMERGE 16
#define MYOB_TIMER_TOTAL 17
#define MYOB_TIMER_LAST 18

extern myobTime MYOB_times [];



// FONCTION DU TEMPS
void MYOB_INIT_TIME();
void MYOB_PRINT_TIME();

#endif //MYOBTIME_H
