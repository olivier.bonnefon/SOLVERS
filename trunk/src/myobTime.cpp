/************************************************************************
  			myobtime.cpp 
**************************************************************************/


#include "myobTime.hpp"
#include <string.h>


#define MYOB_WITH_TIMER

myobTime::~myobTime(){
}
myobTime::myobTime(){
#ifdef MYOB_WITH_TIMER
  mIsRunning=false;
  mCumul=0;
  mCall=0;
  strcpy(mName,"NoName");
#endif
}

void myobTime::start(){
#ifdef MYOB_WITH_TIMER
  gettimeofday(&mStart,NULL);
  mCall++;
  mIsRunning=true;
#endif
}
void myobTime::reset(){
#ifdef MYOB_WITH_TIMER
  if (mIsRunning)
    stop();
  mIsRunning=false;
  mCumul=0;
  mCall=0;
#endif
}
void myobTime::stop(){
#ifdef MYOB_WITH_TIMER
  if (mIsRunning){
    timeval aux;
    gettimeofday(&aux,NULL);
    mCumul += (aux.tv_sec - mStart.tv_sec)*1000000 +(aux.tv_usec - mStart.tv_usec) ;
    mIsRunning = false;
  }
#endif
}
void myobTime::print(ostream& os){
#ifdef MYOB_WITH_TIMER
  os << mName<<" : "<<mCumul<<" usec. "<<mCall<<" calls.";
  if (mCall)
    os << "ie "<<mCumul/mCall<<" per call.";
  os <<endl;
#endif
}
void myobTime::setName(char const *Name){
#ifdef MYOB_WITH_TIMER
  strcpy(mName,Name);
#endif
}



myobTime MYOB_times[MYOB_TIMER_LAST];

void MYOB_INIT_TIME(){
  MYOB_times[MYOB_TIMER_POW0].setName("POW0 ");
  MYOB_times[MYOB_TIMER_POW1].setName("POW1 ");
  MYOB_times[MYOB_TIMER_POW2].setName("POW2 ");
  MYOB_times[MYOB_TIMER_POW3].setName("POW3 ");
  MYOB_times[MYOB_TIMER_POW4].setName("POW4 ");
  MYOB_times[MYOB_TIMER_POW5].setName("POW5 ");
  MYOB_times[MYOB_TIMER_POW6].setName("POW6 ");
  MYOB_times[MYOB_TIMER_POW7].setName("POW7 ");
  MYOB_times[MYOB_TIMER_POW8].setName("POW8 ");
  MYOB_times[MYOB_TIMER_POW9].setName("POW9 ");
  MYOB_times[MYOB_TIMER_POW10].setName("POW10 ");
  MYOB_times[MYOB_TIMER_POW11].setName("POW11 ");
  MYOB_times[MYOB_TIMER_WNOCAST].setName("Sort with no cast ");
  MYOB_times[MYOB_TIMER_WCAST].setName("Sort with cast ");
  MYOB_times[MYOB_TIMER_MERGE].setName("Merge ");
  MYOB_times[MYOB_TIMER_TOTAL].setName("total ");
  MYOB_times[MYOB_TIMER_SORTMERGE].setName("sort and merge ");
  MYOB_times[MYOB_TIMER_2].setName("2 ");
  
}
void MYOB_PRINT_TIME(){
  for (int i=0;i <MYOB_TIMER_LAST;i++)
    MYOB_times[i].print();
}


