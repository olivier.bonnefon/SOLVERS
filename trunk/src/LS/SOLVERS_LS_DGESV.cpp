
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
extern "C" {
int dgesv_(int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b, int *ldb, int *info);
}
int SOLVERS_LS_DGESV(double *A,double *B,double *X,int n){
  int info;
  int *p=(int*)malloc(n*sizeof(int));
	int one=1;
  memcpy(X,B,n*sizeof(double));
  dgesv_(&n,&one,A,&n,p,X,&n,&info);
  if(info) printf("SOLVERS_LS_DGESV error:  dgesv_ %i\n", info);
  free(p);
  return info;
}
