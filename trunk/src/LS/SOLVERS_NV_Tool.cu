#define NbrTh 96

__global__ void rowscale_kernel(double * X, const int N, 
                                const double * ralpha)
{
  
  for(int row=threadIdx.x; row<N; row+=NbrTh) {
    X[row] *= ralpha[row];
//    X[row] = threadIdx.x;
  }
}




//extern "C"
void rowscale(double * X, const int N,
              const double * ralpha) {
  // Setup kernel problem size
//  dim3 blocksize(M,N,1);
//  dim3 gridsize(1,1,1);
  
  // Call kernel
  rowscale_kernel<<<1,NbrTh>>>(X,N,ralpha);
//  cubed_kernel<<<gridsize, blocksize>>>(in, out);
}

