#ifndef SOLVERS_LS_H
#define SOLVERS_LS_H
#include "SOLVERS_LS_Tool.h"


int SOLVERS_LS_GS(double *A,double *B,double *X, int N,double criteria,int maxIt,int *nbIt);

int SOLVERS_LS_CG_SIZE_WORKD(int N);
int SOLVERS_LS_CG_PREC_DIAG_SIZE_WORKD(int N);
int SOLVERS_LS_CG(double *A,double *B,double *X, double *workD,int N,double criteria,int maxIt,int *nbIt);
int SOLVERS_LS_PREC_DIAG_CG(double *A,double *B,double *X,double *workD,int N,double criteria,int maxIt,int *nbIt);
int SOLVERS_LS_PREC_CG(double *A,double *B,double *X, double * C,double *workD,int N,double criteria,int maxIt,int *nbIt);
int SOLVERS_LS_DGESV(double *A,double *B,double *X,int n);


#endif
