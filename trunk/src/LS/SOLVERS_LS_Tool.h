#ifndef SOLVERS_LS_TOOL_H
#define SOLVERS_LS_TOOL_H
void SOLVERS_LS_generateSymDenseSystem(int N);
void SOLVERS_LS_readDenseMatrice( double ** A,double **B, int *pN,char* fileNameA, char* fileNameB);
void SOLVERS_LS_getDominantePart(double *Ain, double *Aout, int n);
int SOLVERS_LS_checkSym(double *A,int n);
void SOLVERS_LS_printSystem(double *A,double * B,int n);
void SOLVERS_LS_prencondDiag(double *A,double * B,int n,double * D);
void SOLVERS_LS_printVect(double * B,int n);
void SOLVERS_LS_printSystemForScilab(double *A,double * B,int n);
int SOLVERS_LS_checkSolutionDense(double * A, double *B,double * X,int n, double criteron);
#define CG_DEBUG_SOL
void _SOLVERS_DEBUG_SET_SOL(double * sol,int N);
void _SOLVERS_DEBUG_RESET_SOL();
void _SOLVERS_DEBUG_DIST_SOL( double * X,int N);
#endif
