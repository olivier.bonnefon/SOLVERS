#ifndef SOLVER_LS_CG_NV_HPP
#define SOLVER_LS_CG_NV_HPP
int SOLVERS_LS_PREC_DIAG_CG_NV(double *A,double *B,double *X, double *workD,int N,double criteria,int maxIt,int *nbIt);
int SOLVERS_LS_CG_NV(double *A,double *B,double *X,double *workD,int N,double criteria,int maxIt,int *nbIt);
#endif
