#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "SOLVERS_LS_Tool.h"
#include "SOLVERS_LS.h"
#include "myobTime.hpp"

int main(int argc, char *argv[]){
  int res=0;
  int N=2;
  int maxit=1000;
  double crit = 1e-12;
  double crit2 = 1e-6;
  double * A2=0;
  double * A=0;
  double * B2=0;
  char fileNameA[256];
  char fileNameB[256];
  double * Buf=0;
  double * X=0;
  double * workD=0;
  myobTime aTimer;
  if (argc==3){
    sscanf(argv[1], "%s", fileNameA);
    sscanf(argv[2], "%s", fileNameB);
  }else{
    strcpy(fileNameA,"data/A2.txt");
    strcpy(fileNameB,"data/b2.txt");
  }
  SOLVERS_LS_readDenseMatrice(&A2,&B2,&N,fileNameA,fileNameB);
  Buf=(double*)calloc(N,sizeof(double));
  X=(double*)calloc(N,sizeof(double));
  A=(double*)malloc(N*N*sizeof(double));
  if (N<100)
    SOLVERS_LS_printSystem(A2,B2,N);
  aTimer.reset();
  aTimer.start();
  memcpy(A,A2,N*N*sizeof(double));
  if (SOLVERS_LS_DGESV(A,B2,X,N)){
    printf("SOLVERS_testDGESV failed 1\n");
    res=1;
  }
  aTimer.stop();
  aTimer.print();
  memcpy(Buf,X,N*sizeof(double));
  if (SOLVERS_LS_checkSolutionDense(A2,B2,Buf,N,crit2)){
    printf("SOLVERS_testDGESV failed 2\n");
    res=1;
  }
  printf("Solution is (truncked if N>10):\n");
  if (N<10)
    SOLVERS_LS_printVect(X,N);
  else
    SOLVERS_LS_printVect(X,10);
  free(A2);
  free(A);
  free(B2);
  free(Buf);
  free(X);
  free(workD);
  return res;
}
