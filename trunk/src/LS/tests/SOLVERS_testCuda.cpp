#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "myobTime.hpp"
#include "SOLVERS_CU_TOOL.h"
#include <cblas.h>
#include "SOLVERS_LS_Tool.h"

#define RES_N 0
#define RES_CBLAS_DDOT 1
#define RES_CUBLAS_DDOT 2
#define RES_MEM_CPY_CUBLAS 3
#define RES_MEM_ALLOC_CUBLAS 4
#define RES_MEM_ALLOC_HOST 5
#define RES_LAST 6
#define NB_LOOP 10
int I_LAST=6;
cublasHandle_t handle;
int * pRes;
int powerN;
 char * RES_MESS[RES_LAST]={"2power","CBLASDDOT","CUBLASDDOT","MEM_CPY_CUDA","MEM_ALLOC_CUDA","MEM_ALLOC_HOST"};

void printRES(){
 for (int info=0; info <I_LAST;info++){
    printf("%s\t",RES_MESS[info]);
    for (int cmp=0;cmp<powerN;cmp++){
      printf("%i\t",pRes[I_LAST*cmp+info]);
    }
    printf("\n");
  }
}


int testDDOT(){
  int N=2;
  myobTime aTimer;
  printf("I'm cumparing cuddot and cblasddot\n");
  for (int cmp=0;cmp<powerN;cmp++){
    pRes[I_LAST*cmp+RES_N]=cmp+1;
    
    aTimer.reset();
    aTimer.start();
    double * pV=(double *)malloc(N*sizeof(double));
    double * pW=(double *)malloc(N*sizeof(double));
    aTimer.stop();
    pRes[I_LAST*cmp+RES_MEM_ALLOC_HOST]=aTimer.value();
   
    for (int i=0;i<N;i++){
      int t=rand() % 1000;
      double dt=t;
      dt=t/1000.0;
      pV[i]=dt;
      t=rand() % 1000;
      dt=t;
      dt=t/1000.0;
      pW[i]=dt;
    }
    aTimer.reset();
    aTimer.start();
    double aux4;
    double aux3=cblas_ddot(N,pV,1,pW,1);
    aTimer.stop();
    pRes[I_LAST*cmp+RES_CBLAS_DDOT]=aTimer.value();

    double * d_V,*d_W;
    unsigned int mem_size_V = sizeof(double) * N;
    
    aTimer.reset();
    aTimer.start();
    checkCudaErrors(cudaMalloc((void**) &d_V, mem_size_V));
    checkCudaErrors(cudaMalloc((void**) &d_W, mem_size_V));
    aTimer.stop();
    pRes[I_LAST*cmp+RES_MEM_ALLOC_CUBLAS]=aTimer.value();

    aTimer.reset();
    aTimer.start();   
    checkCudaErrors(cudaMemcpy(d_V, pV, mem_size_V, cudaMemcpyHostToDevice) );//d_V <- b
    checkCudaErrors(cudaMemcpy(d_W, pW, mem_size_V, cudaMemcpyHostToDevice) );//d_V <- b
    aTimer.stop();
    pRes[I_LAST*cmp+RES_MEM_CPY_CUBLAS]=aTimer.value();
    aTimer.reset();
    aTimer.start();   
    checkCublasStatus(cublasDdot(handle,N,d_V,1,d_W,1,&aux4),"rep5");
    cudaDeviceSynchronize();
    aTimer.stop();
    pRes[I_LAST*cmp+RES_CUBLAS_DDOT]=aTimer.value();
    if (fabs(aux3-aux4)>1e-8*fabs(aux3))
      printf("ERREUR %lf %lf",aux3,aux4);
    checkCudaErrors(cudaFree(d_V));
    free(pV);
    checkCudaErrors(cudaFree(d_W));
    free(pW);

    N=2*N;
  }
  
 
  //free("pRes");
  return 0;
}
int testDaxpy(){
  int N=2;
  myobTime aTimer;
  double rho=sqrt(5);
  printf("I'm cumparing cudaxpy and cblasdaxpy\n");
  I_LAST=3;
  RES_MESS[1]="cudaxpy";
  RES_MESS[2]="cblasdaxpy";
  for (int cmp=0;cmp<powerN;cmp++){
    pRes[I_LAST*cmp]=cmp+1;
    
    double * pV=(double *)malloc(N*sizeof(double));
    double * pW=(double *)malloc(N*sizeof(double));

    for (int i=0;i<N;i++){
      int t=rand() % 1000;
      double dt=t;
      dt=t/1000.0;
      pV[i]=dt;
      t=rand() % 1000;
      dt=t;
      dt=t/1000.0;
      pW[i]=dt;
    }
    double * d_V,*d_W;
    unsigned int mem_size_V = sizeof(double) * N;
    
    checkCudaErrors(cudaMalloc((void**) &d_V, mem_size_V));
    checkCudaErrors(cudaMalloc((void**) &d_W, mem_size_V));
    checkCudaErrors(cudaMemcpy(d_V, pV, mem_size_V, cudaMemcpyHostToDevice) );//d_V <- b
    checkCudaErrors(cudaMemcpy(d_W, pW, mem_size_V, cudaMemcpyHostToDevice) );//d_V <- b
 
    aTimer.reset();
    aTimer.start();
    cblas_daxpy(N,rho,pV,
                1,pW,1);
    aTimer.stop();
    pRes[I_LAST*cmp+2]=aTimer.value();

   aTimer.reset();
   aTimer.start();   
   checkCublasStatus(cublasDaxpy(handle,N,&rho,d_V,1,d_W,1),"rep5");
   cudaDeviceSynchronize();
   aTimer.stop();
   pRes[I_LAST*cmp+1]=aTimer.value();
   checkCudaErrors(cudaFree(d_V));
   free(pV);
   checkCudaErrors(cudaFree(d_W));
   free(pW);
    N=2*N;
  }
  //free("pRes");
  return 0;
}
int testDgemv(){
  int N=2;
  myobTime aTimer;
  double rho=sqrt(5);
  double minusone=-1.0;
  printf("I'm cumparing cudgemv and cblasdgemv\n");
  I_LAST=3;
  RES_MESS[1]="cudgemv";
  RES_MESS[2]="cblasdgemv";
  for (int cmp=0;cmp<powerN;cmp++){
    pRes[I_LAST*cmp]=cmp+1;
    SOLVERS_LS_generateSymDenseSystem(N);
    double * pA,*pX;
    SOLVERS_LS_readDenseMatrice(&pA,&pX,&N,"Matrice.txt","Vect.txt");

    double * pY=(double *)malloc(N*sizeof(double));
    for (int i=0;i<N;i++){
      int t=rand() % 1000;
      double dt=t;
      dt=t/1000.0;
      pY[i]=dt;
    }
    double * d_A,*d_X,*d_Y;
    unsigned int mem_size_X = sizeof(double) * N;
    unsigned int mem_size_A = sizeof(double) * N*N;
    
    checkCudaErrors(cudaMalloc((void**) &d_A, mem_size_A));
    checkCudaErrors(cudaMalloc((void**) &d_X, mem_size_X));
    checkCudaErrors(cudaMalloc((void**) &d_Y, mem_size_X));
    checkCudaErrors(cudaMemcpy(d_A, pA, mem_size_A, cudaMemcpyHostToDevice) );
    checkCudaErrors(cudaMemcpy(d_X, pX, mem_size_X, cudaMemcpyHostToDevice) );
    checkCudaErrors(cudaMemcpy(d_Y, pY, mem_size_X, cudaMemcpyHostToDevice) );
 
    aTimer.reset();
    aTimer.start();
    for (int bb=0;bb<NB_LOOP;bb++)
      cblas_dgemv(CblasRowMajor,
                  CblasNoTrans, N,N,
                  rho,pA,N,
                  pX,1,-1.0,
                  pY,1);
    aTimer.stop();
    pRes[I_LAST*cmp+2]=aTimer.value();

   aTimer.reset();
   aTimer.start();
   for (int bb=0;bb<NB_LOOP;bb++)
     checkCublasStatus(cublasDgemv(handle,
                                   CUBLAS_OP_N, N,N,
                                   &rho,d_A,N,
                                   d_X,1,&minusone,
                                   d_Y,1),"rep5");
   cudaDeviceSynchronize();
   aTimer.stop();
   pRes[I_LAST*cmp+1]=aTimer.value();

   compareDouble("compar Y and d_Y", pY, pX, d_Y, N,1e-8);
     
   checkCudaErrors(cudaFree(d_A));
   free(pA);
   checkCudaErrors(cudaFree(d_X));
   checkCudaErrors(cudaFree(d_Y));
   free(pX);
   free(pY);
    N=2*N;
  }
  //free("pRes");
  return 0;
}
int testDgemm(){
  int N=2;
  myobTime aTimer;
  double rho=sqrt(5);
  double minusone=-1.0;
  printf("I'm cumparing cudgemm and cblasdgemm\n");
  I_LAST=3;
  RES_MESS[1]="cudgemm";
  RES_MESS[2]="cblasdgemm";
  for (int cmp=0;cmp<powerN;cmp++){
    pRes[I_LAST*cmp]=cmp+1;
    SOLVERS_LS_generateSymDenseSystem(N);
    double * pA,*pB,*pX;
    SOLVERS_LS_readDenseMatrice(&pA,0,&N,"Matrice.txt","Vect.txt");
    SOLVERS_LS_generateSymDenseSystem(N);
    SOLVERS_LS_readDenseMatrice(&pB,0,&N,"Matrice.txt","Vect.txt");
    double * pC=(double *)malloc(N*N*sizeof(double));
    for (int i=0;i<N*N;i++){
      int t=rand() % 1000;
      double dt=t;
      dt=t/1000.0;
      pC[i]=dt;
    }
    double * d_A,*d_B,*d_C;
    unsigned int mem_size_A = sizeof(double) * N*N;
    
    checkCudaErrors(cudaMalloc((void**) &d_A, mem_size_A));
    checkCudaErrors(cudaMalloc((void**) &d_B, mem_size_A));
    checkCudaErrors(cudaMalloc((void**) &d_C, mem_size_A));
    checkCudaErrors(cudaMemcpy(d_A, pA, mem_size_A, cudaMemcpyHostToDevice) );
    checkCudaErrors(cudaMemcpy(d_B, pB, mem_size_A, cudaMemcpyHostToDevice) );
    checkCudaErrors(cudaMemcpy(d_C, pC, mem_size_A, cudaMemcpyHostToDevice) );
 
    aTimer.reset();
    aTimer.start();
    for (int bb=0;bb<NB_LOOP;bb++)
      cblas_dgemm(CblasRowMajor,
                  CblasNoTrans,CblasNoTrans, N,N,N,
                  rho,pA,N,
                  pB,N,-1.0,
                  pC,N);
    aTimer.stop();
    pRes[I_LAST*cmp+2]=aTimer.value();

   aTimer.reset();
   aTimer.start();
   for (int bb=0;bb<NB_LOOP;bb++)
     checkCublasStatus(cublasDgemm(handle,
                                   CUBLAS_OP_N,CUBLAS_OP_N,N, N,N,
                                   &rho,d_A,N,
                                   d_B,N,&minusone,
                                   d_C,N),"rep5");
   //cudaDeviceSynchronize();
   aTimer.stop();
   pRes[I_LAST*cmp+1]=aTimer.value();

   compareDouble("compar C and d_C", pC, pB, d_C, N*N,1e-8);
     
   checkCudaErrors(cudaFree(d_A));
   checkCudaErrors(cudaFree(d_B));
   checkCudaErrors(cudaFree(d_C));
   free(pA);
   free(pB);
   free(pC);
   N=2*N;
  }
  //free("pRes");
  return 0;
}
int main  (int argc, char *argv[]){
  
  checkCublasStatus(cublasCreate(&handle),"cublasCreate() error");

  int N=2;
  powerN=2;
  if (argc>1){
    sscanf(argv[1], "%i", &powerN);
  }
  pRes=(int *)malloc(RES_LAST*powerN*sizeof(long));
  int res=testDDOT();
  printRES();
  testDaxpy();
  printRES();
  testDgemv();
  printRES();
  testDgemm();
  printRES();
  free(pRes);
  
  return res;
}
