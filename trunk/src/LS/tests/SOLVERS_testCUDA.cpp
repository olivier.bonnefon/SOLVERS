#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "myobTime.hpp"
#include "SOLVERS_CU_TOOL.h"

#define RES_N 0
#define RES_CBLAS_DDOT 1
#define RES_CUBLAS_DDOT 2
#define RES_MEM_CPY_CUBLAS 3
#define RES_MEM_ALLOC_CUBLAS 4
#define RES_MEM_ALLOC_HOST 5
#define RES_LAST 6

char ** RES_MESS{"N","CBLASDDOT","CUBLASDDOT","MEM_CPY_CUDA","MEM_ALLOC_CUDA","MEM_ALLOC_HOST"};

int main  (int argc, char *argv[]){
 
  int N=2;
  int powerN=2;
  myobTime aTimer;
  if (argc>1){
    sscanf(argv[1], "%i", &powerN);
  }
  int * pRes=(int *)malloc(RES_LAST*powerN*sizeof(long));
  
  cublasHandle_t handle;
  checkCublasStatus(cublasCreate(&handle),"cublasCreate() error");

  for (int cmp=0;cmp<powerN;cmp++){
    pRes[RES_LAST*cmp+RES_N]=N;
    
    aTimer.reset();
    aTimer.start();
    double * pV=(double *)malloc(N*sizeof(double));
    aTimer.stop();
    pRes[RES_LAST*cmp+RES_MEM_ALLOC_HOST]=aTimer.value();
   
    for (int i=0;i<N;i++){
      int t=rand() % 1000;
      double dt=t;
      dt=t/1000.0;
      pB[i]=dt;
    }


    
    aTimer.reset();
    aTimer.start();
    double aux4;
    double aux3=cblas_ddot(N,G,1,CG,1);
    aTimer.stop();
    pRes[RES_LAST*cmp+RES_CBLAS_DDOT]=aTimer.value();

    double * d_V;
    unsigned int mem_size_V = sizeof(double) * N;
    
    aTimer.reset();
    aTimer.start();
    checkCudaErrors(cudaMalloc((void**) &d_V, mem_size_V));
    aTimer.stop();
    pRes[RES_LAST*cmp+RES_MEM_ALLOC_CUBLAS]=aTimer.value();

    aTimer.reset();
    aTimer.start();   
    checkCudaErrors(cudaMemcpy(d_V, pV, mem_size_V, cudaMemcpyHostToDevice) );//d_V <- b
    aTimer.stop();
    pRes[RES_LAST*cmp+RES_MEM_CPY_CUBLAS]=aTimer.value();
    aTimer.reset();
    aTimer.start();   
    ret=cublasDdot(handle,N,d_V,1,d_CG,1,&aux4);
    checkCublasStatus(ret,"rep5");
    aTimer.stop();
    pRes[RES_LAST*cmp+RES_CUBLAS_DDOT]=aTimer.value();

    checkCudaErrors(cudaFree(d_V));
    free("pV");

    N=2*N;
  }
  
  for (int info=0; info <RES_LAST;info++){
    printf("%s\t",RES_MESS[info]);
    for (int cmp=0;cmp<powerN;cmp++){
      printf("%i\t",pRes[RES_LAST*cmp+info]);
    }
    printf("\n");
  }

  }
}
