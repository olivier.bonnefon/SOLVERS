#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void _SOLVERS_DEBUG_SET_SOL(double * sol,int N);
void _SOLVERS_DEBUG_RESET_SOL();
#include "SOLVERS_LS_Tool.h"
#include "SOLVERS_LS.h"
#include "myobTime.hpp"

#include "SOLVERS_LS_CG_NV.h"
int main(int argc, char *argv[]){
  int res=0;
  int N=2;
  int maxit=1000;
  double crit = 1e-12;
  double crit2 = 1e-6;
  double * A2=0;
  double * B2=0;
  double * C=0;
  char fileNameA[256];
  char fileNameB[256];
  double * Buf=0;
  double * X=0;
  double * workD=0;
  int nbIt;
  myobTime aTimer;
  nbIt=0;
  if (argc>=3){
    sscanf(argv[1], "%s", fileNameA);
    sscanf(argv[2], "%s", fileNameB);
    if (argc >3)
      sscanf(argv[3], "%i",&nbIt);
  }else{
    strcpy(fileNameA,"data/A2.txt");
    strcpy(fileNameB,"data/b2.txt");
  }
  SOLVERS_LS_readDenseMatrice(&A2,&B2,&N,fileNameA,fileNameB);
  if (SOLVERS_LS_checkSym(A2,N)){
    printf("FAILED SOLVERS_testDIAGCG CG with non sym mat\n");
    return 1;
  }

  Buf=(double*)calloc(N,sizeof(double));
  X=(double*)calloc(N,sizeof(double));
  int workDSize=SOLVERS_LS_CG_PREC_DIAG_SIZE_WORKD(N);
  workD=(double*)malloc(workDSize*sizeof(double));
  if (N<10)
    SOLVERS_LS_printSystem(A2,B2,N);
    
  aTimer.reset();
  aTimer.start();

  if (SOLVERS_LS_PREC_DIAG_CG_NV(A2,B2,X,workD,N,crit,maxit,&nbIt)){
    printf("SOLVERS_testDIAGCOND_CGNV failed 1\n");
    res=1;
  }
  aTimer.stop();
  aTimer.print();
  printf("SOLVERS_testDIAGCG nbit=%i\n",nbIt);

  printf("now same test printing the dist to solution:\n");
  _SOLVERS_DEBUG_SET_SOL(X,N);
  for (int I=0;I<N;I++)
    X[I]=0;
  
  if (SOLVERS_LS_PREC_DIAG_CG_NV(A2,B2,X,workD,N,crit,maxit,&nbIt)){
    printf("SOLVERS_testDIAGCG failed 11\n");
    res=1;
  }
  _SOLVERS_DEBUG_RESET_SOL();
  printf("SOLVERS_testDIAGCG nbit=%i\n",nbIt);

  
  memcpy(Buf,X,N*sizeof(double));
  if (nbIt>=N)
    crit2=1e-14;
  
  if (SOLVERS_LS_checkSolutionDense(A2,B2,Buf,N,crit2)){
    printf("SOLVERS_testDIAGGS failed 2\n");
    res=1;
  }
  printf("Solution is (truncked if N>10):\n");
  if (N<10)
    SOLVERS_LS_printVect(X,N);
  else
    SOLVERS_LS_printVect(X,10);
  free(A2);
  free(B2);
  free(Buf);
  free(X);
  free(workD);
  return res;
}
