#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "SOLVERS_LS_Tool.h"

int main(int argc, char *argv[]){
 
  int N=2;

  if (argc>1){
    sscanf(argv[1], "%i", &N);
  }
  SOLVERS_LS_generateSymDenseSystem(N);
  double *M,*B;
  SOLVERS_LS_readDenseMatrice( &M,&B, &N,"Matrice.txt", "Vect.txt");
  SOLVERS_LS_checkSym(M,N);
  return 0;
}
