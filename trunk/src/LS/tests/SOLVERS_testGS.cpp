#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "SOLVERS_LS_Tool.h"
#include "SOLVERS_LS.h"
#include "myobTime.hpp"


int main(int argc, char *argv[]){
  int res=0;
  int N=0;
  int maxit=1000;
  double crit = 1e-5;
  double * A2=0;
  double * B2=0;
  char fileNameA[256];
  char fileNameB[256];
  int nbIt;
  myobTime aTimer;
  if (argc==3){
    sscanf(argv[1], "%s", fileNameA);
    sscanf(argv[2], "%s", fileNameB);
  }else{
    strcpy(fileNameA,"data/A2.txt");
    strcpy(fileNameB,"data/b2.txt");
  }
  
  SOLVERS_LS_readDenseMatrice(&A2,&B2,&N,fileNameA,fileNameB);
  double * Buf=(double*)calloc(N,sizeof(double));
  double * X=(double*)calloc(N,sizeof(double));
  if (N<100)
    SOLVERS_LS_printSystem(A2,B2,N);
  aTimer.reset();
  aTimer.start();
  if (SOLVERS_LS_GS(A2,B2,X,N,crit,maxit,&nbIt)){
    printf("SOLVERS_testGS failed 1\n");
    res=1;
  }
  aTimer.stop();
  aTimer.print();
  printf("SOLVERS_testGS nbit=%i\n",nbIt);
  memcpy(Buf,X,N*sizeof(double));
  if (SOLVERS_LS_checkSolutionDense(A2,B2,Buf,N,10*crit)){
    printf("SOLVERS_testGS failed 1\n");
    res=1;
  }
  printf("Solution is (truncked if N>10):\n");
  if (N<10)
    SOLVERS_LS_printVect(X,N);
  else
    SOLVERS_LS_printVect(X,10);
  free(A2);
  free(B2);
  free(Buf);
  free(X);
  return res;
}
