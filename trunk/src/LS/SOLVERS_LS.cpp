#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"
#include <sys/time.h>

static double maxf( double a, double b ){
        if (a>b) return(a); else return(b);
}

void readMatrice( double * A,double *B){
  FILE *pF=fopen("/SandBox/SCALAPACK_EXAMPLE/aA.txt","r");
  if (!pF)
    printf("can't open file readMatrice\n");
  long int i,j,advance;
  int diage=0,ndiage=0;
  int cmp=0;
  int nnz;
  int n,sym,nz;
  double a;
  fscanf(pF,"%i%i%i%i",&n,&n,&sym,&nz);
  printf ("n=%i,nz=%i,nz/n2=%e\n",n,nz,(1.0*(nz))/((1.0*(n))*(1.0*(n))));
  nnz=nz;
  for (int ii=0;ii<nnz;ii++){
    fscanf(pF,"%ld%ld%lf",&i,&j,&a);
    
    //printf("%ld %ld %lf %ld %ld\n",i-1,j-1,a,mat.cols(),mat.rows());
    A[(i-1)+n*(j-1)]=a;
  }
  
  fclose(pF);
  pF=fopen("/SandBox/SCALAPACK_EXAMPLE/b.txt","r");
  if (!pF)
    printf("can't open file readVector\n");
  fscanf(pF,"%i",&n);
  printf ("vector, n=%i\n",n);
    
  for (int ii=0;ii<n;ii++){
    fscanf(pF,"%lf",&a);
    B[ii]=a;
  }
  
  
  /* Sparse Matrix (Morse)  
 first line: n m (is symmetic) nbcoef 
 after for each nonzero coefficient:   i j a_ij where (i,j) \in  {1,...,n}x{1,...,m} 
 39550 39550 1  246946
 1         1 0.0078883951066615054515
 2         1 -0.00028802969865261609638*/
  fclose(pF);
}

void getDominantePart(double *Ain, double *Aout, int n){
  int nbZero=0;
  int nbNZero=0;
  for (int i=0;i<n;i++){
    for (int j=0;j<n;j++){
      if ((10*fabs(Ain[i+n*j]) - fabs(Ain[i+n*i]))>0){
        Aout[i+n*j]=Ain[i+n*j];nbNZero++;
      }else{
        Aout[i+n*j]=0;nbZero++;
      }
    }
  }
  printf("getDominantePart nbZero=%i nbNZero=%i\n",nbZero,nbNZero);
}
int checkSym(double *A,int n){
  for (int i=0;i<n;i++)
    for (int j=0;j<i;j++)
      if (fabs(A[i+n*j]-A[j+n*i]) > 0.9*maxf(fabs(A[i+n*j]),fabs(A[j+n*i]))){
         printf("not sym because %e %e\n",A[i+n*j],A[j+n*i]);
        return 1;
      }
  printf("sym ok\n");
  return 0;
          
}
int diagCond(double *A,double * B,int n){
  int i,j;
  for (i=0;i<n;i++){
    double d = A[i+n*i];
    if (d!=0){
      for (j=0;j<n;j++){
        A[i+n*j]=(A[i+n*j])/d;
      }
      B[i]=B[i]/d;
    }
  }
  
}
void printSystem(double *A,double * B,int n,int line){
  double normInf=0;
  
  int i,j;
  for (i=0;i<n;i++)
    for (j=0;j<n;j++)
      if (fabs(A[i+n*j])>normInf)
        normInf=fabs(A[i+n*j]);
  
  printf("norminf A=%e\n",normInf);
  printf("A:\n");
  for (j=0;j<n;j++){
    printf("\t %e",A[line+n*j]);
  }
  printf("\n");
  printf("B:\n");
  printf("\t %e",B[line]);
  printf("\n");
}

void printSystemForScilab(double *A,double * B,int n){
  FILE *pF=fopen("scA.sci","w");
  fprintf(pF,"A=[");
  for (int i=0;i<n;i++)
    for (int j=0;j<n;j++){
      fprintf(pF,"%e",A[i+n*j]);
      if (i!=n)
        fprintf(pF,",");
      else
        fprintf(pF,";");
    }
  fprintf(pF,"];\n");
  fclose(pF);
}

int GSmethod(double *A,double *B,double *X, int N,double criteria,int maxIt){
  int numit=0;
  double residu=0;
  for (numit=0;numit<maxIt;numit++){
    residu=0;
    for (int i=0;i<N;i++){
      double savXi=X[i];
      double sumi=0;
      X[i]=0.0;
      for (int j=0;j<N;j++){
        sumi+= A[i+N*j]*X[j];
      }
      sumi-=B[i];
      X[i]=sumi/A[i+N*i];
      if (fabs(X[i]-savXi)>residu){
        residu=fabs(X[i]-savXi);
      }
    }
    if (residu<criteria){
      printf("GSmethod cv in %i it\n",numit);
      return 0;
    }
  }
  printf("GSmethod err=%e\n",residu);
  return 1;
}
