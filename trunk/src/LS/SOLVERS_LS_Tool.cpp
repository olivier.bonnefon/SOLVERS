#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
//#include "mpi.h"
#include <sys/time.h>
#include <cblas.h>
#include "SOLVERS_LS_Tool.h"
//#include <cblas.h>

#define MAX(X, Y)  ((X) > (Y) ? (X) : (Y))

/*vreation d'une matrice diagonal dominante positive*/
 void SOLVERS_LS_generateSymDenseSystem(int N){
   FILE *pFM;
   pFM=fopen("Matrice.txt","w");
   FILE *pFV=fopen("Vect.txt","w");
  fprintf(pFM,"%i\t%i\t%i\t%i\n",N,N,1,N*N);
  double * pMat=(double *) malloc(N*N*sizeof(double));
  double * pB=(double *) malloc(N*sizeof(double));
  for (int i=0;i<N;i++){
    int t=rand() % 1000;
    double dt=t;
    dt=t/1000.0;
    double dtdiag=dt;
    pMat[i+i*N]=dtdiag*N;
    for (int j=i+1;j<N;j++){
      t=rand() % 1000;
      dt=t;
      dt=t/1000.0;
      dt = dtdiag*dt;
      pMat[i+j*N]=dt;
      pMat[j+i*N]=dt;
    }
  }
 
  for (int i=0;i<N;i++){
    int t=rand() % 1000;
    double dt=t;
    dt=t/1000.0;
    pB[i]=dt;
  }

  for (int i=0;i<N;i++){
    for(int j=0;j<N;j++){
      fprintf(pFM,"%i\t%i\t%lf\n",i+1,j+1,pMat[i+j*N]);
    }
  }
  fclose(pFM);
  fprintf(pFV,"%i\n",N);
  for (int i=0;i<N;i++){
    fprintf(pFV,"%lf\n",pB[i]);
  }
  fclose(pFV);
  free(pB);
  free(pMat);
 
//   //SOLVERS_LS_readDenseMatrice( &pMat,&pB, &N,"Matrice.txt", "Vect.txt");

//   /*double * X=(double *) malloc(N*sizeof(double));
//     int * ipiv= (int*)malloc(N*sizeof(int));
//     clapack_dgesv(CblasRowMajor,N, 1,
//     pMat, N, ipiv,pB,
//     N);
//     FILE *pFV=fopen("Sol.txt","w");*/
 
 }

void SOLVERS_LS_readDenseMatrice( double ** pA,double **pB, int *pN,char* fileNameA, char* fileNameB){
  FILE *pF=fopen(fileNameA,"r");
  if (!pF)
    printf("SOLVERS_LS_readMatrice can't open file %s\n",fileNameA);
  int i,j,advance;
  int diage=0,ndiage=0;
  int cmp=0;
  int n,sym,nz;
  double a;
  fscanf(pF,"%i%i%i%i",&n,&n,&sym,&nz);
  printf ("n=%i,nz=%i,nz/n2=%e\n",n,nz,(1.0*(nz))/((1.0*(n))*(1.0*(n))));
  *pN=n;
  double *A;
  double *B;
  if (pA){
    *pA=(double*)calloc(n*n,sizeof(double));
    A=*pA;
  }
  if (pB){
    *pB=(double*)calloc(n,sizeof(double));
    B=*pB;
  }
  if (pA){
    for (int ii=0;ii<nz;ii++){
      fscanf(pF,"%i%i%lf",&i,&j,&a);
      //printf("%ld %ld %lf %ld %ld\n",i-1,j-1,a,mat.cols(),mat.rows());
      //printf("(i-1)+n*(j-1)=%i\n",(i-1)+n*(j-1));
      A[(i-1)+n*(j-1)]=a;
    }
    fclose(pF);
  }
  if (pB){
    pF=fopen(fileNameB,"r");
    if (!pF)
      printf("SOLVERS_LS_readMatrice can't open file %s\n",fileNameB);
    fscanf(pF,"%i",&n);
    printf ("vector, n=%i\n",n);
    
    for (int ii=0;ii<n;ii++){
      fscanf(pF,"%lf",&a);
      B[ii]=a;
    }
    fclose(pF);
  }
  
  /* Sparse Matrix (Morse)  
 first line: n m (is symmetic) nbcoef 
 after for each nonzero coefficient:   i j a_ij where (i,j) \in  {1,...,n}x{1,...,m} 
 39550 39550 1  246946
 1         1 0.0078883951066615054515
 2         1 -0.00028802969865261609638*/
  
}

void SOLVERS_LS_getDominantePart(double *Ain, double *Aout, int n){
  int nbZero=0;
  int nbNZero=0;
  for (int i=0;i<n;i++){
    for (int j=0;j<n;j++){
      if ((10*fabs(Ain[i+n*j]) - fabs(Ain[i+n*i]))>0){
        Aout[i+n*j]=Ain[i+n*j];nbNZero++;
      }else{
        Aout[i+n*j]=0;nbZero++;
      }
    }
  }
  printf("getDominantePart nbZero=%i nbNZero=%i\n",nbZero,nbNZero);
}
int SOLVERS_LS_checkSym(double *A,int n){
  for (int i=0;i<n;i++)
    for (int j=0;j<i;j++)
      if (fabs(A[i+n*j]-A[j+n*i]) > 1e-6*MAX(fabs(A[i+n*j]),fabs(A[j+n*i]))){
        printf("line=%i, col=%i, not sym because %e %e\n",i,j,A[i+n*j],A[j+n*i]);
        return 1;
      }
  printf("sym ok\n");
  return 0;
          
}
/*FAUX*/
int diagCond(double *A,double * B,int n){
  int i,j;
  for (i=0;i<n;i++){
    double d = A[i+n*i];
    if (d!=0){
      for (j=0;j<n;j++){
        A[i+n*j]=(A[i+n*j])/d;
      }
      B[i]=B[i]/d;
    }
  }
  
}
void SOLVERS_LS_printSystem(double *A,double * B,int n){
 
  int i,j;
  printf("A:\n");
  for (i=0;i<n;i++){
    for (j=0;j<n;j++){
      printf("\t %e",A[i+n*j]);
    }
    printf("\n");
  }
  printf("B:\n");
  for (i=0;i<n;i++)
    printf("\t %e",B[i]);
  printf("\n");
}

void SOLVERS_LS_printSystemForScilab(double *A,double * B,int n){
  FILE *pF=fopen("scA.sci","w");
  fprintf(pF,"A=[");
  for (int i=0;i<n;i++)
    for (int j=0;j<n;j++){
      fprintf(pF,"%e",A[i+n*j]);
      if (i!=n)
        fprintf(pF,",");
      else
        fprintf(pF,";");
    }
  fprintf(pF,"];\n");
  fclose(pF);
}

int SOLVERS_LS_checkSolutionDense(double * A, double *B,double * X,int n, double criteron){
  int i,j;
  double err=0;
//#ifndef SOLVERS_CBLAS
  for (i=0;i<n;i++){
    for(j=0;j<n;j++){
      B[i]-=A[i+n*j]*X[j];
    }
    if (fabs(B[i])>err)
      err=fabs(B[i]);
  }
// #else
//   /*
//   void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
//                  const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
//                  const int K, const double alpha, const double *A,
//                  const int lda, const double *B, const int ldb,
//                  const double beta, double *C, const int ldc);
//   */
//   cblas_dgemm(CblasRowMajor,CblasRowMajor,CblasNoTrans,
//               CblasNoTrans,n,1,
//               1,1.0,A,
//               n,X,1,
//               -1.0,B,1);
//   and ??
// #endif
  
  printf("SOLVERS_LS_checkSolutionDense err =|AX-B|=%e\n",err);
  if (err > criteron)
    return 1;
  return 0;
}
void SOLVERS_LS_printVect(double * B,int n){
  int i;
  for (i=0;i<n;i++)
    printf("\t %e",B[i]);
  printf("\n");
}
void SOLVERS_LS_prencondDiag(double *A,double * B,int n,double *D){
  for (int i=0;i<n;i++){
    D[i]=sqrt(fabs(A[i+n*i]));
    if (D[i]!=0){
      for (int j=0;j<n;j++){
        A[i+n*j]=A[i+n*j]/D[i];
        A[j+n*i]=A[j+n*i]/D[i];
      }
      B[i]=B[i]/D[i];
    }
  }
}


#ifdef CG_DEBUG_SOL
static double * pSol=0;
static double * pBuf=0;
static double prevDist=100000;
#endif


void _SOLVERS_DEBUG_SET_SOL(double * sol,int N){
#ifdef CG_DEBUG_SOL
  pSol=(double*)malloc(N*sizeof(double));
  pBuf=(double*)malloc(N*sizeof(double));
  memcpy(pSol,sol,N*sizeof(double));
#endif
}
void _SOLVERS_DEBUG_RESET_SOL(){
#ifdef CG_DEBUG_SOL
  free(pSol);
  free(pBuf);
  pSol=0;
  pBuf=0;
  prevDist=100000;
#endif
}
void _SOLVERS_DEBUG_DIST_SOL( double * X,int N){
#ifdef CG_DEBUG_SOL
  if (pSol){
    memcpy(pBuf,pSol,N*sizeof(double));//pBuf<-pSol
    cblas_daxpy(N,-1.0,X,
                1,pBuf,1);// pBuf=-X+pBuf
    double dist=sqrt(cblas_ddot(N,pBuf,1,pBuf,1));
    printf("dist to sol=%e\n",dist);
    if (prevDist<dist)
      printf("WARNING, prevDist=%e<diff=%e \n",prevDist,dist);
    prevDist=dist;
  }
#endif
}
