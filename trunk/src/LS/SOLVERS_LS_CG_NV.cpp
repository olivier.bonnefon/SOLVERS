#include <cblas.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "SOLVERS_LS.h"
#include "SOLVERS_LS_CG_NV.h"
#include "SOLVERS_CU_TOOL.h"
#include "myobTime.hpp"
#define CG_NV_DEBUG
//#define CG_DEBUG
#define CG_DEBUG_SOL
#ifdef CG_DEBUG_SOL
static double * pSol=0;
static double * pBuf=0;
static double prevDist=100000;
#endif

#define WA (4 * block_size) // Matrix A width
#define HA (6 * block_size) // Matrix A height
#define WB (4 * block_size) // Matrix B width
#define HB WA  // Matrix B height
#define WC WB  // Matrix C width 
#define HC HA  // Matrix C height

void rowscale(double * X, const int N,
              const double * ralpha) ;


void randomInit(float* data, int size)
{
  for (int i = 0; i < size; ++i)
    data[i] = rand() / (float)RAND_MAX;
}


int SOLVERS_LS_PREC_CG_NV_INIT(){
  cudaError_t error = cudaGetLastError();

  int devID;
  cudaDeviceProp props;
  
  // get number of SMs on this GPU
  checkCudaErrors(cudaGetDevice(&devID));
  checkCudaErrors(cudaGetDeviceProperties(&props, devID));
}



/*WARNING ONLY WITH C DIAGONAL*/
int SOLVERS_LS_PREC_CG_NV(double *A,double *B,double *X, double * C,double *workD,int N,double criteria,int maxIt,int *nbIt){

  double * H=workD;
  double * G=workD+N;
  double * AH=G+N;
  double * CG=AH;
  double * h_buff=(double*)malloc(N*N*sizeof(double));
  int numIt=0;
  int ii=0;
  myobTime aGlobalTimer;
  aGlobalTimer.reset();
#ifdef CG_NV_DEBUG
  myobTime aTimer;
  aTimer.reset();
  aTimer.start();
#endif
  unsigned int mem_size_H = sizeof(double) * N;
  unsigned int mem_size_G = sizeof(double) * N;
  unsigned int mem_size_X = sizeof(double) * N;
  unsigned int mem_size_A = sizeof(double) * N*N;
  unsigned int mem_size_C = sizeof(double) * N;
  unsigned int mem_size_CG = sizeof(double) * N;
  unsigned int mem_size_AH = sizeof(double) * N;
  double* d_G, *d_A, *d_X,*d_C,*d_CG,*d_H,*d_AH;
  checkCudaErrors(cudaMalloc((void**) &d_H, mem_size_H));
  checkCudaErrors(cudaMalloc((void**) &d_G, mem_size_G));
  checkCudaErrors(cudaMalloc((void**) &d_A, mem_size_A));
  checkCudaErrors(cudaMalloc((void**) &d_X, mem_size_X));
  checkCudaErrors(cudaMalloc((void**) &d_C, mem_size_C));
  checkCudaErrors(cudaMalloc((void**) &d_CG, mem_size_CG));
  d_AH=d_CG;
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("Mem Alloc");
  aTimer.print();
#endif

  for (ii=0;ii<N;ii++)
    h_buff[ii]=C[ii+N*ii];
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
  checkCudaErrors(cudaMemcpy(d_G, B, mem_size_G, cudaMemcpyHostToDevice) );//d_G <- b
  checkCudaErrors(cudaMemcpy(d_A, A, mem_size_A, cudaMemcpyHostToDevice) );//d_A <- A
  checkCudaErrors(cudaMemcpy(d_X, X, mem_size_X, cudaMemcpyHostToDevice) );//d_X <- X
  checkCudaErrors(cudaMemcpy(d_C, h_buff, mem_size_C, cudaMemcpyHostToDevice) );//d_C (vector) <- diag(C)
  
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("mem cp Host -> Device");
  aTimer.print();
#endif

  //G=AX-b
#ifdef CG_NV_DEBUG
  
  memcpy(G,B,N*sizeof(double));//G <- b
  compareDouble("REP1",G,h_buff,d_G,N,1e-6);
  compareDouble("REP2",A,h_buff,d_A,N,1e-6);
  compareDouble("REP3",X,h_buff,d_X,N,1e-6);
#endif
  cublasHandle_t handle;
  checkCublasStatus(cublasCreate(&handle),"cublasCreate() error");
  double zero=0.0;
  double one=1.0;
  double minusone=-1.0;
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
  cublasStatus_t ret = cublasDgemv(handle,
                                   CUBLAS_OP_N, N,N,
                                   &one,d_A,N,
                                   d_X,1,&minusone,
                                   d_G,1);// G <- A.X-b
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("dgemv -> Device:\n");
  aTimer.print();
#endif
  checkCublasStatus(ret,"rep2");
  
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
  cblas_dgemv(CblasRowMajor,
              CblasNoTrans, N,N,
              1.0,A,N,
              X,1,-1.0,
              G,1);// G <- A.X-b
  aTimer.stop();
  aTimer.print();
  
  compareDouble("REP4",G,h_buff,d_G,N,1e-6);
#endif

// not yet
// ret = cublasDdgmm( handle, CUBLAS_SIDE_LEFT,
//                    N,N,
//                    d_A,
//                    N,
//                    d_C,
//                    1,
//                    d_CG,
//                    N);
  checkCudaErrors(cudaMemcpy(d_CG, d_G, mem_size_G, cudaMemcpyDeviceToDevice) );
#ifdef CG_NV_DEBUG
  compareDouble("REP4.5",G,h_buff,d_CG,N,1e-6);
#endif
// printf("G=\n");SOLVERS_LS_printVect(G,N);
// checkCudaErrors(cudaMemcpy(h_buff, d_C, mem_size_C, cudaMemcpyDeviceToHost) );
// printf("C=\n");SOLVERS_LS_printVect(h_buff,N);
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
  rowscale(d_CG,N,d_C);
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("rowscale");
  aTimer.print();
#endif
  
// checkCudaErrors(cudaMemcpy(h_buff, d_CG, mem_size_CG, cudaMemcpyDeviceToHost) );
// printf("d_CG=\n");SOLVERS_LS_printVect(h_buff,N);
  
  checkCublasStatus(ret,"rep3");
#ifdef CG_NV_DEBUG
//CG
// cblas_dgemv(CblasRowMajor,
//             CblasNoTrans, N,N,
//             1.0,C,N,
//             G,1,0.0,
//             CG,1);//CG <- C.G
  aTimer.reset();
  aTimer.start();

  for (ii=0;ii<N;ii++)
    CG[ii]=G[ii]*C[ii+N*ii];

  aTimer.stop();
  aTimer.print();

//  printf("CG=\n");SOLVERS_LS_printVect(CG,N);
  compareDouble("REP5",CG,h_buff,d_CG,N,1e-6);
//  SOLVERS_LS_printVect(h_buff,N);
#endif
  
#ifdef CG_DEBUG
  printf("CG_0=\n");
  SOLVERS_LS_printVect(CG,N);
#endif
//d_H=-d_CG
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
  checkCudaErrors(cudaMemcpy(d_H, d_CG, mem_size_CG, cudaMemcpyDeviceToDevice) );
  ret=cublasDscal(handle,N,&minusone,d_H,1);
  checkCublasStatus(ret,"rep4");
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("dscal");
  aTimer.print();
  
  aTimer.reset();
  aTimer.start();

//H=-CG
  memcpy(H,CG,N*sizeof(double));//H <- CG
  cblas_dscal(N,-1.0,H,1);//H <- -CG
  compareDouble("REP6",H,h_buff,d_H,N,1e-6);
  aTimer.stop();
  aTimer.print();

#endif  

  

  double g_ig_i__c=0;
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
  ret=cublasDdot(handle,N,d_G,1,d_CG,1,&g_ig_i__c);
  checkCublasStatus(ret,"rep5");
  double g_i1g_i1__c=0;
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("ddot");
  aTimer.print();
  
  aTimer.reset();
  aTimer.start();
  double aux3=cblas_ddot(N,G,1,CG,1);
  aTimer.stop();
  aTimer.print();

  
  if (fabs(g_ig_i__c - aux3)>1e-6)
    printf("Error ddot on g_ig_i__c = %lf form dev = %lf from host\n",g_ig_i__c,aux3);
  else
    printf("g_ig_i__c init ok\n");
#endif
  aGlobalTimer.setName("CG boucle");
  aGlobalTimer.start(); 
  while (((numIt < maxIt) && (g_ig_i__c>criteria))||
         numIt < *nbIt){
    /*void cblas_dgemv(const enum CBLAS_ORDER order,
      const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
      const double alpha, const double *A, const int lda,
      const double *X, const int incX, const double beta,
      double *Y, const int incY);*/
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
   
    cublasDgemv(handle,
                CUBLAS_OP_N, N,N,
                &one,d_A,N,
                d_H,1,&zero,
                d_AH,1);// AH_i <- A.H_i
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("dgemv");
  aTimer.print();

#endif

#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
    cblas_dgemv(CblasRowMajor,
                CblasNoTrans, N,N,
                1.0,A,N,
                H,1,0.0,
                AH,1);// AH_i <- A.H_i
  aTimer.stop();
  aTimer.print();
  compareDouble("AH",AH,h_buff,d_AH,N,1e-6);
 
#endif
    
#ifdef CG_DEBUG
    printf("AH_i=\n");
    SOLVERS_LS_printVect(AH,N);
#endif
    double rho=0;
    double auxR=0;
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
    ret=cublasDdot(handle,N,d_G,1,d_H,1,&rho);checkCublasStatus(ret,"rep6");
    ret=cublasDdot(handle,N,d_H,1,d_AH,1,&auxR);checkCublasStatus(ret,"rep7");

#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("ddot x 2");
  aTimer.print();
#endif
    if (fabs(auxR)< 1e-14)
      break;
    rho=-rho/auxR;

#ifdef CG_NV_DEBUG
    aTimer.reset();
    aTimer.start();

    aux3=cblas_ddot(N,G,1,H,1)/cblas_ddot(N,H,1,AH,1);
    aTimer.stop();
    aTimer.print();

      
    if (fabs(rho+aux3) > 1e-6)
      printf("ERREUR on rho (h -d =%lf)\n",fabs(rho-aux3));
    else
      printf(" rho OK\n");
#endif
#ifdef CG_DEBUG
    printf("rho=%e\n",rho);
#endif
    /*void cblas_daxpy(const int N, const double alpha, const double *X,
      const int incX, double *Y, const int incY);*/
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
  ret=cublasDaxpy(handle,
                  N,&rho,d_H,
                  1,d_X,1);
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("Daxpy1");
  aTimer.print();
#endif
    
    checkCublasStatus(ret,"rep8");
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
    cblas_daxpy(N,rho,H,
                1,X,1); // X_{i+1} <- rho H_{i} + X_{i}
  aTimer.stop();
  aTimer.print();
  compareDouble("X_{i+1}",X,h_buff,d_X,N,1e-6);
#endif
#ifdef CG_DEBUG
    printf("X_{i+1}=\n");
    SOLVERS_LS_printVect(X,N);
#endif
#ifdef CG_DEBUG_SOL
    checkCudaErrors(cudaMemcpy(X, d_X, mem_size_X, cudaMemcpyDeviceToHost) );
    _SOLVERS_DEBUG_DIST_SOL(X,N);
#ifdef CG_NV_DEBUG
    aTimer.reset();
    aTimer.start();
#endif
#endif
    ret=cublasDaxpy(handle,N,&rho,d_AH,
                1,d_G,1);// G_{i+1}=rho AH_i + G_i
    checkCublasStatus(ret,"rep9");
#ifdef CG_NV_DEBUG
  aTimer.stop();aTimer.setName("Daxpy2");
  aTimer.print();
  aTimer.reset();
  aTimer.start();
    cblas_daxpy(N,rho,AH,
                1,G,1);// G_{i+1}=rho AH_i + G_i
  aTimer.stop();
  aTimer.print();
  compareDouble("G_{i+1}",G,h_buff,d_G,N,1e-6);
#endif
    // cblas_dgemv(CblasRowMajor,
    //             CblasNoTrans, N,N,
    //             1.0,C,N,
    //             G,1,0.0,
    //             CG,1);// CG_{i+1}=C.G_{i+1}
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
    checkCudaErrors(cudaMemcpy(d_CG, d_G, mem_size_G, cudaMemcpyDeviceToDevice) );
    rowscale(d_CG,N,d_C);
#ifdef CG_NV_DEBUG    
  aTimer.stop();
  aTimer.setName("rowscale");
  aTimer.print();
  aTimer.reset();
  aTimer.start();
    for (ii=0;ii<N;ii++)
      CG[ii]=G[ii]*C[ii+N*ii];
  aTimer.stop();
  aTimer.print();
  compareDouble("CG",CG,h_buff,d_CG,N,1e-6); 
#endif
#ifdef CG_DEBUG
    printf("CG_{i+1}=\n");
    SOLVERS_LS_printVect(CG,N);
#endif    
    double g_ip1g_ip1__c=0;
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif
  ret=cublasDdot(handle,N,d_G,1,d_CG,1,&g_ip1g_ip1__c);
    checkCublasStatus(ret,"rep10");
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("ddot");
  aTimer.print();
#endif  
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
  aux3=cblas_ddot(N,G,1,CG,1);
  aTimer.stop();
  aTimer.print();
      if (fabs(g_ip1g_ip1__c- aux3)>1e-6)
        printf("fabs(g_ip1g_ip1__c- aux3)=%lf>1e-6\n",
               fabs(g_ip1g_ip1__c- aux3));
      else
        printf("g_ip1g_ip1__c OK\n");
#endif
    double gamma=g_ip1g_ip1__c/g_ig_i__c;
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("Dscal");
  aTimer.print();
#endif  
    ret=cublasDscal(handle,N,&gamma,d_H,1);// H <- gamma*H_i
    checkCublasStatus(ret,"rep11");
#ifdef CG_NV_DEBUG     
    aTimer.reset();
    aTimer.start();
    cblas_dscal(N,gamma,H,1);// H <- gamma*H_i
    aTimer.stop();
    aTimer.print();
    compareDouble("H <- gamma*H_i",H,h_buff,d_H,N,1e-6); 
    aTimer.reset();
    aTimer.start();
#endif
    cublasDaxpy(handle,N,&minusone,d_CG,
                1,d_H,1);// H_{i+1}=-CG_{i+1}+gamma*H_i

#ifdef CG_NV_DEBUG    
    aTimer.stop();
    aTimer.setName("Daxpy3");
    aTimer.print();
    aTimer.reset();
    aTimer.start();
    cblas_daxpy(N,-1.0,CG,
                1,H,1);// H_{i+1}=-CG_{i+1}+gamma*H_i
    aTimer.stop();
    aTimer.print();
    compareDouble("H_{i+1}=-CG_{i+1}+gamma*H_i <- gamma*H_i",H,h_buff,d_H,N,1e-6); 
#endif
#ifdef CG_DEBUG
    printf("H_{i+1}=\n");
    SOLVERS_LS_printVect(H,N);
    printf("g_ig_i =%e\n g_ip1g_ip1=%e\n",g_ig_i__c,g_ip1g_ip1__c);
#endif    
    g_ig_i__c=g_ip1g_ip1__c;
    numIt++;
  }
  aGlobalTimer.stop();
  aGlobalTimer.print();
  *nbIt=numIt;
#ifdef CG_NV_DEBUG
  aTimer.reset();
  aTimer.start();
#endif    
  checkCudaErrors(cudaMemcpy(X, d_X, mem_size_X, cudaMemcpyDeviceToHost) );
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("mem Dev -> Host");
  aTimer.print();
  aTimer.reset();
  aTimer.start();
#endif 
  checkCudaErrors(cudaFree(d_H));
  checkCudaErrors(cudaFree(d_G));
  checkCudaErrors(cudaFree(d_A));
  checkCudaErrors(cudaFree(d_X));
  checkCudaErrors(cudaFree(d_C));
  checkCudaErrors(cudaFree(d_CG));

  cudaDeviceReset();
#ifdef CG_NV_DEBUG
  aTimer.stop();
  aTimer.setName("cuda reset and free -> Host");
  aTimer.print();
#endif 
  free (h_buff);
  return 0;
  
}

int SOLVERS_LS_PREC_DIAG_CG_NV(double *A,double *B,double *X, double *workD,int N,double criteria,int maxIt,int *nbIt){
  double * C=workD+SOLVERS_LS_CG_SIZE_WORKD(N);
  for (int i=0;i<N;i++){
    for (int j=0;j<N;j++)
      C[i+N*j]=0;
    if (A[i+N*i]!=0){
      C[i+N*i]=1.0/fabs(A[i+N*i]);
    }else
      C[i+N*i]=1.0;
  }
#ifdef CG_DEBUG
  printf("precond diag:\n");
  SOLVERS_LS_printVect(C,N*N);
#endif
  int res=SOLVERS_LS_PREC_CG_NV(A,B,X,C,workD,N,criteria,maxIt,nbIt);
  return res;

}

// int ___MEMO(){
//   cudaError_t error = cudaGetLastError();

//   int devID;
//   cudaDeviceProp props;
  
//   // get number of SMs on this GPU
//   checkCudaErrors(cudaGetDevice(&devID));
//   checkCudaErrors(cudaGetDeviceProperties(&props, devID));
//   int block_size = (props.major < 2) ? 16 : 32;
//   unsigned int uiWA, uiHA, uiWB, uiHB, uiWC, uiHC;
//   int iSizeMultiple = 5;
//   if (props.multiProcessorCount <= 4) {
// 		uiWA = 2 * block_size * iSizeMultiple;
// 		uiHA = 4 * block_size * iSizeMultiple;
// 		uiWB = 2 * block_size * iSizeMultiple;
// 		uiHB = 4 * block_size * iSizeMultiple;
// 		uiWC = 2 * block_size * iSizeMultiple;
// 		uiHC = 4 * block_size * iSizeMultiple;
// 	} else {
// 		uiWA = WA * iSizeMultiple;
// 		uiHA = HA * iSizeMultiple;
// 		uiWB = WB * iSizeMultiple;
// 		uiHB = HB * iSizeMultiple;
// 		uiWC = WC * iSizeMultiple;
// 		uiHC = HC * iSizeMultiple;
// 	}
//   printf("\nUsing Matrix Sizes: A(%u x %u), B(%u x %u), C(%u x %u)\n\n", 
//          uiWA, uiHA, uiWB, uiHB, uiWC, uiHC);

//   // allocate host memory for matrices A and B
//   unsigned int size_A = uiWA * uiHA;
//   unsigned int mem_size_A = sizeof(float) * size_A;
//   float* h_A = (float*)malloc(mem_size_A);
//   unsigned int size_B = uiWB * uiHB;
//   unsigned int mem_size_B = sizeof(float) * size_B;
//   float* h_B = (float*)malloc(mem_size_B);

//   // initialize host memory
//   randomInit(h_A, size_A);
//   randomInit(h_B, size_B);
//   // allocate device memory
//   float* d_A, *d_B, *d_C;
//   unsigned int size_C = uiWC * uiHC;
//   unsigned int mem_size_C = sizeof(float) * size_C;

//   // allocate host memory for the result
//   float* h_C      = (float*) malloc(mem_size_C);
// 	float* h_CUBLAS = (float*) malloc(mem_size_C);
//   checkCudaErrors(cudaMalloc((void**) &d_A, mem_size_A));
//   checkCudaErrors(cudaMalloc((void**) &d_B, mem_size_B));
//   // copy host memory to device
//   checkCudaErrors(cudaMemcpy(d_A, h_A, mem_size_A, cudaMemcpyHostToDevice) );
//   checkCudaErrors(cudaMemcpy(d_B, h_B, mem_size_B, cudaMemcpyHostToDevice) );
    
//   checkCudaErrors(cudaMalloc((void**) &d_C, mem_size_C));
//   // check if kernel execution generated and error
//   getLastCudaError("Memm alloc failed\n");
//   // setup execution parameters
//   dim3 threads(block_size, block_size);
//   dim3 grid(uiWC / threads.x, uiHC / threads.y);
//   cublasHandle_t handle;
//   checkCublasStatus(cublasCreate(&handle),"cublasCreate() error");
//   const float alpha = 1.0f;
//   const float beta = 0.0f;

//   //Perform warmup operation with cublas
//   cublasStatus_t ret = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, uiWB, uiHA, uiWA, &alpha, d_B, uiWB, d_A, uiWA, &beta, d_C, uiWA);
// //  checkError(ret, "cublas Sgemm returned an error!\n");

//   // Start Timing
// //		sdkCreateTimer(&timer_cublas);
// //		sdkStartTimer(&timer_cublas);
//   //note cublas is column primary!
//   //need to transpose the order
//   cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, uiWB, uiHA, uiWA, &alpha, d_B, uiWB, d_A, uiWA, &beta, d_C, uiWA);

// // check if kernel execution generated and error
//   getLastCudaError("CUBLAS Kernel execution failed");
//   cudaDeviceSynchronize();
// // stop and destroy timer
// //		sdkStopTimer(&timer_cublas);

// //  double dSeconds = sdkGetTimerValue(&timer_cublas)/((double)nIter * 1000.0);
// //  double dNumOps = 2.0 * (double)uiWA * (double)uiHA * (double)uiWB;
// //  double gflops = 1.0e-9 * dNumOps/dSeconds;

// //Log througput, etc
// //	shrLogEx(LOGBOTH | MASTER, 0, "> CUBLAS         %.4f GFlop/s, Time = %.5f s, Size = %.0f Ops\n\n", 			gflops, dSeconds, dNumOps);

// //		sdkDeleteTimer(&timer_cublas);

// // copy result from device to host
//   checkCudaErrors(cudaMemcpy(h_CUBLAS, d_C, mem_size_C, cudaMemcpyDeviceToHost) );

// //checkError(cublasDestroy(handle), "cublasDestroy() error!\n");
	
//   free(h_A);
//   free(h_B);
//   free(h_C);
// //  free(reference);
//   checkCudaErrors(cudaFree(d_A));
//   checkCudaErrors(cudaFree(d_B));
//   checkCudaErrors(cudaFree(d_C));

//   cudaDeviceReset();
// //  shrQAFinishExit(argc, (const char **)argv, (resCUDA == true && resCUBLAS == true) ? QA_PASSED : QA_FAILED);

    

//   return 1;
// }
int SOLVERS_LS_CG_NV(double *A,double *B,double *X,double *workD,int N,double criteria,int maxIt,int *nbIt){
  cudaError_t error = cudaGetLastError();
  return 1;
}
