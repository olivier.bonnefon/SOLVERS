#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "SOLVERS_LS.h"
#include "SOLVERS_LS_Tool.h"

//#define GS_DEBUG


int SOLVERS_LS_GS(double *A,double *B,double *X, int N,double criteria,int maxIt,int *nbIt){
  
  double residu=0;
#ifdef GS_DEBUG
  printf("GS X0:\n");
  SOLVERS_LS_printVect(X,N);
#endif
  for (*nbIt=0;*nbIt<maxIt;(*nbIt)++){
    residu=0;
    for (int i=0;i<N;i++){
      double savXi=X[i];
      double sumi=B[i];
      X[i]=0.0;
      for (int j=0;j<N;j++){
        sumi-= A[i+N*j]*X[j];
      }
      X[i]=sumi/A[i+N*i];
      if (fabs(X[i]-savXi)>residu){
        residu=fabs(X[i]-savXi);
      }
#ifdef GS_DEBUG
      printf("GS X%i residu=%e:\n",numit+1,residu);
      SOLVERS_LS_printVect(X,N);
#endif
    }
    if (residu<criteria){
      printf("SOLVERS_LS_GS cv in %i it\n",*nbIt);
      return 0;
    }
  }
  printf("SOLVERS_LS_GS err=%e\n",residu);
  return 1;
}
