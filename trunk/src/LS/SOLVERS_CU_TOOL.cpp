#include "SOLVERS_CU_TOOL.h"

int checkCublasStatus ( cublasStatus_t status, const char *msg ) 
{
  if ( status != CUBLAS_STATUS_SUCCESS ) {
    fprintf (stderr, "!!!! CUBLAS %s ERROR \n", msg);
    return 1;
  }
  return 0;
}



int gpuDeviceInit(int devID)
{
  int deviceCount;
  checkCudaErrors(cudaGetDeviceCount(&deviceCount));
  if (deviceCount == 0) {
    fprintf(stderr, "gpuDeviceInit() CUDA error: no devices supporting CUDA.\n");
    exit(-1);
  }
  if (devID < 0) 
    devID = 0;
  if (devID > deviceCount-1) {
    fprintf(stderr, "\n");
    fprintf(stderr, ">> %d CUDA capable GPU device(s) detected. <<\n", deviceCount);
    fprintf(stderr, ">> gpuDeviceInit (-device=%d) is not a valid GPU device. <<\n", devID);
    fprintf(stderr, "\n");
    return -devID;
  }

  cudaDeviceProp deviceProp;
  checkCudaErrors( cudaGetDeviceProperties(&deviceProp, devID) );
  if (deviceProp.major < 1) {
    fprintf(stderr, "gpuDeviceInit(): GPU device does not support CUDA.\n");
    exit(-1);                                                  \
  }

  checkCudaErrors( cudaSetDevice(devID) );
  printf("> gpuDeviceInit() CUDA device [%d]: %s\n", devID, deviceProp.name);
  return devID;
}
int compareDouble(char * mess, double * h_D,double * h_buff, double *d_D,int N, double crit){

  unsigned int mem_size = N* sizeof(double);
  checkCudaErrors(cudaMemcpy(h_buff, d_D, mem_size, cudaMemcpyDeviceToHost) );
  double *aux1=h_D;
  double *aux2=h_buff;
  
  for (int i=0;i<N;i++){
    if (fabs(*aux1-*aux2)>crit){
      printf("*** %s FAILED: compareDouble i=%d failed: %lf > %lf\n",mess,i,fabs(*aux1-*aux2),crit);
      return 1;
    }
    aux1++;
    aux2++;
  }
  printf("***%s OK\n",mess);
  return 0;
}
