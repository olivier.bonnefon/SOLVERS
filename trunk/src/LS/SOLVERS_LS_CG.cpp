#include <cblas.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "SOLVERS_LS.h"
#include "SOLVERS_LS_Tool.h"
//#define CG_DEBUG
#include "myobTime.hpp"
    
int SOLVERS_LS_CG_SIZE_WORKD(int N){
  return 3*N;
}
int SOLVERS_LS_CG_PREC_DIAG_SIZE_WORKD(int N){
  return 3*N+N*N;
}
int SOLVERS_LS_PREC_DIAG_CG(double *A,double *B,double *X, double *workD,int N,double criteria,int maxIt,int *nbIt){
  double * C=workD+SOLVERS_LS_CG_SIZE_WORKD(N);
  for (int i=0;i<N;i++){
    for (int j=0;j<N;j++)
      C[i+N*j]=0;
    if (A[i+N*i]!=0){
      C[i+N*i]=1.0/fabs(A[i+N*i]);
    }else
      C[i+N*i]=1.0;
  }
#ifdef CG_DEBUG
  printf("precond diag:\n");
  //SOLVERS_LS_printVect(C,N*N);
#endif
  int res=SOLVERS_LS_PREC_CG(A,B,X,C,workD,N,criteria,maxIt,nbIt);
  return res;

}
/*WARNING ONLY WITH C DIAGONAL*/
int SOLVERS_LS_PREC_CG(double *A,double *B,double *X, double * C,double *workD,int N,double criteria,int maxIt,int *nbIt){
  double * H=workD;
  double * G=workD+N;
  double * AH=G+N;
  double * CG=AH;
  int numIt=0;
  int ii=0;
  myobTime aGlobalTimer;
  aGlobalTimer.reset();
  //G=AX-b
  memcpy(G,B,N*sizeof(double));//G <- b
  cblas_dgemv(CblasRowMajor,
              CblasNoTrans, N,N,
              1.0,A,N,
              X,1,-1.0,
              G,1);// G <- A.X-b
  //CG
  // cblas_dgemv(CblasRowMajor,
  //             CblasNoTrans, N,N,
  //             1.0,C,N,
  //             G,1,0.0,
  //             CG,1);//CG <- C.G
  for (ii=0;ii<N;ii++)
    CG[ii]=G[ii]*C[ii+N*ii];
#ifdef CG_DEBUG
    printf("CG_0=\n");
    SOLVERS_LS_printVect(CG,N);
#endif
  //H=-CG
  memcpy(H,CG,N*sizeof(double));//H <- CG
  cblas_dscal(N,-1.0,H,1);//H <- -CG
  
  double g_ig_i__c=cblas_ddot(N,G,1,CG,1);
  double g_i1g_i1__c=0;
  
  aGlobalTimer.setName("CG boucle");
  aGlobalTimer.start();       
  while (((numIt < maxIt) && (g_ig_i__c>criteria))||
    numIt < *nbIt){
    /*void cblas_dgemv(const enum CBLAS_ORDER order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY);*/
    cblas_dgemv(CblasRowMajor,
                CblasNoTrans, N,N,
                1.0,A,N,
                H,1,0.0,
                AH,1);// AH_i <- A.H_i
#ifdef CG_DEBUG
    printf("AH_i=\n");
    SOLVERS_LS_printVect(AH,N);
#endif
    double aux1=cblas_ddot(N,H,1,AH,1);
    if (fabs(aux1) < 1e-14)
      break;
    double rho=-cblas_ddot(N,G,1,H,1)/aux1;
#ifdef CG_DEBUG
    printf("rho=%e\n",rho);
#endif
    /*void cblas_daxpy(const int N, const double alpha, const double *X,
      const int incX, double *Y, const int incY);*/
    cblas_daxpy(N,rho,H,
                1,X,1); // X_{i+1} <- rho H_{i} + X_{i}
#ifdef CG_DEBUG
    printf("X_{i+1}=\n");
    SOLVERS_LS_printVect(X,N);
#endif
#ifdef CG_DEBUG_SOL
     printf("it = %i\n",numIt);
    _SOLVERS_DEBUG_DIST_SOL(X,N);
#endif
    cblas_daxpy(N,rho,AH,
                1,G,1);// G_{i+1}=rho AH_i + G_i
    // cblas_dgemv(CblasRowMajor,
    //             CblasNoTrans, N,N,
    //             1.0,C,N,
    //             G,1,0.0,
    //             CG,1);// CG_{i+1}=C.G_{i+1}
    for (ii=0;ii<N;ii++)
      CG[ii]=G[ii]*C[ii+N*ii];

#ifdef CG_DEBUG
    printf("CG_{i+1}=\n");
    SOLVERS_LS_printVect(CG,N);
#endif    
    double g_ip1g_ip1__c=cblas_ddot(N,G,1,CG,1);
    double gamma=g_ip1g_ip1__c/g_ig_i__c;
    cblas_dscal(N,gamma,H,1);// H <- gamma*H_i
    cblas_daxpy(N,-1.0,CG,
                1,H,1);// H_{i+1}=-CG_{i+1}+gamma*H_i
#ifdef CG_DEBUG
    printf("H_{i+1}=\n");
    SOLVERS_LS_printVect(H,N);
    printf("g_ig_i =%e\n g_ip1g_ip1=%e\n",g_ig_i__c,g_ip1g_ip1__c);
#endif    
    g_ig_i__c=g_ip1g_ip1__c;
    numIt++;
   }
  aGlobalTimer.stop();
  aGlobalTimer.print();

  *nbIt=numIt;
  return 0;
}
int SOLVERS_LS_CG(double *A,double *B,double *X,double *workD,int N,double criteria,int maxIt,int *nbIt){
  double * H=workD;
  double * G=workD+N;
  double * AH=G+N;
  int numIt=0;
  //G=AX-b
  memcpy(G,B,N*sizeof(double));//G <- b
  cblas_dgemv(CblasRowMajor,
              CblasNoTrans, N,N,
              1.0,A,N,
              X,1,-1.0,
              G,1);// G <- A.X-b
#ifdef CG_DEBUG
    printf("G_0=\n");
    SOLVERS_LS_printVect(G,N);
#endif
  //H=-G
  memcpy(H,G,N*sizeof(double));//H <- G
  cblas_dscal(N,-1.0,H,1);//H <- -G
  
  double g_ig_i=cblas_ddot(N,G,1,G,1);
  double g_i1g_i1=0;
  while ((g_ig_i>criteria && numIt< maxIt) || numIt<(*nbIt) ){
    /*void cblas_dgemv(const enum CBLAS_ORDER order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY);*/
    cblas_dgemv(CblasRowMajor,
                CblasNoTrans, N,N,
                1.0,A,N,
                H,1,0.0,
                AH,1);// AH_i <- A.H_i
#ifdef CG_DEBUG
    printf("AH_i=\n");
    SOLVERS_LS_printVect(AH,N);
#endif
    double rho=-cblas_ddot(N,G,1,H,1)/cblas_ddot(N,H,1,AH,1);
#ifdef CG_DEBUG
    printf("rho=%e\n",rho);
#endif

    /*void cblas_daxpy(const int N, const double alpha, const double *X,
      const int incX, double *Y, const int incY);*/
    cblas_daxpy(N,rho,H,
                1,X,1); // X_{i+1} <- rho H_{i} + X_{i}
#ifdef CG_DEBUG
    printf("X_{i+1}=\n");
    SOLVERS_LS_printVect(X,N);
#endif
#ifdef CG_DEBUG_SOL
    printf("it = %i\n",numIt);
    _SOLVERS_DEBUG_DIST_SOL(X,N);
#endif

    cblas_daxpy(N,rho,AH,
                1,G,1);// G_{i+1}=rho AH_i + G_i
#ifdef CG_DEBUG
    printf("G_{i+1}=\n");
    SOLVERS_LS_printVect(G,N);
#endif
    double g_ip1g_ip1=cblas_ddot(N,G,1,G,1);
    double gamma=g_ip1g_ip1/g_ig_i;
    cblas_dscal(N,gamma,H,1);// H <- gamma*H_i
    cblas_daxpy(N,-1.0,G,
                1,H,1);// H_{i+1}=-G_{i+1}+gamma*H_i
    //printf("residu= %e\n",sqrt(g_ip1g_ip1));
#ifdef CG_DEBUG
    printf("H_{i+1}=\n");
    SOLVERS_LS_printVect(H,N);
    printf("g_ig_i =%e\n g_ip1g_ip1=%e\n",g_ig_i,g_ip1g_ip1);
#endif
    g_ig_i=g_ip1g_ip1;
    numIt++;
   }
  *nbIt=numIt;
  return 0;
}
