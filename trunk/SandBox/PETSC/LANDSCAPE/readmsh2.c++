#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>



void f(float x,float y, float *px, float *py){
  float scalx=(1+(y/1000)*(y/1000));
  float scaly=(1+(x/3000)*(x/3000));
  if (scalx>1.5)
    scalx=1.5;
  if (scaly>1.2)
    scaly=1.2;
  scalx=1+fabs(y*0.0001)*fabs(y*0.0001);
  scaly=sqrt(1+fabs(x*0.0002));
  *px=x/scalx;
  *px=(*px)/500.0;
  *py=y/scaly;
  *py=(*py)/500.0;
}

using namespace std;

int main () {
  ifstream myfile;
  string line;
  float x,y,z;
  float xmin=100;
  float xmax=-100;
  float ymin=100;
  float ymax=-100;
  
  myfile.open ("/home/olivierb/Mesh_1.msh");
  if (myfile.is_open())
  {
    getline (myfile,line);
    getline (myfile,line) ;
    getline (myfile,line) ;
    getline (myfile,line) ;
    getline (myfile,line) ;
    char* chr = (char *)line.c_str();
    //printf("%s\n",chr);
    int Nnodes;
    int node;
    //size_t ss= 2*Nnodes*sizeof(float);
    //float * coords=(float*) malloc(2*Nnodes*sizeof(float));
    sscanf(chr,"%i",&Nnodes);
    float * coords=(float*) malloc(2*Nnodes*sizeof(float));
    float lx,ly;
    printf("int obNbVertex=%i;\n",Nnodes);
    printf("double obVertex[%i]={\n",2*Nnodes);
    for (int i =0;i<Nnodes;i++){
      getline (myfile,line) ;
      chr = (char *)line.c_str();
      sscanf(chr,"%i%e%e%e",&node,&coords[2*i],&coords[2*i+1],&z);
      coords[2*i]=coords[2*i];
      coords[2*i+1]=coords[2*i+1];
      if (xmin>coords[2*i])
        xmin=coords[2*i];
      if (ymin>coords[2*i+1])
        ymin=coords[2*i+1];
      if (xmax<coords[2*i])
        xmax=coords[2*i];
      if (ymax<coords[2*i+1])
        ymax=coords[2*i+1];
      f(coords[2*i],coords[2*i+1],&lx,&ly);
      printf("%e,%e",lx,ly);
      if (i<Nnodes-1)
        printf(",\n");
      else
        printf("};\n");
    }

    getline (myfile,line);
    getline (myfile,line);
    getline (myfile,line);
    
    int NElems;
    int elem;
    int n1,n2,bidon;
    chr = (char *)line.c_str();
    sscanf(chr,"%i",&NElems);
    int type=0;
    int cmp=0;
    
    printf("int obNbBound=358;\nint obBoundary[358]={\n");
    while(1){
      getline (myfile,line) ;
      chr = (char *)line.c_str();
      sscanf(chr,"%i%i",&elem,&type);
      if (type==1){
       
        sscanf(chr,"%i%i%i%i%i%i%i",&bidon,&bidon,&bidon,&bidon,&bidon,&n1,&n2);
        n2=n2-1;
        float sx=0.0001;
        if (fabs(coords[2*n2]-xmin)<sx || fabs(coords[2*n2]-xmax)<sx|| fabs(coords[2*n2+1]-ymax)<sx|| fabs(coords[2*n2+1]-ymin)<sx){
          printf("%i,\n",n2);
          cmp++;
        }
      }
      if (type == 2)
        break;
      NElems--;
    }
    printf("\n//%i 358\n",cmp);

    printf("int obNbCells=%i;\nint obCells[%i]={\n",NElems,3*NElems);
    int * pLabel=(int*)malloc(NElems*sizeof(int));
    for (int i =0;i<NElems;i++){
      int ii,jj,kk;
      int label=1;
      chr = (char *)line.c_str();
      sscanf(chr,"%i%i%i%i%i%i%i%i",&bidon,&bidon,&bidon,&bidon,&bidon,&ii,&jj,&kk);
      ii=ii-1;
      jj=jj-1;
      kk=kk-1;
      float xb=coords[2*jj]+coords[2*ii]+coords[2*kk];
      float yb=coords[2*jj+1]+coords[2*ii+1]+coords[2*kk+1];
      xb=xb/3.0;
      yb=yb/3.0;
      int xbi = floor(xb);
      int ybi = floor(yb);
      if (xbi<-1300)
        label=4;
      else if (xbi>1300)
        label=5;
      else if (ybi>900)
        label=2;
      else if (ybi>-900)
        label=3;
      else
        label=1;
      if (abs(xbi+100)%200 < 10 || abs(xbi+100)%200 > 190)
        label=6;
      if (abs(ybi+100)%200 < 10 || abs(ybi+100)%200 > 190)
        label=6;
      
      pLabel[i]=label;
      
      float x0=coords[2*jj]-coords[2*ii];
      float y0=coords[2*jj+1]-coords[2*ii+1];
      float x1=coords[2*jj]-coords[2*kk];
      float y1=coords[2*jj+1]-coords[2*kk+1];
      if (fabs(x0*y1-x1*y0)<1e-5)
        printf("WARNING nul det\n");
      if ((x0*y1-x1*y0)<0)
        printf("%i,%i,%i",ii,jj,kk);
      else
        printf("%i,%i,%i",ii,kk,jj);
      if (i<NElems-1)
        printf(",\n");
      else
        printf("};\n");
      getline (myfile,line) ;
    }
    printf("int labelCells[%i]={\n",NElems);
    for (int i =0;i<NElems;i++){
      printf("%i",pLabel[i]);
       if (i<NElems-1)
        printf(",");
      else
        printf("};\n");
    }
    myfile.close();
  }
  else cout << "Unable to open file"; 
}
