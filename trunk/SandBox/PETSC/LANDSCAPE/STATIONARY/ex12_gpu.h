#include <stdlib.h>

#define SPATIAL_DIM_0 2

/* Type for scalars */
typedef float realType;

/* Type for vectors */
typedef float2 vecType;

#define NUM_FIELDS 1

#define NUM_BASIS_COMPONENTS_TOTAL 1
