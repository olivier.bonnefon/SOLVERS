#include <stdlib.h>

const int numQuadraturePoints_0 = 1;

/* Quadrature points
   - (x1,y1,x2,y2,...) */
const PetscReal points_0[2] = {
  -0.333333333333,
  -0.333333333333};

/* Quadrature weights
   - (v1,v2,...) */
const PetscReal weights_0[1] = {2.0};

const int numBasisFunctions_0 = 3;

const int numBasisComponents_0 = 1;

/* Nodal basis function evaluations
    - basis function is fastest varying, then point */
const PetscReal Basis_0[3] = {
  0.333333333333,
  0.333333333333,
  0.333333333333};

/* Nodal basis function derivative evaluations,
    - derivative direction fastest varying, then basis function, then point */
const float2 BasisDerivatives_0[3] = {
  -0.5,
  -0.5,
  0.5,
  0.0,
  0.0,
  0.5};

#define f1_func f1_boundary

#define f1_coef_func f1_boundary_coef

/* Number of concurrent blocks */
const int N_bl = 1;

#define NUM_FIELDS 1

#define NUM_BASIS_COMPONENTS_TOTAL 1
