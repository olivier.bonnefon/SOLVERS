from paraview.simple import *

baseName="/Sandbox/BUILD/TPPETSC/WORK6/step_"
numComp=1
for num in range(0,399):
    print num
    fileName=baseName+str(num)+".vtk"
    reader = OpenDataFile(fileName)
    Show()
    dp = GetDisplayProperties(reader)
    dp.Representation = 'Surface With Edges'
    elev = Elevation(reader)
    ai = elev.PointData[1]
    name = ai.GetName()
    dp.LookupTable = MakeBlueToRedLT(0.1, 0.4)
    dp.ColorAttributeType = 'POINT_DATA'
    dp.ColorArrayName = name
    dp.LookupTable.VectorComponent = numComp
    Render()
    WriteImage(baseName+str(num)+".png")
    Delete(reader)

    #WriteImage(baseName+str(num)+".png")
    #Hide(reader)


