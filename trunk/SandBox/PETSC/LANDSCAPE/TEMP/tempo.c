#include <stdio.h>
#define SIMPLE_MESH

#ifdef CASE1
#include "../mesh1.h"
#else
#ifdef SIMPLE_MESH
#include "../simple_mesh.h"
#else
#include "../ob_mesh.h"
#endif
#endif


static char help[] = "Poisson Problem in 2d and 3d with simplicial finite elements.\n\
We solve the Poisson problem in a rectangular\n\
domain, using a parallel unstructured mesh (DMPLEX) to discretize it.\n\n\n";

#include <petscdmplex.h>
#include <petscsnes.h>
#if defined(PETSC_HAVE_EXODUSII)
#include <exodusII.h>
#endif
#include "petsc-private/petscimpl.h"

/* int obNbBound=8; */
/* int obBoundary[8]={ */
/* 	0, */
/* 	1, */
/* 	2, */
/* 	3, */
/* 	5, */
/* 	6, */
/* 	7, */
/* 	8}; */
/* int obNbCells=8; */
/* int obCells[24]={ */
/* 	0,1,3, */
/* 	1,4,3, */
/* 	1,2,4, */
/* 	2,5,4, */
/* 	3,4,6, */
/* 	4,7,6, */
/* 	4,5,7, */
/* 	5,8,7}; */

/* int obNbVertex=9; */
/* double obVertex[18]={ */
/* 	0.000000e+00,0.000000e+00, */
/* 	5.000000e-01,0.000000e+00, */
/* 	1.000000e+00,0.000000e+00, */
/* 	0.000000e+00,5.000000e-01, */
/* 	5.000000e-01,5.000000e-01, */
/* 	1.000000e+00,5.000000e-01, */
/* 	0.000000e+00,1.000000e+00, */
/* 	5.000000e-01,1.000000e+00, */
/* 	1.000000e+00,1.000000e+00}; */

/*------------------------------------------------------------------------------
  This code can be generated using 'bin/pythonscripts/PetscGenerateFEMQuadrature.py dim order dim 1 laplacian dim order dim 1 boundary src/snes/examples/tutorials/ex12.h'
 -----------------------------------------------------------------------------*/
#include "tempo.h"
#include "tempo_bd.h"


void zero(const PetscReal coords[], PetscScalar *u)
{
  *u = 0.0;
}

/*
  In 2D for Dirichlet conditions, we use exact solution:

    u = x^2 + y^2
    f = 4

  so that

    -\Delta u + f = -4 + 4 = 0

  For Neumann conditions, we have

    \nabla u \cdot -\hat y |_{y=0} = -(2y)|_{y=0} = 0 (bottom)
    \nabla u \cdot  \hat y |_{y=1} =  (2y)|_{y=1} = 2 (top)
    \nabla u \cdot -\hat x |_{x=0} = -(2x)|_{x=0} = 0 (left)
    \nabla u \cdot  \hat x |_{x=1} =  (2x)|_{x=1} = 2 (right)

  Which we can express as

    \nabla u \cdot  \hat n|_\Gamma = 2 (x + y)
*/
double alpha=1.0/0.1;
double WW=2.0;//2*M_PI
#define RHO_SCALE 1.0
//static double rho[10]={RHO_SCALE*0.1,RHO_SCALE*0.2,RHO_SCALE*0.3,RHO_SCALE*0.4,RHO_SCALE*0.5,RHO_SCALE*0.6,RHO_SCALE*0.7,RHO_SCALE*0.8,RHO_SCALE*0.9,RHO_SCALE*1.0};
static double rho[10]={1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0};
//static double RR[10]={2,3,4,5,10,12,14,16,18,20};
#define RR_SCALE 1
//static double RR[10]={RR_SCALE*1,RR_SCALE*2,RR_SCALE*3,RR_SCALE*4,RR_SCALE*5,RR_SCALE*6,RR_SCALE*7,RR_SCALE*8,RR_SCALE*9,RR_SCALE*7.5};
static double RR[10]={10,10,10,10,10,10,10,10,10,10};
void quadratic_u_2d(const PetscReal x[], PetscScalar *u)
{
  //*u = x[0]*x[0] + x[1]*x[1];
  //*u = 3*x[0]*x[0]*x[0] + x[1]*x[1]*x[1];
  if (x[0]*x[1]<0.01 || (1-x[0])*(1-x[1])<0.01)
    *u = exp(WW*(x[0])) +exp(WW*(x[1]))  ;
  else
    *u=0;
}
void my_BC(const PetscReal x[], PetscScalar *u)
{
  //*u = x[0]*x[0] + x[1]*x[1];
  //*u = 3*x[0]*x[0]*x[0] + x[1]*x[1]*x[1];
  *u = 0.2;//exp(WW*(x[0])) +exp(WW*(x[1]))  ;
}
void (*my_BCs[NUM_BASIS_COMPONENTS_TOTAL])(const PetscReal x[], PetscScalar *u);
#undef __FUNCT__
#define __FUNCT__ "f0_u"
void f0_u(const PetscScalar u[], const PetscScalar gradU[], const PetscReal x[], PetscScalar f0[], void * user)
{
  const PetscInt Ncomp = NUM_BASIS_COMPONENTS_0;
  PetscInt       comp;
  double lRR=10.0;
  PetscInt crop;
  double * utm1=((AppCtx *) user)->putm1Plugin;
  PetscFunctionBeginUser;
  crop=((AppCtx*)user)->tcrop[((AppCtx*)user)->elem];
  lRR=RR[crop-1];
//  lRR=10.0;
  /* if (((AppCtx*)user)->elem=11){ */
  /*   for (comp = 0; comp < Ncomp; ++comp){ */
  /*     printf("fo_u: utm1[%i]=%e\n",11,utm1[11]); */
  /*     printf("fo_u: u[%i]=%e\n",comp,u[comp]); */
  /*   } */
  /* } */
//  for (comp = 0; comp < Ncomp; ++comp) f0[comp] = -lRR*u[comp]*(1-u[comp]) ;
//
  for (comp = 0; comp < Ncomp; ++comp){
    printf("fo_u: utm1[%i]=%e\n",comp,utm1[comp]);
    printf("fo_u: u[%i]=%e\n",comp,u[comp]);
  }
  for (comp = 0; comp < Ncomp; ++comp) f0[comp] = -lRR*u[comp]*(1-u[comp]);// + alpha*u[comp] - alpha*utm1[comp];
}
#undef __FUNCT__
#define __FUNCT__ "g0_u"
void g0_u(const PetscScalar u[], const PetscScalar gradU[], const PetscReal x[], PetscScalar g1[], void * user)
{
  const PetscInt dim = SPATIAL_DIM_0;
  PetscInt       d;

  //for (d = 0; d < dim; ++d) g1[d*dim+d] = 1.0; /* \frac{\partial\phi^{u_d}}{\partial x_d} */
  const PetscInt Ncomp = NUM_BASIS_COMPONENTS_0;
  PetscInt       comp;
  int rank;
  PetscErrorCode ierr;
  DMLabel         label;
  PetscBool        has;
  double lRR=10.0;
  PetscInt crop;
  PetscFunctionBeginUser;
  crop=((AppCtx*)user)->tcrop[((AppCtx*)user)->elem];

  lRR=RR[crop-1];
//  lRR=10.0;
  for (comp = 0; comp < Ncomp; ++comp) {
    g1[comp] = -lRR*(1-2*u[comp]);// +alpha;
    //g1[comp] = -lRR*(1-2*u[comp]) ;
  }
}
#undef __FUNCT__
#define __FUNCT__ "f0_bd_u"
void f0_bd_u(const PetscScalar u[], const PetscScalar gradU[], const PetscReal x[], const PetscReal n[], PetscScalar f0[], void * user)
{
  const PetscInt Ncomp = NUM_BASIS_COMPONENTS_0;
  PetscInt       comp;
  PetscScalar    val = 0.0;

  PetscFunctionBeginUser;
  if ((fabs(x[0] - 1.0) < 1.0e-9) || (fabs(x[1] - 1.0) < 1.0e-9)) {val = -2.0;}
  for (comp = 0; comp < Ncomp; ++comp) f0[comp] = val;
}

#undef __FUNCT__
#define __FUNCT__ "f0_bd_zero"
void f0_bd_zero(const PetscScalar u[], const PetscScalar gradU[], const PetscReal x[], const PetscReal n[], PetscScalar f0[], void * user)
{
  const PetscInt Ncomp = NUM_BASIS_COMPONENTS_0;
  PetscInt       comp;

  PetscFunctionBeginUser;
  for (comp = 0; comp < Ncomp; ++comp) f0[comp] = 0.0;
}

#undef __FUNCT__
#define __FUNCT__ "f1_bd_zero"
void f1_bd_zero(const PetscScalar u[], const PetscScalar gradU[], const PetscReal x[], const PetscReal n[], PetscScalar f1[], void * user)
{
  const PetscInt Ncomp = SPATIAL_DIM_0*NUM_BASIS_COMPONENTS_0;
  PetscInt       comp;

  PetscFunctionBeginUser;
  for (comp = 0; comp < Ncomp; ++comp) f1[comp] = 0.0;
}

#undef __FUNCT__
#define __FUNCT__ "f1_u"
/* gradU[comp*dim+d] = {u_x, u_y} or {u_x, u_y, u_z} */
void f1_u(const PetscScalar u[], const PetscScalar gradU[], const PetscReal x[], PetscScalar f1[], void * user)
{
  const PetscInt dim   = SPATIAL_DIM_0;
  const PetscInt Ncomp = NUM_BASIS_COMPONENTS_0;
  PetscInt       comp, d;
  PetscInt crop;
  
  PetscFunctionBeginUser;
  crop=((AppCtx*)user)->tcrop[((AppCtx*)user)->elem];
  double lrho=rho[crop-1];
  //lrho=1.0;
  for (comp = 0; comp < Ncomp; ++comp) {
    for (d = 0; d < dim; ++d) {
      f1[comp*dim+d] = lrho*gradU[comp*dim+d];
    }
  }
}

#undef __FUNCT__
#define __FUNCT__ "g3_uu"
/* < \nabla v, \nabla u + {\nabla u}^T >
   This just gives \nabla u, give the perdiagonal for the transpose */
void g3_uu(const PetscScalar u[], const PetscScalar gradU[], const PetscReal x[], PetscScalar g3[], void * user)
{
  const PetscInt dim   = SPATIAL_DIM_0;
  const PetscInt Ncomp = NUM_BASIS_COMPONENTS_0;
  PetscInt       compI, d;
  double lrho;
  PetscInt crop;
  
  PetscFunctionBeginUser;
  crop=((AppCtx*)user)->tcrop[((AppCtx*)user)->elem];
  lrho=rho[crop-1];
//  lrho=1.0;
  for (compI = 0; compI < Ncomp; ++compI) {
    for (d = 0; d < dim; ++d) {
      g3[((compI*Ncomp+compI)*dim+d)*dim+d] = lrho;
    }
  }
}
/* < q, \nabla\cdot v >
   NcompI = 1, NcompJ = dim */


/*
  In 3D we use exact solution:

    u = x^2 + y^2 + z^2
    f = 6

  so that

    -\Delta u + f = -6 + 6 = 0
*/
void quadratic_u_3d(const PetscReal x[], PetscScalar *u)
{
  if (x[0]*x[1]<0.01)
    *u = x[0]*x[0] + x[1]*x[1] + x[2]*x[2];
  else
    *u=0;
}

#undef __FUNCT__
#define __FUNCT__ "ProcessOptions"
PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options)
{
  const char    *bcTypes[2]  = {"neumann", "dirichlet"};
  const char    *runTypes[2] = {"full", "test"};
  PetscInt       bc, run;
  PetscBool      flg;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  options->debug           = 0;
  options->runType         = RUN_FULL;
  options->dim             = 2;
  options->interpolate     = PETSC_FALSE;
  options->refinementLimit = 0.0;
  options->bcType          = DIRICHLET;
  options->numBatches      = 1;
  options->numBlocks       = 1;
  options->jacobianMF      = PETSC_FALSE;
  options->showInitial     = PETSC_FALSE;
  options->showSolution    = PETSC_FALSE;
  sprintf(options->filename,"");
  
  options->fem.quad    = (PetscQuadrature*) &options->q;
  options->fem.f0Funcs = (void (**)(const PetscScalar[], const PetscScalar[], const PetscReal[], PetscScalar[],void * )) &options->f0Funcs;
  options->fem.f1Funcs = (void (**)(const PetscScalar[], const PetscScalar[], const PetscReal[], PetscScalar[],void * )) &options->f1Funcs;
  options->fem.g0Funcs = (void (**)(const PetscScalar[], const PetscScalar[], const PetscReal[], PetscScalar[],void * )) &options->g0Funcs;
  options->fem.g1Funcs = (void (**)(const PetscScalar[], const PetscScalar[], const PetscReal[], PetscScalar[],void * )) &options->g1Funcs;
  options->fem.g2Funcs = (void (**)(const PetscScalar[], const PetscScalar[], const PetscReal[], PetscScalar[],void * )) &options->g2Funcs;
  options->fem.g3Funcs = (void (**)(const PetscScalar[], const PetscScalar[], const PetscReal[], PetscScalar[],void * )) &options->g3Funcs;
  options->fem.bcFuncs = (void (**)(const PetscReal[], PetscScalar *)) (&my_BCs);//&(options->exactFuncs);
  options->fem.quadBd    = (PetscQuadrature*) &options->qbd;
  options->fem.f0BdFuncs = (void (**)(const PetscScalar[], const PetscScalar[], const PetscReal[], const PetscReal[], PetscScalar[],void * )) &options->f0BdFuncs;
  options->fem.f1BdFuncs = (void (**)(const PetscScalar[], const PetscScalar[], const PetscReal[], const PetscReal[], PetscScalar[],void * )) &options->f1BdFuncs;

  ierr = MPI_Comm_size(comm, &options->numProcs);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &options->rank);CHKERRQ(ierr);
  ierr = PetscOptionsBegin(comm, "", "Poisson Problem Options", "DMPLEX");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-debug", "The debugging level", "tempo.c", options->debug, &options->debug, NULL);CHKERRQ(ierr);
  run  = options->runType;
  ierr = PetscOptionsEList("-run_type", "The run type", "tempo.c", runTypes, 2, runTypes[options->runType], &run, NULL);CHKERRQ(ierr);

  options->runType = (RunType) run;
 strcpy(options->filename,"");
  ierr = PetscOptionsInt("-dim", "The topological mesh dimension", "tempo.c", options->dim, &options->dim, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsString("-f", "Exodus.II filename to read", "tempo.c", options->filename, options->filename, sizeof(options->filename), &flg);CHKERRQ(ierr);
#if !defined(PETSC_HAVE_EXODUSII)
  if (flg) SETERRQ(comm, PETSC_ERR_ARG_WRONG, "This option requires ExodusII support. Reconfigure using --download-exodusii");
#endif
  ierr = PetscOptionsBool("-interpolate", "Generate intermediate mesh elements", "tempo.c", options->interpolate, &options->interpolate, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-refinement_limit", "The largest allowable cell volume", "tempo.c", options->refinementLimit, &options->refinementLimit, NULL);CHKERRQ(ierr);
  ierr = PetscStrcpy(options->partitioner, "chaco");CHKERRQ(ierr);
  ierr = PetscOptionsString("-partitioner", "The graph partitioner", "pflotran.cxx", options->partitioner, options->partitioner, 2048, NULL);CHKERRQ(ierr);
  bc   = options->bcType;
  ierr = PetscOptionsEList("-bc_type","Type of boundary condition","tempo.c",bcTypes,2,bcTypes[options->bcType],&bc,NULL);CHKERRQ(ierr);

  options->bcType = (BCType) bc;

  ierr = PetscOptionsInt("-gpu_batches", "The number of cell batches per kernel", "tempo.c", options->numBatches, &options->numBatches, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-gpu_blocks", "The number of concurrent blocks per kernel", "tempo.c", options->numBlocks, &options->numBlocks, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-jacobian_mf", "Calculate the action of the Jacobian on the fly", "tempo.c", options->jacobianMF, &options->jacobianMF, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-show_initial", "Output the initial guess for verification", "tempo.c", options->showInitial, &options->showInitial, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-show_solution", "Output the solution for verification", "tempo.c", options->showSolution, &options->showSolution, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();

  ierr = PetscLogEventRegister("CreateMesh", DM_CLASSID, &options->createMeshEvent);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMVecViewLocal"
PetscErrorCode DMVecViewLocal(DM dm, Vec v, PetscViewer viewer)
{
  Vec            lv;
  PetscInt       p;
  PetscMPIInt    rank, numProcs;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)dm), &rank);CHKERRQ(ierr);
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm), &numProcs);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm, &lv);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm, v, INSERT_VALUES, lv);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm, v, INSERT_VALUES, lv);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Local function\n");CHKERRQ(ierr);
  for (p = 0; p < numProcs; ++p) {
    if (p == rank) {ierr = VecView(lv, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);}
    ierr = PetscBarrier((PetscObject) dm);CHKERRQ(ierr);
  }
  ierr = DMRestoreLocalVector(dm, &lv);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateMesh"
PetscErrorCode CreateMesh(MPI_Comm comm, AppCtx *user, DM *dm)
{
  PetscInt       dim             = user->dim;
  const char    *filename        = user->filename;
  PetscBool      interpolate     = user->interpolate;
  PetscReal      refinementLimit = user->refinementLimit;
  const char    *partitioner     = user->partitioner;
  size_t         len;
  PetscErrorCode ierr;
  PetscMPIInt    rank;
  
  PetscFunctionBeginUser;
  ierr = MPI_Comm_rank(comm, &rank);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(user->createMeshEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = PetscStrlen(filename, &len);CHKERRQ(ierr);
  if (!len) {
     DMLabel label;
     int i;
     if (0){
       ierr = DMPlexCreateBoxMesh(comm, dim, interpolate, dm);CHKERRQ(ierr);
     }else{
    //DMPlexCreateFromCellList(MPI_Comm comm, PetscInt dim, PetscInt numCells, PetscInt numVertices, PetscInt numCorners, PetscBool interpolate, const int cells[], PetscInt spaceDim, const double vertexCoords[], DM *dm)
    
    //  ierr = DMPlexCreateLabel(*dm, "marker");CHKERRQ(ierr);
//    ierr = DMPlexGetLabel(*dm, "marker", &label);CHKERRQ(ierr);
       if (!rank){
         ierr = DMPlexCreateFromCellList(comm,dim,obNbCells,obNbVertex,3,0,obCells,2,obVertex,dm);CHKERRQ(ierr);
         for (i=0;i<obNbBound;i++){
           ierr =DMPlexSetLabelValue(*dm, "marker", obBoundary[i]+obNbCells, 1);CHKERRQ(ierr);
         }
//         DMPlexCreateLabel(*dm, "crop");
//         DMPlexGetLabel(*dm, "marker", &slabel[rank]);
         for (i=0;i<obNbCells;i++){
           //DMLabelSetValue(slabel[rank], i, labelCells[i]+1);
           ierr =DMPlexSetLabelValue(*dm, "crop", i, labelCells[i]+1);CHKERRQ(ierr);
           /* if (labelCells[i]==1){ */
           /*   ierr =DMPlexSetLabelValue(*dm, "marker1", i, 1);CHKERRQ(ierr); */
           /* }else{ */
           /*   ierr =DMPlexSetLabelValue(*dm, "marker2", i, 1);CHKERRQ(ierr); */
           /* } */
         }
       }else {
          ierr = DMPlexCreateFromCellList(comm,dim,0,0,3,0,obCells,2,obVertex,dm);CHKERRQ(ierr);
         /* printf("begin section rank\n"); */
         /* ierr = DMCreate(comm, dm);CHKERRQ(ierr); */
         /* PetscValidLogicalCollectiveInt(dm,dim,2); */
         /* ierr = DMSetType(*dm, DMPLEX);CHKERRQ(ierr); */
         /* ierr = DMPlexSetDimension(*dm, dim);CHKERRQ(ierr); */
         /* printf("end section rank\n"); */
       }
         
     }
    
  } else {
#if defined(PETSC_HAVE_EXODUSII)
    /* int        CPU_word_size = 0, IO_word_size = 0, exoid; */
    /* float       version; */
    /* PetscMPIInt rank; */

    /* ierr = MPI_Comm_rank(comm, &rank);CHKERRQ(ierr); */
    /* if (!rank) { */
    /*   exoid = ex_open(filename, EX_READ, &CPU_word_size, &IO_word_size, &version); */
    /*   if (exoid <= 0) SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_LIB, "ex_open(\"%s\",...) did not return a valid file ID", filename); */
    /* } else exoid = -1;                 /\* Not used *\/ */
    /* ierr = DMPlexCreateExodus(comm, exoid, interpolate, dm);CHKERRQ(ierr); */
    /* ierr = DMPlexSetRefinementUniform(*dm, PETSC_FALSE);CHKERRQ(ierr); */
    /* if (!rank) {ierr = ex_close(exoid);CHKERRQ(ierr);} */
    /* Must have boundary marker for Dirichlet conditions */
#endif
  }
  {
    DM refinedMesh     = NULL;
    DM distributedMesh = NULL;

    /* Refine mesh using a volume constraint */
    /*ierr = DMPlexSetRefinementLimit(*dm, refinementLimit);CHKERRQ(ierr);
    ierr = DMRefine(*dm, comm, &refinedMesh);CHKERRQ(ierr);
    if (refinedMesh) {
      ierr = DMDestroy(dm);CHKERRQ(ierr);
      *dm  = refinedMesh;
      }*/
    /* Distribute mesh over processes */
    ierr = DMPlexDistribute(*dm, partitioner, 0, &distributedMesh);CHKERRQ(ierr);
    if (distributedMesh) {
      ierr = DMDestroy(dm);CHKERRQ(ierr);
      *dm  = distributedMesh;
    }

//build tcrop
    {
       DMLabel         label;
       PetscBool      has;
       PetscInt p,i,size,sumSize;
       ierr = DMPlexHasLabel(*dm, "crop", &has);CHKERRQ(ierr);
       sumSize=0;
       if (has){
         DMPlexGetLabel(*dm, "crop", &label);
         for (i=1; i <11;i++){
           DMLabelGetStratumSize(label,i,&size);
           sumSize+=size;
         }
         //tcrop alloc:
         user->tcrop = (PetscInt * ) calloc(sumSize,sizeof(PetscInt));
         printf("calloc of %i\n",sumSize);
         for (i=1; i< 11;i++){
           PetscInt size;
           ierr =DMLabelGetStratumSize(label,i,&size);CHKERRQ(ierr);
           if (size>0){
             IS ispoints;
             const PetscInt * points;
             ierr =DMLabelGetStratumIS( label, i, &ispoints);CHKERRQ(ierr);
             ierr =ISGetIndices(ispoints, &points);CHKERRQ(ierr);
             for (p=0;p<size;p++){
               (user->tcrop)[points[p]]=i;
             }
             ierr =ISDestroy(&ispoints);CHKERRQ(ierr);
           }else{
             printf("no label %i\n",i);
           }
         } 
       }
    }
    
  }
  ierr     = DMSetFromOptions(*dm);CHKERRQ(ierr);
  ierr     = PetscLogEventEnd(user->createMeshEvent,0,0,0,0);CHKERRQ(ierr);
  user->dm = *dm;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SetupQuadrature"
PetscErrorCode SetupQuadrature(AppCtx *user)
{
  PetscFunctionBeginUser;
  user->fem.quad[0].numQuadPoints = NUM_QUADRATURE_POINTS_0;
  user->fem.quad[0].quadPoints    = points_0;
  user->fem.quad[0].quadWeights   = weights_0;
  user->fem.quad[0].numBasisFuncs = NUM_BASIS_FUNCTIONS_0;
  user->fem.quad[0].numComponents = NUM_BASIS_COMPONENTS_0;
  user->fem.quad[0].basis         = Basis_0;
  user->fem.quad[0].basisDer      = BasisDerivatives_0;
  user->fem.quadBd[0].numQuadPoints = NUM_QUADRATURE_POINTS_0_BD;
  user->fem.quadBd[0].quadPoints    = points_0_BD;
  user->fem.quadBd[0].quadWeights   = weights_0_BD;
  user->fem.quadBd[0].numBasisFuncs = NUM_BASIS_FUNCTIONS_0_BD;
  user->fem.quadBd[0].numComponents = NUM_BASIS_COMPONENTS_0_BD;
  user->fem.quadBd[0].basis         = Basis_0_BD;
  user->fem.quadBd[0].basisDer      = BasisDerivatives_0_BD;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SetupSection"
/*
  There is a problem here with uninterpolated meshes. The index in numDof[] is not dimension in this case,
  but sieve depth.
*/
PetscErrorCode SetupSection(DM dm, AppCtx *user)
{
  PetscSection   section;
  const PetscInt numFields           = NUM_FIELDS;
  PetscInt       dim                 = user->dim;
  const char    *bdLabel             = user->bcType == NEUMANN ? "boundary" : "marker";
  PetscInt       numBC               = 0;
  PetscInt       numComp[NUM_FIELDS] = {NUM_BASIS_COMPONENTS_0};
  PetscInt       bcFields[1]         = {0};
  IS             bcPoints[1]         = {NULL};
  PetscInt       numDof[NUM_FIELDS*(SPATIAL_DIM_0+1)];
  PetscInt       f, d;
  PetscBool      has;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  if (dim != SPATIAL_DIM_0) SETERRQ2(PetscObjectComm((PetscObject)dm), PETSC_ERR_ARG_SIZ, "Spatial dimension %d should be %d", dim, SPATIAL_DIM_0);
  for (d = 0; d <= dim; ++d) {
    numDof[0*(dim+1)+d] = numDof_0[d];
  }
  for (f = 0; f < numFields; ++f) {
    for (d = 1; d < dim; ++d) {
      if ((numDof[f*(dim+1)+d] > 0) && !user->interpolate) SETERRQ(PetscObjectComm((PetscObject)dm), PETSC_ERR_ARG_WRONG, "Mesh must be interpolated when unknowns are specified on edges or faces.");
    }
  }
  ierr = DMPlexHasLabel(dm, bdLabel, &has);CHKERRQ(ierr);
  if (!has) {
    DMLabel label;

    ierr = DMPlexCreateLabel(dm, bdLabel);CHKERRQ(ierr);
    ierr = DMPlexGetLabel(dm, bdLabel, &label);CHKERRQ(ierr);
    ierr = DMPlexMarkBoundaryFaces(dm, label);CHKERRQ(ierr);
    if (user->bcType == DIRICHLET) {
      ierr  = DMPlexLabelComplete(dm, label);CHKERRQ(ierr);
    }
  }
  if (user->bcType == DIRICHLET) {
    numBC = 1;
    ierr  = DMPlexGetStratumIS(dm, bdLabel, 1, &bcPoints[0]);CHKERRQ(ierr);
  }
  ierr = DMPlexCreateSection(dm, dim, numFields, numComp, numDof, numBC, bcFields, bcPoints, &section);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName(section, 0, "potential");CHKERRQ(ierr);
  ierr = DMSetDefaultSection(dm, section);CHKERRQ(ierr);
  if (user->bcType == DIRICHLET) {
    ierr = ISDestroy(&bcPoints[0]);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SetupExactSolution"
PetscErrorCode SetupExactSolution(DM dm, AppCtx *user)
{
  OB_PetscFEM       *fem = &user->fem;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  fem->f0Funcs[0] = f0_u;
  fem->f1Funcs[0] = f1_u;
  fem->g0Funcs[0] = g0_u;
  fem->g1Funcs[0] = NULL;
  fem->g2Funcs[0] = NULL;
  fem->g3Funcs[0] = g3_uu;      /* < \nabla v, \nabla u > */
  fem->f0BdFuncs[0] = f0_bd_zero;
  fem->f1BdFuncs[0] = f1_bd_zero;
  my_BCs[0] = my_BC;
  switch (user->dim) {
  case 2:
    user->exactFuncs[0] = NULL;
    if (user->bcType == NEUMANN) {
      fem->f0BdFuncs[0] = f0_bd_u;
    }
    break;
  case 3:
    user->exactFuncs[0] = quadratic_u_3d;
    break;
  default:
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_OUTOFRANGE, "Invalid dimension %d", user->dim);
  }
  //ierr = DMPlexSetFEMIntegration(dm, FEMIntegrateResidualBatch, FEMIntegrateBdResidualBatch, FEMIntegrateJacobianActionBatch, FEMIntegrateJacobianBatch);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormJacobianAction"
/*
  FormJacobianAction - Form the global Jacobian action Y = JX from the global input X

  Input Parameters:
+ mat - The Jacobian shell matrix
- X  - Global input vector

  Output Parameter:
. Y  - Local output vector

  Note:
  We form the residual one batch of elements at a time. This allows us to offload work onto an accelerator,
  like a GPU, or vectorize on a multicore machine.

.seealso: FormJacobianActionLocal()
*/
PetscErrorCode FormJacobianAction(Mat J, Vec X,  Vec Y)
{
  JacActionCtx   *ctx;
  DM             dm;
  Vec            localX, localY;
  PetscInt       N, n;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
#if 0
  /* Needs petscimpl.h */
  PetscValidHeaderSpecific(J, MAT_CLASSID, 1);
  PetscValidHeaderSpecific(X, VEC_CLASSID, 2);
  PetscValidHeaderSpecific(Y, VEC_CLASSID, 3);
#endif
  ierr = MatShellGetContext(J, &ctx);CHKERRQ(ierr);
  dm   = ctx->dm;

  /* determine whether X = localX */
  ierr = DMGetLocalVector(dm, &localX);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm, &localY);CHKERRQ(ierr);
  ierr = VecGetSize(X, &N);CHKERRQ(ierr);
  ierr = VecGetSize(localX, &n);CHKERRQ(ierr);

  if (n != N) { /* X != localX */
    ierr = VecSet(localX, 0.0);CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(dm, X, INSERT_VALUES, localX);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(dm, X, INSERT_VALUES, localX);CHKERRQ(ierr);
  } else {
    ierr   = DMRestoreLocalVector(dm, &localX);CHKERRQ(ierr);
    localX = X;
  }
  ierr = DMPlexComputeJacobianActionFEM(dm, J, localX, localY, ctx->user);CHKERRQ(ierr);
  if (n != N) {
    ierr = DMRestoreLocalVector(dm, &localX);CHKERRQ(ierr);
  }
  ierr = VecSet(Y, 0.0);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dm, localY, ADD_VALUES, Y);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dm, localY, ADD_VALUES, Y);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm, &localY);CHKERRQ(ierr);
  if (0) {
    Vec       r;
    PetscReal norm;

    ierr = VecDuplicate(X, &r);CHKERRQ(ierr);
    ierr = MatMult(ctx->J, X, r);CHKERRQ(ierr);
    ierr = VecAXPY(r, -1.0, Y);CHKERRQ(ierr);
    ierr = VecNorm(r, NORM_2, &norm);CHKERRQ(ierr);
    if (norm > 1.0e-8) {
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Jacobian Action Input:\n");CHKERRQ(ierr);
      ierr = VecView(X, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Jacobian Action Result:\n");CHKERRQ(ierr);
      ierr = VecView(Y, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Difference:\n");CHKERRQ(ierr);
      ierr = VecView(r, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
      SETERRQ1(PetscObjectComm((PetscObject)J), PETSC_ERR_ARG_WRONG, "The difference with assembled multiply is too large %g", norm);
    }
    ierr = VecDestroy(&r);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
{
  SNES           snes;                 /* nonlinear solver */
  Vec            u,r;                  /* solution, residual vectors */
  Mat            A,J;                  /* Jacobian matrix */
  MatNullSpace   nullSpace;            /* May be necessary for Neumann conditions */
  AppCtx         user;                 /* user-defined work context */
  JacActionCtx   userJ;                /* context for Jacobian MF action */
  PetscInt       its;                  /* iterations for convergence */
  PetscReal      error         = 0.0;  /* L_2 error in the solution */
  const PetscInt numComponents = NUM_BASIS_COMPONENTS_TOTAL;
  PetscErrorCode ierr;
  int nbStep;
  char solFile[256];
  
  ierr = PetscInitialize(&argc, &argv, NULL, help);CHKERRQ(ierr);
  ierr = ProcessOptions(PETSC_COMM_WORLD, &user);CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD, &snes);CHKERRQ(ierr);
  ierr = CreateMesh(PETSC_COMM_WORLD, &user, &user.dm);CHKERRQ(ierr);
  ierr = SNESSetDM(snes, user.dm);CHKERRQ(ierr);

  ierr = SetupExactSolution(user.dm, &user);CHKERRQ(ierr);
  ierr = SetupQuadrature(&user);CHKERRQ(ierr);
  ierr = SetupSection(user.dm, &user);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(user.dm, &u);CHKERRQ(ierr);
  ierr = VecDuplicate(u, &r);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(user.dm, &(user.utm1));CHKERRQ(ierr);

  ierr = DMCreateMatrix(user.dm, MATAIJ, &J);CHKERRQ(ierr);

  A = J;

  ierr = DMSNESSetFunctionLocal(user.dm,  (PetscErrorCode (*)(DM,Vec,Vec,void*))OB_DMPlexComputeResidualFEM,&user);CHKERRQ(ierr);
  ierr = DMSNESSetJacobianLocal(user.dm,  (PetscErrorCode (*)(DM,Vec,Mat,Mat,MatStructure*,void*))OB_DMPlexComputeJacobianFEM,&user);CHKERRQ(ierr);
  ierr = SNESSetJacobian(snes, A, J, NULL, NULL);CHKERRQ(ierr);

  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);

  ierr = DMPlexProjectFunction(user.dm, numComponents, my_BCs, INSERT_ALL_VALUES, u);CHKERRQ(ierr);
  ierr = DMPlexProjectFunction(user.dm, numComponents, my_BCs, INSERT_ALL_VALUES, user.utm1);CHKERRQ(ierr);
  ierr =VecAssemblyBegin(user.utm1);CHKERRQ(ierr);
  ierr =VecAssemblyEnd(user.utm1);CHKERRQ(ierr);
  //ierr = VecView(u, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = VecView(user.utm1, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = DMVecViewLocal(user.dm, user.utm1, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
  if (user.showInitial) {ierr = DMVecViewLocal(user.dm, u, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);}
    PetscScalar (*initialGuess[numComponents])(const PetscReal x[]); 
    PetscInt c;
//    for (c = 0; c < numComponents; ++c) initialGuess[c] = zero;
//    ierr = DMPlexProjectFunction(user.dm, numComponents, initialGuess, INSERT_VALUES, u);CHKERRQ(ierr);
    //ierr = VecView(u, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    if (user.showInitial) {ierr = DMVecViewLocal(user.dm, u, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);}
    if (user.debug) {
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Initial guess\n");CHKERRQ(ierr);
      ierr = VecView(u, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    }

    for (nbStep=0;nbStep<1;nbStep++){
      printf("VecView before solve\n");
      ierr = VecView(u, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
      ierr = VecView(user.utm1, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
      ierr = DMVecViewLocal(user.dm,u, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
      ierr = DMVecViewLocal(user.dm, user.utm1, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
     //printf("VecViewLocal before solve\n");
      //ierr = DMVecViewLocal(user.dm, u, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
      //ierr = DMVecViewLocal(user.dm, user.utm1, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
      printf("solving:\n");
      ierr = SNESSolve(snes, NULL, u);CHKERRQ(ierr);
      //printf("u sol:\n");
      //ierr = DMVecViewLocal(user.dm, u, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
      ierr = SNESGetIterationNumber(snes, &its);CHKERRQ(ierr);
      ierr = PetscPrintf(PETSC_COMM_WORLD, "Number of SNES iterations = %D\n", its);CHKERRQ(ierr);
//    ierr = DMPlexComputeL2Diff(user.dm, user.fem.quad, user.exactFuncs, u, &error);CHKERRQ(ierr);
      Vec localX;
      ierr = DMGetLocalVector(user.dm, &localX);CHKERRQ(ierr);
    
      ierr = DMPlexProjectFunctionLocal(user.dm, 1, my_BCs, INSERT_BC_VALUES, localX);CHKERRQ(ierr);
      ;
      ierr = DMGlobalToLocalBegin(user.dm, u, INSERT_VALUES, localX);CHKERRQ(ierr);
      ierr = DMGlobalToLocalEnd(user.dm, u, INSERT_VALUES, localX);CHKERRQ(ierr);
      printf("after BC:\n");
      ierr = DMVecViewLocal(user.dm, localX, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
//    ierr = PetscPrintf(PETSC_COMM_WORLD, "L_2 Error: %.3g\n", error);CHKERRQ(ierr);
      if (user.showSolution) {
        ierr = PetscPrintf(PETSC_COMM_WORLD, "Solution\n");CHKERRQ(ierr);
        //ierr = VecChop(u, 3.0e-9);CHKERRQ(ierr);
        ierr = VecView(u, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
        ierr = DMVecViewLocal(user.dm, u, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
      }
  

      PetscViewer viewer;
      Vec         uLocal;

      ierr = PetscViewerCreate(PETSC_COMM_WORLD, &viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetType(viewer, PETSCVIEWERVTK);CHKERRQ(ierr);
      ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
      sprintf(solFile,"tempo_sol_%i.vtk",nbStep);
      ierr = PetscViewerFileSetName(viewer, solFile);CHKERRQ(ierr);

      ierr = DMGetLocalVector(user.dm, &uLocal);CHKERRQ(ierr);
      ierr = DMGlobalToLocalBegin(user.dm, u, INSERT_VALUES, uLocal);CHKERRQ(ierr);
      ierr = DMGlobalToLocalEnd(user.dm, u, INSERT_VALUES, uLocal);CHKERRQ(ierr);
      
      ierr = PetscObjectReference((PetscObject) user.dm);CHKERRQ(ierr); /* Needed because viewer destroys the DM */
      ierr = PetscObjectReference((PetscObject) uLocal);CHKERRQ(ierr); /* Needed because viewer destroys the Vec */
      ierr = PetscViewerVTKAddField(viewer, (PetscObject) user.dm, DMPlexVTKWriteAll, PETSC_VTK_POINT_FIELD, (PetscObject) uLocal);CHKERRQ(ierr);
      ierr = DMRestoreLocalVector(user.dm, &uLocal);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
//      ierr = VecCopy(u,user.utm1);CHKERRQ(ierr);
//      ierr = VecZeroEntries(u);CHKERRQ(ierr);
    }

  if (user.bcType == NEUMANN) {
    ierr = MatNullSpaceDestroy(&nullSpace);CHKERRQ(ierr);
  }
  if (user.jacobianMF) {
    ierr = VecDestroy(&userJ.u);CHKERRQ(ierr);
  }
  if (A != J) {
    ierr = MatDestroy(&A);CHKERRQ(ierr);
  }
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&(user.utm1));CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = DMDestroy(&user.dm);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return 0;
}
