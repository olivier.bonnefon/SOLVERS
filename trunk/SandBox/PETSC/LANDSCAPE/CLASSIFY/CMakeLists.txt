CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/" ${CMAKE_MODULE_PATH})

FIND_PACKAGE(PETSc)
include_directories(${PETSC_INCLUDES})
ADD_EXECUTABLE(classify ex12.c)


TARGET_LINK_LIBRARIES(classify ${PETSC_LIBRARIES})
