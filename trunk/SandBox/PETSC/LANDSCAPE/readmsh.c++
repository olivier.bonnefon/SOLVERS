#include <iostream>
#include <fstream>
#include <stdlib.h>




using namespace std;

int main () {
  ifstream myfile;
  string line;
  float x,y,z;
  myfile.open ("mesh.msh");
  if (myfile.is_open())
  {
    getline (myfile,line);
    getline (myfile,line) ;
    getline (myfile,line) ;
    getline (myfile,line) ;
    getline (myfile,line) ;
    char* chr = (char *)line.c_str();
    //printf("%s\n",chr);
    int Nnodes;
    int node;
    float * coords=(float*) malloc(2*Nnodes*sizeof(float));
    sscanf(chr,"%i",&Nnodes);
    printf("int obNbVertex=%i;\n",Nnodes);
    printf("double obVertex[%i]={\n",2*Nnodes);
    for (int i =0;i<Nnodes;i++){
      getline (myfile,line) ;
      chr = (char *)line.c_str();
      sscanf(chr,"%i%e%e%e",&node,&coords[2*i],&coords[2*i+1],&z);
      coords[2*i]=coords[2*i]/100.0;
      coords[2*i+1]=coords[2*i+1]/100.0;
      printf("%e,%e",coords[2*i],coords[2*i+1]);
      if (i<Nnodes-1)
        printf(",\n");
      else
        printf("};\n");
    }

    getline (myfile,line);
    getline (myfile,line);
    getline (myfile,line);
    
    int NElems;
    int elem;
    int n1,n2,bidon;
    chr = (char *)line.c_str();
    sscanf(chr,"%i",&NElems);
    int type=0;
    int cmp=0;
    printf("int obNbBound=;\nint obBoundary[]={\n");
    while(1){
      getline (myfile,line) ;
      chr = (char *)line.c_str();
      sscanf(chr,"%i%i",&elem,&type);
      if (type==1){
        sscanf(chr,"%i%i%i%i%i%i%i",&bidon,&bidon,&bidon,&bidon,&bidon,&n1,&n2);
        printf("%i,\n",n2-1);
        cmp++;
      }
      if (type == 2)
        break;
      NElems--;
    }
    printf("\n%i\n",cmp);

    printf("int obNbCells=%i;\nint obCells[%i]={\n",NElems,3*NElems);
    for (int i =0;i<NElems;i++){
      int ii,jj,kk;
      
      chr = (char *)line.c_str();
      sscanf(chr,"%i%i%i%i%i%i%i%i",&bidon,&bidon,&bidon,&bidon,&bidon,&ii,&jj,&kk);
      ii=ii-1;
      jj=jj-1;
      kk=kk-1;
      float x0=coords[2*jj]-coords[2*ii];
      float y0=coords[2*jj+1]-coords[2*ii+1];
      float x1=coords[2*jj]-coords[2*kk];
      float y1=coords[2*jj+1]-coords[2*kk+1];
      if (x0*y1-x1*y0<0.0001)
        printf("WARNING nul det\n");
      if ((x0*y1-x1*y0)<0)
        printf("%i,%i,%i",ii,jj,kk);
      else
        printf("%i,%i,%i",ii,kk,jj);
      if (i<NElems-1)
        printf(",\n");
      else
        printf("};\n");
      getline (myfile,line) ;
    }
    printf("int labelCells[%i]={\n",NElems);
    for (int i =0;i<NElems;i++){
      printf("%i",1);
       if (i<NElems-1)
        printf(",");
      else
        printf("};\n");
    }
    myfile.close();
  }
  else cout << "Unable to open file"; 
}
