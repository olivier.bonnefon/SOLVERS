
#include <stdio.h>

#define NX 4
#define NY 4

#define NB_CROP 10

#define N_MAX_LENGTH_CROP 4
#define N_MIN_LENGTH_CROP 2



double XX[NX];
double YY[NY];
//double XX[NX]={0,0.5,1};
//double YY[NY]={0,0.5,1};



int main(int argc, char **argv)
{
  int i,j;
  int cur=0;
  int obNbBound=2*NX+2*(NY-2);
  int NX_SIZE,NY_SIZE;
  int LABEL=0;
  double stepX=1.0/(NX-1);
  double stepY=1.0/(NY-1);
  for (i=0; i < NX; i++){
    XX[i]=i*stepX;
  }
  for (i=0; i < NY; i++){
    YY[i]=i*stepY;
  }
  printf("int obNbBound=%i;\n",obNbBound);
  printf("int obBoundary[%i]={\n\t",obNbBound);
  cur=0;
  for (i=0;i<NX;i++){
    printf("%i,\n\t",cur);
    cur++;
  }
  for (i=0;i<NY-2;i++){
    printf("%i,\n\t",cur);
    cur+=NX-1;
    printf("%i,\n\t",cur);
    cur++;
  }
  for (i=0;i<NX-1;i++){
    printf("%i,\n\t",cur);
    cur++;
  }
  printf("%i};\n",cur);

  int nbCells=2*(NX-1)*(NY-1);
  printf("int obNbCells=%i;\n",nbCells);
  printf("int obCells[%i]={\n\t",3*nbCells);
  for (i=0;i<NX-1;i++){
    for (j=0;j<NY-1;j++){
      printf("%i,%i,%i,\n\t",j+NY*i,j+1+NY*i,j+NY*(i+1));
      printf("%i,%i,%i",j+1+NY*i,j+1+NY*(i+1),j+NY*(i+1));
      if (i!=NX-2 || j!= NY-2)
        printf(",\n\t");
      else
        printf("};\n");
        
    }
  }
  int labelCells[2*(NX-1)*(NY-1)];
  int X_SLIDE[NX];
  int NB_X_SLIDE=0;
  int LastX=0;
  while (LastX < NX-1){
    NX_SIZE=rand()%(N_MAX_LENGTH_CROP-N_MIN_LENGTH_CROP)+N_MIN_LENGTH_CROP;
    X_SLIDE[NB_X_SLIDE]=NX_SIZE;
    LastX+=NX_SIZE;
    NB_X_SLIDE++;
  }
    
 

  int curX=0;
  int numXslide=0;
  NY_SIZE=0;
  for (numXslide=0;numXslide<NB_X_SLIDE;numXslide++){
    for (j=0;j<NY-1;j++){
      if (NY_SIZE==0){
        NY_SIZE=rand()%(N_MAX_LENGTH_CROP-N_MIN_LENGTH_CROP)+N_MIN_LENGTH_CROP;
        LABEL=rand()%NB_CROP;
      }else{
        NY_SIZE--;
      }
      for(i=curX;(i<curX+X_SLIDE[numXslide]) && (i < NX-1);i++){
        labelCells[2*j*(NY-1)+2*i]=LABEL;
        labelCells[2*j*(NY-1)+2*i+1]=LABEL;
      }
      //printf("\nWARNING curX= %i\n",i);
    }
    curX+=X_SLIDE[numXslide];
  }
  
  printf("int labelCells[%i]={",nbCells);
  for(i=0;i<2*(NX-1)*(NY-1);i++){
    printf("%i",labelCells[i]);
    if (labelCells[i] > NB_CROP || labelCells[i]<0)
      printf("\nWARNING %i %i %i\n",i/(2*(NY-1)),(i/2)%((NX-1)),labelCells[i]);
    if (i!=2*(NX-1)*(NY-1)-1)
      printf(",\t");
    else
      printf("};\n");
  }
  printf("\n");
  int nbVertex=NX*NY;
  printf("int obNbVertex=%i;\n",nbVertex);
  printf("double obVertex[%i]={\n\t",2*nbVertex);
   for (i=0;i<NX;i++){
    for (j=0;j<NY;j++){
      printf("%e,%e",XX[j],YY[i]);
      if (i!=NX-1 || j!= NY-1){
        printf(",\n\t");
      }else
        printf("};\n");
    }
  }
 
  
  return 0;
}
