#include <stdlib.h>

#define NUM_QUADRATURE_POINTS_0_BD 1

/* Quadrature points
   - (x1,y1,x2,y2,...) */
static PetscReal points_0_BD[1] = {0.0};

/* Quadrature weights
   - (v1,v2,...) */
static PetscReal weights_0_BD[1] = {2.0};

#define SPATIAL_DIM_0_BD 1

#define NUM_BASIS_FUNCTIONS_0_BD 2

#define NUM_BASIS_COMPONENTS_0_BD 1

/* Number of degrees of freedom for each dimension */
static int numDof_0_BD[2] = {
  1,
  0};

/* Nodal basis function evaluations
    - basis component is fastest varying, then basis function, then quad point */
static PetscReal Basis_0_BD[2] = {
  0.5,
  0.5};

/* Nodal basis function derivative evaluations,
    - basis component is fastest varying, then derivative direction, then basis function, then quad point */
static PetscReal BasisDerivatives_0_BD[2] = {
  -0.5,
  0.5};
