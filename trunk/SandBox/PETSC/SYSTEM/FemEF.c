#include "FemEF.h"
#include "FemSystem.h"
#include "plugin.h"


#undef __FUNCT__
#define __FUNCT__ "__affectInt"
PetscErrorCode __affectInt(PetscInt * pInt,int numargs, ...) {
    int     i = 0;
    va_list ap;
    int rank;
    PetscErrorCode ierr=0;
    PetscFunctionBeginUser;
    ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
    va_start(ap, numargs);
    while (numargs--){
      pInt[i]=va_arg(ap, int);
//      PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]affected pInt[%d]=%d\n",rank,i,pInt[i]);
      i++;
    }
    va_end(ap);
    return ierr;
}

#undef __FUNCT__
#define __FUNCT__ "__affectScalar"
PetscErrorCode __affectScalar(PetscScalar * pScalard,int numargs, ...) {
    int     i = 0;
    va_list ap;
    int rank;
    PetscErrorCode ierr=0;
    PetscFunctionBeginUser;
    va_start(ap, numargs);
    while (numargs--){
      pScalard[i]=va_arg(ap, PetscScalar);
//      PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]affected pInt[%d]=%d\n",rank,i,pInt[i]);
      i++;
    }
    va_end(ap);
    return ierr;
}

#undef __FUNCT__
#define __FUNCT__ "ProcessOptions"
PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options)
{
  const char    *bcTypes[3]  = {"neumann", "dirichlet", "none"};
  const char    *runTypes[3] = {"full", "test", "perf"};
  const char    *coeffTypes[4] = {"none", "analytic", "field", "nonlinear"};
  PetscInt       bc, run, coeff;
  PetscBool      flg;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  options->debug               = 0;
  options->runType             = RUN_FULL;
  options->dim                 = 2;
  options->filename[0]         = '\0';
  options->interpolate         = PETSC_FALSE;
  options->refinementLimit     = 0.0;
  options->refinementUniform   = PETSC_FALSE;
  options->refinementRounds    = 1;
  options->bcType              = DIRICHLET;
  options->variableCoefficient = COEFF_NONE;
  options->jacobianMF          = PETSC_FALSE;
  options->showInitial         = PETSC_FALSE;
  options->showSolution        = PETSC_FALSE;
  options->restart             = PETSC_FALSE;
  options->check               = PETSC_FALSE;
  options->checkpoint          = NULL;

  ierr = PetscOptionsBegin(comm, "", "Poisson Problem Options", "DMPLEX");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-debug", "The debugging level", "ex12.c", options->debug, &options->debug, NULL);CHKERRQ(ierr);
  run  = options->runType;
  ierr = PetscOptionsEList("-run_type", "The run type", "ex12.c", runTypes, 3, runTypes[options->runType], &run, NULL);CHKERRQ(ierr);

  options->runType = (RunType) run;

  ierr = PetscOptionsInt("-dim", "The topological mesh dimension", "ex12.c", options->dim, &options->dim, NULL);CHKERRQ(ierr);
//  spatialDim = options->dim;
  ierr = PetscOptionsString("-f", "Exodus.II filename to read", "ex12.c", options->filename, options->filename, sizeof(options->filename), &flg);CHKERRQ(ierr);
#if !defined(PETSC_HAVE_EXODUSII)
  if (flg) SETERRQ(comm, PETSC_ERR_ARG_WRONG, "This option requires ExodusII support. Reconfigure using --download-exodusii");
#endif
  ierr = PetscOptionsBool("-interpolate", "Generate intermediate mesh elements", "ex12.c", options->interpolate, &options->interpolate, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-refinement_limit", "The largest allowable cell volume", "ex12.c", options->refinementLimit, &options->refinementLimit, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-refinement_uniform", "Uniformly refine the mesh", "ex52.c", options->refinementUniform, &options->refinementUniform, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-refinement_rounds", "The number of uniform refinements", "ex52.c", options->refinementRounds, &options->refinementRounds, NULL);CHKERRQ(ierr);
  ierr = PetscStrcpy(options->partitioner, "chaco");CHKERRQ(ierr);
  ierr = PetscOptionsString("-partitioner", "The graph partitioner", "pflotran.cxx", options->partitioner, options->partitioner, 2048, NULL);CHKERRQ(ierr);
  bc   = options->bcType;
  ierr = PetscOptionsEList("-bc_type","Type of boundary condition","ex12.c",bcTypes,3,bcTypes[options->bcType],&bc,NULL);CHKERRQ(ierr);
  options->bcType = (BCType) bc;
  coeff = options->variableCoefficient;
  ierr = PetscOptionsEList("-variable_coefficient","Type of variable coefficent","ex12.c",coeffTypes,4,coeffTypes[options->variableCoefficient],&coeff,NULL);CHKERRQ(ierr);
  options->variableCoefficient = (CoeffType) coeff;

  ierr = PetscOptionsBool("-jacobian_mf", "Calculate the action of the Jacobian on the fly", "ex12.c", options->jacobianMF, &options->jacobianMF, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-show_initial", "Output the initial guess for verification", "ex12.c", options->showInitial, &options->showInitial, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-show_solution", "Output the solution for verification", "ex12.c", options->showSolution, &options->showSolution, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-restart", "Read in the mesh and solution from a file", "ex12.c", options->restart, &options->restart, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-check", "Compare with default integration routines", "ex12.c", options->check, &options->check, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();

  ierr = PetscLogEventRegister("CreateMesh", DM_CLASSID, &options->createMeshEvent);CHKERRQ(ierr);

  if (options->restart) {
    ierr = PetscViewerCreate(comm, &options->checkpoint);CHKERRQ(ierr);
    ierr = PetscViewerSetType(options->checkpoint, PETSCVIEWERHDF5);CHKERRQ(ierr);
    ierr = PetscViewerFileSetMode(options->checkpoint, FILE_MODE_READ);CHKERRQ(ierr);
    ierr = PetscViewerFileSetName(options->checkpoint, options->filename);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "FemSystemPrintFemEF"
PetscErrorCode FemSystemPrintFemEF(FemEF* pFEF){
  int rank,i,j;
  PetscErrorCode ierr=0;
  PetscFunctionBeginUser;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]FemSystemPrintFemMenberEF \n",rank);CHKERRQ(ierr);
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]FemSystemPrintFemMenberEF ->user.dim=%d\n",rank,pFEF->user.dim);CHKERRQ(ierr);
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]FemSystemPrintFemMenberEF ->nbBound=%d :\n",rank,pFEF->nbBound);CHKERRQ(ierr);
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]",rank);CHKERRQ(ierr);
  for (i=0;i<pFEF->nbBound;i++){
    ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD," %d ",pFEF->Boundary[i]);CHKERRQ(ierr);
  }
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"\n");CHKERRQ(ierr);
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]FemSystemPrintFemMenberEF ->nbCells=%d :\n",rank,pFEF->nbCells);CHKERRQ(ierr);
  for (i=0;i<pFEF->nbCells;i++){
    ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i] %d %d %d %d\n",rank,
                                   pFEF->cells[3*i],
                                   pFEF->cells[3*i+1],
                                   pFEF->cells[3*i+2], pFEF->labelCell[i]);CHKERRQ(ierr);
  }
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"\n");CHKERRQ(ierr);
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]FemSystemPrintFemMenberEF ->nbVertex=%d :\n",rank,pFEF->nbVertex);CHKERRQ(ierr);
  ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i]",rank);CHKERRQ(ierr);
  for (i=0;i<pFEF->nbVertex;i++){
    for (j=0;j<pFEF->user.dim;j++){
      ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD," %e ",pFEF->vertex[2*i+j]);CHKERRQ(ierr);
    }
    ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"\n[%i]",rank);CHKERRQ(ierr);
  }
  ierr = PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);CHKERRQ(ierr);
  return ierr;
}

#undef __FUNCT__
#define __FUNCT__ "FemSystemCreateMesh"
PetscErrorCode FemSystemCreateMesh(MPI_Comm comm, FemEF* pFEF){
  PetscErrorCode ierr=0,i;

  PetscFunctionBeginUser;
  ierr = DMPlexCreateFromCellList(PETSC_COMM_WORLD,
                                  pFEF->user.dim,
                                  pFEF->nbCells,
                                  pFEF->nbVertex,
                                  3,1,
                                  pFEF->cells,
                                  2,
                                  pFEF->vertex,
                                  &(pFEF->dm));CHKERRQ(ierr);
  for (i=0;i<pFEF->nbBound;i++){
    ierr =DMPlexSetLabelValue((pFEF->dm), pFEF->user.bcType == NEUMANN?"boundary":"marker", pFEF->Boundary[i]+pFEF->nbCells, 1);CHKERRQ(ierr);
  }
  ierr = DMSetFromOptions(pFEF->dm);CHKERRQ(ierr);
  if (pFEF->user.bcType == NEUMANN) {
    DMLabel label;

    ierr = DMPlexCreateLabel(pFEF->dm, "boundary");CHKERRQ(ierr);
    ierr = DMPlexGetLabel(pFEF->dm, "boundary", &label);CHKERRQ(ierr);
    ierr = DMPlexMarkBoundaryFaces(pFEF->dm, label);CHKERRQ(ierr);
  }
  
/*  const char    *bdLabel             =  "boundary" ;
  PetscBool      has;
  ierr = DMPlexHasLabel(pFEF->dm, bdLabel, &has);CHKERRQ(ierr);
  if (!has) {
    DMLabel label;
    ierr = DMPlexCreateLabel(pFEF->dm, bdLabel);CHKERRQ(ierr);
    ierr = DMPlexGetLabel(pFEF->dm, bdLabel, &label);CHKERRQ(ierr);
    ierr = DMPlexMarkBoundaryFaces(pFEF->dm, label);CHKERRQ(ierr);
    }*/
  return ierr;
}


#undef __FUNCT__
#define __FUNCT__ "FemSystemMeshView"
PetscErrorCode FemSystemMeshView(FemEF* pFEF){
  PetscErrorCode ierr=0;
  PetscFunctionBeginUser;
  ierr=DMView(pFEF->dm,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  return ierr;
}




#undef __FUNCT__
#define __FUNCT__ "SetupProblem"
PetscErrorCode SetupProblem(DM dm, AppCtx *user)
{
  PetscDS        prob;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetDS(dm, &prob);CHKERRQ(ierr);
  switch (user->variableCoefficient) {
  case COEFF_NONE:
    ierr = PetscDSSetResidual(prob, 0, f0_u, f1_u);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, 0, 0, NULL, NULL, NULL, g3_uu);CHKERRQ(ierr);
    break;
  case COEFF_ANALYTIC:
    ierr = PetscDSSetResidual(prob, 0, f0_analytic_u, f1_analytic_u);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, 0, 0, NULL, NULL, NULL, g3_analytic_uu);CHKERRQ(ierr);
    break;
  case COEFF_FIELD:
    ierr = PetscDSSetResidual(prob, 0, f0_analytic_u, f1_field_u);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, 0, 0, NULL, NULL, NULL, g3_field_uu);CHKERRQ(ierr);
    break;
  case COEFF_NONLINEAR:
    ierr = PetscDSSetResidual(prob, 0, f0_analytic_nonlinear_u, f1_analytic_nonlinear_u);CHKERRQ(ierr);
    ierr = PetscDSSetJacobian(prob, 0, 0, NULL, NULL, NULL, g3_analytic_nonlinear_uu);CHKERRQ(ierr);
    break;
  default: SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_ARG_WRONG, "Invalid variable coefficient type %d", user->variableCoefficient);
  }
  switch (user->dim) {
  case 2:
    user->exactFuncs[0] = quadratic_u_2d;
    user->exactFuncsForBd[0] = quadratic_u_2dForBd;
    if (user->bcType == NEUMANN) {ierr = PetscDSSetBdResidual(prob, 0, f0_bd_u, f1_bd_zero);CHKERRQ(ierr);}
    break;
  case 3:
    user->exactFuncs[0] = quadratic_u_3d;
    if (user->bcType == NEUMANN) {ierr = PetscDSSetBdResidual(prob, 0, f0_bd_u, f1_bd_zero);CHKERRQ(ierr);}
    break;
  default:
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_OUTOFRANGE, "Invalid dimension %d", user->dim);
  }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "SetupMaterial"
PetscErrorCode SetupMaterial(DM dm, DM dmAux, AppCtx *user)
{
  void (*matFuncs[1])(const PetscReal x[], PetscScalar *u, void *ctx) = {nu_2d};
  Vec            nu;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMCreateLocalVector(dmAux, &nu);CHKERRQ(ierr);
  ierr = DMPlexProjectFunctionLocal(dmAux, matFuncs, NULL, INSERT_ALL_VALUES, nu);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject) dm, "A", (PetscObject) nu);CHKERRQ(ierr);
  ierr = VecDestroy(&nu);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SetupDiscretization"
PetscErrorCode SetupDiscretization(DM dm, AppCtx *user)
{
  DM             cdm   = dm;
  const PetscInt dim   = user->dim;
  const PetscInt id    = 1;
  PetscFE        feAux = NULL;
  PetscFE        feBd  = NULL;
  PetscFE        feCh  = NULL;
  PetscFE        fe;
  PetscDS        prob;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* Create finite element */
  ierr = PetscFECreateDefault(dm, dim, 1, PETSC_TRUE, NULL, -1, &fe);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) fe, "potential");CHKERRQ(ierr);
  if (user->bcType == NEUMANN) {
    ierr = PetscFECreateDefault(dm, dim-1, 1, PETSC_TRUE, "bd_", -1, &feBd);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject) feBd, "potential");CHKERRQ(ierr);
  }
  if (user->variableCoefficient == COEFF_FIELD) {
    PetscQuadrature q;

    ierr = PetscFECreateDefault(dm, dim, 1, PETSC_TRUE, "mat_", -1, &feAux);CHKERRQ(ierr);
    ierr = PetscFEGetQuadrature(fe, &q);CHKERRQ(ierr);
    ierr = PetscFESetQuadrature(feAux, q);CHKERRQ(ierr);
  }
  if (user->check) {ierr = PetscFECreateDefault(dm, dim, 1, PETSC_TRUE, "ch_", -1, &feCh);CHKERRQ(ierr);}
  /* Set discretization and boundary conditions for each mesh */
  while (cdm) {
    ierr = DMGetDS(cdm, &prob);CHKERRQ(ierr);
    ierr = PetscDSSetDiscretization(prob, 0, (PetscObject) fe);CHKERRQ(ierr);
    ierr = PetscDSSetBdDiscretization(prob, 0, (PetscObject) feBd);CHKERRQ(ierr);
    if (feAux) {
      DM      dmAux;
      PetscDS probAux;

      ierr = DMClone(cdm, &dmAux);CHKERRQ(ierr);
      ierr = DMPlexCopyCoordinates(cdm, dmAux);CHKERRQ(ierr);
      ierr = DMGetDS(dmAux, &probAux);CHKERRQ(ierr);
      ierr = PetscDSSetDiscretization(probAux, 0, (PetscObject) feAux);CHKERRQ(ierr);
      ierr = PetscObjectCompose((PetscObject) dm, "dmAux", (PetscObject) dmAux);CHKERRQ(ierr);
      ierr = SetupMaterial(cdm, dmAux, user);CHKERRQ(ierr);
      ierr = DMDestroy(&dmAux);CHKERRQ(ierr);
    }
    if (feCh) {
      DM      dmCh;
      PetscDS probCh;

      ierr = DMClone(cdm, &dmCh);CHKERRQ(ierr);
      ierr = DMPlexCopyCoordinates(cdm, dmCh);CHKERRQ(ierr);
      ierr = DMGetDS(dmCh, &probCh);CHKERRQ(ierr);
      ierr = PetscDSSetDiscretization(probCh, 0, (PetscObject) feCh);CHKERRQ(ierr);
      ierr = PetscObjectCompose((PetscObject) dm, "dmCh", (PetscObject) dmCh);CHKERRQ(ierr);
      ierr = DMDestroy(&dmCh);CHKERRQ(ierr);
    }
    ierr = SetupProblem(cdm, user);CHKERRQ(ierr);
    ierr = DMPlexAddBoundary(cdm, user->bcType == DIRICHLET, "wall", user->bcType == NEUMANN ? "boundary" : "marker", 0, user->exactFuncsForBd[0], 1, &id, user);CHKERRQ(ierr);
    ierr = DMPlexGetCoarseDM(cdm, &cdm);CHKERRQ(ierr);
  }
  ierr = PetscFEDestroy(&fe);CHKERRQ(ierr);
  ierr = PetscFEDestroy(&feBd);CHKERRQ(ierr);
  ierr = PetscFEDestroy(&feAux);CHKERRQ(ierr);
  ierr = PetscFEDestroy(&feCh);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "FSInitFemEF"
PetscErrorCode FSInitFemEF(FemEF *pFEF){
  PetscErrorCode ierr=0;
  int rank;
  PetscInt m,n;
  FemSystem * pFS=pFEF->pFS;
  PetscFunctionBeginUser;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);

  ierr = ProcessOptions(PETSC_COMM_WORLD, &(pFEF->user));CHKERRQ(ierr);
  pFEF->user.dim=2;
  pFEF->nbBound=8;
  pFEF->Boundary=(PetscInt*)malloc( pFEF->nbBound*sizeof(PetscInt));
  pFEF->nbCells=8;
  pFEF->cells=(PetscInt*)malloc( pFEF->nbCells*3*sizeof(PetscInt));
  pFEF->labelCell =(PetscInt*)malloc( pFEF->nbCells*sizeof(PetscInt));
  pFEF->nbVertex=9;
  pFEF->vertex=(PetscScalar*)malloc( pFEF->user.dim*pFEF->nbVertex*sizeof(PetscScalar));
  __affectInt(pFEF->Boundary,pFEF->nbBound,
              0,
              1,
              2,
              3,
              5,
              6,
              7,
              8);
  __affectInt(pFEF->cells,3*pFEF->nbCells,
              0,1,3,
              1,4,3,
              1,2,4,
              2,5,4,
              3,4,6,
              4,7,6,
              4,5,7,
              5,8,7
    );
  __affectInt(pFEF->labelCell,pFEF->nbCells,
              7,
              7,
              7,
              7,
              7,
              7,
              7,
              7);
  __affectScalar(pFEF->vertex,2*pFEF->nbVertex,
                  0.000000e+00+rank,0.000000e+00,
                  5.000000e-01+rank,0.000000e+00,
                  1.000000e+00+rank,0.000000e+00,
                  0.000000e+00+rank,5.000000e-01,
                  5.000000e-01+rank,5.000000e-01,
                  1.000000e+00+rank,5.000000e-01,
                  0.000000e+00+rank,1.000000e+00,
                  5.000000e-01+rank,1.000000e+00,
                  1.000000e+00+rank,1.000000e+00
    );
  if (pFEF->pFS->printFlags & PRINT_NODE_EF)
    ierr = FemSystemPrintFemEF(pFEF);CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD, &(pFEF->snes));CHKERRQ(ierr);
  ierr = FemSystemCreateMesh(PETSC_COMM_WORLD, pFEF);CHKERRQ(ierr);
  ierr = SNESSetDM(pFEF->snes, pFEF->dm);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(pFEF->dm, &(pFEF->user));CHKERRQ(ierr);
  ierr = PetscMalloc(1 * sizeof(void (*)(const PetscReal[], PetscScalar *, void *)), &(pFEF->user.exactFuncs));CHKERRQ(ierr);
  ierr = PetscMalloc(1 * sizeof(void (*)(const PetscReal[], PetscScalar *, void *)), &(pFEF->user.exactFuncsForBd));CHKERRQ(ierr);
  ierr = SetupDiscretization(pFEF->dm, &(pFEF->user));CHKERRQ(ierr);

  // ierr = DMCreateGlobalVector(pFEF->dm, &u);CHKERRQ(ierr);
  //ierr = PetscObjectSetName((PetscObject) u, "potential");CHKERRQ(ierr);
  //ierr = VecDuplicate(u, &r);CHKERRQ(ierr);

  ierr = DMSetMatType(pFEF->dm,MATAIJ);CHKERRQ(ierr);
  ierr = DMCreateMatrix(pFEF->dm, &pFEF->J);CHKERRQ(ierr);
  pFEF->A=pFEF->J;
//  ierr = DMCreateMatrix(pFEF->dm, &pFEF->A);CHKERRQ(ierr);
  ierr = MatGetLocalSize(pFEF->A,&m,&n);CHKERRQ(ierr);
  pFS->nDofs=m;
//  ierr = MatGetSize(pFEF->A,&m,&n);CHKERRQ(ierr);
  ierr = VecCreateMPI(PETSC_COMM_WORLD,pFS->nDofs,PETSC_DECIDE,&(pFS->vRhs));CHKERRQ(ierr);
  ierr = VecCreateMPI(PETSC_COMM_WORLD,pFS->nDofs,PETSC_DECIDE,&(pFS->vDofs));CHKERRQ(ierr);
  ierr = VecCreateMPI(PETSC_COMM_WORLD,pFS->nDofs,PETSC_DECIDE,&(pFS->vDofstm1));CHKERRQ(ierr);
  /*initial state:*/
  ierr=VecSet(pFS->vDofs, 1) ;CHKERRQ(ierr);
  ierr =VecAssemblyBegin(pFS->vDofs);CHKERRQ(ierr);
  ierr =VecAssemblyEnd(pFS->vDofs);CHKERRQ(ierr);

  if (pFEF->user.bcType == NEUMANN) {
    ierr = MatNullSpaceCreate(PetscObjectComm((PetscObject) pFEF->dm), PETSC_TRUE, 0, NULL, &pFEF->nullSpace);CHKERRQ(ierr);
    ierr = MatSetNullSpace(pFEF->J, pFEF->nullSpace);CHKERRQ(ierr);
    if (pFEF->A != pFEF->J) {
      ierr = MatSetNullSpace(pFEF->A, pFEF->nullSpace);CHKERRQ(ierr);
    }
  }
  ierr = FemSystemMeshView(pFEF);
  ierr = DMSNESSetFunctionLocal(pFEF->dm,  (PetscErrorCode (*)(DM,Vec,Vec,void*)) DMPlexSNESComputeResidualFEM, &pFEF->user);CHKERRQ(ierr);
  ierr = DMSNESSetJacobianLocal(pFEF->dm,  (PetscErrorCode (*)(DM,Vec,Mat,Mat,void*)) DMPlexSNESComputeJacobianFEM, &pFEF->user);CHKERRQ(ierr);
  ierr = SNESSetJacobian(pFEF->snes, pFEF->A, pFEF->J, NULL, NULL);CHKERRQ(ierr);
  
  ierr = SNESSetFromOptions(pFEF->snes);CHKERRQ(ierr);
  if (1){
    const char     *labelname;
    DMLabel         label;
    const PetscInt *ids;
    IS              pointIS,labelIS;
    const PetscInt *points;
    PetscInt        n,m, p,dim=2;
    PetscReal      *v0,*J;
    PetscReal detJ;
    PetscInt cStart,cEnd;
    
    ierr = PetscMalloc2(dim,&v0,dim*dim,&J);CHKERRQ(ierr);
    ierr = DMPlexGetBoundary(pFEF->dm, 0, NULL, NULL, &labelname, NULL, NULL, NULL, &ids, NULL);CHKERRQ(ierr);
    ierr = DMPlexGetLabel(pFEF->dm, labelname, &label);CHKERRQ(ierr);
    ierr = DMLabelGetStratumIS(label, ids[0], &pointIS);CHKERRQ(ierr);
    //ierr = DMPlexGetStratumIS(pFEF->dm,labelname, 0, &pointIS);CHKERRQ(ierr);
    ierr = ISGetLocalSize(pointIS, &n);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD," DMLabelGetStratumIS(label, 0, &pointIS) has %d \n",n);

    //Les points vont de ]cStart, cEnd]
    ierr = DMPlexGetHeightStratum(pFEF->dm, 0, &cStart, &cEnd);CHKERRQ(ierr);
    /*for (p = cStart; p < cEnd; ++p) {
      const PetscInt    point = points[p];
      ierr = DMPlexComputeCellGeometry(pFEF->dm, p, v0, NULL, NULL, &detJ);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"%s Bd point %i, x=%e,y=%e\n",__FUNCT__,point,v0[0],v0[1]);
      
      }*/
    for (p = cStart; p <= cEnd; ++p) {
      const PetscInt    point = points[p];
      PetscCellGeometry geom;
      //if ((point < cStart) || (point >= cEnd)) continue;
      //ierr = DMPlexComputeCellGeometry(pFEF->dm, point, v0, NULL, NULL, &detJ);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"%s Bd point %i, x=%e,y=%e\n",__FUNCT__,point,v0[0],v0[1]);
    }
    ierr = PetscFree2(v0,J);CHKERRQ(ierr);
  }
 
 

  
  return ierr;
}
