#include "petscvec.h"
#include "petscmat.h"
#include "FemSystem.h"
#include "FemTimeStepping.h"
#include "FemEF.h"
//mpirun -n 2 ./ex1 -FEMRunType 1 -viewNodeEF -petscspace_order 1 -show_initial -dm_plex_print_fem 1 -interpolate 1 -bd_petscspace_order 1 -bc_type neumann
int main(int argc,char **argv)
{
  	PetscErrorCode ierr;
    int rank;
    int mpi_size;
    
//    FemSystem aSystem;
    FemSystem* mypSystem;
    FemTimeStepping  aFTS;
    
    int i,row,col;
    PetscScalar value;
    PetscBool  flg;
    PetscInt nNodes=2;
    PetscInt nDofs;
    PetscInt rangeStartVector, rangeEndVector;
    PetscInt printFlags=0;
    PetscInt FemRunType=0;
    PetscInitialize(&argc, &argv, NULL, NULL);
    ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
    ierr = MPI_Comm_size(PETSC_COMM_WORLD, &mpi_size);CHKERRQ(ierr);
    ierr =PetscSynchronizedPrintf(PETSC_COMM_WORLD,"[%i] running with mpi_size=%i\n",rank,mpi_size);CHKERRQ(ierr);
    ierr =PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);CHKERRQ(ierr);
    /*About options*/ 
    PetscPrintf(PETSC_COMM_WORLD,"Possible options:\n-FEMRunType: 0 simple 1 mesh\n");
    PetscPrintf(PETSC_COMM_WORLD,"-viewNodeEF\n");
    PetscPrintf(PETSC_COMM_WORLD,"-viewPrePostSolve\n");
    PetscOptionsName("-viewNode","view node","",&flg);
    if (flg)
      printFlags=printFlags|PRINT_NODE;
    PetscOptionsName("-viewNodeEF","view node EF","",&flg);
    if (flg)
      printFlags=printFlags|PRINT_NODE_EF;
    PetscOptionsName("-FEMRunType","fem run type","",&flg);
    ierr = PetscOptionsInt("-FEMRunType", "fem run type", "0 or 1", 0, &FemRunType, NULL);CHKERRQ(ierr);
    if (FemRunType)
      PetscPrintf(PETSC_COMM_WORLD,"Running Mesh version.\n");
    else
      PetscPrintf(PETSC_COMM_WORLD,"Running simple version.\n");
    PetscOptionsName("-viewPrePostSolve","view vDofs vDofstm1","",&flg);
    if (flg)
      printFlags=printFlags|PRINT_PRE_POST_SOLVE;
    /*The subSystem managed by current process*/
    mypSystem=FSAllocSystem(1);
    mypSystem->printFlags=printFlags;
    if (FemRunType)
      mypSystem->myFSInitSystem=&FSpluginMeshInitSystem;
    else
      mypSystem->myFSInitSystem=&FSpluginSimpleInitSystem;
    
    
    ierr=FSInitSystem(mypSystem);CHKERRQ(ierr);

    
    /*Time stepping*/
    if (FemRunType){
      aFTS.myFTSInitTimeStepping=&FTSplunginMeshInitTimeStepping;
      aFTS.myFTSRunTimeStepping=&FTSpluginMeshRunTimeStepping;
     
    }else{
      aFTS.myFTSInitTimeStepping=&FTSplunginSimpleInitTimeStepping;
      aFTS.myFTSRunTimeStepping=&FTSpluginSimpleRunTimeStepping;
    }
    aFTS.TInit=0;
    aFTS.TEnd=1;
    aFTS.TStep=1;
    aFTS.pFS=mypSystem;
    ierr = FTSInitTimeStepping(&aFTS);CHKERRQ(ierr);
    ierr = FTSRunTimeStepping(&aFTS);CHKERRQ(ierr);
    
    PetscFinalize();
    FSFreeSystem(mypSystem);
    return 0;

}
