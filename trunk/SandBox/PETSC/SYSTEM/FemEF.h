#ifndef FEMEM
#define FEMEM
#include <petscdmplex.h>
#include <petscsnes.h>
#include <petscds.h>
#include <petscviewerhdf5.h>

#define spatialDim 2
#define TracePlugin "traceplugin"
//PetscInt spatialDim = 0;

typedef enum {NEUMANN, DIRICHLET, NONE} BCType;
typedef enum {RUN_FULL, RUN_TEST, RUN_PERF} RunType;
typedef enum {COEFF_NONE, COEFF_ANALYTIC, COEFF_FIELD, COEFF_NONLINEAR} CoeffType;
typedef struct  FS FemSystem;

typedef struct {
  PetscInt      debug;             /* The debugging level */
  RunType       runType;           /* Whether to run tests, or solve the full problem */
  PetscBool     jacobianMF;        /* Whether to calculate the Jacobian action on the fly */
  PetscLogEvent createMeshEvent;
  PetscBool     showInitial, showSolution, restart, check;
  PetscViewer   checkpoint;
  /* Domain and mesh definition */
  PetscInt      dim;               /* The topological mesh dimension */
  char          filename[2048];    /* The optional ExodusII file */
  PetscBool     interpolate;       /* Generate intermediate mesh elements */
  PetscReal     refinementLimit;   /* The largest allowable cell volume */
  PetscBool     refinementUniform; /* Uniformly refine the mesh */
  PetscInt      refinementRounds;  /* The number of uniform refinements */
  char          partitioner[2048]; /* The graph partitioner */
  /* Problem definition */
  BCType        bcType;
  CoeffType     variableCoefficient;
  void       (**exactFuncs)(const PetscReal x[], PetscScalar *u, void *ctx);
  void       (**exactFuncsForBd)(const PetscReal x[], PetscScalar *u, void *ctx);
} AppCtx;

typedef struct  FE {
  FemSystem* pFS;
  AppCtx user;
  PetscInt nbBound;
  PetscInt* Boundary;
  PetscInt nbCells;
  PetscInt *cells;
  PetscInt *labelCell;
  PetscInt nbVertex;
  PetscScalar *vertex;
  DM             dm;          /* Problem specification */
  SNES           snes;        /* nonlinear solver */
  Mat            A,J;         /* Jacobian matrix */
  MatNullSpace   nullSpace;   /* May be necessary for Neumann conditions */
}FemEF;



PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options);
PetscErrorCode SetupProblem(DM dm, AppCtx *user);
PetscErrorCode SetupMaterial(DM dm, DM dmAux, AppCtx *user);
PetscErrorCode SetupDiscretization(DM dm, AppCtx *user);
PetscErrorCode FSInitFemEF(FemEF *pFEF);
//PetscErrorCode FemSystemMeshView(FemMemberEF* pFMEF);
//PetscErrorCode FemSystemCreateMesh(MPI_Comm comm, FemEF* pFMEF);
#endif
