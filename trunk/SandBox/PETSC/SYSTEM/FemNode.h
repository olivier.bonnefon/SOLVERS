#ifndef FEMNODE
#define FEMNODE
#include "petscvec.h"
#include "petscmat.h"

typedef struct FS FemSystem;

/*
  PetscInt nDofs: numbers of node's dofs
  PetscInt IstartInFSDofs: first index in the local vector of pFS
  PetscInt IendInFSDofs;
  FemSystem * pFS: the owner

 */
typedef struct  {
  PetscInt nDofs;
  PetscInt IstartInFSDofs;
  PetscInt IendInFSDofs;
  FemSystem * pFS;
//  void * member;
}FemNode;


PetscErrorCode FSInitNode(FemNode * pFN,PetscInt IstartInFSDofs ,PetscInt ndofs);

PetscErrorCode FSUpdateMatrixNode(FemNode * pFN);

PetscErrorCode FSUpdateRhsNode(FemNode * pFN);

#endif
