#ifndef FEMTIMESTEPPING
#define FEMTIMESTEPPING

#include "FemSystem.h"
#include "petscksp.h"
typedef struct FTS FemTimeStepping;
typedef PetscErrorCode (*pFTSRunTimeStepping)(FemTimeStepping * pFTS);
typedef PetscErrorCode (*pFTSInitTimeStepping)(FemTimeStepping * pFTS);
typedef struct FTS {

  PetscScalar TInit;
  PetscScalar TEnd;
  PetscScalar TStep;
  PetscScalar _TCur;
  FemSystem* pFS;
  pFTSInitTimeStepping myFTSInitTimeStepping;
  pFTSRunTimeStepping myFTSRunTimeStepping;
  KSP ksp; //should be optional
	PC pc;	//should be optional
  
}FemTimeStepping;
/*generic*/
PetscErrorCode FTSRunTimeStepping(FemTimeStepping * pFTS){
  return (*(pFTS->myFTSRunTimeStepping))(pFTS);
}
/*generic*/
PetscErrorCode FTSInitTimeStepping(FemTimeStepping * pFTS){
  return (*(pFTS->myFTSInitTimeStepping))(pFTS);;
}

PetscErrorCode FTSplunginSimpleInitTimeStepping(FemTimeStepping * pFTS);

PetscErrorCode FTSpluginSimpleRunTimeStepping(FemTimeStepping * pFTS);

PetscErrorCode FTSplunginMeshInitTimeStepping(FemTimeStepping * pFTS);

PetscErrorCode FTSpluginMeshRunTimeStepping(FemTimeStepping * pFTS);



#endif
