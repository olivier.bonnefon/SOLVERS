
#include "FemTimeStepping.h"

/*Solve Linear System

  -> solve M*vDofs=rhs

 */
#undef __FUNCT__
#define __FUNCT__ "FTSpluginSimpleRunTimeStepping"
PetscErrorCode FTSpluginSimpleRunTimeStepping(FemTimeStepping * pFTS){
  PetscErrorCode ierr;
  PetscInt aux;
  PetscFunctionBeginUser;
  KSP *pksp=&(pFTS->ksp);
  FemSystem* pFS=pFTS->pFS;
  Vec *prhs=&(pFS->vRhs);
  Vec *pdof=&(pFS->vDofs);
  
  pFTS->_TCur=pFTS->TInit;
  while(pFTS->_TCur<pFTS->TEnd){
    int its;
    PetscPrintf(PETSC_COMM_WORLD,"FTSSolveTimeStepping %e\n",pFTS->_TCur);
    ierr = VecGetSize(*prhs,&aux);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"vrhs global size : %d\n", aux);
    ierr = VecGetSize(*pdof,&aux);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"vdofs global size : %d\n", aux);
    //KSPView(*pksp,PETSC_VIEWER_STDOUT_WORLD);
    ierr = FSUpdateMatrixSystem(pFS);CHKERRQ(ierr);
    ierr = FSUpdateRhsSystem(pFS);CHKERRQ(ierr);
    ierr = KSPSolve(*pksp,*prhs,*pdof);CHKERRQ(ierr);
    KSPGetIterationNumber(*pksp,&its);
    PetscPrintf(PETSC_COMM_WORLD,"FTSSolveTimeStepping its : %d\n", its);
    pFTS->_TCur+=pFTS->TStep;
    ierr = VecView(*pdof,	PETSC_VIEWER_STDOUT_WORLD );CHKERRQ(ierr);
  }
  return ierr;
  
}

#undef __FUNCT__
#define __FUNCT__ "FTSplunginSimpleInitTimeStepping"
PetscErrorCode FTSplunginSimpleInitTimeStepping(FemTimeStepping * pFTS){
  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  KSP *pksp=&(pFTS->ksp);
  PC *ppc=&(pFTS->pc);
  Mat *pM=&(pFTS->pFS->M);
	ierr = KSPCreate(PETSC_COMM_WORLD,pksp);CHKERRQ(ierr);CHKERRQ(ierr);
	ierr = KSPSetOperators(*pksp,*pM,*pM);CHKERRQ(ierr);
	//Set the PC type to be the new method
	ierr = KSPGetPC(*pksp,ppc);CHKERRQ(ierr);
	ierr = KSPSetType(*pksp,"cg");CHKERRQ(ierr);
	ierr = PCSetType(*ppc,"asm");CHKERRQ(ierr);
	ierr = KSPSetType(*pksp, "gmres");CHKERRQ(ierr);
	ierr = KSPSetFromOptions(*pksp);CHKERRQ(ierr);
  return ierr;
}
#undef __FUNCT__
#define __FUNCT__ "FTSplunginMeshInitTimeStepping"
PetscErrorCode FTSplunginMeshInitTimeStepping(FemTimeStepping * pFTS){
  PetscErrorCode ierr=0;
  PetscFunctionBeginUser;
  
  return ierr;
}

#undef __FUNCT__
#define __FUNCT__ "FTSpluginMeshRunTimeStepping"
PetscErrorCode FTSpluginMeshRunTimeStepping(FemTimeStepping * pFTS){
  PetscErrorCode ierr=0;
  PetscInt       its; 
  PetscFunctionBeginUser;
  FemSystem* pFS=pFTS->pFS;
  pFTS->_TCur=pFTS->TInit;
  while(pFTS->_TCur<pFTS->TEnd){
    
    VecView(pFS->vDofs,	PETSC_VIEWER_STDOUT_WORLD 	);
    //SNESSolve(snes,rhs, vDofs: current dofs)
    ierr = FSPreSolveSystem(pFS);CHKERRQ(ierr);
    ierr = SNESSolve(((FemEF* )(pFS->userData))->snes, pFS->vRhs, pFS->vDofs);CHKERRQ(ierr);
    ierr = FSPostSolveSystem(pFS);CHKERRQ(ierr);
    ierr = SNESGetIterationNumber(((FemEF* )(pFS->userData))->snes, &its);CHKERRQ(ierr);
    ierr = VecView(pFS->vDofs,	PETSC_VIEWER_STDOUT_WORLD );CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "Number of SNES iterations = %D\n", its);CHKERRQ(ierr);
    pFTS->_TCur+=pFTS->TStep;
  }

  return ierr;
}
