#include "FemNode.h"
#include "FemSystem.h"

PetscErrorCode FSInitNode(FemNode * pFN, PetscInt IstartInFSDofs,PetscInt ndofs){
  PetscErrorCode ierr=0;
  pFN->nDofs=ndofs;
  pFN->IstartInFSDofs=IstartInFSDofs;
  pFN->IendInFSDofs=IstartInFSDofs+ndofs;
  return ierr;
}

PetscErrorCode FSUpdateMatrixNode(FemNode * pFN){
  PetscErrorCode ierr=0;
  int i,row,col,rank;
  PetscScalar value;
  Mat  M=pFN->pFS->M;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
  /*each process add his diag matrix*/
  for (i=pFN->pFS->loc_start+pFN->IstartInFSDofs;i<pFN->pFS->loc_start+pFN->IendInFSDofs;i++){
    row = i;
    col = i;
    //if( i%2 == 0)
    value = 1.+rank;
    //else
    //value = -1.;
    ierr = MatSetValues(M,1,&row,1,&col,&value,INSERT_VALUES);CHKERRQ(ierr);
  }
  
  ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"FSUpdateMatrixNode Mat M\n");
  //    MatView(M, PETSC_VIEWER_DRAW_WORLD);
  MatView(M, PETSC_VIEWER_STDOUT_WORLD);
  return ierr;
}

PetscErrorCode FSUpdateRhsNode(FemNode * pFN){
  PetscErrorCode ierr=0;
  PetscInt i;
  PetscScalar value;
  Vec vDofs=pFN->pFS->vDofs;
  Vec vRhs=pFN->pFS->vRhs;
  for (i=pFN->pFS->loc_start+pFN->IstartInFSDofs;i<pFN->pFS->loc_start+pFN->IendInFSDofs;i++){
    ierr = VecGetValues(vDofs,1,&i,&value);CHKERRQ(ierr);
    ierr = VecSetValues(vRhs,1,&i,&value,INSERT_VALUES);CHKERRQ(ierr);
  }
  ierr =VecAssemblyBegin(vRhs);CHKERRQ(ierr);
  ierr =VecAssemblyEnd(vRhs);CHKERRQ(ierr);
  
  return ierr;
}

