#include "FemSystem.h"
#include "FemEF.h"
#include "FemEF1D.h"

/*Alloc memory for the FemSystem and the FemNodes*/
FemSystem * FSAllocSystem(PetscInt nNodes){
  
  FemSystem * pFS=(FemSystem *)malloc(sizeof(FemSystem));
  pFS->userData=NULL;
  pFS->printFlags=0;
  
  PetscInt i;
  pFS->nNodes=nNodes;
  pFS->tNodes=(FemNode *)malloc(pFS->nNodes*sizeof(FemNode));
  for (i=0;i<pFS->nNodes;i++){
    pFS->tNodes[0].pFS=pFS;
  }
  return pFS;
}

void FSFreeSystem(FemSystem * pFS){
  free(pFS->tNodes);
  free(pFS);
}
#undef __FUNCT__
#define __FUNCT__ "FSInitSystem"
PetscErrorCode FSInitSystem(FemSystem * pFS){
  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  ierr=(*(pFS->myFSInitSystem))(pFS);CHKERRQ(ierr);
  return ierr;
}
#undef __FUNCT__
#define __FUNCT__ "FSpluginSimpleInitSystem"
PetscErrorCode FSpluginSimpleInitSystem(FemSystem * pFS){
  PetscErrorCode ierr;
  PetscInt i,rank;
  PetscScalar value=1+pFS->nDofs;
  PetscFunctionBeginUser;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
  ierr =FSInitNode(&(pFS->tNodes[0]),0,rank?7:5);CHKERRQ(ierr);
  pFS->nDofs=rank?7:5;
  ierr = VecCreateMPI(PETSC_COMM_WORLD,pFS->nDofs,PETSC_DECIDE,&(pFS->vRhs));CHKERRQ(ierr);
  ierr = VecCreateMPI(PETSC_COMM_WORLD,pFS->nDofs,PETSC_DECIDE,&(pFS->vDofs));CHKERRQ(ierr);

  ierr = VecGetSize(pFS->vDofs,&(pFS->nGlobalDofs));CHKERRQ(ierr);


  ierr = MatCreateAIJ(PETSC_COMM_WORLD,
                         pFS->nDofs,
                         pFS->nDofs,
                         PETSC_DECIDE,
                         PETSC_DECIDE,
                         1,PETSC_NULL,
                         0,PETSC_NULL,
                         &(pFS->M));CHKERRQ(ierr);
   ierr = MatGetOwnershipRange(pFS->M,&(pFS->loc_start),&(pFS->loc_end));CHKERRQ(ierr);
  
  ierr = MatSetFromOptions(pFS->M);CHKERRQ(ierr);
  ierr = MatSetUp(pFS->M);CHKERRQ(ierr);
  /*initial state:*/
  ierr=VecSet(pFS->vDofs, 1) ;CHKERRQ(ierr);
  ierr =VecAssemblyBegin(pFS->vDofs);CHKERRQ(ierr);
  ierr =VecAssemblyEnd(pFS->vDofs);CHKERRQ(ierr);

  return ierr;
}
#undef __FUNCT__
#define __FUNCT__ "__FSMeshToVtk"
PetscErrorCode __FSMeshToVtk(FemSystem * pFS){

  PetscErrorCode ierr=0;
  char filenameOutput[256];
  PetscViewer viewer;
  Vec         uLocal;
  PetscInt rank;
  DM *pDm=&((FemEF*)(pFS->userData))->dm;
  PetscFunctionBeginUser;
  ierr = PetscPrintf(PETSC_COMM_WORLD, "debut %s\n", __FUNCT__);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
  sprintf(filenameOutput,"file%i.vtk",rank);
    
  ierr = PetscViewerCreate(PETSC_COMM_WORLD, &viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer, PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer, filenameOutput);CHKERRQ(ierr);
    
    ierr = DMGetLocalVector(*pDm, &uLocal);CHKERRQ(ierr);
//    ierr = DMPlexProjectFunctionLocal(user.dm, 2, my_BCs, INSERT_BC_VALUES, uLocal);CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(*pDm,pFS->vDofs , INSERT_VALUES, uLocal);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(*pDm,pFS->vDofs , INSERT_VALUES, uLocal);CHKERRQ(ierr);
    
    ierr = PetscObjectReference((PetscObject) *pDm);CHKERRQ(ierr); /* Needed because viewer destroys the DM */
    ierr = PetscObjectReference((PetscObject) uLocal);CHKERRQ(ierr); /* Needed because viewer destroys the Vec */
    ierr =	VecView(uLocal,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PetscViewerVTKAddField(viewer, (PetscObject) *pDm, DMPlexVTKWriteAll, PETSC_VTK_POINT_FIELD, (PetscObject) uLocal);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "fin %s\n", __FUNCT__);CHKERRQ(ierr);
    return ierr;
}
#undef __FUNCT__
#define __FUNCT__ "FSpluginMeshInitSystem"
PetscErrorCode FSpluginMeshInitSystem(FemSystem * pFS){
  FemEF * pFemEF;
  PetscErrorCode ierr=0;
  PetscFunctionBeginUser;
  pFS->userData=malloc(sizeof(FemEF));
  pFemEF=(FemEF *)pFS->userData;
  pFemEF->pFS=pFS;
  ierr=FSInitFemEF(pFemEF);CHKERRQ(ierr);
  ierr=__FSMeshToVtk(pFS);CHKERRQ(ierr);
  return ierr;
}




#undef __FUNCT__
#define __FUNCT__ "FSUpdateMatrixSystem"
PetscErrorCode FSUpdateMatrixSystem(FemSystem * pFS){
  PetscErrorCode ierr=0;
  PetscInt i;
  PetscFunctionBeginUser;
  for (i=0; i<pFS->nNodes; i++){
    ierr= FSUpdateMatrixNode(pFS->tNodes+i);CHKERRQ(ierr);
  }
  return ierr;
}

#undef __FUNCT__
#define __FUNCT__ "FSUpdateRhsSystem"
PetscErrorCode FSUpdateRhsSystem(FemSystem * pFS){
  PetscErrorCode ierr=0;
  PetscInt i;
  PetscFunctionBeginUser;
  for (i=0; i<pFS->nNodes; i++){
    ierr= FSUpdateRhsNode(pFS->tNodes+i);CHKERRQ(ierr);
  }
  return ierr;
}

#undef __FUNCT__
#define __FUNCT__ "FSPostSolveSystem"
PetscErrorCode FSPostSolveSystem(FemSystem * pFS){
  PetscErrorCode ierr=0;
  PetscFunctionBeginUser;
  if (pFS->printFlags & PRINT_PRE_POST_SOLVE){
    ierr = PetscPrintf(PETSC_COMM_WORLD, "debut %s\n", __FUNCT__);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, " vDof\n");CHKERRQ(ierr);
    ierr =	VecView(pFS->vDofs,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "fin %s\n", __FUNCT__);CHKERRQ(ierr);
  }
  ierr=__FSMeshToVtk(pFS);CHKERRQ(ierr);
  return ierr;

}
#undef __FUNCT__
#define __FUNCT__ "FSPreSolveSystem"
PetscErrorCode FSPreSolveSystem(FemSystem * pFS){
  PetscErrorCode ierr=0;
  PetscFunctionBeginUser;
  ierr=VecCopy(pFS->vDofs,pFS->vDofstm1);CHKERRQ(ierr);
  if (pFS->printFlags & PRINT_PRE_POST_SOLVE){
    ierr = PetscPrintf(PETSC_COMM_WORLD, "debut %s\n", __FUNCT__);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, " vDofstm1\n");CHKERRQ(ierr);
    ierr=	VecView(pFS->vDofstm1,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "fin %s\n", __FUNCT__);CHKERRQ(ierr);
  }
  return ierr;
}



#undef __FUNCT__
#define __FUNCT__ "FSplugin1DInitSystem"
PetscErrorCode FSplugin1DInitSystem(FemSystem * pFS){
  FemEF1D * pFemEF1D;
  PetscErrorCode ierr=0;
  PetscInt nPoints=5,i;
  PetscScalar * pCoord =( PetscScalar *)malloc(2*nPoints*sizeof(PetscScalar));
  for (i=0;i<nPoints;i++){
    pCoord[2*i]=i;
    pCoord[2*i+1]=0;
  }
  PetscFunctionBeginUser;
  pFS->userData=malloc(sizeof(FemEF1D));
  pFemEF1D=(FemEF1D *)pFS->userData;
  pFemEF1D->pFS=pFS;
  
  ierr=FSInitFemEF1D(pFemEF1D,nPoints,pCoord);CHKERRQ(ierr);
  
  ierr=MatView(pFemEF1D->MASSE,PETSC_VIEWER_STDOUT_WORLD );
  free(pCoord);
  return ierr;
}
