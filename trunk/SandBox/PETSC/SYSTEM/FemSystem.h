#ifndef FEMSYSTEM
#define FEMSYSTEM

#include "FemNode.h"

enum FEMSYSTEM_PRINT {
PRINT_NODE = 1,
PRINT_NODE_EF = 1 << 1,
PRINT_PRE_POST_SOLVE=1 << 2
};


#include "FemEF.h"
typedef PetscErrorCode (*pFSInitSystem)(FemSystem * pFS);

/*

  PetsInt nNodes: Number of nodes in the system
  FemNode* tNodes: array of nodes
  int nDofs: number of dofs of this system
  PetscInt nGlobalDofs: total number of dofs
  Mat M: Matrix
  Vec vRhs
  Vec vDofs;
  PetsInt loc_start, loc_end; : range of this.dofs in the blobal indices.
 */
typedef struct FS{
  PetscInt nNodes;
  FemNode* tNodes;
  int nDofs;
  PetscInt nGlobalDofs;
  Mat M;
  Vec vRhs;
  Vec vDofs;
  Vec vDofstm1;
  PetscInt loc_start, loc_end;
  pFSInitSystem myFSInitSystem;
  unsigned int printFlags;
  void * userData;
}FemSystem;


/*Alloc memory for the FemSystem and the FemNodes*/
FemSystem * FSAllocSystem(PetscInt nNodes);

void FSFreeSystem(FemSystem * pFS);

/*generic init system*/
PetscErrorCode FSInitSystem(FemSystem * pFS);



/*simple init system*/
PetscErrorCode FSpluginSimpleInitSystem(FemSystem * pFS);
/*snes init system */
PetscErrorCode FSpluginMeshInitSystem(FemSystem * pFS);


PetscErrorCode FSUpdateMatrixSystem(FemSystem * pFS);

PetscErrorCode FSUpdateRhsSystem(FemSystem * pFS);

PetscErrorCode FSPostSolveSystem(FemSystem * pFS);
PetscErrorCode FSPreSolveSystem(FemSystem * pFS);

#endif
