function LikeLihood=callFF(P)% call Freefem

% 
% display('parametre:')
% P
fileID = fopen('inputParam.edp','w');
%diffusion 2D:10eu6[0]
fprintf(fileID,'u6[0]=%.15f;\n',P(1));

%diffusion 1D:10eu6[1]
fprintf(fileID,'u6[1]=%.15f;\n',P(2));

%R=10eu6[2]
fprintf(fileID,'u6[2]=%.15f;\n',P(3));

%mu=u6[3]*u6[3]
fprintf(fileID,'u6[3]=%.15f;\n',P(4));

%nu=u6[4]*u6[4]
fprintf(fileID,'u6[4]=%.15f;\n',P(5));

%dirchlet =u6[5]*u6[5];
fprintf(fileID,'u6[5]=%.15f;\n',P(6));
fclose(fileID);
MatlabPath = getenv('LD_LIBRARY_PATH');
setenv('LD_LIBRARY_PATH','/usr/local/lib:/usr/lib:');
system('/home/commun_biom/CLUSTER/BUILD/archi2/freefem++-3.38/src/nw/FreeFem++-nw cas1.edp ');
setenv('LD_LIBRARY_PATH',MatlabPath);
fileRES = fopen('resLikelihood','r');
LikeLihood = fscanf(fileRES,'%f');
fclose(fileRES);

%save('res_intermediaires','P','LikeLihood')
% fprintf('LikeLihood = %.15f\n',LikeLihood);
% quit
