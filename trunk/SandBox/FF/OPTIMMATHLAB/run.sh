#!/bin/bash
#$ -S /bin/bash
#$ -N D21BFGS
#$ -V
#$ -cwd
#$ -q all.q@pcbiom25
matlab -nodesktop -nodisplay < test_optimFF.m > result.txt 
