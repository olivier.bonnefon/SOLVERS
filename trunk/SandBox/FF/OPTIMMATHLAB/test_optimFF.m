x0=[-5 1 1 2 3 0.5];
bl=[-7 -1 0 1 2 0]; % lower bound
bm=[-4 4 5 5 5 1]; % upper bound

for i=1:1
    options = optimset('Display','iter');
    %minimisation sous contrainte
    [xopt , fopt , exitflag] = fmincon('callFF',x0,[],[],[],[],bl,bm,[],options);
    namesave=['result_' num2str(i)];
    save(namesave,'x0','xopt','fopt')
    x0=bl+rand(1,6).*(bm-bl);
end
