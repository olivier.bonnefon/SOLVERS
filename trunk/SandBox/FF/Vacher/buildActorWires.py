points = vtk.vtkPoints()
points.SetNumberOfPoints(125)
RLine=1
GLine=1
BLine=1
widthLine=2.5

points.SetPoint(0, -1.239156e+02, 4.667865e+01, 0.1)
points.SetPoint(1, -1.237253e+02, 4.619508e+01, 0.1)
points.SetPoint(2, -1.242869e+02, 4.343630e+01, 0.1)
points.SetPoint(3, -1.241975e+02, 4.081185e+01, 0.1)
points.SetPoint(4, -1.238048e+02, 3.928966e+01, 0.1)
points.SetPoint(5, -1.224723e+02, 3.783681e+01, 0.1)
points.SetPoint(6, -1.222736e+02, 3.757052e+01, 0.1)
points.SetPoint(7, -1.218071e+02, 3.671416e+01, 0.1)
points.SetPoint(8, -1.206163e+02, 3.481631e+01, 0.1)
points.SetPoint(9, -1.196591e+02, 3.441681e+01, 0.1)
points.SetPoint(10, -1.172637e+02, 3.285005e+01, 0.1)
points.SetPoint(11, -1.155335e+02, 3.266196e+01, 0.1)
points.SetPoint(12, -1.119794e+02, 3.162068e+01, 0.1)
points.SetPoint(13, -1.082086e+02, 3.149974e+01, 0.1)
points.SetPoint(14, -1.067505e+02, 3.178390e+01, 0.1)
points.SetPoint(15, -1.047112e+02, 3.022292e+01, 0.1)
points.SetPoint(16, -1.035228e+02, 2.913404e+01, 0.1)
points.SetPoint(17, -1.028325e+02, 2.943311e+01, 0.1)
points.SetPoint(18, -1.015802e+02, 2.976966e+01, 0.1)
points.SetPoint(19, -9.926221e+01, 2.681567e+01, 0.1)
points.SetPoint(20, -9.744527e+01, 2.585276e+01, 0.1)
points.SetPoint(21, -9.742230e+01, 2.725771e+01, 0.1)
points.SetPoint(22, -9.728623e+01, 2.770495e+01, 0.1)
points.SetPoint(23, -9.651359e+01, 2.863971e+01, 0.1)
points.SetPoint(24, -9.481070e+01, 2.935344e+01, 0.1)
points.SetPoint(25, -9.470245e+01, 2.965317e+01, 0.1)
points.SetPoint(26, -9.210592e+01, 2.958633e+01, 0.1)
points.SetPoint(27, -9.120009e+01, 2.938955e+01, 0.1)
points.SetPoint(28, -9.007336e+01, 2.922728e+01, 0.1)
points.SetPoint(29, -8.925935e+01, 2.905836e+01, 0.1)
points.SetPoint(30, -8.942238e+01, 2.939063e+01, 0.1)
points.SetPoint(31, -8.941454e+01, 2.975237e+01, 0.1)
points.SetPoint(32, -8.974627e+01, 2.992822e+01, 0.1)
points.SetPoint(33, -8.790439e+01, 3.054106e+01, 0.1)
points.SetPoint(34, -8.765689e+01, 3.024971e+01, 0.1)
points.SetPoint(35, -8.542269e+01, 2.994811e+01, 0.1)
points.SetPoint(36, -8.436588e+01, 3.002459e+01, 0.1)
points.SetPoint(37, -8.305687e+01, 2.914626e+01, 0.1)
points.SetPoint(38, -8.267732e+01, 2.770621e+01, 0.1)
points.SetPoint(39, -8.251426e+01, 2.770559e+01, 0.1)
points.SetPoint(40, -8.209015e+01, 2.692319e+01, 0.1)
points.SetPoint(41, -8.083889e+01, 2.517593e+01, 0.1)
points.SetPoint(42, -8.010527e+01, 2.609626e+01, 0.1)
points.SetPoint(43, -8.129152e+01, 2.992063e+01, 0.1)
points.SetPoint(44, -8.113349e+01, 3.162335e+01, 0.1)
points.SetPoint(45, -7.918487e+01, 3.318009e+01, 0.1)
points.SetPoint(46, -7.647181e+01, 3.477677e+01, 0.1)
points.SetPoint(47, -7.682737e+01, 3.494118e+01, 0.1)
points.SetPoint(48, -7.665163e+01, 3.504621e+01, 0.1)
points.SetPoint(49, -7.700191e+01, 3.550279e+01, 0.1)
points.SetPoint(50, -7.571717e+01, 3.569365e+01, 0.1)
points.SetPoint(51, -7.604636e+01, 3.565907e+01, 0.1)
points.SetPoint(52, -7.672493e+01, 3.595536e+01, 0.1)
points.SetPoint(53, -7.586026e+01, 3.628424e+01, 0.1)
points.SetPoint(54, -7.583370e+01, 3.629363e+01, 0.1)
points.SetPoint(55, -7.584145e+01, 3.644555e+01, 0.1)
points.SetPoint(56, -7.665400e+01, 3.703917e+01, 0.1)
points.SetPoint(57, -7.656793e+01, 3.708047e+01, 0.1)
points.SetPoint(58, -7.652719e+01, 3.761131e+01, 0.1)
points.SetPoint(59, -7.639005e+01, 3.763033e+01, 0.1)
points.SetPoint(60, -7.729608e+01, 3.836980e+01, 0.1)
points.SetPoint(61, -7.657157e+01, 3.820190e+01, 0.1)
points.SetPoint(62, -7.638624e+01, 3.838163e+01, 0.1)
points.SetPoint(63, -7.620235e+01, 3.936445e+01, 0.1)
points.SetPoint(64, -7.615053e+01, 3.907942e+01, 0.1)
points.SetPoint(65, -7.595643e+01, 3.813113e+01, 0.1)
points.SetPoint(66, -7.598162e+01, 3.743412e+01, 0.1)
points.SetPoint(67, -7.522434e+01, 3.805091e+01, 0.1)
points.SetPoint(68, -7.551580e+01, 3.937835e+01, 0.1)
points.SetPoint(69, -7.489406e+01, 3.916598e+01, 0.1)
points.SetPoint(70, -7.397940e+01, 4.041734e+01, 0.1)
points.SetPoint(71, -7.235508e+01, 4.088158e+01, 0.1)
points.SetPoint(72, -7.302086e+01, 4.096538e+01, 0.1)
points.SetPoint(73, -7.233389e+01, 4.128292e+01, 0.1)
points.SetPoint(74, -7.136013e+01, 4.174949e+01, 0.1)
points.SetPoint(75, -7.014088e+01, 4.165042e+01, 0.1)
points.SetPoint(76, -7.000901e+01, 4.187662e+01, 0.1)
points.SetPoint(77, -7.073379e+01, 4.221421e+01, 0.1)
points.SetPoint(78, -7.079830e+01, 4.292664e+01, 0.1)
points.SetPoint(79, -6.878368e+01, 4.447388e+01, 0.1)
points.SetPoint(80, -6.720624e+01, 4.464103e+01, 0.1)
points.SetPoint(81, -6.749092e+01, 4.559149e+01, 0.1)
points.SetPoint(82, -6.778958e+01, 4.695922e+01, 0.1)
points.SetPoint(83, -6.905025e+01, 4.725588e+01, 0.1)
points.SetPoint(84, -7.082561e+01, 4.540031e+01, 0.1)
points.SetPoint(85, -7.402739e+01, 4.499577e+01, 0.1)
points.SetPoint(86, -7.597019e+01, 4.434284e+01, 0.1)
points.SetPoint(87, -7.620103e+01, 4.369667e+01, 0.1)
points.SetPoint(88, -7.810451e+01, 4.337563e+01, 0.1)
points.SetPoint(89, -7.898326e+01, 4.297246e+01, 0.1)
points.SetPoint(90, -8.121662e+01, 4.177223e+01, 0.1)
points.SetPoint(91, -8.319452e+01, 4.163101e+01, 0.1)
points.SetPoint(92, -8.274208e+01, 4.267517e+01, 0.1)
points.SetPoint(93, -8.262865e+01, 4.381130e+01, 0.1)
points.SetPoint(94, -8.356316e+01, 4.368456e+01, 0.1)
points.SetPoint(95, -8.375706e+01, 4.398660e+01, 0.1)
points.SetPoint(96, -8.343895e+01, 4.500001e+01, 0.1)
points.SetPoint(97, -8.500608e+01, 4.575442e+01, 0.1)
points.SetPoint(98, -8.565232e+01, 4.484839e+01, 0.1)
points.SetPoint(99, -8.625193e+01, 4.440098e+01, 0.1)
points.SetPoint(100, -8.623367e+01, 4.301857e+01, 0.1)
points.SetPoint(101, -8.706747e+01, 4.166141e+01, 0.1)
points.SetPoint(102, -8.780325e+01, 4.243548e+01, 0.1)
points.SetPoint(103, -8.718787e+01, 4.495557e+01, 0.1)
points.SetPoint(104, -8.760414e+01, 4.484010e+01, 0.1)
points.SetPoint(105, -8.733808e+01, 4.541436e+01, 0.1)
points.SetPoint(106, -8.662794e+01, 4.565929e+01, 0.1)
points.SetPoint(107, -8.417806e+01, 4.596917e+01, 0.1)
points.SetPoint(108, -8.418808e+01, 4.623495e+01, 0.1)
points.SetPoint(109, -8.492193e+01, 4.646996e+01, 0.1)
points.SetPoint(110, -8.634989e+01, 4.657803e+01, 0.1)
points.SetPoint(111, -8.819985e+01, 4.694650e+01, 0.1)
points.SetPoint(112, -8.823216e+01, 4.714597e+01, 0.1)
points.SetPoint(113, -9.090001e+01, 4.666687e+01, 0.1)
points.SetPoint(114, -9.163785e+01, 4.673756e+01, 0.1)
points.SetPoint(115, -9.002129e+01, 4.782012e+01, 0.1)
points.SetPoint(116, -9.342548e+01, 4.860130e+01, 0.1)
points.SetPoint(117, -9.515342e+01, 4.924999e+01, 0.1)
points.SetPoint(118, -1.225507e+02, 4.900226e+01, 0.1)
points.SetPoint(119, -1.226638e+02, 4.841517e+01, 0.1)
points.SetPoint(120, -1.223272e+02, 4.738772e+01, 0.1)
points.SetPoint(121, -1.228245e+02, 4.740474e+01, 0.1)
points.SetPoint(122, -1.227146e+02, 4.776828e+01, 0.1)
points.SetPoint(123, -1.226873e+02, 4.783100e+01, 0.1)
points.SetPoint(124, -1.247328e+02, 4.816533e+01, 0.1)

lines0 = vtk.vtkCellArray()
lines0.InsertNextCell(126)
lines0.InsertCellPoint(0);
lines0.InsertCellPoint(1);
lines0.InsertCellPoint(2);
lines0.InsertCellPoint(3);
lines0.InsertCellPoint(4);
lines0.InsertCellPoint(5);
lines0.InsertCellPoint(6);
lines0.InsertCellPoint(7);
lines0.InsertCellPoint(8);
lines0.InsertCellPoint(9);
lines0.InsertCellPoint(10);
lines0.InsertCellPoint(11);
lines0.InsertCellPoint(12);
lines0.InsertCellPoint(13);
lines0.InsertCellPoint(14);
lines0.InsertCellPoint(15);
lines0.InsertCellPoint(16);
lines0.InsertCellPoint(17);
lines0.InsertCellPoint(18);
lines0.InsertCellPoint(19);
lines0.InsertCellPoint(20);
lines0.InsertCellPoint(21);
lines0.InsertCellPoint(22);
lines0.InsertCellPoint(23);
lines0.InsertCellPoint(24);
lines0.InsertCellPoint(25);
lines0.InsertCellPoint(26);
lines0.InsertCellPoint(27);
lines0.InsertCellPoint(28);
lines0.InsertCellPoint(29);
lines0.InsertCellPoint(30);
lines0.InsertCellPoint(31);
lines0.InsertCellPoint(32);
lines0.InsertCellPoint(33);
lines0.InsertCellPoint(34);
lines0.InsertCellPoint(35);
lines0.InsertCellPoint(36);
lines0.InsertCellPoint(37);
lines0.InsertCellPoint(38);
lines0.InsertCellPoint(39);
lines0.InsertCellPoint(40);
lines0.InsertCellPoint(41);
lines0.InsertCellPoint(42);
lines0.InsertCellPoint(43);
lines0.InsertCellPoint(44);
lines0.InsertCellPoint(45);
lines0.InsertCellPoint(46);
lines0.InsertCellPoint(47);
lines0.InsertCellPoint(48);
lines0.InsertCellPoint(49);
lines0.InsertCellPoint(50);
lines0.InsertCellPoint(51);
lines0.InsertCellPoint(52);
lines0.InsertCellPoint(53);
lines0.InsertCellPoint(54);
lines0.InsertCellPoint(55);
lines0.InsertCellPoint(56);
lines0.InsertCellPoint(57);
lines0.InsertCellPoint(58);
lines0.InsertCellPoint(59);
lines0.InsertCellPoint(60);
lines0.InsertCellPoint(61);
lines0.InsertCellPoint(62);
lines0.InsertCellPoint(63);
lines0.InsertCellPoint(64);
lines0.InsertCellPoint(65);
lines0.InsertCellPoint(66);
lines0.InsertCellPoint(67);
lines0.InsertCellPoint(68);
lines0.InsertCellPoint(69);
lines0.InsertCellPoint(70);
lines0.InsertCellPoint(71);
lines0.InsertCellPoint(72);
lines0.InsertCellPoint(73);
lines0.InsertCellPoint(74);
lines0.InsertCellPoint(75);
lines0.InsertCellPoint(76);
lines0.InsertCellPoint(77);
lines0.InsertCellPoint(78);
lines0.InsertCellPoint(79);
lines0.InsertCellPoint(80);
lines0.InsertCellPoint(81);
lines0.InsertCellPoint(82);
lines0.InsertCellPoint(83);
lines0.InsertCellPoint(84);
lines0.InsertCellPoint(85);
lines0.InsertCellPoint(86);
lines0.InsertCellPoint(87);
lines0.InsertCellPoint(88);
lines0.InsertCellPoint(89);
lines0.InsertCellPoint(90);
lines0.InsertCellPoint(91);
lines0.InsertCellPoint(92);
lines0.InsertCellPoint(93);
lines0.InsertCellPoint(94);
lines0.InsertCellPoint(95);
lines0.InsertCellPoint(96);
lines0.InsertCellPoint(97);
lines0.InsertCellPoint(98);
lines0.InsertCellPoint(99);
lines0.InsertCellPoint(100);
lines0.InsertCellPoint(101);
lines0.InsertCellPoint(102);
lines0.InsertCellPoint(103);
lines0.InsertCellPoint(104);
lines0.InsertCellPoint(105);
lines0.InsertCellPoint(106);
lines0.InsertCellPoint(107);
lines0.InsertCellPoint(108);
lines0.InsertCellPoint(109);
lines0.InsertCellPoint(110);
lines0.InsertCellPoint(111);
lines0.InsertCellPoint(112);
lines0.InsertCellPoint(113);
lines0.InsertCellPoint(114);
lines0.InsertCellPoint(115);
lines0.InsertCellPoint(116);
lines0.InsertCellPoint(117);
lines0.InsertCellPoint(118);
lines0.InsertCellPoint(119);
lines0.InsertCellPoint(120);
lines0.InsertCellPoint(121);
lines0.InsertCellPoint(122);
lines0.InsertCellPoint(123);
lines0.InsertCellPoint(124);
lines0.InsertCellPoint(0);

polygon0 = vtk.vtkPolyData()
polygon0.SetPoints(points)
polygon0.SetLines(lines0)


polygonMapper0 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper0.SetInputConnection(polygon0.GetProducerPort())
else:
	polygonMapper0.SetInputData(polygon0)
	polygonMapper0.Update()
polygonActor0 = vtk.vtkActor()
polygonActor0.SetMapper(polygonMapper0)
polygonActor0.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor0.GetProperty().SetLineWidth(widthLine)


