cp init.intensity.txt work.txt
sed -i '1d' work.txt

# surf=50*3.14*400*400
# surf=50*3.14*0.4*0.4
surf=2512

awk '{print $4}' work.txt > C0.txt
awk '{print $3}' work.txt > HA0.txt
awk '{print $5}' work.txt > HB0.txt
awk '{print $6}' work.txt > HC0.txt


awk '{print "P2dtm10[]["NR-1"]="$1/2512";"}' C0.txt > C0.edp
awk '{print "A2dtm10[]["NR-1"]="$1/2512";"}' HA0.txt > HA0.edp
awk '{print "B2dtm10[]["NR-1"]="$1/2512";"}' HB0.txt > HB0.edp
awk '{print "C2dtm10[]["NR-1"]="$1/2512";"}' HC0.txt > HC0.edp

