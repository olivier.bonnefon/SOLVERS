import numpy
from matplotlib.pyplot import subplot, title, plot, grid, show, ylim, xlim,gca,savefig,clf
from array import array
import os

execfile("vtktopngGen.py")
os.chdir(OUTPUTDIR+"/D1");
maxs = numpy.zeros((2,nbOmega))
baseName=OUTPUTDIR+"/D1/"
for numStep in range(1,nbStep):
    for numOmega in range(0,nbOmega):
        numSpec=0
        for spec in species:
            Name2=baseName+spec+"1d"+str(numStep*step)+"s"+str(numOmega)+".txt"
            res=numpy.loadtxt(Name2)
            if (max(res)>maxs[numSpec][numOmega]):
                maxs[numSpec][numOmega]=max(res)
            numSpec=numSpec+1
                

for numStep in range(1,nbStep):
    for numOmega in range(0,nbOmega):
        numSpec=0
        for spec in species:
            Name2=baseName+spec+"1d"+str(numStep*step)+"s"+str(numOmega)+".txt"
            res=numpy.loadtxt(Name2)
            subplot(2,1,numSpec+1)
            ylim([-0.1,0.1+maxs[numSpec][numOmega]])
            numSpec=numSpec+1
            plot(res[:])
            grid()
        #show()
        savefig(baseName+spec+"1d"+str(numStep*step)+"s"+str(numOmega)+".jpg")
        clf()

for numOmega in range(0,nbOmega):
    Name2=baseName+"input"+str(numOmega)+".txt"
    f = open(Name2,'w')
    for numStep in range(1,nbStep):
        f.write(baseName+spec+"1d"+str(numStep*step)+"s"+str(numOmega)+".jpg\n")
    f.close()

for numOmega in range(0,nbOmega):
    Name2="input"+str(numOmega)+".txt"
    os.system("mencoder \"mf://@"+Name2+"\" -mf fps=20 -o test"+str(numOmega)+".avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800")
#show()
