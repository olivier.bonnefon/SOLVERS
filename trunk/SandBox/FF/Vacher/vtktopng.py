import vtk
import os
from array import array
#baseName="/home/olivierb/VTK_PYTHON/diff/u"
species=array('c','PABC')

#numStep=1998
#for num in range(1,150):
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(500,500)
nbStep=50
step=20
nbOmega=2
WinSize=500
firstStep=1
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(WinSize,WinSize)
rgb=[0,0,0]
execfile("vtktopngGen.py")
os.chdir(OUTPUTDIR+"/D2");
finput = open("input.txt", 'w')
INTCURSPEC=0
INTCURSTEP=firstStep
if (os.path.isfile('curspec.txt')):
    fcurspec = open("curspec.txt", 'r')
    for line in fcurspec:
        for x in line.split():
            INTCURSPEC=int(x)
    fcurspec.close()

if (os.path.isfile('curstep.txt')):
    fcurstep = open("curstep.txt", 'r')
    for line in fcurstep:
        for x in line.split():
            INTCURSTEP=int(x)-1
    fcurstep.close();

print "CURSPEC "+str(INTCURSPEC)
print "CURSTEP "+str(INTCURSTEP)

numSpec=0
for spec in species:
   if (numSpec < INTCURSPEC):
        print "pass spec "+str(numSpec)
        numSpec=numSpec+1
        continue
   baseName=OUTPUTDIR+"/D2/"+spec
   for numStep in range(INTCURSTEP,nbStep+1):
       fenetre=vtk.vtkRenderWindow()
       fenetre.SetSize(WinSize,WinSize)
       ren=vtk.vtkRenderer()
       fenetre.AddRenderer(ren)
       execfile("../buildActorWires.py")
       execfile("../addActorWires.py")
    #execfile("buildActorWires2.py")
    #execfile("addActorWires2.py")
    #execfile("buildActorWires1.py")
    #execfile("addActorWires1.py")
       ren.SetBackground(0,0,0)
       for numOmega in range(0,nbOmega):
           fileName=baseName+str(numOmega)+"s"+str(numStep*step)+".vtk"
           r=vtk.vtkGenericDataObjectReader()
           r.SetFileName(fileName)
           r.Update()
           o=r.GetOutput()
           o.GetCellData().SetActiveScalars("UU")
           aPolyVertexMapper = vtk.vtkDataSetMapper()
           aPolyVertexMapper.SetInput(o)
           aPolyVertexMapper.SetScalarRange(minV[numSpec],maxV[numSpec])
           aPolyVertexMapper.UseLookupTableScalarRange=1
           lut=aPolyVertexMapper.GetLookupTable()
           lut.SetHueRange(251.0/360.0,0)
           lut.SetSaturationRange(1,1)
           lut.SetValueRange(1,1)
           lut.Build()
           aPolyVertexActor = vtk.vtkActor()
           aPolyVertexActor.SetMapper(aPolyVertexMapper)
           ren.AddActor(aPolyVertexActor)
           atext = vtk.vtkVectorText()
           atext.SetText(str(spec))
           textMapper = vtk.vtkPolyDataMapper()
           textMapper.SetInputConnection(atext.GetOutputPort())
           textActor = vtk.vtkFollower()
           textActor.SetMapper(textMapper)
           textActor.SetScale(scaleLegend,scaleLegend ,scaleLegend )
           textActor.AddPosition(-116, 25, 0)
           ren.AddActor(textActor)
    
    #iren=vtk.vtkRenderWindowInteractor()
    #iren.SetRenderWindow(fenetre)
    #iren.Initialize()
       fenetre.Render()
       image = vtk.vtkWindowToImageFilter()
       image.SetInput(fenetre)
       image.Update()
       fenetre.Start()
       fenetre.Render()
       fenetre.Finalize()
       image.UpdateInformation()
       image.Update()
    #iren.Render()
    #iren.Render()
    #iren.Render()
       jpg = vtk.vtkJPEGWriter()
       fileStepName="step"+spec+str(numStep)+".jpg"
       jpg.SetFileName(fileStepName)
       jpg.SetInputConnection(image.GetOutputPort())
       jpg.Write()
       fcurstep = open("curstep.txt", 'w')
       fcurstep.write(str(numStep))
       fcurstep.close()
  
   numSpec+=1
   INTCURSTEP=firstStep
   fcurspec = open("curspec.txt", 'w')
   fcurspec.write(str(numSpec))
   fcurspec.close()

for numStep in range(firstStep,nbStep+1):
    jpgfiles=""
    for spec in species:
        fileStepName="step"+spec+str(numStep)+".jpg"
        jpgfiles=jpgfiles+" "+fileStepName
    os.system("convert "+jpgfiles+" "+"+append step"+str(numStep)+".jpg")
    finput.write("step"+str(numStep)+".jpg"+"\n")
    #iren.Start()

finput.close()
#os.system("mencoder \"mf://@input.txt\" -mf fps=20 -o test.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800")
print "mencoder \"mf://@input.txt\" -mf fps=20 -o test.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800"
#os.system("vlc test.avi")
   


