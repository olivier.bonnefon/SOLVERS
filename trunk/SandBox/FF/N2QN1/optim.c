#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include <time.h>
#define DEBUG_USING_N2QN1


extern "C"
{
  void n2qn1_(int* n, double* x, double* f, double* g, double* dxmin, double* df1, double* epsabs, int* imp,
	     int *io,int* mode, int* iter, int * nsim, double* binf, double* bsup, int* iz, double* rz, int * reverse);
  void nqhess_(int* n,int* imp,int *io,int* iz,double* rz);
}

void evalFandGradF(int n, double * X, double * FX, double * dFX){
  int ret,i;
  double aux;
  FILE *fileParam=fopen( "defOptimParamsGen.edp","w");
  for(i=0;i<n;i++)
    fprintf(fileParam,"POptim[%i]=%.15e;\n",i,X[i]);
  fclose(fileParam);
//  ret=system ("/home/biometrie/commun_biom/CLUSTER/BUILD/archi2/freefem++-3.38/src/nw/FreeFem++-nw callJ.edp ");
  ret=system ("/home/olivierb/SOFT/freefem++-3.50//src/nw/FreeFem++-nw callJ.edp ");
  
  FILE *fileRes=fopen( "resFdF.txt","r");
  
  fscanf(fileRes,"%lf",&aux);
  *FX=aux;
  
  for (i=0;i<n;i++){
    fscanf(fileRes,"%lf",&aux);
    dFX[i]=aux;
  }
}

int main(int argc, char **argv){
  int i,noptim;
  char fname[256];
  FILE *optimParam=fopen( "optimParam.txt","r");
  int n = 2;
  fscanf(optimParam,"%d",&n);
  double precision;
  fscanf(optimParam,"%lf",&precision);
  int sizeD=
    /*x*/n+
    /*dxmin*/n+
    /*df1*/n+
    /*binf*/n+
    /*bsup*/n+
    /*rz*/n*(n+9)/2 +
    /*because fortran ?*/1;
  double * workZoneD=(double*)malloc(sizeD*sizeof(double));
  int sizeI= 2*n+1 +1;
  int * workZoneI=(int*)malloc(sizeI*sizeof(int));
  
  double *pInWZ=workZoneD+1;
  
  double * x = pInWZ;
  pInWZ +=n;
  double  f =0;
  double * g =pInWZ;
  pInWZ+=n;
  double * dxim = pInWZ;
  pInWZ+=n;
  double  df1 =0;
  double  epsabs_ori=1e-18;
  double  epsabs=epsabs_ori;
  double *binf= pInWZ;
  pInWZ+=n;
  double *bsup= pInWZ;
  pInWZ+=n;
  double * rz = pInWZ;
  
  
  int * iz =workZoneI+1;
  noptim=0;
  
  for (i=0;i<n;i++){
    fscanf(optimParam,"%lf",binf+i);
    fscanf(optimParam,"%lf",bsup+i);
    dxim[i]=precision*(bsup[i]-binf[i]);
  }
  if (argc>1){
    noptim=atoi(argv[1]);
srand(time(NULL)); 
    for ( i=0; i<n;i++)
      x[i]=binf[i]+(rand()*(bsup[i]-binf[i]))/RAND_MAX;
  }else{
    for (i=0;i<n;i++){
      fscanf(optimParam,"%lf",x+i);
    }
  }
  fclose(optimParam);
  sprintf(fname,"trajOptim%i.txt",noptim);
  FILE *trajFile=fopen(fname,"w");
  evalFandGradF(n,x,&f,g);
  df1=f;    
  int mode =1;
  int imp=3;
  int io=(noptim)*1000+10;
  int iter=500;
  int nsim=3*iter;
  int reverse=1;
#ifdef DEBUG_USING_N2QN1
    printf("call n2qn1_: n=%d,fx=%e \n  dxim[0]=%e,dxim[1]=%e,dxim[2]=%e,dxim[3]=%e,epsabs=%e,imp=%d,io=%d,mode=%d,iter=%d,nsim=%d \n binf[0]=%e,binf[1]=%e,binf[2]=%e,binf[3]=%e \n bsup[0]=%e,bsup[1]=%e,bsup[2]=%e,bsup[3]=%e \n sizeD=%d,sizeI=%d\n",n,f,dxim[0],dxim[1],dxim[2],dxim[3],epsabs,imp,io,mode,iter,nsim,binf[0],binf[1],binf[2],binf[3],bsup[0],bsup[1],bsup[2],bsup[3],sizeD,sizeI);
    for ( i=0; i<n;i++){
      printf("x[%i]=%e\n",i,x[i]);
    }
    for ( i=0; i<n;i++){
      printf("g[%i]=%e\n",i,g[i]);
    }
#endif
    
    for ( i=0; i<n;i++) fprintf(trajFile,"%e\t",x[i]);
    for ( i=0; i<n;i++) fprintf(trajFile,"%e\t",g[i]);
    fprintf(trajFile,"%e\t",f);
    n2qn1_(&n, x, &f, g, dxim, &df1, &epsabs, &imp, &io,&mode, &iter, &nsim, binf, bsup, iz, rz, &reverse);
    io++;
    fprintf(trajFile,"%e\t",epsabs);
    fprintf(trajFile,"%d\n",mode);
    fclose(trajFile);

    while(mode > 7){
      trajFile=fopen(fname,"a");
      evalFandGradF(n,x,&f,g);
      for ( i=0; i<n;i++) fprintf(trajFile,"%e\t",x[i]);
      for ( i=0; i<n;i++) fprintf(trajFile,"%e\t",g[i]);
      fprintf(trajFile,"%e\t",f);
      epsabs=epsabs_ori;
	
      n2qn1_(&n, x, &f, g, dxim, &df1, &epsabs, &imp, &io,&mode, &iter, &nsim, binf, bsup, iz, rz, &reverse);
      io++;
      fprintf(trajFile,"%e\t",epsabs);
      fprintf(trajFile,"%d\n",mode);
      fclose(trajFile);

    }
    nqhess_(&n,&imp, &noptim,iz, rz);
#ifdef DEBUG_USING_N2QN1
    printf("mode=%d and min value at  f=%e\n",mode,f);
    printf("nsim=%d \n",nsim);
    for (int i=0; i<n;i++){
      printf("x[%i]=%e\n",i,x[i]);
    }
    for (int i=0; i<n;i++){
      printf("g[%i]=%e\n",i,g[i]);
    }
    
    printf("optimal = %e\n",f);
#endif
  
}
