///////////////////////////////////////////
//////////////////matrices declaration
//masse matrix
real[int,int] Mtab(n1Ddofs,n1Ddofs);

//Kernel matrices
real[int,int] Ktab(n1Ddofs,n1Ddofs);
real[int,int] MtabKtab(n1Ddofs,n1Ddofs); 
real[int,int] MtabKtabMtab(n1Ddofs,n1Ddofs);
// Non locale diffusion matrix (comparable to the laplacien matrix)
real[int,int] NonLocalDiffTab(n1Ddofs,n1Ddofs);
NonLocalDiffTab=0;
//matrix of the non-local formulation
real[int,int] NonLocaltab(n1Ddofs,n1Ddofs);
  
//buffer matrix
real[int,int] Buftab(n1Ddofs,n1Ddofs);
real[int,int] MLocaltab(n1Ddofs,n1Ddofs);

macro buildKernelMatrix
{
  Ktab=0.0;
  for (int ii=0;ii<n1Ddofs;ii++){
    for (int jj=0;jj<n1Ddofs;jj++){
      real diff=pt(ii)-pt(jj);
      Ktab(ii,jj)=JKernel(diff);
    }
  }
}//

macro buildMassMatrix
{
  matrix M;
  {
    varf uv(u,v)=int1d(Th,1)(u*v);
    buildM(uv,M);
  }
  Mtab=0;
  {
    int[int] I(1),J(1); real[int] A(1);
    [I,J,A]=M;
    for (int k=0;k<I.n;k++)
    {
      Mtab(I[k],J[k])=A[k];
    }
  }
}//

macro buildMKM
{
  MtabKtab=Mtab*Ktab;
  MtabKtabMtab=MtabKtab*Mtab;
}//


//Gauss kernel
real jgaussAlpha=1.0;

func real Jgauss(real d){
  real sqrtd=sqrt(jgaussAlpha/3.14159265);
  return sqrtd*exp(-jgaussAlpha*d*d);
}

//////////KERNELS fonctions
//FAT exp


func real JfatExpSqrt(real d){
  real sqrtd=sqrt(abs(d)+1);
  return (0.25/sqrtd)*exp(-sqrtd+1);
}


func real SIDfatExpSqrt(real t){
  real sqrtd=sqrt(abs(t-x0)+1);
  return 0.5*exp(-sqrtd+1);
}

//FAT
real jbeta=1;
func real Jfat(real d){
  return (jbeta/3.14159265)/(1+(jbeta*d)*(jbeta*d));
}

func real SIDfat(real t){
  real res=0.0;
  res=0.5+(1.0/3.14159265)*atan(jbeta*(x0-t));
  return res;
}

//EXPO
real jalpha=1;
real cstart=0;
real c0=3*sqrt(3)/(2.0*jalpha);
real css=sqrt(3)*((1+aAllee)*(1+aAllee) +aAllee*aAllee)/(2.0*jalpha*aAllee);
real ctarget = css-5;
real lambdatarget=(2*jalpha/sqrt(3))*cos((acos(c0/ctarget)+3.14159265)/3.0);

func real Jexp(real d){
  return (jalpha/2)*exp(-jalpha*abs(d));
}
func real SIDexp(real t){
  real res=0.0;
  res= 0.5*exp(jalpha*(x0-t));
  return res;
}

//
//
//
// 1/(iDiracdx)                    _
//
//
//
//
//
//
//
//
//
//                 /                             \
//                /
//               /
//              /                                   \
//             /
//            /                                       \
//           /                                         \
//0_________/___________________________________________\_____________
//       iDirac*dx    ..   -dx       0       dx    ..    iDirac*dx
//
//
//
int iDirac=25;
real radiusDirac=iDirac*StepX;
func real Jdirac(real d){
  real res=0.0;
  real factor=1.0/(radiusDirac);
  if (abs(d)<0.1*StepX)
    res=1.0*factor;
  for (int i=1;i<iDirac;i++){
    if (abs(d-i*StepX)<0.1*StepX || abs(d+i*StepX)<0.1*StepX){
      real fd=iDirac;
      real fi=i;
      res=((fd-fi)/fd)*factor;
    }
  }
  return res;
}


//the source terme due to the :
  //
  //
  // SID(X)= int1D_{-inf}^{x0}(J(X-y))dy
  //  z=X-y
  //       = int1D_{-inf}^{}(J(z)dz
  //
  // It assumes left Diriclhet boundary conditions equals 1.
  //
  func real SIDdirac(real t){
    real res=0.0;
    real precision = StepX*0.2;
    //
    if (t+precision>(x0+radiusDirac)){
      res=0.0;
    }
    if (t-precision<x0-radiusDirac){
      res=1.0;
    }
    if (t+precision>x0 && (t-precision<x0+radiusDirac)){
      res=(t-(x0+radiusDirac))*(t-(x0+radiusDirac))/(2.0*radiusDirac*radiusDirac);
    }
    if (t-precision<x0 && (t-precision>x0-radiusDirac)){
      res=0.5+(t-x0)*(t-x0)/(2.0*radiusDirac*radiusDirac);
    }
    return res;
  }

func real Jsimple(real d){
  real res=0.0;
  real factor=1.0/(1.0*StepX);
  if (abs(d)<0.1*StepX)
    res=1.0*factor;
  // if (abs(d)<0.1*StepX)
  //   res=3.0*factor;
  
  return res;
}
 


 
func real SIDsimple(real t){
  real res=0.0;
  real precision = StepX*0.2;
  if (t>(x0+StepX)){
    res=0.0;
  }
  if (t<x0-StepX){
    res=1.0;
  }
  if (t+precision>x0 && (t-precision<x0+StepX)){
    res=(t-(x0+StepX))*(t-(x0+StepX))/(2.0*StepX*StepX);
  }
  if (t-precision<x0 && (t+precision<x0-StepX)){
    res=0.5+(t-x0)*(t-x0)/(2.0*StepX*StepX);
  }
  return res;
}
//Assume lTabLin is built from the linear part.
//Assume luold and lpu1d contain utm1, user charge before the first call. After, it is ensure by the algo.
//
//
//xxold are newton iter.
//
//
//
//
macro newtonItNonLinSeparated(lTabLin,lTabrhsLin,lrhsLin,lTabbuf,lTabrhs,lmatNonLin,lrhsNonLin,lpu1dold,lcriteron,lutm1,luold,lpu1d)
{
  lutm1=luold;
  lpu1dold=lpu1d;
  buildRHS(lrhsLin,lTabrhsLin);
  for (int k=0; k<=13; k++){
    lTabbuf=lTabLin;
    lTabrhs=lTabrhsLin;
    buildMflag(lmatNonLin,MM,0);
    buildRHS(lrhsNonLin,rhs1dNonLin);
    {
      int[int] I(1),J(1); real[int] A(1);
      [I,J,A]=MM;
      for (int k=0;k<I.n;k++)
      {
        lTabbuf(I[k],J[k])+=A[k];
      }
    }
    lTabrhs+=rhs1dNonLin;
    mydgesv(lTabbuf,lTabrhs,lpu1d);
    udiff1d = lpu1dold-lpu1d;
    if (udiff1d.max < lcriteron && udiff1d.min > - lcriteron){
      cout<<"NonLocal CV du problem non-linaire: "<<udiff1d.max<<" "<<udiff1d.min<<" CV criteron OK at it="<<k<<endl;
      lpu1dold=lpu1d;
      k=14;
    }
    lpu1dold=lpu1d;
    convert1Dto2D(lpu1d,luold);
  }
}//

string SUFFIX(mygetpid());

func real writeinfo(ofstream &File)
{
  if (KERNELTYPE==KERNELDIRAC){
    File<<"kernel dirac"<<endl;
    File<<"iDirac"<<iDirac<<endl;
  }
  if (KERNELTYPE==KERNELEXP){
    File<<"kernel exp"<<endl;
    File<<"jalpha "<<jalpha<<endl;
  }
  if (KERNELTYPE==KERNELFAT){
    File<<"kernel fat"<<endl;
    File<<"jbeta "<<jbeta<<endl;
  }
  if (KERNELTYPE==KERNELFATEXPSQRT){
    File<<"kernel fat 0.25*exp^{-sqrt(abs(d)+1)+1}/sqrt(..)"<<endl;
    File<<"jbeta "<<jbeta<<endl;
  }
  if (FTYPE==FKPP){
    File<<"kpp"<<endl;
  }
  if (FTYPE==FALLEE){
    File<<"Allee u(1-u)(1+au) with aAllee="<<aAllee<<endl;
    File<<"cstart="<<cstart<<" c0="<<c0<<endl;
    if (KERNELTYPE==KERNELEXP){
      File<<"c0="<<c0<<endl;
      File<<"css="<<css<<endl;
      File<<"ctarget="<<ctarget<<endl;
      File<<"lambdatarget="<<lambdatarget<<endl;
    }
  }
  if (FTYPE==FBISTABLE){
    File<<"f bistable "<<"rho "<<rho<<endl;
  }
  if (FTYPE==FEMPTY){
    File<<"f empty"<<endl;
  }
  File<<"SUFFIX "<<SUFFIX<<endl;
  if (ARGV.n > 2)
   File<<"PID "<<ARGV[2]<<endl;

  File<<"nbSignals "<<nbSignals<<endl;
  File<<"feqOutput "<<feqOutput<<endl;
  
  File<<"nbStep "<<nbSteps<<endl;
  File<<"dt "<<dt<<endl;
  File<<"x0 "<<x0<<" x1 "<<x1<<" m "<<m<<" StepX "<<StepX<<endl;
  File<<"XU0 "<<XU0<<endl;
  File<<"eDiff "<<eDiff<<" eDiff "<<eDiffID<<endl;
  return 0.0;
}
