// file diffusion.edp
real DXeval=0.5;
real DX=DXeval;
real DYeval=0.5;
real DY=DYeval;
real dt=0.02;
real alpha=1/dt;
int NSteps=5;
int numStep=0;
int numDof;
real epsilonSav=0.001;
real epsilon=0.001;
real x0=-5,x1=5;
real y0=-5,y1=5;
int n=50,m=50;
mesh Th=square(n,m,[x0+(x1-x0)*x,y0+(y1-y0)*y]);

fespace Vh(Th,P1);
func real defIC(real s, real t){
  return exp(-((s*s+t*t)/3)^2);
}

Vh phi,w;

Vh u0=defIC(x,y);
plot(u0,cmm="u0",wait=true,fill=true,value=true);
Vh utm1=u0;
Vh wxtm1=0;
Vh wytm1=0;
Vh u;
int nDofs=Vh.ndof;
real[int] rhs(nDofs);
real[int] sol(nDofs);
real[int] UU(nDofs*NSteps);
real[int] VV(nDofs*NSteps);
real[int] WWX(nDofs*NSteps);
real[int] WWY(nDofs*NSteps);
real[int] DIFF(nDofs*NSteps);
real[int] DELTA(nDofs*NSteps);

macro visuT(XX)
{
  for (numStep=0;numStep<NSteps;numStep++){
    string mess="DX="+DX+" DY="+DY+" step="+numStep;
    for (numDof=0;numDof<nDofs;numDof++){
      phi[][numDof]=XX[nDofs*numStep+numDof];
    }
    plot(phi,wait=true, value=true, fill=true,cmm=mess);
  }
}//

real ddxDx=1;
real ddxDy=0;
real ddyDx=0;
real ddyDy=1;

varf varfMat(v,w)=int2d(Th)(DX*dx(v)*dx(w)+DY*dy(v)*dy(w)+	alpha*v*w);

varf varfRhs(v,w)= int2d(Th)((alpha*utm1)*w);

/*Simulation de uref*/
DX=DXeval;
DY=DYeval;
utm1=u0;
for (numStep=0;numStep<NSteps;numStep++){
  matrix M=varfMat(Vh,Vh);
  rhs=varfRhs(0,Vh);
  set(M,solver=UMFPACK);
  sol=M^-1*rhs;
  phi[]=sol;
  for (numDof=0;numDof<nDofs;numDof++){
    VV[nDofs*numStep+numDof]=phi[][numDof];
  }
  utm1=phi;
//  v=phi;
}
visuT(VV);


varf rhsddx(v,w)= int2d(Th)((alpha*wxtm1)*w - ddxDx*dx(phi)*dx(w)-ddxDy*dy(phi)*dy(w));
varf rhsddy(v,w)= int2d(Th)((alpha*wytm1)*w - ddyDx*dx(phi)*dx(w)-ddyDy*dy(phi)*dy(w));

/*Simulation de uref et wx*/
DX=DXeval;
DY=DYeval;
utm1=u0;
wxtm1=0;
for (numStep=0;numStep<NSteps;numStep++){
  matrix M=varfMat(Vh,Vh);
  rhs=varfRhs(0,Vh);
  set(M,solver=UMFPACK);
  sol=M^-1*rhs;
  phi[]=sol;
  utm1=phi;
  rhs=rhsddx(0,Vh);
  sol=M^-1*rhs;
  phi[]=sol;
  for (numDof=0;numDof<nDofs;numDof++){
    WWX[nDofs*numStep+numDof]=sol[numDof];
  }
  wxtm1=phi;
}
visuT(WWX);

/*Simulation de uref et wy*/
DX=DXeval;
DY=DYeval;
utm1=u0;
wytm1=0;
for (numStep=0;numStep<NSteps;numStep++){
  matrix M=varfMat(Vh,Vh);
  rhs=varfRhs(0,Vh);
  set(M,solver=UMFPACK);
  sol=M^-1*rhs;
  phi[]=sol;
  utm1=phi;
  rhs=rhsddy(0,Vh);
  sol=M^-1*rhs;
  phi[]=sol;
  for (numDof=0;numDof<nDofs;numDof++){
    WWY[nDofs*numStep+numDof]=sol[numDof];
  }
  wytm1=phi;
  plot(phi,wait=true, value=true, fill=true,cmm="wy "+string(numStep));
}
visuT(WWY);

/*simulation de dx numerique*/
{
  ofstream ffx("convGradX.txt");
}

ofstream ffx("convGradX.txt");ffx.precision(15);
for (int numEpsilon=0;numEpsilon<6;numEpsilon++){
  //calcul de F(D+epsilon) ds UU et de [F(D+epsilon)-F(D)]/epsilon ds DIFF 
  DX=DXeval+epsilon;
  utm1=u0;
  for (numStep=0;numStep<NSteps;numStep++){
    matrix M=varfMat(Vh,Vh);
    rhs=varfRhs(0,Vh);
    set(M,solver=UMFPACK);
    sol=M^-1*rhs;
    phi[]=sol;
    for (numDof=0;numDof<nDofs;numDof++){
      UU[nDofs*numStep+numDof]=phi[][numDof];
      DIFF[nDofs*numStep+numDof]=(UU[nDofs*numStep+numDof]-VV[nDofs*numStep+numDof])/epsilon;
    }
    utm1=phi;
  }
 
  DELTA=DIFF-WWX;
  DIFF=abs(DELTA);
  real aux=0;
  for (numStep=0;numStep<NSteps;numStep++){
    Vh delta;
    for (numDof=0;numDof<nDofs;numDof++){
      delta[][numDof]=DIFF[nDofs*numStep+numDof];
    }
    aux +=int2d(Th)(delta);
  }

  ffx<<epsilon<<" "<<aux<<endl;
  epsilon=epsilon/2;
  
}
DX=DXeval;
epsilon=epsilonSav;
/*simulation de dy numerique*/
{
  ofstream ffy("convGradY.txt");
}
ofstream ffy("convGradY.txt");ffy.precision(15);
for (int numEpsilon=0;numEpsilon<6;numEpsilon++){
  //calcul de F(D+epsilon) ds UU et de [F(D+epsilon)-F(D)]/epsilon ds DIFF 
  DY=DYeval+epsilon;
  utm1=u0;
  for (numStep=0;numStep<NSteps;numStep++){
    matrix M=varfMat(Vh,Vh);
    rhs=varfRhs(0,Vh);
    set(M,solver=UMFPACK);
    sol=M^-1*rhs;
    phi[]=sol;
    for (numDof=0;numDof<nDofs;numDof++){
      UU[nDofs*numStep+numDof]=phi[][numDof];
      DIFF[nDofs*numStep+numDof]=(UU[nDofs*numStep+numDof]-VV[nDofs*numStep+numDof])/epsilon;
    }
    utm1=phi;
  }
 
  DELTA=DIFF-WWY;
  DIFF=abs(DELTA);
  real aux=0;
  for (numStep=0;numStep<NSteps;numStep++){
    Vh delta;
    for (numDof=0;numDof<nDofs;numDof++){
      delta[][numDof]=DIFF[nDofs*numStep+numDof];
    }
    aux +=int2d(Th)(delta);
  }

  ffy<<epsilon<<" "<<aux<<endl;
  epsilon=epsilon/2;
  
}
