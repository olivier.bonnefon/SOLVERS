// SUMMARY  :   volume finis
// USAGE    : LGPL      
// ORG      : INRA
// AUTHOR   : O. BONNEFON
//


#include "ff++.hpp"
#include "AFunction_ext.hpp"
#define DEBUG

int NN;
int MM;
double *XX,*YY,*MX,*MY,*LX,*LY;

enum paramtype{
  XMIN_TYPE=1,
  XMAX_TYPE=2,
  YMIN_TYPE=3,
  YMAX_TYPE=4,
  DISCRET_TYPE=5,
  DISCRET_PARAM_TYPE=6
};


double xmin=0;
double xmax=1;
double ymin=0;
double ymax=1;
int discretType=0;
double disctretParam=0.05;

long setVolFiniParam(const long &paramtypevalue,const double &value){

  switch(paramtypevalue){
  case XMIN_TYPE:
    xmin=value;
    break;
  case XMAX_TYPE:
    xmax=value;
    break;
  case YMIN_TYPE:
    ymin=value;
    break;
  case YMAX_TYPE:
    ymax=value;
    break;
  case DISCRET_TYPE:
    discretType=lrint(value);
    break;
  case DISCRET_PARAM_TYPE:
    disctretParam=value;
    break;
  default:
    break;
  }
}

double Gx(int i) 
{
  return xmin+(xmax-xmin)*i*(1.0/NN);
}

double Gy(int i) 
{
  return ymin+(ymax-ymin)*i*(1.0/MM);
}


//4*i < NN --> x_i=i*0.25/NN
//4*i=NN --> x_i=0.25*0.25=0.625
//NN<4*i<=4NN
// -> a*i+c
// -> for i=NN    we want a*NN+c=1    ie a*4NN+4c=4 
// -> for 4*i=NN  we want a*i+c=0.625 ie a*NN +4c=0.25
//                  =>                   3NNa=3.75 => a=3.75/(3NN)     
//                  => 3.75/3+4c=0.25
//                  => c=(0.25-3.75/3)/4
//                  => 


double picewiseGx(int i) 
{
//4*i \in  0...4*NN
//
  
  if (4*i<NN){
    return xmin+(xmax-xmin)*i*(0.25/NN);
  }
  //
  double c=(0.25-3.75/3.0)/4.0;
  double a=3.75/(3.0*NN);
  return xmin+(xmax-xmin)*(a*i+c);
}

double picewiseGy(int i) 
{
  if (4*i<MM){
    return ymin+(ymax-ymin)*i*(0.25/MM);
  }
  //
  double c=(0.25-3.75/3.0)/4.0;
  double a=3.75/(3.0*MM);
  return ymin+(ymax-ymin)*(a*i+c);
}

long getIndexFromX(long *const &iX,const double &V){
  long res=0;
  for (res=0;res<NN-1;res++){
    if (V<XX[res+1]){
      *iX= res;
      return 0;
    }
  }
  *iX=NN-1;
  return 0;
}
long getIndexFromY(long *const &iY,const double &V){
  long res=0;
  for (res=0;res<MM-1;res++){
    if (V<YY[res+1]){
      *iY= res;
      return 0;
    }
  }
  *iY=MM-1;
  return 0;
}

long initQuad(const long &n,const long &m){
  NN=n;
  MM=m;
  XX=(double*)malloc((NN+1)*sizeof(double));
  MX=(double*)malloc(NN*sizeof(double));
  LX=(double*)malloc(NN*sizeof(double));
  YY=(double*)malloc((MM+1)*sizeof(double));
  MY=(double*)malloc(MM*sizeof(double));
  LY=(double*)malloc(MM*sizeof(double));
  if (discretType==0){
    for (int i=0;i<NN+1;i++){
      XX[i]=Gx(i);
    }
    for (int i=0;i<MM+1;i++){
      YY[i]=Gy(i);
    }
  }else if (discretType==1){
    for (int i=0;i<NN+1;i++){
      XX[i]=picewiseGx(i);
    }
    for (int i=0;i<MM+1;i++){
      YY[i]=picewiseGy(i);
    }
  }else{
    double dx0=disctretParam*1.0/NN;
    double alphax=(2*(1-NN*dx0))/(NN*(NN-1));
    double dy0=disctretParam*1.0/MM;
    double alphay=(2*(1-MM*dy0))/(MM*(MM-1));
    XX[0]=0;
    double curlx=dx0;
    for (int i=1;i<NN+1;i++){
      XX[i]=XX[i-1]+curlx;
      curlx+=alphax;
    }
    for (int i=0;i<NN+1;i++)
      XX[i]=xmin+(xmax-xmin)*XX[i];
    YY[0]=0;
    double curly=dy0;
    for (int i=1;i<MM+1;i++){
      YY[i]=YY[i-1]+curly;
      curly+=alphay;
    }
    for (int i=0;i<MM+1;i++)
      YY[i]=ymin+(ymax-ymin)*YY[i];
  }
  
#ifdef DEBUG
  printf("X:\n");
  for (int i=0;i<NN+1;i++)
    printf("%i\t%e\n",i,XX[i]);
  printf("Y:\n");
  for (int i=0;i<MM+1;i++)
    printf("%i\t%e\n",i,YY[i]);
#endif
  
  for (int i=0;i<NN;i++){
    MX[i]=0.5*(XX[i]+XX[i+1]);
    LX[i]=XX[i+1]-XX[i];
  }
  for (int i=0;i<MM;i++){ 
    MY[i]=0.5*(YY[i]+YY[i+1]);
    LY[i]=YY[i+1]-YY[i];
  }
#ifdef DEBUG
  printf("LX:\n");
  for (int i=0;i<NN;i++)
    printf("%i\t%e\n",i,LX[i]);
  printf("LY:\n");
  for (int i=0;i<MM;i++)
    printf("%i\t%e\n",i,LY[i]);
#endif
}

long resetQuad(){
  free(XX);free(YY);free(LX);free(LY);free(MX);free(MY);
}


long getLX(double *const &pLX,const long &n){
  *pLX=LX[n];
}
long getLY(double *const &pLY,const long &n){
  *pLY=LY[n];
}
long getMX(double *const &pMX,const long &n){
  *pMX=MX[n];
}
long getMY(double *const &pMY,const long &n){
  *pMY=MY[n];
}

long fillMat(Matrice_Creuse<double>* const &Av,Matrice_Creuse<double>* const &Ah) {
 
  MatriceMorse<double>* mAv = static_cast<MatriceMorse<double>*>(&(*Av->A));
  MatriceMorse<double>* mAh = static_cast<MatriceMorse<double>*>(&(*Ah->A));
  map< pair<int,int>, R> Avij;
  map< pair<int,int>, R> Ahij;

  //Le flux entrant provenant de la droite
  for (int j=0;j<MM;j++)
    for (int i=0;i<NN-1;i++){
      int ligne=i+j*NN;
      int col=i+1+j*NN;
      double mesureCell=LY[j]*LX[i];
      Avij[make_pair(ligne,col)]+=(LY[j])/mesureCell;
    }

  //Le flux sortant par la gauche
  for (int j=0;j<MM;j++)
    for (int i=0;i<NN;i++){
      int ligne=i+j*NN;
      int col=i+j*NN;
      double mesureCell=LY[j]*LX[i];
      Avij[make_pair(ligne,col)]+=-LY[j]/mesureCell;
    }

  //Le flux entrant provenant du haut
  for (int i=0;i<NN;i++)
    for (int j=0;j<MM-1;j++){
       int ligne=i+j*NN;
       int col=i+(j+1)*NN;
       double mesureCell=LY[j]*LX[i];
       Ahij[make_pair(ligne,col)]+=LX[i]/mesureCell;
    }

  //flux sortant par le bas
  for (int i=0;i<NN;i++)
    for (int j=0;j<MM;j++){
       int ligne=i+j*NN;
       int col=i+j*NN;
       double mesureCell=LY[j]*LX[i];
       Ahij[make_pair(ligne,col)]+=-LX[i]/mesureCell;
    }  
  mAv=new MatriceMorse<double>(NN*MM,NN*MM,Avij,false);
  Av->A=mAv;
  mAh=new MatriceMorse<double>(NN*MM,NN*MM,Ahij,false);
  Ah->A=mAh;
  
  
}

class Init {
    public:
        Init();
};

Init init;
Init::Init() {
  Global.Add("resetQuad", "(", new OneOperator0<long>(resetQuad));
  Global.Add("setVolFiniParam", "(", new OneOperator2_<long, const long , const double>(setVolFiniParam));
  Global.Add("initQuad", "(", new OneOperator2_<long, const long , const long>(initQuad));
  Global.Add("fillMat", "(", new OneOperator2_<long, Matrice_Creuse<double>* ,Matrice_Creuse<double>* >(fillMat));
  Global.Add("getLX", "(", new OneOperator2_<long,  double *, const long>(getLX));
  Global.Add("getLY", "(", new OneOperator2_<long,  double *, const long>(getLY));
  Global.Add("getMX", "(", new OneOperator2_<long,  double *, const long>(getMX));
  Global.Add("getMY", "(", new OneOperator2_<long,  double *, const long>(getMY));
  Global.Add("getIndexFromX", "(", new OneOperator2_<long,  long *, const double>(getIndexFromX));
  Global.Add("getIndexFromY", "(", new OneOperator2_<long,  long *, const double>(getIndexFromY));
  
}
