export FF_SRC=/home/olivierb/SOFT/freefem++-3.23/
export SOLVER_BIN_DIR=/home/biometrie/obonnefon/BUILD/NODEBUG/PC38/SOLVERS/LS


mpic++ -c -fPIC -g -m64 -fPIC  -mmmx -msse -msse2 -DDRAWING -DBAMG_LONG_LONG -DNCHECKPTR -fPIC -I$FF_SRC/examples++-load/ -I$FF_SRC/examples++-load/include 'fflapack.cpp'
mpic++ -shared -fPIC -g -m64 -fPIC -mmmx -msse -msse2 -DDRAWING -DBAMG_LONG_LONG -DNCHECKPTR -fPIC 'fflapack.o' -o fflapack.so -llapack -lblas 

mpic++ -c -fPIC -g -m64 -fPIC  -mmmx -msse -msse2 -DDRAWING -DBAMG_LONG_LONG -DNCHECKPTR -fPIC -I$FF_SRC/examples++-load/ -I$FF_SRC/examples++-load/include 'interfaceSolver.cpp'
mpic++ -shared -fPIC -g -m64 -fPIC -mmmx -msse -msse2 -DDRAWING -DBAMG_LONG_LONG -DNCHECKPTR -fPIC 'interfaceSolver.o' -o obInterfaceSolver.so -L$SOLVER_BIN_DIR -llapack -lblas -lSOLVERS


