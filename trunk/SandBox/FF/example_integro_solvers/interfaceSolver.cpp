/*
 * this file is the interface beteween Freefem++ and the SOLVERS library.
 *
 */


#include "ff++.hpp"
#include "RNM.hpp"
#include "AFunction_ext.hpp" // Extension of "AFunction.hpp" to deal with more than 3 parameters function
#include "../../../src/LS/SOLVERS_LS.h"
using namespace std;
#ifdef __LP64__
  typedef int intblas;
  typedef int integer;
#else
  typedef long intblas;
  typedef long integer;
#endif

typedef integer  logical;
typedef float   LAPACK_real;
typedef double   doublereal;
typedef logical  (* L_fp)();
typedef integer      ftnlen;

typedef complex<float> LAPACK_complex;
typedef complex<double> doublecomplex;
typedef void VOID; 
#define complex LAPACK_complex 
#define real LAPACK_real 

class Init { public:
  Init();
};


long mydgesv(KNM<double>* const&a,KN<double>* const&b,KN<double> * const&X) {
 
  integer info;
  integer  n= a->N();
  int *p=(int*)malloc(n*sizeof(int));
	integer one=1;
  double *pA=&(*a)(0,0);
  double *pB=&(*b)(0);
  double *pX=&(*X)(0);
  //SOLVERS_LS_DGESV(double *A,double *B,double *X,int n);
  info=SOLVERS_LS_DGESV(pA,pB,pX,n);
  if(info) cerr << " error:  dgesv_ "<< info << endl;
  return info;
}


long mysolver_init(KNM<double>* A){
    int N=A->N(); 
    int aS=SOLVERS_LS_CG_PREC_DIAG_SIZE_WORKD(N);
    return aS;
}
long mysolve(KNM<double>* const &A,KN<double>* const &X0,KN<double>* const &RHS,
             KN<double>* const &WorkD){
  double crit;
  
  
  intblas N=A->N();
  int nbIt=N/100;
  intblas m=A->M();
  int nbmax=N;
  double *a=&(*A)(0,0);
  double *prhs=&(*RHS)(0);
  double *pX0=&(*X0)(0);
  
  double * pWorkD=&(*WorkD)(0);
  crit=pWorkD[0];
  SOLVERS_LS_PREC_DIAG_CG(a,prhs,pX0,pWorkD,N,crit,nbmax,&nbIt);
  cout<<"crit="<<crit<<", mysolve nb it of CG:"<<nbIt<<endl;
}
long mychecksol(KNM<double>* const &A,KN<double>* const &X0,KN<double>* const &RHS){
  intblas n=A->N();
  double *a=&(*A)(0,0);
  double *x=&(*X0)(0);
  double *b=&(*RHS)(0);
  double * bcopy=(double*)malloc(n*sizeof(double));
  memcpy(bcopy,b,n*sizeof(double));
  
  SOLVERS_LS_checkSolutionDense(a,bcopy,x,n,10e-9);
  free(bcopy);
}

LOADINIT(Init);  //  une variable globale qui serat construite  au chargement dynamique 

Init::Init(){  // le constructeur qui ajoute la fonction "splitmesh3"  a freefem++ 

//  if( map_type.find(typeid(Inverse<KNM<double >* >).name() ) == map_type.end() )
    {
      if(verbosity) 
	cout << " Add my interface solver ..." ;
      Global.Add("mysolverinit","(",new  OneOperator1<long,KNM<double>*>(mysolver_init));
      Global.Add("mydgesv","(",new  OneOperator3_<long,KNM<double>*,KN<double>*,KN<double>*>(mydgesv));  
      Global.Add("mysolve","(",new  OneOperator4_<long,KNM<double>*,KN<double>*,KN<double>*,KN<double>*>(mysolve));  
      Global.Add("mychecksol","(",new  OneOperator3_<long,KNM<double>*,KN<double>*,KN<double>*>(mychecksol));  
    }
//  else
    if(verbosity)
      cout << "( load: lapack <=> fflapack , skeep ) ";
}

