 /* this file is the interface beteween Freefem++ and the integro tools.
 *
 */


#include "ff++.hpp"
#include "RNM.hpp"
#include "AFunction_ext.hpp" // Extension of "AFunction.hpp" to deal with more than 3 parameters function
//#include "/SandBox/BUILD/INCLUDE/SOLVERS_LS.h"
using namespace std;
#ifdef __LP64__
  typedef int intblas;
  typedef int integer;
#else
  typedef long intblas;
  typedef long integer;
#endif

typedef integer  logical;
typedef float   LAPACK_real;
typedef double   doublereal;
typedef logical  (* L_fp)();
typedef integer      ftnlen;

typedef complex<float> LAPACK_complex;
typedef complex<double> doublecomplex;
typedef void VOID; 
#define complex LAPACK_complex 
#define real LAPACK_real 

class Init { public:
  Init();
};

static double StepX;
static double eDiff;
static long n1Ddofs;

long initVals(const double &dx, const long &n1dof,const double &e){
  StepX=dx;
  n1Ddofs=n1dof;
  eDiff=e;
  return 0;
}

double Jkernel(double d){
  double res=0.0;
  double factor=1.0/(1.0*StepX);
  if (abs(d)<0.1*StepX)
    res=1.0*factor;
  
  return res;
}
long buildK(KNM<double>* const &K,KN<double>* const &PT){
  intblas n=K->N();
  double *kmat=&(*K)(0,0);
  double *points=&(*PT)(0);
  for (int ii=0;ii<n1Ddofs;ii++){
    for (int jj=0;jj<n1Ddofs;jj++){
      double diff=points[ii]-points[jj];
      kmat[ii+n*jj]=eDiff*Jkernel(diff);
    }
  }
  return 0;
}
#include <sys/types.h>
#include <unistd.h>

long myGetPid(){
  long res=(long)getpid();
  return res;
}

LOADINIT(Init);  //  une variable globale qui serat construite  au chargement dynamique 

Init::Init(){  // le constructeur qui ajoute la fonction "splitmesh3"  a freefem++ 

//  if( map_type.find(typeid(Inverse<KNM<double >* >).name() ) == map_type.end() )
    {
      if(verbosity) 
        cout << " Add my interface solver ..." ;
      Global.Add("myBuildK","(",new  OneOperator2_<long,KNM<double>*,KN<double>*>(buildK));
      Global.Add("myIntegroToolsInit","(",new  OneOperator3_<long,double,long,double>(initVals));
      Global.Add("mygetpid","(",new OneOperator0<long>(myGetPid));
    }

}
