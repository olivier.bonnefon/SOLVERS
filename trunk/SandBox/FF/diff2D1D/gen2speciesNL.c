#include "gen2speciesNL.h"

int withSource=0;
int withControl=0;
//#define WITH_GRADIENT_P
/*
Vh0 uc=utm10s4+utm10s5+utm10s6+utm10s7+utm10s8;
    visuScal("2D1",Th0,uc,10);
Vh0 uc=utm10s0+utm10s1+utm10s2+utm10s3;

    cout<<"M1D0s3 "<<M1D0s3<<endl;


cout<<"M1D0s1 "<<M1D0s1<<endl;
    
*/

char OUTPUTDIR[512];
//char POSTPRODIR[512];
char FILENAME[512];
// 1 for left material.
// -1 for right material.
int reverseEdges=1;
int  BUILD_MESH=1;
int NFilesCopy=0;
int NbSpecies=2;
int NDynFieldType=1;
int NDynEdgeType=1;
double nPerLength=3e-5;
int nEFinter=0;
int nEEinter=0;
int nParams=2;



char specNames[]={'u','v','w','x','y','z'};

#define VARF_1D_WITH_FF


enum solverType LinearSolver_2D1D=UMFPACK_2D1D;

int slowStart=0;
int curLab=1;
//vecteur unitaire des edges
//double *pVE;
int maxDofs1d=-1;
char caux1[1024];
char caux2[1024];
char caux3[1024];
char caux4[1024];




MatrixStruct **ppMS;
int NB_BLOCK_LINES=0;
int NB_BLOCK_LINES_PER_SPECIES=0;


void printModelParams(){
  char pname[512];
  int ii,jj,numS,iw,numSpecies;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"modelParams.edp");
  if(access( FILENAME, F_OK ) != -1){
    printf("File %s exists, skip it\n",FILENAME);
    return;
  }
  FILE *pF=fopen(FILENAME,"w");
  fprintf(pF,"string specNames=\"");
  for ( numSpecies=0;numSpecies<NbSpecies;numSpecies++)
    fprintf(pF,"%c",specNames[numSpecies]);
  fprintf(pF,"\";\n");
  
  for (int numP=0;numP<nParams;numP++){
    fprintf(pF,"real p%i=1;\n",numP+1);
  }
  fclose(pF);
}


void addParam(enum paramType type, char * name, int num,double v){
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defParamsGen.edp");
  FILE *pOut = fopen(FILENAME, "a");
  switch (type){
  case INT:
    fprintf(pOut,"int %s=%i;\n",name,(int) v);
    break;
  case DOUBLE:
    fprintf(pOut,"real %s=%e;\n",name,v);
    break;
  case FCT:
    fprintf(pOut,"Vh%i %s=%e;\n",num,name,v);
    break;
  default:
    printf("ERROR: addParam with type undfifed\n");
  }
  fclose(pOut);
}
char VarName[126];
int lastVar=0;
void addDomVar(){
  int varExist=0,nDyn;
  for (nDyn=0;nDyn<NDynFieldType;nDyn++){
    sprintf(FILENAME,"%s/userFDef%i.txt",OUTPUTDIR,nDyn);
    if(access( FILENAME, F_OK ) != -1){
      char * pIn=NULL;
      FILE* pF= fopen(FILENAME, "r");
      size_t n;
      if (getline(&pIn,&n,pF)!=-1){//char **lineptr, size_t *n, FILE *stream
        int strLength=(int)strlen(pIn);
        for (int ii=1;ii<strLength-3;ii++){
          if (pIn[ii]=='$' && pIn[ii+1]=='d' && pIn[ii+2]=='o'&& pIn[ii+3]=='m'){
            VarName[lastVar]=pIn[ii-1];
            //variable deja ajoutee
            for (int iv=0;iv<lastVar;iv++){
              if (VarName[iv]==VarName[lastVar]){
                varExist=1;
                break;
              }
            }
            if (!varExist){
              //addDomDepVar(VarName[lastVar]);
              lastVar++;
            }
            varExist=0;
          }
        }
      }
      free(pIn);
      fclose(pF);
    }
  }
}


void printParams(){
  char pname[512];
  int ii,jj,numS,iw;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defParamsGen.edp");
  if(access( FILENAME, F_OK ) != -1){
    printf("File %s exists, skip it\n",FILENAME);
    return;
  }
 
  FILE *pOut = fopen(FILENAME, "w");
  fclose(pOut);
  for (ii=0;ii<NDynFieldType;ii++){
    for ( numS=0;numS<NbSpecies;numS++){
      //D param
      if (aFieldPop[ii].isFieldDep[numS][D2DX_I]){
        for (iw=0;iw<Nwires;iw++){
          sprintf(pname,"D2DX%i%c%i",iw,specNames[numS],ii);
          addParam(FCT,pname,iw,aFieldPop[ii].D2DX[numS]);
          sprintf(pname,"D2DY%i%c%i",iw,specNames[numS],ii);
          addParam(FCT,pname,iw,aFieldPop[ii].D2DY[numS]);
        }
      }else{	
        sprintf(pname,"D2DX%c%i",specNames[numS],ii);
        if (aFieldPop[ii].isFct[numS][D2DX_I])
          addParam(FCT,pname,0,aFieldPop[ii].D2DX[numS]);
        else
          addParam(DOUBLE,pname,0,aFieldPop[ii].D2DX[numS]);
        sprintf(pname,"D2DY%c%i",specNames[numS],ii);
        if (aFieldPop[ii].isFct[numS][D2DX_I])
          addParam(FCT,pname,0,aFieldPop[ii].D2DY[numS]);
        else
          addParam(DOUBLE,pname,0,aFieldPop[ii].D2DY[numS]);
      }
//      sprintf(pname,"D2DX%c%i",specNames[numS],ii);
//      addParam(DOUBLE,pname,0,aFieldPop[ii].D2DX[numS]);
//      sprintf(pname,"D2DY%c%i",specNames[numS],ii);
//      addParam(DOUBLE,pname,0,aFieldPop[ii].D2DY[numS]);
     
      
    }
  }
  for (ii=0;ii<NDynEdgeType;ii++){
    for ( numS=0;numS<NbSpecies;numS++){
      sprintf(pname,"D1D%c%i",specNames[numS],ii);
      addParam(DOUBLE,pname,0,aEdgePop[ii].D[numS]);
      sprintf(pname,"Cmalthus1D%c%i",specNames[numS],ii);
      addParam(DOUBLE,pname,0,aEdgePop[ii].D[numS]);
    }
  }
  
  for(ii=0;ii<nEFinter;ii++){
    for ( numS=0;numS<NbSpecies;numS++){
      sprintf(pname,"mu%c%i",specNames[numS],ii);
      addParam(DOUBLE,pname,0,aEdgeFieldInter[ii].mu[numS]);
      sprintf(pname,"nu%c%i",specNames[numS],ii);
      addParam(DOUBLE,pname,0,aEdgeFieldInter[ii].nu[numS]);
    }
  }

  for(ii=0;ii<nEEinter;ii++){
    for ( numS=0;numS<NbSpecies;numS++){
      sprintf(pname,"alphaC%c%i",specNames[numS],ii);
      addParam(DOUBLE,pname,0,aEdgeEdgeInter[ii].alpha[numS]);
      sprintf(pname,"gamma%c%i",specNames[numS],ii);
      addParam(DOUBLE,pname,0,aEdgeEdgeInter[ii].gamma[numS]);
    }
  }
  for(ii=0;ii<lastVar;ii++){
    for (iw=0;iw<Nwires;iw++){      
      sprintf(pname,"%c%i",VarName[ii],iw);
      addParam(DOUBLE,pname,iw,1.0);
    }
  }
  
}


void printMatrixName(FILE *pF, MatrixStruct *pMS){
  if (!pMS){
    fprintf(pF,"0");
    return;
  }
  switch (pMS->type)
  {
  case VARF2D:
    fprintf(pF,"%cM2D%i",specNames[pMS->numSpecies],pMS->index2D1);
    break;
  case VARF1D:
    fprintf(pF,"%cM1D%is%i",specNames[pMS->numSpecies],pMS->index2D1,pMS->index1D1);
    break;
  case COUPLAGE_1Dto2D:
    fprintf(pF,"%cMMNn%is%i",specNames[pMS->numSpecies],pMS->index2D1,pMS->index1D1);
    break;
  case COUPLAGE_2Dto1D:
    fprintf(pF,"%cMMnN%is%i",specNames[pMS->numSpecies],pMS->index2D1,pMS->index1D1);
    break;
  case COUPLAGE_EDGE:
    fprintf(pF,"%cC%is%is%i",specNames[pMS->numSpecies],pMS->index2D1,pMS->index1D1,pMS->index1D2);
    break;
  case COUPLAGE_DOMAIN:
    fprintf(pF,"%cC1DE%is%ionE%is%i",specNames[pMS->numSpecies],pMS->index2D1,pMS->index1D1,pMS->index2D2,pMS->index1D2);
    break;
  case COUPLAGE_CONTACT_2Dto2D:
    fprintf(pF,"%cNN%is%i",specNames[pMS->numSpecies],pMS->index2D1,pMS->index2D2);
    break;
  case COUPLAGE_SPECIES:
    fprintf(pF,"%c%cMCoup2D%i",specNames[pMS->numSpecies],specNames[pMS->numSpeciesCoupled],pMS->index2D1);
    break;
  default :
    printf("default in printMatrixName\n");
    break;
  }
  
}

/*void printPoints(FILE *pF){
  int i;
  fprintf(pF,"//BEGIN POINTS\n");
  fprintf(pF,"int NPoints=%i;\n",NNodes);
  fprintf(pF,"real[int] Nx=[\n");
  for (i=0;i<NNodes;i++){
    fprintf(pF,"\t%e",Nx[i]);
    if (i<NNodes-1)
       fprintf(pF,",\n");
  }
  fprintf(pF,"];\n");
  fprintf(pF,"real[int] Ny=[\n");
  for (i=0;i<NNodes;i++){
    fprintf(pF,"\t%e",Ny[i]);
    if (i<NNodes-1)
       fprintf(pF,",\n");
  }
  fprintf(pF,"];\n");
  fprintf(pF,"//END POINTS\n");
  }*/
void printEdges(FILE *pF){
  int ii;
  fprintf(pF,"//BEGIN LABEL EDGES\n");
  for (ii=0;ii<NEdges;ii++){
    fprintf(pF,"int labE%i=%i;\n",ii,curLab);
    curLab++;
  }
  fprintf(pF,"//END LABEL EDGES\n");
  

  fprintf(pF,"//BEGIN DECL CST FOR GET 1D DOFS\n");
  fprintf(pF,"//ELi for the length of the edge i, this is the length of the polilyne of the input data, not the length of the polyline used for triangles. This length is useful to know the number of points added to the mesh. \n");
  fprintf(pF,"//EAi for the abscisse of the normalized edge vector\n");
  fprintf(pF,"//EBi for the ordonate of the normalized edge vector\n");
  fprintf(pF,"//EGDEABSCCURV(numP,0) is the abs curv\n");
  fprintf(pF,"//EGDEABSCCURV(numP,1) is the abscisse of point\n");
  fprintf(pF,"//EGDEABSCCURV(numP,2) is the ordinate of point\n");
  fprintf(pF,"//\n");
  int * pEdgIsUsed=(int*)malloc(NEdges*sizeof(int));
  for (ii=0;ii<NEdges;ii++){
    fprintf(pF,"//EDEGE %d\n",ii);
    int numP;
    double aux=0;
    double dNodes=sqrt((PointsX[GET_EDGE_NODE_END(ii)]-PointsX[GET_EDGE_NODE_BEGIN(ii)])*(PointsX[GET_EDGE_NODE_END(ii)]-PointsX[GET_EDGE_NODE_BEGIN(ii)])+
      (PointsY[GET_EDGE_NODE_END(ii)]-PointsY[GET_EDGE_NODE_BEGIN(ii)])*(PointsY[GET_EDGE_NODE_END(ii)]-PointsY[GET_EDGE_NODE_BEGIN(ii)]));
    if (dNodes<1e-13){
      pEdgIsUsed[ii]=0;
      fprintf(pF,"//edges (%i) too small (%e) musn't be used\n",ii,dNodes);
      continue;
    }else
      pEdgIsUsed[ii]=1;
    double vNormX=(PointsX[GET_EDGE_NODE_END(ii)]-PointsX[GET_EDGE_NODE_BEGIN(ii)])/dNodes;
    double vNormY=(PointsY[GET_EDGE_NODE_END(ii)]-PointsY[GET_EDGE_NODE_BEGIN(ii)])/dNodes;
    printf("Edges %i, dNodes=%e vNormX=%e vNormY=%e\n",ii,dNodes,vNormX,vNormY);
    EDGES[ii].geom._abscCurv[0]=0;
//    printf("EDGES[%i]._absCurv[0]=0\n",ii);
    for (numP=0;numP<EDGES[ii].geom.nPoints-2;numP++){
      //for dist compuation
      double dx=PointsX[EDGES[ii].geom.indicesPoints[numP+1]]-PointsX[EDGES[ii].geom.indicesPoints[numP]];
      double dy=PointsY[EDGES[ii].geom.indicesPoints[numP+1]]-PointsY[EDGES[ii].geom.indicesPoints[numP]];
      double d2=dx*dx+dy*dy;
      aux+=sqrt(d2);
      //for curviline abs
      EDGES[ii].geom._abscCurv[numP+1]=aux;
    }
    double dx=PointsX[EDGES[ii].geom.indicesPoints[numP+1]]-PointsX[EDGES[ii].geom.indicesPoints[numP]];
    double dy=PointsY[EDGES[ii].geom.indicesPoints[numP+1]]-PointsY[EDGES[ii].geom.indicesPoints[numP]];
    double d2=dx*dx+dy*dy;
    aux+=sqrt(d2);
    for (numP=0;numP<EDGES[ii].geom.nPoints-2;numP++){
      EDGES[ii].geom._abscCurv[numP+1]=(EDGES[ii].geom._abscCurv[numP+1]/aux);
    }
     
    EDGES[ii].geom._abscCurv[EDGES[ii].geom.nPoints-1]=1;
    //  printf("EDGES[%i]._absCurv[%i]=1\n",ii,EDGES[ii].geom.nPoints-1);
    EDGES[ii].geom._length=aux;
    fprintf(pF,"real EL%i=%e;\n",ii,aux);
    fprintf(pF,"real EA%i=%e;\n",ii,vNormX);
    fprintf(pF,"real EB%i=%e;\n",ii,vNormY);
    fprintf(pF,"//Array of pt with abscCurv\n");
    fprintf(pF,"int EGDE%iNBPOINTS=%i;\n",ii,EDGES[ii].geom.nPoints);
    fprintf(pF,"real[int,int] EGDE%iABSCCURV(%i,3);\n",ii,EDGES[ii].geom.nPoints);
    for (numP=0;numP<EDGES[ii].geom.nPoints;numP++){
      fprintf(pF,"//Point %i\n",numP);
      fprintf(pF," EGDE%iABSCCURV(%i,0)=%e;\n",ii,numP,EDGES[ii].geom._abscCurv[numP]);
      fprintf(pF," EGDE%iABSCCURV(%i,1)=%e;\n",ii,numP,PointsX[EDGES[ii].geom.indicesPoints[numP]]);
      fprintf(pF," EGDE%iABSCCURV(%i,2)=%e;\n",ii,numP,PointsY[EDGES[ii].geom.indicesPoints[numP]]);
    }
      
  }
  fprintf(pF,"//END DECL CST FOR GET 1D DOFS\n");
  fprintf(pF,"//BEGIN EDGES\n");
  fprintf(pF,"int NEdges=%i;\n",NEdges);
  for (ii=0;ii<NEdges;ii++){
    if (!pEdgIsUsed[ii])
      continue;
    fprintf(pF,"cout<<\"Edge %i\"<<endl;\n",ii);
    fprintf(pF,"border EDGE%i(t=0,1){x=curvToAbsc(%i,EGDE%iABSCCURV,EGDE%iNBPOINTS,t)  ; y =curvToOrdo(%i,EGDE%iABSCCURV,EGDE%iNBPOINTS,t);label= labE%i;};\n",
            ii,ii,ii,ii,ii,ii,ii,ii);
  }
  fprintf(pF,"//END EDGES\n");
  free(pEdgIsUsed);
}
void printMeshes(FILE *pF){
  int ii,jj;
  int numSpecies;
  fprintf(pF,"\nint NbSpecies=%i;\n",NbSpecies);
  fprintf(pF,"//BEGIN MESHES\n");
  fprintf(pF,"//node per unit length\n");
  fprintf(pF,"real n=%e;\n",nPerLength);
  fprintf(pF,"int Nwires=%i;\n",Nwires);
  fprintf(pF,"int[int] Wsizes=[\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t%i",Wsizes[ii]);
    if (ii<Nwires-1)
      fprintf(pF,",\n");
  }
  fprintf(pF,"\t];\n\n");
  fprintf(pF,"int[int] WIRES=[\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t%i,%i",WIRES[2*ii].indexEdge,WIRES[2*ii+1].indexEdge);
    if (ii<Nwires-1)
      fprintf(pF,",\n");
  }
  fprintf(pF,"\t];\n\n");
  
  fprintf(pF,"include \"geomMacro.edp\"\n");

  struct wireSide* pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"cout<<\"INFO: meshing Th%i\"<<endl;\n",ii);
    fprintf(pF,"mesh Th%i=buildmesh(\n",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      double edgeLength=EDGES[pW->indexEdge].geom._length;
      int nbPtInEdge=nPerLength*edgeLength;
      if (nbPtInEdge==0) nbPtInEdge =1;
      if (pW->isReversed) nbPtInEdge=-nbPtInEdge;
//      int nbPtInEdge=nPerLength*((*pW)>=0?LEDGES[*pW]:-LEDGES[-(*pW)]);
//      printf("LEDGES[*pW]=%e\n",edgeLength);
      fprintf(pF,"\tEDGE%i(%i)",pW->indexEdge,nbPtInEdge?reverseEdges*nbPtInEdge:reverseEdges);
      //printf("printMeshes %i %i\n",ii,jj);
      if (nPerLength*edgeLength>maxDofs1d)
        maxDofs1d=(int)(nPerLength*edgeLength);
      if (jj<Wsizes[ii]-1)
        fprintf(pF,"+\n");
      pW++;
    }
    fprintf(pF,",fixeborder=true);\n");
  }
  fprintf(pF,"plot(");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"Th%i",ii);
    if (ii<Nwires-1)
      fprintf(pF,",");
  }
  fprintf(pF,",wait=true);\n");
  fprintf(pF,"//END MESHES\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"fespace Vh%i(Th%i,P1);\n",ii,ii);
    fprintf(pF,"fespace Vh%iP2(Th%i,P2);\n",ii,ii);
  }





  
  for (ii=0;ii<Nwires;ii++)
    fprintf(pF,"real Surf%i=int2d(Th%i)(1.0);\n",ii,ii);
  printUnknomns(pF);
  /*for ( numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
    pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      //fprintf(pF,"Vh%i %c2d%i;\n",ii,spec,ii);
      //fprintf(pF,"Vh%i %ctm12d%i;\n",ii,spec,ii);
      //fprintf(pF,"Vh%i %ck2d%i;\n",ii,spec,ii);
      for (jj=0;jj<Wsizes[ii];jj++){
        
        //commented because seems not used fprintf(pF,"Vh%i %ctm1%is%i;\n",ii,spec,ii,pW->indexEdge);
        if (!(pW->isReversed)){
          fprintf(pF,"//Because we need the lengh of the mesh's polyline for the varf\n");
          fprintf(pF,"EL%i=abs(int1d(Th%i,labE%i)(1.0));\n",pW->indexEdge,ii,pW->indexEdge);
          fprintf(pF,"cout<<\"EL%i=\"<<EL%i<<endl;\n",pW->indexEdge,pW->indexEdge);
        }
        pW++;
      }
    }
    }*/
  
}

void printUnknomns(FILE *pF){
  for (int ii=0;ii<Nwires;ii++){
    fprintf(pF,"Vh%i[int] tab2d%i(NbSpecies);\n",ii,ii);
    fprintf(pF,"Vh%i[int] tabtm12d%i(NbSpecies);\n",ii,ii);
    fprintf(pF,"Vh%i[int] tabk2d%i(NbSpecies);\n",ii,ii);
    fprintf(pF,"Vh%i[int] tabsave2d%i(NbSpecies);\n",ii,ii);
    fprintf(pF,"Vh%i[int] tabprev2d%i(NbSpecies);\n",ii,ii);
  }
  fprintf(pF,"macro saveState(MNumStep){\n");
  for ( int numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
    for (int ii=0;ii<Nwires;ii++){
      fprintf(pF,"\t\tSaveVec(tabtm12d%i[%i][],\"SIMU/%cs\"+string(MNumStep)+\"2D%i\");\n",ii,numSpecies,spec,ii);
    }
  }
  fprintf(pF,"}//\n");
  fprintf(pF,"macro loadState(MNumStep){\n");
  for ( int numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
    for (int ii=0;ii<Nwires;ii++){
      fprintf(pF,"\t\tLoadVec(tabtm12d%i[%i][],\"SIMU/%cs\"+string(MNumStep)+\"2D%i\");\n",ii,numSpecies,spec,ii);
    }
  }
  fprintf(pF,"}//\n");
  fprintf(pF,"macro copyStateToPrev{\n");
  fprintf(pF,"\tfor (int lnumSpec=0;lnumSpec<NbSpecies;lnumSpec++){\n");
  for (int ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t\ttabprev2d%i[lnumSpec]=tabtm12d%i[lnumSpec];\n",ii,ii);
  }
  fprintf(pF,"\t}\n");
  fprintf(pF,"}//\n");

fprintf(pF,"macro state2Dtm1tok{\n");

  fprintf(pF,"\tfor (int lnumSpec=0;lnumSpec<NbSpecies;lnumSpec++){\n");
  for (int ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t\ttabk2d%i[lnumSpec]=tabtm12d%i[lnumSpec];\n",ii,ii);
  }
  fprintf(pF,"\t}\n");
  fprintf(pF,"}//\n");

  fprintf(pF,"macro state2Dktotm1{\n");
  fprintf(pF,"\tfor (int lnumSpec=0;lnumSpec<NbSpecies;lnumSpec++){\n");
  for (int ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t\ttabtm12d%i[lnumSpec]=tabk2d%i[lnumSpec];\n",ii,ii);
  }
  fprintf(pF,"\t}\n");
  fprintf(pF,"}//\n");

  
  for ( int numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
    struct wireSide* pW=WIRES;
    for (int ii=0;ii<Nwires;ii++){
      fprintf(pF,"macro %c2d%i tab2d%i[%i]//\n",spec,ii,ii,numSpecies);
      fprintf(pF,"macro %ctm12d%i tabtm12d%i[%i]//\n",spec,ii,ii,numSpecies);
      fprintf(pF,"macro %ck2d%i tabk2d%i[%i]//\n",spec,ii,ii,numSpecies);
      if (NDynEdgeType){
        for (int jj=0;jj<Wsizes[ii];jj++){
//          fprintf(pF,"Vh%i %ctm1%is%i;\n",ii,spec,ii,pW->indexEdge);
          if (!(pW->isReversed)){
            fprintf(pF,"//Because we need the lengh of the mesh's polyline for the varf\n");
            fprintf(pF,"EL%i=abs(int1d(Th%i,labE%i)(1.0));\n",pW->indexEdge,ii,pW->indexEdge);
            fprintf(pF,"cout<<\"EL%i=\"<<EL%i<<endl;\n",pW->indexEdge,pW->indexEdge);
          }
          pW++;
        }
      }
    }
  }
}



void printDofs2D(FILE *pF){
  int ii,jj,numSpecies;
  
  fprintf(pF,"//BEGIN DOFS 2D DEF\n");
  struct wireSide* pW=WIRES;
  fprintf(pF,"\t//BEGIN DOFS 2D declar\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"//Omega %i\n",ii);
    fprintf(pF,"int n2Ddofs%i=Vh%i.ndof;\n",ii,ii);
  }
  fprintf(pF,"\t//END DOFS 2D declar\n");
//  fprintf(pF,"//if couple1D, edge-corners are connected\n");
//  fprintf(pF,"int couple1D=%i;\n",couple1D);

  
  fprintf(pF,"//END DOFS 2D DEF\n");
  fprintf(pF,"\n");
  
}
void printDofs1D(FILE *pF){
  int ii,jj,numSpecies;
  
  fprintf(pF,"//BEGIN DOFS 1D DEF\n");
  fprintf(pF,"int[int] zoibuff(%i);\n",maxDofs1d+2);
  struct wireSide* pW=WIRES;
  fprintf(pF,"\t//BEGIN DOFS 1D declar\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"//Omega %i\n",ii);
    //if (NDynEdgeType){
    fprintf(pF,"int ");
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"n1Ddofs%is%i",ii,pW->indexEdge);
      if (jj<Wsizes[ii]-1)
        fprintf(pF,",");
      pW++;
    }
    fprintf(pF,";\n");
    //}
  }
  fprintf(pF,"\t//END DOFS 1D declar\n");
//  fprintf(pF,"//if couple1D, edge-corners are connected\n");
//  fprintf(pF,"int couple1D=%i;\n",couple1D);
  if (NDynEdgeType || 1){
    pW=WIRES;
    fprintf(pF,"\t//BEGIN GET 1D DOFS index\n");
    for (ii=0;ii<Nwires;ii++){
      for (jj=0;jj<Wsizes[ii];jj++){
        fprintf(pF,"boundarydofs(labE%i,EA%i*x+EB%i*y-10000000,Th%i,Vh%i,zoibuff,n1Ddofs%is%i);\n",pW->indexEdge,pW->indexEdge,pW->indexEdge,ii,ii,ii,pW->indexEdge);
        fprintf(pF,"int[int] zoi%is%i(n1Ddofs%is%i);\n",ii,pW->indexEdge,ii,pW->indexEdge);
        fprintf(pF,"zoi%is%i=zoibuff(0:n1Ddofs%is%i-1);\n\n",ii,pW->indexEdge,ii,pW->indexEdge);
        fprintf(pF,"cout<<\"labE%i\"<<endl;\n",pW->indexEdge);
        pW++;
      }
    }
    fprintf(pF,"\t//END GET 1D DOFS index\n");
    
    fprintf(pF,"\t//BEGIN 1D DOFS declar\n");
    for ( numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      char spec=specNames[numSpecies];
      pW=WIRES;
      for (ii=0;ii<Nwires;ii++){
        fprintf(pF,"real[int] ");
        for (jj=0;jj<Wsizes[ii];jj++){
          fprintf(pF,"rhs%c1d%is%i(n1Ddofs%is%i)",specNames[numSpecies],ii,pW->indexEdge,ii,pW->indexEdge);
          fprintf(pF,",rhs%c1d%is%iBis(n1Ddofs%is%i)",specNames[numSpecies],ii,pW->indexEdge,ii,pW->indexEdge);
          //        fprintf(pF,"buff%c1d%is%i(n1Ddofs%is%i)",specNames[numSpecies],ii,pW->indexEdge,ii,pW->indexEdge);
          if (jj<Wsizes[ii]-1)
            fprintf(pF,",");
          pW++;
        }
        fprintf(pF,";\n");
      }
      fprintf(pF,"\n");
      
      pW=WIRES;
      for (ii=0;ii<Nwires;ii++){
        fprintf(pF,"real[int] ");
        for (jj=0;jj<Wsizes[ii];jj++){
          fprintf(pF,"p%c1d%is%i(n1Ddofs%is%i),psav%c1d%is%i(n1Ddofs%is%i)",
                  specNames[numSpecies],ii,pW->indexEdge,ii,pW->indexEdge,
                  specNames[numSpecies],ii,pW->indexEdge,ii,pW->indexEdge);
          if (jj<Wsizes[ii]-1)
            fprintf(pF,",");
          pW++;
        }
        fprintf(pF,";\n");
      }

      fprintf(pF,"\n");
  
  
      pW=WIRES;
      for (ii=0;ii<Nwires;ii++){
        fprintf(pF,"real[int] ");
        for (jj=0;jj<Wsizes[ii];jj++){
          fprintf(pF,"p%c1dtm1%is%i(n1Ddofs%is%i)",specNames[numSpecies],ii,pW->indexEdge,ii,pW->indexEdge);
          //printf("p%c1dtm1%is%i(n1Ddofs%is%i)",specNames[numSpecies],ii,pW->indexEdge,ii,pW->indexEdge);
          if (jj<Wsizes[ii]-1)
            fprintf(pF,",");
          pW++;
        }
        fprintf(pF,";\n");
      }
      fprintf(pF,"\n");

     

      pW=WIRES;
      for (ii=0;ii<Nwires;ii++){
        fprintf(pF,"real[int] ");
        for (jj=0;jj<Wsizes[ii];jj++){
          fprintf(pF,"p%c1dk%is%i(n1Ddofs%is%i)",specNames[numSpecies],ii,pW->indexEdge,ii,pW->indexEdge);
          //printf("p%c1dk%is%i(n1Ddofs%is%i)",specNames[numSpecies],ii,pW->indexEdge,ii,pW->indexEdge);
          if (jj<Wsizes[ii]-1)
            fprintf(pF,",");
          pW++;
        }
        fprintf(pF,";\n");
      }
      fprintf(pF,"\n");

      
    }
  }
  
  fprintf(pF,"macro state1Dktotm1{\n");
  for ( numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      for (jj=0;jj<Wsizes[ii];jj++){
        fprintf(pF,"  p%c1dtm1%is%i=p%c1dk%is%i;\n",
                specNames[numSpecies],ii,pW->indexEdge,
                specNames[numSpecies],ii,pW->indexEdge);
        pW++;
      }
    }
  }
fprintf(pF,"}//\n");
  
  
}
void printDofs(FILE *pF){
  int ii,jj,numSpecies;
  fprintf(pF,"\ninclude \"defDofs2DGen.edp\";\n");
  if (NDynEdgeType)
    fprintf(pF,"\ninclude \"defDofs1DGen.edp\";\n");
  struct wireSide* pW=WIRES;
  
  pW=WIRES;
  fprintf(pF,"int nbDofs=NbSpecies*(");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"n2Ddofs%i",ii);
    if (NDynEdgeType){
      fprintf(pF,"+");
      for (jj=0;jj<Wsizes[ii];jj++){
        fprintf(pF,"n1Ddofs%is%i",ii,pW->indexEdge);
        if (jj<Wsizes[ii]-1 || ii<Nwires-1)
          fprintf(pF,"+");
        pW++;
      }
    }
    if(ii<Nwires-1)
      fprintf(pF,"\n");
  }
  fprintf(pF,");\n");
  
 
  fprintf(pF,"real[int] sol(nbDofs);\n");
  fprintf(pF,"real[int] rhs(nbDofs);\n");

  fprintf(pF,"\n");
  for ( numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"real[int] rhs%c2d%i(n2Ddofs%i);\n",specNames[numSpecies],ii,ii);
    }
    fprintf(pF,"\n");
  }
}
void printVarfs(FILE *pF){
  int ii,jj,numSpecies, numSpecies2;;
  struct wireSide* pW=WIRES;
  int* curW=0;
  int numEdge=0;
  char sShared[512],sMu[512],sNu[512];
  char FileNameUpdate[512];
  sprintf(FileNameUpdate,"%supdateMatrix1D.edp",OUTPUTDIR);
  FILE *pFupdateMat=fopen(FileNameUpdate,"w");
  struct edgePop* pedgePop=NULL;
  if (NDynEdgeType)
    pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
  fprintf(pF,"//BEGIN VARFS\n\n");
//#ifdef WITH_GRADIENT
  fprintf(pF,"include \"defFuncFGen.edp\";\n");
//#endif
  //fprintf(pF,"real D2DX=0.05;\nreal D2DY=0.05;\n");

  //fprintf(pF,"real ux0=1;\nreal ux1=0;\nreal dux1=0;\nreal dux0=0;\n");

  fprintf(pF,"//BEGIN MATRIX DEC\n");
  fprintf(pF,"matrix A,MM,Maux;\n");
//  fprintf(pF,"real D1D=10.0;\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    for (ii=0;ii<Nwires;ii++)
      fprintf(pF,"matrix %cM2D%i;\n",specNames[numSpecies],ii);
  }
  pW=WIRES;
  if (NDynEdgeType){
    fprintf(pF,"matrix ");
    for (ii=0;ii<Nwires;ii++){
      for (jj=0;jj<Wsizes[ii];jj++){
        for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
          fprintf(pF,"%cM1D%is%i,",specNames[numSpecies],ii,pW->indexEdge);
          fprintf(pF,"%cBisM1D%is%i,",specNames[numSpecies],ii,pW->indexEdge);
        }
        fprintf(pF,"MASSE1D%is%i,",ii,pW->indexEdge);
        fprintf(pF,"LAP1D%is%i",ii,pW->indexEdge);
        if (jj<Wsizes[ii]-1|| ii < Nwires-1)
          fprintf(pF,",");
        pW++;
      }
    }
    fprintf(pF,";\n");
  }
  fprintf(pF,"//couplage UV matrices\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
    
    for (ii=0;ii<Nwires;ii++){
      for (numSpecies2=0;numSpecies2<NbSpecies;numSpecies2++){
        if (numSpecies!=numSpecies2 && isCoupled(ii,numSpecies,numSpecies2)){
          fprintf(pF,"matrix %c%cMCoup2D%i;\n",
                  specNames[numSpecies],specNames[numSpecies2],ii);
        }
      }
    }
  }
  
  fprintf(pF,"//END MATRIX DEC\n\n");
  if (NDynEdgeType){
  fprintf(pF,"//begin 1Dx1D varf\n");
  //pW=WIRES;
  //pedgePop=aEdgePop;
//  int numEdgeInWire=0;
  
  fprintf(pF,"real aux1;\n");
  fprintf(pF,"real naux;\n");
  fprintf(pF,"matrix MatAux;\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
//    char spec2=specNames[numSpecies];
    pW=WIRES;
    numEdge=0;
    pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
    for (ii=0;ii<Nwires;ii++){
      for (jj=0;jj<Wsizes[ii];jj++){
        pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
        fprintf(pF,"aux1=EL%i/(n1Ddofs%is%i-1);\n",pW->indexEdge,ii,pW->indexEdge);
        sprintf(sMu,"mu%c%i",spec,pedgePop->indexEFInter);
        if (!numSpecies){
          fprintf(pF,"\nreal[int] stepsBis%is%i(n1Ddofs%is%i);\n",ii,pW->indexEdge,ii,pW->indexEdge);
          fprintf(pF,"compute1DAbsc(%c2d%i,p%c1d%is%i,p%c1dtm1%is%i,n1Ddofs%is%i,zoi%is%i,%i,%i,stepsBis%is%i);\n",
                  spec,ii,
                  spec,ii,pW->indexEdge,
                  spec,ii,pW->indexEdge,
                  ii,pW->indexEdge,
                  ii,pW->indexEdge,
                  ii,1,//(*pW)<0?-1:1,
                  ii,pW->indexEdge);
          fprintf(pF,"mesh Th%is%i = square(n1Ddofs%is%i-1,1,[EL%i*x,0.1*EL%i*y]);\n",
                  ii,pW->indexEdge,
                  ii,pW->indexEdge,
                  pW->indexEdge,pW->indexEdge);
          fprintf(pF,"Th%is%i = movemesh(Th%is%i,[moveMeshX(x,stepsBis%is%i,aux1),y]);\n",
                  ii,pW->indexEdge,
                  ii,pW->indexEdge,
                  ii,pW->indexEdge);
          fprintf(pF,"fespace Vh%is%i(Th%is%i,P1);\n",
                  ii,pW->indexEdge,ii,pW->indexEdge);
        }
        if (numSpecies==0){
          fprintf(pF,"Vh%is%i[int] tab1D%is%itm1(NbSpecies);\n",
                  ii,pW->indexEdge,
                  ii,pW->indexEdge);
          fprintf(pF,"Vh%is%i[int] tabsave1D%is%itm1(NbSpecies);\n",
                  ii,pW->indexEdge,
                  ii,pW->indexEdge);
          fprintf(pF,"Vh%is%i[int] tabpred1D%is%itm1(NbSpecies);\n",
                  ii,pW->indexEdge,
                  ii,pW->indexEdge);
        }
        fprintf(pF,"macro %c1D%is%itm1 tab1D%is%itm1[%i]//\n",
                spec,ii,pW->indexEdge,
                ii,pW->indexEdge,
                numSpecies);
     
        
        //fprintf(pF,"Vh%is%i %c1D%is%itm1;\n",
        //        ii,pW->indexEdge,spec,ii,pW->indexEdge);
        if (EDGES[pW->indexEdge]._isShared)
          sprintf(sShared,"+(alphaC%c%i)",spec,pedgePop->indexEEInter);
        else
          sprintf(sShared," ");
        fprintf(pF,"varf mat%c%is%i(u,v)=int1d(Th%is%i,1)((alpha-(Cmalthus1D%c%i)%s+%s)*u*v+(D1D%c%i)*dx(u)*dx(v))",spec,
                ii,pW->indexEdge,ii,pW->indexEdge,spec,EDGES[pW->indexEdge].popType,
                sShared,sMu,
                spec,EDGES[pW->indexEdge].popType);
                
        if (EDGES[pW->indexEdge]._bcinf.type==DIRICHLET){
          if (pW->isReversed)
            fprintf(pF,"\n\t+on(2,u=dirichlet0)");//,EDGES[pW->indexEdge]._bcinf._value);
          else
            fprintf(pF,"\n\t+on(4,u=dirichlet0)");//,EDGES[pW->indexEdge]._bcinf._value);
        }
        if (EDGES[pW->indexEdge]._bcsup.type==DIRICHLET){
          if (pW->isReversed)
            fprintf(pF,"\n\t+on(4,u=dirichlet0)");//,EDGES[pW->indexEdge]._bcsup._value);
          else
            fprintf(pF,"\n\t+on(2,u=dirichlet0)");//,EDGES[pW->indexEdge]._bcsup._value);
        }
        fprintf(pF,";\n");
        
        fprintf(pF,"varf varfRhs%c%is%i(u,v)=int1d(Th%is%i,1)(alpha*%c1D%is%itm1*v)",spec,
                ii,pW->indexEdge,ii,pW->indexEdge,spec,ii,pW->indexEdge);
        if (pedgePop->source[numSpecies]!=0){
          fprintf(pF,"\n\t+int1d(Th%is%i,1)(source1D%c%i*v)",ii,pW->indexEdge,spec,EDGES[pW->indexEdge].popType);
        }
        if (EDGES[pW->indexEdge]._bcinf.type==DIRICHLET){
          if (pW->isReversed)
            fprintf(pF,"\n\t+on(2,u=dirichlet0)");//,EDGES[pW->indexEdge]._bcinf._value);
          else
            fprintf(pF,"\n\t+on(4,u=dirichlet0)");//,EDGES[pW->indexEdge]._bcinf._value);
        }
        if (EDGES[pW->indexEdge]._bcsup.type==DIRICHLET){
          if (pW->isReversed)
            fprintf(pF,"\n\t+on(4,u=dirichlet0)");//,EDGES[pW->indexEdge]._bcsup._value);
          else
            fprintf(pF,"\n\t+on(2,u=dirichlet0)");//,EDGES[pW->indexEdge]._bcsup._value);
        }
        fprintf(pF,";\n");

        fprintf(pF,"naux=%c1D%is%itm1[].n;\n",spec,ii,pW->indexEdge);
//        fprintf(pF,"cout<<\"naux=\"<<naux<<\" \"<<n1Ddofs%is%i<<endl;\n",ii,pW->indexEdge);
        if (!numSpecies){
          fprintf(pF,"int[int] zoi%is%ibis(naux);\n",ii,pW->indexEdge);
          fprintf(pF,"boundarydofs(1,x-100000000,Th%is%i,Vh%is%i,zoi%is%ibis,n1Ddofs%is%i);\n",ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge);
          fprintf(pF,"zoi%is%ibis.resize(n1Ddofs%is%i);\n",ii,pW->indexEdge,ii,pW->indexEdge);
        }
        fprintf(pFupdateMat,"buildM2(mat%c%is%i,%cM1D%is%i,zoi%is%ibis,Vh%is%i);\n\n",spec,ii,pW->indexEdge,spec,ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge);
/*        fprintf(pF,"buildM2(mat%is%i,%cBisM1D%is%i,zoi%is%ibis,Vh%is%i);\n",ii,pW->indexEdge,spec,ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge);
        fprintf(pF,"MatAux=%cBisM1D%is%i+(-1.0)*%cM1D%is%i;\n",spec,ii,pW->indexEdge,spec,ii,pW->indexEdge);
        fprintf(pF,"cout<<MatAux<<endl;\n\n");*/
        
        pW++;
        numEdge++;
      }
      fprintf(pF,"\n");
    }
  }
  fprintf(pF,"//end 1Dx1D varf\n\n");

  fprintf(pF,"//begin 1Dx1D domain couple\n");
  for(ii=0;ii<NB_BLOCK_LINES;ii++){
    for(jj=0;jj<NB_BLOCK_LINES;jj++){
      if (ppMS[ii+jj*NB_BLOCK_LINES] && (ppMS[ii+jj*NB_BLOCK_LINES])->type == COUPLAGE_DOMAIN){
        int numSpec=(ppMS[ii+jj*NB_BLOCK_LINES])->numSpecies;
        int i2D=(ppMS[ii+jj*NB_BLOCK_LINES])->index2D1;
        int i1D=(ppMS[ii+jj*NB_BLOCK_LINES])->index1D1;
        fprintf(pF,"matrix ");
        printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
/*        fprintf(pF,"=-1*(%e)*MASSE1D%is%i;\n",
                (ppMS[ii+jj*NB_BLOCK_LINES])->pEEInter->alpha[numSpec],
                (ppMS[ii+jj*NB_BLOCK_LINES])->index2D1,
                (ppMS[ii+jj*NB_BLOCK_LINES])->index1D1);*/
        fprintf(pF,";\n");
        fprintf(pF,"varf matC%c%is%i(u,v)=int1d(Th%is%i,1)((-1*(alphaC%c%i))*u*v);\n",specNames[numSpec],
                i2D,
                i1D,
                i2D,
                i1D,
                specNames[numSpec],(ppMS[ii+jj*NB_BLOCK_LINES])->indexEEInter);
//                aEdgeEdgeInter[(ppMS[ii+jj*NB_BLOCK_LINES])->indexEEInter].alpha[numSpec]);
        fprintf(pF,"buildM2(matC%c%is%i,",specNames[numSpec],
                i2D,
                i1D);
        printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
        fprintf(pF,",zoi%is%ibis,Vh%is%i);\n",
                i2D,
                i1D,
                i2D,
                i1D);
        
        
      }
    }
  }
  
  fprintf(pF,"//end 1Dx1D domain couple\n\n");

  
  fprintf(pF,"//begin 2D ---> 1D varf\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    pW=WIRES;
    int nedge=0;
    pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
   
    char spec=specNames[numSpecies];
    
    for (ii=0;ii<Nwires;ii++){
      if (!numSpecies){
        fprintf(pF,"int[int] allLine%i(n2Ddofs%i);\n",ii,ii);
        fprintf(pF,"for (int ii=0; ii<n2Ddofs%i; ii++)\n",ii);
        fprintf(pF,"\tallLine%i[ii]=ii;\n",ii);
      }
      for (jj=0;jj<Wsizes[ii];jj++){
        pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
//        fprintf(pF,"real %cnu%i=%e;\n",spec,nedge,aEdgeFieldInter[pedgePop->indexEFInter].nu[numSpecies]);
        fprintf(pF,"varf varfMat%c1D2D%is%i(v,w) = -int1d(Th%i,labE%i)(nu%c%i*v*w);\n",
                spec,ii,pW->indexEdge,ii,pW->indexEdge,spec,pedgePop->indexEFInter);
        fprintf(pF,"Maux=varfMat%c1D2D%is%i(Vh%i,Vh%i);\n",
                spec,ii,pW->indexEdge,ii,ii);
        fprintf(pF,"matrix %cMMnN%is%i=Maux(zoi%is%i,allLine%i);\n",
                spec,ii,pW->indexEdge,ii,pW->indexEdge,ii);/*ici?*/
        /* if (NbSpecies==2) */
        /*   fprintf(pF,"matrix VMMnN%is%i=UMMnN%is%i;\n",ii,pW->indexEdge,ii,pW->indexEdge); */
        pW++;
        nedge++;
        
      }
      fprintf(pF,"\n");
    }
  }
  fprintf(pF,"//end 2D ---> 1D varf\n");

  }
  fprintf(pF,"\n//begin 2D varf\n");
//#ifdef WITH_GRADIENT
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defFuncFGen.edp");
  FILE *pFF = fopen(FILENAME, "w");
//  sprintf(FILENAME,"%s%s",OUTPUTDIR,"setFuncFGen.edp");
//  FILE *psetFF = fopen(FILENAME, "w");

  //
 
  char sD2X[512],sD2Y[512];
  char sfname[512],sfnameX[512],sGrad[MAX_NB_SPECIES][512],sGradX[MAX_NB_SPECIES][512],sVar[MAX_NB_SPECIES][512];
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    pW=WIRES;
    int nedge=0;
//    int curNedge=0;
    char spec=specNames[numSpecies];
    int numSpecies2;
    for (ii=0;ii<Nwires;ii++){
      sprintf(sD2X,"D2DX%c%i",spec,FieldTypes[ii]);
      sprintf(sD2Y,"D2DY%c%i",spec,FieldTypes[ii]);
      //
      // notations:
      //
      //
      // dans le domaine ii: U \in R^{NbSpecies}
      // d_t U = \laplac U + f2dk'FieldTypes[ii]specNames[k]ii' avec 0<k<NbSpecies
      //                     |_________________________________|
      //                                      |
      //                                      f
      // Les gradients de f:
      // d_{Uk} f_l = d'specNames[k]'f2dk'FieldTypes[ii]specNames[l]ii'
      //  
      //
      sprintf(sfname,"f2dk%i%c%i",FieldTypes[ii],specNames[numSpecies],ii);
      sprintf(sfnameX,"f2d%i%c%i",FieldTypes[ii],specNames[numSpecies],ii);
      fprintf(pFF,"Vh%iP2 %s=0;\n",ii,sfname);
      fprintf(pFF,"Vh%iP2 %s=0;\n",ii,sfnameX);
//      fprintf(psetFF,"%s=0;\n",sfname);
      for (numSpecies2=0;numSpecies2<NbSpecies;numSpecies2++){
        sprintf(sGrad[numSpecies2],"d%c%s",specNames[numSpecies2],sfname);
        sprintf(sGradX[numSpecies2],"d%c%s",specNames[numSpecies2],sfnameX);
        fprintf(pFF,"Vh%i %s=0;\n",ii,sGrad[numSpecies2]);
        fprintf(pFF,"Vh%i %s=0;\n",ii,sGradX[numSpecies2]);
//        fprintf(psetFF,"%s=0;\n",sGrad[numSpecies2]);
        sprintf(sVar[numSpecies2],"%ck2d%i",specNames[numSpecies2],ii);
      }
      fprintf(pF,"varf varfMat%c2D%i(v,w)=",spec,ii);
      //mass matrix et diffusion:
      fprintf(pF,"int2d(Th%i)(tm*%s*dx(v)*dx(w)+tm*%s*dy(v)*dy(w)+\n\t(alpha+",
              ii,
              sD2X,sD2Y);
      
      //
      //Le gradient en U_numSpecies du domain ii de la composante U_numSpecies: d'spec'2dkii}fii
      fprintf(pF,"\t(-1.0*tm)*%s)*v*w)",sGrad[numSpecies]);
      if (NDynEdgeType){
        fprintf(pF,"+\n\t");
        for (jj=0;jj<Wsizes[ii];jj++){
          pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
          fprintf(pF,"int1d(Th%i,",ii);
          fprintf(pF,"labE%i)",pW->indexEdge);
          if (EDGES[pW->indexEdge]._isShared){
            fprintf(pF,"((nu%c%i+gamma%c%i)*v*w)",
                    specNames[numSpecies],pedgePop->indexEFInter,
                    specNames[numSpecies],pedgePop->indexEEInter);
            
          }else{
            fprintf(pF,"(nu%c%i*v*w)",
                    specNames[numSpecies],pedgePop->indexEFInter);
          }
          if(jj+1<Wsizes[ii])
            fprintf(pF,"+\n\t");
          //else
          //  fprintf(pF,";\n");
          pW++;
          nedge++;
        }
      }
      fprintf(pF,";\n");
      fprintf(pF,"\n");
      fprintf(pF,"varf varfRhs%c2D%i(v,w)=\n\t",spec,ii);
      fprintf(pF," int2d(Th%i)(\n\t\t(alpha*%ctm12d%i+\n",ii,spec,ii);
      fprintf(pF," \t\t\t tm*(%s  \n",sfname);
      for (numSpecies2=0;numSpecies2<NbSpecies;numSpecies2++){
        fprintf(pF," \t\t\t - %s*%s\n",sGrad[numSpecies2],sVar[numSpecies2]);
      }
      fprintf(pF,"\t))*w);\n\n");

      fprintf(pF,"\n//begin 2D couplage varf of domain %i spec %c:\n",ii,spec);
      for (numSpecies2=0;numSpecies2<NbSpecies;numSpecies2++){
        if (numSpecies2==numSpecies)
          continue;
        fprintf(pF,"varf varfMat%c%cCoup2D%i(v,w)=",spec,specNames[numSpecies2],ii);
        fprintf(pF," int2d(Th%i)((-1.0*tm)*%s*v*w);\n",ii,sGrad[numSpecies2]);
        
      }
      fprintf(pF,"\n//end 2D couplage varf\n\n");
    }
  }
  fclose(pFF);
//  fclose(psetFF);

  if (NDynEdgeType){
  fprintf(pF,"//begin 1D ---> 2D  varf\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
    int nedge=0;
    pW=WIRES;
    pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
    for (ii=0;ii<Nwires;ii++){
      for (jj=0;jj<Wsizes[ii];jj++){
        pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
        //fprintf(pF,"real %cmu%i=%e;\n",spec,nedge,pedgePop->pEFInter->mu[numSpecies]);
        fprintf(pF,"varf varfMat%c2D1D%is%i(u,w)=-int1d(Th%i,labE%i)(mu%c%i*u*w);\n",
                spec,ii,pW->indexEdge,ii,pW->indexEdge,spec,pedgePop->indexEFInter);
        fprintf(pF,"Maux=varfMat%c2D1D%is%i(Vh%i,Vh%i);\n",
                spec,ii,pW->indexEdge,ii,ii);
        fprintf(pF,"matrix %cMMNn%is%i=Maux(allLine%i,zoi%is%i);\n",
                spec,ii,pW->indexEdge,ii,ii,pW->indexEdge);
        //fprintf(pF,"matrix VMMNn%is%i=UMMNn%is%i;\n",ii,pW->indexEdge,ii,pW->indexEdge);
        pW++;
        nedge++;
      }
      fprintf(pF,"\n");
    }
  }
  
  fprintf(pF,"//end 1D ---> 2D  varf\n");
  }
  fprintf(pF,"\n");
  fprintf(pF,"//begin 2D / 2D exchange. Pop move jumping edges  varf\n");
  fprintf(pF,"matrix Maux2;\n");
  for(ii=0;ii<NB_BLOCK_LINES;ii++){
    for(jj=0;jj<NB_BLOCK_LINES;jj++){
      if (ppMS[ii+jj*NB_BLOCK_LINES] && (ppMS[ii+jj*NB_BLOCK_LINES])->type == COUPLAGE_CONTACT_2Dto2D){
        int i2D1=(ppMS[ii+jj*NB_BLOCK_LINES])->index2D1;
        int i2D2=(ppMS[ii+jj*NB_BLOCK_LINES])->index2D2;
        MatrixStruct *pM=ppMS[ii+jj*NB_BLOCK_LINES];
        int first=1;
        fprintf(pF,"//terme dn V%i=gamma V%i\n",i2D1,i2D2);
        fprintf(pF,"matrix ");
        printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
        fprintf(pF,";\n");
        int numSpec=pM->numSpecies;
        while (pM){
          int i1D=pM->index1D1;
          fprintf(pF,"// edge %i\n{\n",i1D);
          fprintf(pF,"\tmatrix Maux0,Maux1,Maux3;\n");
          fprintf(pF,"\tmatrix ");
          printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
          fprintf(pF,"s%i",i1D);
          fprintf(pF,";\n");
          fprintf(pF,"\tvarf varfNN%is%is%i(u,w)=-int1d(Th%i,labE%i)(gamma%c%i*u*w);\n",i2D1,i2D2,i1D,i2D2,i1D,specNames[numSpec],(ppMS[ii+jj*NB_BLOCK_LINES])->indexEEInter);
          fprintf(pF,"\tMaux1=varfNN%is%is%i(Vh%i,Vh%i);\n",i2D1,i2D2,i1D,i2D2,i2D2);
          fprintf(pF,"\tMaux3=Maux1(zoi%is%i,allLine%i);\n",i2D2,i1D,i2D2);
          fprintf(pF,"\tOBbuildMat(Maux3,");
          printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
          fprintf(pF,"s%i,",i1D);
          fprintf(pF,"zoi%is%i,n2Ddofs%i);\n",i2D1,i1D,i2D1);
          pM=(MatrixStruct *)pM->pNextMatStruct;
          if (first){
            fprintf(pF,"\t");
            printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
            fprintf(pF,"=");
            printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
            fprintf(pF,"s%i",i1D);
            
          }else{
            fprintf(pF,"\tMaux0=");
            printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
            fprintf(pF,";\n\t");
            printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
            fprintf(pF,"=Maux0+");
            printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
            fprintf(pF,"s%i",i1D);
            
          }
          fprintf(pF,";\n}\n");
          first=0;
        }
        fprintf(pF,"\n");
      }
    }
  }


  fprintf(pF,"//end 2D / 2D exchange. Pop move jumping edges  varf\n");
  
  fprintf(pF,"\n");
  fprintf(pF,"\n");
  fprintf(pF,"//END VARFS\n");
  fclose(pFupdateMat);
//  fprintf(pF,"include \"%s\";\n",FileNameUpdate);

}
void printCheckdPF(FILE *pF){
  int ii,numSpecies,numSpecies2,p;
  char spec,spec2;
  for (ii=0;ii<Nwires;ii++){
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      spec=specNames[numSpecies];
      fprintf(pF,"%c2d%i=%ck2d%i;\n",spec,ii,spec,ii);
    }
  }
  
  fprintf(pF,"{\nreal err,errPrev,prevP;\n");
  fprintf(pF,"real epsilonGrad;\nint suspiciousGrad;\nint iig;\n");
  fprintf(pF,"include \"updateOptimParamsValues.edp\";\n");
  fprintf(pF,"include \"setFuncFGenX.edp\";\n");
  fprintf(pF,"include \"setdPFGen.edp\";\n");
 for (ii=0;ii<Nwires;ii++){
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      spec=specNames[numSpecies];
      fprintf(pF,"Vh%i f2dref%i%c%i=f2d%i%c%i;\n",ii,FieldTypes[ii],spec,ii,FieldTypes[ii],spec,ii);
    }
  }
  
  fprintf(pF,"\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"real surf=int2d(Th%i)(1.0);\n",ii);
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      spec=specNames[numSpecies];
      fprintf(pF,"//check derivatives of the component %c ie d.f2d%c%i\n",spec,spec,ii);
      for (p=1;p<nParams+1;p++){
//        spec2=specNames[numSpecies2];
        fprintf(pF,"{\n\tcout<<\"check the derivative dP%if2d%i%c\"<<endl;\n",p,ii,spec);
        fprintf(pF,"\tprevP=POptim(%i);//save param value\n",p-1);

        fprintf(pF,"\t\tofstream checkFile(\"dP%if2d%i%c_\"+string(curStep)+\".txt\");\n",p,ii,spec);
        fprintf(pF,"\t\tepsilonGrad=1;\n");
        fprintf(pF,"\t\tsuspiciousGrad=0;\n");        
        fprintf(pF,"\t\tfor (iig=0;iig<10;iig++){\n");
        
        fprintf(pF,"\t\t\terrPrev=err;\n");
        fprintf(pF,"\t\t\tPOptim(%i)=prevP+epsilonGrad;\n",p-1);
        fprintf(pF,"\t\t\tinclude \"updateOptimParamsValues.edp\";\n");
        fprintf(pF,"\t\t\tinclude \"setFuncFGenX.edp\";\n");
        fprintf(pF,"\t\t\tVh0 errG=(f2d%i%c%i-f2dref%i%c%i)/epsilonGrad - dP%if2d%i%c;\n",FieldTypes[ii],spec,ii,FieldTypes[ii],spec,ii,p,ii,spec);
        fprintf(pF,"\t\t\terr=int2d(Th0)(abs(errG))/surf;\n");
        fprintf(pF,"\t\t\tcheckFile<<err<<endl;\n");
        fprintf(pF,"\t\t\tif (iig && (err > 1e-10) && (errPrev-err) < 0.25*errPrev) suspiciousGrad=1;\n");
        fprintf(pF,"\t\t\tepsilonGrad=epsilonGrad/2.0;\n");
        
        fprintf(pF,"\t\t}\n");
        fprintf(pF,"\tif (suspiciousGrad){ \n");
        fprintf(pF,"\t\tcout<<\"WARNING suspicious gradient, give a look to \"+\"dP%if2d%i%c_\"+string(curStep)+\".txt\"<<endl;\n",p,ii,spec);
        fprintf(pF,"\t}else \n");
        fprintf(pF,"\t\tcout<<\"grad seems ok\"<<endl;\n\n");
        fprintf(pF,"\t\tPOptim(%i)=prevP;\n}\n",p-1);
      }

    }
  }
  fprintf(pF,"}");
}
void printCheckdUF(FILE *pF,int callable){
  int ii,numSpecies,numSpecies2;
  char spec,spec2;
  if (callable){
    fprintf(pF,"include \"myMacro.edp\";\n");
    fprintf(pF,"include \"defGeomGen.edp\"\n");
    fprintf(pF,"include \"defDofsGen.edp\"\n");
    fprintf(pF,"include \"modelParams.edp\"\n");
    fprintf(pF,"include \"defPluginVaraibleGen.edp\"\n");
    fprintf(pF,"include \"defParamsGen.edp\"\n");
    fprintf(pF,"include \"mat1D2Duseful.edp\";\n");
    fprintf(pF,"real T0=0;\n");
    fprintf(pF,"real dt=0.03;\n");
    fprintf(pF,"int nbSteps=100;\n");
    fprintf(pF,"real tm=1.0;\n");
    fprintf(pF,"real alpha=1/dt;\n");
    fprintf(pF,"include \"defVarfGen.edp\"\n");
    fprintf(pF,"include \"defICGen.edp\"\n");
    fprintf(pF,"int curStep=1;\n");
  }
  fprintf(pF,"{\nVh0 dir;\nreal epsilonGrad;\nint iig;\nreal surf;\nVh0 gradDir;\n");
  fprintf(pF,"real err,errPrev;\n");
  fprintf(pF,"int suspiciousGrad;\n");
  for (ii=0;ii<Nwires;ii++){
    
//    pW=WIRES;
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      spec=specNames[numSpecies];
      fprintf(pF,"%c2d%i=%ck2d%i;\n",spec,ii,spec,ii);
    }
    fprintf(pF,"include \"setFuncFGenX.edp\";\n\n");
  }
  fprintf(pF,"\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"surf=int2d(Th%i)(1.0);\n",ii);
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      spec=specNames[numSpecies];
      fprintf(pF,"cout<<\"check derivatives of the component %c ie d.f2d%c%i\"<<endl;\n",spec,spec,ii);
      for (numSpecies2=0;numSpecies2<NbSpecies;numSpecies2++){
        spec2=specNames[numSpecies2];
        fprintf(pF,"\t//check the derivative d%cf2d%i%c\n",spec2,ii,spec);

        fprintf(pF,"\t\t//along 1\n{");
        fprintf(pF,"\t\tofstream checkFile(\"d%cf2d%i%c1_\"+string(curStep)+\".txt\");\n",spec2,ii,spec);
        fprintf(pF,"\t\tepsilonGrad=1;\n");
        fprintf(pF,"\t\tsuspiciousGrad=0;\n");        
        fprintf(pF,"\t\tdir=1;\n");
        fprintf(pF,"\t\tgradDir=dir*d%cf2d%i%c%i;\n",spec2,FieldTypes[ii],spec,ii);
        fprintf(pF,"\t\tfor (iig=0;iig<10;iig++){\n");
        fprintf(pF,"\t\t\terrPrev=err;\n");
        fprintf(pF,"\t\t\t%ck2d%i=%c2d%i+epsilonGrad*dir;\n",spec2,ii,spec2,ii);
        fprintf(pF,"\t\t\tinclude \"setFuncFGenK.edp\";\n");
        fprintf(pF,"\t\t\tVh0 errG=(f2dk%i%c%i-f2d%i%c%i)/epsilonGrad - gradDir;\n",FieldTypes[ii],spec,ii,FieldTypes[ii],spec,ii);
        fprintf(pF,"\t\t\terr=int2d(Th0)(abs(errG))/surf;\n");
        fprintf(pF,"\t\t\tcheckFile<<err<<endl;\n");
        fprintf(pF,"\t\t\tif (iig && (err > 1e-10) && (errPrev-err) < 0.25*errPrev) suspiciousGrad=1;\n");
        fprintf(pF,"\t\t\tepsilonGrad=epsilonGrad/2.0;\n");
        
        fprintf(pF,"\t\t}\n}\n");
        fprintf(pF,"\tif (suspiciousGrad) \n");
        fprintf(pF,"\t\tcout<<\"WARNING suspicious gradient, give a look to \"+\"d%cf2d%i%c1_\"+string(curStep)+\".txt\"<<endl;\n",spec2,ii,spec);
        fprintf(pF,"\telse cout<<\"grad seems ok\"<<endl;");
        fprintf(pF,"\t\t%ck2d%i=%c2d%i;\n",spec2,ii,spec2,ii);
      }

    }
  }
  fprintf(pF,"}");
}
void printDefTargetLoc(){
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defTargetLocs.edp");
  if(access( FILENAME, F_OK ) != -1){
    printf("file %s exits, do nothing\n",FILENAME);
    return;
  }
 ;
  sprintf(FILENAME,"cp %s/EDPSRC/defTargetLocs.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);
}
void printDefTargetDate(){
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defTargetDates.edp");
  if(access( FILENAME, F_OK ) != -1){
    printf("file %s exits, do nothing\n",FILENAME);
    return;
  }
  sprintf(FILENAME,"cp %s/EDPSRC/defTargetDates.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

}
void printRecorder(){
  int ii,jj,numSpecies,nP;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"recorderGen.edp");
  FILE *pF= fopen(FILENAME, "w");
  fprintf(pF,"macro recorderState(NUMDATE)\n{");
  for (ii=0;ii<Nwires;ii++){
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      fprintf(pF,"recordField(%c2dRec,NUMDATE,%i,%c2d%i);\n",
              specNames[numSpecies],numSpecies,specNames[numSpecies],ii);
    }
  }
  fprintf(pF,"}//\n");

  fprintf(pF,"macro recorderW(NUMDATE)\n{");
for (ii=0;ii<Nwires;ii++){
  
    for (nP=0;nP<nParams;nP++){
      for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
        fprintf(pF,"recordWField(WPxd%iRec,NUMDATE,%i,%i,W%i%c2d%i);\n",
                ii,nP,numSpecies,nP+1,specNames[numSpecies],ii);
      }
    }
}
  fprintf(pF,"}//\n");


  
  
}
void printOptimDef(){
  FILE *pF=NULL;

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defOptimParamsGen.edp");
  if(access( FILENAME, F_OK ) == -1){
    pF= fopen(FILENAME, "w");
    for (int nP=0;nP<nParams;nP++)
      fprintf(pF,"POptim[%i]=0;\n",nP);
    fclose(pF);
  }
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"updateOptimParamsValues.edp");
  if(access( FILENAME, F_OK ) == -1){
    fclose(fopen(FILENAME, "w"));
  }
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defdPFGen.edp");
  pF= fopen(FILENAME, "w");

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"userdPFDef.txt");
  FILE *pFset=NULL;
  if(access( FILENAME, F_OK ) == -1)
    pFset = fopen(FILENAME, "w");
  {
    sprintf(FILENAME,"%s%s",OUTPUTDIR,"genSetdPF.sh");
    FILE *pdPFsh=fopen(FILENAME, "w");
    int numS1;
    fprintf(pdPFsh,"sed ");
    for (numS1=0;numS1<NbSpecies;numS1++){
      fprintf(pdPFsh," 's/$%c/%c2d0/g' ",specNames[numS1],specNames[numS1]);
      if (numS1==0){
        fprintf(pdPFsh," userdPFDef.txt ");
      }
      if (numS1+1<NbSpecies)
        fprintf(pdPFsh," | sed ");
    }
    fprintf(pdPFsh," > setdPFGen.edp\n");
    fclose(pdPFsh);
  }
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defWVarfGen.edp");
  FILE *pFGrad=NULL;
  if(access( FILENAME, F_OK ) == -1)
    pFGrad=fopen(FILENAME, "w");

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defWVarGen.edp");
  FILE *pVGrad=NULL;
  if(access( FILENAME, F_OK ) == -1)
    pVGrad=fopen(FILENAME, "w");
 
  
  int i,ndyn,j;
  for (ndyn=0;ndyn<NDynFieldType;ndyn++){// bug: ici on confond ndyn et le numéro de domaine...
    for (i=0;i<nParams;i++){
      if (pFGrad)
        fprintf(pFGrad,"\n//P%i\n",i+1);
      for (j=0;j<NbSpecies;j++){
        fprintf(pF,"Vh%iP2 dP%if2d%i%c;\n",ndyn,i+1,ndyn,specNames[j]);
        if (pFset)
          fprintf(pFset,"dP%if2d%i%c=0;\n",i+1,ndyn,specNames[j]);
        if (pFGrad)
          fprintf(pFGrad,"varf varfRhsdP%if2d%i%c(v,w)=int2d(Th%i)((alpha*W%i%ctm12d%i+dP%if2d%i%c)*w);\n",
                  i+1,ndyn,specNames[j],
                  ndyn,i+1,specNames[j],ndyn,
                  i+1,ndyn,specNames[j]);
        if (pVGrad)
          fprintf(pVGrad,"Vh%i W%i%ctm12d%i=0;\nVh%i W%i%c2d%i=0;\n",ndyn,i+1,specNames[j],ndyn,ndyn,i+1,specNames[j],ndyn);
      }
    }
  }
  fclose(pF);
  if (pFset)
    fclose(pFset);
  if (pFGrad)
    fclose(pFGrad);
  if (pVGrad)
    fclose(pVGrad);
}

//void printMatCoup(FILE *pF,int);
void printMatStruct(FILE *pF);
//void printMatStruct2(FILE *pF);
void buildMat(FILE *pF){
  int ii,numSpecies,numSpecies2;
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"%cM2D%i=varfMat%c2D%i(Vh%i,Vh%i);\n",specNames[numSpecies],ii,specNames[numSpecies],ii,ii,ii);
    }
  }
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
    for (ii=0;ii<Nwires;ii++){
      for (numSpecies2=0;numSpecies2<NbSpecies;numSpecies2++)
	if (numSpecies!=numSpecies2 && isCoupled(ii,numSpecies,numSpecies2)){
	  fprintf(pF,"%c%cMCoup2D%i=varfMat%c%cCoup2D%i(Vh%i,Vh%i);\n",
		  specNames[numSpecies],specNames[numSpecies2],ii,
		  spec,specNames[numSpecies2],ii,ii,ii);
      }
    }
  }
  printMatStruct(pF);
/*  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    printMatStruct(pF,numSpecies);
    fprintf(pF,"matrix M%c;\n",specNames[numSpecies]);
    fprintf(pF,"OBtestCSR(M%c1,M%c,I1I2);\n",specNames[numSpecies],specNames[numSpecies]);
    }*/
  
  
//  fprintf(pF,"if (couple1D){\n");
  if (NDynEdgeType){
//    fprintf(pF,"matrix M;\n");
    fprintf(pF,"\tOBtestCSR(M1,M,I1I2,NbSpecies);\n");
  }
//  fprintf(pF,"}else{M=M1;}\n");
  if (LinearSolver_2D1D==UMFPACK_2D1D)
    fprintf(pF,"set(M,solver=UMFPACK);\n");
  else
    fprintf(pF,"set(M,solver=GMRES);\n");

}
void printMat(FILE *pF){
  int ii,firstEdge,prevEdge,curEdge,jj,kk,numSpecies;
  int* pWsav;
  struct wireSide *pW=WIRES;
  int Nedges=0;
  int indexInI1I2=0;
  int NedgesMaxPerDom=0;
  int numEdge=0;
  int nbConnexion=0;
  struct edgePop *curEdgePop=NULL;
  if (NDynEdgeType){
    curEdgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
    for (ii=0;ii<Nwires;ii++){
      Nedges+=Wsizes[ii];
      if (Wsizes[ii]>NedgesMaxPerDom)
        NedgesMaxPerDom=Wsizes[ii];
    }
  }
  
  
  
  fprintf(pF,"//BEGIN MAT DEF\n");
  fprintf(pF,"\n");
  fprintf(pF,"int[int] indexBeginMat(%i*%i);\n",Nwires,NedgesMaxPerDom);
  fprintf(pF,"int curIndexBlock=0;\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"curIndexBlock+=n2Ddofs%i;\n",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"indexBeginMat[%i*%i+%i]=curIndexBlock;\n",ii,NedgesMaxPerDom,jj);
      curEdge=pW->indexEdge;
      pW++;
      fprintf(pF,"curIndexBlock+=n1Ddofs%is%i;\n",ii,curEdge);
    }
//    indexBeginMat[NedgesMaxPerDom*ii]=curIndexBlock;
    fprintf(pF,"\n");
  }


  //get the number of 1D connexion
  int coupledFirst;
  int coupledPrev;
  int coupledCur;
  int couplednextPrev;
  pW=WIRES;

  for(ii=0;ii<Nwires;ii++){
    int next1D=1;
    for (jj=0;jj<Wsizes[ii];jj++){
      if (((pW[jj].isReversed && EDGES[pW[jj].indexEdge]._bcinf.type==COUPLE1D) ||
           (!(pW[jj].isReversed) && EDGES[pW[jj].indexEdge]._bcsup.type==COUPLE1D) ) &&
          ( pW[next1D].isReversed && EDGES[pW[next1D].indexEdge]._bcsup.type==COUPLE1D ||
            (!(pW[next1D].isReversed) && EDGES[pW[next1D].indexEdge]._bcinf.type==COUPLE1D))){
        nbConnexion++;
      }
      next1D=(next1D+1)%(Wsizes[ii]);
    }
    pW=pW+Wsizes[ii];
  }
  /* for (ii=0;ii<Nwires;ii++){ */
  /*   prevEdge=pW->indexEdge; */
  /*   coupledPrev=1; */
  /*   if ((*pW<0 && EDGES[prevEdge]._bcinf.type!=COUPLE1D) || (*pW>=0 && EDGES[prevEdge]._bcsup.type!=COUPLE1D)){ */
  /*     coupledPrev=0; */
  /*   } */
  /*   firstEdge=prevEdge; */
  /*   pW++; */
  /*   for (jj=1;jj<Wsizes[ii]+1;jj++){ */
  /*     if (jj < Wsizes[ii]){ */
  /*       curEdge=pW->indexEdge; */
  /*       coupledCur=1; */
  /*       if ((*pW<0 && EDGES[curEdge]._bcsup.type!=COUPLE1D) || (*pW>=0 && EDGES[curEdge]._bcinf.type!=COUPLE1D)){ */
  /*         coupledCur=0; */
  /*       } */
  /*       couplednextPrev=1; */
  /*       if ((*pW<0 && EDGES[curEdge]._bcinf.type!=COUPLE1D) || (*pW>=0 && EDGES[curEdge]._bcsup.type!=COUPLE1D)){ */
  /*         couplednextPrev=0; */
  /*       } */
  /*       pW++; */
  /*     }else{ */
  /*       curEdge=firstEdge; */
  /*       coupledCur=coupledFirst; */
  /*     } */
  /*     if (coupledPrev && coupledCur){ */
  /*       nbConnexion++; */
  /*      } */
  /*     prevEdge=curEdge; */
  /*     coupledPrev=couplednextPrev; */
     
  /*   } */
    
    
  /* } */

  indexInI1I2=0;
  fprintf(pF,"//get shared dofs between edges \n");
  fprintf(pF,"int[int] I1I2(%i);\n",2*nbConnexion);
  pW=WIRES;


for(ii=0;ii<Nwires;ii++){
    int next1D=1;
    for (jj=0;jj<Wsizes[ii];jj++){
      if (((pW[jj].isReversed && EDGES[pW[jj].indexEdge]._bcinf.type==COUPLE1D) || (!(pW[jj].isReversed) && EDGES[pW[jj].indexEdge]._bcsup.type==COUPLE1D) ) &&
          ( pW[next1D].isReversed && EDGES[pW[next1D].indexEdge]._bcsup.type==COUPLE1D || (!(pW[next1D].isReversed) && EDGES[pW[next1D].indexEdge]._bcinf.type==COUPLE1D))){
        prevEdge=pW[jj].indexEdge;
        curEdge=pW[next1D].indexEdge;
        sprintf(caux1,"i%i%is%is%i",prevEdge,ii,prevEdge,curEdge);
        sprintf(caux2,"i%i%is%is%i",curEdge,ii,prevEdge,curEdge);
        fprintf(pF,"int %s, %s;\n",caux1,caux2);
        //fprintf(pF,"if (couple1D){\n");
        fprintf(pF,"\tgetCommunDof(zoi%is%i,zoi%is%i,%s,%s);\n",ii,prevEdge,ii,curEdge,caux1,caux2);
/*      fprintf(pF,"plugCSR(M1D%is%i,M1D%is%i,%s,%s,zoi%is%i,zoi%is%i,%s,%s);\n",
              ii,prevEdge,ii,curEdge,
              caux3,caux4,
              ii,prevEdge,ii,curEdge,
              caux1,caux2);*/
        fprintf(pF,"\tI1I2[2*%i]=%s+indexBeginMat[%i*%i+%i];\n",indexInI1I2,caux1,ii,NedgesMaxPerDom,jj);
        fprintf(pF,"\tI1I2[2*%i+1]=%s+indexBeginMat[%i*%i+%i];\n",indexInI1I2,caux2,ii,NedgesMaxPerDom,(jj+1 < Wsizes[ii])?jj+1:0);
        //fprintf(pF,"}\n");
        indexInI1I2=indexInI1I2+1;
        fprintf(pF,"\n"); 
      }
      next1D=(next1D+1)%(Wsizes[ii]);
    }
    pW=pW+Wsizes[ii];
    fprintf(pF,"\n\n"); 
  }

  
  fprintf(pF,"//END MAT DEF\n");
}




void resetMatStruct(){
  int ii, jj;
  for(ii=0;ii<NB_BLOCK_LINES;ii++)
    for(jj=0;jj<NB_BLOCK_LINES;jj++)
      if (ppMS[ii+jj*NB_BLOCK_LINES])
        free(ppMS[ii+jj*NB_BLOCK_LINES]);
  free(ppMS);
//  free(pEdgeIsShared);
}


void printMatStruct(FILE *pF){
  int ii, jj;
  if (NDynEdgeType){
    fprintf(pF,"include \"updateMatrix1D.edp\"\n");
    fprintf(pF,"matrix M1=[\n");
    
  }else
    fprintf(pF,"M=[\n");
  
  for(ii=0;ii<NB_BLOCK_LINES;ii++){
    fprintf(pF,"[");
    for(jj=0;jj<NB_BLOCK_LINES;jj++){
      printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
      if (jj<NB_BLOCK_LINES-1)
        fprintf(pF,",\t");
    }
    fprintf(pF,"]");
    if(ii<NB_BLOCK_LINES-1)
      fprintf(pF,",");
    fprintf(pF,"\n");
  }
  fprintf(pF,"];\n");
}
/* 
 *
 *
 * outpout: NB_BLOCK_LINES the number of blocks per lines. This means the global matrix is NB_BLOCK_LINESxNB_BLOCK_LINES of blocks.
The structure is the same for each species.
A line is like:
  |----------speci 0------------------------------------|----------speci 1------|----------speci x-------| the line is subdivided by NbSpecies sub-line.
  |------------wire 0-------|---- wire 1 ---|---wire x--|            Each sub-line is subdivided by NbWire sub-sub-line
  |2D block|1D blocks.......|  Each sub-sub-line contains a 2D blocks and  1d blocks for each edges (Wsize[.] 1d blocks).
 * 
 *
 * output: ppMS, the block matrix. ppMS[i+j*NB_BLOCK_LINES] is either NULL or a MatrixStruct element, it is the element of the line i raw j.
 *
 */
void initMatStruct(){
  int ii, jj,numSpecies,numSpecies2;
  int nbBlockLines,nbBlockCols;
  MatrixStruct * pAux=0;
  int * IndexOf2DBlocksInSubLine=0;
  IndexOf2DBlocksInSubLine=(int*)malloc(Nwires*sizeof(int));
//  pEdgeIsShared=(int*)calloc(NEdges,sizeof(int));
  NB_BLOCK_LINES=Nwires;
  if (NDynEdgeType){
    for (ii=0;ii<Nwires;ii++)
      NB_BLOCK_LINES+=Wsizes[ii];
  }
  IndexOf2DBlocksInSubLine[0]=0;
  for (ii=1;ii<Nwires;ii++){
    IndexOf2DBlocksInSubLine[ii]=IndexOf2DBlocksInSubLine[ii-1]+1;
    if (NDynEdgeType)
      IndexOf2DBlocksInSubLine[ii]+=Wsizes[ii-1];
  }
  
  NB_BLOCK_LINES_PER_SPECIES=NB_BLOCK_LINES;
  NB_BLOCK_LINES=NB_BLOCK_LINES*NbSpecies;
//  nbBlockLines=NbSpecies*Nwires;
//  nbBlockCols=NbSpecies*Nwires;
  ppMS=( MatrixStruct**)calloc(NB_BLOCK_LINES*NB_BLOCK_LINES,sizeof( MatrixStruct**));
  
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    //les blocs 2D
    int offsetSpecies=numSpecies*NB_BLOCK_LINES_PER_SPECIES;
    int curIndex=offsetSpecies;
    int curLine,curCol;
    for (ii=0;ii<Nwires;ii++){
      pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
      ppMS[curIndex+curIndex*NB_BLOCK_LINES]=pAux;
      pAux->type=VARF2D;
      pAux->index2D1=ii;
      pAux->numSpecies=numSpecies;
     
      curIndex=curIndex+1+Wsizes[ii];
    }
    //les couplages 2D
    for (ii=0;ii<Nwires;ii++){
      curLine=offsetSpecies+IndexOf2DBlocksInSubLine[ii];
      for (numSpecies2=0;numSpecies2<NbSpecies;numSpecies2++){
        int offsetSpecies2=numSpecies2*NB_BLOCK_LINES_PER_SPECIES;
        curCol=offsetSpecies2+IndexOf2DBlocksInSubLine[ii];
        if ( numSpecies!=numSpecies2 && isCoupled(ii,numSpecies,numSpecies2)){
          pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
          ppMS[curLine+curCol*NB_BLOCK_LINES]=pAux;
          pAux->type=COUPLAGE_SPECIES;
          pAux->index2D1=ii;
          pAux->numSpecies=numSpecies;
          pAux->numSpeciesCoupled=numSpecies2;
	   
        }
      }
       
    }
    
    curIndex=offsetSpecies;
    if (NDynEdgeType){
    //le blocs 1D
    curIndex=offsetSpecies;
    struct wireSide *pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      curIndex++;
      for (jj=0;jj<Wsizes[ii];jj++){
        pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
        ppMS[curIndex+curIndex*NB_BLOCK_LINES]=pAux;
        pAux->type=VARF1D;
        pAux->index2D1=ii;
        pAux->index1D1=pW->indexEdge;
        pAux->numSpecies=numSpecies;
        /* if ((*pW)<0){ */
        /*   pEdgeIsShared[pW->indexEdge]=1; */
        /* } */
        pW++;
        curIndex++;
      }
    }
    //1D --> 2D
    int line2D=0;
    int curCol=0;
    pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      curCol++;
      for (jj=0;jj<Wsizes[ii];jj++){
        pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
        ppMS[(line2D+offsetSpecies)+(curCol+offsetSpecies)*NB_BLOCK_LINES]=pAux;
        pAux->type=COUPLAGE_1Dto2D;
        pAux->index2D1=ii;
        pAux->index1D1=pW->indexEdge;
        pAux->numSpecies=numSpecies;
        curCol++;
        pW++;
      }
      line2D=line2D+1+Wsizes[ii];
    }

    //2D --> 1D
    int col2D=0;
    int curLine=offsetSpecies;
    pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      curLine++;
      for (jj=0;jj<Wsizes[ii];jj++){
        pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
        ppMS[curLine+(col2D+offsetSpecies)*NB_BLOCK_LINES]=pAux;
        pAux->type=COUPLAGE_2Dto1D;
        pAux->index2D1=ii;
        pAux->index1D1=pW->indexEdge;
        pAux->numSpecies=numSpecies;
        curLine++;
        pW++;
      }
      col2D=col2D+1+Wsizes[ii];
    }
    
 
    //couplage domain
    struct wireSide *pWneg=WIRES;
    int numEdge=0;
    struct edgePop *curEdgePop=&(aEdgePop[EDGES[pWneg->indexEdge].popType]);
    int lineNeg=0;
    int NumNegWire,NumNegEdge,NumPosWire,NumPosEdge;

    int indexNegWire=0;
    for (NumNegWire=0;NumNegWire<Nwires;NumNegWire++){
      indexNegWire=lineNeg;
      lineNeg++;
      
      for (NumNegEdge=0;NumNegEdge<Wsizes[NumNegWire];NumNegEdge++){
        if (pWneg->isReversed){
          EDGES[pWneg->indexEdge]._isShared=1;
          curEdgePop=&(aEdgePop[EDGES[pWneg->indexEdge].popType]);
          printf("*** Wneg=%i\n",pWneg->indexEdge);
          int colPos=0;
          int indexPosWire=0;
          struct wireSide *pWpos=WIRES;
          for (NumPosWire=0;NumPosWire<Nwires;NumPosWire++){
            indexPosWire=colPos;
            colPos++;
            
            for (NumPosEdge=0;NumPosEdge<Wsizes[NumPosWire];NumPosEdge++){
              if (pWpos->indexEdge==pWneg->indexEdge){
                //exchange between edges 
                if(aEdgeEdgeInter[curEdgePop->indexEEInter].alpha[numSpecies]!=0){
                  printf("*** Wpos=%i\n",pWpos->indexEdge);
                  pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
                  ppMS[(colPos+offsetSpecies)+(lineNeg+offsetSpecies)*NB_BLOCK_LINES]=pAux;
                  pAux->type=COUPLAGE_DOMAIN;
                  pAux->index2D1=NumNegWire;
                  pAux->index1D1=pWneg->indexEdge;
                  pAux->index2D2=NumPosWire;
                  pAux->index1D2=pWpos->indexEdge;
                  pAux->numSpecies=numSpecies;
                  pAux->indexEEInter=curEdgePop->indexEEInter;
                  pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
                  ppMS[(lineNeg+offsetSpecies)+(colPos+offsetSpecies)*NB_BLOCK_LINES]=pAux;
                  pAux->type=COUPLAGE_DOMAIN;
                  pAux->index2D2=NumNegWire;
                  pAux->index1D2=pWneg->indexEdge;
                  pAux->index2D1=NumPosWire;
                  pAux->index1D1=pWpos->indexEdge;
                  pAux->numSpecies=numSpecies;
                  pAux->indexEEInter=curEdgePop->indexEEInter;
                  
                }
                //exchange between 2D domain
                if (1 || aEdgeEdgeInter[curEdgePop->indexEEInter].gamma[numSpecies]!=0){
                  pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
                  pAux->pNextMatStruct=NULL;
                  if (ppMS[indexPosWire+offsetSpecies+(indexNegWire+offsetSpecies)*NB_BLOCK_LINES])
                    pAux->pNextMatStruct= ppMS[indexPosWire+offsetSpecies+(indexNegWire+offsetSpecies)*NB_BLOCK_LINES];
                  ppMS[indexPosWire+offsetSpecies+(indexNegWire+offsetSpecies)*NB_BLOCK_LINES]=pAux;
                  
                  pAux->type=COUPLAGE_CONTACT_2Dto2D;
                  pAux->index2D2=NumNegWire;
                  pAux->index1D2=pWneg->indexEdge;
                  pAux->index2D1=NumPosWire;
                  pAux->index1D1=pWpos->indexEdge;
                  pAux->numSpecies=numSpecies;
                  pAux->indexEEInter=curEdgePop->indexEEInter;
                  printf("COUPLAGE_CONTACT_2Dto2D %i \n",numSpecies);
                  pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
                  pAux->pNextMatStruct=NULL;
                  if (ppMS[indexNegWire+offsetSpecies+(indexPosWire+offsetSpecies)*NB_BLOCK_LINES])
                    pAux->pNextMatStruct= ppMS[(indexNegWire+offsetSpecies)+(indexPosWire+offsetSpecies)*NB_BLOCK_LINES];
                  ppMS[indexNegWire+offsetSpecies+(indexPosWire+offsetSpecies)*NB_BLOCK_LINES]=pAux;
                  pAux->type=COUPLAGE_CONTACT_2Dto2D;
                  pAux->index2D1=NumNegWire;
                  pAux->index1D1=pWneg->indexEdge;
                  pAux->index2D2=NumPosWire;
                  pAux->index1D2=pWpos->indexEdge;
                  pAux->numSpecies=numSpecies;
                  pAux->indexEEInter=curEdgePop->indexEEInter;
                }
                //to exit
                NumPosWire=Nwires; //to break the research of positive edge
                break; //looking for next shared edge
              }
              colPos++;
              pWpos++;
            }
          }
        }
        lineNeg++;
        pWneg++;
        numEdge++;
      }
    }
    }
  }
  free(IndexOf2DBlocksInSubLine);
}





#define WITHMASSECOMPUTE
void printMassInfo(FILE *pF){
  int ii,jj,numSpecies;
//  struct wireSide *pW=WIRES;
  int numEdge=0;
//  struct edgePop* pedgePop=&(aEdgePop[EdgeTypes[pW->indexEdge]]);
  fprintf(pF,"(1)curstep ");
  int cmpinfo=2;
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
//    pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"(%i)masse2D%c%i ",cmpinfo++,spec,ii);
      fprintf(pF,"(%i)masse1D%c%i ",cmpinfo++,spec,ii);
    }
  }
}





void printsolToDof(FILE *pF){
  int ii,jj,numSpecies;
  struct wireSide *pW=WIRES;
  int numEdge=0;
  
  struct edgePop* pedgePop=NULL;
  if (NDynEdgeType)
    pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
//  fprintf(pF,"\tinclude \"updateParam.edp\";\n");
  fprintf(pF,"\t//sol to dof\n");
  
  
/*  fprintf(pF,"\t\t    for (int ii=0;ii<sol.n;ii++)\n");
  fprintf(pF,"\t\t    if (sol[ii]<0)\n");
  fprintf(pF,"\t\t      sol[ii]=0;\n");*/

  
  fprintf(pF,"\t\tcurDof=0;\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    pW=WIRES;
    char spec=specNames[numSpecies];
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"\t\t%c2d%i[]=sol(curDof:curDof+n2Ddofs%i-1);\n",spec,ii,ii);
      fprintf(pF,"\t\t%c2d%i[]-=%ck2d%i[];\n",spec,ii,spec,ii);
      fprintf(pF,"\t\tresidu=residu+%c2d%i[].l2;\n",spec,ii);
      fprintf(pF,"\t\t%ck2d%i[]=dampNR*sol(curDof:curDof+n2Ddofs%i-1)+(1-dampNR)*%ck2d%i[];\n",spec,ii,ii,spec,ii);
      fprintf(pF,"\t\tcurDof=curDof+n2Ddofs%i;\n",ii);
      if (NDynEdgeType)
	for (jj=0;jj<Wsizes[ii];jj++){
	  fprintf(pF,"\t\tp%c1dk%is%i=sol(curDof: curDof+n1Ddofs%is%i-1);\n",spec,
		  ii,pW->indexEdge,ii,pW->indexEdge);
	  fprintf(pF,"\t\tcurDof=curDof+n1Ddofs%is%i;\n",ii,pW->indexEdge);
	  pW++;
	}
    }
    fprintf(pF,"\n");
  }
  
  /*pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"\tconvert1Dto2D2(pu1d%is%i,utm1%is%i,n1Ddofs%is%i,zoi%is%i);\n",
              ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge);
      pW++;
    }
    }*/



}

void printTSOneStepW(){
  int p,numSpecies,ii,jj;
  struct wireSide *pW=WIRES;
  sprintf(FILENAME,"%stimeSteppingOneStepWGen.edp",OUTPUTDIR);
  FILE *pF=fopen(FILENAME, "w");
  fprintf(pF,"//simulation des P systemes\n");
  fprintf(pF,"//maj de F_k \n");
  fprintf(pF,"include \"setFuncFGenK.edp\";\n");
  fprintf(pF,"//la linearise de F en U\n");
  fprintf(pF,"include \"buildMatGen.edp\";\n");
  fprintf(pF,"//maj des gradient_P\n");
  fprintf(pF,"include \"setdPFGen.edp\"\n\n");
  for (p=1;p<nParams+1;p++){
    fprintf(pF,"//calcul de W%i\n",p);
    fprintf(pF,"//le second menbre\n");
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      for(ii=0;ii<Nwires;ii++){
        fprintf(pF,"\t\trhs%c2d%i=varfRhsdP%if2d%i%c(0,Vh%i);\n",specNames[numSpecies],ii,p,ii,specNames[numSpecies],ii);
      }
    }
    fprintf(pF,"\t\trhs=[");
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      pW=WIRES;
      for(ii=0;ii<Nwires;ii++){
        fprintf(pF,"rhs%c2d%i",specNames[numSpecies],ii);
        if (NDynEdgeType){
          fprintf(pF,",");
          for (jj=0;jj<Wsizes[ii];jj++){
            fprintf(pF,"rhs%c1d%is%i",specNames[numSpecies],ii,pW->indexEdge);
            pW++;
            if (jj<Wsizes[ii]-1 || ii<Nwires-1 || (numSpecies+1)<NbSpecies)
              fprintf(pF,",");
          }
        }else{
          if (ii<Nwires-1 || numSpecies+1<NbSpecies)
            fprintf(pF,",");
        }
        if (ii<Nwires-1 || numSpecies<NbSpecies)
          fprintf(pF,"\n\t\t\t");
        
      }
    }
    fprintf(pF,"];\n");
    fprintf(pF,"sol=M^-1*rhs;\n");
    fprintf(pF,"include \"solToW%i.edp\"\n",p);
    fprintf(pF,"\n");
    fprintf(pF,"\n");
 
  }
  
  fclose(pF);
}

void printsolToW(){
  int p;

  for (p=1;p<nParams+1;p++){
    sprintf(FILENAME,"%ssolToW%i.edp",OUTPUTDIR,p);
    FILE *pF=fopen(FILENAME, "w");
    int ii,jj,numSpecies;
    struct wireSide *pW=WIRES;
    int numEdge=0;
  
    struct edgePop* pedgePop=NULL;
    if (NDynEdgeType)
      pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
    fprintf(pF,"\t//sol to W%i\n",p);
  
    fprintf(pF,"\t\tcurDof=0;\n");
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      pW=WIRES;
      char spec=specNames[numSpecies];
      for (ii=0;ii<Nwires;ii++){
        fprintf(pF,"\t\tW%i%c2d%i[]=sol(curDof:curDof+n2Ddofs%i-1);\n",p,spec,ii,ii);
        fprintf(pF,"\t\tW%i%ctm12d%i[]=W%i%c2d%i[];\n",p,spec,ii,p,spec,ii);
        fprintf(pF,"\t\tcurDof=curDof+n2Ddofs%i;\n",ii);
        if (NDynEdgeType)
          for (jj=0;jj<Wsizes[ii];jj++){
            fprintf(pF,"\t\tp%c1dk%is%i=sol(curDof: curDof+n1Ddofs%is%i-1);\n",spec,
                    ii,pW->indexEdge,ii,pW->indexEdge);
            fprintf(pF,"\t\tcurDof=curDof+n1Ddofs%is%i;\n",ii,pW->indexEdge);
            pW++;
          }
      }
      fprintf(pF,"\n");
    }
  
    fclose(pF);
  }

}


void printTS(FILE *pF){
  int ii,jj,numSpecies;
  struct wireSide *pW=WIRES;
  int numEdge=0;

  struct edgePop* pedgePop=NULL;
  if (NDynEdgeType)
    pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);

  
  fprintf(pF,"//BEGIN TIME STEPPING\n");
#ifdef WITH_POST_PRO
  fprintf(pF,"//Load post processing.\n");
  fprintf(pF,"include \"postTraitementLoad.edp\";\n");
#endif
  
#ifdef WITHMASSECOMPUTE
  fprintf(pF,"//Reset mass file\n");
  fprintf(pF,"{ofstream pMass(OUTPUTDIR+\"mass.txt\");}\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"{ofstream pTmp(OUTPUTDIR+\"control%i.txt\");}\n",ii);
  }
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char charSpec=specNames[numSpecies];
    fprintf(pF,"real %cmaxDensity=0;\n",charSpec);
    fprintf(pF,"real %cminDensity=100000;\n",charSpec);
  }
#endif
fprintf(pF,"int curStep=0;\n");
 if (NDynEdgeType){
   fprintf(pF,"macro my1Dto2D{\n");
   for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
     char charSpec=specNames[numSpecies];
     pW=WIRES;
     for(ii=0;ii<Nwires;ii++){
       for (jj=0;jj<Wsizes[ii];jj++){
	 fprintf(pF,"\tconvert1Dto2D2(p%c1d%is%i,%c1D%is%itm1,n1Ddofs%is%i,zoi%is%ibis);\n",
		 charSpec,ii,pW->indexEdge,charSpec,ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge);
	 pW++;
       }
     }
   }
   fprintf(pF,"}//\n");
 }
 //begin func updateMinMax
 fprintf(pF,"func real updateMinMax(){\n");
 fprintf(pF,"\treal buf=0;\n");
 for(ii=0;ii<Nwires;ii++)
   for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
     char charSpec=specNames[numSpecies];
     fprintf(pF,"\tbuf=%c2d%i[].max;\n",charSpec,ii);
     fprintf(pF,"\tif ( buf> %cmaxDensity)\t%cmaxDensity = buf;\n",charSpec,charSpec);
     fprintf(pF,"\tbuf=%c2d%i[].min;\n",charSpec,ii);
     fprintf(pF,"\tif ( buf< %cminDensity)\t%cminDensity = buf;\n",charSpec,charSpec);
   }
 fprintf(pF,"}\n");
 //end func updateMinMax
  
#ifdef WITHMASSECOMPUTE
 fprintf(pF,"func real computeMass(){\n\t\tofstream pMass(OUTPUTDIR+\"mass.txt\",append);\n");
 fprintf(pF,"\t\tpMass<<curStep<<\" \";\n");
 if (NDynEdgeType)
   fprintf(pF,"\t\tmy1Dto2D;\n");
 for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
   char spec=specNames[numSpecies];
   fprintf(pF,"\t\treal masse2D%c=0;\n",spec);
   if (NDynEdgeType){
     fprintf(pF,"\t\treal masse1D%c=0;\n",spec);
     fprintf(pF,"\t\treal masse1D%cbis=0;\n",spec);
   }
 }
 if (NDynEdgeType)
   fprintf(pF,"\t\treal masse1Dbuf=0;\n");
 
 for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
   char spec=specNames[numSpecies];
   pW=WIRES;
   for (ii=0;ii<Nwires;ii++){
     if (NDynEdgeType){
       fprintf(pF,"\t\t//compute 1D extremal density\n");
       fprintf(pF,"\t\tmasse1Dbuf=%ctm12d%i[].max;\n",spec,ii);
       fprintf(pF,"\t\tif (masse1Dbuf>%cmaxDensity && curStep > 0.5*nbSteps)\n\t\t\t%cmaxDensity=masse1Dbuf;\n",spec,spec);
       fprintf(pF,"\t\tmasse1Dbuf=%c2d%i[].min;\n",spec,ii);
       fprintf(pF,"\t\tif (masse1Dbuf<%cminDensity && curStep > 0.5*nbSteps)\n\t\t\t%cminDensity=masse1Dbuf;\n",spec,spec);
     }
     fprintf(pF,"\t\t//compute 2D masse of %c in domain %i\n",spec,ii);
     fprintf(pF,"\t\tmasse2D%c=int2d(Th%i)(%ctm12d%i);\n",spec,ii,spec,ii);
     fprintf(pF,"\t\tpMass<<masse2D%c<<\" \";\n",spec);
     if (NDynEdgeType){
       fprintf(pF,"\t\t//compute 1D masse of %c in border domain %i\n",spec,ii);
       fprintf(pF,"\t\tmasse1D%c=0;\n",spec);
       fprintf(pF,"\t\tmasse1D%cbis=0;\n",spec);
       for (jj=0;jj<Wsizes[ii];jj++){
	 /*       fprintf(pF,"\t\tcomput1d(masse1Dbuf,steps%is%i,p%c1d%is%i,n1Ddofs%is%i);\n",ii,pW->indexEdge,spec,ii,pW->indexEdge,ii,pW->indexEdge);
		  fprintf(pF,"\t\tmasse1D%c+=masse1Dbuf;\n",spec);
		  fprintf(pF,"\t\tmasse1D%cbis+=int1d(Th%is%i,1)(u1D%is%itm1);\n",spec,ii,pW->indexEdge,ii,pW->indexEdge);*/
         fprintf(pF,"\t\tmasse1D%c+=int1d(Th%is%i,1)(%c1D%is%itm1);\n",
                 spec,
                 ii,pW->indexEdge,
                 spec,ii,pW->indexEdge);
         pW++;
       }
       //     fprintf(pF,"\t\tcout<<masse1D%c<<\" \"<<masse1D%cbis<<\" \";\n",spec,spec);
       fprintf(pF,"\t\tpMass<<masse1D%c<<\" \";\n",spec);
     }
   }
 }
 fprintf(pF,"\t\tpMass<<endl;\n}\n");
#endif
#ifdef WITHMASSECOMPUTE
 fprintf(pF,"computeMass();\n");
#endif
/* for (numSpecies=0; numSpecies<NbSpecies; numSpecies++){
   for (ii=0;ii<Nwires;ii++){
     fprintf(pF,"\tvisuScal(\"2D%i\",Th%i,%ctm12d%i,10);\n",ii,ii,specNames[numSpecies],ii);
   }
   }*/

 fprintf(pF,"curStep=0;\n");
 fprintf(pF,"curIndexDate=0;\n");
 fprintf(pF,"curT=T0;\n");
#ifdef WITH_GRADIENT_P
 fprintf(pF,"include \"defWVarGen.edp\";\n");
 fprintf(pF,"include \"defdPFGen.edp\";\n");
 fprintf(pF,"include \"defWVarfGen.edp\";\n");
#endif
 fprintf(pF,"include \"defSaveVtk.edp\";\n");

 
 if (slowStart){
   fprintf(pF,"  dt=0.1*dt;\n");
   fprintf(pF,"  alpha=1.0/dt;\n");
   fprintf(pF,"  include \"preTraitement.edp\";\n");
   fprintf(pF,"  for ( curStep=1;curStep<=10;curStep++){\n");
   fprintf(pF,"    real prevT=T0+(curStep-1)*dt;\n");
   fprintf(pF,"    real curT=T0+curStep*dt;\n");
   fprintf(pF,"    include \"timeSteppingOneStepGen.edp\";\n");
   fprintf(pF,"  }\n");
   fprintf(pF,"  dt=10.0*dt;\n");
   fprintf(pF,"  alpha=1.0/dt;\n");
   fprintf(pF,"  for ( curStep=2;curStep<=nbSteps;curStep++){\n");
 }else{
   fprintf(pF,"  include \"preTraitement.edp\";\n");
   fprintf(pF,"for ( curStep=1;curStep<=nbSteps;curStep++){\n");
   fprintf(pF,"    real curT=T0+curStep*dt;\n");
}
 fprintf(pF,"  curT+=dt;\n");
  fprintf(pF,"  include \"timeSteppingOneStepGen.edp\";\n");
  fprintf(pF,"\tif (curStep%%freqVtk == 0){\n");
  fprintf(pF,"\t\tinclude \"defSaveVtk.edp\";\n");
  fprintf(pF,"\t}\n");

  

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defSaveVtk.edp");
  FILE *pVtk = fopen(FILENAME, "w");
  
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
   char spec=specNames[numSpecies];
   pW=WIRES;
   for (ii=0;ii<Nwires;ii++){
   
     fprintf(pVtk,"\t\tsavevtk(OUTPUTDIR+\"D2/%c%is\"+string(curStep)+\".vtk\",Th%i,%ctm12d%i,dataname=\"UU\");\n",spec,ii,ii,spec,ii);
     if (NDynEdgeType){
       fprintf(pVtk,"\t\t{\n");
       fprintf(pVtk,"\t\t\tofstream pWire(OUTPUTDIR+\"D1/%c1d\"+string(curStep)+\"s%i.txt\");\n",spec,ii);
       for (jj=0;jj<Wsizes[ii];jj++){
	 int notLast=1;
	 int revers=0;
	 if (pW->isReversed)
	   revers=1;
	 if (jj==Wsizes[ii]-1)
	   notLast=0;
	 fprintf(pVtk,"\t\t\tprintVect(pWire,%i,p%c1dtm1%is%i,n1Ddofs%is%i-%i);\n",revers,spec,ii,pW->indexEdge,ii,pW->indexEdge,notLast);
    
	 pW++;
       }
       fprintf(pVtk,"\t\t}\n");

     }
   }
  }
//  fprintf(pVtk,"\t}\n");

  fclose(pVtk);
  
/*  fprintf(pF,"\tif (curStep%%freqAff == 0){\n");
  for (numSpecies=0; numSpecies<NbSpecies; numSpecies++){
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"\t\tvisuScal(\"2D%i\",Th%i,%c2d%i,10);\n",ii,ii,specNames[numSpecies],ii);
    }
  }
  fprintf(pF,"\t}\n");*/
#ifdef WITH_POST_PRO
  fprintf(pF,"\tif (curStep%%freqPostPro == 0){\n");
  fprintf(pF,"\t\tinclude \"postTraitement.edp\";\n");
  fprintf(pF,"\t}\n");
#endif
  
  fprintf(pF,"\t\n");
  fprintf(pF,"\t\n");

  fprintf(pF,"}\n");
#ifdef WITH_POST_PRO
  fprintf(pF,"\tinclude \"postTraitementEnd.edp\";\n");
#endif
  fprintf(pF,"if (curIndexDate != NbTargetDates)\n");
  fprintf(pF,"cout<<\"WARNING, curIndexDate != NbTargetDates.\\n\"<<endl;\n");
fprintf(pF,"//END TIME STEPPING\n");
  
}

void printViewVtk(){
  int ii,kk;
//  char filename[256];
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"view.sh");
  FILE *pmain=fopen(FILENAME, "w");
  
  for(kk=0; kk<10;kk++){
//    sprintf(filenameFILENAME,"view%i.py",kk);
    sprintf(FILENAME,"%sview%i.py",OUTPUTDIR,kk);
    fprintf(pmain,"python %s\n",FILENAME);
    FILE *pVtk = fopen(FILENAME, "w");
    fprintf(pVtk,"from paraview.simple import *\n");
    fprintf(pVtk,"import sys\nbaseName=\"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/u\"\n");
    fprintf(pVtk,"freq=10\n");
    fprintf(pVtk,"minv=0.0003\n");
    fprintf(pVtk,"maxv=0.0005\n");

    fprintf(pVtk,"for num in range(%i,%i):\n",kk*20+1,(kk+1)*20+1);
    for(ii=0;ii<Nwires;ii++){
      fprintf(pVtk,"\tfileName%i=baseName+str(%i)+str(freq*num)+\".vtk\"\n",ii,ii);
      fprintf(pVtk,"\treader%i = OpenDataFile(fileName%i)\n",ii,ii);
    }
    fprintf(pVtk,"\tShow()\n");
    fprintf(pVtk,"\tview = GetActiveView()\n\tview.CameraPosition = [0,0,100]\n\tview.ViewSize = [1000, 1000]\n\t#view.CameraViewAngle = 90\n\t#Render()\n");

    for(ii=0;ii<Nwires;ii++){
      fprintf(pVtk,"\tdp%i = GetDisplayProperties(reader%i)\n",ii,ii);
      fprintf(pVtk,"\tdp%i.Representation = 'Surface'\n",ii);
      fprintf(pVtk,"\telev%i = Elevation(reader%i)\n",ii,ii);
      fprintf(pVtk,"\tdp%i.LookupTable = MakeBlueToRedLT(minv, maxv)\n",ii);
      fprintf(pVtk,"\tdp%i.ColorArrayName = 'UU'\n",ii);
    }
    fprintf(pVtk,"\t#Render()\n");
    fprintf(pVtk,"\tWriteImage(baseName+str(freq*num)+\".png\")\n");
    for(ii=0;ii<Nwires;ii++){
      fprintf(pVtk,"\tDelete(reader%i)\n\tdel reader%i\n",ii,ii);
    }
    fprintf(pVtk,"\tdel view");
    fclose(pVtk);
  }
  fclose(pmain);
}

void printSaveMeshes(){
  char filename[256];
  int ii;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"genSaveMeshes.edp");
  FILE *ps=fopen(FILENAME, "w");
  for(ii=0;ii<Nwires;ii++){
    fprintf(ps,"savemesh(Th%i,\"th%i.msh\");\n",ii,ii);
  }
  fclose(ps);
}


void printVtkViewWires(){
  int ii,kk,numEdge,jj;
  char filename[256];
  char sbuff[4096];
  struct wireSide *pW,*pWaux;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"viewWires.py");
  FILE *pmain=fopen(FILENAME, "w");
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"genBuildWires.edp");
  FILE *pbuildActors=fopen(FILENAME, "w");
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"addActorWires.py");
  FILE *paddActors=fopen(FILENAME, "w");
  fprintf(pmain,"import vtk\n");
  fprintf(pmain,"execfile(\"buildActorWires.py\")\n");
  
  //fprintf(pbuildActors,"points = vtk.vtkPoints()\n");
  //fprintf(pbuildActors,"points.SetNumberOfPoints(%i)\n",NNodes);
  fprintf(pmain,"ren = vtk.vtkRenderer()\n");
  fprintf(pmain,"execfile(\"addActorWires.py\")\n");
  fprintf(pbuildActors,"pyWire<<\"RLine=1\"<<endl;\npyWire<<\"GLine=1\"<<endl;\npyWire<<\"BLine=1\"<<endl;\npyWire<<\"widthLine=2.5\"<<endl;\n");
  fprintf(pbuildActors,"\n");
//  for(ii=0;ii<NNodes;ii++){
//    fprintf(pbuildActors,"points.SetPoint(%i, %e, %e, 0.1)\n",ii,PointsX[ii],PointsY[ii]);
//  }
//  fprintf(pbuildActors,"\n");
  
  pW=WIRES;
  for(ii=0;ii<Nwires;ii++){
    fprintf(pbuildActors,"curPt=0;\n");
    fprintf(pbuildActors,"nAux=");
    pWaux=pW;
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pbuildActors,"n1Ddofs%is%i",ii,pWaux->indexEdge);
      pWaux++;
      if (jj+1<Wsizes[ii])
        fprintf(pbuildActors,"+");
    }
    fprintf(pbuildActors,";\n");
    sprintf(sbuff,"points%i = vtk.vtkPoints()",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"points%i.SetNumberOfPoints(\"<<nAux<<\")",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"lines%i = vtk.vtkCellArray()",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"lines%i.InsertNextCell(\"<<nAux+1<<\")",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    for (jj=0;jj<Wsizes[ii];jj++){
      numEdge=pW->indexEdge;
      fprintf(pbuildActors,"cout<<\"call with edge %i \";\n",numEdge);
      fprintf(pbuildActors,"actorWire(%c2d%i,p%c1d%is%i,p%c1dtm1%is%i,n1Ddofs%is%i,zoi%is%i,%i,%i);\n",
              specNames[0],ii,specNames[0],ii,numEdge,specNames[0],ii,numEdge,ii,numEdge,ii,numEdge,ii,(pW->isReversed)?-1:1);
      pW++;
    }
    sprintf(sbuff,"lines%i.InsertCellPoint(0);",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    fprintf(pbuildActors,"\n");
    sprintf(sbuff,"polygon%i = vtk.vtkPolyData()",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"polygon%i.SetPoints(points%i)",ii,ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"polygon%i.SetLines(lines%i)",ii,ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    fprintf(pbuildActors,"\n");

    fprintf(pbuildActors,"\n");
    
    sprintf(sbuff,"polygonMapper%i = vtk.vtkPolyDataMapper()",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"if vtk.VTK_MAJOR_VERSION <= 5:");fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"\\tpolygonMapper%i.SetInputConnection(polygon%i.GetProducerPort())",ii,ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"else:");fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"\\tpolygonMapper%i.SetInputData(polygon%i)",ii,ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"\\tpolygonMapper%i.Update()",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"polygonActor%i = vtk.vtkActor()",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"polygonActor%i.SetMapper(polygonMapper%i)",ii,ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);

    fprintf(paddActors,"ren.AddActor(polygonActor%i)\n",ii);
    sprintf(sbuff,"polygonActor%i.GetProperty().SetColor(RLine,GLine,BLine)",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    sprintf(sbuff,"polygonActor%i.GetProperty().SetLineWidth(widthLine)",ii);fprintf(pbuildActors,"pyWire<<\"%s\"<<endl;\n",sbuff);
    fprintf(pbuildActors,"\n");
    fprintf(pmain,"\n");
  }
  fprintf(pmain,"ren.SetBackground(0.1, 0.2, 0.4)\n");
  fprintf(pmain,"ren.ResetCamera()\n");
  fprintf(pmain,"renWin = vtk.vtkRenderWindow()\n");
  fprintf(pmain,"renWin.AddRenderer(ren)\n");
  fprintf(pmain,"renWin.SetSize(500, 500)\n");
  fprintf(pmain,"iren = vtk.vtkRenderWindowInteractor()\niren.SetRenderWindow(renWin)\niren.Initialize()\niren.Start()\n");
  fprintf(pbuildActors,"\n");
  fprintf(pmain,"\n");
  fclose(pmain);
  fclose(pbuildActors);
  fclose(paddActors);
}
//n is a number of polygone
void printFuncexp(FILE *pF,int n){
  double x,y;
  int kk;
  int indInWIRES,isav;
  double length,auxlength;
  indInWIRES=0;
  for (kk=0;kk<n;kk++)
    indInWIRES+=Wsizes[kk];
  
  length=0;
  
  //baricenter
  for(kk=0;kk<Wsizes[n];kk++){
    int iPt=GET_EDGE_NODE_BEGIN(WIRES[indInWIRES].indexEdge);//EDGES[2*abs(WIRES[indInWIRES])]
    int iPt2=GET_EDGE_NODE_END(WIRES[indInWIRES].indexEdge);//EDGES[2*abs(WIRES[indInWIRES])+1];
    auxlength=(PointsX[iPt]-PointsX[iPt2])*(PointsX[iPt]-PointsX[iPt2])+(PointsY[iPt]-PointsY[iPt2])*(PointsY[iPt]-PointsY[iPt2]);
    if (auxlength > length)
      length=auxlength;
    iPt=GET_EDGE_NODE_BEGIN(WIRES[indInWIRES].indexEdge);//EDGES[2*abs(WIRES[indInWIRES])];
    x+=PointsX[iPt];
    y+=PointsY[iPt];
    indInWIRES++;
  }
  x=x/Wsizes[n];
  y=y/Wsizes[n];
  fprintf(pF,"//length %i\n",n);
  fprintf(pF,"real length%i=%e;\n",n,length);
  fprintf(pF,"real x%i=%e;\n",n,x);
  fprintf(pF,"real y%i=%e;\n",n,y);
  fprintf(pF,"real xnoise%i=0;\n",n);
  fprintf(pF,"real ynoise%i=0;\n",n);
  fprintf(pF,"func real fct%i(real v, real w){\n",n);
  fprintf(pF,"real dist2=(x%i+xnoise%i-v)*(x%i+xnoise%i-v)+(y%i+ynoise%i-w)*(y%i+ynoise%i-w);\n",n,n,n,n,n,n,n,n);
  fprintf(pF,"\tif (dist2>0.25*0.25*length%i)\n",n);  
  fprintf(pF,"\t\treturn 0;\n");  
  fprintf(pF,"\treturn exp(-2.30*(dist2)/length%i);\n",n);

  fprintf(pF,"}\n");
  fprintf(pF,"//indicatrice Fct %i\n",n);
  fprintf(pF,"func real fctI%i(real v, real w){\n",n);
  fprintf(pF,"\tif ((%e-%c)*(%e-%c)+(%e-%c)*(%e-%c)<0.25*0.25*%e)\n",x,'v',x,'v',y,'w',y,'w',length);  
  fprintf(pF,"\t\treturn 1;\n");  
  fprintf(pF,"\treturn 0;\n");

  fprintf(pF,"}\n");
  
}
//n is a number of polygone
void printFunc(FILE *pF,int n){
  double x=0;double y=0;
  int kk;
  int indInWIRES;
  double length,auxlength;
  double ratio=0.3;
  indInWIRES=0;
  for (kk=0;kk<n;kk++)
    indInWIRES+=Wsizes[kk];

  length=0;
  
  //baricenter
  for(kk=0;kk<Wsizes[n];kk++){
    int iPt=GET_EDGE_NODE_BEGIN(WIRES[indInWIRES].indexEdge);//EDGES[2*abs(WIRES[indInWIRES])];
    int iPt2=GET_EDGE_NODE_END(WIRES[indInWIRES].indexEdge);//EDGES[2*abs(WIRES[indInWIRES])+1];
    auxlength=(PointsX[iPt]-PointsX[iPt2])*(PointsX[iPt]-PointsX[iPt2])+(PointsY[iPt]-PointsY[iPt2])*(PointsY[iPt]-PointsY[iPt2]);
    if (auxlength>length)
      length=auxlength;

    
    iPt=GET_EDGE_NODE_BEGIN(WIRES[indInWIRES].indexEdge);//EDGES[2*abs(WIRES[indInWIRES])];
    x+=PointsX[iPt];
    y+=PointsY[iPt];
    indInWIRES++;
  }
  x=x/Wsizes[n];
  y=y/Wsizes[n];
  fprintf(pF,"//Fct %i\n",n);
  fprintf(pF,"real length%i=%e;\n",n,length);
  fprintf(pF,"real x%i=%e;\n",n,x);
  fprintf(pF,"real y%i=%e;\n",n,y);
  fprintf(pF,"real xnoise%i=0;\n",n);
  fprintf(pF,"real ynoise%i=0;\n",n);
   fprintf(pF,"func real fct%i(real v, real w){\n",n);
   fprintf(pF,"real dist2Ratio=((x%i+xnoise%i-v)*(x%i+xnoise%i-v)+(y%i+ynoise%i-w)*(y%i+ynoise%i-w))/length%i;\n",n,n,n,n,n,n,n,n,n);
  fprintf(pF,"\tif (dist2Ratio>0.25*0.25)\n");  
  fprintf(pF,"\t\treturn 0;\n");  
  //  fprintf(pF,"\treturn exp(-2.30*dist2Ratio);\n",n);
  fprintf(pF,"\treturn exp(-2.30*dist2Ratio);\n");

  fprintf(pF,"}\n");
  fprintf(pF,"//indicatrice Fct %i\n",n);
  fprintf(pF,"func real fctI%i(real v, real w){\n",n);
  fprintf(pF,"\treal aux=(%e-%c)*(%e-%c)+(%e-%c)*(%e-%c);\n",x,'v',x,'v',y,'w',y,'w');
  fprintf(pF,"\tif (aux<0.25*0.25*%e){\n",length);
  fprintf(pF,"\t\taux=aux/(0.25*0.25*%e);\n",length);
	fprintf(pF,"\t\t	return 1-aux;\n\t}\n");

  fprintf(pF,"\treturn 0;\n");

  fprintf(pF,"}\n");
  
}
void printInitialCond(FILE *pF,int IDCAS){
  int numSpecies,ii,jj;
  struct wireSide *pW;
  //size_t l=strlen(POSTPRODIR);
  int done=0;
  /*if (l){
    printf("printInitialCond, %zd\n",l);
    //strlen exludes null byte '\0'
    char *fileName=(char *)malloc((l-2+13+1)*sizeof(char));
    char *aux=fileName;
    strcpy(fileName,POSTPRODIR);
    aux=aux+(l-2);
    strcpy(aux,"ICDefault.edp");
    //printf("printInitialCond, looking for IC in file %s\n",fileName);
    FILE *pFaux=fopen(fileName,"r");
    if (pFaux){
      done=1;
      fclose(pFaux);
      sprintf(FILENAME,"%s%s%s%s","cp ",fileName," ",OUTPUTDIR);
      system(FILENAME);
      
      fprintf(pF,"include \"%s\"\n",fileName);
      for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
        char spec=specNames[numSpecies];
        for(ii=0;ii<Nwires;ii++){
          fprintf(pF,"%ctm12d%i=fctI%iDefault(x,y);\n",spec,ii,ii);
        }
      }
      for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
        char spec=specNames[numSpecies];
        pW=WIRES;
        for (ii=0;ii<Nwires;ii++){
          for (jj=0;jj<Wsizes[ii];jj++){
            fprintf(pF,"p%c1d%is%i=%e;\n",spec,ii,pW->indexEdge,aEdgePop[EDGES[pW->indexEdge].popType].IC[numSpecies]);
            printf("p%c1d%is%i=%e;\n",spec,ii,pW->indexEdge,aEdgePop[EDGES[pW->indexEdge].popType].IC[numSpecies]);
            pW++;
          }
        }
      }
    }
    }*/
  if (!done || IDCAS){
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      char spec=specNames[numSpecies];
      for(ii=0;ii<Nwires;ii++){
        if (!numSpecies && NDynEdgeType) printFunc(pF,ii);
        double kaux=aFieldPop[FieldTypes[ii]].K[numSpecies];
        //if (kaux!=0)
        //fprintf(pF,"%ctm12d%i=%e*K%c%i*fct%i(x,y);\n",spec,ii,((double)rand())/((double)RAND_MAX),spec,ii,ii);
        //fprintf(pF,"%ctm12d%i=%e*fctI%i(x,y);\n",spec,ii,aFieldPop[FieldTypes[ii]].IC[numSpecies],ii);
        fprintf(pF,"%ctm12d%i=%e;\n",spec,ii,aFieldPop[FieldTypes[ii]].IC[numSpecies]);
        //else
        //fprintf(pF,"%ctm12d%i=%e*0.5;\n",spec,ii,((double)rand())/((double)RAND_MAX));
      }
    }
    if (NDynEdgeType){
      for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
        char spec=specNames[numSpecies];
        pW=WIRES;
        for (ii=0;ii<Nwires;ii++){
          for (jj=0;jj<Wsizes[ii];jj++){
            fprintf(pF,"p%c1dtm1%is%i=%e;\n",spec,ii,pW->indexEdge,aEdgePop[EDGES[pW->indexEdge].popType].IC[numSpecies]);
            printf("p%c1dtm1%is%i=%e;\n",spec,ii,pW->indexEdge,aEdgePop[EDGES[pW->indexEdge].popType].IC[numSpecies]);
            pW++;
          }
        }
      }
    }
  }
  
}


/*void randomizePoint(double amp){
  int ii;
  for (ii=0;ii<NNodes;ii++){
    Nx[ii]+=(amp*rand())/RAND_MAX;
    Ny[ii]+=(amp*rand())/RAND_MAX;
  }
    
  }*/
void printPluginVariable(FILE *pF){
  fprintf(pF,"real SourceDuration=0.1;\n");
//  fprintf(pF,"real SourcePeriod=7;\n");
//  fprintf(pF,"real xSourceNoise=0.25;\n");
//  fprintf(pF,"real ySourceNoise=0.25;\n");
  fprintf(pF,"real TherholdPest=0.2;\n");
  fprintf(pF,"real TraiteDuration=0.1;\n");
  fprintf(pF,"real TraiteTRamp=0.05;\n");
//  fprintf(pF,"real Tt=5;\n");
//  fprintf(pF,"real Tnt=1;\n");
  fprintf(pF,"real Kt=0.01;\n");
  fprintf(pF,"real Knt=20;\n");
  fprintf(pF,"real scaleSource=2;\n");
  fprintf(pF,"\n");
  int ii;
    fprintf(pF,"int[int] SourceState(Nwires);\n");
    fprintf(pF,"real[int] SourceTstart(Nwires);\n");
    fprintf(pF,"real[int] SourceTstop(Nwires);\n");
    fprintf(pF,"real[int] SourceNextT(Nwires);\n");
    
    fprintf(pF,"int[int] TraiteState(Nwires);\n");
    fprintf(pF,"real[int] TraiteStart(Nwires);\n");
    fprintf(pF,"real[int] TraiteStop(Nwires);\n");
    
     fprintf(pF,"for (int li;li<Nwires;li++){\n");
   fprintf(pF,"//Field li\n");
    fprintf(pF," SourceState[li]=0;\n");
    fprintf(pF," SourceTstart[li]=0.1;\n");
    fprintf(pF," SourceTstop[li]=0.1;\n");
    fprintf(pF," SourceNextT[li]=Tf+1;\n");
    
    fprintf(pF," TraiteState[li]=0;\n");
    fprintf(pF," TraiteStart[li]=0;\n");
    fprintf(pF," TraiteStop[li]=0;\n");
    fprintf(pF,"}\n");
}

void printXXYY(FILE *pF){
  int ii;
  for(ii=0;ii<Nwires;ii++){
    fprintf(pF,"Vh%i xx%i=x;\n",ii,ii);
    fprintf(pF,"Vh%i yy%i=y;\n",ii,ii);
  }
}
//just for demo
void printPlugin(FILE *pF){
  
  int ii;
  char charSpec0=specNames[0];
  //fprintf(pF,"real curT=T0+curStep*dt;\n");
  fprintf(pF,"//Control or not?\nif (%i){\n",withControl);
  for(ii=0;ii<Nwires;ii++){
    if (!FieldTypes[ii])
      continue;
    fprintf(pF,"//Field %i dynamic %i\n",ii,FieldTypes[ii]);
    fprintf(pF,"if (TraiteState[%i]==0){\n",ii);
    fprintf(pF,"\treal IntegralPest=int2d(Th%i)(%ctm12d%i);\n",ii,charSpec0,ii);
    fprintf(pF,"\tIntegralPest=IntegralPest/Surf%i;\n",ii);
    fprintf(pF,"\t//declanchement du traitement?\n");
    fprintf(pF,"\tif (IntegralPest >TherholdPest ){\n");
    fprintf(pF,"\t\tcout<<\"traitement debut  %i\"<<endl;\n",ii);
    fprintf(pF,"\t\tTraiteState[%i]=1;\n",ii);
    fprintf(pF,"\t\tTraiteStart[%i]=curT ;\n",ii);
    fprintf(pF,"\t\tTraiteStop[%i]=curT+ TraiteDuration;\n",ii);
//    fprintf(pF,"\t\tT%iu%i=Tt ;\n",ii,FieldTypes[ii]);
    //fprintf(pF,"\t\tMalu%i=-1;;\n",ii);
    fprintf(pF,"\t\tofstream pTmp(\"control%i.txt\",append);;\n",ii);
    fprintf(pF,"\t\treal vmax=%ctm12d%i[].max;\n",charSpec0,ii);
    fprintf(pF,"\t\tint imax=%ctm12d%i[].imax;\n",charSpec0,ii);
    fprintf(pF,"\t\tpTmp<<\"debut traitement \"<<curT<<\" -> \"<<TraiteStop[%i]<<\" \";\n",ii);
    fprintf(pF,"\t\tpTmp<<vmax<<\" \"<<IntegralPest<<\" \"<<xx%i[][imax]<<\" \"<<yy%i[][imax]<<endl;\n",ii,ii);
    fprintf(pF,"\t\tfor (int li=0;li<n2Ddofs%i;li++){\n",ii);
    fprintf(pF,"\t\t   if (utm12d%i[][li]>Kt){\n",ii);
    fprintf(pF,"\t\t     utm12d%i[][li]=Kt;\n",ii);
    fprintf(pF,"\t\t     u2d%i[][li]=Kt;\n",ii);
    fprintf(pF,"\t\t   }\n");
    fprintf(pF,"\t\t}\n");

    
    fprintf(pF,"\t}\n");
    fprintf(pF,"}\n");

    
    fprintf(pF,"if (TraiteState[%i] == 1){\n",ii);
//    fprintf(pF,"\tif (curT<TraiteStart[%i]+TraiteTRamp+0.0001){;\n",ii);
//    fprintf(pF,"\t\treal taux=(TraiteStart[%i]+TraiteTRamp-curT)/TraiteTRamp;\n",ii);
//    fprintf(pF,"\t\treal vKt=Kt*(1-taux)+Knt*taux;\n");
//    fprintf(pF,"\tcout<<\"newton, set vKt %i=\"<<vKt<<endl;\n",ii);
//    fprintf(pF,"\t\tK%iu%i=vKt;\n",ii,FieldTypes[ii]);
//    fprintf(pF,"\t}else{\n");
    fprintf(pF,"\t\tK%iu%i=Kt;\n",ii,FieldTypes[ii]);
//    fprintf(pF,"\t}\n");
    
    fprintf(pF,"\tif (curT>TraiteStop[%i]){\n",ii);
    fprintf(pF,"\t\tK%iu%i=Knt;\n",ii,FieldTypes[ii]);
    //   fprintf(pF,"\t\tT%iu%i=Tnt ;\n",ii,FieldTypes[ii]);

    fprintf(pF,"\t\tTraiteState[%i]=0;\n",ii);
    fprintf(pF,"\t\tofstream pTmp(OUTPUTDIR+\"control%i.txt\",append);;\n",ii);
    fprintf(pF,"\t\tpTmp<<\"traitement fin \"<<curT<<endl;\n");
    //fprintf(pF,"\t\tMalu%i=0;\n",ii);
    fprintf(pF,"\t\tcout<<\"control fin traitement %i\"<<endl;\n",ii);
    fprintf(pF,"\t}\n");
    fprintf(pF,"}\n");
  }
  fprintf(pF,"}\n");

  fprintf(pF,"//Source or not?\nif (%i){\n",withSource);
  for(ii=0;ii<Nwires;ii++){
    if (!FieldTypes[ii])
      continue;
    fprintf(pF,"//Source %i\n",ii);
    fprintf(pF,"if (SourceState[%i]==0){\n",ii);
    fprintf(pF,"\tif (SourceNextT[%i]){\n",ii);
    fprintf(pF,"\t\tSourceState[%i]=1;\n",ii);
    fprintf(pF,"\t\tSourceTstart[%i]=curT;\n",ii);
    //   fprintf(pF,"\t\txnoise%i=length%i*xSourceNoise*(0.5-randreal1());\n",ii,ii);
//    fprintf(pF,"\t\tynoise%i=length%i*ySourceNoise*(0.5-randreal1());\n",ii,ii);
    fprintf(pF,"\t\tsource2D%iu%i=scaleSource*fct%i(x,y);\n",ii,FieldTypes[ii],ii);
    fprintf(pF,"\t\treal aauuxx=SourceDuration;\n");
//    fprintf(pF,"\t\tcout<<\" source %i duration \"<<aauuxx <<\" \"<<SourceDuration<<endl;\n",ii);
    fprintf(pF,"\t\tSourceTstop[%i]=curT+aauuxx;\n",ii);
    fprintf(pF,"\t\tcout<<\"control source %i begin \"<<curT << \" \"<<curStep  <<\" \"<<SourceTstop[%i] <<\" with noise \"<<xnoise%i <<\" \"<<ynoise%i<<endl;\n",ii,ii,ii,ii);
    fprintf(pF,"\t\tofstream pTmp(OUTPUTDIR+\"control%i.txt\",append);\n",ii);
    fprintf(pF,"\t\tpTmp<<\"source \"<<SourceTstart[%i]<< \" \"<<curT<<\" \"<<SourceTstop[%i]<<\" with noise \"<<xnoise%i <<\" \"<<ynoise%i<<endl;\n",ii,ii,ii,ii);
    fprintf(pF,"\t}\n");
    fprintf(pF,"}\n");

    fprintf(pF,"if (SourceState[%i]==1){\n",ii);
    fprintf(pF,"\tif (curT>SourceTstop[%i]){\n",ii);
    fprintf(pF,"\t\tSourceState[%i]=0;\n",ii);
    fprintf(pF,"\t\tSourceNextT[%i]=Tf+1;\n",ii);
    
    fprintf(pF,"\t\tsource2D%iu%i=0;\n",ii,FieldTypes[ii]);
    //fprintf(pF,"\t\tSourceNextT%i=curT+SourcePeriod*randreal1();\n",ii);
    fprintf(pF,"\t\tcout<<\"source %i end \"<<curT<<endl;\n",ii);
    fprintf(pF,"\t\tofstream pTmp(OUTPUTDIR+\"control%i.txt\",append);\n",ii);
    fprintf(pF,"\t\tpTmp<<\"source end \"<<curT<<endl;\n");
    fprintf(pF,"\t}\n");
   
    fprintf(pF,"}\n");
  }
  fprintf(pF,"}\n");

}

void printVTKGENPY(FILE *pF){
  int numS;
  fprintf(pF," pyFile<<\"species=array(\'c\',\'");
  for ( numS=0;numS<NbSpecies;numS++)
    fprintf(pF,"%c",specNames[numS]);
  fprintf(pF,"\');\"<<endl;\n");
  for ( numS=0;numS<NbSpecies;numS++){
    fprintf(pF," pyFile<<\"%cmaxDensity=\"<<%cmaxDensity<<endl;\n",specNames[numS],specNames[numS]);
    fprintf(pF," pyFile<<\"%cminDensity=\"<<%cminDensity<<endl;\n",specNames[numS],specNames[numS]);
  }
  
  fprintf(pF," pyFile<<\"minV=array('d',[");
  for ( numS=0;numS<NbSpecies;numS++){
    fprintf(pF,"%cminDensity",specNames[numS]);
    if (numS+1<NbSpecies)
      fprintf(pF,",");
  }
  fprintf(pF," ])\"<<endl;\n");

  fprintf(pF," pyFile<<\"maxV=array('d',[");
  for ( numS=0;numS<NbSpecies;numS++){
    fprintf(pF, "%cmaxDensity",specNames[numS]);
    if (numS+1<NbSpecies)
      fprintf(pF,",");
  }
  fprintf(pF," ])\"<<endl;\n");
  
  
}

void _buildVtkGen(){
  int iw;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"doVtkGen.edp");
  FILE *pF=fopen(FILENAME,"w");
  fprintf(pF,"cout<<\"loading file \"<<\"SIMU/\"+specNames(numSpec:numSpec)+\"s\"+(prevStep)+\"2Dx\"<<endl;\n");
  fprintf(pF,"cout<<\"loading file \"<<\"SIMU/\"+specNames(numSpec:numSpec)+\"s\"+(curStep)+\"2Dx\"<<endl;\n\n");
  for (iw=0;iw<Nwires;iw++){
    fprintf(pF,"LoadVec(tabtm12d%i[numSpec][],\"SIMU/\"+specNames(numSpec:numSpec)+\"s\"+(prevStep)+\"2D%i\");\n",iw,iw);
    fprintf(pF,"LoadVec(tab2d%i[numSpec][],\"SIMU/\"+specNames(numSpec:numSpec)+\"s\"+curStep+\"2D%i\");\n",iw,iw);
    fprintf(pF,"tabk2d%i[numSpec]= (1-coef)*tabtm12d%i[numSpec]+ coef*tab2d%i[numSpec];\n",iw,iw,iw);
    fprintf(pF,"savevtk(\"SIMU/VTK/\"+specNames(numSpec:numSpec)+\"%is\"+string(curTargetDate)+\".vtk\",Th%i,tabk2d%i[numSpec],dataname=\"UU\");\n\n",iw,iw,iw);
  }
  fclose(pF);
}

int buildDirectories(){
  int ii;
  struct stat st = {0};
  
  if (stat(OUTPUTDIR, &st) == -1) {
    mkdir(OUTPUTDIR, 0700);
  }
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"D2");
  if (stat(FILENAME, &st) == -1) {
    mkdir(FILENAME, 0700);
  }
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"D1");
  if (stat(FILENAME, &st) == -1) {
    mkdir(FILENAME, 0700);
  }
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"SIMU");
  if (stat(FILENAME, &st) == -1) {
    mkdir(FILENAME, 0700);
  }

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"SIMU/VTK");
  if (stat(FILENAME, &st) == -1) {
    mkdir(FILENAME, 0700);
  }

  sprintf(FILENAME,"cp %s/EDPSRC/freefem++.pref %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

  sprintf(FILENAME,"cp %s/EDPSRC/myMacro.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

  sprintf(FILENAME,"cp %s/EDPSRC/beforeOneStep.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);
  
  sprintf(FILENAME,"cp %s/EDPSRC/toolsAdaptiveTstepping.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

  sprintf(FILENAME,"cp %s/EDPSRC/timeSteppingOneStepGenAdaptive.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

  sprintf(FILENAME,"cp %s/EDPSRC/timeSteppingAdaptive.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

  sprintf(FILENAME,"cp %s/EDPSRC/userRestoreState.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

  sprintf(FILENAME,"cp %s/EDPSRC/userSaveState.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

  sprintf(FILENAME,"cp %s/EDPSRC/doVtk.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);
  _buildVtkGen();

  sprintf(FILENAME,"cp %s/EDPSRC/defVisuGen.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

  sprintf(FILENAME,"cp %s/EDPSRC/defPluginVariableGen.edp %s\n",SRCDIR,OUTPUTDIR);
  printf("doing :%s\n",FILENAME);
  system(FILENAME);

 
  for (ii=0;ii<NFilesCopy;ii++){
    sprintf(FILENAME,"cp %s %s",FILES_COPY[ii],OUTPUTDIR);
    printf("doing :%s\n",FILENAME);
    system(FILENAME);
  }
  return 0;
}

int buildDefOutput(FILE *pF){
  int ii;
  fprintf(pF," string OUTPUTDIR=\"%s\";\n",OUTPUTDIR);
  fprintf(pF,"{ofstream pTmp(OUTPUTDIR+\"Surfaces.txt\");\n");
  for (ii=0;ii<Nwires;ii++)
    fprintf(pF,"pTmp<<\"Surf%i=\"<<Surf%i<<endl;\n",ii,ii);
  fprintf(pF,"}\n");

}

int buildRunSGEFile(FILE *pF){
  fprintf(pF,"#!/bin/bash\n");
  fprintf(pF,"#$ -S /bin/bash\n");
  fprintf(pF,"#$ -N D2D1\n");
  fprintf(pF,"#$ -V\n");
  fprintf(pF,"#$ -cwd\n");
  fprintf(pF,"#$ -q all.q\n");
  fprintf(pF,"/opt/freefem++/src/nw/FreeFem++-nw %s/cas1.edp\n",OUTPUTDIR);

}

void printUserF(){
  int nDyn,iw;
  int numS1,numS2;
  FILE * pF;
  sprintf(FILENAME,"%s/setFuncFGenX.edp",OUTPUTDIR);
  FILE * pFX=fopen(FILENAME,"w");
  sprintf(FILENAME,"%s/setFuncFGenK.edp",OUTPUTDIR);
  FILE * pFK=fopen(FILENAME,"w");
  for (nDyn=0;nDyn<NDynFieldType;nDyn++){
    sprintf(FILENAME,"%s%s%i%s",OUTPUTDIR,"userFDef",nDyn,".txt");
    if(access( FILENAME, F_OK ) != -1){
      printf("file %s exits, do nothing\n",FILENAME);
      //return;
    }else{
    pF= fopen(FILENAME, "w");
    
    for (numS1=0;numS1<NbSpecies;numS1++){
      fprintf(pF,"f2d%i%c=0;\n",nDyn,specNames[numS1]);
      for (numS2=0;numS2<NbSpecies;numS2++){
        fprintf(pF,"d%cf2d%i%c=0;\n",specNames[numS2],nDyn,specNames[numS1]);
        
      }
    }
    fclose(pF);
    }
  }
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"genSetFunc.sh");
  pF= fopen(FILENAME, "w");
  for (iw=0;iw<Nwires;iw++){
    fprintf(pF,"sed ");
    for (numS1=0;numS1<NbSpecies;numS1++){
      fprintf(pF," 's/$%c/%c2d%i/g' ",specNames[numS1],specNames[numS1],iw);
      if (numS1==0){
        fprintf(pF," userFDef%i.txt | sed 's/=/%i=/g' | sed 's/$dom/%i/g'",FieldTypes[iw],iw,iw);
      }
      if (numS1+1<NbSpecies)
        fprintf(pF," | sed ");
    }
    fprintf(pF," > setFuncFGenX%i.edp\n",iw);
    fprintf(pFX,"include \"setFuncFGenX%i.edp\";\n",iw);
    fprintf(pF,"sed 's/2d/2dk/g' userFDef%i.txt | sed 's/=/%i=/g' | sed 's/$dom/%i/g' | sed ",FieldTypes[iw],iw,iw);
    for (numS1=0;numS1<NbSpecies;numS1++){
      fprintf(pF," 's/$%c/%ck2d%i/g' ",specNames[numS1],specNames[numS1],iw);
      if (numS1+1<NbSpecies)
        fprintf(pF," | sed ");
    }
    fprintf(pF," > setFuncFGenK%i.edp\n",iw);
    fprintf(pFK,"include \"setFuncFGenK%i.edp\";\n",iw);
  }
  fclose(pF);
  fclose(pFX);
  fclose(pFK);
}


