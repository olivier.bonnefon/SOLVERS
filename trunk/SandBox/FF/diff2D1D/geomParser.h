#ifndef GEOMPARSER_H
#define GEOMPARSER_H
#include "geomStruct.h"
#include <stdio.h>
extern int * FieldTypes;
//int * EdgeTypes=0;

//The points:
extern int NPoints;
extern double *PointsX;
extern double *PointsY;
extern double scalCoord;
extern double scalXCoord;
extern double scalYCoord;
extern double Xmin;
extern double Xmax;
extern double Ymin;
extern double Ymax;

//The nodes
extern int NNodes;
extern int *Nodes;
//The edges
extern int *NodeEdges;
extern int *PopTypeEdges;
extern int *NumberOfPointsInEdge;
//int *lcouple1D=NULL;
//The domains
extern int* pEdgeIsReversedInWire;
extern int  BUILD_MESH;
extern int NFilesCopy;
extern char FILES_COPY[][256];//max copy 200 files
extern int *NodesNumber;
extern char  GEODIR[];
//0 do nothing
//1 x <--> y
extern int symGeom;
FILE * openPointsFile();

FILE * openEdgesFile();

FILE * openWiresFile();


void readPoints();

void printNodes(int n);
void printBB();
void readEdges();





void readPointsInEdges();

int readWires();



#endif
