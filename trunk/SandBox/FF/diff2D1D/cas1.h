int NbSpecies=1;
#include "systemDescr.h"
int NPoints=4;
double Px[]={-1,1,1,-1};
double Py[]={-1,-1,1,1};
//double Px[]={10., 0,  10, 0.,10};
//double Py[]={10., 10, 0,  0.,5};

int NEdges=4;
int EDGES[]={
             0,1,
             1,2,
             2,3,
             3,0
             };


int Nwires=1;//one wire per domain
int Wsizes[]={4};
int WIRES[]={0,1,2,3};
int ABSWIRES[]={0,1,2,3};

//  -1,4,5,6,7};

int nPerLength=6;
int nEFinter=1;
int nEEinter=0;

void initStructure(){
  int ii;
  allocStructs();
  aFieldPop[0].D2DX[0]=0.5;
  aFieldPop[0].D2DY[0]=0.5;
  aFieldPop[0].K[0]=2;
  aFieldPop[0].R[0]=5;
  aFieldPop[0].source[0]=0.00;

 
  //species [1] doesn't cross the edge.
  aEdgeFieldInter[0].mu[0]=0.0;
  aEdgeFieldInter[0].nu[0]=0.0;

  printFieldPops(aFieldPop,Nwires);
}



