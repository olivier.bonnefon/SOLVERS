#include "systemDescr.h"
#include "geomStruct.h"
#include <stdio.h>
#include <stdlib.h>
struct fieldPop* aFieldPop=NULL;
struct edgePop* aEdgePop=NULL;
//The array of Edge/Edge interaction type
struct edgeEdgeInter* aEdgeEdgeInter=NULL;
//The array of Edge/field interaction type
struct edgeFieldInter* aEdgeFieldInter=NULL;
//just print
void printFieldPops(struct fieldPop * pMod,int n){
  int ii,jj,numS,numS2;
  int numEdge=0;
  
  struct edgePop* pedgePop=0;

  printf("BEGIN PRINT DYNAMICS Of %i POPS\n",NbSpecies);
  for (numS=0;numS<NbSpecies;numS++){
    printf("About pop %i:\n",numS);
    printf("\tAbout 2D dynamic type, there are %i type :\n",NDynFieldType);
    for (ii=0;ii<NDynFieldType;ii++){
      printf("\t\t->2D type %i\n",ii);
      printf("\t\t\tD2DX=%e\n",aFieldPop[ii].D2DX[numS]);
      printf("\t\t\tD2DY=%e\n",aFieldPop[ii].D2DY[numS]);
      printf("\t\t\tK=%e\n",aFieldPop[ii].K[numS]);
      printf("\t\t\tR=%e\n",aFieldPop[ii].R[numS]);
      printf("\t\t\tT=%e\n",aFieldPop[ii].T[numS]);
      printf("\t\t\trho=%e\n",aFieldPop[ii].rho[numS]);
      printf("\t\t\tCmalthus=%e\n",aFieldPop[ii].Cmalthus[numS]);
      for (numS2=0;numS2<NbSpecies;numS2++)
	printf("\t\t\tCcoup=%e\t",aFieldPop[ii].Ccoup[numS][numS2]);
      printf("\n");
    }
    printf("\tAbout 1D dynamic type, there are %i type :\n",NDynEdgeType);
    for (ii=0;ii<NDynEdgeType;ii++){
      printf("\t\t->1D type %i\n",ii);
      printf("\t\tD=%e\n",aEdgePop[ii].D[numS]);
      printf("\t\tsource=%e\n",aEdgePop[ii].source[numS]);
      printf("\t\tCmalthus=%e\n",aEdgePop[ii].Cmalthus[numS]);
      
    }
    printf("\tAbout 1D/2D type connextion, there are %i type :\n",nEFinter);
    for(ii=0;ii<nEFinter;ii++){
      printf("\t\t->1D/2D type %i %p\n",ii,&(aEdgeFieldInter[ii]));
      printf("\t\t(1D to 2D)mu=%e\n",aEdgeFieldInter[ii].mu[numS]);
      printf("\t\t(2D to 1D)nu=%e\n",aEdgeFieldInter[ii].nu[numS]);
      
    }
    printf("\tAbout 1D/1D type connextion, there are %i type :\n",nEEinter);
    for(ii=0;ii<nEEinter;ii++){
      printf("\t\t->1D/1D type %i %p\n",ii,&(aEdgeEdgeInter[ii]));
      printf("\t\talpha=%e\n",aEdgeEdgeInter[ii].alpha[numS]);
      
    }
  }
  printf("\tAbout dynamics per domain %i \n",ii);
  struct wireSide *pW=WIRES;
  for (ii=0;ii<n;ii++){
    printf("\t\tDomain 2D %i has dinamic %i\n",ii,FieldTypes[ii]);
    printf("\t\t\tAbout 1D:\n");
    if (NDynEdgeType)
      for (jj=0;jj<Wsizes[ii];jj++){
        int popType=EDGES[pW->indexEdge].popType;
        pedgePop=&(aEdgePop[popType]);
        printf("\t\t\t edge %i dynamic type %i\n",pW->indexEdge,popType);
        printf("\t\t\t edge %i connected to 2D with %i\n",pW->indexEdge,pedgePop->indexEFInter);
        printf("\t\t\t edge %i connected to 1D with %i\n",pW->indexEdge,pedgePop->indexEEInter);
//      numEdge++;
        pW++;
      }
  
  }
  printf("END PRINT DYNAMICS POPS\n");
}

//return 1 iff the spec 'numSpecies2' apears in the system of 'numSpecies1'
int isCoupled(int numDom, int numSpecies1, int numSpecies2){
//  if (NbSpecies<2 || numSpecies1==numSpecies2)
//    return 0;
//  else{
//    if (aFieldPop[FieldTypes[numDom]].Ccoup[numSpecies1][numSpecies2]!=0)
//      return 1;
//  }
  return 1;
}

int allocStructs(){
  int ii,numS;
  if (NbSpecies > MAX_NB_SPECIES){
    printf("Erreur: NbSpecies > MAX_NB_SPECIES, %d > %d.\n",NbSpecies,MAX_NB_SPECIES);
    exit(1);
  }
  //alloc memory for each type of fields dynamique
  aFieldPop=(struct fieldPop *)calloc(NDynFieldType,sizeof(struct fieldPop));
  for (ii=0;ii<NDynFieldType;ii++){
    for (numS=0;numS<NbSpecies;numS++){
      aFieldPop[ii].D2DX[numS]=1;
      aFieldPop[ii].D2DY[numS]=1;
      aFieldPop[ii].T[numS]=1;
      aFieldPop[ii].rho[numS]=0.1;
      aFieldPop[ii].IC[numS]=1;
      aFieldPop[ii].IC[numS]=1;
    }
  }
  //alloc and init memory for Field type.
  //default: all the field type are 0, ie aFieldPop[0].
  FieldTypes=(int*)calloc(Nwires,sizeof(int));
  
  NEdgesInstance=0;
  aEdgeEdgeInter=0;
  if (NDynEdgeType){
    //who many edges are there ?
    for (ii=0;ii<Nwires;ii++)
      NEdgesInstance+=Wsizes[ii];
    //alloc and init memory for edge type. +1 for the loop, the last type is not used.
//  EdgeTypes=(int*)calloc(NEdges,sizeof(int));
  
    aEdgePop=(struct edgePop *)calloc(NDynEdgeType,sizeof(struct edgePop));
  
  

    //define potential edge-field interactions.
    printf("nEFinter=%i\n",nEFinter);
    aEdgeFieldInter=(struct edgeFieldInter *)calloc(nEFinter,sizeof(struct edgeFieldInter));
    for (ii=0;ii<nEFinter;ii++){
      //2D to 1D
      for (numS=0;numS<NbSpecies;numS++)
        aEdgeFieldInter[ii].mu[numS]=1.0;
    
      //1D to 2D
      for (numS=0;numS<NbSpecies;numS++)
        aEdgeFieldInter[ii].nu[numS]=0.5;
    
    }
  
  
  
    aEdgeEdgeInter=(struct edgeEdgeInter *)calloc(nEEinter,sizeof(struct edgeEdgeInter));
    for (ii=0;ii<nEEinter;ii++){
      for (numS=0;numS<NbSpecies;numS++){
        aEdgeEdgeInter[ii].alpha[numS]=1.0;
        aEdgeEdgeInter[ii].gamma[numS]=0;
      }
    }
    //affect interaction EdgeEdge to edge.
    //init edge type
    //affect interaction EdgeField to edge type.
    for (ii=0;ii<NDynEdgeType;ii++){
      for (numS=0;numS<NbSpecies;numS++){
        aEdgePop[ii].D[numS]=1;
        aEdgePop[ii].source[numS]=0;
        aEdgePop[ii].Cmalthus[numS]=0;
      }
      //if (ii%2)
      aEdgePop[ii].indexEFInter=0;
      //else
      //aEdgePop[ii].pEFInter=&(aEdgeFieldInter[1]);
      aEdgePop[ii].indexEEInter=0;
    }
  }
}

int freeStructs(){
  if (NDynEdgeType){
    free(aEdgeEdgeInter);
    free(aEdgeFieldInter);
    free(aEdgePop);
  }
  free(FieldTypes);
  free(aFieldPop);
//  free(EdgeTypes);
}
/*
struct  modelKPP{
  double D2DX;
  double D2DY;
  double K[2];
  double R[2];
};


union modelParams {
  struct modelDiff mDiff;
  struct modelKPP mKpp;
};

enum typePop { DIFF ,
               KPP 
                };

struct modelPop{
  enum typePop type;
  union modelParams aModel;
};
*/
