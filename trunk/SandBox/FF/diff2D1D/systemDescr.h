#ifndef SYSTEMDESCR_H
#define SYSTEMDESCR_H

#define MAX_NB_SPECIES 4


#include "geomStruct.h"


/*
  d_t u= D[0] laplac u + R[0]u(1-u/K[0])+T[0](u-rho)u(1-u/K[0])+Cmalthus[0]*u+U2[0]*u^2+Ccoup[0] u*v + source[0]
  d_t v= D[1] laplac v + R[1]v(1-v/K[1])+Cmalthus[1]*v+Ccoup[1] u*v+ U2[1]*v^2+source[1]
 */
enum fiedPopIndex{
  D2DX_I=0,
  D2DY_I=1,
  K_I=2,
  R_I=3,
  U2_I=4,
  T_I=5,
  rho_I=6,
  Cmalthus_I=7,
  Ccoup_I=8,
  source_I=9,
  IC_I=10,
  LAST_I=11
};

struct  fieldPop{
  //Warning, for de Neumann BC, Diffusion is suposed isotrop
  double D2DX[MAX_NB_SPECIES];
  double D2DY[MAX_NB_SPECIES];
  double K[MAX_NB_SPECIES];
  double R[MAX_NB_SPECIES];
  double U2[MAX_NB_SPECIES];
  double T[MAX_NB_SPECIES];
  double rho[MAX_NB_SPECIES];
  double Cmalthus[MAX_NB_SPECIES];
  double Ccoup[MAX_NB_SPECIES][MAX_NB_SPECIES];
  double source[MAX_NB_SPECIES];
  double IC[MAX_NB_SPECIES];
  int isFct[MAX_NB_SPECIES][LAST_I];
  int isFieldDep[MAX_NB_SPECIES][LAST_I];
};

/*
  d_t u = D[0] u_{xx} + source[0] +Cmalthus[0]*u;
 */

struct edgePop{
  double D[MAX_NB_SPECIES];
  double source[MAX_NB_SPECIES];
  double Cmalthus[MAX_NB_SPECIES];
  double IC[MAX_NB_SPECIES];
  int indexEEInter;
  int indexEFInter;
//  struct edgeEdgeInter* pEEInter;
//  struct edgeFieldInter* pEFInter;
};
/*Edge edge interaction
 alpha for the edges exchanges.
gamma for 2D domaine exchanges.
Rename this struct by neighbooringInter*/
struct edgeEdgeInter{
  double alpha[MAX_NB_SPECIES];
  double gamma[MAX_NB_SPECIES];
};
/* Edge field interaction*/
struct edgeFieldInter{
  double mu[MAX_NB_SPECIES];
  double nu[MAX_NB_SPECIES];
};


//Number of Edge/Field interaction, ie size of FieldTypes
extern int nEFinter;
//Number of Edge/Edge interaction
extern int nEEinter;
//Number of point on edge per unit of length
extern double nPerLength;
//if couple1D[numEdge], means edge can be connected.
//extern int* couple1D;


/*DYNAMICAL POPULATION DESCRIPTION*/
//Number of species
extern int NbSpecies;
//Number of field type
//ie Number of 'struct  fieldPop' allocated
//each field point on a 'struct  fieldPop'
extern int NDynFieldType;
//Number of edge type
//ie Number of 'struct  edgePop' allocated
//each edge point on a 'struct  fieldPop'
extern int NDynEdgeType;
//FieldTypes[i] is the index of the field type
//ie aFieldPop[FieldTypes[i]] is the 'struct  fieldPop' of the field i.
extern int * FieldTypes;
//ie aEdgePop[Edge[i]->popType] is the 'struct  edgePop' of the edge i.
//With i the number of the edge in the WIRES array.
//extern int * EdgeTypes;
//The number of edges, ie the sum of the number of edge in each field. 
int NEdgesInstance;

//The array of field type.
extern struct fieldPop* aFieldPop;
//The array of edge type.
extern struct edgePop* aEdgePop;
//The array of Edge/Edge interaction type
extern struct edgeEdgeInter* aEdgeEdgeInter;
//The array of Edge/field interaction type
extern struct edgeFieldInter* aEdgeFieldInter;
extern int nEFinter;
extern int nEEinter;

//timestepping param
extern int slowStart;

//just print
void printFieldPops(struct fieldPop * pMod,int n);

//return 1 iff the spec 'numSpecies2' apears in the system of 'numSpecies1'
int isCoupled(int numDom, int numSpecies1, int numSpecies2);

int allocStructs();

int freeStructs();
#endif
