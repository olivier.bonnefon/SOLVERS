// SUMMARY  :   resize mat
// USAGE    : LGPL      
// ORG      : INRA
// AUTHOR   : O. BONNEFON
//


#include "ff++.hpp"
#include "AFunction_ext.hpp"

long resizeCSR(Matrice_Creuse<double>* const &A,const long &n,const long &m) {
  printf("resizeCSR A=%p\n",(void*)A);
    MatriceMorse<double>* mA = static_cast<MatriceMorse<double>*>(&(*A->A));
    map< pair<int,int>, R> Aij;
    Aij[make_pair(0,m-1)]=-1e20;
    mA=new MatriceMorse<double>(n,m,Aij,false);
    A->A=mA;
    printf("resizeCSR %p\n",(void*)mA);
    //mA->resize(n,m);
   
    return 1L;
}

long getCommunDof(KN<long>*const&ZOI1,KN<long>*const&ZOI2,  long *const &pi1,  long *const &pi2){
  long n1 = ZOI1->N();
  long n2 = ZOI2->N();
  printf("begin getCommunDof\n");
  printf("  ZOI1[0;%ld]: %ld %ld \n",n1-1,(*ZOI1)[0],(*ZOI1)[n1-1]);
  printf("  ZOI2[0;%ld]: %ld %ld \n",n2-1,(*ZOI2)[0],(*ZOI2)[n2-1]);
//  printf("__getCommunDof n1=%ld n2=%ld\n",n1,n2);
//  printf("__getCommunDof %ld %ld %ld %ld\n",(*ZOI1)[0],(*ZOI1)[n1-1],(*ZOI2)[0],(*ZOI2)[n2-1]);
  if ((*ZOI1)[0]==(*ZOI2)[0]){
    *pi1=0;
    *pi2=0;
  }
  else if ((*ZOI1[0])==(*ZOI2)[n2-1]){
    *pi1=0;
    *pi2=n2-1;
  }
  else if ((*ZOI1)[n1-1]==(*ZOI2)[0]){
    *pi1=n1-1;
    *pi2=0;
  }
  else if ((*ZOI1)[n1-1]==(*ZOI2)[n2-1]){
    *pi1=n1-1;
    *pi2=n2-1;
  }else{
    
    printf("  resizeCSR::getCommunDof Cant find commun dofs\n");
    
//    printf("ZOI1[0;%ld]: %ld %ld \n",n1-1,(*ZOI1)[0],(*ZOI1)[n1-1]);
//    printf("ZOI2[0;%ld]: %ld %ld \n",n2-1,(*ZOI2)[0],(*ZOI1)[n2-1]);
    return 1;
  }
  return 0;
}
void __setDiagCSR(MatriceMorse<double>* pA,int n,double v){
  double * pV=pA->a;
  int * lg=pA->lg;
  int * cl=pA->cl;
  int iline=n;
  for (int col=lg[iline];col<lg[iline+1];col++){
    if (cl[col]==n){
      pV[col]=v;
      break;
    }
  }
}
void __setDiagCSR2(MatriceMorse<double>* pA,int n,double v){
  double * pV=pA->a;
  int * lg=pA->lg;
  int * cl=pA->cl;
  int iline=n;
  for (int col=lg[iline];col<lg[iline+1];col++){
    if (cl[col]==n){
      pV[col]=v;
    }else{
      pV[col]=0;
    }
  }
}

//long addFluxCond(Matrice_Creuse<double>* const &Ain, Matrice_Creuse<double>* const &Aout,
/*
 * The goal is to connect edge.
 * {(*I1I2)[2*ii], (*I1I2)[2*ii+1]} contains the indeces of of edge dof that must be join, ie edge corners.
 *
 *
 *
 */
long OBtestCSR(Matrice_Creuse<double>* const &Ain,Matrice_Creuse<double>* const &Aout,KN<long>*const &I1I2,long *const & nbSpecies){
  MatriceMorse<double>* pAin = static_cast<MatriceMorse<double>*>(&(*Ain->A));
  int n=pAin->N;
  int m=pAin->M;
  int ii,numSpecies;
  printf("n=%i m=%i\n",n,m);
  map< pair<int,int>, double> Aij;
  printf("rep1\n");
  double * pV=pAin->a;
  int * lg=pAin->lg;
  int * cl=pAin->cl;
  int lineOffset=0;
  //For the first species, no offset ie lineOffset=0;
  for (numSpecies=0;numSpecies<*nbSpecies;numSpecies++){
    //get the line i2 of Ain
    //add it to the line i1 in Aij
    long nI1 = (I1I2->N())/2;
    for (ii=0; ii<nI1;ii++){
      //get the line i2 of Ain
      //add it to the line i1 in Aij
      long i1=(*I1I2)[2*ii]+lineOffset;
      long i2=(*I1I2)[2*ii+1]+lineOffset;
//      printf("i1=%ld i2=%ld\n",i1,i2);
      for (int col=lg[i2];col<lg[i2+1];col++){
        Aij[make_pair(i1,cl[col])]=pV[col];
      }
      Aij[make_pair(i2,i1)]=-1e50;
      __setDiagCSR(pAin,i2,1e50);
    }
    //offset for the second species
    lineOffset=n/2;
  }
  pAin->addMatTo(1.0,Aij,false,0,0,false,1e-14);
  printf("rep2\n");
  Aout->A=new MatriceMorse<double>(n,m,Aij,false);
  printf("rep3\n");
  
}

/*
 * Cette routine permet de changer le nombre de lignes d'une matrice en utilisant le tableau de correspondance zoi:
 * Aout_{zoi[i],*}=Ain_{i,*} 
 *
 * Elle est utilisee pour convertir une matrice 1D dans l'espace 2D, dans ce cas Ain une matrice issue d'un formulation faible sur un lineaire (matrice de masse),  nLinOut, le nombre de ligne de la matrice Aout, correspondant en fait au nombre de dofs 2D.
 *
 * input  : Ain 
 * input  : nLinOut, le nombre de ligne de la matrice Aout
 * zoi    : Les index des dofs 1d dans la matrice 2d
 * output : nouvelle matrice
 */

long OBbuildMat(Matrice_Creuse<double>* const &Ain,Matrice_Creuse<double>* const &Aout,KN<long>*const & zoi,long* const &nLinOut){
  MatriceMorse<double>* pAin = static_cast<MatriceMorse<double>*>(&(*Ain->A));
  map< pair<int,int>, R> Aij;
  int N=pAin->N;
  int M=pAin->M;
  int n1dDofs=zoi->N();
  if (N!=n1dDofs){
    printf("erreur in resizeCSR, OBbuildMat because n!=n1dDofs\n");
    exit(1);
  }
  double * pV=pAin->a;
  int * lg=pAin->lg;
  int * cl=pAin->cl;
  double * aux=pV;
  int * auxcl=cl;
  for (int iline=0;iline<N;iline++)
  {
    int ncC=lg[iline+1]-lg[iline];
    for (int col=0;col<ncC;col++){
      Aij[make_pair((*zoi)[iline],* auxcl)]=*aux;
      auxcl++;aux++;
    }
  }
  Aout->A=new MatriceMorse<double>(*nLinOut,M,Aij,false);
}

long setDiagCSR(Matrice_Creuse<double>* const &A,const long &n,const double &v) {
  printf("resizeCSR A=%p\n",(void*)A);
    MatriceMorse<double>* mA = static_cast<MatriceMorse<double>*>(&(*A->A));
    if (n>=0){
      __setDiagCSR(mA,n,v);
    }else {
      int N=mA->N;
      for (int ii=0;ii<N;ii++)
        __setDiagCSR2(mA,ii,v);
    }
    return 1L;
}

long printCSR(Matrice_Creuse<double>* const &A) {
  MatriceMorse<double>* mA = static_cast<MatriceMorse<double>*>(&(*A->A));
  double * pV=mA->a;
  int * lg=mA->lg;
  int * cl=mA->cl;
  int N=mA->N;
  int M=mA->M;
  double * aux=pV;
  int * auxcl=cl;
  int curCol=0;
  printf("[");
  for (int iline=0;iline<N;iline++)
  {
    int ncC=lg[iline+1]-lg[iline];
    curCol=0;
    printf("[");
    for (int col=0;col<ncC;col++){
      for(int ii=curCol;ii<*auxcl;ii++)
        printf("0,");
      printf("%e",*aux);
      if (*auxcl<N-1)
         printf(",");
      curCol=*auxcl+1;
      auxcl++;aux++;
    }
    for(int ii=curCol;ii<M;ii++){
        printf("0");
        if (ii<N-1)
          printf(",");
    }
    printf("];\n");
  }
  printf("]");
    return 1L;
}
long plugCSR(Matrice_Creuse<double>* const &A1,Matrice_Creuse<double>* const &A2,
             Matrice_Creuse<double>* const &A12,Matrice_Creuse<double>* const &A21, long* const &ci1,  long* const &ci2) {
  printf("plugCSR \n");
  long *i1=(long *)ci1;
  long *i2=(long *)ci2;
  
  //long i1,i2;
  printf("plugCSR i1=%ld i2=%ld\n",*i1,*i2);
  MatriceMorse<double>* pA1 = static_cast<MatriceMorse<double>*>(&(*A1->A));
  MatriceMorse<double>* pA2 = static_cast<MatriceMorse<double>*>(&(*A2->A));
  map< pair<int,int>, double> A12ij;
  long n=pA2->N;
  long m=pA1->M;
  map< pair<int,int>, double> A21ij;
  A21ij[make_pair(*i2,*i1)]=-1e50;
  A21->A=new MatriceMorse<double>(n,m,A21ij,false);
  
  double * pV=pA2->a;
  int * lg=pA2->lg;
  int * cl=pA2->cl;
  int iline=*i2;
  for (int col=lg[iline];col<lg[iline+1];col++){
    A12ij[make_pair(*i1,cl[col])]=pV[col];
  }
  A12->A=new MatriceMorse<double>(pA1->N,pA2->M,A12ij,false);
  __setDiagCSR(pA2,*i2,1e50);
  return 0;
}
//It build Aout with the same size than Amodel, with the line i2 of Ain at the line i1.
long getLineMatrice(Matrice_Creuse<double>* const &Ain,Matrice_Creuse<double>* const &Amodel,
                    Matrice_Creuse<double>* const &Aout,long* const &ci1,  long* const &ci2){
  long *i1=(long *)ci1;
  long *i2=(long *)ci2;
  
  MatriceMorse<double>* pModel = static_cast<MatriceMorse<double>*>(&(*Amodel->A));
  MatriceMorse<double>* pAin = static_cast<MatriceMorse<double>*>(&(*Ain->A));
  long n=pModel->N;
  long m=pModel->M;
  map< pair<int,int>, double> Aoutij;
  double * pV=pAin->a;
  int * lg=pAin->lg;
  int * cl=pAin->cl;
  int iline=*i2;
  for (int col=lg[iline];col<lg[iline+1];col++){
    Aoutij[make_pair(*i1,cl[col])]=pV[col];
  }
  Aout->A=new MatriceMorse<double>(n,m,Aoutij,false);
  return 0;
}
/*
 * Assume 1D discretisation with step pl, and diffusion D.
 * N is the number of points.
 * return:
 * the masse matrix MASSE=int phi_i phi_j
 * the laplace mtrix LAPLA=int grad(phi_i) grad(phi_j)
 */
long my1DvarfCSR(Matrice_Creuse<double>*const & MASSE,
                 Matrice_Creuse<double>*const & LAPLA,
                 long* const & nDofs,
                 KN<double>*const &LENGTH){
  int ii;
  int N=*nDofs;
  //double ALPHA=(*palpha);
  map< pair<int,int>, double> MASSEij;
  map< pair<int,int>, double> LAPLAij;
  ii=0;
  double lim1=0;
  double li=(*LENGTH)[0];
  printf("\nmy1DvarfCSR li=%e,lim1=%e\n",li,lim1);

  
  MASSEij[make_pair(0,0)]=li/3.0;
  MASSEij[make_pair(0,1)]=li/6;
  
  LAPLAij[make_pair(0,0)]=1.0/li;
  LAPLAij[make_pair(0,1)]=-1.0/li;


  for (ii=1;ii<N-1;ii++){
    lim1=(*LENGTH)[ii-1];
    li=(*LENGTH)[ii];
    printf("my1DvarfCSR li=%e,lim1=%e\n",li,lim1);
    MASSEij[make_pair(ii,ii-1)]=lim1/6.0;
    MASSEij[make_pair(ii,ii)]=(lim1+li)/3.0;
    MASSEij[make_pair(ii,ii+1)]=li/6.0;
    
    LAPLAij[make_pair(ii,ii-1)]=-1.0/lim1;
    LAPLAij[make_pair(ii,ii)]=1.0/lim1+1.0/li;
    LAPLAij[make_pair(ii,ii+1)]=-1.0/li;

  }
  lim1=(*LENGTH)[N-2];
  li=0;
  printf("my1DvarfCSR li=%e,lim1=%e\n",li,lim1);
  MASSEij[make_pair(N-1,N-1)]=lim1/3.0;
  MASSEij[make_pair(N-1,N-2)]=lim1/6.0;

  LAPLAij[make_pair(N-1,N-1)]=1.0/lim1;
  LAPLAij[make_pair(N-1,N-2)]=-1.0/lim1;

   
  MASSE->A=new MatriceMorse<double>((*nDofs),(*nDofs),MASSEij,false);
  LAPLA->A=new MatriceMorse<double>((*nDofs),(*nDofs),LAPLAij,false);
  return 0;
}

class Init {
    public:
        Init();
};

Init init;
Init::Init() {
  Global.Add("printCSR", "(", new OneOperator1_<long, Matrice_Creuse<double>* >(printCSR));
  Global.Add("OBtestCSR", "(", new OneOperator4_<long, Matrice_Creuse<double>* ,Matrice_Creuse<double>*,KN<long>*, long*>(OBtestCSR));
  Global.Add("OBbuildMat", "(", new OneOperator4_<long, Matrice_Creuse<double>* ,Matrice_Creuse<double>*,KN<long>*, long*>(OBbuildMat));
  Global.Add("resizeCSR", "(", new OneOperator3_<long, Matrice_Creuse<double>* ,const long , const long>(resizeCSR));
  Global.Add("setDiagCSR", "(", new OneOperator3_<long, Matrice_Creuse<double>* ,const long , const double >(setDiagCSR));
  Global.Add("plugCSR", "(", new OneOperator6_<long, Matrice_Creuse<double>* ,Matrice_Creuse<double>* ,
                                               Matrice_Creuse<double>* ,Matrice_Creuse<double>* ,
                                               long* ,long* >(plugCSR));
  Global.Add("my1DvarfCSR", "(", new OneOperator4_< long,Matrice_Creuse<double>* ,Matrice_Creuse<double>* ,
                                                    long* ,KN<double>*>(my1DvarfCSR));
  Global.Add("getLineMatrice", "(", new OneOperator5_< long,Matrice_Creuse<double>* ,Matrice_Creuse<double>* ,
                                                    Matrice_Creuse<double>* ,long* ,long* >(getLineMatrice));
  Global.Add("getCommunDof", "(", new OneOperator4_< long,KN<long>*,KN<long>* ,long* ,long* >(getCommunDof));
}
