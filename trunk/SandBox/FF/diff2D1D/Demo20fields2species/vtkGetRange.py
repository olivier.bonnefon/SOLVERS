import vtk
import os
from array import array
#baseName="/home/olivierb/VTK_PYTHON/diff/u"
#species=array('c','uv')

#numStep=1998
#for num in range(1,150):
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(500,500)
nbStep=50
step=20
nbOmega=2
WinSize=500
firstStep=1
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(WinSize,WinSize)
rgb=[0,0,0]
execfile("vtktopngGen.py")
os.chdir(OUTPUTDIR+"/D2");
finput = open("input.txt", 'w')
minPerDomain=array('d',range(1,nbOmega*2))
maxPerDomain=array('d',range(1,nbOmega*2))
numSpec=0
for spec in species:
    baseName=OUTPUTDIR+"/D2/"+spec
    for numOmega in range(0,nbOmega):
        minv=1000
        maxv=-1000
        for numStep in range(firstStep,nbStep):
            fileName=baseName+str(numOmega)+"s"+str(numStep*step)+".vtk"
            r=vtk.vtkGenericDataObjectReader()
            r.SetFileName(fileName)
            r.Update()
            o=r.GetOutput()
            o.GetCellData().GetArray("UU")
            o.GetCellData().SetActiveScalars("UU")
            rg= o.GetCellData().GetArray("UU").GetRange(0)
            if (rg[0]<minv):
                minv=rg[0]
            if (rg[1]>maxv):
                maxv=rg[1]
        print "domain %i %c (%e,%e)" % (numOmega,spec , minv,maxv)
    numSpec+=1

   


