RLine=1
GLine=1
BLine=1
widthLine=2.5
points0 = vtk.vtkPoints()
points0.SetNumberOfPoints(119)
lines0 = vtk.vtkCellArray()
lines0.InsertNextCell(120)
points0.SetPoint(0,0,0, 0.1)
lines0.InsertCellPoint(0);
points0.SetPoint(1,0.2,0, 0.1)
lines0.InsertCellPoint(1);
points0.SetPoint(2,0.4,0, 0.1)
lines0.InsertCellPoint(2);
points0.SetPoint(3,0.6,0, 0.1)
lines0.InsertCellPoint(3);
points0.SetPoint(4,0.8,0, 0.1)
lines0.InsertCellPoint(4);
points0.SetPoint(5,1,0, 0.1)
lines0.InsertCellPoint(5);
points0.SetPoint(6,1.2,0, 0.1)
lines0.InsertCellPoint(6);
points0.SetPoint(7,1.4,0, 0.1)
lines0.InsertCellPoint(7);
points0.SetPoint(8,1.6,0, 0.1)
lines0.InsertCellPoint(8);
points0.SetPoint(9,1.8,0, 0.1)
lines0.InsertCellPoint(9);
points0.SetPoint(10,2,0, 0.1)
lines0.InsertCellPoint(10);
points0.SetPoint(11,2.2,0, 0.1)
lines0.InsertCellPoint(11);
points0.SetPoint(12,2.4,0, 0.1)
lines0.InsertCellPoint(12);
points0.SetPoint(13,2.6,0, 0.1)
lines0.InsertCellPoint(13);
points0.SetPoint(14,2.8,0, 0.1)
lines0.InsertCellPoint(14);
points0.SetPoint(15,3,0, 0.1)
lines0.InsertCellPoint(15);
points0.SetPoint(16,3.2,0, 0.1)
lines0.InsertCellPoint(16);
points0.SetPoint(17,3.4,0, 0.1)
lines0.InsertCellPoint(17);
points0.SetPoint(18,3.6,0, 0.1)
lines0.InsertCellPoint(18);
points0.SetPoint(19,3.8,0, 0.1)
lines0.InsertCellPoint(19);
points0.SetPoint(20,4,0, 0.1)
lines0.InsertCellPoint(20);
points0.SetPoint(21,4.2,0, 0.1)
lines0.InsertCellPoint(21);
points0.SetPoint(22,4.4,0, 0.1)
lines0.InsertCellPoint(22);
points0.SetPoint(23,4.6,0, 0.1)
lines0.InsertCellPoint(23);
points0.SetPoint(24,4.8,0, 0.1)
lines0.InsertCellPoint(24);
points0.SetPoint(25,5,0, 0.1)
lines0.InsertCellPoint(25);
points0.SetPoint(26,5,0, 0.1)
lines0.InsertCellPoint(26);
points0.SetPoint(27,5,0.2, 0.1)
lines0.InsertCellPoint(27);
points0.SetPoint(28,5,0.4, 0.1)
lines0.InsertCellPoint(28);
points0.SetPoint(29,5,0.6, 0.1)
lines0.InsertCellPoint(29);
points0.SetPoint(30,5,0.8, 0.1)
lines0.InsertCellPoint(30);
points0.SetPoint(31,5,1, 0.1)
lines0.InsertCellPoint(31);
points0.SetPoint(32,5,1.2, 0.1)
lines0.InsertCellPoint(32);
points0.SetPoint(33,5,1.4, 0.1)
lines0.InsertCellPoint(33);
points0.SetPoint(34,5,1.6, 0.1)
lines0.InsertCellPoint(34);
points0.SetPoint(35,5,1.8, 0.1)
lines0.InsertCellPoint(35);
points0.SetPoint(36,5,2, 0.1)
lines0.InsertCellPoint(36);
points0.SetPoint(37,5,2.2, 0.1)
lines0.InsertCellPoint(37);
points0.SetPoint(38,5,2.4, 0.1)
lines0.InsertCellPoint(38);
points0.SetPoint(39,5,2.6, 0.1)
lines0.InsertCellPoint(39);
points0.SetPoint(40,5,2.8, 0.1)
lines0.InsertCellPoint(40);
points0.SetPoint(41,5,3, 0.1)
lines0.InsertCellPoint(41);
points0.SetPoint(42,5,3.2, 0.1)
lines0.InsertCellPoint(42);
points0.SetPoint(43,5,3.4, 0.1)
lines0.InsertCellPoint(43);
points0.SetPoint(44,5,3.6, 0.1)
lines0.InsertCellPoint(44);
points0.SetPoint(45,5,3.8, 0.1)
lines0.InsertCellPoint(45);
points0.SetPoint(46,5,4, 0.1)
lines0.InsertCellPoint(46);
points0.SetPoint(47,5,4.2, 0.1)
lines0.InsertCellPoint(47);
points0.SetPoint(48,5,4.4, 0.1)
lines0.InsertCellPoint(48);
points0.SetPoint(49,5,4.6, 0.1)
lines0.InsertCellPoint(49);
points0.SetPoint(50,5,4.8, 0.1)
lines0.InsertCellPoint(50);
points0.SetPoint(51,5,5, 0.1)
lines0.InsertCellPoint(51);
points0.SetPoint(52,5,5, 0.1)
lines0.InsertCellPoint(52);
points0.SetPoint(53,4.81481,5.09259, 0.1)
lines0.InsertCellPoint(53);
points0.SetPoint(54,4.62963,5.18519, 0.1)
lines0.InsertCellPoint(54);
points0.SetPoint(55,4.44444,5.27778, 0.1)
lines0.InsertCellPoint(55);
points0.SetPoint(56,4.25926,5.37037, 0.1)
lines0.InsertCellPoint(56);
points0.SetPoint(57,4.07407,5.46296, 0.1)
lines0.InsertCellPoint(57);
points0.SetPoint(58,3.88889,5.55556, 0.1)
lines0.InsertCellPoint(58);
points0.SetPoint(59,3.7037,5.64815, 0.1)
lines0.InsertCellPoint(59);
points0.SetPoint(60,3.51852,5.74074, 0.1)
lines0.InsertCellPoint(60);
points0.SetPoint(61,3.33333,5.83333, 0.1)
lines0.InsertCellPoint(61);
points0.SetPoint(62,3.14815,5.92593, 0.1)
lines0.InsertCellPoint(62);
points0.SetPoint(63,2.96296,6.01852, 0.1)
lines0.InsertCellPoint(63);
points0.SetPoint(64,2.77778,6.11111, 0.1)
lines0.InsertCellPoint(64);
points0.SetPoint(65,2.59259,6.2037, 0.1)
lines0.InsertCellPoint(65);
points0.SetPoint(66,2.40741,6.2963, 0.1)
lines0.InsertCellPoint(66);
points0.SetPoint(67,2.22222,6.38889, 0.1)
lines0.InsertCellPoint(67);
points0.SetPoint(68,2.03704,6.48148, 0.1)
lines0.InsertCellPoint(68);
points0.SetPoint(69,1.85185,6.57407, 0.1)
lines0.InsertCellPoint(69);
points0.SetPoint(70,1.66667,6.66667, 0.1)
lines0.InsertCellPoint(70);
points0.SetPoint(71,1.48148,6.75926, 0.1)
lines0.InsertCellPoint(71);
points0.SetPoint(72,1.2963,6.85185, 0.1)
lines0.InsertCellPoint(72);
points0.SetPoint(73,1.11111,6.94444, 0.1)
lines0.InsertCellPoint(73);
points0.SetPoint(74,0.925926,7.03704, 0.1)
lines0.InsertCellPoint(74);
points0.SetPoint(75,0.740741,7.12963, 0.1)
lines0.InsertCellPoint(75);
points0.SetPoint(76,0.555556,7.22222, 0.1)
lines0.InsertCellPoint(76);
points0.SetPoint(77,0.37037,7.31481, 0.1)
lines0.InsertCellPoint(77);
points0.SetPoint(78,0.185185,7.40741, 0.1)
lines0.InsertCellPoint(78);
points0.SetPoint(79,0,7.5, 0.1)
lines0.InsertCellPoint(79);
points0.SetPoint(80,0,7.5, 0.1)
lines0.InsertCellPoint(80);
points0.SetPoint(81,0,7.29167, 0.1)
lines0.InsertCellPoint(81);
points0.SetPoint(82,0,7.08333, 0.1)
lines0.InsertCellPoint(82);
points0.SetPoint(83,0,6.875, 0.1)
lines0.InsertCellPoint(83);
points0.SetPoint(84,0,6.66667, 0.1)
lines0.InsertCellPoint(84);
points0.SetPoint(85,0,6.45833, 0.1)
lines0.InsertCellPoint(85);
points0.SetPoint(86,0,6.25, 0.1)
lines0.InsertCellPoint(86);
points0.SetPoint(87,0,6.04167, 0.1)
lines0.InsertCellPoint(87);
points0.SetPoint(88,0,5.83333, 0.1)
lines0.InsertCellPoint(88);
points0.SetPoint(89,0,5.625, 0.1)
lines0.InsertCellPoint(89);
points0.SetPoint(90,0,5.41667, 0.1)
lines0.InsertCellPoint(90);
points0.SetPoint(91,0,5.20833, 0.1)
lines0.InsertCellPoint(91);
points0.SetPoint(92,0,5, 0.1)
lines0.InsertCellPoint(92);
points0.SetPoint(93,0,5, 0.1)
lines0.InsertCellPoint(93);
points0.SetPoint(94,0,4.8, 0.1)
lines0.InsertCellPoint(94);
points0.SetPoint(95,0,4.6, 0.1)
lines0.InsertCellPoint(95);
points0.SetPoint(96,0,4.4, 0.1)
lines0.InsertCellPoint(96);
points0.SetPoint(97,0,4.2, 0.1)
lines0.InsertCellPoint(97);
points0.SetPoint(98,0,4, 0.1)
lines0.InsertCellPoint(98);
points0.SetPoint(99,0,3.8, 0.1)
lines0.InsertCellPoint(99);
points0.SetPoint(100,0,3.6, 0.1)
lines0.InsertCellPoint(100);
points0.SetPoint(101,0,3.4, 0.1)
lines0.InsertCellPoint(101);
points0.SetPoint(102,0,3.2, 0.1)
lines0.InsertCellPoint(102);
points0.SetPoint(103,0,3, 0.1)
lines0.InsertCellPoint(103);
points0.SetPoint(104,0,2.8, 0.1)
lines0.InsertCellPoint(104);
points0.SetPoint(105,0,2.6, 0.1)
lines0.InsertCellPoint(105);
points0.SetPoint(106,0,2.4, 0.1)
lines0.InsertCellPoint(106);
points0.SetPoint(107,0,2.2, 0.1)
lines0.InsertCellPoint(107);
points0.SetPoint(108,0,2, 0.1)
lines0.InsertCellPoint(108);
points0.SetPoint(109,0,1.8, 0.1)
lines0.InsertCellPoint(109);
points0.SetPoint(110,0,1.6, 0.1)
lines0.InsertCellPoint(110);
points0.SetPoint(111,0,1.4, 0.1)
lines0.InsertCellPoint(111);
points0.SetPoint(112,0,1.2, 0.1)
lines0.InsertCellPoint(112);
points0.SetPoint(113,0,1, 0.1)
lines0.InsertCellPoint(113);
points0.SetPoint(114,0,0.8, 0.1)
lines0.InsertCellPoint(114);
points0.SetPoint(115,0,0.6, 0.1)
lines0.InsertCellPoint(115);
points0.SetPoint(116,0,0.4, 0.1)
lines0.InsertCellPoint(116);
points0.SetPoint(117,0,0.2, 0.1)
lines0.InsertCellPoint(117);
points0.SetPoint(118,0,0, 0.1)
lines0.InsertCellPoint(118);
lines0.InsertCellPoint(0);
polygon0 = vtk.vtkPolyData()
polygon0.SetPoints(points0)
polygon0.SetLines(lines0)
polygonMapper0 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper0.SetInputConnection(polygon0.GetProducerPort())
else:
	polygonMapper0.SetInputData(polygon0)
	polygonMapper0.Update()
polygonActor0 = vtk.vtkActor()
polygonActor0.SetMapper(polygonMapper0)
polygonActor0.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor0.GetProperty().SetLineWidth(widthLine)
points1 = vtk.vtkPoints()
points1.SetNumberOfPoints(104)
lines1 = vtk.vtkCellArray()
lines1.InsertNextCell(105)
points1.SetPoint(0,0,5, 0.1)
lines1.InsertCellPoint(0);
points1.SetPoint(1,-0.2,5, 0.1)
lines1.InsertCellPoint(1);
points1.SetPoint(2,-0.4,5, 0.1)
lines1.InsertCellPoint(2);
points1.SetPoint(3,-0.6,5, 0.1)
lines1.InsertCellPoint(3);
points1.SetPoint(4,-0.8,5, 0.1)
lines1.InsertCellPoint(4);
points1.SetPoint(5,-1,5, 0.1)
lines1.InsertCellPoint(5);
points1.SetPoint(6,-1.2,5, 0.1)
lines1.InsertCellPoint(6);
points1.SetPoint(7,-1.4,5, 0.1)
lines1.InsertCellPoint(7);
points1.SetPoint(8,-1.6,5, 0.1)
lines1.InsertCellPoint(8);
points1.SetPoint(9,-1.8,5, 0.1)
lines1.InsertCellPoint(9);
points1.SetPoint(10,-2,5, 0.1)
lines1.InsertCellPoint(10);
points1.SetPoint(11,-2.2,5, 0.1)
lines1.InsertCellPoint(11);
points1.SetPoint(12,-2.4,5, 0.1)
lines1.InsertCellPoint(12);
points1.SetPoint(13,-2.6,5, 0.1)
lines1.InsertCellPoint(13);
points1.SetPoint(14,-2.8,5, 0.1)
lines1.InsertCellPoint(14);
points1.SetPoint(15,-3,5, 0.1)
lines1.InsertCellPoint(15);
points1.SetPoint(16,-3.2,5, 0.1)
lines1.InsertCellPoint(16);
points1.SetPoint(17,-3.4,5, 0.1)
lines1.InsertCellPoint(17);
points1.SetPoint(18,-3.6,5, 0.1)
lines1.InsertCellPoint(18);
points1.SetPoint(19,-3.8,5, 0.1)
lines1.InsertCellPoint(19);
points1.SetPoint(20,-4,5, 0.1)
lines1.InsertCellPoint(20);
points1.SetPoint(21,-4.2,5, 0.1)
lines1.InsertCellPoint(21);
points1.SetPoint(22,-4.4,5, 0.1)
lines1.InsertCellPoint(22);
points1.SetPoint(23,-4.6,5, 0.1)
lines1.InsertCellPoint(23);
points1.SetPoint(24,-4.8,5, 0.1)
lines1.InsertCellPoint(24);
points1.SetPoint(25,-5,5, 0.1)
lines1.InsertCellPoint(25);
points1.SetPoint(26,-5,5, 0.1)
lines1.InsertCellPoint(26);
points1.SetPoint(27,-5,4.8, 0.1)
lines1.InsertCellPoint(27);
points1.SetPoint(28,-5,4.6, 0.1)
lines1.InsertCellPoint(28);
points1.SetPoint(29,-5,4.4, 0.1)
lines1.InsertCellPoint(29);
points1.SetPoint(30,-5,4.2, 0.1)
lines1.InsertCellPoint(30);
points1.SetPoint(31,-5,4, 0.1)
lines1.InsertCellPoint(31);
points1.SetPoint(32,-5,3.8, 0.1)
lines1.InsertCellPoint(32);
points1.SetPoint(33,-5,3.6, 0.1)
lines1.InsertCellPoint(33);
points1.SetPoint(34,-5,3.4, 0.1)
lines1.InsertCellPoint(34);
points1.SetPoint(35,-5,3.2, 0.1)
lines1.InsertCellPoint(35);
points1.SetPoint(36,-5,3, 0.1)
lines1.InsertCellPoint(36);
points1.SetPoint(37,-5,2.8, 0.1)
lines1.InsertCellPoint(37);
points1.SetPoint(38,-5,2.6, 0.1)
lines1.InsertCellPoint(38);
points1.SetPoint(39,-5,2.4, 0.1)
lines1.InsertCellPoint(39);
points1.SetPoint(40,-5,2.2, 0.1)
lines1.InsertCellPoint(40);
points1.SetPoint(41,-5,2, 0.1)
lines1.InsertCellPoint(41);
points1.SetPoint(42,-5,1.8, 0.1)
lines1.InsertCellPoint(42);
points1.SetPoint(43,-5,1.6, 0.1)
lines1.InsertCellPoint(43);
points1.SetPoint(44,-5,1.4, 0.1)
lines1.InsertCellPoint(44);
points1.SetPoint(45,-5,1.2, 0.1)
lines1.InsertCellPoint(45);
points1.SetPoint(46,-5,1, 0.1)
lines1.InsertCellPoint(46);
points1.SetPoint(47,-5,0.8, 0.1)
lines1.InsertCellPoint(47);
points1.SetPoint(48,-5,0.6, 0.1)
lines1.InsertCellPoint(48);
points1.SetPoint(49,-5,0.4, 0.1)
lines1.InsertCellPoint(49);
points1.SetPoint(50,-5,0.2, 0.1)
lines1.InsertCellPoint(50);
points1.SetPoint(51,-5,0, 0.1)
lines1.InsertCellPoint(51);
points1.SetPoint(52,-5,0, 0.1)
lines1.InsertCellPoint(52);
points1.SetPoint(53,-4.8,0, 0.1)
lines1.InsertCellPoint(53);
points1.SetPoint(54,-4.6,0, 0.1)
lines1.InsertCellPoint(54);
points1.SetPoint(55,-4.4,0, 0.1)
lines1.InsertCellPoint(55);
points1.SetPoint(56,-4.2,0, 0.1)
lines1.InsertCellPoint(56);
points1.SetPoint(57,-4,0, 0.1)
lines1.InsertCellPoint(57);
points1.SetPoint(58,-3.8,0, 0.1)
lines1.InsertCellPoint(58);
points1.SetPoint(59,-3.6,0, 0.1)
lines1.InsertCellPoint(59);
points1.SetPoint(60,-3.4,0, 0.1)
lines1.InsertCellPoint(60);
points1.SetPoint(61,-3.2,0, 0.1)
lines1.InsertCellPoint(61);
points1.SetPoint(62,-3,0, 0.1)
lines1.InsertCellPoint(62);
points1.SetPoint(63,-2.8,0, 0.1)
lines1.InsertCellPoint(63);
points1.SetPoint(64,-2.6,0, 0.1)
lines1.InsertCellPoint(64);
points1.SetPoint(65,-2.4,0, 0.1)
lines1.InsertCellPoint(65);
points1.SetPoint(66,-2.2,0, 0.1)
lines1.InsertCellPoint(66);
points1.SetPoint(67,-2,0, 0.1)
lines1.InsertCellPoint(67);
points1.SetPoint(68,-1.8,0, 0.1)
lines1.InsertCellPoint(68);
points1.SetPoint(69,-1.6,0, 0.1)
lines1.InsertCellPoint(69);
points1.SetPoint(70,-1.4,0, 0.1)
lines1.InsertCellPoint(70);
points1.SetPoint(71,-1.2,0, 0.1)
lines1.InsertCellPoint(71);
points1.SetPoint(72,-1,0, 0.1)
lines1.InsertCellPoint(72);
points1.SetPoint(73,-0.8,0, 0.1)
lines1.InsertCellPoint(73);
points1.SetPoint(74,-0.6,0, 0.1)
lines1.InsertCellPoint(74);
points1.SetPoint(75,-0.4,0, 0.1)
lines1.InsertCellPoint(75);
points1.SetPoint(76,-0.2,0, 0.1)
lines1.InsertCellPoint(76);
points1.SetPoint(77,0,0, 0.1)
lines1.InsertCellPoint(77);
points1.SetPoint(78,0,0, 0.1)
lines1.InsertCellPoint(78);
points1.SetPoint(79,0,0.2, 0.1)
lines1.InsertCellPoint(79);
points1.SetPoint(80,0,0.4, 0.1)
lines1.InsertCellPoint(80);
points1.SetPoint(81,0,0.6, 0.1)
lines1.InsertCellPoint(81);
points1.SetPoint(82,0,0.8, 0.1)
lines1.InsertCellPoint(82);
points1.SetPoint(83,0,1, 0.1)
lines1.InsertCellPoint(83);
points1.SetPoint(84,0,1.2, 0.1)
lines1.InsertCellPoint(84);
points1.SetPoint(85,0,1.4, 0.1)
lines1.InsertCellPoint(85);
points1.SetPoint(86,0,1.6, 0.1)
lines1.InsertCellPoint(86);
points1.SetPoint(87,0,1.8, 0.1)
lines1.InsertCellPoint(87);
points1.SetPoint(88,0,2, 0.1)
lines1.InsertCellPoint(88);
points1.SetPoint(89,0,2.2, 0.1)
lines1.InsertCellPoint(89);
points1.SetPoint(90,0,2.4, 0.1)
lines1.InsertCellPoint(90);
points1.SetPoint(91,0,2.6, 0.1)
lines1.InsertCellPoint(91);
points1.SetPoint(92,0,2.8, 0.1)
lines1.InsertCellPoint(92);
points1.SetPoint(93,0,3, 0.1)
lines1.InsertCellPoint(93);
points1.SetPoint(94,0,3.2, 0.1)
lines1.InsertCellPoint(94);
points1.SetPoint(95,0,3.4, 0.1)
lines1.InsertCellPoint(95);
points1.SetPoint(96,0,3.6, 0.1)
lines1.InsertCellPoint(96);
points1.SetPoint(97,0,3.8, 0.1)
lines1.InsertCellPoint(97);
points1.SetPoint(98,0,4, 0.1)
lines1.InsertCellPoint(98);
points1.SetPoint(99,0,4.2, 0.1)
lines1.InsertCellPoint(99);
points1.SetPoint(100,0,4.4, 0.1)
lines1.InsertCellPoint(100);
points1.SetPoint(101,0,4.6, 0.1)
lines1.InsertCellPoint(101);
points1.SetPoint(102,0,4.8, 0.1)
lines1.InsertCellPoint(102);
points1.SetPoint(103,0,5, 0.1)
lines1.InsertCellPoint(103);
lines1.InsertCellPoint(0);
polygon1 = vtk.vtkPolyData()
polygon1.SetPoints(points1)
polygon1.SetLines(lines1)
polygonMapper1 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper1.SetInputConnection(polygon1.GetProducerPort())
else:
	polygonMapper1.SetInputData(polygon1)
	polygonMapper1.Update()
polygonActor1 = vtk.vtkActor()
polygonActor1.SetMapper(polygonMapper1)
polygonActor1.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor1.GetProperty().SetLineWidth(widthLine)
points2 = vtk.vtkPoints()
points2.SetNumberOfPoints(168)
lines2 = vtk.vtkCellArray()
lines2.InsertNextCell(169)
points2.SetPoint(0,0,0, 0.1)
lines2.InsertCellPoint(0);
points2.SetPoint(1,-0.2,0, 0.1)
lines2.InsertCellPoint(1);
points2.SetPoint(2,-0.4,0, 0.1)
lines2.InsertCellPoint(2);
points2.SetPoint(3,-0.6,0, 0.1)
lines2.InsertCellPoint(3);
points2.SetPoint(4,-0.8,0, 0.1)
lines2.InsertCellPoint(4);
points2.SetPoint(5,-1,0, 0.1)
lines2.InsertCellPoint(5);
points2.SetPoint(6,-1.2,0, 0.1)
lines2.InsertCellPoint(6);
points2.SetPoint(7,-1.4,0, 0.1)
lines2.InsertCellPoint(7);
points2.SetPoint(8,-1.6,0, 0.1)
lines2.InsertCellPoint(8);
points2.SetPoint(9,-1.8,0, 0.1)
lines2.InsertCellPoint(9);
points2.SetPoint(10,-2,0, 0.1)
lines2.InsertCellPoint(10);
points2.SetPoint(11,-2.2,0, 0.1)
lines2.InsertCellPoint(11);
points2.SetPoint(12,-2.4,0, 0.1)
lines2.InsertCellPoint(12);
points2.SetPoint(13,-2.6,0, 0.1)
lines2.InsertCellPoint(13);
points2.SetPoint(14,-2.8,0, 0.1)
lines2.InsertCellPoint(14);
points2.SetPoint(15,-3,0, 0.1)
lines2.InsertCellPoint(15);
points2.SetPoint(16,-3.2,0, 0.1)
lines2.InsertCellPoint(16);
points2.SetPoint(17,-3.4,0, 0.1)
lines2.InsertCellPoint(17);
points2.SetPoint(18,-3.6,0, 0.1)
lines2.InsertCellPoint(18);
points2.SetPoint(19,-3.8,0, 0.1)
lines2.InsertCellPoint(19);
points2.SetPoint(20,-4,0, 0.1)
lines2.InsertCellPoint(20);
points2.SetPoint(21,-4.2,0, 0.1)
lines2.InsertCellPoint(21);
points2.SetPoint(22,-4.4,0, 0.1)
lines2.InsertCellPoint(22);
points2.SetPoint(23,-4.6,0, 0.1)
lines2.InsertCellPoint(23);
points2.SetPoint(24,-4.8,0, 0.1)
lines2.InsertCellPoint(24);
points2.SetPoint(25,-5,0, 0.1)
lines2.InsertCellPoint(25);
points2.SetPoint(26,-5,0, 0.1)
lines2.InsertCellPoint(26);
points2.SetPoint(27,-5,-0.2, 0.1)
lines2.InsertCellPoint(27);
points2.SetPoint(28,-5,-0.4, 0.1)
lines2.InsertCellPoint(28);
points2.SetPoint(29,-5,-0.6, 0.1)
lines2.InsertCellPoint(29);
points2.SetPoint(30,-5,-0.8, 0.1)
lines2.InsertCellPoint(30);
points2.SetPoint(31,-5,-1, 0.1)
lines2.InsertCellPoint(31);
points2.SetPoint(32,-5,-1.2, 0.1)
lines2.InsertCellPoint(32);
points2.SetPoint(33,-5,-1.4, 0.1)
lines2.InsertCellPoint(33);
points2.SetPoint(34,-5,-1.6, 0.1)
lines2.InsertCellPoint(34);
points2.SetPoint(35,-5,-1.8, 0.1)
lines2.InsertCellPoint(35);
points2.SetPoint(36,-5,-2, 0.1)
lines2.InsertCellPoint(36);
points2.SetPoint(37,-5,-2.2, 0.1)
lines2.InsertCellPoint(37);
points2.SetPoint(38,-5,-2.4, 0.1)
lines2.InsertCellPoint(38);
points2.SetPoint(39,-5,-2.6, 0.1)
lines2.InsertCellPoint(39);
points2.SetPoint(40,-5,-2.8, 0.1)
lines2.InsertCellPoint(40);
points2.SetPoint(41,-5,-3, 0.1)
lines2.InsertCellPoint(41);
points2.SetPoint(42,-5,-3.2, 0.1)
lines2.InsertCellPoint(42);
points2.SetPoint(43,-5,-3.4, 0.1)
lines2.InsertCellPoint(43);
points2.SetPoint(44,-5,-3.6, 0.1)
lines2.InsertCellPoint(44);
points2.SetPoint(45,-5,-3.8, 0.1)
lines2.InsertCellPoint(45);
points2.SetPoint(46,-5,-4, 0.1)
lines2.InsertCellPoint(46);
points2.SetPoint(47,-5,-4.2, 0.1)
lines2.InsertCellPoint(47);
points2.SetPoint(48,-5,-4.4, 0.1)
lines2.InsertCellPoint(48);
points2.SetPoint(49,-5,-4.6, 0.1)
lines2.InsertCellPoint(49);
points2.SetPoint(50,-5,-4.8, 0.1)
lines2.InsertCellPoint(50);
points2.SetPoint(51,-5,-5, 0.1)
lines2.InsertCellPoint(51);
points2.SetPoint(52,-5,-5, 0.1)
lines2.InsertCellPoint(52);
points2.SetPoint(53,-4.80392,-5.04902, 0.1)
lines2.InsertCellPoint(53);
points2.SetPoint(54,-4.60784,-5.09804, 0.1)
lines2.InsertCellPoint(54);
points2.SetPoint(55,-4.41176,-5.14706, 0.1)
lines2.InsertCellPoint(55);
points2.SetPoint(56,-4.21569,-5.19608, 0.1)
lines2.InsertCellPoint(56);
points2.SetPoint(57,-4.01961,-5.2451, 0.1)
lines2.InsertCellPoint(57);
points2.SetPoint(58,-3.82353,-5.29412, 0.1)
lines2.InsertCellPoint(58);
points2.SetPoint(59,-3.62745,-5.34314, 0.1)
lines2.InsertCellPoint(59);
points2.SetPoint(60,-3.43137,-5.39216, 0.1)
lines2.InsertCellPoint(60);
points2.SetPoint(61,-3.23529,-5.44118, 0.1)
lines2.InsertCellPoint(61);
points2.SetPoint(62,-3.03922,-5.4902, 0.1)
lines2.InsertCellPoint(62);
points2.SetPoint(63,-2.84314,-5.53922, 0.1)
lines2.InsertCellPoint(63);
points2.SetPoint(64,-2.64706,-5.58824, 0.1)
lines2.InsertCellPoint(64);
points2.SetPoint(65,-2.45098,-5.63725, 0.1)
lines2.InsertCellPoint(65);
points2.SetPoint(66,-2.2549,-5.68627, 0.1)
lines2.InsertCellPoint(66);
points2.SetPoint(67,-2.05882,-5.73529, 0.1)
lines2.InsertCellPoint(67);
points2.SetPoint(68,-1.86275,-5.78431, 0.1)
lines2.InsertCellPoint(68);
points2.SetPoint(69,-1.66667,-5.83333, 0.1)
lines2.InsertCellPoint(69);
points2.SetPoint(70,-1.47059,-5.88235, 0.1)
lines2.InsertCellPoint(70);
points2.SetPoint(71,-1.27451,-5.93137, 0.1)
lines2.InsertCellPoint(71);
points2.SetPoint(72,-1.07843,-5.98039, 0.1)
lines2.InsertCellPoint(72);
points2.SetPoint(73,-0.882353,-6.02941, 0.1)
lines2.InsertCellPoint(73);
points2.SetPoint(74,-0.686275,-6.07843, 0.1)
lines2.InsertCellPoint(74);
points2.SetPoint(75,-0.490196,-6.12745, 0.1)
lines2.InsertCellPoint(75);
points2.SetPoint(76,-0.294118,-6.17647, 0.1)
lines2.InsertCellPoint(76);
points2.SetPoint(77,-0.0980392,-6.22549, 0.1)
lines2.InsertCellPoint(77);
points2.SetPoint(78,0.0980392,-6.27451, 0.1)
lines2.InsertCellPoint(78);
points2.SetPoint(79,0.294118,-6.32353, 0.1)
lines2.InsertCellPoint(79);
points2.SetPoint(80,0.490196,-6.37255, 0.1)
lines2.InsertCellPoint(80);
points2.SetPoint(81,0.686275,-6.42157, 0.1)
lines2.InsertCellPoint(81);
points2.SetPoint(82,0.882353,-6.47059, 0.1)
lines2.InsertCellPoint(82);
points2.SetPoint(83,1.07843,-6.51961, 0.1)
lines2.InsertCellPoint(83);
points2.SetPoint(84,1.27451,-6.56863, 0.1)
lines2.InsertCellPoint(84);
points2.SetPoint(85,1.47059,-6.61765, 0.1)
lines2.InsertCellPoint(85);
points2.SetPoint(86,1.66667,-6.66667, 0.1)
lines2.InsertCellPoint(86);
points2.SetPoint(87,1.86275,-6.71569, 0.1)
lines2.InsertCellPoint(87);
points2.SetPoint(88,2.05882,-6.76471, 0.1)
lines2.InsertCellPoint(88);
points2.SetPoint(89,2.2549,-6.81373, 0.1)
lines2.InsertCellPoint(89);
points2.SetPoint(90,2.45098,-6.86275, 0.1)
lines2.InsertCellPoint(90);
points2.SetPoint(91,2.64706,-6.91176, 0.1)
lines2.InsertCellPoint(91);
points2.SetPoint(92,2.84314,-6.96078, 0.1)
lines2.InsertCellPoint(92);
points2.SetPoint(93,3.03922,-7.0098, 0.1)
lines2.InsertCellPoint(93);
points2.SetPoint(94,3.23529,-7.05882, 0.1)
lines2.InsertCellPoint(94);
points2.SetPoint(95,3.43137,-7.10784, 0.1)
lines2.InsertCellPoint(95);
points2.SetPoint(96,3.62745,-7.15686, 0.1)
lines2.InsertCellPoint(96);
points2.SetPoint(97,3.82353,-7.20588, 0.1)
lines2.InsertCellPoint(97);
points2.SetPoint(98,4.01961,-7.2549, 0.1)
lines2.InsertCellPoint(98);
points2.SetPoint(99,4.21569,-7.30392, 0.1)
lines2.InsertCellPoint(99);
points2.SetPoint(100,4.41176,-7.35294, 0.1)
lines2.InsertCellPoint(100);
points2.SetPoint(101,4.60784,-7.40196, 0.1)
lines2.InsertCellPoint(101);
points2.SetPoint(102,4.80392,-7.45098, 0.1)
lines2.InsertCellPoint(102);
points2.SetPoint(103,5,-7.5, 0.1)
lines2.InsertCellPoint(103);
points2.SetPoint(104,5,-7.5, 0.1)
lines2.InsertCellPoint(104);
points2.SetPoint(105,5,-7.2973, 0.1)
lines2.InsertCellPoint(105);
points2.SetPoint(106,5,-7.09459, 0.1)
lines2.InsertCellPoint(106);
points2.SetPoint(107,5,-6.89189, 0.1)
lines2.InsertCellPoint(107);
points2.SetPoint(108,5,-6.68919, 0.1)
lines2.InsertCellPoint(108);
points2.SetPoint(109,5,-6.48649, 0.1)
lines2.InsertCellPoint(109);
points2.SetPoint(110,5,-6.28378, 0.1)
lines2.InsertCellPoint(110);
points2.SetPoint(111,5,-6.08108, 0.1)
lines2.InsertCellPoint(111);
points2.SetPoint(112,5,-5.87838, 0.1)
lines2.InsertCellPoint(112);
points2.SetPoint(113,5,-5.67568, 0.1)
lines2.InsertCellPoint(113);
points2.SetPoint(114,5,-5.47297, 0.1)
lines2.InsertCellPoint(114);
points2.SetPoint(115,5,-5.27027, 0.1)
lines2.InsertCellPoint(115);
points2.SetPoint(116,5,-5.06757, 0.1)
lines2.InsertCellPoint(116);
points2.SetPoint(117,5,-4.86486, 0.1)
lines2.InsertCellPoint(117);
points2.SetPoint(118,5,-4.66216, 0.1)
lines2.InsertCellPoint(118);
points2.SetPoint(119,5,-4.45946, 0.1)
lines2.InsertCellPoint(119);
points2.SetPoint(120,5,-4.25676, 0.1)
lines2.InsertCellPoint(120);
points2.SetPoint(121,5,-4.05405, 0.1)
lines2.InsertCellPoint(121);
points2.SetPoint(122,5,-3.85135, 0.1)
lines2.InsertCellPoint(122);
points2.SetPoint(123,5,-3.64865, 0.1)
lines2.InsertCellPoint(123);
points2.SetPoint(124,5,-3.44595, 0.1)
lines2.InsertCellPoint(124);
points2.SetPoint(125,5,-3.24324, 0.1)
lines2.InsertCellPoint(125);
points2.SetPoint(126,5,-3.04054, 0.1)
lines2.InsertCellPoint(126);
points2.SetPoint(127,5,-2.83784, 0.1)
lines2.InsertCellPoint(127);
points2.SetPoint(128,5,-2.63514, 0.1)
lines2.InsertCellPoint(128);
points2.SetPoint(129,5,-2.43243, 0.1)
lines2.InsertCellPoint(129);
points2.SetPoint(130,5,-2.22973, 0.1)
lines2.InsertCellPoint(130);
points2.SetPoint(131,5,-2.02703, 0.1)
lines2.InsertCellPoint(131);
points2.SetPoint(132,5,-1.82432, 0.1)
lines2.InsertCellPoint(132);
points2.SetPoint(133,5,-1.62162, 0.1)
lines2.InsertCellPoint(133);
points2.SetPoint(134,5,-1.41892, 0.1)
lines2.InsertCellPoint(134);
points2.SetPoint(135,5,-1.21622, 0.1)
lines2.InsertCellPoint(135);
points2.SetPoint(136,5,-1.01351, 0.1)
lines2.InsertCellPoint(136);
points2.SetPoint(137,5,-0.810811, 0.1)
lines2.InsertCellPoint(137);
points2.SetPoint(138,5,-0.608108, 0.1)
lines2.InsertCellPoint(138);
points2.SetPoint(139,5,-0.405405, 0.1)
lines2.InsertCellPoint(139);
points2.SetPoint(140,5,-0.202703, 0.1)
lines2.InsertCellPoint(140);
points2.SetPoint(141,5,0, 0.1)
lines2.InsertCellPoint(141);
points2.SetPoint(142,5,0, 0.1)
lines2.InsertCellPoint(142);
points2.SetPoint(143,4.8,0, 0.1)
lines2.InsertCellPoint(143);
points2.SetPoint(144,4.6,0, 0.1)
lines2.InsertCellPoint(144);
points2.SetPoint(145,4.4,0, 0.1)
lines2.InsertCellPoint(145);
points2.SetPoint(146,4.2,0, 0.1)
lines2.InsertCellPoint(146);
points2.SetPoint(147,4,0, 0.1)
lines2.InsertCellPoint(147);
points2.SetPoint(148,3.8,0, 0.1)
lines2.InsertCellPoint(148);
points2.SetPoint(149,3.6,0, 0.1)
lines2.InsertCellPoint(149);
points2.SetPoint(150,3.4,0, 0.1)
lines2.InsertCellPoint(150);
points2.SetPoint(151,3.2,0, 0.1)
lines2.InsertCellPoint(151);
points2.SetPoint(152,3,0, 0.1)
lines2.InsertCellPoint(152);
points2.SetPoint(153,2.8,0, 0.1)
lines2.InsertCellPoint(153);
points2.SetPoint(154,2.6,0, 0.1)
lines2.InsertCellPoint(154);
points2.SetPoint(155,2.4,0, 0.1)
lines2.InsertCellPoint(155);
points2.SetPoint(156,2.2,0, 0.1)
lines2.InsertCellPoint(156);
points2.SetPoint(157,2,0, 0.1)
lines2.InsertCellPoint(157);
points2.SetPoint(158,1.8,0, 0.1)
lines2.InsertCellPoint(158);
points2.SetPoint(159,1.6,0, 0.1)
lines2.InsertCellPoint(159);
points2.SetPoint(160,1.4,0, 0.1)
lines2.InsertCellPoint(160);
points2.SetPoint(161,1.2,0, 0.1)
lines2.InsertCellPoint(161);
points2.SetPoint(162,1,0, 0.1)
lines2.InsertCellPoint(162);
points2.SetPoint(163,0.8,0, 0.1)
lines2.InsertCellPoint(163);
points2.SetPoint(164,0.6,0, 0.1)
lines2.InsertCellPoint(164);
points2.SetPoint(165,0.4,0, 0.1)
lines2.InsertCellPoint(165);
points2.SetPoint(166,0.2,0, 0.1)
lines2.InsertCellPoint(166);
points2.SetPoint(167,0,0, 0.1)
lines2.InsertCellPoint(167);
lines2.InsertCellPoint(0);
polygon2 = vtk.vtkPolyData()
polygon2.SetPoints(points2)
polygon2.SetLines(lines2)
polygonMapper2 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper2.SetInputConnection(polygon2.GetProducerPort())
else:
	polygonMapper2.SetInputData(polygon2)
	polygonMapper2.Update()
polygonActor2 = vtk.vtkActor()
polygonActor2.SetMapper(polygonMapper2)
polygonActor2.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor2.GetProperty().SetLineWidth(widthLine)
points3 = vtk.vtkPoints()
points3.SetNumberOfPoints(109)
lines3 = vtk.vtkCellArray()
lines3.InsertNextCell(110)
points3.SetPoint(0,5,0, 0.1)
lines3.InsertCellPoint(0);
points3.SetPoint(1,5.2,0, 0.1)
lines3.InsertCellPoint(1);
points3.SetPoint(2,5.4,0, 0.1)
lines3.InsertCellPoint(2);
points3.SetPoint(3,5.6,0, 0.1)
lines3.InsertCellPoint(3);
points3.SetPoint(4,5.8,0, 0.1)
lines3.InsertCellPoint(4);
points3.SetPoint(5,6,0, 0.1)
lines3.InsertCellPoint(5);
points3.SetPoint(6,6.2,0, 0.1)
lines3.InsertCellPoint(6);
points3.SetPoint(7,6.4,0, 0.1)
lines3.InsertCellPoint(7);
points3.SetPoint(8,6.6,0, 0.1)
lines3.InsertCellPoint(8);
points3.SetPoint(9,6.8,0, 0.1)
lines3.InsertCellPoint(9);
points3.SetPoint(10,7,0, 0.1)
lines3.InsertCellPoint(10);
points3.SetPoint(11,7.2,0, 0.1)
lines3.InsertCellPoint(11);
points3.SetPoint(12,7.4,0, 0.1)
lines3.InsertCellPoint(12);
points3.SetPoint(13,7.6,0, 0.1)
lines3.InsertCellPoint(13);
points3.SetPoint(14,7.8,0, 0.1)
lines3.InsertCellPoint(14);
points3.SetPoint(15,8,0, 0.1)
lines3.InsertCellPoint(15);
points3.SetPoint(16,8.2,0, 0.1)
lines3.InsertCellPoint(16);
points3.SetPoint(17,8.4,0, 0.1)
lines3.InsertCellPoint(17);
points3.SetPoint(18,8.6,0, 0.1)
lines3.InsertCellPoint(18);
points3.SetPoint(19,8.8,0, 0.1)
lines3.InsertCellPoint(19);
points3.SetPoint(20,9,0, 0.1)
lines3.InsertCellPoint(20);
points3.SetPoint(21,9,0, 0.1)
lines3.InsertCellPoint(21);
points3.SetPoint(22,9.12,0.16, 0.1)
lines3.InsertCellPoint(22);
points3.SetPoint(23,9.24,0.32, 0.1)
lines3.InsertCellPoint(23);
points3.SetPoint(24,9.36,0.48, 0.1)
lines3.InsertCellPoint(24);
points3.SetPoint(25,9.48,0.64, 0.1)
lines3.InsertCellPoint(25);
points3.SetPoint(26,9.6,0.8, 0.1)
lines3.InsertCellPoint(26);
points3.SetPoint(27,9.72,0.96, 0.1)
lines3.InsertCellPoint(27);
points3.SetPoint(28,9.84,1.12, 0.1)
lines3.InsertCellPoint(28);
points3.SetPoint(29,9.96,1.28, 0.1)
lines3.InsertCellPoint(29);
points3.SetPoint(30,10.08,1.44, 0.1)
lines3.InsertCellPoint(30);
points3.SetPoint(31,10.2,1.6, 0.1)
lines3.InsertCellPoint(31);
points3.SetPoint(32,10.32,1.76, 0.1)
lines3.InsertCellPoint(32);
points3.SetPoint(33,10.44,1.92, 0.1)
lines3.InsertCellPoint(33);
points3.SetPoint(34,10.56,2.08, 0.1)
lines3.InsertCellPoint(34);
points3.SetPoint(35,10.68,2.24, 0.1)
lines3.InsertCellPoint(35);
points3.SetPoint(36,10.8,2.4, 0.1)
lines3.InsertCellPoint(36);
points3.SetPoint(37,10.92,2.56, 0.1)
lines3.InsertCellPoint(37);
points3.SetPoint(38,11.04,2.72, 0.1)
lines3.InsertCellPoint(38);
points3.SetPoint(39,11.16,2.88, 0.1)
lines3.InsertCellPoint(39);
points3.SetPoint(40,11.28,3.04, 0.1)
lines3.InsertCellPoint(40);
points3.SetPoint(41,11.4,3.2, 0.1)
lines3.InsertCellPoint(41);
points3.SetPoint(42,11.52,3.36, 0.1)
lines3.InsertCellPoint(42);
points3.SetPoint(43,11.64,3.52, 0.1)
lines3.InsertCellPoint(43);
points3.SetPoint(44,11.76,3.68, 0.1)
lines3.InsertCellPoint(44);
points3.SetPoint(45,11.88,3.84, 0.1)
lines3.InsertCellPoint(45);
points3.SetPoint(46,12,4, 0.1)
lines3.InsertCellPoint(46);
points3.SetPoint(47,12,4, 0.1)
lines3.InsertCellPoint(47);
points3.SetPoint(48,11.8,4.02857, 0.1)
lines3.InsertCellPoint(48);
points3.SetPoint(49,11.6,4.05714, 0.1)
lines3.InsertCellPoint(49);
points3.SetPoint(50,11.4,4.08571, 0.1)
lines3.InsertCellPoint(50);
points3.SetPoint(51,11.2,4.11429, 0.1)
lines3.InsertCellPoint(51);
points3.SetPoint(52,11,4.14286, 0.1)
lines3.InsertCellPoint(52);
points3.SetPoint(53,10.8,4.17143, 0.1)
lines3.InsertCellPoint(53);
points3.SetPoint(54,10.6,4.2, 0.1)
lines3.InsertCellPoint(54);
points3.SetPoint(55,10.4,4.22857, 0.1)
lines3.InsertCellPoint(55);
points3.SetPoint(56,10.2,4.25714, 0.1)
lines3.InsertCellPoint(56);
points3.SetPoint(57,10,4.28571, 0.1)
lines3.InsertCellPoint(57);
points3.SetPoint(58,9.8,4.31429, 0.1)
lines3.InsertCellPoint(58);
points3.SetPoint(59,9.6,4.34286, 0.1)
lines3.InsertCellPoint(59);
points3.SetPoint(60,9.4,4.37143, 0.1)
lines3.InsertCellPoint(60);
points3.SetPoint(61,9.2,4.4, 0.1)
lines3.InsertCellPoint(61);
points3.SetPoint(62,9,4.42857, 0.1)
lines3.InsertCellPoint(62);
points3.SetPoint(63,8.8,4.45714, 0.1)
lines3.InsertCellPoint(63);
points3.SetPoint(64,8.6,4.48571, 0.1)
lines3.InsertCellPoint(64);
points3.SetPoint(65,8.4,4.51429, 0.1)
lines3.InsertCellPoint(65);
points3.SetPoint(66,8.2,4.54286, 0.1)
lines3.InsertCellPoint(66);
points3.SetPoint(67,8,4.57143, 0.1)
lines3.InsertCellPoint(67);
points3.SetPoint(68,7.8,4.6, 0.1)
lines3.InsertCellPoint(68);
points3.SetPoint(69,7.6,4.62857, 0.1)
lines3.InsertCellPoint(69);
points3.SetPoint(70,7.4,4.65714, 0.1)
lines3.InsertCellPoint(70);
points3.SetPoint(71,7.2,4.68571, 0.1)
lines3.InsertCellPoint(71);
points3.SetPoint(72,7,4.71429, 0.1)
lines3.InsertCellPoint(72);
points3.SetPoint(73,6.8,4.74286, 0.1)
lines3.InsertCellPoint(73);
points3.SetPoint(74,6.6,4.77143, 0.1)
lines3.InsertCellPoint(74);
points3.SetPoint(75,6.4,4.8, 0.1)
lines3.InsertCellPoint(75);
points3.SetPoint(76,6.2,4.82857, 0.1)
lines3.InsertCellPoint(76);
points3.SetPoint(77,6,4.85714, 0.1)
lines3.InsertCellPoint(77);
points3.SetPoint(78,5.8,4.88571, 0.1)
lines3.InsertCellPoint(78);
points3.SetPoint(79,5.6,4.91429, 0.1)
lines3.InsertCellPoint(79);
points3.SetPoint(80,5.4,4.94286, 0.1)
lines3.InsertCellPoint(80);
points3.SetPoint(81,5.2,4.97143, 0.1)
lines3.InsertCellPoint(81);
points3.SetPoint(82,5,5, 0.1)
lines3.InsertCellPoint(82);
points3.SetPoint(83,5,5, 0.1)
lines3.InsertCellPoint(83);
points3.SetPoint(84,5,4.8, 0.1)
lines3.InsertCellPoint(84);
points3.SetPoint(85,5,4.6, 0.1)
lines3.InsertCellPoint(85);
points3.SetPoint(86,5,4.4, 0.1)
lines3.InsertCellPoint(86);
points3.SetPoint(87,5,4.2, 0.1)
lines3.InsertCellPoint(87);
points3.SetPoint(88,5,4, 0.1)
lines3.InsertCellPoint(88);
points3.SetPoint(89,5,3.8, 0.1)
lines3.InsertCellPoint(89);
points3.SetPoint(90,5,3.6, 0.1)
lines3.InsertCellPoint(90);
points3.SetPoint(91,5,3.4, 0.1)
lines3.InsertCellPoint(91);
points3.SetPoint(92,5,3.2, 0.1)
lines3.InsertCellPoint(92);
points3.SetPoint(93,5,3, 0.1)
lines3.InsertCellPoint(93);
points3.SetPoint(94,5,2.8, 0.1)
lines3.InsertCellPoint(94);
points3.SetPoint(95,5,2.6, 0.1)
lines3.InsertCellPoint(95);
points3.SetPoint(96,5,2.4, 0.1)
lines3.InsertCellPoint(96);
points3.SetPoint(97,5,2.2, 0.1)
lines3.InsertCellPoint(97);
points3.SetPoint(98,5,2, 0.1)
lines3.InsertCellPoint(98);
points3.SetPoint(99,5,1.8, 0.1)
lines3.InsertCellPoint(99);
points3.SetPoint(100,5,1.6, 0.1)
lines3.InsertCellPoint(100);
points3.SetPoint(101,5,1.4, 0.1)
lines3.InsertCellPoint(101);
points3.SetPoint(102,5,1.2, 0.1)
lines3.InsertCellPoint(102);
points3.SetPoint(103,5,1, 0.1)
lines3.InsertCellPoint(103);
points3.SetPoint(104,5,0.8, 0.1)
lines3.InsertCellPoint(104);
points3.SetPoint(105,5,0.6, 0.1)
lines3.InsertCellPoint(105);
points3.SetPoint(106,5,0.4, 0.1)
lines3.InsertCellPoint(106);
points3.SetPoint(107,5,0.2, 0.1)
lines3.InsertCellPoint(107);
points3.SetPoint(108,5,0, 0.1)
lines3.InsertCellPoint(108);
lines3.InsertCellPoint(0);
polygon3 = vtk.vtkPolyData()
polygon3.SetPoints(points3)
polygon3.SetLines(lines3)
polygonMapper3 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper3.SetInputConnection(polygon3.GetProducerPort())
else:
	polygonMapper3.SetInputData(polygon3)
	polygonMapper3.Update()
polygonActor3 = vtk.vtkActor()
polygonActor3.SetMapper(polygonMapper3)
polygonActor3.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor3.GetProperty().SetLineWidth(widthLine)
points4 = vtk.vtkPoints()
points4.SetNumberOfPoints(104)
lines4 = vtk.vtkCellArray()
lines4.InsertNextCell(105)
points4.SetPoint(0,5,-7.5, 0.1)
lines4.InsertCellPoint(0);
points4.SetPoint(1,5.12,-7.34, 0.1)
lines4.InsertCellPoint(1);
points4.SetPoint(2,5.24,-7.18, 0.1)
lines4.InsertCellPoint(2);
points4.SetPoint(3,5.36,-7.02, 0.1)
lines4.InsertCellPoint(3);
points4.SetPoint(4,5.48,-6.86, 0.1)
lines4.InsertCellPoint(4);
points4.SetPoint(5,5.6,-6.7, 0.1)
lines4.InsertCellPoint(5);
points4.SetPoint(6,5.72,-6.54, 0.1)
lines4.InsertCellPoint(6);
points4.SetPoint(7,5.84,-6.38, 0.1)
lines4.InsertCellPoint(7);
points4.SetPoint(8,5.96,-6.22, 0.1)
lines4.InsertCellPoint(8);
points4.SetPoint(9,6.08,-6.06, 0.1)
lines4.InsertCellPoint(9);
points4.SetPoint(10,6.2,-5.9, 0.1)
lines4.InsertCellPoint(10);
points4.SetPoint(11,6.32,-5.74, 0.1)
lines4.InsertCellPoint(11);
points4.SetPoint(12,6.44,-5.58, 0.1)
lines4.InsertCellPoint(12);
points4.SetPoint(13,6.56,-5.42, 0.1)
lines4.InsertCellPoint(13);
points4.SetPoint(14,6.68,-5.26, 0.1)
lines4.InsertCellPoint(14);
points4.SetPoint(15,6.8,-5.1, 0.1)
lines4.InsertCellPoint(15);
points4.SetPoint(16,6.92,-4.94, 0.1)
lines4.InsertCellPoint(16);
points4.SetPoint(17,7.04,-4.78, 0.1)
lines4.InsertCellPoint(17);
points4.SetPoint(18,7.16,-4.62, 0.1)
lines4.InsertCellPoint(18);
points4.SetPoint(19,7.28,-4.46, 0.1)
lines4.InsertCellPoint(19);
points4.SetPoint(20,7.4,-4.3, 0.1)
lines4.InsertCellPoint(20);
points4.SetPoint(21,7.52,-4.14, 0.1)
lines4.InsertCellPoint(21);
points4.SetPoint(22,7.64,-3.98, 0.1)
lines4.InsertCellPoint(22);
points4.SetPoint(23,7.76,-3.82, 0.1)
lines4.InsertCellPoint(23);
points4.SetPoint(24,7.88,-3.66, 0.1)
lines4.InsertCellPoint(24);
points4.SetPoint(25,8,-3.5, 0.1)
lines4.InsertCellPoint(25);
points4.SetPoint(26,8,-3.5, 0.1)
lines4.InsertCellPoint(26);
points4.SetPoint(27,8.05556,-3.30556, 0.1)
lines4.InsertCellPoint(27);
points4.SetPoint(28,8.11111,-3.11111, 0.1)
lines4.InsertCellPoint(28);
points4.SetPoint(29,8.16667,-2.91667, 0.1)
lines4.InsertCellPoint(29);
points4.SetPoint(30,8.22222,-2.72222, 0.1)
lines4.InsertCellPoint(30);
points4.SetPoint(31,8.27778,-2.52778, 0.1)
lines4.InsertCellPoint(31);
points4.SetPoint(32,8.33333,-2.33333, 0.1)
lines4.InsertCellPoint(32);
points4.SetPoint(33,8.38889,-2.13889, 0.1)
lines4.InsertCellPoint(33);
points4.SetPoint(34,8.44444,-1.94444, 0.1)
lines4.InsertCellPoint(34);
points4.SetPoint(35,8.5,-1.75, 0.1)
lines4.InsertCellPoint(35);
points4.SetPoint(36,8.55556,-1.55556, 0.1)
lines4.InsertCellPoint(36);
points4.SetPoint(37,8.61111,-1.36111, 0.1)
lines4.InsertCellPoint(37);
points4.SetPoint(38,8.66667,-1.16667, 0.1)
lines4.InsertCellPoint(38);
points4.SetPoint(39,8.72222,-0.972222, 0.1)
lines4.InsertCellPoint(39);
points4.SetPoint(40,8.77778,-0.777778, 0.1)
lines4.InsertCellPoint(40);
points4.SetPoint(41,8.83333,-0.583333, 0.1)
lines4.InsertCellPoint(41);
points4.SetPoint(42,8.88889,-0.388889, 0.1)
lines4.InsertCellPoint(42);
points4.SetPoint(43,8.94444,-0.194444, 0.1)
lines4.InsertCellPoint(43);
points4.SetPoint(44,9,0, 0.1)
lines4.InsertCellPoint(44);
points4.SetPoint(45,9,0, 0.1)
lines4.InsertCellPoint(45);
points4.SetPoint(46,8.8,0, 0.1)
lines4.InsertCellPoint(46);
points4.SetPoint(47,8.6,0, 0.1)
lines4.InsertCellPoint(47);
points4.SetPoint(48,8.4,0, 0.1)
lines4.InsertCellPoint(48);
points4.SetPoint(49,8.2,0, 0.1)
lines4.InsertCellPoint(49);
points4.SetPoint(50,8,0, 0.1)
lines4.InsertCellPoint(50);
points4.SetPoint(51,7.8,0, 0.1)
lines4.InsertCellPoint(51);
points4.SetPoint(52,7.6,0, 0.1)
lines4.InsertCellPoint(52);
points4.SetPoint(53,7.4,0, 0.1)
lines4.InsertCellPoint(53);
points4.SetPoint(54,7.2,0, 0.1)
lines4.InsertCellPoint(54);
points4.SetPoint(55,7,0, 0.1)
lines4.InsertCellPoint(55);
points4.SetPoint(56,6.8,0, 0.1)
lines4.InsertCellPoint(56);
points4.SetPoint(57,6.6,0, 0.1)
lines4.InsertCellPoint(57);
points4.SetPoint(58,6.4,0, 0.1)
lines4.InsertCellPoint(58);
points4.SetPoint(59,6.2,0, 0.1)
lines4.InsertCellPoint(59);
points4.SetPoint(60,6,0, 0.1)
lines4.InsertCellPoint(60);
points4.SetPoint(61,5.8,0, 0.1)
lines4.InsertCellPoint(61);
points4.SetPoint(62,5.6,0, 0.1)
lines4.InsertCellPoint(62);
points4.SetPoint(63,5.4,0, 0.1)
lines4.InsertCellPoint(63);
points4.SetPoint(64,5.2,0, 0.1)
lines4.InsertCellPoint(64);
points4.SetPoint(65,5,0, 0.1)
lines4.InsertCellPoint(65);
points4.SetPoint(66,5,0, 0.1)
lines4.InsertCellPoint(66);
points4.SetPoint(67,5,-0.202703, 0.1)
lines4.InsertCellPoint(67);
points4.SetPoint(68,5,-0.405405, 0.1)
lines4.InsertCellPoint(68);
points4.SetPoint(69,5,-0.608108, 0.1)
lines4.InsertCellPoint(69);
points4.SetPoint(70,5,-0.810811, 0.1)
lines4.InsertCellPoint(70);
points4.SetPoint(71,5,-1.01351, 0.1)
lines4.InsertCellPoint(71);
points4.SetPoint(72,5,-1.21622, 0.1)
lines4.InsertCellPoint(72);
points4.SetPoint(73,5,-1.41892, 0.1)
lines4.InsertCellPoint(73);
points4.SetPoint(74,5,-1.62162, 0.1)
lines4.InsertCellPoint(74);
points4.SetPoint(75,5,-1.82432, 0.1)
lines4.InsertCellPoint(75);
points4.SetPoint(76,5,-2.02703, 0.1)
lines4.InsertCellPoint(76);
points4.SetPoint(77,5,-2.22973, 0.1)
lines4.InsertCellPoint(77);
points4.SetPoint(78,5,-2.43243, 0.1)
lines4.InsertCellPoint(78);
points4.SetPoint(79,5,-2.63514, 0.1)
lines4.InsertCellPoint(79);
points4.SetPoint(80,5,-2.83784, 0.1)
lines4.InsertCellPoint(80);
points4.SetPoint(81,5,-3.04054, 0.1)
lines4.InsertCellPoint(81);
points4.SetPoint(82,5,-3.24324, 0.1)
lines4.InsertCellPoint(82);
points4.SetPoint(83,5,-3.44595, 0.1)
lines4.InsertCellPoint(83);
points4.SetPoint(84,5,-3.64865, 0.1)
lines4.InsertCellPoint(84);
points4.SetPoint(85,5,-3.85135, 0.1)
lines4.InsertCellPoint(85);
points4.SetPoint(86,5,-4.05405, 0.1)
lines4.InsertCellPoint(86);
points4.SetPoint(87,5,-4.25676, 0.1)
lines4.InsertCellPoint(87);
points4.SetPoint(88,5,-4.45946, 0.1)
lines4.InsertCellPoint(88);
points4.SetPoint(89,5,-4.66216, 0.1)
lines4.InsertCellPoint(89);
points4.SetPoint(90,5,-4.86486, 0.1)
lines4.InsertCellPoint(90);
points4.SetPoint(91,5,-5.06757, 0.1)
lines4.InsertCellPoint(91);
points4.SetPoint(92,5,-5.27027, 0.1)
lines4.InsertCellPoint(92);
points4.SetPoint(93,5,-5.47297, 0.1)
lines4.InsertCellPoint(93);
points4.SetPoint(94,5,-5.67568, 0.1)
lines4.InsertCellPoint(94);
points4.SetPoint(95,5,-5.87838, 0.1)
lines4.InsertCellPoint(95);
points4.SetPoint(96,5,-6.08108, 0.1)
lines4.InsertCellPoint(96);
points4.SetPoint(97,5,-6.28378, 0.1)
lines4.InsertCellPoint(97);
points4.SetPoint(98,5,-6.48649, 0.1)
lines4.InsertCellPoint(98);
points4.SetPoint(99,5,-6.68919, 0.1)
lines4.InsertCellPoint(99);
points4.SetPoint(100,5,-6.89189, 0.1)
lines4.InsertCellPoint(100);
points4.SetPoint(101,5,-7.09459, 0.1)
lines4.InsertCellPoint(101);
points4.SetPoint(102,5,-7.2973, 0.1)
lines4.InsertCellPoint(102);
points4.SetPoint(103,5,-7.5, 0.1)
lines4.InsertCellPoint(103);
lines4.InsertCellPoint(0);
polygon4 = vtk.vtkPolyData()
polygon4.SetPoints(points4)
polygon4.SetLines(lines4)
polygonMapper4 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper4.SetInputConnection(polygon4.GetProducerPort())
else:
	polygonMapper4.SetInputData(polygon4)
	polygonMapper4.Update()
polygonActor4 = vtk.vtkActor()
polygonActor4.SetMapper(polygonMapper4)
polygonActor4.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor4.GetProperty().SetLineWidth(widthLine)
points5 = vtk.vtkPoints()
points5.SetNumberOfPoints(161)
lines5 = vtk.vtkCellArray()
lines5.InsertNextCell(162)
points5.SetPoint(0,5,5, 0.1)
lines5.InsertCellPoint(0);
points5.SetPoint(1,5,5.2, 0.1)
lines5.InsertCellPoint(1);
points5.SetPoint(2,5,5.4, 0.1)
lines5.InsertCellPoint(2);
points5.SetPoint(3,5,5.6, 0.1)
lines5.InsertCellPoint(3);
points5.SetPoint(4,5,5.8, 0.1)
lines5.InsertCellPoint(4);
points5.SetPoint(5,5,6, 0.1)
lines5.InsertCellPoint(5);
points5.SetPoint(6,5,6.2, 0.1)
lines5.InsertCellPoint(6);
points5.SetPoint(7,5,6.4, 0.1)
lines5.InsertCellPoint(7);
points5.SetPoint(8,5,6.6, 0.1)
lines5.InsertCellPoint(8);
points5.SetPoint(9,5,6.8, 0.1)
lines5.InsertCellPoint(9);
points5.SetPoint(10,5,7, 0.1)
lines5.InsertCellPoint(10);
points5.SetPoint(11,5,7.2, 0.1)
lines5.InsertCellPoint(11);
points5.SetPoint(12,5,7.4, 0.1)
lines5.InsertCellPoint(12);
points5.SetPoint(13,5,7.6, 0.1)
lines5.InsertCellPoint(13);
points5.SetPoint(14,5,7.8, 0.1)
lines5.InsertCellPoint(14);
points5.SetPoint(15,5,8, 0.1)
lines5.InsertCellPoint(15);
points5.SetPoint(16,5,8.2, 0.1)
lines5.InsertCellPoint(16);
points5.SetPoint(17,5,8.4, 0.1)
lines5.InsertCellPoint(17);
points5.SetPoint(18,5,8.6, 0.1)
lines5.InsertCellPoint(18);
points5.SetPoint(19,5,8.8, 0.1)
lines5.InsertCellPoint(19);
points5.SetPoint(20,5,9, 0.1)
lines5.InsertCellPoint(20);
points5.SetPoint(21,5,9, 0.1)
lines5.InsertCellPoint(21);
points5.SetPoint(22,4.8,9.05, 0.1)
lines5.InsertCellPoint(22);
points5.SetPoint(23,4.6,9.1, 0.1)
lines5.InsertCellPoint(23);
points5.SetPoint(24,4.4,9.15, 0.1)
lines5.InsertCellPoint(24);
points5.SetPoint(25,4.2,9.2, 0.1)
lines5.InsertCellPoint(25);
points5.SetPoint(26,4,9.25, 0.1)
lines5.InsertCellPoint(26);
points5.SetPoint(27,3.8,9.3, 0.1)
lines5.InsertCellPoint(27);
points5.SetPoint(28,3.6,9.35, 0.1)
lines5.InsertCellPoint(28);
points5.SetPoint(29,3.4,9.4, 0.1)
lines5.InsertCellPoint(29);
points5.SetPoint(30,3.2,9.45, 0.1)
lines5.InsertCellPoint(30);
points5.SetPoint(31,3,9.5, 0.1)
lines5.InsertCellPoint(31);
points5.SetPoint(32,2.8,9.55, 0.1)
lines5.InsertCellPoint(32);
points5.SetPoint(33,2.6,9.6, 0.1)
lines5.InsertCellPoint(33);
points5.SetPoint(34,2.4,9.65, 0.1)
lines5.InsertCellPoint(34);
points5.SetPoint(35,2.2,9.7, 0.1)
lines5.InsertCellPoint(35);
points5.SetPoint(36,2,9.75, 0.1)
lines5.InsertCellPoint(36);
points5.SetPoint(37,1.8,9.8, 0.1)
lines5.InsertCellPoint(37);
points5.SetPoint(38,1.6,9.85, 0.1)
lines5.InsertCellPoint(38);
points5.SetPoint(39,1.4,9.9, 0.1)
lines5.InsertCellPoint(39);
points5.SetPoint(40,1.2,9.95, 0.1)
lines5.InsertCellPoint(40);
points5.SetPoint(41,1,10, 0.1)
lines5.InsertCellPoint(41);
points5.SetPoint(42,1,10, 0.1)
lines5.InsertCellPoint(42);
points5.SetPoint(43,0.8,9.96667, 0.1)
lines5.InsertCellPoint(43);
points5.SetPoint(44,0.6,9.93333, 0.1)
lines5.InsertCellPoint(44);
points5.SetPoint(45,0.4,9.9, 0.1)
lines5.InsertCellPoint(45);
points5.SetPoint(46,0.2,9.86667, 0.1)
lines5.InsertCellPoint(46);
points5.SetPoint(47,1.11022e-16,9.83333, 0.1)
lines5.InsertCellPoint(47);
points5.SetPoint(48,-0.2,9.8, 0.1)
lines5.InsertCellPoint(48);
points5.SetPoint(49,-0.4,9.76667, 0.1)
lines5.InsertCellPoint(49);
points5.SetPoint(50,-0.6,9.73333, 0.1)
lines5.InsertCellPoint(50);
points5.SetPoint(51,-0.8,9.7, 0.1)
lines5.InsertCellPoint(51);
points5.SetPoint(52,-1,9.66667, 0.1)
lines5.InsertCellPoint(52);
points5.SetPoint(53,-1.2,9.63333, 0.1)
lines5.InsertCellPoint(53);
points5.SetPoint(54,-1.4,9.6, 0.1)
lines5.InsertCellPoint(54);
points5.SetPoint(55,-1.6,9.56667, 0.1)
lines5.InsertCellPoint(55);
points5.SetPoint(56,-1.8,9.53333, 0.1)
lines5.InsertCellPoint(56);
points5.SetPoint(57,-2,9.5, 0.1)
lines5.InsertCellPoint(57);
points5.SetPoint(58,-2.2,9.46667, 0.1)
lines5.InsertCellPoint(58);
points5.SetPoint(59,-2.4,9.43333, 0.1)
lines5.InsertCellPoint(59);
points5.SetPoint(60,-2.6,9.4, 0.1)
lines5.InsertCellPoint(60);
points5.SetPoint(61,-2.8,9.36667, 0.1)
lines5.InsertCellPoint(61);
points5.SetPoint(62,-3,9.33333, 0.1)
lines5.InsertCellPoint(62);
points5.SetPoint(63,-3.2,9.3, 0.1)
lines5.InsertCellPoint(63);
points5.SetPoint(64,-3.4,9.26667, 0.1)
lines5.InsertCellPoint(64);
points5.SetPoint(65,-3.6,9.23333, 0.1)
lines5.InsertCellPoint(65);
points5.SetPoint(66,-3.8,9.2, 0.1)
lines5.InsertCellPoint(66);
points5.SetPoint(67,-4,9.16667, 0.1)
lines5.InsertCellPoint(67);
points5.SetPoint(68,-4.2,9.13333, 0.1)
lines5.InsertCellPoint(68);
points5.SetPoint(69,-4.4,9.1, 0.1)
lines5.InsertCellPoint(69);
points5.SetPoint(70,-4.6,9.06667, 0.1)
lines5.InsertCellPoint(70);
points5.SetPoint(71,-4.8,9.03333, 0.1)
lines5.InsertCellPoint(71);
points5.SetPoint(72,-5,9, 0.1)
lines5.InsertCellPoint(72);
points5.SetPoint(73,-5,9, 0.1)
lines5.InsertCellPoint(73);
points5.SetPoint(74,-5,8.8, 0.1)
lines5.InsertCellPoint(74);
points5.SetPoint(75,-5,8.6, 0.1)
lines5.InsertCellPoint(75);
points5.SetPoint(76,-5,8.4, 0.1)
lines5.InsertCellPoint(76);
points5.SetPoint(77,-5,8.2, 0.1)
lines5.InsertCellPoint(77);
points5.SetPoint(78,-5,8, 0.1)
lines5.InsertCellPoint(78);
points5.SetPoint(79,-5,7.8, 0.1)
lines5.InsertCellPoint(79);
points5.SetPoint(80,-5,7.6, 0.1)
lines5.InsertCellPoint(80);
points5.SetPoint(81,-5,7.4, 0.1)
lines5.InsertCellPoint(81);
points5.SetPoint(82,-5,7.2, 0.1)
lines5.InsertCellPoint(82);
points5.SetPoint(83,-5,7, 0.1)
lines5.InsertCellPoint(83);
points5.SetPoint(84,-5,6.8, 0.1)
lines5.InsertCellPoint(84);
points5.SetPoint(85,-5,6.6, 0.1)
lines5.InsertCellPoint(85);
points5.SetPoint(86,-5,6.4, 0.1)
lines5.InsertCellPoint(86);
points5.SetPoint(87,-5,6.2, 0.1)
lines5.InsertCellPoint(87);
points5.SetPoint(88,-5,6, 0.1)
lines5.InsertCellPoint(88);
points5.SetPoint(89,-5,5.8, 0.1)
lines5.InsertCellPoint(89);
points5.SetPoint(90,-5,5.6, 0.1)
lines5.InsertCellPoint(90);
points5.SetPoint(91,-5,5.4, 0.1)
lines5.InsertCellPoint(91);
points5.SetPoint(92,-5,5.2, 0.1)
lines5.InsertCellPoint(92);
points5.SetPoint(93,-5,5, 0.1)
lines5.InsertCellPoint(93);
points5.SetPoint(94,-5,5, 0.1)
lines5.InsertCellPoint(94);
points5.SetPoint(95,-4.8,5, 0.1)
lines5.InsertCellPoint(95);
points5.SetPoint(96,-4.6,5, 0.1)
lines5.InsertCellPoint(96);
points5.SetPoint(97,-4.4,5, 0.1)
lines5.InsertCellPoint(97);
points5.SetPoint(98,-4.2,5, 0.1)
lines5.InsertCellPoint(98);
points5.SetPoint(99,-4,5, 0.1)
lines5.InsertCellPoint(99);
points5.SetPoint(100,-3.8,5, 0.1)
lines5.InsertCellPoint(100);
points5.SetPoint(101,-3.6,5, 0.1)
lines5.InsertCellPoint(101);
points5.SetPoint(102,-3.4,5, 0.1)
lines5.InsertCellPoint(102);
points5.SetPoint(103,-3.2,5, 0.1)
lines5.InsertCellPoint(103);
points5.SetPoint(104,-3,5, 0.1)
lines5.InsertCellPoint(104);
points5.SetPoint(105,-2.8,5, 0.1)
lines5.InsertCellPoint(105);
points5.SetPoint(106,-2.6,5, 0.1)
lines5.InsertCellPoint(106);
points5.SetPoint(107,-2.4,5, 0.1)
lines5.InsertCellPoint(107);
points5.SetPoint(108,-2.2,5, 0.1)
lines5.InsertCellPoint(108);
points5.SetPoint(109,-2,5, 0.1)
lines5.InsertCellPoint(109);
points5.SetPoint(110,-1.8,5, 0.1)
lines5.InsertCellPoint(110);
points5.SetPoint(111,-1.6,5, 0.1)
lines5.InsertCellPoint(111);
points5.SetPoint(112,-1.4,5, 0.1)
lines5.InsertCellPoint(112);
points5.SetPoint(113,-1.2,5, 0.1)
lines5.InsertCellPoint(113);
points5.SetPoint(114,-1,5, 0.1)
lines5.InsertCellPoint(114);
points5.SetPoint(115,-0.8,5, 0.1)
lines5.InsertCellPoint(115);
points5.SetPoint(116,-0.6,5, 0.1)
lines5.InsertCellPoint(116);
points5.SetPoint(117,-0.4,5, 0.1)
lines5.InsertCellPoint(117);
points5.SetPoint(118,-0.2,5, 0.1)
lines5.InsertCellPoint(118);
points5.SetPoint(119,0,5, 0.1)
lines5.InsertCellPoint(119);
points5.SetPoint(120,0,5, 0.1)
lines5.InsertCellPoint(120);
points5.SetPoint(121,0,5.20833, 0.1)
lines5.InsertCellPoint(121);
points5.SetPoint(122,0,5.41667, 0.1)
lines5.InsertCellPoint(122);
points5.SetPoint(123,0,5.625, 0.1)
lines5.InsertCellPoint(123);
points5.SetPoint(124,0,5.83333, 0.1)
lines5.InsertCellPoint(124);
points5.SetPoint(125,0,6.04167, 0.1)
lines5.InsertCellPoint(125);
points5.SetPoint(126,0,6.25, 0.1)
lines5.InsertCellPoint(126);
points5.SetPoint(127,0,6.45833, 0.1)
lines5.InsertCellPoint(127);
points5.SetPoint(128,0,6.66667, 0.1)
lines5.InsertCellPoint(128);
points5.SetPoint(129,0,6.875, 0.1)
lines5.InsertCellPoint(129);
points5.SetPoint(130,0,7.08333, 0.1)
lines5.InsertCellPoint(130);
points5.SetPoint(131,0,7.29167, 0.1)
lines5.InsertCellPoint(131);
points5.SetPoint(132,0,7.5, 0.1)
lines5.InsertCellPoint(132);
points5.SetPoint(133,0,7.5, 0.1)
lines5.InsertCellPoint(133);
points5.SetPoint(134,0.185185,7.40741, 0.1)
lines5.InsertCellPoint(134);
points5.SetPoint(135,0.37037,7.31481, 0.1)
lines5.InsertCellPoint(135);
points5.SetPoint(136,0.555556,7.22222, 0.1)
lines5.InsertCellPoint(136);
points5.SetPoint(137,0.740741,7.12963, 0.1)
lines5.InsertCellPoint(137);
points5.SetPoint(138,0.925926,7.03704, 0.1)
lines5.InsertCellPoint(138);
points5.SetPoint(139,1.11111,6.94444, 0.1)
lines5.InsertCellPoint(139);
points5.SetPoint(140,1.2963,6.85185, 0.1)
lines5.InsertCellPoint(140);
points5.SetPoint(141,1.48148,6.75926, 0.1)
lines5.InsertCellPoint(141);
points5.SetPoint(142,1.66667,6.66667, 0.1)
lines5.InsertCellPoint(142);
points5.SetPoint(143,1.85185,6.57407, 0.1)
lines5.InsertCellPoint(143);
points5.SetPoint(144,2.03704,6.48148, 0.1)
lines5.InsertCellPoint(144);
points5.SetPoint(145,2.22222,6.38889, 0.1)
lines5.InsertCellPoint(145);
points5.SetPoint(146,2.40741,6.2963, 0.1)
lines5.InsertCellPoint(146);
points5.SetPoint(147,2.59259,6.2037, 0.1)
lines5.InsertCellPoint(147);
points5.SetPoint(148,2.77778,6.11111, 0.1)
lines5.InsertCellPoint(148);
points5.SetPoint(149,2.96296,6.01852, 0.1)
lines5.InsertCellPoint(149);
points5.SetPoint(150,3.14815,5.92593, 0.1)
lines5.InsertCellPoint(150);
points5.SetPoint(151,3.33333,5.83333, 0.1)
lines5.InsertCellPoint(151);
points5.SetPoint(152,3.51852,5.74074, 0.1)
lines5.InsertCellPoint(152);
points5.SetPoint(153,3.7037,5.64815, 0.1)
lines5.InsertCellPoint(153);
points5.SetPoint(154,3.88889,5.55556, 0.1)
lines5.InsertCellPoint(154);
points5.SetPoint(155,4.07407,5.46296, 0.1)
lines5.InsertCellPoint(155);
points5.SetPoint(156,4.25926,5.37037, 0.1)
lines5.InsertCellPoint(156);
points5.SetPoint(157,4.44444,5.27778, 0.1)
lines5.InsertCellPoint(157);
points5.SetPoint(158,4.62963,5.18519, 0.1)
lines5.InsertCellPoint(158);
points5.SetPoint(159,4.81481,5.09259, 0.1)
lines5.InsertCellPoint(159);
points5.SetPoint(160,5,5, 0.1)
lines5.InsertCellPoint(160);
lines5.InsertCellPoint(0);
polygon5 = vtk.vtkPolyData()
polygon5.SetPoints(points5)
polygon5.SetLines(lines5)
polygonMapper5 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper5.SetInputConnection(polygon5.GetProducerPort())
else:
	polygonMapper5.SetInputData(polygon5)
	polygonMapper5.Update()
polygonActor5 = vtk.vtkActor()
polygonActor5.SetMapper(polygonMapper5)
polygonActor5.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor5.GetProperty().SetLineWidth(widthLine)
points6 = vtk.vtkPoints()
points6.SetNumberOfPoints(188)
lines6 = vtk.vtkCellArray()
lines6.InsertNextCell(189)
points6.SetPoint(0,12,4, 0.1)
lines6.InsertCellPoint(0);
points6.SetPoint(1,11.9184,4.18367, 0.1)
lines6.InsertCellPoint(1);
points6.SetPoint(2,11.8367,4.36735, 0.1)
lines6.InsertCellPoint(2);
points6.SetPoint(3,11.7551,4.55102, 0.1)
lines6.InsertCellPoint(3);
points6.SetPoint(4,11.6735,4.73469, 0.1)
lines6.InsertCellPoint(4);
points6.SetPoint(5,11.5918,4.91837, 0.1)
lines6.InsertCellPoint(5);
points6.SetPoint(6,11.5102,5.10204, 0.1)
lines6.InsertCellPoint(6);
points6.SetPoint(7,11.4286,5.28571, 0.1)
lines6.InsertCellPoint(7);
points6.SetPoint(8,11.3469,5.46939, 0.1)
lines6.InsertCellPoint(8);
points6.SetPoint(9,11.2653,5.65306, 0.1)
lines6.InsertCellPoint(9);
points6.SetPoint(10,11.1837,5.83673, 0.1)
lines6.InsertCellPoint(10);
points6.SetPoint(11,11.102,6.02041, 0.1)
lines6.InsertCellPoint(11);
points6.SetPoint(12,11.0204,6.20408, 0.1)
lines6.InsertCellPoint(12);
points6.SetPoint(13,10.9388,6.38776, 0.1)
lines6.InsertCellPoint(13);
points6.SetPoint(14,10.8571,6.57143, 0.1)
lines6.InsertCellPoint(14);
points6.SetPoint(15,10.7755,6.7551, 0.1)
lines6.InsertCellPoint(15);
points6.SetPoint(16,10.6939,6.93878, 0.1)
lines6.InsertCellPoint(16);
points6.SetPoint(17,10.6122,7.12245, 0.1)
lines6.InsertCellPoint(17);
points6.SetPoint(18,10.5306,7.30612, 0.1)
lines6.InsertCellPoint(18);
points6.SetPoint(19,10.449,7.4898, 0.1)
lines6.InsertCellPoint(19);
points6.SetPoint(20,10.3673,7.67347, 0.1)
lines6.InsertCellPoint(20);
points6.SetPoint(21,10.2857,7.85714, 0.1)
lines6.InsertCellPoint(21);
points6.SetPoint(22,10.2041,8.04082, 0.1)
lines6.InsertCellPoint(22);
points6.SetPoint(23,10.1224,8.22449, 0.1)
lines6.InsertCellPoint(23);
points6.SetPoint(24,10.0408,8.40816, 0.1)
lines6.InsertCellPoint(24);
points6.SetPoint(25,9.95918,8.59184, 0.1)
lines6.InsertCellPoint(25);
points6.SetPoint(26,9.87755,8.77551, 0.1)
lines6.InsertCellPoint(26);
points6.SetPoint(27,9.79592,8.95918, 0.1)
lines6.InsertCellPoint(27);
points6.SetPoint(28,9.71429,9.14286, 0.1)
lines6.InsertCellPoint(28);
points6.SetPoint(29,9.63265,9.32653, 0.1)
lines6.InsertCellPoint(29);
points6.SetPoint(30,9.55102,9.5102, 0.1)
lines6.InsertCellPoint(30);
points6.SetPoint(31,9.46939,9.69388, 0.1)
lines6.InsertCellPoint(31);
points6.SetPoint(32,9.38776,9.87755, 0.1)
lines6.InsertCellPoint(32);
points6.SetPoint(33,9.30612,10.0612, 0.1)
lines6.InsertCellPoint(33);
points6.SetPoint(34,9.22449,10.2449, 0.1)
lines6.InsertCellPoint(34);
points6.SetPoint(35,9.14286,10.4286, 0.1)
lines6.InsertCellPoint(35);
points6.SetPoint(36,9.06122,10.6122, 0.1)
lines6.InsertCellPoint(36);
points6.SetPoint(37,8.97959,10.7959, 0.1)
lines6.InsertCellPoint(37);
points6.SetPoint(38,8.89796,10.9796, 0.1)
lines6.InsertCellPoint(38);
points6.SetPoint(39,8.81633,11.1633, 0.1)
lines6.InsertCellPoint(39);
points6.SetPoint(40,8.73469,11.3469, 0.1)
lines6.InsertCellPoint(40);
points6.SetPoint(41,8.65306,11.5306, 0.1)
lines6.InsertCellPoint(41);
points6.SetPoint(42,8.57143,11.7143, 0.1)
lines6.InsertCellPoint(42);
points6.SetPoint(43,8.4898,11.898, 0.1)
lines6.InsertCellPoint(43);
points6.SetPoint(44,8.40816,12.0816, 0.1)
lines6.InsertCellPoint(44);
points6.SetPoint(45,8.32653,12.2653, 0.1)
lines6.InsertCellPoint(45);
points6.SetPoint(46,8.2449,12.449, 0.1)
lines6.InsertCellPoint(46);
points6.SetPoint(47,8.16327,12.6327, 0.1)
lines6.InsertCellPoint(47);
points6.SetPoint(48,8.08163,12.8163, 0.1)
lines6.InsertCellPoint(48);
points6.SetPoint(49,8,13, 0.1)
lines6.InsertCellPoint(49);
points6.SetPoint(50,8,13, 0.1)
lines6.InsertCellPoint(50);
points6.SetPoint(51,7.8,13.0125, 0.1)
lines6.InsertCellPoint(51);
points6.SetPoint(52,7.6,13.025, 0.1)
lines6.InsertCellPoint(52);
points6.SetPoint(53,7.4,13.0375, 0.1)
lines6.InsertCellPoint(53);
points6.SetPoint(54,7.2,13.05, 0.1)
lines6.InsertCellPoint(54);
points6.SetPoint(55,7,13.0625, 0.1)
lines6.InsertCellPoint(55);
points6.SetPoint(56,6.8,13.075, 0.1)
lines6.InsertCellPoint(56);
points6.SetPoint(57,6.6,13.0875, 0.1)
lines6.InsertCellPoint(57);
points6.SetPoint(58,6.4,13.1, 0.1)
lines6.InsertCellPoint(58);
points6.SetPoint(59,6.2,13.1125, 0.1)
lines6.InsertCellPoint(59);
points6.SetPoint(60,6,13.125, 0.1)
lines6.InsertCellPoint(60);
points6.SetPoint(61,5.8,13.1375, 0.1)
lines6.InsertCellPoint(61);
points6.SetPoint(62,5.6,13.15, 0.1)
lines6.InsertCellPoint(62);
points6.SetPoint(63,5.4,13.1625, 0.1)
lines6.InsertCellPoint(63);
points6.SetPoint(64,5.2,13.175, 0.1)
lines6.InsertCellPoint(64);
points6.SetPoint(65,5,13.1875, 0.1)
lines6.InsertCellPoint(65);
points6.SetPoint(66,4.8,13.2, 0.1)
lines6.InsertCellPoint(66);
points6.SetPoint(67,4.6,13.2125, 0.1)
lines6.InsertCellPoint(67);
points6.SetPoint(68,4.4,13.225, 0.1)
lines6.InsertCellPoint(68);
points6.SetPoint(69,4.2,13.2375, 0.1)
lines6.InsertCellPoint(69);
points6.SetPoint(70,4,13.25, 0.1)
lines6.InsertCellPoint(70);
points6.SetPoint(71,3.8,13.2625, 0.1)
lines6.InsertCellPoint(71);
points6.SetPoint(72,3.6,13.275, 0.1)
lines6.InsertCellPoint(72);
points6.SetPoint(73,3.4,13.2875, 0.1)
lines6.InsertCellPoint(73);
points6.SetPoint(74,3.2,13.3, 0.1)
lines6.InsertCellPoint(74);
points6.SetPoint(75,3,13.3125, 0.1)
lines6.InsertCellPoint(75);
points6.SetPoint(76,2.8,13.325, 0.1)
lines6.InsertCellPoint(76);
points6.SetPoint(77,2.6,13.3375, 0.1)
lines6.InsertCellPoint(77);
points6.SetPoint(78,2.4,13.35, 0.1)
lines6.InsertCellPoint(78);
points6.SetPoint(79,2.2,13.3625, 0.1)
lines6.InsertCellPoint(79);
points6.SetPoint(80,2,13.375, 0.1)
lines6.InsertCellPoint(80);
points6.SetPoint(81,1.8,13.3875, 0.1)
lines6.InsertCellPoint(81);
points6.SetPoint(82,1.6,13.4, 0.1)
lines6.InsertCellPoint(82);
points6.SetPoint(83,1.4,13.4125, 0.1)
lines6.InsertCellPoint(83);
points6.SetPoint(84,1.2,13.425, 0.1)
lines6.InsertCellPoint(84);
points6.SetPoint(85,1,13.4375, 0.1)
lines6.InsertCellPoint(85);
points6.SetPoint(86,0.8,13.45, 0.1)
lines6.InsertCellPoint(86);
points6.SetPoint(87,0.6,13.4625, 0.1)
lines6.InsertCellPoint(87);
points6.SetPoint(88,0.4,13.475, 0.1)
lines6.InsertCellPoint(88);
points6.SetPoint(89,0.2,13.4875, 0.1)
lines6.InsertCellPoint(89);
points6.SetPoint(90,0,13.5, 0.1)
lines6.InsertCellPoint(90);
points6.SetPoint(91,0,13.5, 0.1)
lines6.InsertCellPoint(91);
points6.SetPoint(92,0.0555556,13.3056, 0.1)
lines6.InsertCellPoint(92);
points6.SetPoint(93,0.111111,13.1111, 0.1)
lines6.InsertCellPoint(93);
points6.SetPoint(94,0.166667,12.9167, 0.1)
lines6.InsertCellPoint(94);
points6.SetPoint(95,0.222222,12.7222, 0.1)
lines6.InsertCellPoint(95);
points6.SetPoint(96,0.277778,12.5278, 0.1)
lines6.InsertCellPoint(96);
points6.SetPoint(97,0.333333,12.3333, 0.1)
lines6.InsertCellPoint(97);
points6.SetPoint(98,0.388889,12.1389, 0.1)
lines6.InsertCellPoint(98);
points6.SetPoint(99,0.444444,11.9444, 0.1)
lines6.InsertCellPoint(99);
points6.SetPoint(100,0.5,11.75, 0.1)
lines6.InsertCellPoint(100);
points6.SetPoint(101,0.555556,11.5556, 0.1)
lines6.InsertCellPoint(101);
points6.SetPoint(102,0.611111,11.3611, 0.1)
lines6.InsertCellPoint(102);
points6.SetPoint(103,0.666667,11.1667, 0.1)
lines6.InsertCellPoint(103);
points6.SetPoint(104,0.722222,10.9722, 0.1)
lines6.InsertCellPoint(104);
points6.SetPoint(105,0.777778,10.7778, 0.1)
lines6.InsertCellPoint(105);
points6.SetPoint(106,0.833333,10.5833, 0.1)
lines6.InsertCellPoint(106);
points6.SetPoint(107,0.888889,10.3889, 0.1)
lines6.InsertCellPoint(107);
points6.SetPoint(108,0.944444,10.1944, 0.1)
lines6.InsertCellPoint(108);
points6.SetPoint(109,1,10, 0.1)
lines6.InsertCellPoint(109);
points6.SetPoint(110,1,10, 0.1)
lines6.InsertCellPoint(110);
points6.SetPoint(111,1.2,9.95, 0.1)
lines6.InsertCellPoint(111);
points6.SetPoint(112,1.4,9.9, 0.1)
lines6.InsertCellPoint(112);
points6.SetPoint(113,1.6,9.85, 0.1)
lines6.InsertCellPoint(113);
points6.SetPoint(114,1.8,9.8, 0.1)
lines6.InsertCellPoint(114);
points6.SetPoint(115,2,9.75, 0.1)
lines6.InsertCellPoint(115);
points6.SetPoint(116,2.2,9.7, 0.1)
lines6.InsertCellPoint(116);
points6.SetPoint(117,2.4,9.65, 0.1)
lines6.InsertCellPoint(117);
points6.SetPoint(118,2.6,9.6, 0.1)
lines6.InsertCellPoint(118);
points6.SetPoint(119,2.8,9.55, 0.1)
lines6.InsertCellPoint(119);
points6.SetPoint(120,3,9.5, 0.1)
lines6.InsertCellPoint(120);
points6.SetPoint(121,3.2,9.45, 0.1)
lines6.InsertCellPoint(121);
points6.SetPoint(122,3.4,9.4, 0.1)
lines6.InsertCellPoint(122);
points6.SetPoint(123,3.6,9.35, 0.1)
lines6.InsertCellPoint(123);
points6.SetPoint(124,3.8,9.3, 0.1)
lines6.InsertCellPoint(124);
points6.SetPoint(125,4,9.25, 0.1)
lines6.InsertCellPoint(125);
points6.SetPoint(126,4.2,9.2, 0.1)
lines6.InsertCellPoint(126);
points6.SetPoint(127,4.4,9.15, 0.1)
lines6.InsertCellPoint(127);
points6.SetPoint(128,4.6,9.1, 0.1)
lines6.InsertCellPoint(128);
points6.SetPoint(129,4.8,9.05, 0.1)
lines6.InsertCellPoint(129);
points6.SetPoint(130,5,9, 0.1)
lines6.InsertCellPoint(130);
points6.SetPoint(131,5,9, 0.1)
lines6.InsertCellPoint(131);
points6.SetPoint(132,5,8.8, 0.1)
lines6.InsertCellPoint(132);
points6.SetPoint(133,5,8.6, 0.1)
lines6.InsertCellPoint(133);
points6.SetPoint(134,5,8.4, 0.1)
lines6.InsertCellPoint(134);
points6.SetPoint(135,5,8.2, 0.1)
lines6.InsertCellPoint(135);
points6.SetPoint(136,5,8, 0.1)
lines6.InsertCellPoint(136);
points6.SetPoint(137,5,7.8, 0.1)
lines6.InsertCellPoint(137);
points6.SetPoint(138,5,7.6, 0.1)
lines6.InsertCellPoint(138);
points6.SetPoint(139,5,7.4, 0.1)
lines6.InsertCellPoint(139);
points6.SetPoint(140,5,7.2, 0.1)
lines6.InsertCellPoint(140);
points6.SetPoint(141,5,7, 0.1)
lines6.InsertCellPoint(141);
points6.SetPoint(142,5,6.8, 0.1)
lines6.InsertCellPoint(142);
points6.SetPoint(143,5,6.6, 0.1)
lines6.InsertCellPoint(143);
points6.SetPoint(144,5,6.4, 0.1)
lines6.InsertCellPoint(144);
points6.SetPoint(145,5,6.2, 0.1)
lines6.InsertCellPoint(145);
points6.SetPoint(146,5,6, 0.1)
lines6.InsertCellPoint(146);
points6.SetPoint(147,5,5.8, 0.1)
lines6.InsertCellPoint(147);
points6.SetPoint(148,5,5.6, 0.1)
lines6.InsertCellPoint(148);
points6.SetPoint(149,5,5.4, 0.1)
lines6.InsertCellPoint(149);
points6.SetPoint(150,5,5.2, 0.1)
lines6.InsertCellPoint(150);
points6.SetPoint(151,5,5, 0.1)
lines6.InsertCellPoint(151);
points6.SetPoint(152,5,5, 0.1)
lines6.InsertCellPoint(152);
points6.SetPoint(153,5.2,4.97143, 0.1)
lines6.InsertCellPoint(153);
points6.SetPoint(154,5.4,4.94286, 0.1)
lines6.InsertCellPoint(154);
points6.SetPoint(155,5.6,4.91429, 0.1)
lines6.InsertCellPoint(155);
points6.SetPoint(156,5.8,4.88571, 0.1)
lines6.InsertCellPoint(156);
points6.SetPoint(157,6,4.85714, 0.1)
lines6.InsertCellPoint(157);
points6.SetPoint(158,6.2,4.82857, 0.1)
lines6.InsertCellPoint(158);
points6.SetPoint(159,6.4,4.8, 0.1)
lines6.InsertCellPoint(159);
points6.SetPoint(160,6.6,4.77143, 0.1)
lines6.InsertCellPoint(160);
points6.SetPoint(161,6.8,4.74286, 0.1)
lines6.InsertCellPoint(161);
points6.SetPoint(162,7,4.71429, 0.1)
lines6.InsertCellPoint(162);
points6.SetPoint(163,7.2,4.68571, 0.1)
lines6.InsertCellPoint(163);
points6.SetPoint(164,7.4,4.65714, 0.1)
lines6.InsertCellPoint(164);
points6.SetPoint(165,7.6,4.62857, 0.1)
lines6.InsertCellPoint(165);
points6.SetPoint(166,7.8,4.6, 0.1)
lines6.InsertCellPoint(166);
points6.SetPoint(167,8,4.57143, 0.1)
lines6.InsertCellPoint(167);
points6.SetPoint(168,8.2,4.54286, 0.1)
lines6.InsertCellPoint(168);
points6.SetPoint(169,8.4,4.51429, 0.1)
lines6.InsertCellPoint(169);
points6.SetPoint(170,8.6,4.48571, 0.1)
lines6.InsertCellPoint(170);
points6.SetPoint(171,8.8,4.45714, 0.1)
lines6.InsertCellPoint(171);
points6.SetPoint(172,9,4.42857, 0.1)
lines6.InsertCellPoint(172);
points6.SetPoint(173,9.2,4.4, 0.1)
lines6.InsertCellPoint(173);
points6.SetPoint(174,9.4,4.37143, 0.1)
lines6.InsertCellPoint(174);
points6.SetPoint(175,9.6,4.34286, 0.1)
lines6.InsertCellPoint(175);
points6.SetPoint(176,9.8,4.31429, 0.1)
lines6.InsertCellPoint(176);
points6.SetPoint(177,10,4.28571, 0.1)
lines6.InsertCellPoint(177);
points6.SetPoint(178,10.2,4.25714, 0.1)
lines6.InsertCellPoint(178);
points6.SetPoint(179,10.4,4.22857, 0.1)
lines6.InsertCellPoint(179);
points6.SetPoint(180,10.6,4.2, 0.1)
lines6.InsertCellPoint(180);
points6.SetPoint(181,10.8,4.17143, 0.1)
lines6.InsertCellPoint(181);
points6.SetPoint(182,11,4.14286, 0.1)
lines6.InsertCellPoint(182);
points6.SetPoint(183,11.2,4.11429, 0.1)
lines6.InsertCellPoint(183);
points6.SetPoint(184,11.4,4.08571, 0.1)
lines6.InsertCellPoint(184);
points6.SetPoint(185,11.6,4.05714, 0.1)
lines6.InsertCellPoint(185);
points6.SetPoint(186,11.8,4.02857, 0.1)
lines6.InsertCellPoint(186);
points6.SetPoint(187,12,4, 0.1)
lines6.InsertCellPoint(187);
lines6.InsertCellPoint(0);
polygon6 = vtk.vtkPolyData()
polygon6.SetPoints(points6)
polygon6.SetLines(lines6)
polygonMapper6 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper6.SetInputConnection(polygon6.GetProducerPort())
else:
	polygonMapper6.SetInputData(polygon6)
	polygonMapper6.Update()
polygonActor6 = vtk.vtkActor()
polygonActor6.SetMapper(polygonMapper6)
polygonActor6.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor6.GetProperty().SetLineWidth(widthLine)
points7 = vtk.vtkPoints()
points7.SetNumberOfPoints(84)
lines7 = vtk.vtkCellArray()
lines7.InsertNextCell(85)
points7.SetPoint(0,0,13.5, 0.1)
lines7.InsertCellPoint(0);
points7.SetPoint(1,-0.151515,13.3636, 0.1)
lines7.InsertCellPoint(1);
points7.SetPoint(2,-0.30303,13.2273, 0.1)
lines7.InsertCellPoint(2);
points7.SetPoint(3,-0.454545,13.0909, 0.1)
lines7.InsertCellPoint(3);
points7.SetPoint(4,-0.606061,12.9545, 0.1)
lines7.InsertCellPoint(4);
points7.SetPoint(5,-0.757576,12.8182, 0.1)
lines7.InsertCellPoint(5);
points7.SetPoint(6,-0.909091,12.6818, 0.1)
lines7.InsertCellPoint(6);
points7.SetPoint(7,-1.06061,12.5455, 0.1)
lines7.InsertCellPoint(7);
points7.SetPoint(8,-1.21212,12.4091, 0.1)
lines7.InsertCellPoint(8);
points7.SetPoint(9,-1.36364,12.2727, 0.1)
lines7.InsertCellPoint(9);
points7.SetPoint(10,-1.51515,12.1364, 0.1)
lines7.InsertCellPoint(10);
points7.SetPoint(11,-1.66667,12, 0.1)
lines7.InsertCellPoint(11);
points7.SetPoint(12,-1.81818,11.8636, 0.1)
lines7.InsertCellPoint(12);
points7.SetPoint(13,-1.9697,11.7273, 0.1)
lines7.InsertCellPoint(13);
points7.SetPoint(14,-2.12121,11.5909, 0.1)
lines7.InsertCellPoint(14);
points7.SetPoint(15,-2.27273,11.4545, 0.1)
lines7.InsertCellPoint(15);
points7.SetPoint(16,-2.42424,11.3182, 0.1)
lines7.InsertCellPoint(16);
points7.SetPoint(17,-2.57576,11.1818, 0.1)
lines7.InsertCellPoint(17);
points7.SetPoint(18,-2.72727,11.0455, 0.1)
lines7.InsertCellPoint(18);
points7.SetPoint(19,-2.87879,10.9091, 0.1)
lines7.InsertCellPoint(19);
points7.SetPoint(20,-3.0303,10.7727, 0.1)
lines7.InsertCellPoint(20);
points7.SetPoint(21,-3.18182,10.6364, 0.1)
lines7.InsertCellPoint(21);
points7.SetPoint(22,-3.33333,10.5, 0.1)
lines7.InsertCellPoint(22);
points7.SetPoint(23,-3.48485,10.3636, 0.1)
lines7.InsertCellPoint(23);
points7.SetPoint(24,-3.63636,10.2273, 0.1)
lines7.InsertCellPoint(24);
points7.SetPoint(25,-3.78788,10.0909, 0.1)
lines7.InsertCellPoint(25);
points7.SetPoint(26,-3.93939,9.95455, 0.1)
lines7.InsertCellPoint(26);
points7.SetPoint(27,-4.09091,9.81818, 0.1)
lines7.InsertCellPoint(27);
points7.SetPoint(28,-4.24242,9.68182, 0.1)
lines7.InsertCellPoint(28);
points7.SetPoint(29,-4.39394,9.54545, 0.1)
lines7.InsertCellPoint(29);
points7.SetPoint(30,-4.54545,9.40909, 0.1)
lines7.InsertCellPoint(30);
points7.SetPoint(31,-4.69697,9.27273, 0.1)
lines7.InsertCellPoint(31);
points7.SetPoint(32,-4.84848,9.13636, 0.1)
lines7.InsertCellPoint(32);
points7.SetPoint(33,-5,9, 0.1)
lines7.InsertCellPoint(33);
points7.SetPoint(34,-5,9, 0.1)
lines7.InsertCellPoint(34);
points7.SetPoint(35,-4.8,9.03333, 0.1)
lines7.InsertCellPoint(35);
points7.SetPoint(36,-4.6,9.06667, 0.1)
lines7.InsertCellPoint(36);
points7.SetPoint(37,-4.4,9.1, 0.1)
lines7.InsertCellPoint(37);
points7.SetPoint(38,-4.2,9.13333, 0.1)
lines7.InsertCellPoint(38);
points7.SetPoint(39,-4,9.16667, 0.1)
lines7.InsertCellPoint(39);
points7.SetPoint(40,-3.8,9.2, 0.1)
lines7.InsertCellPoint(40);
points7.SetPoint(41,-3.6,9.23333, 0.1)
lines7.InsertCellPoint(41);
points7.SetPoint(42,-3.4,9.26667, 0.1)
lines7.InsertCellPoint(42);
points7.SetPoint(43,-3.2,9.3, 0.1)
lines7.InsertCellPoint(43);
points7.SetPoint(44,-3,9.33333, 0.1)
lines7.InsertCellPoint(44);
points7.SetPoint(45,-2.8,9.36667, 0.1)
lines7.InsertCellPoint(45);
points7.SetPoint(46,-2.6,9.4, 0.1)
lines7.InsertCellPoint(46);
points7.SetPoint(47,-2.4,9.43333, 0.1)
lines7.InsertCellPoint(47);
points7.SetPoint(48,-2.2,9.46667, 0.1)
lines7.InsertCellPoint(48);
points7.SetPoint(49,-2,9.5, 0.1)
lines7.InsertCellPoint(49);
points7.SetPoint(50,-1.8,9.53333, 0.1)
lines7.InsertCellPoint(50);
points7.SetPoint(51,-1.6,9.56667, 0.1)
lines7.InsertCellPoint(51);
points7.SetPoint(52,-1.4,9.6, 0.1)
lines7.InsertCellPoint(52);
points7.SetPoint(53,-1.2,9.63333, 0.1)
lines7.InsertCellPoint(53);
points7.SetPoint(54,-1,9.66667, 0.1)
lines7.InsertCellPoint(54);
points7.SetPoint(55,-0.8,9.7, 0.1)
lines7.InsertCellPoint(55);
points7.SetPoint(56,-0.6,9.73333, 0.1)
lines7.InsertCellPoint(56);
points7.SetPoint(57,-0.4,9.76667, 0.1)
lines7.InsertCellPoint(57);
points7.SetPoint(58,-0.2,9.8, 0.1)
lines7.InsertCellPoint(58);
points7.SetPoint(59,1.11022e-16,9.83333, 0.1)
lines7.InsertCellPoint(59);
points7.SetPoint(60,0.2,9.86667, 0.1)
lines7.InsertCellPoint(60);
points7.SetPoint(61,0.4,9.9, 0.1)
lines7.InsertCellPoint(61);
points7.SetPoint(62,0.6,9.93333, 0.1)
lines7.InsertCellPoint(62);
points7.SetPoint(63,0.8,9.96667, 0.1)
lines7.InsertCellPoint(63);
points7.SetPoint(64,1,10, 0.1)
lines7.InsertCellPoint(64);
points7.SetPoint(65,1,10, 0.1)
lines7.InsertCellPoint(65);
points7.SetPoint(66,0.944444,10.1944, 0.1)
lines7.InsertCellPoint(66);
points7.SetPoint(67,0.888889,10.3889, 0.1)
lines7.InsertCellPoint(67);
points7.SetPoint(68,0.833333,10.5833, 0.1)
lines7.InsertCellPoint(68);
points7.SetPoint(69,0.777778,10.7778, 0.1)
lines7.InsertCellPoint(69);
points7.SetPoint(70,0.722222,10.9722, 0.1)
lines7.InsertCellPoint(70);
points7.SetPoint(71,0.666667,11.1667, 0.1)
lines7.InsertCellPoint(71);
points7.SetPoint(72,0.611111,11.3611, 0.1)
lines7.InsertCellPoint(72);
points7.SetPoint(73,0.555556,11.5556, 0.1)
lines7.InsertCellPoint(73);
points7.SetPoint(74,0.5,11.75, 0.1)
lines7.InsertCellPoint(74);
points7.SetPoint(75,0.444444,11.9444, 0.1)
lines7.InsertCellPoint(75);
points7.SetPoint(76,0.388889,12.1389, 0.1)
lines7.InsertCellPoint(76);
points7.SetPoint(77,0.333333,12.3333, 0.1)
lines7.InsertCellPoint(77);
points7.SetPoint(78,0.277778,12.5278, 0.1)
lines7.InsertCellPoint(78);
points7.SetPoint(79,0.222222,12.7222, 0.1)
lines7.InsertCellPoint(79);
points7.SetPoint(80,0.166667,12.9167, 0.1)
lines7.InsertCellPoint(80);
points7.SetPoint(81,0.111111,13.1111, 0.1)
lines7.InsertCellPoint(81);
points7.SetPoint(82,0.0555556,13.3056, 0.1)
lines7.InsertCellPoint(82);
points7.SetPoint(83,0,13.5, 0.1)
lines7.InsertCellPoint(83);
lines7.InsertCellPoint(0);
polygon7 = vtk.vtkPolyData()
polygon7.SetPoints(points7)
polygon7.SetLines(lines7)
polygonMapper7 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper7.SetInputConnection(polygon7.GetProducerPort())
else:
	polygonMapper7.SetInputData(polygon7)
	polygonMapper7.Update()
polygonActor7 = vtk.vtkActor()
polygonActor7.SetMapper(polygonMapper7)
polygonActor7.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor7.GetProperty().SetLineWidth(widthLine)
points8 = vtk.vtkPoints()
points8.SetNumberOfPoints(277)
lines8 = vtk.vtkCellArray()
lines8.InsertNextCell(278)
points8.SetPoint(0,0,13.5, 0.1)
lines8.InsertCellPoint(0);
points8.SetPoint(1,-0.2,13.5333, 0.1)
lines8.InsertCellPoint(1);
points8.SetPoint(2,-0.4,13.5667, 0.1)
lines8.InsertCellPoint(2);
points8.SetPoint(3,-0.6,13.6, 0.1)
lines8.InsertCellPoint(3);
points8.SetPoint(4,-0.8,13.6333, 0.1)
lines8.InsertCellPoint(4);
points8.SetPoint(5,-1,13.6667, 0.1)
lines8.InsertCellPoint(5);
points8.SetPoint(6,-1.2,13.7, 0.1)
lines8.InsertCellPoint(6);
points8.SetPoint(7,-1.4,13.7333, 0.1)
lines8.InsertCellPoint(7);
points8.SetPoint(8,-1.6,13.7667, 0.1)
lines8.InsertCellPoint(8);
points8.SetPoint(9,-1.8,13.8, 0.1)
lines8.InsertCellPoint(9);
points8.SetPoint(10,-2,13.8333, 0.1)
lines8.InsertCellPoint(10);
points8.SetPoint(11,-2.2,13.8667, 0.1)
lines8.InsertCellPoint(11);
points8.SetPoint(12,-2.4,13.9, 0.1)
lines8.InsertCellPoint(12);
points8.SetPoint(13,-2.6,13.9333, 0.1)
lines8.InsertCellPoint(13);
points8.SetPoint(14,-2.8,13.9667, 0.1)
lines8.InsertCellPoint(14);
points8.SetPoint(15,-3,14, 0.1)
lines8.InsertCellPoint(15);
points8.SetPoint(16,-3.2,14.0333, 0.1)
lines8.InsertCellPoint(16);
points8.SetPoint(17,-3.4,14.0667, 0.1)
lines8.InsertCellPoint(17);
points8.SetPoint(18,-3.6,14.1, 0.1)
lines8.InsertCellPoint(18);
points8.SetPoint(19,-3.8,14.1333, 0.1)
lines8.InsertCellPoint(19);
points8.SetPoint(20,-4,14.1667, 0.1)
lines8.InsertCellPoint(20);
points8.SetPoint(21,-4.2,14.2, 0.1)
lines8.InsertCellPoint(21);
points8.SetPoint(22,-4.4,14.2333, 0.1)
lines8.InsertCellPoint(22);
points8.SetPoint(23,-4.6,14.2667, 0.1)
lines8.InsertCellPoint(23);
points8.SetPoint(24,-4.8,14.3, 0.1)
lines8.InsertCellPoint(24);
points8.SetPoint(25,-5,14.3333, 0.1)
lines8.InsertCellPoint(25);
points8.SetPoint(26,-5.2,14.3667, 0.1)
lines8.InsertCellPoint(26);
points8.SetPoint(27,-5.4,14.4, 0.1)
lines8.InsertCellPoint(27);
points8.SetPoint(28,-5.6,14.4333, 0.1)
lines8.InsertCellPoint(28);
points8.SetPoint(29,-5.8,14.4667, 0.1)
lines8.InsertCellPoint(29);
points8.SetPoint(30,-6,14.5, 0.1)
lines8.InsertCellPoint(30);
points8.SetPoint(31,-6.2,14.5333, 0.1)
lines8.InsertCellPoint(31);
points8.SetPoint(32,-6.4,14.5667, 0.1)
lines8.InsertCellPoint(32);
points8.SetPoint(33,-6.6,14.6, 0.1)
lines8.InsertCellPoint(33);
points8.SetPoint(34,-6.8,14.6333, 0.1)
lines8.InsertCellPoint(34);
points8.SetPoint(35,-7,14.6667, 0.1)
lines8.InsertCellPoint(35);
points8.SetPoint(36,-7.2,14.7, 0.1)
lines8.InsertCellPoint(36);
points8.SetPoint(37,-7.4,14.7333, 0.1)
lines8.InsertCellPoint(37);
points8.SetPoint(38,-7.6,14.7667, 0.1)
lines8.InsertCellPoint(38);
points8.SetPoint(39,-7.8,14.8, 0.1)
lines8.InsertCellPoint(39);
points8.SetPoint(40,-8,14.8333, 0.1)
lines8.InsertCellPoint(40);
points8.SetPoint(41,-8.2,14.8667, 0.1)
lines8.InsertCellPoint(41);
points8.SetPoint(42,-8.4,14.9, 0.1)
lines8.InsertCellPoint(42);
points8.SetPoint(43,-8.6,14.9333, 0.1)
lines8.InsertCellPoint(43);
points8.SetPoint(44,-8.8,14.9667, 0.1)
lines8.InsertCellPoint(44);
points8.SetPoint(45,-9,15, 0.1)
lines8.InsertCellPoint(45);
points8.SetPoint(46,-9,15, 0.1)
lines8.InsertCellPoint(46);
points8.SetPoint(47,-8.96,14.8, 0.1)
lines8.InsertCellPoint(47);
points8.SetPoint(48,-8.92,14.6, 0.1)
lines8.InsertCellPoint(48);
points8.SetPoint(49,-8.88,14.4, 0.1)
lines8.InsertCellPoint(49);
points8.SetPoint(50,-8.84,14.2, 0.1)
lines8.InsertCellPoint(50);
points8.SetPoint(51,-8.8,14, 0.1)
lines8.InsertCellPoint(51);
points8.SetPoint(52,-8.76,13.8, 0.1)
lines8.InsertCellPoint(52);
points8.SetPoint(53,-8.72,13.6, 0.1)
lines8.InsertCellPoint(53);
points8.SetPoint(54,-8.68,13.4, 0.1)
lines8.InsertCellPoint(54);
points8.SetPoint(55,-8.64,13.2, 0.1)
lines8.InsertCellPoint(55);
points8.SetPoint(56,-8.6,13, 0.1)
lines8.InsertCellPoint(56);
points8.SetPoint(57,-8.56,12.8, 0.1)
lines8.InsertCellPoint(57);
points8.SetPoint(58,-8.52,12.6, 0.1)
lines8.InsertCellPoint(58);
points8.SetPoint(59,-8.48,12.4, 0.1)
lines8.InsertCellPoint(59);
points8.SetPoint(60,-8.44,12.2, 0.1)
lines8.InsertCellPoint(60);
points8.SetPoint(61,-8.4,12, 0.1)
lines8.InsertCellPoint(61);
points8.SetPoint(62,-8.36,11.8, 0.1)
lines8.InsertCellPoint(62);
points8.SetPoint(63,-8.32,11.6, 0.1)
lines8.InsertCellPoint(63);
points8.SetPoint(64,-8.28,11.4, 0.1)
lines8.InsertCellPoint(64);
points8.SetPoint(65,-8.24,11.2, 0.1)
lines8.InsertCellPoint(65);
points8.SetPoint(66,-8.2,11, 0.1)
lines8.InsertCellPoint(66);
points8.SetPoint(67,-8.16,10.8, 0.1)
lines8.InsertCellPoint(67);
points8.SetPoint(68,-8.12,10.6, 0.1)
lines8.InsertCellPoint(68);
points8.SetPoint(69,-8.08,10.4, 0.1)
lines8.InsertCellPoint(69);
points8.SetPoint(70,-8.04,10.2, 0.1)
lines8.InsertCellPoint(70);
points8.SetPoint(71,-8,10, 0.1)
lines8.InsertCellPoint(71);
points8.SetPoint(72,-7.96,9.8, 0.1)
lines8.InsertCellPoint(72);
points8.SetPoint(73,-7.92,9.6, 0.1)
lines8.InsertCellPoint(73);
points8.SetPoint(74,-7.88,9.4, 0.1)
lines8.InsertCellPoint(74);
points8.SetPoint(75,-7.84,9.2, 0.1)
lines8.InsertCellPoint(75);
points8.SetPoint(76,-7.8,9, 0.1)
lines8.InsertCellPoint(76);
points8.SetPoint(77,-7.76,8.8, 0.1)
lines8.InsertCellPoint(77);
points8.SetPoint(78,-7.72,8.6, 0.1)
lines8.InsertCellPoint(78);
points8.SetPoint(79,-7.68,8.4, 0.1)
lines8.InsertCellPoint(79);
points8.SetPoint(80,-7.64,8.2, 0.1)
lines8.InsertCellPoint(80);
points8.SetPoint(81,-7.6,8, 0.1)
lines8.InsertCellPoint(81);
points8.SetPoint(82,-7.56,7.8, 0.1)
lines8.InsertCellPoint(82);
points8.SetPoint(83,-7.52,7.6, 0.1)
lines8.InsertCellPoint(83);
points8.SetPoint(84,-7.48,7.4, 0.1)
lines8.InsertCellPoint(84);
points8.SetPoint(85,-7.44,7.2, 0.1)
lines8.InsertCellPoint(85);
points8.SetPoint(86,-7.4,7, 0.1)
lines8.InsertCellPoint(86);
points8.SetPoint(87,-7.36,6.8, 0.1)
lines8.InsertCellPoint(87);
points8.SetPoint(88,-7.32,6.6, 0.1)
lines8.InsertCellPoint(88);
points8.SetPoint(89,-7.28,6.4, 0.1)
lines8.InsertCellPoint(89);
points8.SetPoint(90,-7.24,6.2, 0.1)
lines8.InsertCellPoint(90);
points8.SetPoint(91,-7.2,6, 0.1)
lines8.InsertCellPoint(91);
points8.SetPoint(92,-7.16,5.8, 0.1)
lines8.InsertCellPoint(92);
points8.SetPoint(93,-7.12,5.6, 0.1)
lines8.InsertCellPoint(93);
points8.SetPoint(94,-7.08,5.4, 0.1)
lines8.InsertCellPoint(94);
points8.SetPoint(95,-7.04,5.2, 0.1)
lines8.InsertCellPoint(95);
points8.SetPoint(96,-7,5, 0.1)
lines8.InsertCellPoint(96);
points8.SetPoint(97,-7,5, 0.1)
lines8.InsertCellPoint(97);
points8.SetPoint(98,-6.98148,4.7963, 0.1)
lines8.InsertCellPoint(98);
points8.SetPoint(99,-6.96296,4.59259, 0.1)
lines8.InsertCellPoint(99);
points8.SetPoint(100,-6.94444,4.38889, 0.1)
lines8.InsertCellPoint(100);
points8.SetPoint(101,-6.92593,4.18519, 0.1)
lines8.InsertCellPoint(101);
points8.SetPoint(102,-6.90741,3.98148, 0.1)
lines8.InsertCellPoint(102);
points8.SetPoint(103,-6.88889,3.77778, 0.1)
lines8.InsertCellPoint(103);
points8.SetPoint(104,-6.87037,3.57407, 0.1)
lines8.InsertCellPoint(104);
points8.SetPoint(105,-6.85185,3.37037, 0.1)
lines8.InsertCellPoint(105);
points8.SetPoint(106,-6.83333,3.16667, 0.1)
lines8.InsertCellPoint(106);
points8.SetPoint(107,-6.81481,2.96296, 0.1)
lines8.InsertCellPoint(107);
points8.SetPoint(108,-6.7963,2.75926, 0.1)
lines8.InsertCellPoint(108);
points8.SetPoint(109,-6.77778,2.55556, 0.1)
lines8.InsertCellPoint(109);
points8.SetPoint(110,-6.75926,2.35185, 0.1)
lines8.InsertCellPoint(110);
points8.SetPoint(111,-6.74074,2.14815, 0.1)
lines8.InsertCellPoint(111);
points8.SetPoint(112,-6.72222,1.94444, 0.1)
lines8.InsertCellPoint(112);
points8.SetPoint(113,-6.7037,1.74074, 0.1)
lines8.InsertCellPoint(113);
points8.SetPoint(114,-6.68519,1.53704, 0.1)
lines8.InsertCellPoint(114);
points8.SetPoint(115,-6.66667,1.33333, 0.1)
lines8.InsertCellPoint(115);
points8.SetPoint(116,-6.64815,1.12963, 0.1)
lines8.InsertCellPoint(116);
points8.SetPoint(117,-6.62963,0.925926, 0.1)
lines8.InsertCellPoint(117);
points8.SetPoint(118,-6.61111,0.722222, 0.1)
lines8.InsertCellPoint(118);
points8.SetPoint(119,-6.59259,0.518519, 0.1)
lines8.InsertCellPoint(119);
points8.SetPoint(120,-6.57407,0.314815, 0.1)
lines8.InsertCellPoint(120);
points8.SetPoint(121,-6.55556,0.111111, 0.1)
lines8.InsertCellPoint(121);
points8.SetPoint(122,-6.53704,-0.0925926, 0.1)
lines8.InsertCellPoint(122);
points8.SetPoint(123,-6.51852,-0.296296, 0.1)
lines8.InsertCellPoint(123);
points8.SetPoint(124,-6.5,-0.5, 0.1)
lines8.InsertCellPoint(124);
points8.SetPoint(125,-6.5,-0.5, 0.1)
lines8.InsertCellPoint(125);
points8.SetPoint(126,-6.55357,-0.696429, 0.1)
lines8.InsertCellPoint(126);
points8.SetPoint(127,-6.60714,-0.892857, 0.1)
lines8.InsertCellPoint(127);
points8.SetPoint(128,-6.66071,-1.08929, 0.1)
lines8.InsertCellPoint(128);
points8.SetPoint(129,-6.71429,-1.28571, 0.1)
lines8.InsertCellPoint(129);
points8.SetPoint(130,-6.76786,-1.48214, 0.1)
lines8.InsertCellPoint(130);
points8.SetPoint(131,-6.82143,-1.67857, 0.1)
lines8.InsertCellPoint(131);
points8.SetPoint(132,-6.875,-1.875, 0.1)
lines8.InsertCellPoint(132);
points8.SetPoint(133,-6.92857,-2.07143, 0.1)
lines8.InsertCellPoint(133);
points8.SetPoint(134,-6.98214,-2.26786, 0.1)
lines8.InsertCellPoint(134);
points8.SetPoint(135,-7.03571,-2.46429, 0.1)
lines8.InsertCellPoint(135);
points8.SetPoint(136,-7.08929,-2.66071, 0.1)
lines8.InsertCellPoint(136);
points8.SetPoint(137,-7.14286,-2.85714, 0.1)
lines8.InsertCellPoint(137);
points8.SetPoint(138,-7.19643,-3.05357, 0.1)
lines8.InsertCellPoint(138);
points8.SetPoint(139,-7.25,-3.25, 0.1)
lines8.InsertCellPoint(139);
points8.SetPoint(140,-7.30357,-3.44643, 0.1)
lines8.InsertCellPoint(140);
points8.SetPoint(141,-7.35714,-3.64286, 0.1)
lines8.InsertCellPoint(141);
points8.SetPoint(142,-7.41071,-3.83929, 0.1)
lines8.InsertCellPoint(142);
points8.SetPoint(143,-7.46429,-4.03571, 0.1)
lines8.InsertCellPoint(143);
points8.SetPoint(144,-7.51786,-4.23214, 0.1)
lines8.InsertCellPoint(144);
points8.SetPoint(145,-7.57143,-4.42857, 0.1)
lines8.InsertCellPoint(145);
points8.SetPoint(146,-7.625,-4.625, 0.1)
lines8.InsertCellPoint(146);
points8.SetPoint(147,-7.67857,-4.82143, 0.1)
lines8.InsertCellPoint(147);
points8.SetPoint(148,-7.73214,-5.01786, 0.1)
lines8.InsertCellPoint(148);
points8.SetPoint(149,-7.78571,-5.21429, 0.1)
lines8.InsertCellPoint(149);
points8.SetPoint(150,-7.83929,-5.41071, 0.1)
lines8.InsertCellPoint(150);
points8.SetPoint(151,-7.89286,-5.60714, 0.1)
lines8.InsertCellPoint(151);
points8.SetPoint(152,-7.94643,-5.80357, 0.1)
lines8.InsertCellPoint(152);
points8.SetPoint(153,-8,-6, 0.1)
lines8.InsertCellPoint(153);
points8.SetPoint(154,-8,-6, 0.1)
lines8.InsertCellPoint(154);
points8.SetPoint(155,-7.8,-5.93333, 0.1)
lines8.InsertCellPoint(155);
points8.SetPoint(156,-7.6,-5.86667, 0.1)
lines8.InsertCellPoint(156);
points8.SetPoint(157,-7.4,-5.8, 0.1)
lines8.InsertCellPoint(157);
points8.SetPoint(158,-7.2,-5.73333, 0.1)
lines8.InsertCellPoint(158);
points8.SetPoint(159,-7,-5.66667, 0.1)
lines8.InsertCellPoint(159);
points8.SetPoint(160,-6.8,-5.6, 0.1)
lines8.InsertCellPoint(160);
points8.SetPoint(161,-6.6,-5.53333, 0.1)
lines8.InsertCellPoint(161);
points8.SetPoint(162,-6.4,-5.46667, 0.1)
lines8.InsertCellPoint(162);
points8.SetPoint(163,-6.2,-5.4, 0.1)
lines8.InsertCellPoint(163);
points8.SetPoint(164,-6,-5.33333, 0.1)
lines8.InsertCellPoint(164);
points8.SetPoint(165,-5.8,-5.26667, 0.1)
lines8.InsertCellPoint(165);
points8.SetPoint(166,-5.6,-5.2, 0.1)
lines8.InsertCellPoint(166);
points8.SetPoint(167,-5.4,-5.13333, 0.1)
lines8.InsertCellPoint(167);
points8.SetPoint(168,-5.2,-5.06667, 0.1)
lines8.InsertCellPoint(168);
points8.SetPoint(169,-5,-5, 0.1)
lines8.InsertCellPoint(169);
points8.SetPoint(170,-5,-5, 0.1)
lines8.InsertCellPoint(170);
points8.SetPoint(171,-5,-4.8, 0.1)
lines8.InsertCellPoint(171);
points8.SetPoint(172,-5,-4.6, 0.1)
lines8.InsertCellPoint(172);
points8.SetPoint(173,-5,-4.4, 0.1)
lines8.InsertCellPoint(173);
points8.SetPoint(174,-5,-4.2, 0.1)
lines8.InsertCellPoint(174);
points8.SetPoint(175,-5,-4, 0.1)
lines8.InsertCellPoint(175);
points8.SetPoint(176,-5,-3.8, 0.1)
lines8.InsertCellPoint(176);
points8.SetPoint(177,-5,-3.6, 0.1)
lines8.InsertCellPoint(177);
points8.SetPoint(178,-5,-3.4, 0.1)
lines8.InsertCellPoint(178);
points8.SetPoint(179,-5,-3.2, 0.1)
lines8.InsertCellPoint(179);
points8.SetPoint(180,-5,-3, 0.1)
lines8.InsertCellPoint(180);
points8.SetPoint(181,-5,-2.8, 0.1)
lines8.InsertCellPoint(181);
points8.SetPoint(182,-5,-2.6, 0.1)
lines8.InsertCellPoint(182);
points8.SetPoint(183,-5,-2.4, 0.1)
lines8.InsertCellPoint(183);
points8.SetPoint(184,-5,-2.2, 0.1)
lines8.InsertCellPoint(184);
points8.SetPoint(185,-5,-2, 0.1)
lines8.InsertCellPoint(185);
points8.SetPoint(186,-5,-1.8, 0.1)
lines8.InsertCellPoint(186);
points8.SetPoint(187,-5,-1.6, 0.1)
lines8.InsertCellPoint(187);
points8.SetPoint(188,-5,-1.4, 0.1)
lines8.InsertCellPoint(188);
points8.SetPoint(189,-5,-1.2, 0.1)
lines8.InsertCellPoint(189);
points8.SetPoint(190,-5,-1, 0.1)
lines8.InsertCellPoint(190);
points8.SetPoint(191,-5,-0.8, 0.1)
lines8.InsertCellPoint(191);
points8.SetPoint(192,-5,-0.6, 0.1)
lines8.InsertCellPoint(192);
points8.SetPoint(193,-5,-0.4, 0.1)
lines8.InsertCellPoint(193);
points8.SetPoint(194,-5,-0.2, 0.1)
lines8.InsertCellPoint(194);
points8.SetPoint(195,-5,0, 0.1)
lines8.InsertCellPoint(195);
points8.SetPoint(196,-5,0, 0.1)
lines8.InsertCellPoint(196);
points8.SetPoint(197,-5,0.2, 0.1)
lines8.InsertCellPoint(197);
points8.SetPoint(198,-5,0.4, 0.1)
lines8.InsertCellPoint(198);
points8.SetPoint(199,-5,0.6, 0.1)
lines8.InsertCellPoint(199);
points8.SetPoint(200,-5,0.8, 0.1)
lines8.InsertCellPoint(200);
points8.SetPoint(201,-5,1, 0.1)
lines8.InsertCellPoint(201);
points8.SetPoint(202,-5,1.2, 0.1)
lines8.InsertCellPoint(202);
points8.SetPoint(203,-5,1.4, 0.1)
lines8.InsertCellPoint(203);
points8.SetPoint(204,-5,1.6, 0.1)
lines8.InsertCellPoint(204);
points8.SetPoint(205,-5,1.8, 0.1)
lines8.InsertCellPoint(205);
points8.SetPoint(206,-5,2, 0.1)
lines8.InsertCellPoint(206);
points8.SetPoint(207,-5,2.2, 0.1)
lines8.InsertCellPoint(207);
points8.SetPoint(208,-5,2.4, 0.1)
lines8.InsertCellPoint(208);
points8.SetPoint(209,-5,2.6, 0.1)
lines8.InsertCellPoint(209);
points8.SetPoint(210,-5,2.8, 0.1)
lines8.InsertCellPoint(210);
points8.SetPoint(211,-5,3, 0.1)
lines8.InsertCellPoint(211);
points8.SetPoint(212,-5,3.2, 0.1)
lines8.InsertCellPoint(212);
points8.SetPoint(213,-5,3.4, 0.1)
lines8.InsertCellPoint(213);
points8.SetPoint(214,-5,3.6, 0.1)
lines8.InsertCellPoint(214);
points8.SetPoint(215,-5,3.8, 0.1)
lines8.InsertCellPoint(215);
points8.SetPoint(216,-5,4, 0.1)
lines8.InsertCellPoint(216);
points8.SetPoint(217,-5,4.2, 0.1)
lines8.InsertCellPoint(217);
points8.SetPoint(218,-5,4.4, 0.1)
lines8.InsertCellPoint(218);
points8.SetPoint(219,-5,4.6, 0.1)
lines8.InsertCellPoint(219);
points8.SetPoint(220,-5,4.8, 0.1)
lines8.InsertCellPoint(220);
points8.SetPoint(221,-5,5, 0.1)
lines8.InsertCellPoint(221);
points8.SetPoint(222,-5,5, 0.1)
lines8.InsertCellPoint(222);
points8.SetPoint(223,-5,5.2, 0.1)
lines8.InsertCellPoint(223);
points8.SetPoint(224,-5,5.4, 0.1)
lines8.InsertCellPoint(224);
points8.SetPoint(225,-5,5.6, 0.1)
lines8.InsertCellPoint(225);
points8.SetPoint(226,-5,5.8, 0.1)
lines8.InsertCellPoint(226);
points8.SetPoint(227,-5,6, 0.1)
lines8.InsertCellPoint(227);
points8.SetPoint(228,-5,6.2, 0.1)
lines8.InsertCellPoint(228);
points8.SetPoint(229,-5,6.4, 0.1)
lines8.InsertCellPoint(229);
points8.SetPoint(230,-5,6.6, 0.1)
lines8.InsertCellPoint(230);
points8.SetPoint(231,-5,6.8, 0.1)
lines8.InsertCellPoint(231);
points8.SetPoint(232,-5,7, 0.1)
lines8.InsertCellPoint(232);
points8.SetPoint(233,-5,7.2, 0.1)
lines8.InsertCellPoint(233);
points8.SetPoint(234,-5,7.4, 0.1)
lines8.InsertCellPoint(234);
points8.SetPoint(235,-5,7.6, 0.1)
lines8.InsertCellPoint(235);
points8.SetPoint(236,-5,7.8, 0.1)
lines8.InsertCellPoint(236);
points8.SetPoint(237,-5,8, 0.1)
lines8.InsertCellPoint(237);
points8.SetPoint(238,-5,8.2, 0.1)
lines8.InsertCellPoint(238);
points8.SetPoint(239,-5,8.4, 0.1)
lines8.InsertCellPoint(239);
points8.SetPoint(240,-5,8.6, 0.1)
lines8.InsertCellPoint(240);
points8.SetPoint(241,-5,8.8, 0.1)
lines8.InsertCellPoint(241);
points8.SetPoint(242,-5,9, 0.1)
lines8.InsertCellPoint(242);
points8.SetPoint(243,-5,9, 0.1)
lines8.InsertCellPoint(243);
points8.SetPoint(244,-4.84848,9.13636, 0.1)
lines8.InsertCellPoint(244);
points8.SetPoint(245,-4.69697,9.27273, 0.1)
lines8.InsertCellPoint(245);
points8.SetPoint(246,-4.54545,9.40909, 0.1)
lines8.InsertCellPoint(246);
points8.SetPoint(247,-4.39394,9.54545, 0.1)
lines8.InsertCellPoint(247);
points8.SetPoint(248,-4.24242,9.68182, 0.1)
lines8.InsertCellPoint(248);
points8.SetPoint(249,-4.09091,9.81818, 0.1)
lines8.InsertCellPoint(249);
points8.SetPoint(250,-3.93939,9.95455, 0.1)
lines8.InsertCellPoint(250);
points8.SetPoint(251,-3.78788,10.0909, 0.1)
lines8.InsertCellPoint(251);
points8.SetPoint(252,-3.63636,10.2273, 0.1)
lines8.InsertCellPoint(252);
points8.SetPoint(253,-3.48485,10.3636, 0.1)
lines8.InsertCellPoint(253);
points8.SetPoint(254,-3.33333,10.5, 0.1)
lines8.InsertCellPoint(254);
points8.SetPoint(255,-3.18182,10.6364, 0.1)
lines8.InsertCellPoint(255);
points8.SetPoint(256,-3.0303,10.7727, 0.1)
lines8.InsertCellPoint(256);
points8.SetPoint(257,-2.87879,10.9091, 0.1)
lines8.InsertCellPoint(257);
points8.SetPoint(258,-2.72727,11.0455, 0.1)
lines8.InsertCellPoint(258);
points8.SetPoint(259,-2.57576,11.1818, 0.1)
lines8.InsertCellPoint(259);
points8.SetPoint(260,-2.42424,11.3182, 0.1)
lines8.InsertCellPoint(260);
points8.SetPoint(261,-2.27273,11.4545, 0.1)
lines8.InsertCellPoint(261);
points8.SetPoint(262,-2.12121,11.5909, 0.1)
lines8.InsertCellPoint(262);
points8.SetPoint(263,-1.9697,11.7273, 0.1)
lines8.InsertCellPoint(263);
points8.SetPoint(264,-1.81818,11.8636, 0.1)
lines8.InsertCellPoint(264);
points8.SetPoint(265,-1.66667,12, 0.1)
lines8.InsertCellPoint(265);
points8.SetPoint(266,-1.51515,12.1364, 0.1)
lines8.InsertCellPoint(266);
points8.SetPoint(267,-1.36364,12.2727, 0.1)
lines8.InsertCellPoint(267);
points8.SetPoint(268,-1.21212,12.4091, 0.1)
lines8.InsertCellPoint(268);
points8.SetPoint(269,-1.06061,12.5455, 0.1)
lines8.InsertCellPoint(269);
points8.SetPoint(270,-0.909091,12.6818, 0.1)
lines8.InsertCellPoint(270);
points8.SetPoint(271,-0.757576,12.8182, 0.1)
lines8.InsertCellPoint(271);
points8.SetPoint(272,-0.606061,12.9545, 0.1)
lines8.InsertCellPoint(272);
points8.SetPoint(273,-0.454545,13.0909, 0.1)
lines8.InsertCellPoint(273);
points8.SetPoint(274,-0.30303,13.2273, 0.1)
lines8.InsertCellPoint(274);
points8.SetPoint(275,-0.151515,13.3636, 0.1)
lines8.InsertCellPoint(275);
points8.SetPoint(276,0,13.5, 0.1)
lines8.InsertCellPoint(276);
lines8.InsertCellPoint(0);
polygon8 = vtk.vtkPolyData()
polygon8.SetPoints(points8)
polygon8.SetLines(lines8)
polygonMapper8 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper8.SetInputConnection(polygon8.GetProducerPort())
else:
	polygonMapper8.SetInputData(polygon8)
	polygonMapper8.Update()
polygonActor8 = vtk.vtkActor()
polygonActor8.SetMapper(polygonMapper8)
polygonActor8.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor8.GetProperty().SetLineWidth(widthLine)
points9 = vtk.vtkPoints()
points9.SetNumberOfPoints(119)
lines9 = vtk.vtkCellArray()
lines9.InsertNextCell(120)
points9.SetPoint(0,24,0, 0.1)
lines9.InsertCellPoint(0);
points9.SetPoint(1,24.2,0, 0.1)
lines9.InsertCellPoint(1);
points9.SetPoint(2,24.4,0, 0.1)
lines9.InsertCellPoint(2);
points9.SetPoint(3,24.6,0, 0.1)
lines9.InsertCellPoint(3);
points9.SetPoint(4,24.8,0, 0.1)
lines9.InsertCellPoint(4);
points9.SetPoint(5,25,0, 0.1)
lines9.InsertCellPoint(5);
points9.SetPoint(6,25.2,0, 0.1)
lines9.InsertCellPoint(6);
points9.SetPoint(7,25.4,0, 0.1)
lines9.InsertCellPoint(7);
points9.SetPoint(8,25.6,0, 0.1)
lines9.InsertCellPoint(8);
points9.SetPoint(9,25.8,0, 0.1)
lines9.InsertCellPoint(9);
points9.SetPoint(10,26,0, 0.1)
lines9.InsertCellPoint(10);
points9.SetPoint(11,26.2,0, 0.1)
lines9.InsertCellPoint(11);
points9.SetPoint(12,26.4,0, 0.1)
lines9.InsertCellPoint(12);
points9.SetPoint(13,26.6,0, 0.1)
lines9.InsertCellPoint(13);
points9.SetPoint(14,26.8,0, 0.1)
lines9.InsertCellPoint(14);
points9.SetPoint(15,27,0, 0.1)
lines9.InsertCellPoint(15);
points9.SetPoint(16,27.2,0, 0.1)
lines9.InsertCellPoint(16);
points9.SetPoint(17,27.4,0, 0.1)
lines9.InsertCellPoint(17);
points9.SetPoint(18,27.6,0, 0.1)
lines9.InsertCellPoint(18);
points9.SetPoint(19,27.8,0, 0.1)
lines9.InsertCellPoint(19);
points9.SetPoint(20,28,0, 0.1)
lines9.InsertCellPoint(20);
points9.SetPoint(21,28.2,0, 0.1)
lines9.InsertCellPoint(21);
points9.SetPoint(22,28.4,0, 0.1)
lines9.InsertCellPoint(22);
points9.SetPoint(23,28.6,0, 0.1)
lines9.InsertCellPoint(23);
points9.SetPoint(24,28.8,0, 0.1)
lines9.InsertCellPoint(24);
points9.SetPoint(25,29,0, 0.1)
lines9.InsertCellPoint(25);
points9.SetPoint(26,29,0, 0.1)
lines9.InsertCellPoint(26);
points9.SetPoint(27,29,0.2, 0.1)
lines9.InsertCellPoint(27);
points9.SetPoint(28,29,0.4, 0.1)
lines9.InsertCellPoint(28);
points9.SetPoint(29,29,0.6, 0.1)
lines9.InsertCellPoint(29);
points9.SetPoint(30,29,0.8, 0.1)
lines9.InsertCellPoint(30);
points9.SetPoint(31,29,1, 0.1)
lines9.InsertCellPoint(31);
points9.SetPoint(32,29,1.2, 0.1)
lines9.InsertCellPoint(32);
points9.SetPoint(33,29,1.4, 0.1)
lines9.InsertCellPoint(33);
points9.SetPoint(34,29,1.6, 0.1)
lines9.InsertCellPoint(34);
points9.SetPoint(35,29,1.8, 0.1)
lines9.InsertCellPoint(35);
points9.SetPoint(36,29,2, 0.1)
lines9.InsertCellPoint(36);
points9.SetPoint(37,29,2.2, 0.1)
lines9.InsertCellPoint(37);
points9.SetPoint(38,29,2.4, 0.1)
lines9.InsertCellPoint(38);
points9.SetPoint(39,29,2.6, 0.1)
lines9.InsertCellPoint(39);
points9.SetPoint(40,29,2.8, 0.1)
lines9.InsertCellPoint(40);
points9.SetPoint(41,29,3, 0.1)
lines9.InsertCellPoint(41);
points9.SetPoint(42,29,3.2, 0.1)
lines9.InsertCellPoint(42);
points9.SetPoint(43,29,3.4, 0.1)
lines9.InsertCellPoint(43);
points9.SetPoint(44,29,3.6, 0.1)
lines9.InsertCellPoint(44);
points9.SetPoint(45,29,3.8, 0.1)
lines9.InsertCellPoint(45);
points9.SetPoint(46,29,4, 0.1)
lines9.InsertCellPoint(46);
points9.SetPoint(47,29,4.2, 0.1)
lines9.InsertCellPoint(47);
points9.SetPoint(48,29,4.4, 0.1)
lines9.InsertCellPoint(48);
points9.SetPoint(49,29,4.6, 0.1)
lines9.InsertCellPoint(49);
points9.SetPoint(50,29,4.8, 0.1)
lines9.InsertCellPoint(50);
points9.SetPoint(51,29,5, 0.1)
lines9.InsertCellPoint(51);
points9.SetPoint(52,29,5, 0.1)
lines9.InsertCellPoint(52);
points9.SetPoint(53,28.8148,5.09259, 0.1)
lines9.InsertCellPoint(53);
points9.SetPoint(54,28.6296,5.18519, 0.1)
lines9.InsertCellPoint(54);
points9.SetPoint(55,28.4444,5.27778, 0.1)
lines9.InsertCellPoint(55);
points9.SetPoint(56,28.2593,5.37037, 0.1)
lines9.InsertCellPoint(56);
points9.SetPoint(57,28.0741,5.46296, 0.1)
lines9.InsertCellPoint(57);
points9.SetPoint(58,27.8889,5.55556, 0.1)
lines9.InsertCellPoint(58);
points9.SetPoint(59,27.7037,5.64815, 0.1)
lines9.InsertCellPoint(59);
points9.SetPoint(60,27.5185,5.74074, 0.1)
lines9.InsertCellPoint(60);
points9.SetPoint(61,27.3333,5.83333, 0.1)
lines9.InsertCellPoint(61);
points9.SetPoint(62,27.1481,5.92593, 0.1)
lines9.InsertCellPoint(62);
points9.SetPoint(63,26.963,6.01852, 0.1)
lines9.InsertCellPoint(63);
points9.SetPoint(64,26.7778,6.11111, 0.1)
lines9.InsertCellPoint(64);
points9.SetPoint(65,26.5926,6.2037, 0.1)
lines9.InsertCellPoint(65);
points9.SetPoint(66,26.4074,6.2963, 0.1)
lines9.InsertCellPoint(66);
points9.SetPoint(67,26.2222,6.38889, 0.1)
lines9.InsertCellPoint(67);
points9.SetPoint(68,26.037,6.48148, 0.1)
lines9.InsertCellPoint(68);
points9.SetPoint(69,25.8519,6.57407, 0.1)
lines9.InsertCellPoint(69);
points9.SetPoint(70,25.6667,6.66667, 0.1)
lines9.InsertCellPoint(70);
points9.SetPoint(71,25.4815,6.75926, 0.1)
lines9.InsertCellPoint(71);
points9.SetPoint(72,25.2963,6.85185, 0.1)
lines9.InsertCellPoint(72);
points9.SetPoint(73,25.1111,6.94444, 0.1)
lines9.InsertCellPoint(73);
points9.SetPoint(74,24.9259,7.03704, 0.1)
lines9.InsertCellPoint(74);
points9.SetPoint(75,24.7407,7.12963, 0.1)
lines9.InsertCellPoint(75);
points9.SetPoint(76,24.5556,7.22222, 0.1)
lines9.InsertCellPoint(76);
points9.SetPoint(77,24.3704,7.31481, 0.1)
lines9.InsertCellPoint(77);
points9.SetPoint(78,24.1852,7.40741, 0.1)
lines9.InsertCellPoint(78);
points9.SetPoint(79,24,7.5, 0.1)
lines9.InsertCellPoint(79);
points9.SetPoint(80,24,7.5, 0.1)
lines9.InsertCellPoint(80);
points9.SetPoint(81,24,7.29167, 0.1)
lines9.InsertCellPoint(81);
points9.SetPoint(82,24,7.08333, 0.1)
lines9.InsertCellPoint(82);
points9.SetPoint(83,24,6.875, 0.1)
lines9.InsertCellPoint(83);
points9.SetPoint(84,24,6.66667, 0.1)
lines9.InsertCellPoint(84);
points9.SetPoint(85,24,6.45833, 0.1)
lines9.InsertCellPoint(85);
points9.SetPoint(86,24,6.25, 0.1)
lines9.InsertCellPoint(86);
points9.SetPoint(87,24,6.04167, 0.1)
lines9.InsertCellPoint(87);
points9.SetPoint(88,24,5.83333, 0.1)
lines9.InsertCellPoint(88);
points9.SetPoint(89,24,5.625, 0.1)
lines9.InsertCellPoint(89);
points9.SetPoint(90,24,5.41667, 0.1)
lines9.InsertCellPoint(90);
points9.SetPoint(91,24,5.20833, 0.1)
lines9.InsertCellPoint(91);
points9.SetPoint(92,24,5, 0.1)
lines9.InsertCellPoint(92);
points9.SetPoint(93,24,5, 0.1)
lines9.InsertCellPoint(93);
points9.SetPoint(94,24,4.8, 0.1)
lines9.InsertCellPoint(94);
points9.SetPoint(95,24,4.6, 0.1)
lines9.InsertCellPoint(95);
points9.SetPoint(96,24,4.4, 0.1)
lines9.InsertCellPoint(96);
points9.SetPoint(97,24,4.2, 0.1)
lines9.InsertCellPoint(97);
points9.SetPoint(98,24,4, 0.1)
lines9.InsertCellPoint(98);
points9.SetPoint(99,24,3.8, 0.1)
lines9.InsertCellPoint(99);
points9.SetPoint(100,24,3.6, 0.1)
lines9.InsertCellPoint(100);
points9.SetPoint(101,24,3.4, 0.1)
lines9.InsertCellPoint(101);
points9.SetPoint(102,24,3.2, 0.1)
lines9.InsertCellPoint(102);
points9.SetPoint(103,24,3, 0.1)
lines9.InsertCellPoint(103);
points9.SetPoint(104,24,2.8, 0.1)
lines9.InsertCellPoint(104);
points9.SetPoint(105,24,2.6, 0.1)
lines9.InsertCellPoint(105);
points9.SetPoint(106,24,2.4, 0.1)
lines9.InsertCellPoint(106);
points9.SetPoint(107,24,2.2, 0.1)
lines9.InsertCellPoint(107);
points9.SetPoint(108,24,2, 0.1)
lines9.InsertCellPoint(108);
points9.SetPoint(109,24,1.8, 0.1)
lines9.InsertCellPoint(109);
points9.SetPoint(110,24,1.6, 0.1)
lines9.InsertCellPoint(110);
points9.SetPoint(111,24,1.4, 0.1)
lines9.InsertCellPoint(111);
points9.SetPoint(112,24,1.2, 0.1)
lines9.InsertCellPoint(112);
points9.SetPoint(113,24,1, 0.1)
lines9.InsertCellPoint(113);
points9.SetPoint(114,24,0.8, 0.1)
lines9.InsertCellPoint(114);
points9.SetPoint(115,24,0.6, 0.1)
lines9.InsertCellPoint(115);
points9.SetPoint(116,24,0.4, 0.1)
lines9.InsertCellPoint(116);
points9.SetPoint(117,24,0.2, 0.1)
lines9.InsertCellPoint(117);
points9.SetPoint(118,24,0, 0.1)
lines9.InsertCellPoint(118);
lines9.InsertCellPoint(0);
polygon9 = vtk.vtkPolyData()
polygon9.SetPoints(points9)
polygon9.SetLines(lines9)
polygonMapper9 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper9.SetInputConnection(polygon9.GetProducerPort())
else:
	polygonMapper9.SetInputData(polygon9)
	polygonMapper9.Update()
polygonActor9 = vtk.vtkActor()
polygonActor9.SetMapper(polygonMapper9)
polygonActor9.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor9.GetProperty().SetLineWidth(widthLine)
points10 = vtk.vtkPoints()
points10.SetNumberOfPoints(104)
lines10 = vtk.vtkCellArray()
lines10.InsertNextCell(105)
points10.SetPoint(0,24,5, 0.1)
lines10.InsertCellPoint(0);
points10.SetPoint(1,23.8,5, 0.1)
lines10.InsertCellPoint(1);
points10.SetPoint(2,23.6,5, 0.1)
lines10.InsertCellPoint(2);
points10.SetPoint(3,23.4,5, 0.1)
lines10.InsertCellPoint(3);
points10.SetPoint(4,23.2,5, 0.1)
lines10.InsertCellPoint(4);
points10.SetPoint(5,23,5, 0.1)
lines10.InsertCellPoint(5);
points10.SetPoint(6,22.8,5, 0.1)
lines10.InsertCellPoint(6);
points10.SetPoint(7,22.6,5, 0.1)
lines10.InsertCellPoint(7);
points10.SetPoint(8,22.4,5, 0.1)
lines10.InsertCellPoint(8);
points10.SetPoint(9,22.2,5, 0.1)
lines10.InsertCellPoint(9);
points10.SetPoint(10,22,5, 0.1)
lines10.InsertCellPoint(10);
points10.SetPoint(11,21.8,5, 0.1)
lines10.InsertCellPoint(11);
points10.SetPoint(12,21.6,5, 0.1)
lines10.InsertCellPoint(12);
points10.SetPoint(13,21.4,5, 0.1)
lines10.InsertCellPoint(13);
points10.SetPoint(14,21.2,5, 0.1)
lines10.InsertCellPoint(14);
points10.SetPoint(15,21,5, 0.1)
lines10.InsertCellPoint(15);
points10.SetPoint(16,20.8,5, 0.1)
lines10.InsertCellPoint(16);
points10.SetPoint(17,20.6,5, 0.1)
lines10.InsertCellPoint(17);
points10.SetPoint(18,20.4,5, 0.1)
lines10.InsertCellPoint(18);
points10.SetPoint(19,20.2,5, 0.1)
lines10.InsertCellPoint(19);
points10.SetPoint(20,20,5, 0.1)
lines10.InsertCellPoint(20);
points10.SetPoint(21,19.8,5, 0.1)
lines10.InsertCellPoint(21);
points10.SetPoint(22,19.6,5, 0.1)
lines10.InsertCellPoint(22);
points10.SetPoint(23,19.4,5, 0.1)
lines10.InsertCellPoint(23);
points10.SetPoint(24,19.2,5, 0.1)
lines10.InsertCellPoint(24);
points10.SetPoint(25,19,5, 0.1)
lines10.InsertCellPoint(25);
points10.SetPoint(26,19,5, 0.1)
lines10.InsertCellPoint(26);
points10.SetPoint(27,19,4.8, 0.1)
lines10.InsertCellPoint(27);
points10.SetPoint(28,19,4.6, 0.1)
lines10.InsertCellPoint(28);
points10.SetPoint(29,19,4.4, 0.1)
lines10.InsertCellPoint(29);
points10.SetPoint(30,19,4.2, 0.1)
lines10.InsertCellPoint(30);
points10.SetPoint(31,19,4, 0.1)
lines10.InsertCellPoint(31);
points10.SetPoint(32,19,3.8, 0.1)
lines10.InsertCellPoint(32);
points10.SetPoint(33,19,3.6, 0.1)
lines10.InsertCellPoint(33);
points10.SetPoint(34,19,3.4, 0.1)
lines10.InsertCellPoint(34);
points10.SetPoint(35,19,3.2, 0.1)
lines10.InsertCellPoint(35);
points10.SetPoint(36,19,3, 0.1)
lines10.InsertCellPoint(36);
points10.SetPoint(37,19,2.8, 0.1)
lines10.InsertCellPoint(37);
points10.SetPoint(38,19,2.6, 0.1)
lines10.InsertCellPoint(38);
points10.SetPoint(39,19,2.4, 0.1)
lines10.InsertCellPoint(39);
points10.SetPoint(40,19,2.2, 0.1)
lines10.InsertCellPoint(40);
points10.SetPoint(41,19,2, 0.1)
lines10.InsertCellPoint(41);
points10.SetPoint(42,19,1.8, 0.1)
lines10.InsertCellPoint(42);
points10.SetPoint(43,19,1.6, 0.1)
lines10.InsertCellPoint(43);
points10.SetPoint(44,19,1.4, 0.1)
lines10.InsertCellPoint(44);
points10.SetPoint(45,19,1.2, 0.1)
lines10.InsertCellPoint(45);
points10.SetPoint(46,19,1, 0.1)
lines10.InsertCellPoint(46);
points10.SetPoint(47,19,0.8, 0.1)
lines10.InsertCellPoint(47);
points10.SetPoint(48,19,0.6, 0.1)
lines10.InsertCellPoint(48);
points10.SetPoint(49,19,0.4, 0.1)
lines10.InsertCellPoint(49);
points10.SetPoint(50,19,0.2, 0.1)
lines10.InsertCellPoint(50);
points10.SetPoint(51,19,0, 0.1)
lines10.InsertCellPoint(51);
points10.SetPoint(52,19,0, 0.1)
lines10.InsertCellPoint(52);
points10.SetPoint(53,19.2,0, 0.1)
lines10.InsertCellPoint(53);
points10.SetPoint(54,19.4,0, 0.1)
lines10.InsertCellPoint(54);
points10.SetPoint(55,19.6,0, 0.1)
lines10.InsertCellPoint(55);
points10.SetPoint(56,19.8,0, 0.1)
lines10.InsertCellPoint(56);
points10.SetPoint(57,20,0, 0.1)
lines10.InsertCellPoint(57);
points10.SetPoint(58,20.2,0, 0.1)
lines10.InsertCellPoint(58);
points10.SetPoint(59,20.4,0, 0.1)
lines10.InsertCellPoint(59);
points10.SetPoint(60,20.6,0, 0.1)
lines10.InsertCellPoint(60);
points10.SetPoint(61,20.8,0, 0.1)
lines10.InsertCellPoint(61);
points10.SetPoint(62,21,0, 0.1)
lines10.InsertCellPoint(62);
points10.SetPoint(63,21.2,0, 0.1)
lines10.InsertCellPoint(63);
points10.SetPoint(64,21.4,0, 0.1)
lines10.InsertCellPoint(64);
points10.SetPoint(65,21.6,0, 0.1)
lines10.InsertCellPoint(65);
points10.SetPoint(66,21.8,0, 0.1)
lines10.InsertCellPoint(66);
points10.SetPoint(67,22,0, 0.1)
lines10.InsertCellPoint(67);
points10.SetPoint(68,22.2,0, 0.1)
lines10.InsertCellPoint(68);
points10.SetPoint(69,22.4,0, 0.1)
lines10.InsertCellPoint(69);
points10.SetPoint(70,22.6,0, 0.1)
lines10.InsertCellPoint(70);
points10.SetPoint(71,22.8,0, 0.1)
lines10.InsertCellPoint(71);
points10.SetPoint(72,23,0, 0.1)
lines10.InsertCellPoint(72);
points10.SetPoint(73,23.2,0, 0.1)
lines10.InsertCellPoint(73);
points10.SetPoint(74,23.4,0, 0.1)
lines10.InsertCellPoint(74);
points10.SetPoint(75,23.6,0, 0.1)
lines10.InsertCellPoint(75);
points10.SetPoint(76,23.8,0, 0.1)
lines10.InsertCellPoint(76);
points10.SetPoint(77,24,0, 0.1)
lines10.InsertCellPoint(77);
points10.SetPoint(78,24,0, 0.1)
lines10.InsertCellPoint(78);
points10.SetPoint(79,24,0.2, 0.1)
lines10.InsertCellPoint(79);
points10.SetPoint(80,24,0.4, 0.1)
lines10.InsertCellPoint(80);
points10.SetPoint(81,24,0.6, 0.1)
lines10.InsertCellPoint(81);
points10.SetPoint(82,24,0.8, 0.1)
lines10.InsertCellPoint(82);
points10.SetPoint(83,24,1, 0.1)
lines10.InsertCellPoint(83);
points10.SetPoint(84,24,1.2, 0.1)
lines10.InsertCellPoint(84);
points10.SetPoint(85,24,1.4, 0.1)
lines10.InsertCellPoint(85);
points10.SetPoint(86,24,1.6, 0.1)
lines10.InsertCellPoint(86);
points10.SetPoint(87,24,1.8, 0.1)
lines10.InsertCellPoint(87);
points10.SetPoint(88,24,2, 0.1)
lines10.InsertCellPoint(88);
points10.SetPoint(89,24,2.2, 0.1)
lines10.InsertCellPoint(89);
points10.SetPoint(90,24,2.4, 0.1)
lines10.InsertCellPoint(90);
points10.SetPoint(91,24,2.6, 0.1)
lines10.InsertCellPoint(91);
points10.SetPoint(92,24,2.8, 0.1)
lines10.InsertCellPoint(92);
points10.SetPoint(93,24,3, 0.1)
lines10.InsertCellPoint(93);
points10.SetPoint(94,24,3.2, 0.1)
lines10.InsertCellPoint(94);
points10.SetPoint(95,24,3.4, 0.1)
lines10.InsertCellPoint(95);
points10.SetPoint(96,24,3.6, 0.1)
lines10.InsertCellPoint(96);
points10.SetPoint(97,24,3.8, 0.1)
lines10.InsertCellPoint(97);
points10.SetPoint(98,24,4, 0.1)
lines10.InsertCellPoint(98);
points10.SetPoint(99,24,4.2, 0.1)
lines10.InsertCellPoint(99);
points10.SetPoint(100,24,4.4, 0.1)
lines10.InsertCellPoint(100);
points10.SetPoint(101,24,4.6, 0.1)
lines10.InsertCellPoint(101);
points10.SetPoint(102,24,4.8, 0.1)
lines10.InsertCellPoint(102);
points10.SetPoint(103,24,5, 0.1)
lines10.InsertCellPoint(103);
lines10.InsertCellPoint(0);
polygon10 = vtk.vtkPolyData()
polygon10.SetPoints(points10)
polygon10.SetLines(lines10)
polygonMapper10 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper10.SetInputConnection(polygon10.GetProducerPort())
else:
	polygonMapper10.SetInputData(polygon10)
	polygonMapper10.Update()
polygonActor10 = vtk.vtkActor()
polygonActor10.SetMapper(polygonMapper10)
polygonActor10.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor10.GetProperty().SetLineWidth(widthLine)
points11 = vtk.vtkPoints()
points11.SetNumberOfPoints(154)
lines11 = vtk.vtkCellArray()
lines11.InsertNextCell(155)
points11.SetPoint(0,24,0, 0.1)
lines11.InsertCellPoint(0);
points11.SetPoint(1,23.8,0, 0.1)
lines11.InsertCellPoint(1);
points11.SetPoint(2,23.6,0, 0.1)
lines11.InsertCellPoint(2);
points11.SetPoint(3,23.4,0, 0.1)
lines11.InsertCellPoint(3);
points11.SetPoint(4,23.2,0, 0.1)
lines11.InsertCellPoint(4);
points11.SetPoint(5,23,0, 0.1)
lines11.InsertCellPoint(5);
points11.SetPoint(6,22.8,0, 0.1)
lines11.InsertCellPoint(6);
points11.SetPoint(7,22.6,0, 0.1)
lines11.InsertCellPoint(7);
points11.SetPoint(8,22.4,0, 0.1)
lines11.InsertCellPoint(8);
points11.SetPoint(9,22.2,0, 0.1)
lines11.InsertCellPoint(9);
points11.SetPoint(10,22,0, 0.1)
lines11.InsertCellPoint(10);
points11.SetPoint(11,21.8,0, 0.1)
lines11.InsertCellPoint(11);
points11.SetPoint(12,21.6,0, 0.1)
lines11.InsertCellPoint(12);
points11.SetPoint(13,21.4,0, 0.1)
lines11.InsertCellPoint(13);
points11.SetPoint(14,21.2,0, 0.1)
lines11.InsertCellPoint(14);
points11.SetPoint(15,21,0, 0.1)
lines11.InsertCellPoint(15);
points11.SetPoint(16,20.8,0, 0.1)
lines11.InsertCellPoint(16);
points11.SetPoint(17,20.6,0, 0.1)
lines11.InsertCellPoint(17);
points11.SetPoint(18,20.4,0, 0.1)
lines11.InsertCellPoint(18);
points11.SetPoint(19,20.2,0, 0.1)
lines11.InsertCellPoint(19);
points11.SetPoint(20,20,0, 0.1)
lines11.InsertCellPoint(20);
points11.SetPoint(21,19.8,0, 0.1)
lines11.InsertCellPoint(21);
points11.SetPoint(22,19.6,0, 0.1)
lines11.InsertCellPoint(22);
points11.SetPoint(23,19.4,0, 0.1)
lines11.InsertCellPoint(23);
points11.SetPoint(24,19.2,0, 0.1)
lines11.InsertCellPoint(24);
points11.SetPoint(25,19,0, 0.1)
lines11.InsertCellPoint(25);
points11.SetPoint(26,19,0, 0.1)
lines11.InsertCellPoint(26);
points11.SetPoint(27,19,-0.2, 0.1)
lines11.InsertCellPoint(27);
points11.SetPoint(28,19,-0.4, 0.1)
lines11.InsertCellPoint(28);
points11.SetPoint(29,19,-0.6, 0.1)
lines11.InsertCellPoint(29);
points11.SetPoint(30,19,-0.8, 0.1)
lines11.InsertCellPoint(30);
points11.SetPoint(31,19,-1, 0.1)
lines11.InsertCellPoint(31);
points11.SetPoint(32,19,-1.2, 0.1)
lines11.InsertCellPoint(32);
points11.SetPoint(33,19,-1.4, 0.1)
lines11.InsertCellPoint(33);
points11.SetPoint(34,19,-1.6, 0.1)
lines11.InsertCellPoint(34);
points11.SetPoint(35,19,-1.8, 0.1)
lines11.InsertCellPoint(35);
points11.SetPoint(36,19,-2, 0.1)
lines11.InsertCellPoint(36);
points11.SetPoint(37,19,-2.2, 0.1)
lines11.InsertCellPoint(37);
points11.SetPoint(38,19,-2.4, 0.1)
lines11.InsertCellPoint(38);
points11.SetPoint(39,19,-2.6, 0.1)
lines11.InsertCellPoint(39);
points11.SetPoint(40,19,-2.8, 0.1)
lines11.InsertCellPoint(40);
points11.SetPoint(41,19,-3, 0.1)
lines11.InsertCellPoint(41);
points11.SetPoint(42,19,-3.2, 0.1)
lines11.InsertCellPoint(42);
points11.SetPoint(43,19,-3.4, 0.1)
lines11.InsertCellPoint(43);
points11.SetPoint(44,19,-3.6, 0.1)
lines11.InsertCellPoint(44);
points11.SetPoint(45,19,-3.8, 0.1)
lines11.InsertCellPoint(45);
points11.SetPoint(46,19,-4, 0.1)
lines11.InsertCellPoint(46);
points11.SetPoint(47,19,-4.2, 0.1)
lines11.InsertCellPoint(47);
points11.SetPoint(48,19,-4.4, 0.1)
lines11.InsertCellPoint(48);
points11.SetPoint(49,19,-4.6, 0.1)
lines11.InsertCellPoint(49);
points11.SetPoint(50,19,-4.8, 0.1)
lines11.InsertCellPoint(50);
points11.SetPoint(51,19,-5, 0.1)
lines11.InsertCellPoint(51);
points11.SetPoint(52,19,-5, 0.1)
lines11.InsertCellPoint(52);
points11.SetPoint(53,19,-5.20833, 0.1)
lines11.InsertCellPoint(53);
points11.SetPoint(54,19,-5.41667, 0.1)
lines11.InsertCellPoint(54);
points11.SetPoint(55,19,-5.625, 0.1)
lines11.InsertCellPoint(55);
points11.SetPoint(56,19,-5.83333, 0.1)
lines11.InsertCellPoint(56);
points11.SetPoint(57,19,-6.04167, 0.1)
lines11.InsertCellPoint(57);
points11.SetPoint(58,19,-6.25, 0.1)
lines11.InsertCellPoint(58);
points11.SetPoint(59,19,-6.45833, 0.1)
lines11.InsertCellPoint(59);
points11.SetPoint(60,19,-6.66667, 0.1)
lines11.InsertCellPoint(60);
points11.SetPoint(61,19,-6.875, 0.1)
lines11.InsertCellPoint(61);
points11.SetPoint(62,19,-7.08333, 0.1)
lines11.InsertCellPoint(62);
points11.SetPoint(63,19,-7.29167, 0.1)
lines11.InsertCellPoint(63);
points11.SetPoint(64,19,-7.5, 0.1)
lines11.InsertCellPoint(64);
points11.SetPoint(65,19,-7.5, 0.1)
lines11.InsertCellPoint(65);
points11.SetPoint(66,19.1613,-7.37903, 0.1)
lines11.InsertCellPoint(66);
points11.SetPoint(67,19.3226,-7.25806, 0.1)
lines11.InsertCellPoint(67);
points11.SetPoint(68,19.4839,-7.1371, 0.1)
lines11.InsertCellPoint(68);
points11.SetPoint(69,19.6452,-7.01613, 0.1)
lines11.InsertCellPoint(69);
points11.SetPoint(70,19.8065,-6.89516, 0.1)
lines11.InsertCellPoint(70);
points11.SetPoint(71,19.9677,-6.77419, 0.1)
lines11.InsertCellPoint(71);
points11.SetPoint(72,20.129,-6.65323, 0.1)
lines11.InsertCellPoint(72);
points11.SetPoint(73,20.2903,-6.53226, 0.1)
lines11.InsertCellPoint(73);
points11.SetPoint(74,20.4516,-6.41129, 0.1)
lines11.InsertCellPoint(74);
points11.SetPoint(75,20.6129,-6.29032, 0.1)
lines11.InsertCellPoint(75);
points11.SetPoint(76,20.7742,-6.16935, 0.1)
lines11.InsertCellPoint(76);
points11.SetPoint(77,20.9355,-6.04839, 0.1)
lines11.InsertCellPoint(77);
points11.SetPoint(78,21.0968,-5.92742, 0.1)
lines11.InsertCellPoint(78);
points11.SetPoint(79,21.2581,-5.80645, 0.1)
lines11.InsertCellPoint(79);
points11.SetPoint(80,21.4194,-5.68548, 0.1)
lines11.InsertCellPoint(80);
points11.SetPoint(81,21.5806,-5.56452, 0.1)
lines11.InsertCellPoint(81);
points11.SetPoint(82,21.7419,-5.44355, 0.1)
lines11.InsertCellPoint(82);
points11.SetPoint(83,21.9032,-5.32258, 0.1)
lines11.InsertCellPoint(83);
points11.SetPoint(84,22.0645,-5.20161, 0.1)
lines11.InsertCellPoint(84);
points11.SetPoint(85,22.2258,-5.08065, 0.1)
lines11.InsertCellPoint(85);
points11.SetPoint(86,22.3871,-4.95968, 0.1)
lines11.InsertCellPoint(86);
points11.SetPoint(87,22.5484,-4.83871, 0.1)
lines11.InsertCellPoint(87);
points11.SetPoint(88,22.7097,-4.71774, 0.1)
lines11.InsertCellPoint(88);
points11.SetPoint(89,22.871,-4.59677, 0.1)
lines11.InsertCellPoint(89);
points11.SetPoint(90,23.0323,-4.47581, 0.1)
lines11.InsertCellPoint(90);
points11.SetPoint(91,23.1935,-4.35484, 0.1)
lines11.InsertCellPoint(91);
points11.SetPoint(92,23.3548,-4.23387, 0.1)
lines11.InsertCellPoint(92);
points11.SetPoint(93,23.5161,-4.1129, 0.1)
lines11.InsertCellPoint(93);
points11.SetPoint(94,23.6774,-3.99194, 0.1)
lines11.InsertCellPoint(94);
points11.SetPoint(95,23.8387,-3.87097, 0.1)
lines11.InsertCellPoint(95);
points11.SetPoint(96,24,-3.75, 0.1)
lines11.InsertCellPoint(96);
points11.SetPoint(97,24.1613,-3.62903, 0.1)
lines11.InsertCellPoint(97);
points11.SetPoint(98,24.3226,-3.50806, 0.1)
lines11.InsertCellPoint(98);
points11.SetPoint(99,24.4839,-3.3871, 0.1)
lines11.InsertCellPoint(99);
points11.SetPoint(100,24.6452,-3.26613, 0.1)
lines11.InsertCellPoint(100);
points11.SetPoint(101,24.8065,-3.14516, 0.1)
lines11.InsertCellPoint(101);
points11.SetPoint(102,24.9677,-3.02419, 0.1)
lines11.InsertCellPoint(102);
points11.SetPoint(103,25.129,-2.90323, 0.1)
lines11.InsertCellPoint(103);
points11.SetPoint(104,25.2903,-2.78226, 0.1)
lines11.InsertCellPoint(104);
points11.SetPoint(105,25.4516,-2.66129, 0.1)
lines11.InsertCellPoint(105);
points11.SetPoint(106,25.6129,-2.54032, 0.1)
lines11.InsertCellPoint(106);
points11.SetPoint(107,25.7742,-2.41935, 0.1)
lines11.InsertCellPoint(107);
points11.SetPoint(108,25.9355,-2.29839, 0.1)
lines11.InsertCellPoint(108);
points11.SetPoint(109,26.0968,-2.17742, 0.1)
lines11.InsertCellPoint(109);
points11.SetPoint(110,26.2581,-2.05645, 0.1)
lines11.InsertCellPoint(110);
points11.SetPoint(111,26.4194,-1.93548, 0.1)
lines11.InsertCellPoint(111);
points11.SetPoint(112,26.5806,-1.81452, 0.1)
lines11.InsertCellPoint(112);
points11.SetPoint(113,26.7419,-1.69355, 0.1)
lines11.InsertCellPoint(113);
points11.SetPoint(114,26.9032,-1.57258, 0.1)
lines11.InsertCellPoint(114);
points11.SetPoint(115,27.0645,-1.45161, 0.1)
lines11.InsertCellPoint(115);
points11.SetPoint(116,27.2258,-1.33065, 0.1)
lines11.InsertCellPoint(116);
points11.SetPoint(117,27.3871,-1.20968, 0.1)
lines11.InsertCellPoint(117);
points11.SetPoint(118,27.5484,-1.08871, 0.1)
lines11.InsertCellPoint(118);
points11.SetPoint(119,27.7097,-0.967742, 0.1)
lines11.InsertCellPoint(119);
points11.SetPoint(120,27.871,-0.846774, 0.1)
lines11.InsertCellPoint(120);
points11.SetPoint(121,28.0323,-0.725806, 0.1)
lines11.InsertCellPoint(121);
points11.SetPoint(122,28.1935,-0.604839, 0.1)
lines11.InsertCellPoint(122);
points11.SetPoint(123,28.3548,-0.483871, 0.1)
lines11.InsertCellPoint(123);
points11.SetPoint(124,28.5161,-0.362903, 0.1)
lines11.InsertCellPoint(124);
points11.SetPoint(125,28.6774,-0.241935, 0.1)
lines11.InsertCellPoint(125);
points11.SetPoint(126,28.8387,-0.120968, 0.1)
lines11.InsertCellPoint(126);
points11.SetPoint(127,29,0, 0.1)
lines11.InsertCellPoint(127);
points11.SetPoint(128,29,0, 0.1)
lines11.InsertCellPoint(128);
points11.SetPoint(129,28.8,0, 0.1)
lines11.InsertCellPoint(129);
points11.SetPoint(130,28.6,0, 0.1)
lines11.InsertCellPoint(130);
points11.SetPoint(131,28.4,0, 0.1)
lines11.InsertCellPoint(131);
points11.SetPoint(132,28.2,0, 0.1)
lines11.InsertCellPoint(132);
points11.SetPoint(133,28,0, 0.1)
lines11.InsertCellPoint(133);
points11.SetPoint(134,27.8,0, 0.1)
lines11.InsertCellPoint(134);
points11.SetPoint(135,27.6,0, 0.1)
lines11.InsertCellPoint(135);
points11.SetPoint(136,27.4,0, 0.1)
lines11.InsertCellPoint(136);
points11.SetPoint(137,27.2,0, 0.1)
lines11.InsertCellPoint(137);
points11.SetPoint(138,27,0, 0.1)
lines11.InsertCellPoint(138);
points11.SetPoint(139,26.8,0, 0.1)
lines11.InsertCellPoint(139);
points11.SetPoint(140,26.6,0, 0.1)
lines11.InsertCellPoint(140);
points11.SetPoint(141,26.4,0, 0.1)
lines11.InsertCellPoint(141);
points11.SetPoint(142,26.2,0, 0.1)
lines11.InsertCellPoint(142);
points11.SetPoint(143,26,0, 0.1)
lines11.InsertCellPoint(143);
points11.SetPoint(144,25.8,0, 0.1)
lines11.InsertCellPoint(144);
points11.SetPoint(145,25.6,0, 0.1)
lines11.InsertCellPoint(145);
points11.SetPoint(146,25.4,0, 0.1)
lines11.InsertCellPoint(146);
points11.SetPoint(147,25.2,0, 0.1)
lines11.InsertCellPoint(147);
points11.SetPoint(148,25,0, 0.1)
lines11.InsertCellPoint(148);
points11.SetPoint(149,24.8,0, 0.1)
lines11.InsertCellPoint(149);
points11.SetPoint(150,24.6,0, 0.1)
lines11.InsertCellPoint(150);
points11.SetPoint(151,24.4,0, 0.1)
lines11.InsertCellPoint(151);
points11.SetPoint(152,24.2,0, 0.1)
lines11.InsertCellPoint(152);
points11.SetPoint(153,24,0, 0.1)
lines11.InsertCellPoint(153);
lines11.InsertCellPoint(0);
polygon11 = vtk.vtkPolyData()
polygon11.SetPoints(points11)
polygon11.SetLines(lines11)
polygonMapper11 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper11.SetInputConnection(polygon11.GetProducerPort())
else:
	polygonMapper11.SetInputData(polygon11)
	polygonMapper11.Update()
polygonActor11 = vtk.vtkActor()
polygonActor11.SetMapper(polygonMapper11)
polygonActor11.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor11.GetProperty().SetLineWidth(widthLine)
points12 = vtk.vtkPoints()
points12.SetNumberOfPoints(109)
lines12 = vtk.vtkCellArray()
lines12.InsertNextCell(110)
points12.SetPoint(0,29,0, 0.1)
lines12.InsertCellPoint(0);
points12.SetPoint(1,29.2,0, 0.1)
lines12.InsertCellPoint(1);
points12.SetPoint(2,29.4,0, 0.1)
lines12.InsertCellPoint(2);
points12.SetPoint(3,29.6,0, 0.1)
lines12.InsertCellPoint(3);
points12.SetPoint(4,29.8,0, 0.1)
lines12.InsertCellPoint(4);
points12.SetPoint(5,30,0, 0.1)
lines12.InsertCellPoint(5);
points12.SetPoint(6,30.2,0, 0.1)
lines12.InsertCellPoint(6);
points12.SetPoint(7,30.4,0, 0.1)
lines12.InsertCellPoint(7);
points12.SetPoint(8,30.6,0, 0.1)
lines12.InsertCellPoint(8);
points12.SetPoint(9,30.8,0, 0.1)
lines12.InsertCellPoint(9);
points12.SetPoint(10,31,0, 0.1)
lines12.InsertCellPoint(10);
points12.SetPoint(11,31.2,0, 0.1)
lines12.InsertCellPoint(11);
points12.SetPoint(12,31.4,0, 0.1)
lines12.InsertCellPoint(12);
points12.SetPoint(13,31.6,0, 0.1)
lines12.InsertCellPoint(13);
points12.SetPoint(14,31.8,0, 0.1)
lines12.InsertCellPoint(14);
points12.SetPoint(15,32,0, 0.1)
lines12.InsertCellPoint(15);
points12.SetPoint(16,32.2,0, 0.1)
lines12.InsertCellPoint(16);
points12.SetPoint(17,32.4,0, 0.1)
lines12.InsertCellPoint(17);
points12.SetPoint(18,32.6,0, 0.1)
lines12.InsertCellPoint(18);
points12.SetPoint(19,32.8,0, 0.1)
lines12.InsertCellPoint(19);
points12.SetPoint(20,33,0, 0.1)
lines12.InsertCellPoint(20);
points12.SetPoint(21,33,0, 0.1)
lines12.InsertCellPoint(21);
points12.SetPoint(22,33.12,0.16, 0.1)
lines12.InsertCellPoint(22);
points12.SetPoint(23,33.24,0.32, 0.1)
lines12.InsertCellPoint(23);
points12.SetPoint(24,33.36,0.48, 0.1)
lines12.InsertCellPoint(24);
points12.SetPoint(25,33.48,0.64, 0.1)
lines12.InsertCellPoint(25);
points12.SetPoint(26,33.6,0.8, 0.1)
lines12.InsertCellPoint(26);
points12.SetPoint(27,33.72,0.96, 0.1)
lines12.InsertCellPoint(27);
points12.SetPoint(28,33.84,1.12, 0.1)
lines12.InsertCellPoint(28);
points12.SetPoint(29,33.96,1.28, 0.1)
lines12.InsertCellPoint(29);
points12.SetPoint(30,34.08,1.44, 0.1)
lines12.InsertCellPoint(30);
points12.SetPoint(31,34.2,1.6, 0.1)
lines12.InsertCellPoint(31);
points12.SetPoint(32,34.32,1.76, 0.1)
lines12.InsertCellPoint(32);
points12.SetPoint(33,34.44,1.92, 0.1)
lines12.InsertCellPoint(33);
points12.SetPoint(34,34.56,2.08, 0.1)
lines12.InsertCellPoint(34);
points12.SetPoint(35,34.68,2.24, 0.1)
lines12.InsertCellPoint(35);
points12.SetPoint(36,34.8,2.4, 0.1)
lines12.InsertCellPoint(36);
points12.SetPoint(37,34.92,2.56, 0.1)
lines12.InsertCellPoint(37);
points12.SetPoint(38,35.04,2.72, 0.1)
lines12.InsertCellPoint(38);
points12.SetPoint(39,35.16,2.88, 0.1)
lines12.InsertCellPoint(39);
points12.SetPoint(40,35.28,3.04, 0.1)
lines12.InsertCellPoint(40);
points12.SetPoint(41,35.4,3.2, 0.1)
lines12.InsertCellPoint(41);
points12.SetPoint(42,35.52,3.36, 0.1)
lines12.InsertCellPoint(42);
points12.SetPoint(43,35.64,3.52, 0.1)
lines12.InsertCellPoint(43);
points12.SetPoint(44,35.76,3.68, 0.1)
lines12.InsertCellPoint(44);
points12.SetPoint(45,35.88,3.84, 0.1)
lines12.InsertCellPoint(45);
points12.SetPoint(46,36,4, 0.1)
lines12.InsertCellPoint(46);
points12.SetPoint(47,36,4, 0.1)
lines12.InsertCellPoint(47);
points12.SetPoint(48,35.8,4.02857, 0.1)
lines12.InsertCellPoint(48);
points12.SetPoint(49,35.6,4.05714, 0.1)
lines12.InsertCellPoint(49);
points12.SetPoint(50,35.4,4.08571, 0.1)
lines12.InsertCellPoint(50);
points12.SetPoint(51,35.2,4.11429, 0.1)
lines12.InsertCellPoint(51);
points12.SetPoint(52,35,4.14286, 0.1)
lines12.InsertCellPoint(52);
points12.SetPoint(53,34.8,4.17143, 0.1)
lines12.InsertCellPoint(53);
points12.SetPoint(54,34.6,4.2, 0.1)
lines12.InsertCellPoint(54);
points12.SetPoint(55,34.4,4.22857, 0.1)
lines12.InsertCellPoint(55);
points12.SetPoint(56,34.2,4.25714, 0.1)
lines12.InsertCellPoint(56);
points12.SetPoint(57,34,4.28571, 0.1)
lines12.InsertCellPoint(57);
points12.SetPoint(58,33.8,4.31429, 0.1)
lines12.InsertCellPoint(58);
points12.SetPoint(59,33.6,4.34286, 0.1)
lines12.InsertCellPoint(59);
points12.SetPoint(60,33.4,4.37143, 0.1)
lines12.InsertCellPoint(60);
points12.SetPoint(61,33.2,4.4, 0.1)
lines12.InsertCellPoint(61);
points12.SetPoint(62,33,4.42857, 0.1)
lines12.InsertCellPoint(62);
points12.SetPoint(63,32.8,4.45714, 0.1)
lines12.InsertCellPoint(63);
points12.SetPoint(64,32.6,4.48571, 0.1)
lines12.InsertCellPoint(64);
points12.SetPoint(65,32.4,4.51429, 0.1)
lines12.InsertCellPoint(65);
points12.SetPoint(66,32.2,4.54286, 0.1)
lines12.InsertCellPoint(66);
points12.SetPoint(67,32,4.57143, 0.1)
lines12.InsertCellPoint(67);
points12.SetPoint(68,31.8,4.6, 0.1)
lines12.InsertCellPoint(68);
points12.SetPoint(69,31.6,4.62857, 0.1)
lines12.InsertCellPoint(69);
points12.SetPoint(70,31.4,4.65714, 0.1)
lines12.InsertCellPoint(70);
points12.SetPoint(71,31.2,4.68571, 0.1)
lines12.InsertCellPoint(71);
points12.SetPoint(72,31,4.71429, 0.1)
lines12.InsertCellPoint(72);
points12.SetPoint(73,30.8,4.74286, 0.1)
lines12.InsertCellPoint(73);
points12.SetPoint(74,30.6,4.77143, 0.1)
lines12.InsertCellPoint(74);
points12.SetPoint(75,30.4,4.8, 0.1)
lines12.InsertCellPoint(75);
points12.SetPoint(76,30.2,4.82857, 0.1)
lines12.InsertCellPoint(76);
points12.SetPoint(77,30,4.85714, 0.1)
lines12.InsertCellPoint(77);
points12.SetPoint(78,29.8,4.88571, 0.1)
lines12.InsertCellPoint(78);
points12.SetPoint(79,29.6,4.91429, 0.1)
lines12.InsertCellPoint(79);
points12.SetPoint(80,29.4,4.94286, 0.1)
lines12.InsertCellPoint(80);
points12.SetPoint(81,29.2,4.97143, 0.1)
lines12.InsertCellPoint(81);
points12.SetPoint(82,29,5, 0.1)
lines12.InsertCellPoint(82);
points12.SetPoint(83,29,5, 0.1)
lines12.InsertCellPoint(83);
points12.SetPoint(84,29,4.8, 0.1)
lines12.InsertCellPoint(84);
points12.SetPoint(85,29,4.6, 0.1)
lines12.InsertCellPoint(85);
points12.SetPoint(86,29,4.4, 0.1)
lines12.InsertCellPoint(86);
points12.SetPoint(87,29,4.2, 0.1)
lines12.InsertCellPoint(87);
points12.SetPoint(88,29,4, 0.1)
lines12.InsertCellPoint(88);
points12.SetPoint(89,29,3.8, 0.1)
lines12.InsertCellPoint(89);
points12.SetPoint(90,29,3.6, 0.1)
lines12.InsertCellPoint(90);
points12.SetPoint(91,29,3.4, 0.1)
lines12.InsertCellPoint(91);
points12.SetPoint(92,29,3.2, 0.1)
lines12.InsertCellPoint(92);
points12.SetPoint(93,29,3, 0.1)
lines12.InsertCellPoint(93);
points12.SetPoint(94,29,2.8, 0.1)
lines12.InsertCellPoint(94);
points12.SetPoint(95,29,2.6, 0.1)
lines12.InsertCellPoint(95);
points12.SetPoint(96,29,2.4, 0.1)
lines12.InsertCellPoint(96);
points12.SetPoint(97,29,2.2, 0.1)
lines12.InsertCellPoint(97);
points12.SetPoint(98,29,2, 0.1)
lines12.InsertCellPoint(98);
points12.SetPoint(99,29,1.8, 0.1)
lines12.InsertCellPoint(99);
points12.SetPoint(100,29,1.6, 0.1)
lines12.InsertCellPoint(100);
points12.SetPoint(101,29,1.4, 0.1)
lines12.InsertCellPoint(101);
points12.SetPoint(102,29,1.2, 0.1)
lines12.InsertCellPoint(102);
points12.SetPoint(103,29,1, 0.1)
lines12.InsertCellPoint(103);
points12.SetPoint(104,29,0.8, 0.1)
lines12.InsertCellPoint(104);
points12.SetPoint(105,29,0.6, 0.1)
lines12.InsertCellPoint(105);
points12.SetPoint(106,29,0.4, 0.1)
lines12.InsertCellPoint(106);
points12.SetPoint(107,29,0.2, 0.1)
lines12.InsertCellPoint(107);
points12.SetPoint(108,29,0, 0.1)
lines12.InsertCellPoint(108);
lines12.InsertCellPoint(0);
polygon12 = vtk.vtkPolyData()
polygon12.SetPoints(points12)
polygon12.SetLines(lines12)
polygonMapper12 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper12.SetInputConnection(polygon12.GetProducerPort())
else:
	polygonMapper12.SetInputData(polygon12)
	polygonMapper12.Update()
polygonActor12 = vtk.vtkActor()
polygonActor12.SetMapper(polygonMapper12)
polygonActor12.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor12.GetProperty().SetLineWidth(widthLine)
points13 = vtk.vtkPoints()
points13.SetNumberOfPoints(172)
lines13 = vtk.vtkCellArray()
lines13.InsertNextCell(173)
points13.SetPoint(0,19,-7.5, 0.1)
lines13.InsertCellPoint(0);
points13.SetPoint(1,19.1912,-7.44118, 0.1)
lines13.InsertCellPoint(1);
points13.SetPoint(2,19.3824,-7.38235, 0.1)
lines13.InsertCellPoint(2);
points13.SetPoint(3,19.5735,-7.32353, 0.1)
lines13.InsertCellPoint(3);
points13.SetPoint(4,19.7647,-7.26471, 0.1)
lines13.InsertCellPoint(4);
points13.SetPoint(5,19.9559,-7.20588, 0.1)
lines13.InsertCellPoint(5);
points13.SetPoint(6,20.1471,-7.14706, 0.1)
lines13.InsertCellPoint(6);
points13.SetPoint(7,20.3382,-7.08824, 0.1)
lines13.InsertCellPoint(7);
points13.SetPoint(8,20.5294,-7.02941, 0.1)
lines13.InsertCellPoint(8);
points13.SetPoint(9,20.7206,-6.97059, 0.1)
lines13.InsertCellPoint(9);
points13.SetPoint(10,20.9118,-6.91176, 0.1)
lines13.InsertCellPoint(10);
points13.SetPoint(11,21.1029,-6.85294, 0.1)
lines13.InsertCellPoint(11);
points13.SetPoint(12,21.2941,-6.79412, 0.1)
lines13.InsertCellPoint(12);
points13.SetPoint(13,21.4853,-6.73529, 0.1)
lines13.InsertCellPoint(13);
points13.SetPoint(14,21.6765,-6.67647, 0.1)
lines13.InsertCellPoint(14);
points13.SetPoint(15,21.8676,-6.61765, 0.1)
lines13.InsertCellPoint(15);
points13.SetPoint(16,22.0588,-6.55882, 0.1)
lines13.InsertCellPoint(16);
points13.SetPoint(17,22.25,-6.5, 0.1)
lines13.InsertCellPoint(17);
points13.SetPoint(18,22.4412,-6.44118, 0.1)
lines13.InsertCellPoint(18);
points13.SetPoint(19,22.6324,-6.38235, 0.1)
lines13.InsertCellPoint(19);
points13.SetPoint(20,22.8235,-6.32353, 0.1)
lines13.InsertCellPoint(20);
points13.SetPoint(21,23.0147,-6.26471, 0.1)
lines13.InsertCellPoint(21);
points13.SetPoint(22,23.2059,-6.20588, 0.1)
lines13.InsertCellPoint(22);
points13.SetPoint(23,23.3971,-6.14706, 0.1)
lines13.InsertCellPoint(23);
points13.SetPoint(24,23.5882,-6.08824, 0.1)
lines13.InsertCellPoint(24);
points13.SetPoint(25,23.7794,-6.02941, 0.1)
lines13.InsertCellPoint(25);
points13.SetPoint(26,23.9706,-5.97059, 0.1)
lines13.InsertCellPoint(26);
points13.SetPoint(27,24.1618,-5.91176, 0.1)
lines13.InsertCellPoint(27);
points13.SetPoint(28,24.3529,-5.85294, 0.1)
lines13.InsertCellPoint(28);
points13.SetPoint(29,24.5441,-5.79412, 0.1)
lines13.InsertCellPoint(29);
points13.SetPoint(30,24.7353,-5.73529, 0.1)
lines13.InsertCellPoint(30);
points13.SetPoint(31,24.9265,-5.67647, 0.1)
lines13.InsertCellPoint(31);
points13.SetPoint(32,25.1176,-5.61765, 0.1)
lines13.InsertCellPoint(32);
points13.SetPoint(33,25.3088,-5.55882, 0.1)
lines13.InsertCellPoint(33);
points13.SetPoint(34,25.5,-5.5, 0.1)
lines13.InsertCellPoint(34);
points13.SetPoint(35,25.6912,-5.44118, 0.1)
lines13.InsertCellPoint(35);
points13.SetPoint(36,25.8824,-5.38235, 0.1)
lines13.InsertCellPoint(36);
points13.SetPoint(37,26.0735,-5.32353, 0.1)
lines13.InsertCellPoint(37);
points13.SetPoint(38,26.2647,-5.26471, 0.1)
lines13.InsertCellPoint(38);
points13.SetPoint(39,26.4559,-5.20588, 0.1)
lines13.InsertCellPoint(39);
points13.SetPoint(40,26.6471,-5.14706, 0.1)
lines13.InsertCellPoint(40);
points13.SetPoint(41,26.8382,-5.08824, 0.1)
lines13.InsertCellPoint(41);
points13.SetPoint(42,27.0294,-5.02941, 0.1)
lines13.InsertCellPoint(42);
points13.SetPoint(43,27.2206,-4.97059, 0.1)
lines13.InsertCellPoint(43);
points13.SetPoint(44,27.4118,-4.91176, 0.1)
lines13.InsertCellPoint(44);
points13.SetPoint(45,27.6029,-4.85294, 0.1)
lines13.InsertCellPoint(45);
points13.SetPoint(46,27.7941,-4.79412, 0.1)
lines13.InsertCellPoint(46);
points13.SetPoint(47,27.9853,-4.73529, 0.1)
lines13.InsertCellPoint(47);
points13.SetPoint(48,28.1765,-4.67647, 0.1)
lines13.InsertCellPoint(48);
points13.SetPoint(49,28.3676,-4.61765, 0.1)
lines13.InsertCellPoint(49);
points13.SetPoint(50,28.5588,-4.55882, 0.1)
lines13.InsertCellPoint(50);
points13.SetPoint(51,28.75,-4.5, 0.1)
lines13.InsertCellPoint(51);
points13.SetPoint(52,28.9412,-4.44118, 0.1)
lines13.InsertCellPoint(52);
points13.SetPoint(53,29.1324,-4.38235, 0.1)
lines13.InsertCellPoint(53);
points13.SetPoint(54,29.3235,-4.32353, 0.1)
lines13.InsertCellPoint(54);
points13.SetPoint(55,29.5147,-4.26471, 0.1)
lines13.InsertCellPoint(55);
points13.SetPoint(56,29.7059,-4.20588, 0.1)
lines13.InsertCellPoint(56);
points13.SetPoint(57,29.8971,-4.14706, 0.1)
lines13.InsertCellPoint(57);
points13.SetPoint(58,30.0882,-4.08824, 0.1)
lines13.InsertCellPoint(58);
points13.SetPoint(59,30.2794,-4.02941, 0.1)
lines13.InsertCellPoint(59);
points13.SetPoint(60,30.4706,-3.97059, 0.1)
lines13.InsertCellPoint(60);
points13.SetPoint(61,30.6618,-3.91176, 0.1)
lines13.InsertCellPoint(61);
points13.SetPoint(62,30.8529,-3.85294, 0.1)
lines13.InsertCellPoint(62);
points13.SetPoint(63,31.0441,-3.79412, 0.1)
lines13.InsertCellPoint(63);
points13.SetPoint(64,31.2353,-3.73529, 0.1)
lines13.InsertCellPoint(64);
points13.SetPoint(65,31.4265,-3.67647, 0.1)
lines13.InsertCellPoint(65);
points13.SetPoint(66,31.6176,-3.61765, 0.1)
lines13.InsertCellPoint(66);
points13.SetPoint(67,31.8088,-3.55882, 0.1)
lines13.InsertCellPoint(67);
points13.SetPoint(68,32,-3.5, 0.1)
lines13.InsertCellPoint(68);
points13.SetPoint(69,32,-3.5, 0.1)
lines13.InsertCellPoint(69);
points13.SetPoint(70,32.0556,-3.30556, 0.1)
lines13.InsertCellPoint(70);
points13.SetPoint(71,32.1111,-3.11111, 0.1)
lines13.InsertCellPoint(71);
points13.SetPoint(72,32.1667,-2.91667, 0.1)
lines13.InsertCellPoint(72);
points13.SetPoint(73,32.2222,-2.72222, 0.1)
lines13.InsertCellPoint(73);
points13.SetPoint(74,32.2778,-2.52778, 0.1)
lines13.InsertCellPoint(74);
points13.SetPoint(75,32.3333,-2.33333, 0.1)
lines13.InsertCellPoint(75);
points13.SetPoint(76,32.3889,-2.13889, 0.1)
lines13.InsertCellPoint(76);
points13.SetPoint(77,32.4444,-1.94444, 0.1)
lines13.InsertCellPoint(77);
points13.SetPoint(78,32.5,-1.75, 0.1)
lines13.InsertCellPoint(78);
points13.SetPoint(79,32.5556,-1.55556, 0.1)
lines13.InsertCellPoint(79);
points13.SetPoint(80,32.6111,-1.36111, 0.1)
lines13.InsertCellPoint(80);
points13.SetPoint(81,32.6667,-1.16667, 0.1)
lines13.InsertCellPoint(81);
points13.SetPoint(82,32.7222,-0.972222, 0.1)
lines13.InsertCellPoint(82);
points13.SetPoint(83,32.7778,-0.777778, 0.1)
lines13.InsertCellPoint(83);
points13.SetPoint(84,32.8333,-0.583333, 0.1)
lines13.InsertCellPoint(84);
points13.SetPoint(85,32.8889,-0.388889, 0.1)
lines13.InsertCellPoint(85);
points13.SetPoint(86,32.9444,-0.194444, 0.1)
lines13.InsertCellPoint(86);
points13.SetPoint(87,33,0, 0.1)
lines13.InsertCellPoint(87);
points13.SetPoint(88,33,0, 0.1)
lines13.InsertCellPoint(88);
points13.SetPoint(89,32.8,0, 0.1)
lines13.InsertCellPoint(89);
points13.SetPoint(90,32.6,0, 0.1)
lines13.InsertCellPoint(90);
points13.SetPoint(91,32.4,0, 0.1)
lines13.InsertCellPoint(91);
points13.SetPoint(92,32.2,0, 0.1)
lines13.InsertCellPoint(92);
points13.SetPoint(93,32,0, 0.1)
lines13.InsertCellPoint(93);
points13.SetPoint(94,31.8,0, 0.1)
lines13.InsertCellPoint(94);
points13.SetPoint(95,31.6,0, 0.1)
lines13.InsertCellPoint(95);
points13.SetPoint(96,31.4,0, 0.1)
lines13.InsertCellPoint(96);
points13.SetPoint(97,31.2,0, 0.1)
lines13.InsertCellPoint(97);
points13.SetPoint(98,31,0, 0.1)
lines13.InsertCellPoint(98);
points13.SetPoint(99,30.8,0, 0.1)
lines13.InsertCellPoint(99);
points13.SetPoint(100,30.6,0, 0.1)
lines13.InsertCellPoint(100);
points13.SetPoint(101,30.4,0, 0.1)
lines13.InsertCellPoint(101);
points13.SetPoint(102,30.2,0, 0.1)
lines13.InsertCellPoint(102);
points13.SetPoint(103,30,0, 0.1)
lines13.InsertCellPoint(103);
points13.SetPoint(104,29.8,0, 0.1)
lines13.InsertCellPoint(104);
points13.SetPoint(105,29.6,0, 0.1)
lines13.InsertCellPoint(105);
points13.SetPoint(106,29.4,0, 0.1)
lines13.InsertCellPoint(106);
points13.SetPoint(107,29.2,0, 0.1)
lines13.InsertCellPoint(107);
points13.SetPoint(108,29,0, 0.1)
lines13.InsertCellPoint(108);
points13.SetPoint(109,29,0, 0.1)
lines13.InsertCellPoint(109);
points13.SetPoint(110,28.8387,-0.120968, 0.1)
lines13.InsertCellPoint(110);
points13.SetPoint(111,28.6774,-0.241935, 0.1)
lines13.InsertCellPoint(111);
points13.SetPoint(112,28.5161,-0.362903, 0.1)
lines13.InsertCellPoint(112);
points13.SetPoint(113,28.3548,-0.483871, 0.1)
lines13.InsertCellPoint(113);
points13.SetPoint(114,28.1935,-0.604839, 0.1)
lines13.InsertCellPoint(114);
points13.SetPoint(115,28.0323,-0.725806, 0.1)
lines13.InsertCellPoint(115);
points13.SetPoint(116,27.871,-0.846774, 0.1)
lines13.InsertCellPoint(116);
points13.SetPoint(117,27.7097,-0.967742, 0.1)
lines13.InsertCellPoint(117);
points13.SetPoint(118,27.5484,-1.08871, 0.1)
lines13.InsertCellPoint(118);
points13.SetPoint(119,27.3871,-1.20968, 0.1)
lines13.InsertCellPoint(119);
points13.SetPoint(120,27.2258,-1.33065, 0.1)
lines13.InsertCellPoint(120);
points13.SetPoint(121,27.0645,-1.45161, 0.1)
lines13.InsertCellPoint(121);
points13.SetPoint(122,26.9032,-1.57258, 0.1)
lines13.InsertCellPoint(122);
points13.SetPoint(123,26.7419,-1.69355, 0.1)
lines13.InsertCellPoint(123);
points13.SetPoint(124,26.5806,-1.81452, 0.1)
lines13.InsertCellPoint(124);
points13.SetPoint(125,26.4194,-1.93548, 0.1)
lines13.InsertCellPoint(125);
points13.SetPoint(126,26.2581,-2.05645, 0.1)
lines13.InsertCellPoint(126);
points13.SetPoint(127,26.0968,-2.17742, 0.1)
lines13.InsertCellPoint(127);
points13.SetPoint(128,25.9355,-2.29839, 0.1)
lines13.InsertCellPoint(128);
points13.SetPoint(129,25.7742,-2.41935, 0.1)
lines13.InsertCellPoint(129);
points13.SetPoint(130,25.6129,-2.54032, 0.1)
lines13.InsertCellPoint(130);
points13.SetPoint(131,25.4516,-2.66129, 0.1)
lines13.InsertCellPoint(131);
points13.SetPoint(132,25.2903,-2.78226, 0.1)
lines13.InsertCellPoint(132);
points13.SetPoint(133,25.129,-2.90323, 0.1)
lines13.InsertCellPoint(133);
points13.SetPoint(134,24.9677,-3.02419, 0.1)
lines13.InsertCellPoint(134);
points13.SetPoint(135,24.8065,-3.14516, 0.1)
lines13.InsertCellPoint(135);
points13.SetPoint(136,24.6452,-3.26613, 0.1)
lines13.InsertCellPoint(136);
points13.SetPoint(137,24.4839,-3.3871, 0.1)
lines13.InsertCellPoint(137);
points13.SetPoint(138,24.3226,-3.50806, 0.1)
lines13.InsertCellPoint(138);
points13.SetPoint(139,24.1613,-3.62903, 0.1)
lines13.InsertCellPoint(139);
points13.SetPoint(140,24,-3.75, 0.1)
lines13.InsertCellPoint(140);
points13.SetPoint(141,23.8387,-3.87097, 0.1)
lines13.InsertCellPoint(141);
points13.SetPoint(142,23.6774,-3.99194, 0.1)
lines13.InsertCellPoint(142);
points13.SetPoint(143,23.5161,-4.1129, 0.1)
lines13.InsertCellPoint(143);
points13.SetPoint(144,23.3548,-4.23387, 0.1)
lines13.InsertCellPoint(144);
points13.SetPoint(145,23.1935,-4.35484, 0.1)
lines13.InsertCellPoint(145);
points13.SetPoint(146,23.0323,-4.47581, 0.1)
lines13.InsertCellPoint(146);
points13.SetPoint(147,22.871,-4.59677, 0.1)
lines13.InsertCellPoint(147);
points13.SetPoint(148,22.7097,-4.71774, 0.1)
lines13.InsertCellPoint(148);
points13.SetPoint(149,22.5484,-4.83871, 0.1)
lines13.InsertCellPoint(149);
points13.SetPoint(150,22.3871,-4.95968, 0.1)
lines13.InsertCellPoint(150);
points13.SetPoint(151,22.2258,-5.08065, 0.1)
lines13.InsertCellPoint(151);
points13.SetPoint(152,22.0645,-5.20161, 0.1)
lines13.InsertCellPoint(152);
points13.SetPoint(153,21.9032,-5.32258, 0.1)
lines13.InsertCellPoint(153);
points13.SetPoint(154,21.7419,-5.44355, 0.1)
lines13.InsertCellPoint(154);
points13.SetPoint(155,21.5806,-5.56452, 0.1)
lines13.InsertCellPoint(155);
points13.SetPoint(156,21.4194,-5.68548, 0.1)
lines13.InsertCellPoint(156);
points13.SetPoint(157,21.2581,-5.80645, 0.1)
lines13.InsertCellPoint(157);
points13.SetPoint(158,21.0968,-5.92742, 0.1)
lines13.InsertCellPoint(158);
points13.SetPoint(159,20.9355,-6.04839, 0.1)
lines13.InsertCellPoint(159);
points13.SetPoint(160,20.7742,-6.16935, 0.1)
lines13.InsertCellPoint(160);
points13.SetPoint(161,20.6129,-6.29032, 0.1)
lines13.InsertCellPoint(161);
points13.SetPoint(162,20.4516,-6.41129, 0.1)
lines13.InsertCellPoint(162);
points13.SetPoint(163,20.2903,-6.53226, 0.1)
lines13.InsertCellPoint(163);
points13.SetPoint(164,20.129,-6.65323, 0.1)
lines13.InsertCellPoint(164);
points13.SetPoint(165,19.9677,-6.77419, 0.1)
lines13.InsertCellPoint(165);
points13.SetPoint(166,19.8065,-6.89516, 0.1)
lines13.InsertCellPoint(166);
points13.SetPoint(167,19.6452,-7.01613, 0.1)
lines13.InsertCellPoint(167);
points13.SetPoint(168,19.4839,-7.1371, 0.1)
lines13.InsertCellPoint(168);
points13.SetPoint(169,19.3226,-7.25806, 0.1)
lines13.InsertCellPoint(169);
points13.SetPoint(170,19.1613,-7.37903, 0.1)
lines13.InsertCellPoint(170);
points13.SetPoint(171,19,-7.5, 0.1)
lines13.InsertCellPoint(171);
lines13.InsertCellPoint(0);
polygon13 = vtk.vtkPolyData()
polygon13.SetPoints(points13)
polygon13.SetLines(lines13)
polygonMapper13 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper13.SetInputConnection(polygon13.GetProducerPort())
else:
	polygonMapper13.SetInputData(polygon13)
	polygonMapper13.Update()
polygonActor13 = vtk.vtkActor()
polygonActor13.SetMapper(polygonMapper13)
polygonActor13.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor13.GetProperty().SetLineWidth(widthLine)
points14 = vtk.vtkPoints()
points14.SetNumberOfPoints(161)
lines14 = vtk.vtkCellArray()
lines14.InsertNextCell(162)
points14.SetPoint(0,29,5, 0.1)
lines14.InsertCellPoint(0);
points14.SetPoint(1,29,5.2, 0.1)
lines14.InsertCellPoint(1);
points14.SetPoint(2,29,5.4, 0.1)
lines14.InsertCellPoint(2);
points14.SetPoint(3,29,5.6, 0.1)
lines14.InsertCellPoint(3);
points14.SetPoint(4,29,5.8, 0.1)
lines14.InsertCellPoint(4);
points14.SetPoint(5,29,6, 0.1)
lines14.InsertCellPoint(5);
points14.SetPoint(6,29,6.2, 0.1)
lines14.InsertCellPoint(6);
points14.SetPoint(7,29,6.4, 0.1)
lines14.InsertCellPoint(7);
points14.SetPoint(8,29,6.6, 0.1)
lines14.InsertCellPoint(8);
points14.SetPoint(9,29,6.8, 0.1)
lines14.InsertCellPoint(9);
points14.SetPoint(10,29,7, 0.1)
lines14.InsertCellPoint(10);
points14.SetPoint(11,29,7.2, 0.1)
lines14.InsertCellPoint(11);
points14.SetPoint(12,29,7.4, 0.1)
lines14.InsertCellPoint(12);
points14.SetPoint(13,29,7.6, 0.1)
lines14.InsertCellPoint(13);
points14.SetPoint(14,29,7.8, 0.1)
lines14.InsertCellPoint(14);
points14.SetPoint(15,29,8, 0.1)
lines14.InsertCellPoint(15);
points14.SetPoint(16,29,8.2, 0.1)
lines14.InsertCellPoint(16);
points14.SetPoint(17,29,8.4, 0.1)
lines14.InsertCellPoint(17);
points14.SetPoint(18,29,8.6, 0.1)
lines14.InsertCellPoint(18);
points14.SetPoint(19,29,8.8, 0.1)
lines14.InsertCellPoint(19);
points14.SetPoint(20,29,9, 0.1)
lines14.InsertCellPoint(20);
points14.SetPoint(21,29,9, 0.1)
lines14.InsertCellPoint(21);
points14.SetPoint(22,28.8,9.05, 0.1)
lines14.InsertCellPoint(22);
points14.SetPoint(23,28.6,9.1, 0.1)
lines14.InsertCellPoint(23);
points14.SetPoint(24,28.4,9.15, 0.1)
lines14.InsertCellPoint(24);
points14.SetPoint(25,28.2,9.2, 0.1)
lines14.InsertCellPoint(25);
points14.SetPoint(26,28,9.25, 0.1)
lines14.InsertCellPoint(26);
points14.SetPoint(27,27.8,9.3, 0.1)
lines14.InsertCellPoint(27);
points14.SetPoint(28,27.6,9.35, 0.1)
lines14.InsertCellPoint(28);
points14.SetPoint(29,27.4,9.4, 0.1)
lines14.InsertCellPoint(29);
points14.SetPoint(30,27.2,9.45, 0.1)
lines14.InsertCellPoint(30);
points14.SetPoint(31,27,9.5, 0.1)
lines14.InsertCellPoint(31);
points14.SetPoint(32,26.8,9.55, 0.1)
lines14.InsertCellPoint(32);
points14.SetPoint(33,26.6,9.6, 0.1)
lines14.InsertCellPoint(33);
points14.SetPoint(34,26.4,9.65, 0.1)
lines14.InsertCellPoint(34);
points14.SetPoint(35,26.2,9.7, 0.1)
lines14.InsertCellPoint(35);
points14.SetPoint(36,26,9.75, 0.1)
lines14.InsertCellPoint(36);
points14.SetPoint(37,25.8,9.8, 0.1)
lines14.InsertCellPoint(37);
points14.SetPoint(38,25.6,9.85, 0.1)
lines14.InsertCellPoint(38);
points14.SetPoint(39,25.4,9.9, 0.1)
lines14.InsertCellPoint(39);
points14.SetPoint(40,25.2,9.95, 0.1)
lines14.InsertCellPoint(40);
points14.SetPoint(41,25,10, 0.1)
lines14.InsertCellPoint(41);
points14.SetPoint(42,25,10, 0.1)
lines14.InsertCellPoint(42);
points14.SetPoint(43,24.8,9.96667, 0.1)
lines14.InsertCellPoint(43);
points14.SetPoint(44,24.6,9.93333, 0.1)
lines14.InsertCellPoint(44);
points14.SetPoint(45,24.4,9.9, 0.1)
lines14.InsertCellPoint(45);
points14.SetPoint(46,24.2,9.86667, 0.1)
lines14.InsertCellPoint(46);
points14.SetPoint(47,24,9.83333, 0.1)
lines14.InsertCellPoint(47);
points14.SetPoint(48,23.8,9.8, 0.1)
lines14.InsertCellPoint(48);
points14.SetPoint(49,23.6,9.76667, 0.1)
lines14.InsertCellPoint(49);
points14.SetPoint(50,23.4,9.73333, 0.1)
lines14.InsertCellPoint(50);
points14.SetPoint(51,23.2,9.7, 0.1)
lines14.InsertCellPoint(51);
points14.SetPoint(52,23,9.66667, 0.1)
lines14.InsertCellPoint(52);
points14.SetPoint(53,22.8,9.63333, 0.1)
lines14.InsertCellPoint(53);
points14.SetPoint(54,22.6,9.6, 0.1)
lines14.InsertCellPoint(54);
points14.SetPoint(55,22.4,9.56667, 0.1)
lines14.InsertCellPoint(55);
points14.SetPoint(56,22.2,9.53333, 0.1)
lines14.InsertCellPoint(56);
points14.SetPoint(57,22,9.5, 0.1)
lines14.InsertCellPoint(57);
points14.SetPoint(58,21.8,9.46667, 0.1)
lines14.InsertCellPoint(58);
points14.SetPoint(59,21.6,9.43333, 0.1)
lines14.InsertCellPoint(59);
points14.SetPoint(60,21.4,9.4, 0.1)
lines14.InsertCellPoint(60);
points14.SetPoint(61,21.2,9.36667, 0.1)
lines14.InsertCellPoint(61);
points14.SetPoint(62,21,9.33333, 0.1)
lines14.InsertCellPoint(62);
points14.SetPoint(63,20.8,9.3, 0.1)
lines14.InsertCellPoint(63);
points14.SetPoint(64,20.6,9.26667, 0.1)
lines14.InsertCellPoint(64);
points14.SetPoint(65,20.4,9.23333, 0.1)
lines14.InsertCellPoint(65);
points14.SetPoint(66,20.2,9.2, 0.1)
lines14.InsertCellPoint(66);
points14.SetPoint(67,20,9.16667, 0.1)
lines14.InsertCellPoint(67);
points14.SetPoint(68,19.8,9.13333, 0.1)
lines14.InsertCellPoint(68);
points14.SetPoint(69,19.6,9.1, 0.1)
lines14.InsertCellPoint(69);
points14.SetPoint(70,19.4,9.06667, 0.1)
lines14.InsertCellPoint(70);
points14.SetPoint(71,19.2,9.03333, 0.1)
lines14.InsertCellPoint(71);
points14.SetPoint(72,19,9, 0.1)
lines14.InsertCellPoint(72);
points14.SetPoint(73,19,9, 0.1)
lines14.InsertCellPoint(73);
points14.SetPoint(74,19,8.8, 0.1)
lines14.InsertCellPoint(74);
points14.SetPoint(75,19,8.6, 0.1)
lines14.InsertCellPoint(75);
points14.SetPoint(76,19,8.4, 0.1)
lines14.InsertCellPoint(76);
points14.SetPoint(77,19,8.2, 0.1)
lines14.InsertCellPoint(77);
points14.SetPoint(78,19,8, 0.1)
lines14.InsertCellPoint(78);
points14.SetPoint(79,19,7.8, 0.1)
lines14.InsertCellPoint(79);
points14.SetPoint(80,19,7.6, 0.1)
lines14.InsertCellPoint(80);
points14.SetPoint(81,19,7.4, 0.1)
lines14.InsertCellPoint(81);
points14.SetPoint(82,19,7.2, 0.1)
lines14.InsertCellPoint(82);
points14.SetPoint(83,19,7, 0.1)
lines14.InsertCellPoint(83);
points14.SetPoint(84,19,6.8, 0.1)
lines14.InsertCellPoint(84);
points14.SetPoint(85,19,6.6, 0.1)
lines14.InsertCellPoint(85);
points14.SetPoint(86,19,6.4, 0.1)
lines14.InsertCellPoint(86);
points14.SetPoint(87,19,6.2, 0.1)
lines14.InsertCellPoint(87);
points14.SetPoint(88,19,6, 0.1)
lines14.InsertCellPoint(88);
points14.SetPoint(89,19,5.8, 0.1)
lines14.InsertCellPoint(89);
points14.SetPoint(90,19,5.6, 0.1)
lines14.InsertCellPoint(90);
points14.SetPoint(91,19,5.4, 0.1)
lines14.InsertCellPoint(91);
points14.SetPoint(92,19,5.2, 0.1)
lines14.InsertCellPoint(92);
points14.SetPoint(93,19,5, 0.1)
lines14.InsertCellPoint(93);
points14.SetPoint(94,19,5, 0.1)
lines14.InsertCellPoint(94);
points14.SetPoint(95,19.2,5, 0.1)
lines14.InsertCellPoint(95);
points14.SetPoint(96,19.4,5, 0.1)
lines14.InsertCellPoint(96);
points14.SetPoint(97,19.6,5, 0.1)
lines14.InsertCellPoint(97);
points14.SetPoint(98,19.8,5, 0.1)
lines14.InsertCellPoint(98);
points14.SetPoint(99,20,5, 0.1)
lines14.InsertCellPoint(99);
points14.SetPoint(100,20.2,5, 0.1)
lines14.InsertCellPoint(100);
points14.SetPoint(101,20.4,5, 0.1)
lines14.InsertCellPoint(101);
points14.SetPoint(102,20.6,5, 0.1)
lines14.InsertCellPoint(102);
points14.SetPoint(103,20.8,5, 0.1)
lines14.InsertCellPoint(103);
points14.SetPoint(104,21,5, 0.1)
lines14.InsertCellPoint(104);
points14.SetPoint(105,21.2,5, 0.1)
lines14.InsertCellPoint(105);
points14.SetPoint(106,21.4,5, 0.1)
lines14.InsertCellPoint(106);
points14.SetPoint(107,21.6,5, 0.1)
lines14.InsertCellPoint(107);
points14.SetPoint(108,21.8,5, 0.1)
lines14.InsertCellPoint(108);
points14.SetPoint(109,22,5, 0.1)
lines14.InsertCellPoint(109);
points14.SetPoint(110,22.2,5, 0.1)
lines14.InsertCellPoint(110);
points14.SetPoint(111,22.4,5, 0.1)
lines14.InsertCellPoint(111);
points14.SetPoint(112,22.6,5, 0.1)
lines14.InsertCellPoint(112);
points14.SetPoint(113,22.8,5, 0.1)
lines14.InsertCellPoint(113);
points14.SetPoint(114,23,5, 0.1)
lines14.InsertCellPoint(114);
points14.SetPoint(115,23.2,5, 0.1)
lines14.InsertCellPoint(115);
points14.SetPoint(116,23.4,5, 0.1)
lines14.InsertCellPoint(116);
points14.SetPoint(117,23.6,5, 0.1)
lines14.InsertCellPoint(117);
points14.SetPoint(118,23.8,5, 0.1)
lines14.InsertCellPoint(118);
points14.SetPoint(119,24,5, 0.1)
lines14.InsertCellPoint(119);
points14.SetPoint(120,24,5, 0.1)
lines14.InsertCellPoint(120);
points14.SetPoint(121,24,5.20833, 0.1)
lines14.InsertCellPoint(121);
points14.SetPoint(122,24,5.41667, 0.1)
lines14.InsertCellPoint(122);
points14.SetPoint(123,24,5.625, 0.1)
lines14.InsertCellPoint(123);
points14.SetPoint(124,24,5.83333, 0.1)
lines14.InsertCellPoint(124);
points14.SetPoint(125,24,6.04167, 0.1)
lines14.InsertCellPoint(125);
points14.SetPoint(126,24,6.25, 0.1)
lines14.InsertCellPoint(126);
points14.SetPoint(127,24,6.45833, 0.1)
lines14.InsertCellPoint(127);
points14.SetPoint(128,24,6.66667, 0.1)
lines14.InsertCellPoint(128);
points14.SetPoint(129,24,6.875, 0.1)
lines14.InsertCellPoint(129);
points14.SetPoint(130,24,7.08333, 0.1)
lines14.InsertCellPoint(130);
points14.SetPoint(131,24,7.29167, 0.1)
lines14.InsertCellPoint(131);
points14.SetPoint(132,24,7.5, 0.1)
lines14.InsertCellPoint(132);
points14.SetPoint(133,24,7.5, 0.1)
lines14.InsertCellPoint(133);
points14.SetPoint(134,24.1852,7.40741, 0.1)
lines14.InsertCellPoint(134);
points14.SetPoint(135,24.3704,7.31481, 0.1)
lines14.InsertCellPoint(135);
points14.SetPoint(136,24.5556,7.22222, 0.1)
lines14.InsertCellPoint(136);
points14.SetPoint(137,24.7407,7.12963, 0.1)
lines14.InsertCellPoint(137);
points14.SetPoint(138,24.9259,7.03704, 0.1)
lines14.InsertCellPoint(138);
points14.SetPoint(139,25.1111,6.94444, 0.1)
lines14.InsertCellPoint(139);
points14.SetPoint(140,25.2963,6.85185, 0.1)
lines14.InsertCellPoint(140);
points14.SetPoint(141,25.4815,6.75926, 0.1)
lines14.InsertCellPoint(141);
points14.SetPoint(142,25.6667,6.66667, 0.1)
lines14.InsertCellPoint(142);
points14.SetPoint(143,25.8519,6.57407, 0.1)
lines14.InsertCellPoint(143);
points14.SetPoint(144,26.037,6.48148, 0.1)
lines14.InsertCellPoint(144);
points14.SetPoint(145,26.2222,6.38889, 0.1)
lines14.InsertCellPoint(145);
points14.SetPoint(146,26.4074,6.2963, 0.1)
lines14.InsertCellPoint(146);
points14.SetPoint(147,26.5926,6.2037, 0.1)
lines14.InsertCellPoint(147);
points14.SetPoint(148,26.7778,6.11111, 0.1)
lines14.InsertCellPoint(148);
points14.SetPoint(149,26.963,6.01852, 0.1)
lines14.InsertCellPoint(149);
points14.SetPoint(150,27.1481,5.92593, 0.1)
lines14.InsertCellPoint(150);
points14.SetPoint(151,27.3333,5.83333, 0.1)
lines14.InsertCellPoint(151);
points14.SetPoint(152,27.5185,5.74074, 0.1)
lines14.InsertCellPoint(152);
points14.SetPoint(153,27.7037,5.64815, 0.1)
lines14.InsertCellPoint(153);
points14.SetPoint(154,27.8889,5.55556, 0.1)
lines14.InsertCellPoint(154);
points14.SetPoint(155,28.0741,5.46296, 0.1)
lines14.InsertCellPoint(155);
points14.SetPoint(156,28.2593,5.37037, 0.1)
lines14.InsertCellPoint(156);
points14.SetPoint(157,28.4444,5.27778, 0.1)
lines14.InsertCellPoint(157);
points14.SetPoint(158,28.6296,5.18519, 0.1)
lines14.InsertCellPoint(158);
points14.SetPoint(159,28.8148,5.09259, 0.1)
lines14.InsertCellPoint(159);
points14.SetPoint(160,29,5, 0.1)
lines14.InsertCellPoint(160);
lines14.InsertCellPoint(0);
polygon14 = vtk.vtkPolyData()
polygon14.SetPoints(points14)
polygon14.SetLines(lines14)
polygonMapper14 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper14.SetInputConnection(polygon14.GetProducerPort())
else:
	polygonMapper14.SetInputData(polygon14)
	polygonMapper14.Update()
polygonActor14 = vtk.vtkActor()
polygonActor14.SetMapper(polygonMapper14)
polygonActor14.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor14.GetProperty().SetLineWidth(widthLine)
points15 = vtk.vtkPoints()
points15.SetNumberOfPoints(188)
lines15 = vtk.vtkCellArray()
lines15.InsertNextCell(189)
points15.SetPoint(0,36,4, 0.1)
lines15.InsertCellPoint(0);
points15.SetPoint(1,35.9184,4.18367, 0.1)
lines15.InsertCellPoint(1);
points15.SetPoint(2,35.8367,4.36735, 0.1)
lines15.InsertCellPoint(2);
points15.SetPoint(3,35.7551,4.55102, 0.1)
lines15.InsertCellPoint(3);
points15.SetPoint(4,35.6735,4.73469, 0.1)
lines15.InsertCellPoint(4);
points15.SetPoint(5,35.5918,4.91837, 0.1)
lines15.InsertCellPoint(5);
points15.SetPoint(6,35.5102,5.10204, 0.1)
lines15.InsertCellPoint(6);
points15.SetPoint(7,35.4286,5.28571, 0.1)
lines15.InsertCellPoint(7);
points15.SetPoint(8,35.3469,5.46939, 0.1)
lines15.InsertCellPoint(8);
points15.SetPoint(9,35.2653,5.65306, 0.1)
lines15.InsertCellPoint(9);
points15.SetPoint(10,35.1837,5.83673, 0.1)
lines15.InsertCellPoint(10);
points15.SetPoint(11,35.102,6.02041, 0.1)
lines15.InsertCellPoint(11);
points15.SetPoint(12,35.0204,6.20408, 0.1)
lines15.InsertCellPoint(12);
points15.SetPoint(13,34.9388,6.38776, 0.1)
lines15.InsertCellPoint(13);
points15.SetPoint(14,34.8571,6.57143, 0.1)
lines15.InsertCellPoint(14);
points15.SetPoint(15,34.7755,6.7551, 0.1)
lines15.InsertCellPoint(15);
points15.SetPoint(16,34.6939,6.93878, 0.1)
lines15.InsertCellPoint(16);
points15.SetPoint(17,34.6122,7.12245, 0.1)
lines15.InsertCellPoint(17);
points15.SetPoint(18,34.5306,7.30612, 0.1)
lines15.InsertCellPoint(18);
points15.SetPoint(19,34.449,7.4898, 0.1)
lines15.InsertCellPoint(19);
points15.SetPoint(20,34.3673,7.67347, 0.1)
lines15.InsertCellPoint(20);
points15.SetPoint(21,34.2857,7.85714, 0.1)
lines15.InsertCellPoint(21);
points15.SetPoint(22,34.2041,8.04082, 0.1)
lines15.InsertCellPoint(22);
points15.SetPoint(23,34.1224,8.22449, 0.1)
lines15.InsertCellPoint(23);
points15.SetPoint(24,34.0408,8.40816, 0.1)
lines15.InsertCellPoint(24);
points15.SetPoint(25,33.9592,8.59184, 0.1)
lines15.InsertCellPoint(25);
points15.SetPoint(26,33.8776,8.77551, 0.1)
lines15.InsertCellPoint(26);
points15.SetPoint(27,33.7959,8.95918, 0.1)
lines15.InsertCellPoint(27);
points15.SetPoint(28,33.7143,9.14286, 0.1)
lines15.InsertCellPoint(28);
points15.SetPoint(29,33.6327,9.32653, 0.1)
lines15.InsertCellPoint(29);
points15.SetPoint(30,33.551,9.5102, 0.1)
lines15.InsertCellPoint(30);
points15.SetPoint(31,33.4694,9.69388, 0.1)
lines15.InsertCellPoint(31);
points15.SetPoint(32,33.3878,9.87755, 0.1)
lines15.InsertCellPoint(32);
points15.SetPoint(33,33.3061,10.0612, 0.1)
lines15.InsertCellPoint(33);
points15.SetPoint(34,33.2245,10.2449, 0.1)
lines15.InsertCellPoint(34);
points15.SetPoint(35,33.1429,10.4286, 0.1)
lines15.InsertCellPoint(35);
points15.SetPoint(36,33.0612,10.6122, 0.1)
lines15.InsertCellPoint(36);
points15.SetPoint(37,32.9796,10.7959, 0.1)
lines15.InsertCellPoint(37);
points15.SetPoint(38,32.898,10.9796, 0.1)
lines15.InsertCellPoint(38);
points15.SetPoint(39,32.8163,11.1633, 0.1)
lines15.InsertCellPoint(39);
points15.SetPoint(40,32.7347,11.3469, 0.1)
lines15.InsertCellPoint(40);
points15.SetPoint(41,32.6531,11.5306, 0.1)
lines15.InsertCellPoint(41);
points15.SetPoint(42,32.5714,11.7143, 0.1)
lines15.InsertCellPoint(42);
points15.SetPoint(43,32.4898,11.898, 0.1)
lines15.InsertCellPoint(43);
points15.SetPoint(44,32.4082,12.0816, 0.1)
lines15.InsertCellPoint(44);
points15.SetPoint(45,32.3265,12.2653, 0.1)
lines15.InsertCellPoint(45);
points15.SetPoint(46,32.2449,12.449, 0.1)
lines15.InsertCellPoint(46);
points15.SetPoint(47,32.1633,12.6327, 0.1)
lines15.InsertCellPoint(47);
points15.SetPoint(48,32.0816,12.8163, 0.1)
lines15.InsertCellPoint(48);
points15.SetPoint(49,32,13, 0.1)
lines15.InsertCellPoint(49);
points15.SetPoint(50,32,13, 0.1)
lines15.InsertCellPoint(50);
points15.SetPoint(51,31.8,13.0125, 0.1)
lines15.InsertCellPoint(51);
points15.SetPoint(52,31.6,13.025, 0.1)
lines15.InsertCellPoint(52);
points15.SetPoint(53,31.4,13.0375, 0.1)
lines15.InsertCellPoint(53);
points15.SetPoint(54,31.2,13.05, 0.1)
lines15.InsertCellPoint(54);
points15.SetPoint(55,31,13.0625, 0.1)
lines15.InsertCellPoint(55);
points15.SetPoint(56,30.8,13.075, 0.1)
lines15.InsertCellPoint(56);
points15.SetPoint(57,30.6,13.0875, 0.1)
lines15.InsertCellPoint(57);
points15.SetPoint(58,30.4,13.1, 0.1)
lines15.InsertCellPoint(58);
points15.SetPoint(59,30.2,13.1125, 0.1)
lines15.InsertCellPoint(59);
points15.SetPoint(60,30,13.125, 0.1)
lines15.InsertCellPoint(60);
points15.SetPoint(61,29.8,13.1375, 0.1)
lines15.InsertCellPoint(61);
points15.SetPoint(62,29.6,13.15, 0.1)
lines15.InsertCellPoint(62);
points15.SetPoint(63,29.4,13.1625, 0.1)
lines15.InsertCellPoint(63);
points15.SetPoint(64,29.2,13.175, 0.1)
lines15.InsertCellPoint(64);
points15.SetPoint(65,29,13.1875, 0.1)
lines15.InsertCellPoint(65);
points15.SetPoint(66,28.8,13.2, 0.1)
lines15.InsertCellPoint(66);
points15.SetPoint(67,28.6,13.2125, 0.1)
lines15.InsertCellPoint(67);
points15.SetPoint(68,28.4,13.225, 0.1)
lines15.InsertCellPoint(68);
points15.SetPoint(69,28.2,13.2375, 0.1)
lines15.InsertCellPoint(69);
points15.SetPoint(70,28,13.25, 0.1)
lines15.InsertCellPoint(70);
points15.SetPoint(71,27.8,13.2625, 0.1)
lines15.InsertCellPoint(71);
points15.SetPoint(72,27.6,13.275, 0.1)
lines15.InsertCellPoint(72);
points15.SetPoint(73,27.4,13.2875, 0.1)
lines15.InsertCellPoint(73);
points15.SetPoint(74,27.2,13.3, 0.1)
lines15.InsertCellPoint(74);
points15.SetPoint(75,27,13.3125, 0.1)
lines15.InsertCellPoint(75);
points15.SetPoint(76,26.8,13.325, 0.1)
lines15.InsertCellPoint(76);
points15.SetPoint(77,26.6,13.3375, 0.1)
lines15.InsertCellPoint(77);
points15.SetPoint(78,26.4,13.35, 0.1)
lines15.InsertCellPoint(78);
points15.SetPoint(79,26.2,13.3625, 0.1)
lines15.InsertCellPoint(79);
points15.SetPoint(80,26,13.375, 0.1)
lines15.InsertCellPoint(80);
points15.SetPoint(81,25.8,13.3875, 0.1)
lines15.InsertCellPoint(81);
points15.SetPoint(82,25.6,13.4, 0.1)
lines15.InsertCellPoint(82);
points15.SetPoint(83,25.4,13.4125, 0.1)
lines15.InsertCellPoint(83);
points15.SetPoint(84,25.2,13.425, 0.1)
lines15.InsertCellPoint(84);
points15.SetPoint(85,25,13.4375, 0.1)
lines15.InsertCellPoint(85);
points15.SetPoint(86,24.8,13.45, 0.1)
lines15.InsertCellPoint(86);
points15.SetPoint(87,24.6,13.4625, 0.1)
lines15.InsertCellPoint(87);
points15.SetPoint(88,24.4,13.475, 0.1)
lines15.InsertCellPoint(88);
points15.SetPoint(89,24.2,13.4875, 0.1)
lines15.InsertCellPoint(89);
points15.SetPoint(90,24,13.5, 0.1)
lines15.InsertCellPoint(90);
points15.SetPoint(91,24,13.5, 0.1)
lines15.InsertCellPoint(91);
points15.SetPoint(92,24.0556,13.3056, 0.1)
lines15.InsertCellPoint(92);
points15.SetPoint(93,24.1111,13.1111, 0.1)
lines15.InsertCellPoint(93);
points15.SetPoint(94,24.1667,12.9167, 0.1)
lines15.InsertCellPoint(94);
points15.SetPoint(95,24.2222,12.7222, 0.1)
lines15.InsertCellPoint(95);
points15.SetPoint(96,24.2778,12.5278, 0.1)
lines15.InsertCellPoint(96);
points15.SetPoint(97,24.3333,12.3333, 0.1)
lines15.InsertCellPoint(97);
points15.SetPoint(98,24.3889,12.1389, 0.1)
lines15.InsertCellPoint(98);
points15.SetPoint(99,24.4444,11.9444, 0.1)
lines15.InsertCellPoint(99);
points15.SetPoint(100,24.5,11.75, 0.1)
lines15.InsertCellPoint(100);
points15.SetPoint(101,24.5556,11.5556, 0.1)
lines15.InsertCellPoint(101);
points15.SetPoint(102,24.6111,11.3611, 0.1)
lines15.InsertCellPoint(102);
points15.SetPoint(103,24.6667,11.1667, 0.1)
lines15.InsertCellPoint(103);
points15.SetPoint(104,24.7222,10.9722, 0.1)
lines15.InsertCellPoint(104);
points15.SetPoint(105,24.7778,10.7778, 0.1)
lines15.InsertCellPoint(105);
points15.SetPoint(106,24.8333,10.5833, 0.1)
lines15.InsertCellPoint(106);
points15.SetPoint(107,24.8889,10.3889, 0.1)
lines15.InsertCellPoint(107);
points15.SetPoint(108,24.9444,10.1944, 0.1)
lines15.InsertCellPoint(108);
points15.SetPoint(109,25,10, 0.1)
lines15.InsertCellPoint(109);
points15.SetPoint(110,25,10, 0.1)
lines15.InsertCellPoint(110);
points15.SetPoint(111,25.2,9.95, 0.1)
lines15.InsertCellPoint(111);
points15.SetPoint(112,25.4,9.9, 0.1)
lines15.InsertCellPoint(112);
points15.SetPoint(113,25.6,9.85, 0.1)
lines15.InsertCellPoint(113);
points15.SetPoint(114,25.8,9.8, 0.1)
lines15.InsertCellPoint(114);
points15.SetPoint(115,26,9.75, 0.1)
lines15.InsertCellPoint(115);
points15.SetPoint(116,26.2,9.7, 0.1)
lines15.InsertCellPoint(116);
points15.SetPoint(117,26.4,9.65, 0.1)
lines15.InsertCellPoint(117);
points15.SetPoint(118,26.6,9.6, 0.1)
lines15.InsertCellPoint(118);
points15.SetPoint(119,26.8,9.55, 0.1)
lines15.InsertCellPoint(119);
points15.SetPoint(120,27,9.5, 0.1)
lines15.InsertCellPoint(120);
points15.SetPoint(121,27.2,9.45, 0.1)
lines15.InsertCellPoint(121);
points15.SetPoint(122,27.4,9.4, 0.1)
lines15.InsertCellPoint(122);
points15.SetPoint(123,27.6,9.35, 0.1)
lines15.InsertCellPoint(123);
points15.SetPoint(124,27.8,9.3, 0.1)
lines15.InsertCellPoint(124);
points15.SetPoint(125,28,9.25, 0.1)
lines15.InsertCellPoint(125);
points15.SetPoint(126,28.2,9.2, 0.1)
lines15.InsertCellPoint(126);
points15.SetPoint(127,28.4,9.15, 0.1)
lines15.InsertCellPoint(127);
points15.SetPoint(128,28.6,9.1, 0.1)
lines15.InsertCellPoint(128);
points15.SetPoint(129,28.8,9.05, 0.1)
lines15.InsertCellPoint(129);
points15.SetPoint(130,29,9, 0.1)
lines15.InsertCellPoint(130);
points15.SetPoint(131,29,9, 0.1)
lines15.InsertCellPoint(131);
points15.SetPoint(132,29,8.8, 0.1)
lines15.InsertCellPoint(132);
points15.SetPoint(133,29,8.6, 0.1)
lines15.InsertCellPoint(133);
points15.SetPoint(134,29,8.4, 0.1)
lines15.InsertCellPoint(134);
points15.SetPoint(135,29,8.2, 0.1)
lines15.InsertCellPoint(135);
points15.SetPoint(136,29,8, 0.1)
lines15.InsertCellPoint(136);
points15.SetPoint(137,29,7.8, 0.1)
lines15.InsertCellPoint(137);
points15.SetPoint(138,29,7.6, 0.1)
lines15.InsertCellPoint(138);
points15.SetPoint(139,29,7.4, 0.1)
lines15.InsertCellPoint(139);
points15.SetPoint(140,29,7.2, 0.1)
lines15.InsertCellPoint(140);
points15.SetPoint(141,29,7, 0.1)
lines15.InsertCellPoint(141);
points15.SetPoint(142,29,6.8, 0.1)
lines15.InsertCellPoint(142);
points15.SetPoint(143,29,6.6, 0.1)
lines15.InsertCellPoint(143);
points15.SetPoint(144,29,6.4, 0.1)
lines15.InsertCellPoint(144);
points15.SetPoint(145,29,6.2, 0.1)
lines15.InsertCellPoint(145);
points15.SetPoint(146,29,6, 0.1)
lines15.InsertCellPoint(146);
points15.SetPoint(147,29,5.8, 0.1)
lines15.InsertCellPoint(147);
points15.SetPoint(148,29,5.6, 0.1)
lines15.InsertCellPoint(148);
points15.SetPoint(149,29,5.4, 0.1)
lines15.InsertCellPoint(149);
points15.SetPoint(150,29,5.2, 0.1)
lines15.InsertCellPoint(150);
points15.SetPoint(151,29,5, 0.1)
lines15.InsertCellPoint(151);
points15.SetPoint(152,29,5, 0.1)
lines15.InsertCellPoint(152);
points15.SetPoint(153,29.2,4.97143, 0.1)
lines15.InsertCellPoint(153);
points15.SetPoint(154,29.4,4.94286, 0.1)
lines15.InsertCellPoint(154);
points15.SetPoint(155,29.6,4.91429, 0.1)
lines15.InsertCellPoint(155);
points15.SetPoint(156,29.8,4.88571, 0.1)
lines15.InsertCellPoint(156);
points15.SetPoint(157,30,4.85714, 0.1)
lines15.InsertCellPoint(157);
points15.SetPoint(158,30.2,4.82857, 0.1)
lines15.InsertCellPoint(158);
points15.SetPoint(159,30.4,4.8, 0.1)
lines15.InsertCellPoint(159);
points15.SetPoint(160,30.6,4.77143, 0.1)
lines15.InsertCellPoint(160);
points15.SetPoint(161,30.8,4.74286, 0.1)
lines15.InsertCellPoint(161);
points15.SetPoint(162,31,4.71429, 0.1)
lines15.InsertCellPoint(162);
points15.SetPoint(163,31.2,4.68571, 0.1)
lines15.InsertCellPoint(163);
points15.SetPoint(164,31.4,4.65714, 0.1)
lines15.InsertCellPoint(164);
points15.SetPoint(165,31.6,4.62857, 0.1)
lines15.InsertCellPoint(165);
points15.SetPoint(166,31.8,4.6, 0.1)
lines15.InsertCellPoint(166);
points15.SetPoint(167,32,4.57143, 0.1)
lines15.InsertCellPoint(167);
points15.SetPoint(168,32.2,4.54286, 0.1)
lines15.InsertCellPoint(168);
points15.SetPoint(169,32.4,4.51429, 0.1)
lines15.InsertCellPoint(169);
points15.SetPoint(170,32.6,4.48571, 0.1)
lines15.InsertCellPoint(170);
points15.SetPoint(171,32.8,4.45714, 0.1)
lines15.InsertCellPoint(171);
points15.SetPoint(172,33,4.42857, 0.1)
lines15.InsertCellPoint(172);
points15.SetPoint(173,33.2,4.4, 0.1)
lines15.InsertCellPoint(173);
points15.SetPoint(174,33.4,4.37143, 0.1)
lines15.InsertCellPoint(174);
points15.SetPoint(175,33.6,4.34286, 0.1)
lines15.InsertCellPoint(175);
points15.SetPoint(176,33.8,4.31429, 0.1)
lines15.InsertCellPoint(176);
points15.SetPoint(177,34,4.28571, 0.1)
lines15.InsertCellPoint(177);
points15.SetPoint(178,34.2,4.25714, 0.1)
lines15.InsertCellPoint(178);
points15.SetPoint(179,34.4,4.22857, 0.1)
lines15.InsertCellPoint(179);
points15.SetPoint(180,34.6,4.2, 0.1)
lines15.InsertCellPoint(180);
points15.SetPoint(181,34.8,4.17143, 0.1)
lines15.InsertCellPoint(181);
points15.SetPoint(182,35,4.14286, 0.1)
lines15.InsertCellPoint(182);
points15.SetPoint(183,35.2,4.11429, 0.1)
lines15.InsertCellPoint(183);
points15.SetPoint(184,35.4,4.08571, 0.1)
lines15.InsertCellPoint(184);
points15.SetPoint(185,35.6,4.05714, 0.1)
lines15.InsertCellPoint(185);
points15.SetPoint(186,35.8,4.02857, 0.1)
lines15.InsertCellPoint(186);
points15.SetPoint(187,36,4, 0.1)
lines15.InsertCellPoint(187);
lines15.InsertCellPoint(0);
polygon15 = vtk.vtkPolyData()
polygon15.SetPoints(points15)
polygon15.SetLines(lines15)
polygonMapper15 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper15.SetInputConnection(polygon15.GetProducerPort())
else:
	polygonMapper15.SetInputData(polygon15)
	polygonMapper15.Update()
polygonActor15 = vtk.vtkActor()
polygonActor15.SetMapper(polygonMapper15)
polygonActor15.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor15.GetProperty().SetLineWidth(widthLine)
points16 = vtk.vtkPoints()
points16.SetNumberOfPoints(84)
lines16 = vtk.vtkCellArray()
lines16.InsertNextCell(85)
points16.SetPoint(0,24,13.5, 0.1)
lines16.InsertCellPoint(0);
points16.SetPoint(1,23.8485,13.3636, 0.1)
lines16.InsertCellPoint(1);
points16.SetPoint(2,23.697,13.2273, 0.1)
lines16.InsertCellPoint(2);
points16.SetPoint(3,23.5455,13.0909, 0.1)
lines16.InsertCellPoint(3);
points16.SetPoint(4,23.3939,12.9545, 0.1)
lines16.InsertCellPoint(4);
points16.SetPoint(5,23.2424,12.8182, 0.1)
lines16.InsertCellPoint(5);
points16.SetPoint(6,23.0909,12.6818, 0.1)
lines16.InsertCellPoint(6);
points16.SetPoint(7,22.9394,12.5455, 0.1)
lines16.InsertCellPoint(7);
points16.SetPoint(8,22.7879,12.4091, 0.1)
lines16.InsertCellPoint(8);
points16.SetPoint(9,22.6364,12.2727, 0.1)
lines16.InsertCellPoint(9);
points16.SetPoint(10,22.4848,12.1364, 0.1)
lines16.InsertCellPoint(10);
points16.SetPoint(11,22.3333,12, 0.1)
lines16.InsertCellPoint(11);
points16.SetPoint(12,22.1818,11.8636, 0.1)
lines16.InsertCellPoint(12);
points16.SetPoint(13,22.0303,11.7273, 0.1)
lines16.InsertCellPoint(13);
points16.SetPoint(14,21.8788,11.5909, 0.1)
lines16.InsertCellPoint(14);
points16.SetPoint(15,21.7273,11.4545, 0.1)
lines16.InsertCellPoint(15);
points16.SetPoint(16,21.5758,11.3182, 0.1)
lines16.InsertCellPoint(16);
points16.SetPoint(17,21.4242,11.1818, 0.1)
lines16.InsertCellPoint(17);
points16.SetPoint(18,21.2727,11.0455, 0.1)
lines16.InsertCellPoint(18);
points16.SetPoint(19,21.1212,10.9091, 0.1)
lines16.InsertCellPoint(19);
points16.SetPoint(20,20.9697,10.7727, 0.1)
lines16.InsertCellPoint(20);
points16.SetPoint(21,20.8182,10.6364, 0.1)
lines16.InsertCellPoint(21);
points16.SetPoint(22,20.6667,10.5, 0.1)
lines16.InsertCellPoint(22);
points16.SetPoint(23,20.5152,10.3636, 0.1)
lines16.InsertCellPoint(23);
points16.SetPoint(24,20.3636,10.2273, 0.1)
lines16.InsertCellPoint(24);
points16.SetPoint(25,20.2121,10.0909, 0.1)
lines16.InsertCellPoint(25);
points16.SetPoint(26,20.0606,9.95455, 0.1)
lines16.InsertCellPoint(26);
points16.SetPoint(27,19.9091,9.81818, 0.1)
lines16.InsertCellPoint(27);
points16.SetPoint(28,19.7576,9.68182, 0.1)
lines16.InsertCellPoint(28);
points16.SetPoint(29,19.6061,9.54545, 0.1)
lines16.InsertCellPoint(29);
points16.SetPoint(30,19.4545,9.40909, 0.1)
lines16.InsertCellPoint(30);
points16.SetPoint(31,19.303,9.27273, 0.1)
lines16.InsertCellPoint(31);
points16.SetPoint(32,19.1515,9.13636, 0.1)
lines16.InsertCellPoint(32);
points16.SetPoint(33,19,9, 0.1)
lines16.InsertCellPoint(33);
points16.SetPoint(34,19,9, 0.1)
lines16.InsertCellPoint(34);
points16.SetPoint(35,19.2,9.03333, 0.1)
lines16.InsertCellPoint(35);
points16.SetPoint(36,19.4,9.06667, 0.1)
lines16.InsertCellPoint(36);
points16.SetPoint(37,19.6,9.1, 0.1)
lines16.InsertCellPoint(37);
points16.SetPoint(38,19.8,9.13333, 0.1)
lines16.InsertCellPoint(38);
points16.SetPoint(39,20,9.16667, 0.1)
lines16.InsertCellPoint(39);
points16.SetPoint(40,20.2,9.2, 0.1)
lines16.InsertCellPoint(40);
points16.SetPoint(41,20.4,9.23333, 0.1)
lines16.InsertCellPoint(41);
points16.SetPoint(42,20.6,9.26667, 0.1)
lines16.InsertCellPoint(42);
points16.SetPoint(43,20.8,9.3, 0.1)
lines16.InsertCellPoint(43);
points16.SetPoint(44,21,9.33333, 0.1)
lines16.InsertCellPoint(44);
points16.SetPoint(45,21.2,9.36667, 0.1)
lines16.InsertCellPoint(45);
points16.SetPoint(46,21.4,9.4, 0.1)
lines16.InsertCellPoint(46);
points16.SetPoint(47,21.6,9.43333, 0.1)
lines16.InsertCellPoint(47);
points16.SetPoint(48,21.8,9.46667, 0.1)
lines16.InsertCellPoint(48);
points16.SetPoint(49,22,9.5, 0.1)
lines16.InsertCellPoint(49);
points16.SetPoint(50,22.2,9.53333, 0.1)
lines16.InsertCellPoint(50);
points16.SetPoint(51,22.4,9.56667, 0.1)
lines16.InsertCellPoint(51);
points16.SetPoint(52,22.6,9.6, 0.1)
lines16.InsertCellPoint(52);
points16.SetPoint(53,22.8,9.63333, 0.1)
lines16.InsertCellPoint(53);
points16.SetPoint(54,23,9.66667, 0.1)
lines16.InsertCellPoint(54);
points16.SetPoint(55,23.2,9.7, 0.1)
lines16.InsertCellPoint(55);
points16.SetPoint(56,23.4,9.73333, 0.1)
lines16.InsertCellPoint(56);
points16.SetPoint(57,23.6,9.76667, 0.1)
lines16.InsertCellPoint(57);
points16.SetPoint(58,23.8,9.8, 0.1)
lines16.InsertCellPoint(58);
points16.SetPoint(59,24,9.83333, 0.1)
lines16.InsertCellPoint(59);
points16.SetPoint(60,24.2,9.86667, 0.1)
lines16.InsertCellPoint(60);
points16.SetPoint(61,24.4,9.9, 0.1)
lines16.InsertCellPoint(61);
points16.SetPoint(62,24.6,9.93333, 0.1)
lines16.InsertCellPoint(62);
points16.SetPoint(63,24.8,9.96667, 0.1)
lines16.InsertCellPoint(63);
points16.SetPoint(64,25,10, 0.1)
lines16.InsertCellPoint(64);
points16.SetPoint(65,25,10, 0.1)
lines16.InsertCellPoint(65);
points16.SetPoint(66,24.9444,10.1944, 0.1)
lines16.InsertCellPoint(66);
points16.SetPoint(67,24.8889,10.3889, 0.1)
lines16.InsertCellPoint(67);
points16.SetPoint(68,24.8333,10.5833, 0.1)
lines16.InsertCellPoint(68);
points16.SetPoint(69,24.7778,10.7778, 0.1)
lines16.InsertCellPoint(69);
points16.SetPoint(70,24.7222,10.9722, 0.1)
lines16.InsertCellPoint(70);
points16.SetPoint(71,24.6667,11.1667, 0.1)
lines16.InsertCellPoint(71);
points16.SetPoint(72,24.6111,11.3611, 0.1)
lines16.InsertCellPoint(72);
points16.SetPoint(73,24.5556,11.5556, 0.1)
lines16.InsertCellPoint(73);
points16.SetPoint(74,24.5,11.75, 0.1)
lines16.InsertCellPoint(74);
points16.SetPoint(75,24.4444,11.9444, 0.1)
lines16.InsertCellPoint(75);
points16.SetPoint(76,24.3889,12.1389, 0.1)
lines16.InsertCellPoint(76);
points16.SetPoint(77,24.3333,12.3333, 0.1)
lines16.InsertCellPoint(77);
points16.SetPoint(78,24.2778,12.5278, 0.1)
lines16.InsertCellPoint(78);
points16.SetPoint(79,24.2222,12.7222, 0.1)
lines16.InsertCellPoint(79);
points16.SetPoint(80,24.1667,12.9167, 0.1)
lines16.InsertCellPoint(80);
points16.SetPoint(81,24.1111,13.1111, 0.1)
lines16.InsertCellPoint(81);
points16.SetPoint(82,24.0556,13.3056, 0.1)
lines16.InsertCellPoint(82);
points16.SetPoint(83,24,13.5, 0.1)
lines16.InsertCellPoint(83);
lines16.InsertCellPoint(0);
polygon16 = vtk.vtkPolyData()
polygon16.SetPoints(points16)
polygon16.SetLines(lines16)
polygonMapper16 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper16.SetInputConnection(polygon16.GetProducerPort())
else:
	polygonMapper16.SetInputData(polygon16)
	polygonMapper16.Update()
polygonActor16 = vtk.vtkActor()
polygonActor16.SetMapper(polygonMapper16)
polygonActor16.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor16.GetProperty().SetLineWidth(widthLine)
points17 = vtk.vtkPoints()
points17.SetNumberOfPoints(277)
lines17 = vtk.vtkCellArray()
lines17.InsertNextCell(278)
points17.SetPoint(0,24,13.5, 0.1)
lines17.InsertCellPoint(0);
points17.SetPoint(1,23.8,13.5333, 0.1)
lines17.InsertCellPoint(1);
points17.SetPoint(2,23.6,13.5667, 0.1)
lines17.InsertCellPoint(2);
points17.SetPoint(3,23.4,13.6, 0.1)
lines17.InsertCellPoint(3);
points17.SetPoint(4,23.2,13.6333, 0.1)
lines17.InsertCellPoint(4);
points17.SetPoint(5,23,13.6667, 0.1)
lines17.InsertCellPoint(5);
points17.SetPoint(6,22.8,13.7, 0.1)
lines17.InsertCellPoint(6);
points17.SetPoint(7,22.6,13.7333, 0.1)
lines17.InsertCellPoint(7);
points17.SetPoint(8,22.4,13.7667, 0.1)
lines17.InsertCellPoint(8);
points17.SetPoint(9,22.2,13.8, 0.1)
lines17.InsertCellPoint(9);
points17.SetPoint(10,22,13.8333, 0.1)
lines17.InsertCellPoint(10);
points17.SetPoint(11,21.8,13.8667, 0.1)
lines17.InsertCellPoint(11);
points17.SetPoint(12,21.6,13.9, 0.1)
lines17.InsertCellPoint(12);
points17.SetPoint(13,21.4,13.9333, 0.1)
lines17.InsertCellPoint(13);
points17.SetPoint(14,21.2,13.9667, 0.1)
lines17.InsertCellPoint(14);
points17.SetPoint(15,21,14, 0.1)
lines17.InsertCellPoint(15);
points17.SetPoint(16,20.8,14.0333, 0.1)
lines17.InsertCellPoint(16);
points17.SetPoint(17,20.6,14.0667, 0.1)
lines17.InsertCellPoint(17);
points17.SetPoint(18,20.4,14.1, 0.1)
lines17.InsertCellPoint(18);
points17.SetPoint(19,20.2,14.1333, 0.1)
lines17.InsertCellPoint(19);
points17.SetPoint(20,20,14.1667, 0.1)
lines17.InsertCellPoint(20);
points17.SetPoint(21,19.8,14.2, 0.1)
lines17.InsertCellPoint(21);
points17.SetPoint(22,19.6,14.2333, 0.1)
lines17.InsertCellPoint(22);
points17.SetPoint(23,19.4,14.2667, 0.1)
lines17.InsertCellPoint(23);
points17.SetPoint(24,19.2,14.3, 0.1)
lines17.InsertCellPoint(24);
points17.SetPoint(25,19,14.3333, 0.1)
lines17.InsertCellPoint(25);
points17.SetPoint(26,18.8,14.3667, 0.1)
lines17.InsertCellPoint(26);
points17.SetPoint(27,18.6,14.4, 0.1)
lines17.InsertCellPoint(27);
points17.SetPoint(28,18.4,14.4333, 0.1)
lines17.InsertCellPoint(28);
points17.SetPoint(29,18.2,14.4667, 0.1)
lines17.InsertCellPoint(29);
points17.SetPoint(30,18,14.5, 0.1)
lines17.InsertCellPoint(30);
points17.SetPoint(31,17.8,14.5333, 0.1)
lines17.InsertCellPoint(31);
points17.SetPoint(32,17.6,14.5667, 0.1)
lines17.InsertCellPoint(32);
points17.SetPoint(33,17.4,14.6, 0.1)
lines17.InsertCellPoint(33);
points17.SetPoint(34,17.2,14.6333, 0.1)
lines17.InsertCellPoint(34);
points17.SetPoint(35,17,14.6667, 0.1)
lines17.InsertCellPoint(35);
points17.SetPoint(36,16.8,14.7, 0.1)
lines17.InsertCellPoint(36);
points17.SetPoint(37,16.6,14.7333, 0.1)
lines17.InsertCellPoint(37);
points17.SetPoint(38,16.4,14.7667, 0.1)
lines17.InsertCellPoint(38);
points17.SetPoint(39,16.2,14.8, 0.1)
lines17.InsertCellPoint(39);
points17.SetPoint(40,16,14.8333, 0.1)
lines17.InsertCellPoint(40);
points17.SetPoint(41,15.8,14.8667, 0.1)
lines17.InsertCellPoint(41);
points17.SetPoint(42,15.6,14.9, 0.1)
lines17.InsertCellPoint(42);
points17.SetPoint(43,15.4,14.9333, 0.1)
lines17.InsertCellPoint(43);
points17.SetPoint(44,15.2,14.9667, 0.1)
lines17.InsertCellPoint(44);
points17.SetPoint(45,15,15, 0.1)
lines17.InsertCellPoint(45);
points17.SetPoint(46,15,15, 0.1)
lines17.InsertCellPoint(46);
points17.SetPoint(47,15.04,14.8, 0.1)
lines17.InsertCellPoint(47);
points17.SetPoint(48,15.08,14.6, 0.1)
lines17.InsertCellPoint(48);
points17.SetPoint(49,15.12,14.4, 0.1)
lines17.InsertCellPoint(49);
points17.SetPoint(50,15.16,14.2, 0.1)
lines17.InsertCellPoint(50);
points17.SetPoint(51,15.2,14, 0.1)
lines17.InsertCellPoint(51);
points17.SetPoint(52,15.24,13.8, 0.1)
lines17.InsertCellPoint(52);
points17.SetPoint(53,15.28,13.6, 0.1)
lines17.InsertCellPoint(53);
points17.SetPoint(54,15.32,13.4, 0.1)
lines17.InsertCellPoint(54);
points17.SetPoint(55,15.36,13.2, 0.1)
lines17.InsertCellPoint(55);
points17.SetPoint(56,15.4,13, 0.1)
lines17.InsertCellPoint(56);
points17.SetPoint(57,15.44,12.8, 0.1)
lines17.InsertCellPoint(57);
points17.SetPoint(58,15.48,12.6, 0.1)
lines17.InsertCellPoint(58);
points17.SetPoint(59,15.52,12.4, 0.1)
lines17.InsertCellPoint(59);
points17.SetPoint(60,15.56,12.2, 0.1)
lines17.InsertCellPoint(60);
points17.SetPoint(61,15.6,12, 0.1)
lines17.InsertCellPoint(61);
points17.SetPoint(62,15.64,11.8, 0.1)
lines17.InsertCellPoint(62);
points17.SetPoint(63,15.68,11.6, 0.1)
lines17.InsertCellPoint(63);
points17.SetPoint(64,15.72,11.4, 0.1)
lines17.InsertCellPoint(64);
points17.SetPoint(65,15.76,11.2, 0.1)
lines17.InsertCellPoint(65);
points17.SetPoint(66,15.8,11, 0.1)
lines17.InsertCellPoint(66);
points17.SetPoint(67,15.84,10.8, 0.1)
lines17.InsertCellPoint(67);
points17.SetPoint(68,15.88,10.6, 0.1)
lines17.InsertCellPoint(68);
points17.SetPoint(69,15.92,10.4, 0.1)
lines17.InsertCellPoint(69);
points17.SetPoint(70,15.96,10.2, 0.1)
lines17.InsertCellPoint(70);
points17.SetPoint(71,16,10, 0.1)
lines17.InsertCellPoint(71);
points17.SetPoint(72,16.04,9.8, 0.1)
lines17.InsertCellPoint(72);
points17.SetPoint(73,16.08,9.6, 0.1)
lines17.InsertCellPoint(73);
points17.SetPoint(74,16.12,9.4, 0.1)
lines17.InsertCellPoint(74);
points17.SetPoint(75,16.16,9.2, 0.1)
lines17.InsertCellPoint(75);
points17.SetPoint(76,16.2,9, 0.1)
lines17.InsertCellPoint(76);
points17.SetPoint(77,16.24,8.8, 0.1)
lines17.InsertCellPoint(77);
points17.SetPoint(78,16.28,8.6, 0.1)
lines17.InsertCellPoint(78);
points17.SetPoint(79,16.32,8.4, 0.1)
lines17.InsertCellPoint(79);
points17.SetPoint(80,16.36,8.2, 0.1)
lines17.InsertCellPoint(80);
points17.SetPoint(81,16.4,8, 0.1)
lines17.InsertCellPoint(81);
points17.SetPoint(82,16.44,7.8, 0.1)
lines17.InsertCellPoint(82);
points17.SetPoint(83,16.48,7.6, 0.1)
lines17.InsertCellPoint(83);
points17.SetPoint(84,16.52,7.4, 0.1)
lines17.InsertCellPoint(84);
points17.SetPoint(85,16.56,7.2, 0.1)
lines17.InsertCellPoint(85);
points17.SetPoint(86,16.6,7, 0.1)
lines17.InsertCellPoint(86);
points17.SetPoint(87,16.64,6.8, 0.1)
lines17.InsertCellPoint(87);
points17.SetPoint(88,16.68,6.6, 0.1)
lines17.InsertCellPoint(88);
points17.SetPoint(89,16.72,6.4, 0.1)
lines17.InsertCellPoint(89);
points17.SetPoint(90,16.76,6.2, 0.1)
lines17.InsertCellPoint(90);
points17.SetPoint(91,16.8,6, 0.1)
lines17.InsertCellPoint(91);
points17.SetPoint(92,16.84,5.8, 0.1)
lines17.InsertCellPoint(92);
points17.SetPoint(93,16.88,5.6, 0.1)
lines17.InsertCellPoint(93);
points17.SetPoint(94,16.92,5.4, 0.1)
lines17.InsertCellPoint(94);
points17.SetPoint(95,16.96,5.2, 0.1)
lines17.InsertCellPoint(95);
points17.SetPoint(96,17,5, 0.1)
lines17.InsertCellPoint(96);
points17.SetPoint(97,17,5, 0.1)
lines17.InsertCellPoint(97);
points17.SetPoint(98,17.0185,4.7963, 0.1)
lines17.InsertCellPoint(98);
points17.SetPoint(99,17.037,4.59259, 0.1)
lines17.InsertCellPoint(99);
points17.SetPoint(100,17.0556,4.38889, 0.1)
lines17.InsertCellPoint(100);
points17.SetPoint(101,17.0741,4.18519, 0.1)
lines17.InsertCellPoint(101);
points17.SetPoint(102,17.0926,3.98148, 0.1)
lines17.InsertCellPoint(102);
points17.SetPoint(103,17.1111,3.77778, 0.1)
lines17.InsertCellPoint(103);
points17.SetPoint(104,17.1296,3.57407, 0.1)
lines17.InsertCellPoint(104);
points17.SetPoint(105,17.1481,3.37037, 0.1)
lines17.InsertCellPoint(105);
points17.SetPoint(106,17.1667,3.16667, 0.1)
lines17.InsertCellPoint(106);
points17.SetPoint(107,17.1852,2.96296, 0.1)
lines17.InsertCellPoint(107);
points17.SetPoint(108,17.2037,2.75926, 0.1)
lines17.InsertCellPoint(108);
points17.SetPoint(109,17.2222,2.55556, 0.1)
lines17.InsertCellPoint(109);
points17.SetPoint(110,17.2407,2.35185, 0.1)
lines17.InsertCellPoint(110);
points17.SetPoint(111,17.2593,2.14815, 0.1)
lines17.InsertCellPoint(111);
points17.SetPoint(112,17.2778,1.94444, 0.1)
lines17.InsertCellPoint(112);
points17.SetPoint(113,17.2963,1.74074, 0.1)
lines17.InsertCellPoint(113);
points17.SetPoint(114,17.3148,1.53704, 0.1)
lines17.InsertCellPoint(114);
points17.SetPoint(115,17.3333,1.33333, 0.1)
lines17.InsertCellPoint(115);
points17.SetPoint(116,17.3519,1.12963, 0.1)
lines17.InsertCellPoint(116);
points17.SetPoint(117,17.3704,0.925926, 0.1)
lines17.InsertCellPoint(117);
points17.SetPoint(118,17.3889,0.722222, 0.1)
lines17.InsertCellPoint(118);
points17.SetPoint(119,17.4074,0.518519, 0.1)
lines17.InsertCellPoint(119);
points17.SetPoint(120,17.4259,0.314815, 0.1)
lines17.InsertCellPoint(120);
points17.SetPoint(121,17.4444,0.111111, 0.1)
lines17.InsertCellPoint(121);
points17.SetPoint(122,17.463,-0.0925926, 0.1)
lines17.InsertCellPoint(122);
points17.SetPoint(123,17.4815,-0.296296, 0.1)
lines17.InsertCellPoint(123);
points17.SetPoint(124,17.5,-0.5, 0.1)
lines17.InsertCellPoint(124);
points17.SetPoint(125,17.5,-0.5, 0.1)
lines17.InsertCellPoint(125);
points17.SetPoint(126,17.4464,-0.696429, 0.1)
lines17.InsertCellPoint(126);
points17.SetPoint(127,17.3929,-0.892857, 0.1)
lines17.InsertCellPoint(127);
points17.SetPoint(128,17.3393,-1.08929, 0.1)
lines17.InsertCellPoint(128);
points17.SetPoint(129,17.2857,-1.28571, 0.1)
lines17.InsertCellPoint(129);
points17.SetPoint(130,17.2321,-1.48214, 0.1)
lines17.InsertCellPoint(130);
points17.SetPoint(131,17.1786,-1.67857, 0.1)
lines17.InsertCellPoint(131);
points17.SetPoint(132,17.125,-1.875, 0.1)
lines17.InsertCellPoint(132);
points17.SetPoint(133,17.0714,-2.07143, 0.1)
lines17.InsertCellPoint(133);
points17.SetPoint(134,17.0179,-2.26786, 0.1)
lines17.InsertCellPoint(134);
points17.SetPoint(135,16.9643,-2.46429, 0.1)
lines17.InsertCellPoint(135);
points17.SetPoint(136,16.9107,-2.66071, 0.1)
lines17.InsertCellPoint(136);
points17.SetPoint(137,16.8571,-2.85714, 0.1)
lines17.InsertCellPoint(137);
points17.SetPoint(138,16.8036,-3.05357, 0.1)
lines17.InsertCellPoint(138);
points17.SetPoint(139,16.75,-3.25, 0.1)
lines17.InsertCellPoint(139);
points17.SetPoint(140,16.6964,-3.44643, 0.1)
lines17.InsertCellPoint(140);
points17.SetPoint(141,16.6429,-3.64286, 0.1)
lines17.InsertCellPoint(141);
points17.SetPoint(142,16.5893,-3.83929, 0.1)
lines17.InsertCellPoint(142);
points17.SetPoint(143,16.5357,-4.03571, 0.1)
lines17.InsertCellPoint(143);
points17.SetPoint(144,16.4821,-4.23214, 0.1)
lines17.InsertCellPoint(144);
points17.SetPoint(145,16.4286,-4.42857, 0.1)
lines17.InsertCellPoint(145);
points17.SetPoint(146,16.375,-4.625, 0.1)
lines17.InsertCellPoint(146);
points17.SetPoint(147,16.3214,-4.82143, 0.1)
lines17.InsertCellPoint(147);
points17.SetPoint(148,16.2679,-5.01786, 0.1)
lines17.InsertCellPoint(148);
points17.SetPoint(149,16.2143,-5.21429, 0.1)
lines17.InsertCellPoint(149);
points17.SetPoint(150,16.1607,-5.41071, 0.1)
lines17.InsertCellPoint(150);
points17.SetPoint(151,16.1071,-5.60714, 0.1)
lines17.InsertCellPoint(151);
points17.SetPoint(152,16.0536,-5.80357, 0.1)
lines17.InsertCellPoint(152);
points17.SetPoint(153,16,-6, 0.1)
lines17.InsertCellPoint(153);
points17.SetPoint(154,16,-6, 0.1)
lines17.InsertCellPoint(154);
points17.SetPoint(155,16.2,-5.93333, 0.1)
lines17.InsertCellPoint(155);
points17.SetPoint(156,16.4,-5.86667, 0.1)
lines17.InsertCellPoint(156);
points17.SetPoint(157,16.6,-5.8, 0.1)
lines17.InsertCellPoint(157);
points17.SetPoint(158,16.8,-5.73333, 0.1)
lines17.InsertCellPoint(158);
points17.SetPoint(159,17,-5.66667, 0.1)
lines17.InsertCellPoint(159);
points17.SetPoint(160,17.2,-5.6, 0.1)
lines17.InsertCellPoint(160);
points17.SetPoint(161,17.4,-5.53333, 0.1)
lines17.InsertCellPoint(161);
points17.SetPoint(162,17.6,-5.46667, 0.1)
lines17.InsertCellPoint(162);
points17.SetPoint(163,17.8,-5.4, 0.1)
lines17.InsertCellPoint(163);
points17.SetPoint(164,18,-5.33333, 0.1)
lines17.InsertCellPoint(164);
points17.SetPoint(165,18.2,-5.26667, 0.1)
lines17.InsertCellPoint(165);
points17.SetPoint(166,18.4,-5.2, 0.1)
lines17.InsertCellPoint(166);
points17.SetPoint(167,18.6,-5.13333, 0.1)
lines17.InsertCellPoint(167);
points17.SetPoint(168,18.8,-5.06667, 0.1)
lines17.InsertCellPoint(168);
points17.SetPoint(169,19,-5, 0.1)
lines17.InsertCellPoint(169);
points17.SetPoint(170,19,-5, 0.1)
lines17.InsertCellPoint(170);
points17.SetPoint(171,19,-4.8, 0.1)
lines17.InsertCellPoint(171);
points17.SetPoint(172,19,-4.6, 0.1)
lines17.InsertCellPoint(172);
points17.SetPoint(173,19,-4.4, 0.1)
lines17.InsertCellPoint(173);
points17.SetPoint(174,19,-4.2, 0.1)
lines17.InsertCellPoint(174);
points17.SetPoint(175,19,-4, 0.1)
lines17.InsertCellPoint(175);
points17.SetPoint(176,19,-3.8, 0.1)
lines17.InsertCellPoint(176);
points17.SetPoint(177,19,-3.6, 0.1)
lines17.InsertCellPoint(177);
points17.SetPoint(178,19,-3.4, 0.1)
lines17.InsertCellPoint(178);
points17.SetPoint(179,19,-3.2, 0.1)
lines17.InsertCellPoint(179);
points17.SetPoint(180,19,-3, 0.1)
lines17.InsertCellPoint(180);
points17.SetPoint(181,19,-2.8, 0.1)
lines17.InsertCellPoint(181);
points17.SetPoint(182,19,-2.6, 0.1)
lines17.InsertCellPoint(182);
points17.SetPoint(183,19,-2.4, 0.1)
lines17.InsertCellPoint(183);
points17.SetPoint(184,19,-2.2, 0.1)
lines17.InsertCellPoint(184);
points17.SetPoint(185,19,-2, 0.1)
lines17.InsertCellPoint(185);
points17.SetPoint(186,19,-1.8, 0.1)
lines17.InsertCellPoint(186);
points17.SetPoint(187,19,-1.6, 0.1)
lines17.InsertCellPoint(187);
points17.SetPoint(188,19,-1.4, 0.1)
lines17.InsertCellPoint(188);
points17.SetPoint(189,19,-1.2, 0.1)
lines17.InsertCellPoint(189);
points17.SetPoint(190,19,-1, 0.1)
lines17.InsertCellPoint(190);
points17.SetPoint(191,19,-0.8, 0.1)
lines17.InsertCellPoint(191);
points17.SetPoint(192,19,-0.6, 0.1)
lines17.InsertCellPoint(192);
points17.SetPoint(193,19,-0.4, 0.1)
lines17.InsertCellPoint(193);
points17.SetPoint(194,19,-0.2, 0.1)
lines17.InsertCellPoint(194);
points17.SetPoint(195,19,0, 0.1)
lines17.InsertCellPoint(195);
points17.SetPoint(196,19,0, 0.1)
lines17.InsertCellPoint(196);
points17.SetPoint(197,19,0.2, 0.1)
lines17.InsertCellPoint(197);
points17.SetPoint(198,19,0.4, 0.1)
lines17.InsertCellPoint(198);
points17.SetPoint(199,19,0.6, 0.1)
lines17.InsertCellPoint(199);
points17.SetPoint(200,19,0.8, 0.1)
lines17.InsertCellPoint(200);
points17.SetPoint(201,19,1, 0.1)
lines17.InsertCellPoint(201);
points17.SetPoint(202,19,1.2, 0.1)
lines17.InsertCellPoint(202);
points17.SetPoint(203,19,1.4, 0.1)
lines17.InsertCellPoint(203);
points17.SetPoint(204,19,1.6, 0.1)
lines17.InsertCellPoint(204);
points17.SetPoint(205,19,1.8, 0.1)
lines17.InsertCellPoint(205);
points17.SetPoint(206,19,2, 0.1)
lines17.InsertCellPoint(206);
points17.SetPoint(207,19,2.2, 0.1)
lines17.InsertCellPoint(207);
points17.SetPoint(208,19,2.4, 0.1)
lines17.InsertCellPoint(208);
points17.SetPoint(209,19,2.6, 0.1)
lines17.InsertCellPoint(209);
points17.SetPoint(210,19,2.8, 0.1)
lines17.InsertCellPoint(210);
points17.SetPoint(211,19,3, 0.1)
lines17.InsertCellPoint(211);
points17.SetPoint(212,19,3.2, 0.1)
lines17.InsertCellPoint(212);
points17.SetPoint(213,19,3.4, 0.1)
lines17.InsertCellPoint(213);
points17.SetPoint(214,19,3.6, 0.1)
lines17.InsertCellPoint(214);
points17.SetPoint(215,19,3.8, 0.1)
lines17.InsertCellPoint(215);
points17.SetPoint(216,19,4, 0.1)
lines17.InsertCellPoint(216);
points17.SetPoint(217,19,4.2, 0.1)
lines17.InsertCellPoint(217);
points17.SetPoint(218,19,4.4, 0.1)
lines17.InsertCellPoint(218);
points17.SetPoint(219,19,4.6, 0.1)
lines17.InsertCellPoint(219);
points17.SetPoint(220,19,4.8, 0.1)
lines17.InsertCellPoint(220);
points17.SetPoint(221,19,5, 0.1)
lines17.InsertCellPoint(221);
points17.SetPoint(222,19,5, 0.1)
lines17.InsertCellPoint(222);
points17.SetPoint(223,19,5.2, 0.1)
lines17.InsertCellPoint(223);
points17.SetPoint(224,19,5.4, 0.1)
lines17.InsertCellPoint(224);
points17.SetPoint(225,19,5.6, 0.1)
lines17.InsertCellPoint(225);
points17.SetPoint(226,19,5.8, 0.1)
lines17.InsertCellPoint(226);
points17.SetPoint(227,19,6, 0.1)
lines17.InsertCellPoint(227);
points17.SetPoint(228,19,6.2, 0.1)
lines17.InsertCellPoint(228);
points17.SetPoint(229,19,6.4, 0.1)
lines17.InsertCellPoint(229);
points17.SetPoint(230,19,6.6, 0.1)
lines17.InsertCellPoint(230);
points17.SetPoint(231,19,6.8, 0.1)
lines17.InsertCellPoint(231);
points17.SetPoint(232,19,7, 0.1)
lines17.InsertCellPoint(232);
points17.SetPoint(233,19,7.2, 0.1)
lines17.InsertCellPoint(233);
points17.SetPoint(234,19,7.4, 0.1)
lines17.InsertCellPoint(234);
points17.SetPoint(235,19,7.6, 0.1)
lines17.InsertCellPoint(235);
points17.SetPoint(236,19,7.8, 0.1)
lines17.InsertCellPoint(236);
points17.SetPoint(237,19,8, 0.1)
lines17.InsertCellPoint(237);
points17.SetPoint(238,19,8.2, 0.1)
lines17.InsertCellPoint(238);
points17.SetPoint(239,19,8.4, 0.1)
lines17.InsertCellPoint(239);
points17.SetPoint(240,19,8.6, 0.1)
lines17.InsertCellPoint(240);
points17.SetPoint(241,19,8.8, 0.1)
lines17.InsertCellPoint(241);
points17.SetPoint(242,19,9, 0.1)
lines17.InsertCellPoint(242);
points17.SetPoint(243,19,9, 0.1)
lines17.InsertCellPoint(243);
points17.SetPoint(244,19.1515,9.13636, 0.1)
lines17.InsertCellPoint(244);
points17.SetPoint(245,19.303,9.27273, 0.1)
lines17.InsertCellPoint(245);
points17.SetPoint(246,19.4545,9.40909, 0.1)
lines17.InsertCellPoint(246);
points17.SetPoint(247,19.6061,9.54545, 0.1)
lines17.InsertCellPoint(247);
points17.SetPoint(248,19.7576,9.68182, 0.1)
lines17.InsertCellPoint(248);
points17.SetPoint(249,19.9091,9.81818, 0.1)
lines17.InsertCellPoint(249);
points17.SetPoint(250,20.0606,9.95455, 0.1)
lines17.InsertCellPoint(250);
points17.SetPoint(251,20.2121,10.0909, 0.1)
lines17.InsertCellPoint(251);
points17.SetPoint(252,20.3636,10.2273, 0.1)
lines17.InsertCellPoint(252);
points17.SetPoint(253,20.5152,10.3636, 0.1)
lines17.InsertCellPoint(253);
points17.SetPoint(254,20.6667,10.5, 0.1)
lines17.InsertCellPoint(254);
points17.SetPoint(255,20.8182,10.6364, 0.1)
lines17.InsertCellPoint(255);
points17.SetPoint(256,20.9697,10.7727, 0.1)
lines17.InsertCellPoint(256);
points17.SetPoint(257,21.1212,10.9091, 0.1)
lines17.InsertCellPoint(257);
points17.SetPoint(258,21.2727,11.0455, 0.1)
lines17.InsertCellPoint(258);
points17.SetPoint(259,21.4242,11.1818, 0.1)
lines17.InsertCellPoint(259);
points17.SetPoint(260,21.5758,11.3182, 0.1)
lines17.InsertCellPoint(260);
points17.SetPoint(261,21.7273,11.4545, 0.1)
lines17.InsertCellPoint(261);
points17.SetPoint(262,21.8788,11.5909, 0.1)
lines17.InsertCellPoint(262);
points17.SetPoint(263,22.0303,11.7273, 0.1)
lines17.InsertCellPoint(263);
points17.SetPoint(264,22.1818,11.8636, 0.1)
lines17.InsertCellPoint(264);
points17.SetPoint(265,22.3333,12, 0.1)
lines17.InsertCellPoint(265);
points17.SetPoint(266,22.4848,12.1364, 0.1)
lines17.InsertCellPoint(266);
points17.SetPoint(267,22.6364,12.2727, 0.1)
lines17.InsertCellPoint(267);
points17.SetPoint(268,22.7879,12.4091, 0.1)
lines17.InsertCellPoint(268);
points17.SetPoint(269,22.9394,12.5455, 0.1)
lines17.InsertCellPoint(269);
points17.SetPoint(270,23.0909,12.6818, 0.1)
lines17.InsertCellPoint(270);
points17.SetPoint(271,23.2424,12.8182, 0.1)
lines17.InsertCellPoint(271);
points17.SetPoint(272,23.3939,12.9545, 0.1)
lines17.InsertCellPoint(272);
points17.SetPoint(273,23.5455,13.0909, 0.1)
lines17.InsertCellPoint(273);
points17.SetPoint(274,23.697,13.2273, 0.1)
lines17.InsertCellPoint(274);
points17.SetPoint(275,23.8485,13.3636, 0.1)
lines17.InsertCellPoint(275);
points17.SetPoint(276,24,13.5, 0.1)
lines17.InsertCellPoint(276);
lines17.InsertCellPoint(0);
polygon17 = vtk.vtkPolyData()
polygon17.SetPoints(points17)
polygon17.SetLines(lines17)
polygonMapper17 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper17.SetInputConnection(polygon17.GetProducerPort())
else:
	polygonMapper17.SetInputData(polygon17)
	polygonMapper17.Update()
polygonActor17 = vtk.vtkActor()
polygonActor17.SetMapper(polygonMapper17)
polygonActor17.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor17.GetProperty().SetLineWidth(widthLine)
points18 = vtk.vtkPoints()
points18.SetNumberOfPoints(164)
lines18 = vtk.vtkCellArray()
lines18.InsertNextCell(165)
points18.SetPoint(0,8,13, 0.1)
lines18.InsertCellPoint(0);
points18.SetPoint(1,8.08163,12.8163, 0.1)
lines18.InsertCellPoint(1);
points18.SetPoint(2,8.16327,12.6327, 0.1)
lines18.InsertCellPoint(2);
points18.SetPoint(3,8.2449,12.449, 0.1)
lines18.InsertCellPoint(3);
points18.SetPoint(4,8.32653,12.2653, 0.1)
lines18.InsertCellPoint(4);
points18.SetPoint(5,8.40816,12.0816, 0.1)
lines18.InsertCellPoint(5);
points18.SetPoint(6,8.4898,11.898, 0.1)
lines18.InsertCellPoint(6);
points18.SetPoint(7,8.57143,11.7143, 0.1)
lines18.InsertCellPoint(7);
points18.SetPoint(8,8.65306,11.5306, 0.1)
lines18.InsertCellPoint(8);
points18.SetPoint(9,8.73469,11.3469, 0.1)
lines18.InsertCellPoint(9);
points18.SetPoint(10,8.81633,11.1633, 0.1)
lines18.InsertCellPoint(10);
points18.SetPoint(11,8.89796,10.9796, 0.1)
lines18.InsertCellPoint(11);
points18.SetPoint(12,8.97959,10.7959, 0.1)
lines18.InsertCellPoint(12);
points18.SetPoint(13,9.06122,10.6122, 0.1)
lines18.InsertCellPoint(13);
points18.SetPoint(14,9.14286,10.4286, 0.1)
lines18.InsertCellPoint(14);
points18.SetPoint(15,9.22449,10.2449, 0.1)
lines18.InsertCellPoint(15);
points18.SetPoint(16,9.30612,10.0612, 0.1)
lines18.InsertCellPoint(16);
points18.SetPoint(17,9.38776,9.87755, 0.1)
lines18.InsertCellPoint(17);
points18.SetPoint(18,9.46939,9.69388, 0.1)
lines18.InsertCellPoint(18);
points18.SetPoint(19,9.55102,9.5102, 0.1)
lines18.InsertCellPoint(19);
points18.SetPoint(20,9.63265,9.32653, 0.1)
lines18.InsertCellPoint(20);
points18.SetPoint(21,9.71429,9.14286, 0.1)
lines18.InsertCellPoint(21);
points18.SetPoint(22,9.79592,8.95918, 0.1)
lines18.InsertCellPoint(22);
points18.SetPoint(23,9.87755,8.77551, 0.1)
lines18.InsertCellPoint(23);
points18.SetPoint(24,9.95918,8.59184, 0.1)
lines18.InsertCellPoint(24);
points18.SetPoint(25,10.0408,8.40816, 0.1)
lines18.InsertCellPoint(25);
points18.SetPoint(26,10.1224,8.22449, 0.1)
lines18.InsertCellPoint(26);
points18.SetPoint(27,10.2041,8.04082, 0.1)
lines18.InsertCellPoint(27);
points18.SetPoint(28,10.2857,7.85714, 0.1)
lines18.InsertCellPoint(28);
points18.SetPoint(29,10.3673,7.67347, 0.1)
lines18.InsertCellPoint(29);
points18.SetPoint(30,10.449,7.4898, 0.1)
lines18.InsertCellPoint(30);
points18.SetPoint(31,10.5306,7.30612, 0.1)
lines18.InsertCellPoint(31);
points18.SetPoint(32,10.6122,7.12245, 0.1)
lines18.InsertCellPoint(32);
points18.SetPoint(33,10.6939,6.93878, 0.1)
lines18.InsertCellPoint(33);
points18.SetPoint(34,10.7755,6.7551, 0.1)
lines18.InsertCellPoint(34);
points18.SetPoint(35,10.8571,6.57143, 0.1)
lines18.InsertCellPoint(35);
points18.SetPoint(36,10.9388,6.38776, 0.1)
lines18.InsertCellPoint(36);
points18.SetPoint(37,11.0204,6.20408, 0.1)
lines18.InsertCellPoint(37);
points18.SetPoint(38,11.102,6.02041, 0.1)
lines18.InsertCellPoint(38);
points18.SetPoint(39,11.1837,5.83673, 0.1)
lines18.InsertCellPoint(39);
points18.SetPoint(40,11.2653,5.65306, 0.1)
lines18.InsertCellPoint(40);
points18.SetPoint(41,11.3469,5.46939, 0.1)
lines18.InsertCellPoint(41);
points18.SetPoint(42,11.4286,5.28571, 0.1)
lines18.InsertCellPoint(42);
points18.SetPoint(43,11.5102,5.10204, 0.1)
lines18.InsertCellPoint(43);
points18.SetPoint(44,11.5918,4.91837, 0.1)
lines18.InsertCellPoint(44);
points18.SetPoint(45,11.6735,4.73469, 0.1)
lines18.InsertCellPoint(45);
points18.SetPoint(46,11.7551,4.55102, 0.1)
lines18.InsertCellPoint(46);
points18.SetPoint(47,11.8367,4.36735, 0.1)
lines18.InsertCellPoint(47);
points18.SetPoint(48,11.9184,4.18367, 0.1)
lines18.InsertCellPoint(48);
points18.SetPoint(49,12,4, 0.1)
lines18.InsertCellPoint(49);
points18.SetPoint(50,12,4, 0.1)
lines18.InsertCellPoint(50);
points18.SetPoint(51,12.2,4.04, 0.1)
lines18.InsertCellPoint(51);
points18.SetPoint(52,12.4,4.08, 0.1)
lines18.InsertCellPoint(52);
points18.SetPoint(53,12.6,4.12, 0.1)
lines18.InsertCellPoint(53);
points18.SetPoint(54,12.8,4.16, 0.1)
lines18.InsertCellPoint(54);
points18.SetPoint(55,13,4.2, 0.1)
lines18.InsertCellPoint(55);
points18.SetPoint(56,13.2,4.24, 0.1)
lines18.InsertCellPoint(56);
points18.SetPoint(57,13.4,4.28, 0.1)
lines18.InsertCellPoint(57);
points18.SetPoint(58,13.6,4.32, 0.1)
lines18.InsertCellPoint(58);
points18.SetPoint(59,13.8,4.36, 0.1)
lines18.InsertCellPoint(59);
points18.SetPoint(60,14,4.4, 0.1)
lines18.InsertCellPoint(60);
points18.SetPoint(61,14.2,4.44, 0.1)
lines18.InsertCellPoint(61);
points18.SetPoint(62,14.4,4.48, 0.1)
lines18.InsertCellPoint(62);
points18.SetPoint(63,14.6,4.52, 0.1)
lines18.InsertCellPoint(63);
points18.SetPoint(64,14.8,4.56, 0.1)
lines18.InsertCellPoint(64);
points18.SetPoint(65,15,4.6, 0.1)
lines18.InsertCellPoint(65);
points18.SetPoint(66,15.2,4.64, 0.1)
lines18.InsertCellPoint(66);
points18.SetPoint(67,15.4,4.68, 0.1)
lines18.InsertCellPoint(67);
points18.SetPoint(68,15.6,4.72, 0.1)
lines18.InsertCellPoint(68);
points18.SetPoint(69,15.8,4.76, 0.1)
lines18.InsertCellPoint(69);
points18.SetPoint(70,16,4.8, 0.1)
lines18.InsertCellPoint(70);
points18.SetPoint(71,16.2,4.84, 0.1)
lines18.InsertCellPoint(71);
points18.SetPoint(72,16.4,4.88, 0.1)
lines18.InsertCellPoint(72);
points18.SetPoint(73,16.6,4.92, 0.1)
lines18.InsertCellPoint(73);
points18.SetPoint(74,16.8,4.96, 0.1)
lines18.InsertCellPoint(74);
points18.SetPoint(75,17,5, 0.1)
lines18.InsertCellPoint(75);
points18.SetPoint(76,17,5, 0.1)
lines18.InsertCellPoint(76);
points18.SetPoint(77,16.96,5.2, 0.1)
lines18.InsertCellPoint(77);
points18.SetPoint(78,16.92,5.4, 0.1)
lines18.InsertCellPoint(78);
points18.SetPoint(79,16.88,5.6, 0.1)
lines18.InsertCellPoint(79);
points18.SetPoint(80,16.84,5.8, 0.1)
lines18.InsertCellPoint(80);
points18.SetPoint(81,16.8,6, 0.1)
lines18.InsertCellPoint(81);
points18.SetPoint(82,16.76,6.2, 0.1)
lines18.InsertCellPoint(82);
points18.SetPoint(83,16.72,6.4, 0.1)
lines18.InsertCellPoint(83);
points18.SetPoint(84,16.68,6.6, 0.1)
lines18.InsertCellPoint(84);
points18.SetPoint(85,16.64,6.8, 0.1)
lines18.InsertCellPoint(85);
points18.SetPoint(86,16.6,7, 0.1)
lines18.InsertCellPoint(86);
points18.SetPoint(87,16.56,7.2, 0.1)
lines18.InsertCellPoint(87);
points18.SetPoint(88,16.52,7.4, 0.1)
lines18.InsertCellPoint(88);
points18.SetPoint(89,16.48,7.6, 0.1)
lines18.InsertCellPoint(89);
points18.SetPoint(90,16.44,7.8, 0.1)
lines18.InsertCellPoint(90);
points18.SetPoint(91,16.4,8, 0.1)
lines18.InsertCellPoint(91);
points18.SetPoint(92,16.36,8.2, 0.1)
lines18.InsertCellPoint(92);
points18.SetPoint(93,16.32,8.4, 0.1)
lines18.InsertCellPoint(93);
points18.SetPoint(94,16.28,8.6, 0.1)
lines18.InsertCellPoint(94);
points18.SetPoint(95,16.24,8.8, 0.1)
lines18.InsertCellPoint(95);
points18.SetPoint(96,16.2,9, 0.1)
lines18.InsertCellPoint(96);
points18.SetPoint(97,16.16,9.2, 0.1)
lines18.InsertCellPoint(97);
points18.SetPoint(98,16.12,9.4, 0.1)
lines18.InsertCellPoint(98);
points18.SetPoint(99,16.08,9.6, 0.1)
lines18.InsertCellPoint(99);
points18.SetPoint(100,16.04,9.8, 0.1)
lines18.InsertCellPoint(100);
points18.SetPoint(101,16,10, 0.1)
lines18.InsertCellPoint(101);
points18.SetPoint(102,15.96,10.2, 0.1)
lines18.InsertCellPoint(102);
points18.SetPoint(103,15.92,10.4, 0.1)
lines18.InsertCellPoint(103);
points18.SetPoint(104,15.88,10.6, 0.1)
lines18.InsertCellPoint(104);
points18.SetPoint(105,15.84,10.8, 0.1)
lines18.InsertCellPoint(105);
points18.SetPoint(106,15.8,11, 0.1)
lines18.InsertCellPoint(106);
points18.SetPoint(107,15.76,11.2, 0.1)
lines18.InsertCellPoint(107);
points18.SetPoint(108,15.72,11.4, 0.1)
lines18.InsertCellPoint(108);
points18.SetPoint(109,15.68,11.6, 0.1)
lines18.InsertCellPoint(109);
points18.SetPoint(110,15.64,11.8, 0.1)
lines18.InsertCellPoint(110);
points18.SetPoint(111,15.6,12, 0.1)
lines18.InsertCellPoint(111);
points18.SetPoint(112,15.56,12.2, 0.1)
lines18.InsertCellPoint(112);
points18.SetPoint(113,15.52,12.4, 0.1)
lines18.InsertCellPoint(113);
points18.SetPoint(114,15.48,12.6, 0.1)
lines18.InsertCellPoint(114);
points18.SetPoint(115,15.44,12.8, 0.1)
lines18.InsertCellPoint(115);
points18.SetPoint(116,15.4,13, 0.1)
lines18.InsertCellPoint(116);
points18.SetPoint(117,15.36,13.2, 0.1)
lines18.InsertCellPoint(117);
points18.SetPoint(118,15.32,13.4, 0.1)
lines18.InsertCellPoint(118);
points18.SetPoint(119,15.28,13.6, 0.1)
lines18.InsertCellPoint(119);
points18.SetPoint(120,15.24,13.8, 0.1)
lines18.InsertCellPoint(120);
points18.SetPoint(121,15.2,14, 0.1)
lines18.InsertCellPoint(121);
points18.SetPoint(122,15.16,14.2, 0.1)
lines18.InsertCellPoint(122);
points18.SetPoint(123,15.12,14.4, 0.1)
lines18.InsertCellPoint(123);
points18.SetPoint(124,15.08,14.6, 0.1)
lines18.InsertCellPoint(124);
points18.SetPoint(125,15.04,14.8, 0.1)
lines18.InsertCellPoint(125);
points18.SetPoint(126,15,15, 0.1)
lines18.InsertCellPoint(126);
points18.SetPoint(127,15,15, 0.1)
lines18.InsertCellPoint(127);
points18.SetPoint(128,14.8056,14.9444, 0.1)
lines18.InsertCellPoint(128);
points18.SetPoint(129,14.6111,14.8889, 0.1)
lines18.InsertCellPoint(129);
points18.SetPoint(130,14.4167,14.8333, 0.1)
lines18.InsertCellPoint(130);
points18.SetPoint(131,14.2222,14.7778, 0.1)
lines18.InsertCellPoint(131);
points18.SetPoint(132,14.0278,14.7222, 0.1)
lines18.InsertCellPoint(132);
points18.SetPoint(133,13.8333,14.6667, 0.1)
lines18.InsertCellPoint(133);
points18.SetPoint(134,13.6389,14.6111, 0.1)
lines18.InsertCellPoint(134);
points18.SetPoint(135,13.4444,14.5556, 0.1)
lines18.InsertCellPoint(135);
points18.SetPoint(136,13.25,14.5, 0.1)
lines18.InsertCellPoint(136);
points18.SetPoint(137,13.0556,14.4444, 0.1)
lines18.InsertCellPoint(137);
points18.SetPoint(138,12.8611,14.3889, 0.1)
lines18.InsertCellPoint(138);
points18.SetPoint(139,12.6667,14.3333, 0.1)
lines18.InsertCellPoint(139);
points18.SetPoint(140,12.4722,14.2778, 0.1)
lines18.InsertCellPoint(140);
points18.SetPoint(141,12.2778,14.2222, 0.1)
lines18.InsertCellPoint(141);
points18.SetPoint(142,12.0833,14.1667, 0.1)
lines18.InsertCellPoint(142);
points18.SetPoint(143,11.8889,14.1111, 0.1)
lines18.InsertCellPoint(143);
points18.SetPoint(144,11.6944,14.0556, 0.1)
lines18.InsertCellPoint(144);
points18.SetPoint(145,11.5,14, 0.1)
lines18.InsertCellPoint(145);
points18.SetPoint(146,11.3056,13.9444, 0.1)
lines18.InsertCellPoint(146);
points18.SetPoint(147,11.1111,13.8889, 0.1)
lines18.InsertCellPoint(147);
points18.SetPoint(148,10.9167,13.8333, 0.1)
lines18.InsertCellPoint(148);
points18.SetPoint(149,10.7222,13.7778, 0.1)
lines18.InsertCellPoint(149);
points18.SetPoint(150,10.5278,13.7222, 0.1)
lines18.InsertCellPoint(150);
points18.SetPoint(151,10.3333,13.6667, 0.1)
lines18.InsertCellPoint(151);
points18.SetPoint(152,10.1389,13.6111, 0.1)
lines18.InsertCellPoint(152);
points18.SetPoint(153,9.94444,13.5556, 0.1)
lines18.InsertCellPoint(153);
points18.SetPoint(154,9.75,13.5, 0.1)
lines18.InsertCellPoint(154);
points18.SetPoint(155,9.55556,13.4444, 0.1)
lines18.InsertCellPoint(155);
points18.SetPoint(156,9.36111,13.3889, 0.1)
lines18.InsertCellPoint(156);
points18.SetPoint(157,9.16667,13.3333, 0.1)
lines18.InsertCellPoint(157);
points18.SetPoint(158,8.97222,13.2778, 0.1)
lines18.InsertCellPoint(158);
points18.SetPoint(159,8.77778,13.2222, 0.1)
lines18.InsertCellPoint(159);
points18.SetPoint(160,8.58333,13.1667, 0.1)
lines18.InsertCellPoint(160);
points18.SetPoint(161,8.38889,13.1111, 0.1)
lines18.InsertCellPoint(161);
points18.SetPoint(162,8.19444,13.0556, 0.1)
lines18.InsertCellPoint(162);
points18.SetPoint(163,8,13, 0.1)
lines18.InsertCellPoint(163);
lines18.InsertCellPoint(0);
polygon18 = vtk.vtkPolyData()
polygon18.SetPoints(points18)
polygon18.SetLines(lines18)
polygonMapper18 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper18.SetInputConnection(polygon18.GetProducerPort())
else:
	polygonMapper18.SetInputData(polygon18)
	polygonMapper18.Update()
polygonActor18 = vtk.vtkActor()
polygonActor18.SetMapper(polygonMapper18)
polygonActor18.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor18.GetProperty().SetLineWidth(widthLine)
points19 = vtk.vtkPoints()
points19.SetNumberOfPoints(161)
lines19 = vtk.vtkCellArray()
lines19.InsertNextCell(162)
points19.SetPoint(0,8,-3.5, 0.1)
lines19.InsertCellPoint(0);
points19.SetPoint(1,7.88,-3.66, 0.1)
lines19.InsertCellPoint(1);
points19.SetPoint(2,7.76,-3.82, 0.1)
lines19.InsertCellPoint(2);
points19.SetPoint(3,7.64,-3.98, 0.1)
lines19.InsertCellPoint(3);
points19.SetPoint(4,7.52,-4.14, 0.1)
lines19.InsertCellPoint(4);
points19.SetPoint(5,7.4,-4.3, 0.1)
lines19.InsertCellPoint(5);
points19.SetPoint(6,7.28,-4.46, 0.1)
lines19.InsertCellPoint(6);
points19.SetPoint(7,7.16,-4.62, 0.1)
lines19.InsertCellPoint(7);
points19.SetPoint(8,7.04,-4.78, 0.1)
lines19.InsertCellPoint(8);
points19.SetPoint(9,6.92,-4.94, 0.1)
lines19.InsertCellPoint(9);
points19.SetPoint(10,6.8,-5.1, 0.1)
lines19.InsertCellPoint(10);
points19.SetPoint(11,6.68,-5.26, 0.1)
lines19.InsertCellPoint(11);
points19.SetPoint(12,6.56,-5.42, 0.1)
lines19.InsertCellPoint(12);
points19.SetPoint(13,6.44,-5.58, 0.1)
lines19.InsertCellPoint(13);
points19.SetPoint(14,6.32,-5.74, 0.1)
lines19.InsertCellPoint(14);
points19.SetPoint(15,6.2,-5.9, 0.1)
lines19.InsertCellPoint(15);
points19.SetPoint(16,6.08,-6.06, 0.1)
lines19.InsertCellPoint(16);
points19.SetPoint(17,5.96,-6.22, 0.1)
lines19.InsertCellPoint(17);
points19.SetPoint(18,5.84,-6.38, 0.1)
lines19.InsertCellPoint(18);
points19.SetPoint(19,5.72,-6.54, 0.1)
lines19.InsertCellPoint(19);
points19.SetPoint(20,5.6,-6.7, 0.1)
lines19.InsertCellPoint(20);
points19.SetPoint(21,5.48,-6.86, 0.1)
lines19.InsertCellPoint(21);
points19.SetPoint(22,5.36,-7.02, 0.1)
lines19.InsertCellPoint(22);
points19.SetPoint(23,5.24,-7.18, 0.1)
lines19.InsertCellPoint(23);
points19.SetPoint(24,5.12,-7.34, 0.1)
lines19.InsertCellPoint(24);
points19.SetPoint(25,5,-7.5, 0.1)
lines19.InsertCellPoint(25);
points19.SetPoint(26,5,-7.5, 0.1)
lines19.InsertCellPoint(26);
points19.SetPoint(27,5.2,-7.47273, 0.1)
lines19.InsertCellPoint(27);
points19.SetPoint(28,5.4,-7.44545, 0.1)
lines19.InsertCellPoint(28);
points19.SetPoint(29,5.6,-7.41818, 0.1)
lines19.InsertCellPoint(29);
points19.SetPoint(30,5.8,-7.39091, 0.1)
lines19.InsertCellPoint(30);
points19.SetPoint(31,6,-7.36364, 0.1)
lines19.InsertCellPoint(31);
points19.SetPoint(32,6.2,-7.33636, 0.1)
lines19.InsertCellPoint(32);
points19.SetPoint(33,6.4,-7.30909, 0.1)
lines19.InsertCellPoint(33);
points19.SetPoint(34,6.6,-7.28182, 0.1)
lines19.InsertCellPoint(34);
points19.SetPoint(35,6.8,-7.25455, 0.1)
lines19.InsertCellPoint(35);
points19.SetPoint(36,7,-7.22727, 0.1)
lines19.InsertCellPoint(36);
points19.SetPoint(37,7.2,-7.2, 0.1)
lines19.InsertCellPoint(37);
points19.SetPoint(38,7.4,-7.17273, 0.1)
lines19.InsertCellPoint(38);
points19.SetPoint(39,7.6,-7.14545, 0.1)
lines19.InsertCellPoint(39);
points19.SetPoint(40,7.8,-7.11818, 0.1)
lines19.InsertCellPoint(40);
points19.SetPoint(41,8,-7.09091, 0.1)
lines19.InsertCellPoint(41);
points19.SetPoint(42,8.2,-7.06364, 0.1)
lines19.InsertCellPoint(42);
points19.SetPoint(43,8.4,-7.03636, 0.1)
lines19.InsertCellPoint(43);
points19.SetPoint(44,8.6,-7.00909, 0.1)
lines19.InsertCellPoint(44);
points19.SetPoint(45,8.8,-6.98182, 0.1)
lines19.InsertCellPoint(45);
points19.SetPoint(46,9,-6.95455, 0.1)
lines19.InsertCellPoint(46);
points19.SetPoint(47,9.2,-6.92727, 0.1)
lines19.InsertCellPoint(47);
points19.SetPoint(48,9.4,-6.9, 0.1)
lines19.InsertCellPoint(48);
points19.SetPoint(49,9.6,-6.87273, 0.1)
lines19.InsertCellPoint(49);
points19.SetPoint(50,9.8,-6.84545, 0.1)
lines19.InsertCellPoint(50);
points19.SetPoint(51,10,-6.81818, 0.1)
lines19.InsertCellPoint(51);
points19.SetPoint(52,10.2,-6.79091, 0.1)
lines19.InsertCellPoint(52);
points19.SetPoint(53,10.4,-6.76364, 0.1)
lines19.InsertCellPoint(53);
points19.SetPoint(54,10.6,-6.73636, 0.1)
lines19.InsertCellPoint(54);
points19.SetPoint(55,10.8,-6.70909, 0.1)
lines19.InsertCellPoint(55);
points19.SetPoint(56,11,-6.68182, 0.1)
lines19.InsertCellPoint(56);
points19.SetPoint(57,11.2,-6.65455, 0.1)
lines19.InsertCellPoint(57);
points19.SetPoint(58,11.4,-6.62727, 0.1)
lines19.InsertCellPoint(58);
points19.SetPoint(59,11.6,-6.6, 0.1)
lines19.InsertCellPoint(59);
points19.SetPoint(60,11.8,-6.57273, 0.1)
lines19.InsertCellPoint(60);
points19.SetPoint(61,12,-6.54545, 0.1)
lines19.InsertCellPoint(61);
points19.SetPoint(62,12.2,-6.51818, 0.1)
lines19.InsertCellPoint(62);
points19.SetPoint(63,12.4,-6.49091, 0.1)
lines19.InsertCellPoint(63);
points19.SetPoint(64,12.6,-6.46364, 0.1)
lines19.InsertCellPoint(64);
points19.SetPoint(65,12.8,-6.43636, 0.1)
lines19.InsertCellPoint(65);
points19.SetPoint(66,13,-6.40909, 0.1)
lines19.InsertCellPoint(66);
points19.SetPoint(67,13.2,-6.38182, 0.1)
lines19.InsertCellPoint(67);
points19.SetPoint(68,13.4,-6.35455, 0.1)
lines19.InsertCellPoint(68);
points19.SetPoint(69,13.6,-6.32727, 0.1)
lines19.InsertCellPoint(69);
points19.SetPoint(70,13.8,-6.3, 0.1)
lines19.InsertCellPoint(70);
points19.SetPoint(71,14,-6.27273, 0.1)
lines19.InsertCellPoint(71);
points19.SetPoint(72,14.2,-6.24545, 0.1)
lines19.InsertCellPoint(72);
points19.SetPoint(73,14.4,-6.21818, 0.1)
lines19.InsertCellPoint(73);
points19.SetPoint(74,14.6,-6.19091, 0.1)
lines19.InsertCellPoint(74);
points19.SetPoint(75,14.8,-6.16364, 0.1)
lines19.InsertCellPoint(75);
points19.SetPoint(76,15,-6.13636, 0.1)
lines19.InsertCellPoint(76);
points19.SetPoint(77,15.2,-6.10909, 0.1)
lines19.InsertCellPoint(77);
points19.SetPoint(78,15.4,-6.08182, 0.1)
lines19.InsertCellPoint(78);
points19.SetPoint(79,15.6,-6.05455, 0.1)
lines19.InsertCellPoint(79);
points19.SetPoint(80,15.8,-6.02727, 0.1)
lines19.InsertCellPoint(80);
points19.SetPoint(81,16,-6, 0.1)
lines19.InsertCellPoint(81);
points19.SetPoint(82,16,-6, 0.1)
lines19.InsertCellPoint(82);
points19.SetPoint(83,16.0536,-5.80357, 0.1)
lines19.InsertCellPoint(83);
points19.SetPoint(84,16.1071,-5.60714, 0.1)
lines19.InsertCellPoint(84);
points19.SetPoint(85,16.1607,-5.41071, 0.1)
lines19.InsertCellPoint(85);
points19.SetPoint(86,16.2143,-5.21429, 0.1)
lines19.InsertCellPoint(86);
points19.SetPoint(87,16.2679,-5.01786, 0.1)
lines19.InsertCellPoint(87);
points19.SetPoint(88,16.3214,-4.82143, 0.1)
lines19.InsertCellPoint(88);
points19.SetPoint(89,16.375,-4.625, 0.1)
lines19.InsertCellPoint(89);
points19.SetPoint(90,16.4286,-4.42857, 0.1)
lines19.InsertCellPoint(90);
points19.SetPoint(91,16.4821,-4.23214, 0.1)
lines19.InsertCellPoint(91);
points19.SetPoint(92,16.5357,-4.03571, 0.1)
lines19.InsertCellPoint(92);
points19.SetPoint(93,16.5893,-3.83929, 0.1)
lines19.InsertCellPoint(93);
points19.SetPoint(94,16.6429,-3.64286, 0.1)
lines19.InsertCellPoint(94);
points19.SetPoint(95,16.6964,-3.44643, 0.1)
lines19.InsertCellPoint(95);
points19.SetPoint(96,16.75,-3.25, 0.1)
lines19.InsertCellPoint(96);
points19.SetPoint(97,16.8036,-3.05357, 0.1)
lines19.InsertCellPoint(97);
points19.SetPoint(98,16.8571,-2.85714, 0.1)
lines19.InsertCellPoint(98);
points19.SetPoint(99,16.9107,-2.66071, 0.1)
lines19.InsertCellPoint(99);
points19.SetPoint(100,16.9643,-2.46429, 0.1)
lines19.InsertCellPoint(100);
points19.SetPoint(101,17.0179,-2.26786, 0.1)
lines19.InsertCellPoint(101);
points19.SetPoint(102,17.0714,-2.07143, 0.1)
lines19.InsertCellPoint(102);
points19.SetPoint(103,17.125,-1.875, 0.1)
lines19.InsertCellPoint(103);
points19.SetPoint(104,17.1786,-1.67857, 0.1)
lines19.InsertCellPoint(104);
points19.SetPoint(105,17.2321,-1.48214, 0.1)
lines19.InsertCellPoint(105);
points19.SetPoint(106,17.2857,-1.28571, 0.1)
lines19.InsertCellPoint(106);
points19.SetPoint(107,17.3393,-1.08929, 0.1)
lines19.InsertCellPoint(107);
points19.SetPoint(108,17.3929,-0.892857, 0.1)
lines19.InsertCellPoint(108);
points19.SetPoint(109,17.4464,-0.696429, 0.1)
lines19.InsertCellPoint(109);
points19.SetPoint(110,17.5,-0.5, 0.1)
lines19.InsertCellPoint(110);
points19.SetPoint(111,17.5,-0.5, 0.1)
lines19.InsertCellPoint(111);
points19.SetPoint(112,17.3061,-0.561224, 0.1)
lines19.InsertCellPoint(112);
points19.SetPoint(113,17.1122,-0.622449, 0.1)
lines19.InsertCellPoint(113);
points19.SetPoint(114,16.9184,-0.683673, 0.1)
lines19.InsertCellPoint(114);
points19.SetPoint(115,16.7245,-0.744898, 0.1)
lines19.InsertCellPoint(115);
points19.SetPoint(116,16.5306,-0.806122, 0.1)
lines19.InsertCellPoint(116);
points19.SetPoint(117,16.3367,-0.867347, 0.1)
lines19.InsertCellPoint(117);
points19.SetPoint(118,16.1429,-0.928571, 0.1)
lines19.InsertCellPoint(118);
points19.SetPoint(119,15.949,-0.989796, 0.1)
lines19.InsertCellPoint(119);
points19.SetPoint(120,15.7551,-1.05102, 0.1)
lines19.InsertCellPoint(120);
points19.SetPoint(121,15.5612,-1.11224, 0.1)
lines19.InsertCellPoint(121);
points19.SetPoint(122,15.3673,-1.17347, 0.1)
lines19.InsertCellPoint(122);
points19.SetPoint(123,15.1735,-1.23469, 0.1)
lines19.InsertCellPoint(123);
points19.SetPoint(124,14.9796,-1.29592, 0.1)
lines19.InsertCellPoint(124);
points19.SetPoint(125,14.7857,-1.35714, 0.1)
lines19.InsertCellPoint(125);
points19.SetPoint(126,14.5918,-1.41837, 0.1)
lines19.InsertCellPoint(126);
points19.SetPoint(127,14.398,-1.47959, 0.1)
lines19.InsertCellPoint(127);
points19.SetPoint(128,14.2041,-1.54082, 0.1)
lines19.InsertCellPoint(128);
points19.SetPoint(129,14.0102,-1.60204, 0.1)
lines19.InsertCellPoint(129);
points19.SetPoint(130,13.8163,-1.66327, 0.1)
lines19.InsertCellPoint(130);
points19.SetPoint(131,13.6224,-1.72449, 0.1)
lines19.InsertCellPoint(131);
points19.SetPoint(132,13.4286,-1.78571, 0.1)
lines19.InsertCellPoint(132);
points19.SetPoint(133,13.2347,-1.84694, 0.1)
lines19.InsertCellPoint(133);
points19.SetPoint(134,13.0408,-1.90816, 0.1)
lines19.InsertCellPoint(134);
points19.SetPoint(135,12.8469,-1.96939, 0.1)
lines19.InsertCellPoint(135);
points19.SetPoint(136,12.6531,-2.03061, 0.1)
lines19.InsertCellPoint(136);
points19.SetPoint(137,12.4592,-2.09184, 0.1)
lines19.InsertCellPoint(137);
points19.SetPoint(138,12.2653,-2.15306, 0.1)
lines19.InsertCellPoint(138);
points19.SetPoint(139,12.0714,-2.21429, 0.1)
lines19.InsertCellPoint(139);
points19.SetPoint(140,11.8776,-2.27551, 0.1)
lines19.InsertCellPoint(140);
points19.SetPoint(141,11.6837,-2.33673, 0.1)
lines19.InsertCellPoint(141);
points19.SetPoint(142,11.4898,-2.39796, 0.1)
lines19.InsertCellPoint(142);
points19.SetPoint(143,11.2959,-2.45918, 0.1)
lines19.InsertCellPoint(143);
points19.SetPoint(144,11.102,-2.52041, 0.1)
lines19.InsertCellPoint(144);
points19.SetPoint(145,10.9082,-2.58163, 0.1)
lines19.InsertCellPoint(145);
points19.SetPoint(146,10.7143,-2.64286, 0.1)
lines19.InsertCellPoint(146);
points19.SetPoint(147,10.5204,-2.70408, 0.1)
lines19.InsertCellPoint(147);
points19.SetPoint(148,10.3265,-2.76531, 0.1)
lines19.InsertCellPoint(148);
points19.SetPoint(149,10.1327,-2.82653, 0.1)
lines19.InsertCellPoint(149);
points19.SetPoint(150,9.93878,-2.88776, 0.1)
lines19.InsertCellPoint(150);
points19.SetPoint(151,9.7449,-2.94898, 0.1)
lines19.InsertCellPoint(151);
points19.SetPoint(152,9.55102,-3.0102, 0.1)
lines19.InsertCellPoint(152);
points19.SetPoint(153,9.35714,-3.07143, 0.1)
lines19.InsertCellPoint(153);
points19.SetPoint(154,9.16327,-3.13265, 0.1)
lines19.InsertCellPoint(154);
points19.SetPoint(155,8.96939,-3.19388, 0.1)
lines19.InsertCellPoint(155);
points19.SetPoint(156,8.77551,-3.2551, 0.1)
lines19.InsertCellPoint(156);
points19.SetPoint(157,8.58163,-3.31633, 0.1)
lines19.InsertCellPoint(157);
points19.SetPoint(158,8.38776,-3.37755, 0.1)
lines19.InsertCellPoint(158);
points19.SetPoint(159,8.19388,-3.43878, 0.1)
lines19.InsertCellPoint(159);
points19.SetPoint(160,8,-3.5, 0.1)
lines19.InsertCellPoint(160);
lines19.InsertCellPoint(0);
polygon19 = vtk.vtkPolyData()
polygon19.SetPoints(points19)
polygon19.SetLines(lines19)
polygonMapper19 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper19.SetInputConnection(polygon19.GetProducerPort())
else:
	polygonMapper19.SetInputData(polygon19)
	polygonMapper19.Update()
polygonActor19 = vtk.vtkActor()
polygonActor19.SetMapper(polygonMapper19)
polygonActor19.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor19.GetProperty().SetLineWidth(widthLine)
