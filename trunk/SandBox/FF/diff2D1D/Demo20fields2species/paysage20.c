#include "../gen2speciesNL.h"

void initStructure(){
  int ii;
  
  NbSpecies=2;
  NDynFieldType=2;
  NDynEdgeType=2;
  nPerLength=5;
  nEFinter=1;
  nEEinter=1;
//  reverseEdges=-1;
  symGeom=1;
  nParams=0;
  BUILD_MESH=1;
  specNames[0]='A';
  specNames[1]='B';
  int wb=1;
  LinearSolver_2D1D=GMRES_2D1D;
  sprintf(GEODIR,"%s/GEODATA/PAYSAGE20",SRCDIR);
  //sprintf(GEODIR,"W3");
  readPoints();
  sprintf(OUTPUTDIR,"%s/../OUTPUT/",SRCDIR);
  NFilesCopy=2;
  sprintf(FILES_COPY[0],"%s/Demo20fields2species/*.edp",SRCDIR);
  sprintf(FILES_COPY[1],"%s/Demo20fields2species/*.txt",SRCDIR);
  
  readEdges();
  buildEdges();
  readPointsInEdges();
  for (ii=0;ii<NEdges;ii++)
     printEdge(EDGES+ii);
  readWires();
  printWires();
  
 
  allocStructs();
  FieldTypes[0]=1;
  FieldTypes[10]=1;
  
  double coef=1.0;
/*

  aFieldPop[0].IC[0]=0;//5;
  aFieldPop[0].D2DX[0]=1;
  aFieldPop[0].D2DY[0]=1;
  aFieldPop[0].isFieldDep[0][K_I]=1;
  aFieldPop[0].K[0]=20;
  aFieldPop[0].R[0]=0;
  aFieldPop[0].isFieldDep[0][T_I]=1;
  aFieldPop[0].T[0]=1;
  aFieldPop[0].rho[0]=0.05;
  
  aFieldPop[0].Ccoup[0][1]=-1*coef;
  aFieldPop[0].Cmalthus[0]=0;
  aFieldPop[0].isFieldDep[0][source_I]=1;
  aFieldPop[0].source[0]=0.00;
  
  aFieldPop[0].IC[1]=0;//(5-0.05)*(1-0.25);
  aFieldPop[0].D2DX[1]=1;
  aFieldPop[0].D2DY[1]=1;
  aFieldPop[0].K[1]=15;
  aFieldPop[0].R[1]=0;
  aFieldPop[0].T[1]=0;
  aFieldPop[0].Ccoup[1][0]=1*coef;
  aFieldPop[0].Cmalthus[1]=-5;
  aFieldPop[0].source[1]=0.0;


//Fieldtype 1
  aFieldPop[1].IC[0]=5*1.2;
  aFieldPop[1].D2DX[0]=5;
  aFieldPop[1].D2DY[0]=5;
  aFieldPop[1].K[0]=5;
  aFieldPop[1].R[0]=0;
  aFieldPop[1].T[0]=1;
  aFieldPop[1].rho[0]=0.05;
  
  aFieldPop[1].Ccoup[0][1]=-1*coef;
  aFieldPop[1].Cmalthus[0]=0;
  aFieldPop[1].source[0]=0.00;
  
  aFieldPop[1].IC[1]=(5-0.05)*(1-0.25)*1.5;
  aFieldPop[1].D2DX[1]=15;
  aFieldPop[1].D2DY[1]=15;
  aFieldPop[1].K[1]=20;
  aFieldPop[1].R[1]=5;
  aFieldPop[1].T[1]=0;
  aFieldPop[1].Ccoup[1][0]=1*coef;
  aFieldPop[1].Cmalthus[1]=0;
  aFieldPop[1].source[1]=0.0;
  */
  aEdgeEdgeInter[0].alpha[0]=1.0;
  aEdgeEdgeInter[0].alpha[1]=25;
  
 for (ii=0;ii<nEFinter;ii++){
    //1D to 2D
    aEdgeFieldInter[ii].mu[0]=0.15;
    aEdgeFieldInter[ii].mu[1]=wb?5.0:0.2;
    //2D to 1D
    aEdgeFieldInter[ii].nu[0]=0.375;
    aEdgeFieldInter[ii].nu[1]=wb?6.25:0.0625;
  }
  aEdgePop[0].D[0]=1;
  aEdgePop[0].D[1]=wb?10:0;
  
  printFieldPops(aFieldPop,Nwires);
  
}

/*

pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;




pu1d0s38=1;
pu1d0s37=0;
pu1d0s36=0;
pu1d0s42=0;
pu1d0s41=1;
pu1d0s40=0;
pu1d0s39=0;
pu1d1s20=0;
pu1d1s21=1;
pu1d1s22=0;
pu1d1s23=0;
pu1d1s24=0;
pu1d1s35=1;
pu1d1s37=0;
pu1d1s38=0;
pu1d1s19=1;
pu1d2s0=0;
pu1d2s1=0;
pu1d2s2=1;
pu1d2s3=0;
pu1d2s4=0;
pu1d2s5=1;
pu1d2s6=0;
pu1d2s7=0;
pu1d2s8=1;
pu1d2s9=0;
pu1d2s10=0;
pu1d2s11=0;
pu1d2s12=1;
pu1d2s13=0;
pu1d2s14=0;
pu1d2s15=0;
pu1d2s16=0;
pu1d2s17=1;
pu1d2s18=0;
pu1d2s39=0;
pu1d2s40=0;
pu1d2s41=0;
pu1d2s42=1;
pu1d2s36=0;
pu1d2s35=0;
pu1d2s25=0;
pu1d2s26=0;
pu1d2s27=0;
pu1d2s28=0;
pu1d2s29=0;
pu1d2s30=0;
pu1d2s31=0;
pu1d2s32=0;
pu1d2s33=0;
pu1d2s34=0;
pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;*/
