#include "../defaultMain.h"
#include "../gen2speciesNL.h"

void initStructure(){
  int ii;
  
  NbSpecies=2;
  NDynFieldType=2;
  NDynEdgeType=2;
  nPerLength=5;
  nEFinter=1;
  nEEinter=2;
//  reverseEdges=-1;
  symGeom=1;
  nParams=0;
  BUILD_MESH=1;
  specNames[0]='A';
  specNames[1]='B';
  int wb=1;
  LinearSolver_2D1D=GMRES_2D1D;
  sprintf(GEODIR,"%s/GEODATA/PAYSAGE20",SRCDIR);
  //sprintf(GEODIR,"W3");
  readPoints();
  sprintf(OUTPUTDIR,"%s/../DEMO20FIELDS2SPECIES/",SRCDIR);
  NFilesCopy=2;
  sprintf(FILES_COPY[0],"%s/Demo20fields2species/*.edp",SRCDIR);
  sprintf(FILES_COPY[1],"%s/Demo20fields2species/*.txt",SRCDIR);
  
  readEdges();
  buildEdges();
  readPointsInEdges();
  for (ii=0;ii<NEdges;ii++)
     printEdge(EDGES+ii);
  readWires();
  printWires();
  
 
  allocStructs();
  FieldTypes[0]=1;
  FieldTypes[10]=1;
  
  double coef=1.0;

  aEdgeEdgeInter[0].alpha[0]=1.0;
  aEdgeEdgeInter[0].alpha[1]=25;
  aEdgeEdgeInter[0].gamma[0]=25;
  aEdgeEdgeInter[0].gamma[1]=0;
  
  aEdgeEdgeInter[1].alpha[0]=1.0;
  aEdgeEdgeInter[1].alpha[1]=25;
  aEdgeEdgeInter[1].gamma[0]=0;
  aEdgeEdgeInter[1].gamma[1]=0;
  
  for (ii=0;ii<nEFinter;ii++){
    //1D to 2D
    aEdgeFieldInter[ii].mu[0]=0.15;
    aEdgeFieldInter[ii].mu[1]=wb?5.0:0.2;
    //2D to 1D
    aEdgeFieldInter[ii].nu[0]=0.375;
    aEdgeFieldInter[ii].nu[1]=wb?6.25:0.0625;
  }
  aEdgePop[0].D[0]=1;
  aEdgePop[0].D[1]=wb?10:0;
  
  printFieldPops(aFieldPop,Nwires);
  
}


int main(int argc, char *argv[]){
  return defaultMain(argc,argv);
}

