import vtk
import os
from array import array
#baseName="/home/olivierb/VTK_PYTHON/diff/u"
#species=array('c','uv')

#numStep=1998
#for num in range(1,150):
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(500,500)

if (os.path.isfile('buildActorWires.py')):
    print "wires actor has been generated"
else:
    print "WARNING: wires actor has not been generated"
    print "RUN: FF buildWire.edp"
    quit()

nbStep=50
step=20
nbOmega=2
WinSizeWidth=1400
WinSizeHeight=1000
firstStep=1
imgSuffix=".jpg"
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(WinSize,WinSize)
rgb=[0,0,0]
execfile("vtktopngGen.py")
os.chdir(OUTPUTDIR+"/D2");
finput = open("input.txt", 'w')
rg=array('i',[1,2])
INTCURSPEC=0
INTCURSTEP=firstStep
if (os.path.isfile('curspec.txt')):
    fcurspec = open("curspec.txt", 'r')
    for line in fcurspec:
        for x in line.split():
            INTCURSPEC=int(x)
    fcurspec.close()

if (os.path.isfile('curstep.txt')):
    fcurstep = open("curstep.txt", 'r')
    for line in fcurstep:
        for x in line.split():
            INTCURSTEP=int(x)-1
    fcurstep.close();

print "CURSPEC "+str(INTCURSPEC)
print "CURSTEP "+str(INTCURSTEP)
    
numSpec=0
for spec in species:
    if (numSpec < INTCURSPEC):
        print "pass spec "+str(numSpec)
        numSpec=numSpec+1
        continue
    baseName=OUTPUTDIR+"/D2/"+spec
    for numStep in range(INTCURSTEP,nbStep):
        print "vtktojpg "+str(numSpec)+" "+str(numStep)
        fenetre=vtk.vtkRenderWindow()
        fenetre.SetSize(WinSizeWidth,WinSizeHeight)
        ren=vtk.vtkRenderer()
        fenetre.AddRenderer(ren)
        execfile("../buildActorWires.py")
        execfile("../addActorWires.py")
    #execfile("buildActorWires2.py")
    #execfile("addActorWires2.py")
    #execfile("buildActorWires1.py")
    #execfile("addActorWires1.py")
        ren.SetBackground(1,1,1)
        #ren.ResetCamera(-2.1,6.1,44,49,0,0.1)
        for numOmega in range(0,nbOmega):
            fileName=baseName+str(numOmega)+"s"+str(numStep*step)+".vtk"
            r=vtk.vtkGenericDataObjectReader()
            r.SetFileName(fileName)
            r.Update()
            o=r.GetOutput()
            o.GetCellData().SetActiveScalars("UU")
            aPolyVertexMapper = vtk.vtkDataSetMapper()
            aPolyVertexMapper.SetInput(o)
            aPolyVertexMapper.SetScalarRange(minV[numSpec],maxV[numSpec])
            aPolyVertexMapper.UseLookupTableScalarRange=1
            lut=aPolyVertexMapper.GetLookupTable()
            lut.SetHueRange(251.0/360.0,0)
            lut.SetSaturationRange(1,1)
            lut.SetValueRange(1,1)
            lut.Build()
            aPolyVertexActor = vtk.vtkActor()
            aPolyVertexActor.SetMapper(aPolyVertexMapper)
            ren.AddActor(aPolyVertexActor)
        
    
    #iren=vtk.vtkRenderWindowInteractor()
    #iren.SetRenderWindow(fenetre)
    #iren.Initialize()
        fenetre.Render()
        image = vtk.vtkWindowToImageFilter()
        image.SetInput(fenetre)
        image.Update()
        fenetre.Start()
        fenetre.Render()
        fenetre.Finalize()
        image.UpdateInformation()
        image.Update()
    #iren.Render()
    #iren.Render()
    #iren.Render()
        #jpg = vtk.vtkTIFFWriter()
        if (imgSuffix==".jpg"):
            jpg = vtk.vtkJPEGWriter()
        else:
            jpg = vtk.vtkTIFFWriter()
        fileStepName="step"+spec+str(numStep)+imgSuffix
        jpg.SetFileName(fileStepName)
        jpg.SetInputConnection(image.GetOutputPort())
        jpg.Write()
        fcurstep = open("curstep.txt", 'w')
        fcurstep.write(str(numStep))
        fcurstep.close()
        
    numSpec+=1
    INTCURSTEP=firstStep
    fcurspec = open("curspec.txt", 'w')
    fcurspec.write(str(numSpec))
    fcurspec.close()

for numStep in range(firstStep,nbStep):
    jpgfiles=""
    print "merge u v "+str(numStep)
    for spec in species:
        fileStepName="step"+spec+str(numStep)+imgSuffix
	#os.system("convert -crop 500x250+0+125 "+fileStepName+" step"+spec+str(numStep)+"c"+imgSuffix)
	#fileStepName="step"+spec+str(numStep)+"c"+imgSuffix
        #os.system("convert -resize 40% "+fileStepName+" step"+spec+str(numStep)+"cr"+imgSuffix)
        os.system("convert  "+fileStepName+" step"+spec+str(numStep)+"cr"+imgSuffix)
        fileStepName="step"+spec+str(numStep)+"cr"+imgSuffix
        jpgfiles=jpgfiles+" "+fileStepName
    os.system("convert "+jpgfiles+" "+"-append step"+str(numStep)+imgSuffix)
    finput.write("step"+str(numStep)+imgSuffix+"\n")


finput.close()
#os.system("mencoder \"mf://@input.txt\" -mf fps=20 -o test.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800")
print "mencoder \"mf://@input.txt\" -mf fps=20 -o test.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800"
#os.system("vlc test.avi")
   


