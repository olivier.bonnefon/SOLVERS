library(maps)
france<-map(database="france")
#2003
color <- match.map(france,"",exact=TRUE)
color[c(14,21,58,16,95)]=3;
map(database="france", fill=TRUE, col=color)
#2004
color[c(14,18)]=7;
color[95]=2;
map(database="france", fill=TRUE, col=color)
#2005
color[c(14,21,58,16,18,102,100,93,96,90,85,83,92)]=3;
color[95]=2;
map(database="france", fill=TRUE, col=color)
#2006
color[1:114]=3;
color[14]=7;
color[c(95,102)]=2;
map(database="france", fill=TRUE, col=color)
#2007
color[14]=3;
map(database="france", fill=TRUE, col=color)
#2008
map(database="france", fill=TRUE, col=color)
#2009
color[1:114]=3;
color[c(100,93,96,83,77,76,70,65,113)]=7;
color[c(95,102)]=2;
map(database="france", fill=TRUE, col=color)
#2010
color[1:114]=3;
color[c(100,93,96,83,77,76,70,65,113,85,90,59,92,99,105)]=7;
color[c(95,102,100)]=2;
map(database="france", fill=TRUE, col=color)
#2011
color[1:114]=3;
color[c(100,93,96,83,77,76,70,65,113,85,90,59,92,99,105,80,89)]=7;
color[c(95,102,100,90,92,99)]=2;
map(database="france", fill=TRUE, col=color)
#2012
color[1:114]=3;
color[c(100,93,96,83,77,76,70,65,113,85,90,59,92,99,105,80,89,56,104,101,88)]=7;
color[c(95,102,100,90,92,99,93,96,89)]=2;
map(database="france", fill=TRUE, col=color)
#2013
color[1:114]=3;
color[c(100,93,96,83,77,76,70,65,113,85,90,59,92,99,105,80,89,104,101,43,38,69)]=7;
color[c(95,102,100,90,92,99,93,96,89,105,113,101,84,83,77,70)]=2;
map(database="france", fill=TRUE, col=color)
#2014
color[1:114]=3;
color[c(100,93,96,83,77,76,70,65,113,90,59,92,99,105,80,89,104,101,88,66,38,69,16,18,58,14,21)]=7;
color[c(95,102,100,90,92,99,93,96,89,113,101,84,83,77,70)]=2;
map(database="france", fill=TRUE, col=color)
#Janvier 2015
color[1:114]=3;
color[c(100,93,96,83,77,76,70,65,113,90,59,92,99,105,80,89,104,101,88,66,38,69,16,18,58,14,21,56,91,60,61,19)]=7;
color[c(95,102,100,90,92,99,93,96,89,113,101,84,83,77,70,105)]=2;
