import matplotlib.mlab as mlab
import numpy as np
import sys
import subprocess
from matplotlib.pyplot import *
import os.path

#geodirl=["../GEODATA/PAYSAGE20/"]
#geodirl=["../GEODATA/FRA_DEP/"]
#geodirl=["../GEODATA/TESTMERGE/"]
#geodirl=["../GEODATA/SELOMMES/"]
#geodirl=["../GEODATA/AUTOROUTES/"]
geodirl=["../GEODATA/SPOGGI/"]
import Edge as Edge
import Wire as Wire
xmin=1000000000
xmax=-1000000000
ymin=1000000000
ymax=-1000000000

Emin=-249
Emax=252
Wmin=-140
Wmax=1000

showWires=1
showSave=0
for geodir in geodirl:
    line = subprocess.check_output(['wc', '-l', geodir+"Points.txt"])
    aux=line.strip().split()
    nbPt=int(aux[0])+1
    Points=np.zeros((nbPt,3))
    labelPt=[]
    labelPt.append("bidon")
    with open(geodir+"Points.txt", "r") as dataFile:
        for line in dataFile:
            aux = line.strip().split()
            numPt=int(aux[0])
            print "Point="+str(numPt)
            Points[numPt,0]=float(aux[1])
            Points[numPt,1]=float(aux[2])
            if (float(aux[2]) > ymax):
                ymax=float(aux[2])
            if (float(aux[1]) > xmax):
                xmax=float(aux[1])
            if (float(aux[2]) < ymin):
                ymin=float(aux[2])
            if (float(aux[1]) < xmin):
                xmin=float(aux[1])
            labelPt.append(aux[3])
    
    Edges=[]
    aE=Edge.Edge(0,0,0,0,"",0)
    Edges.append(aE)
    cmpE=1
    if (showSave):
        edgefile=geodir+"EdgesSaved.txt"
    else:
        edgefile=geodir+"Edges.txt"
    with open(edgefile, "r") as dataFile:
        for line in dataFile:
            aux = line.strip().split()
            typeEdge=int(aux[0])
            print "Edge="+str(aux)
            numNb=int(aux[1])
            numNe=int(aux[2])
            numPt=int(aux[3])
            label=aux[3+1+numPt]
            numEdge=int(aux[3+1+numPt+1])
            aE=Edge.Edge(numEdge,numNb,numNe,numPt,label,typeEdge)
            Edges.append(aE)
            num=1
            while (num<=numPt):
                aE.add_pt(aux[3+num])
                num=num+1
            if (cmpE>=Emin and cmpE<=Emax):
                aE.plot(Points,labelPt,1)
            cmpE=cmpE+1

    Wires=[]
    cmpW=1
    WireDrawPoint=1
    WireEmptyPoly=0
    WireWithName=1
    if (showSave):
        wireFile=geodir+"WiresSaved.txt"
    else:
        wireFile=geodir+"Wires.txt"
    if (os.path.exists(wireFile) and showWires):
        with open(wireFile, "r") as dataFile:
            for line in dataFile:
                aux = line.strip().split()
                print "Wire="+str(aux)
                numW=int(aux[0])
                nbE=int(aux[1])
                W=Wire.Wire(numW,nbE,"W1")
                num=1
                print "numW="+str(numW)+"nb Edges="+str(nbE)
                while(num<=nbE):
                    print "rep "+str(num)+" "+str(nbE)
                    reverse=0
                    numE=int(aux[1+num])
                    print numE
                    if (numE<-0.1):
                        reverse=1
                        numE=-numE
                    print "num edge="+str(numE)
                    E=Edges[numE]
                    W.add_edge(Edges[numE],reverse)
                    if (WireDrawPoint and cmpW>=Wmin and cmpW<=Wmax):
                        Edges[numE].plot(Points,labelPt,2)
                    num=num+1
                W.label=aux[num+1]
                if (cmpW>=Wmin and cmpW<=Wmax):
                    W.plot(Points,WireEmptyPoly,WireDrawPoint,WireWithName)
                cmpW=cmpW+1
    else:
        print wireFile+" doesn t exist"
        
    

xlim([xmin, xmax])
ylim([ymin, ymax])

axis('off')
savefig("res.eps")
show()
