import matplotlib.mlab as mlab
import numpy as np
import sys
import subprocess
from matplotlib.pyplot import *
import os.path

geodirl=["../GEODATA/CORSESUD/"]
import Edge as Edge
import Wire as Wire
xmin=1000000000
xmax=-1000000000
ymin=1000000000
ymax=-1000000000

Emin=-60
Emax=210
Wmin=10
Wmax=200

showWires=1

for geodir in geodirl:
    line = subprocess.check_output(['wc', '-l', geodir+"Points.txt"])
    aux=line.strip().split()
    nbPt=int(aux[0])+1
    Points=np.zeros((nbPt,3))
    labelPt=[]
 
    with open(geodir+"Points.txt", "r") as dataFile:
        for line in dataFile:
            aux = line.strip().split()
            numPt=int(aux[0])
            print "Point="+str(numPt)
            Points[numPt,0]=float(aux[1])
            Points[numPt,1]=float(aux[2])
            if (float(aux[2]) > xmax):
                xmax=float(aux[2])
            if (float(aux[1]) > ymax):
                ymax=float(aux[1])
            if (float(aux[2]) < xmin):
                xmin=float(aux[2])
            if (float(aux[1]) < ymin):
                ymin=float(aux[1])
            labelPt.append(aux[3])

    Edges=[]
    cmpE=1
    with open(geodir+"Edges.txt", "r") as dataFile:
        for line in dataFile:
            aux = line.strip().split()
            typeEdge=int(aux[0])
            print "Edge="+str(aux)
            numNb=int(aux[1])
            numNe=int(aux[2])
            numPt=int(aux[3])
            label=aux[3+1+numPt]
            numEdge=int(aux[3+1+numPt+1])
            aE=Edge.Edge(numEdge,numNb,numNe,numPt,label)
            Edges.append(aE)
            num=1
            while (num<=numPt):
                aE.add_pt(aux[3+num])
                num=num+1
            if (cmpE>=Emin and cmpE<=Emax):
                aE.plot(Points,labelPt,3)
            cmpE=cmpE+1


    Wires=[]
    cmpW=1
    WireDrawPoint=0
    WireEmptyPoly=0
    WireWithName=1
    if (os.path.exists(geodir+"Wires.txt") and showWires):
        with open(geodir+"Wires.txt", "r") as dataFile:
            for line in dataFile:
                aux = line.strip().split()
                print "Wire="+str(aux)
                numW=int(aux[0])
                nbE=int(aux[1])
                W=Wire.Wire(numW,nbE,"W1")
                num=1
                while(num<=nbE):
                    reverse=0
                    numE=int(aux[1+num])
                    print numE
                    if (numE<0):
                        reverse=1
                        numE=-numE
                    print "num edge="+str(numE)
                    E=Edges[numE]
                    W.add_edge(Edges[numE],reverse)
                    if (WireDrawPoint and cmpW>=Wmin and cmpW<=Wmax):
                        Edges[numE].plot(Points,labelPt,2)
                    num=num+1
                W.label=aux[num+1]
                if (cmpW>=Wmin and cmpW<=Wmax):
                    W.plot(Points,WireEmptyPoly,WireDrawPoint,WireWithName)
                cmpW=cmpW+1
    else:
        print geodir+"Wires.txt doesn t exist"
        
    

xlim([xmin, xmax])
ylim([ymin, ymax])

axis('off')
savefig("res.eps")
show()
