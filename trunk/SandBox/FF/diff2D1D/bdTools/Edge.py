import matplotlib.mlab as mlab
import numpy as np
import matplotlib.pyplot as plt

fontPointSize=12
fontEdgeSize=12
offset=0.01
class Edge:
    
    def __init__(self, id,ptb,pte,nbPt,label,edtype):
        self.id=id
        self.ptb=ptb
        self.pte=pte
        self.nbPt=nbPt
        self.pts=[]
        self.label=label
        self.edtype=edtype
        print "edges builded "+self.label

    def add_pt(self,pt):
        self.pts.append(pt)
    

    def plot(self,Points,labelPt,withLabel):
        ptE=np.zeros((self.nbPt+2,2))
        ptE[0,0]=Points[self.ptb,0]
        ptE[0,1]=Points[self.ptb,1]
        ptE[self.nbPt+1,0]=Points[self.pte,0]
        ptE[self.nbPt+1,1]=Points[self.pte,1]
        num=1
        print "begin plot edge ,self.ptb="+str(self.ptb)
        while(num<=self.nbPt):
            npt=int(self.pts[num-1])
            ptE[num,0]=Points[npt,0]
            ptE[num,1]=Points[npt,1]
#            print "repere "+str(ptE[num,0])+" "+str(ptE[num,1])
            num=num+1
            if (withLabel>=3):
                plt.text(Points[npt,0], Points[npt,1],labelPt[npt]+"-"+str(npt), fontsize=fontPointSize)
        if (withLabel>=2):
            plt.text(Points[self.ptb,0], Points[self.ptb,1]-offset,"B-"+labelPt[self.ptb]+"-"+str(self.ptb), fontsize=fontPointSize)
            #plt.text(Points[self.ptb,1], Points[self.ptb,0]-offset,"B-"+labelPt[self.ptb]+"-"+str(self.ptb), fontsize=fontPointSize)
            #plt.text(Points[self.pte,1], Points[self.pte,0]+offset,"E-"+self.label+labelPt[self.pte]+"-"+str(self.pte), fontsize=fontPointSize)
        myLineW=1.0;
        if (self.id>=1):
            myLineW=3.0;
        if (withLabel>=1):    
            plt.plot(ptE[:,0],ptE[:,1],'o',ptE[:,0],ptE[:,1], linewidth=myLineW)
        else:
            plt.plot(ptE[:,0],ptE[:,1], linewidth=myLineW,color='black')
        if (withLabel>=1):
            #plt.text(0.85*Points[self.ptb,1]+0.15*Points[self.pte,1],0.85* Points[self.ptb,0]+0.15*Points[self.pte,0],"B-"+self.label+"-"+str(self.id), fontsize=fontEdgeSize)
            plt.text(0.15*Points[self.ptb,0]+0.85*Points[self.pte,0],0.15* Points[self.ptb,1]+0.85*Points[self.pte,1],"E-"+self.label+"-"+str(self.id)+"-"+str(self.edtype), fontsize=fontEdgeSize)

