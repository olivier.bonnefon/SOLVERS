#define WITH_GRADIENT
int NbSpecies=4;
int NDynFieldType=1;
int NDynEdgeType=0;
double nPerLength=3e-5;
int nEFinter=0;
int nEEinter=0;
int whithSource=0;
int whithControl=0;
char FILES_COPY[200][128];
int NFilesCopy=1;
int nParams=16;
void initStructure(){
  int ii,type;
  int wb=1;
  double s=40;
  double m=-20;
  double coef=1;
  //param model
  double K=1.0;
  double r=1e-3;
  double mu=5;//1D to 2D
  double nu=10.0;//2D to 1D
  double D1d=10;
  double D2d=1e-6;
  FILE *edgesFile;
  char *line=NULL;
  reverseEdges=1;
  ssize_t read;
  size_t len = 0;
  specNames[0]='X';
  specNames[1]='Y';
  specNames[2]='Z';
  specNames[3]='C';
  
  sprintf(GEODIR,"US");
  readPoints();
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/VACHERTESTFUNC/");
  sprintf(POSTPRODIR,"/home/olivierb/solvers/trunk/SandBox/FF/Vacher/");
  sprintf(FILES_COPY[0],"GRAD/*");
  
  readEdges();
  buildEdges();
  readPointsInEdges();
  for (ii=0;ii<NEdges;ii++)
     printEdge(EDGES+ii);
  readWires();
  printWires();
  
 
  allocStructs();
  
  //A
  aFieldPop[0].D2DX[0]=D2d;
  aFieldPop[0].D2DY[0]=D2d;
  //B
  aFieldPop[0].D2DX[1]=D2d;
  aFieldPop[0].D2DY[1]=D2d;
  //P
  aFieldPop[0].D2DX[2]=D2d;
  aFieldPop[0].D2DY[2]=D2d;
  //C
  aFieldPop[0].D2DX[3]=D2d;
  aFieldPop[0].D2DY[3]=D2d;
 
  printFieldPops(aFieldPop,Nwires);
  
}




