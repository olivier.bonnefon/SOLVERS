#define WITH_GRADIENT
#define INTER_ANNUELLE
int NbSpecies=1;
int NDynFieldType=1;
int NDynEdgeType=2;
#ifdef INTER_ANNUELLE
double nPerLength=200/1e6;
#else
double nPerLength=20;
#endif
int nEFinter=2;
int nEEinter=1;
int NFilesCopy=115+14;
char FILES_COPY[200][128];
int BUILD_MESH=1;
int nParams=0;
#ifdef INTER_ANNUELLE
#define MESH_DIR "meshes/LAMBERTIIE/"
#else
#define MESH_DIR "meshes/WGS84/"
#endif
#include "tigerFileToCopy.h"
void initStructure(){
  LinearSolver_2D1D=GMRES_2D1D;
  slowStart=1;
  int ii,type;
  int wb=1;
  double s=40;
  double m=-20;
  double coef=1;
  //param model
  double K=1.0;
  double r=1e-3;
  double mu=5;//1D to 2D
  double nu=10.0;//2D to 1D
  double D1d=10;
  double D2d=1e-6;
  FILE *edgesFile;
  char *line=NULL;
  ssize_t read;
  size_t len = 0;
  sprintf(GEODIR,"AUTOROUTES");
//  sprintf(GEODIR,"SIMPLE1");
  preparMeshesCopy();
  sprintf(FILES_COPY[115+0],"PT%s/postTraitementLoad.edp",GEODIR);
  sprintf(FILES_COPY[115+1],"PT%s/postTraitement.edp",GEODIR);
  sprintf(FILES_COPY[115+2],"PT%s/postTraitementEnd.edp",GEODIR);
#ifdef INTER_ANNUELLE
  sprintf(FILES_COPY[115+3],"intraAnnuel/cas1.edp");
  sprintf(FILES_COPY[115+4],"intraAnnuel/inputParam.edp");
  sprintf(FILES_COPY[115+6],"intraAnnuel/timeSteppingGen.edp");
  sprintf(FILES_COPY[115+7],"intraAnnuel/timeSteppingOneStepGen.edp");
  sprintf(FILES_COPY[115+5],"intraAnnuel/defICGen.edp");
#else
  sprintf(FILES_COPY[115+3],"cas1.edp");
  sprintf(FILES_COPY[115+4],"inputParam.edp");
  sprintf(FILES_COPY[115+7],"timeSteppingGen.edp");
  sprintf(FILES_COPY[115+5],"defICGen.edp");
#endif
  sprintf(FILES_COPY[115+12],"PT%s/postTraitementInit.edp",GEODIR);
  sprintf(FILES_COPY[115+13],"preTraitement.edp");
  sprintf(FILES_COPY[115+8],"viewMeshes.edp");
  sprintf(FILES_COPY[115+9],"defParamsGen.edp");
  sprintf(FILES_COPY[115+10],"userFDef0.txt");
  sprintf(FILES_COPY[115+11],"Readme.txt");
  readPoints();
#ifdef INTER_ANNUELLE
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/AEDESINTERBIS_%s/",GEODIR);
#else
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/AEDESWGS84_%s/",GEODIR);
#endif
  sprintf(POSTPRODIR,"/home/olivierb/solvers/trunk/SandBox/FF/AedesAlbopictus/");
  readEdges();
  buildEdges();
  readPointsInEdges();
  for (ii=0;ii<NEdges;ii++)
     printEdge(EDGES+ii);
  readWires();
  printWires();
  
  //exit(0);
  /*double scal=1;
  for (ii=0;ii<NEdges;ii++){
    Nx[ii]=scal* Nx[ii];
    Ny[ii]=scal* Ny[ii];
    }*/

 
  allocStructs();
  

  
  aFieldPop[0].D2DX[0]=D2d;
  aFieldPop[0].D2DY[0]=D2d;
  aFieldPop[0].K[0]=K;
  aFieldPop[0].R[0]=r;
  aFieldPop[0].T[0]=0;
//  aFieldPop[1].T[0]=0;
  aFieldPop[0].rho[0]=0.0;
//  aFieldPop[1].rho[0]=0.0;
  
  aFieldPop[0].Cmalthus[0]=0;
  aFieldPop[0].source[0]=0.00;
  
  aEdgePop[0].D[0]=0;
  aEdgePop[0].indexEFInter=0;
  
  aEdgePop[1].D[0]=D1d;
  aEdgePop[1].indexEFInter=1;
  
    
  //no 2D/1D flux
  aEdgeFieldInter[0].mu[0]=0;
  aEdgeFieldInter[0].nu[0]=0;
  //2D/1D flux
  aEdgeFieldInter[1].mu[0]=mu;
  aEdgeFieldInter[1].nu[0]=nu;
  aEdgeEdgeInter[0].alpha[0]=1000;
  
  edgesFile = openEdgesFile();
  ii=0;
  while ((read = getline(&line, &len, edgesFile)) != -1) { 
    sscanf(line,"%i",&type);
    EDGES[ii].popType=1;
    if (!type){
      EDGES[ii]._bcinf.type=ALONE;
      EDGES[ii]._bcsup.type=ALONE;
      EDGES[ii].popType=0;
    }
    if (type==2){
      EDGES[ii]._bcinf.type=DIRICHLET;
      EDGES[ii]._bcinf._value=1.0;
    }
    if (type==3){
      EDGES[ii]._bcsup.type=DIRICHLET;
      EDGES[ii]._bcsup._value=1.0;
    }
        //EDGES[ii].couple1D=type;
    
    ii++;
  }
  fclose(edgesFile);

  printFieldPops(aFieldPop,Nwires);
  
}

/*

pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;




pu1d0s38=1;
pu1d0s37=0;
pu1d0s36=0;
pu1d0s42=0;
pu1d0s41=1;
pu1d0s40=0;
pu1d0s39=0;
pu1d1s20=0;
pu1d1s21=1;
pu1d1s22=0;
pu1d1s23=0;
pu1d1s24=0;
pu1d1s35=1;
pu1d1s37=0;
pu1d1s38=0;
pu1d1s19=1;
pu1d2s0=0;
pu1d2s1=0;
pu1d2s2=1;
pu1d2s3=0;
pu1d2s4=0;
pu1d2s5=1;
pu1d2s6=0;
pu1d2s7=0;
pu1d2s8=1;
pu1d2s9=0;
pu1d2s10=0;
pu1d2s11=0;
pu1d2s12=1;
pu1d2s13=0;
pu1d2s14=0;
pu1d2s15=0;
pu1d2s16=0;
pu1d2s17=1;
pu1d2s18=0;
pu1d2s39=0;
pu1d2s40=0;
pu1d2s41=0;
pu1d2s42=1;
pu1d2s36=0;
pu1d2s35=0;
pu1d2s25=0;
pu1d2s26=0;
pu1d2s27=0;
pu1d2s28=0;
pu1d2s29=0;
pu1d2s30=0;
pu1d2s31=0;
pu1d2s32=0;
pu1d2s33=0;
pu1d2s34=0;
pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;*/
