#define WITH_GRADIENT
int NbSpecies=4;
int NDynFieldType=1;
int NDynEdgeType=0;
double nPerLength=3e-5;
int nEFinter=0;
int nEEinter=0;
int whithSource=0;
int whithControl=0;
char FILES_COPY[200][128];
int NFilesCopy=9;
int nParams=16;
void initStructure(){
  int ii,type;
  int wb=1;
  double s=40;
  double m=-20;
  double coef=1;
  //param model
  double K=1.0;
  double r=1e-3;
  double mu=5;//1D to 2D
  double nu=10.0;//2D to 1D
  double D1d=10;
  double D2d=1e-6;
  FILE *edgesFile;
  char *line=NULL;
  reverseEdges=1;
  ssize_t read;
  size_t len = 0;
  specNames[0]='X';
  specNames[1]='Y';
  specNames[2]='Z';
  specNames[3]='C';
  
  sprintf(FILES_COPY[0],"postTraitementLoad.edp");
  sprintf(FILES_COPY[1],"postTraitement.edp");
  sprintf(FILES_COPY[2],"postTraitementEnd.edp");
  sprintf(FILES_COPY[3],"exportSommets.edp");//callJ
  sprintf(FILES_COPY[4],"exportSommets.edp");
  sprintf(FILES_COPY[5],"inputParams.edp");
  sprintf(FILES_COPY[6],"main.edp");
  sprintf(FILES_COPY[7],"Z1.edp");
  sprintf(FILES_COPY[8],"Z2.edp");
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/VACHER16P/");
  sprintf(POSTPRODIR,"/home/olivierb/solvers/trunk/SandBox/FF/Vacher/");
  
  Nwires=1;
  Wsizes=(int*)calloc(Nwires,sizeof(int));

  allocStructs();
  
  //A
  aFieldPop[0].D2DX[0]=D2d;
  aFieldPop[0].D2DY[0]=D2d;
  //B
  aFieldPop[0].D2DX[1]=D2d;
  aFieldPop[0].D2DY[1]=D2d;
  //P
  aFieldPop[0].D2DX[2]=D2d;
  aFieldPop[0].D2DY[2]=D2d;
  //C
  aFieldPop[0].D2DX[3]=D2d;
  aFieldPop[0].D2DY[3]=D2d;

  
  printFieldPops(aFieldPop,Nwires);
  
}

/*

pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;




pu1d0s38=1;
pu1d0s37=0;
pu1d0s36=0;
pu1d0s42=0;
pu1d0s41=1;
pu1d0s40=0;
pu1d0s39=0;
pu1d1s20=0;
pu1d1s21=1;
pu1d1s22=0;
pu1d1s23=0;
pu1d1s24=0;
pu1d1s35=1;
pu1d1s37=0;
pu1d1s38=0;
pu1d1s19=1;
pu1d2s0=0;
pu1d2s1=0;
pu1d2s2=1;
pu1d2s3=0;
pu1d2s4=0;
pu1d2s5=1;
pu1d2s6=0;
pu1d2s7=0;
pu1d2s8=1;
pu1d2s9=0;
pu1d2s10=0;
pu1d2s11=0;
pu1d2s12=1;
pu1d2s13=0;
pu1d2s14=0;
pu1d2s15=0;
pu1d2s16=0;
pu1d2s17=1;
pu1d2s18=0;
pu1d2s39=0;
pu1d2s40=0;
pu1d2s41=0;
pu1d2s42=1;
pu1d2s36=0;
pu1d2s35=0;
pu1d2s25=0;
pu1d2s26=0;
pu1d2s27=0;
pu1d2s28=0;
pu1d2s29=0;
pu1d2s30=0;
pu1d2s31=0;
pu1d2s32=0;
pu1d2s33=0;
pu1d2s34=0;
pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;*/
