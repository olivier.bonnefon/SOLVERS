#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
struct edge* EDGES=NULL;

char FILENAME[512];
// 1 for left material.
// -1 for right material.
int reverseEdges=-1;

//0 do nothing
//1 x <--> y
int symGeom=0;
int nTotalEdges=0;

#include "geomParser.h"
#include "edgeTools.h"
char  GEODIR[128];

void saveEdges();
void saveWires();
void removeEdge(int iRemEdges);
int edgesIsUsed(int iE);



int edgesIsUsed(int iE){
  struct wireSide* pW=WIRES;
  for (int ii=0;ii<Nwires;ii++)
     for (int jj=0;jj<Wsizes[ii];jj++){
       if (pW->indexEdge == iE )
         return 1;
     }
  return 0;
}

int edgesIsShared(int iE){
  struct wireSide* pW=WIRES;
  for (int ii=0;ii<Nwires;ii++)
     for (int jj=0;jj<Wsizes[ii];jj++){
       if (pW->indexEdge == iE && pW->isReversed)
         return 1;
       pW++;
     }
  return 0;
}
int curWire;
//avoid demi tour
int canAddNodeInEdges(struct edge* pE,int numPoint){
  if (pE->geom.nPoints<2)
    return 1;
  else{
    //if (curWire!=75)
    //  return 0;
    int bP=pE->geom.indicesPoints[0];
    int eP=pE->geom.indicesPoints[pE->geom.nPoints-1];
    double vx,vy;
    vx=PointsX[numPoint]-PointsX[eP];
    vy=PointsY[numPoint]-PointsY[eP];
    double dnorm1=sqrt(vx*vx+vy*vy);
    double vxx,vyy;
    vxx=PointsX[eP]-PointsX[bP];
    vyy=PointsY[eP]-PointsY[bP];
    double dnorm2=sqrt(vxx*vxx+vyy*vyy);
    double cosA=(vx*vxx+vy*vyy)/(dnorm1*dnorm2);
    if (cosA <= 0.25)
      return 0;
  }
  return 1;
  
}
void copyEdges(struct edge* eTarget,struct edge* eSource){
  *eTarget=*eSource;
  eTarget->geom.indicesPoints=(int*)malloc(eTarget->geom.nPoints*sizeof(int));
  memcpy(eTarget->geom.indicesPoints,eSource->geom.indicesPoints,eTarget->geom.nPoints*sizeof(int));
}
void mergeNoSharedEdges(){

  struct wireSide* pW=WIRES;
  struct wireSide* beginCurPoly=0;
  int* newWsizes=(int*)malloc(Nwires*sizeof(int));
  struct wireSide* pNewW=(struct wireSide*)malloc(nTotalEdges*sizeof(struct wireSide));
  struct edge* newEDGES=(struct edge*)malloc(NEdges*sizeof(struct edge));
  int curIndexEdge=0;
  int* sav1=newWsizes;
  struct wireSide* sav2=pNewW;
  struct edge* sav3=newEDGES;
  struct edge curMergedEdge;
  curMergedEdge.popType=0;
  curMergedEdge.geom.indicesPoints=(int*)malloc(NPoints*sizeof(int));
  //isShared is used to now if a edge has been added
  //idInGeomFile will be used to remind the index of this edge
  int ii,jj,kk;
  for (ii=0;ii<NEdges;ii++)
    EDGES[ii]._isShared=0;
  
  beginCurPoly=WIRES;
  
  for (ii=0;ii<Nwires;ii++){
    curWire=ii;
    pW=beginCurPoly;
    //looking for shared edges
    for (jj=0;jj<Wsizes[ii];jj++){
      if (edgesIsShared(pW->indexEdge)){
        break;
      }
      pW++;
    }
    
    int indexOfBeginNewPolygone=jj;
    curMergedEdge.geom.nPoints=0;
    
//    curMergedEdge.indicesPoints[curMergedEdge.geom.nPoints-1]=GET_EDGE_NODE_BEGIN(abs(pW->indexEdge));
    newWsizes[ii]=0;
    pW=beginCurPoly;
    for(kk=indexOfBeginNewPolygone;kk<indexOfBeginNewPolygone+Wsizes[ii];kk++){
      jj=kk%Wsizes[ii];
      pW=beginCurPoly+jj;
      if (edgesIsShared(pW->indexEdge)){//add pW in polygone description
        if (curMergedEdge.geom.nPoints!=0){//add current merged edge
          copyEdges(newEDGES+curIndexEdge,&curMergedEdge);
          curMergedEdge.geom.nPoints=0;
          
          pNewW->indexEdge=curIndexEdge;
          pNewW->isReversed=0;
          curIndexEdge++;
          pNewW++;newWsizes[ii]++;
          
        }
        //add shared edge
        if (EDGES[pW->indexEdge]._isShared){//edge pW is yet registred
         
          pNewW->indexEdge=EDGES[pW->indexEdge].geom.idInGeomFile;
          pNewW->isReversed=pW->isReversed;
             
          pNewW++;newWsizes[ii]++;
        }else{//edge pW is not registred
          copyEdges(newEDGES+curIndexEdge,EDGES+pW->indexEdge);
          EDGES[pW->indexEdge]._isShared=1;
          EDGES[pW->indexEdge].geom.idInGeomFile=curIndexEdge;
          
          pNewW->isReversed=pW->isReversed;
           
          pNewW->indexEdge=curIndexEdge;
          curIndexEdge++;
          pNewW++;newWsizes[ii]++;
          
        }
      }else{//pW is not shared
       
        if (canAddNodeInEdges(&curMergedEdge,GET_EDGE_NODE_END(pW->indexEdge))){//merge pW in curMergedEdge
          int startI=1;
          if (curMergedEdge.geom.nPoints==0)
            startI=0;
          for (int lkk=startI;lkk< EDGES[pW->indexEdge].geom.nPoints;lkk++){
            curMergedEdge.geom.indicesPoints[curMergedEdge.geom.nPoints]=EDGES[pW->indexEdge].geom.indicesPoints[lkk];
            curMergedEdge.geom.nPoints++;
          }
        }else{//flush curMergedEge and pW --> curMergedEge
          copyEdges(newEDGES+curIndexEdge,&curMergedEdge);
          
          pNewW->indexEdge=curIndexEdge;pNewW->isReversed=0;
          curIndexEdge++;
          pNewW++;newWsizes[ii]++;
          
          
          curMergedEdge.geom.nPoints=0;
          for (int lkk=0;lkk< EDGES[pW->indexEdge].geom.nPoints;lkk++){
            curMergedEdge.geom.indicesPoints[lkk]=EDGES[pW->indexEdge].geom.indicesPoints[lkk];
            curMergedEdge.geom.nPoints++;
          }
          curMergedEdge.geom.nPoints=EDGES[pW->indexEdge].geom.nPoints;
        }
      }
    }
    //the last
    if (curMergedEdge.geom.nPoints!=0){
      //curMergedEdge.geom.indicesPoints[curMergedEdge.geom.nPoints]=EDGES[pW->indexEdge].geom.indicesPoints[EDGES[pW->indexEdge].geom.nPoints-1];
      //curMergedEdge.geom.nPoints++;
      copyEdges(newEDGES+curIndexEdge,&curMergedEdge);
      
      pNewW->indexEdge=curIndexEdge;pNewW->isReversed=0;
      newWsizes[ii]++;
      curIndexEdge++;
      pNewW++;
    }
    beginCurPoly=beginCurPoly+Wsizes[ii];
  }
  
  free(curMergedEdge.geom.indicesPoints);
  Wsizes=sav1;
  WIRES=sav2;
  EDGES=sav3;
  NEdges=curIndexEdge;
  printWires();
}

void markSmallEdges(double threshold){
  int ii;
  int nWillRemEdges=0;
  for (ii=0;ii<NEdges;ii++){
    double dNodes=sqrt((PointsX[GET_EDGE_NODE_END(ii)]-PointsX[GET_EDGE_NODE_BEGIN(ii)])*(PointsX[GET_EDGE_NODE_END(ii)]-PointsX[GET_EDGE_NODE_BEGIN(ii)])+
      (PointsY[GET_EDGE_NODE_END(ii)]-PointsY[GET_EDGE_NODE_BEGIN(ii)])*(PointsY[GET_EDGE_NODE_END(ii)]-PointsY[GET_EDGE_NODE_BEGIN(ii)]));
    if (dNodes < threshold && edgesIsUsed(ii)){
      EDGES[ii].geom._length = -1;
      printf("making edges %i\n",ii+1);
      nWillRemEdges++;
      //break;
    }else
      EDGES[ii].geom._length = 0;
  }
  printf("will remove %i edges used\n",nWillRemEdges);
}

void removeEdge(int iRemEdges){
  //remplacer GET_EDGE_NODE_BEGIN(ii) par GET_EDGE_NODE_END(ii)
  int removedPoint=GET_EDGE_NODE_BEGIN(iRemEdges);
  int keepingPoint=GET_EDGE_NODE_END(iRemEdges);
  int ii,jj;
  for (ii=0;ii<NEdges;ii++){
    if (GET_EDGE_NODE_END(ii) == removedPoint)
      GET_EDGE_NODE_END(ii)=keepingPoint;
    if (GET_EDGE_NODE_BEGIN(ii) == removedPoint)
      GET_EDGE_NODE_BEGIN(ii)=keepingPoint;
  }

//  int nEdgesTotal=?;
  struct wireSide* pW=WIRES;
  int* newWsizes=(int*)malloc(Nwires*sizeof(int));
  struct wireSide* pNewW=(struct wireSide*)malloc(nTotalEdges*sizeof(struct wireSide));
  int* sav1=newWsizes;
   struct wireSide* sav2=pNewW;
  //for all wires
  int edgesUsedSomeWhere=0;
  for (ii=0;ii<Nwires;ii++){
    int edgesUsedinWire=0;
    //for all wire's edges
    for (jj=0;jj<Wsizes[ii];jj++){
      if (pW->indexEdge==iRemEdges){
        edgesUsedSomeWhere=1;
        edgesUsedinWire=1;
        pW++;
        printf("polygone %i has been modified\n",ii+1);
      }else{
        *pNewW=*pW;
        pW++;
        pNewW++;
      }
    }
    if (edgesUsedinWire){
      newWsizes[ii]=Wsizes[ii]-1;
    }else{
      newWsizes[ii]=Wsizes[ii];
    }
  }
  
  free(Wsizes);
  free(WIRES);
  Wsizes=sav1;
  WIRES=sav2;
}

void saveEdges(){
  char filename[256];
  int iE;
  sprintf(filename,"GEODATA/%s/%s",GEODIR,"EdgesSaved.txt");
  FILE * res=fopen(filename,"w");
  if (!res){
    printf("openEdgesWire failed with %s\n",filename);
    return;
  }else
     printf("openEdgesWire ok with %s\n",filename);
  
  for (iE=0;iE<NEdges;iE++){
    fprintf(res,"%i ",EDGES[iE].popType);
    fprintf(res,"%i %i ",GET_EDGE_NODE_BEGIN(iE)+1,GET_EDGE_NODE_END(iE)+1);
    fprintf(res,"%i ",EDGES[iE].geom.nPoints-2);
    for (int i=1;i<EDGES[iE].geom.nPoints-1;i++)
      fprintf(res,"%i ",EDGES[iE].geom.indicesPoints[i]+1);
    fprintf(res,"ed%i %i\n",iE+1,iE+1);
  }
  fclose(res);
}
void saveWires(){
  char filename[256];
  int iW,iE;
  int * lWsize=Wsizes;
  struct wireSide * lWires=WIRES;
  
  sprintf(filename,"GEODATA/%s/%s",GEODIR,"WiresSaved.txt");
  FILE * res=fopen(filename,"w");
  if (!res){
    printf("openSaveWire failed with %s\n",filename);
    return;
  }else
     printf("openSaveWire ok with %s\n",filename);

  for (iW=0;iW<Nwires;iW++){
    fprintf(res,"%i %i ",iW+1,lWsize[iW]);
    for (iE=0;iE<lWsize[iW];iE++){
      if (lWires->isReversed)
        fprintf(res,"%i ",-(lWires->indexEdge+1));
      else
        fprintf(res,"%i ",lWires->indexEdge+1);
      lWires++;
    }
    fprintf(res,"W%i\n",iW+1);
  }
  fclose(res);
}

void removeMarkedEdgesFromWires(){
  int ii;
  for (ii=0;ii<NEdges;ii++){
    if (EDGES[ii].geom._length < -0.5){
      removeEdge(ii);
    }
  }
  
}


int main(){
  int simplify=1;
  int merge=0;
  sprintf(GEODIR,"SELOMMES");
//  sprintf(GEODIR,"TESTMERGE");
  readPoints();
  readEdges();
  buildEdges();
  readPointsInEdges();
//  for (int ii=0;ii<NEdges;ii++)
//     printEdge(EDGES+ii);
  nTotalEdges= readWires();
  printWires();
  if (simplify){
//    markSmallEdges(0.01);
    for (int ii=0;ii<NEdges;ii++){
      if (ii==194 || ii==560 || ii==491)
        EDGES[ii].geom._length=-1;
      else
        EDGES[ii].geom._length=1;
    }
    removeMarkedEdgesFromWires();
  }
  if (merge){
    mergeNoSharedEdges();
  }
  saveWires();
  saveEdges();
  
  return 0;
}
