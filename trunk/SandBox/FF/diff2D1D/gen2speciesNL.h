#ifndef GEN2NL_H
#define GEN2NL_H

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

extern int withSource;
extern int withControl;
//#define WITH_GRADIENT_P
/*
Vh0 uc=utm10s4+utm10s5+utm10s6+utm10s7+utm10s8;
    visuScal("2D1",Th0,uc,10);
Vh0 uc=utm10s0+utm10s1+utm10s2+utm10s3;

    cout<<"M1D0s3 "<<M1D0s3<<endl;


cout<<"M1D0s1 "<<M1D0s1<<endl;
    
*/
extern char OUTPUTDIR[];
//extern char POSTPRODIR[];
extern char FILENAME[];
// 1 for left material.
// -1 for right material.
extern int reverseEdges;

extern char specNames[];

#define VARF_1D_WITH_FF

#include "geomParser.h"
#include "systemDescr.h"
#include "edgeTools.h"
#include "optimParams.h"

enum solverType{
  UMFPACK_2D1D,
  GMRES_2D1D
};
extern enum solverType LinearSolver_2D1D;

extern int slowStart;
extern int curLab;
//vecteur unitaire des edges
//double *pVE;
extern int maxDofs1d;
extern char caux1[];
extern char caux2[];
extern char caux3[];
extern char caux4[];


enum MatrixType{
  VARF2D,
  VARF1D,
  COUPLAGE_1Dto2D,
  COUPLAGE_2Dto1D,
  COUPLAGE_EDGE,
  COUPLAGE_DOMAIN,
  COUPLAGE_SPECIES,
  COUPLAGE_CONTACT_2Dto2D
};

typedef struct{
  enum MatrixType type;
  int index2D1;
  int index1D1;
  int index2D2;
  int index1D2;
  int numSpecies;
  int numSpeciesCoupled;
  int indexEEInter;
  void * pNextMatStruct;
//  struct edgeEdgeInter* pEEInter;
}MatrixStruct;


extern MatrixStruct **ppMS;
extern int NB_BLOCK_LINES;
extern int NB_BLOCK_LINES_PER_SPECIES;

enum paramType {INT,DOUBLE,FCT};
void addDomVar();
void addParam(enum paramType type, char * name, int num,double v);

void printParams();
void printModelParams();

void printMatrixName(FILE *pF, MatrixStruct *pMS);

/*void printPoints(FILE *pF){
  int i;
  fprintf(pF,"//BEGIN POINTS\n");
  fprintf(pF,"int NPoints=%i;\n",NNodes);
  fprintf(pF,"real[int] Nx=[\n");
  for (i=0;i<NNodes;i++){
    fprintf(pF,"\t%e",Nx[i]);
    if (i<NNodes-1)
       fprintf(pF,",\n");
  }
  fprintf(pF,"];\n");
  fprintf(pF,"real[int] Ny=[\n");
  for (i=0;i<NNodes;i++){
    fprintf(pF,"\t%e",Ny[i]);
    if (i<NNodes-1)
       fprintf(pF,",\n");
  }
  fprintf(pF,"];\n");
  fprintf(pF,"//END POINTS\n");
  }*/
void printEdges(FILE *pF);
void printMeshes(FILE *pF);
void printUnknomns(FILE *pF);
void printDofs2D(FILE *pF);
void printDofs1D(FILE *pF);
void printDofs(FILE *pF);
void printVarfs(FILE *pF);
void printCheckdPF(FILE *pF);
void printCheckdUF(FILE *pF,int callable);
void printDefTargetLoc();
void printDefTargetDate();
void printRecorder();
void printOptimDef();

//void printMatCoup(FILE *pF,int);
void printMatStruct(FILE *pF);
//void printMatStruct2(FILE *pF);
void buildMat(FILE *pF);
void printMat(FILE *pF);



void resetMatStruct();
void printMatStruct(FILE *pF);
/* 
 *
 *
 * outpout: NB_BLOCK_LINES the number of blocks per lines. This means the global matrix is NB_BLOCK_LINESxNB_BLOCK_LINES of blocks.
The structure is the same for each species.
A line is like:
  |----------speci 0------------------------------------|----------speci 1------|----------speci x-------| the line is subdivided by NbSpecies sub-line.
  |------------wire 0-------|---- wire 1 ---|---wire x--|            Each sub-line is subdivided by NbWire sub-sub-line
  |2D block|1D blocks.......|  Each sub-sub-line contains a 2D blocks and  1d blocks for each edges (Wsize[.] 1d blocks).
 * 
 *
 * output: ppMS, the block matrix. ppMS[i+j*NB_BLOCK_LINES] is either NULL or a MatrixStruct element, it is the element of the line i raw j.
 *
 */
void initMatStruct();





#define WITHMASSECOMPUTE
void printMassInfo(FILE *pF);

#define WITH_POST_PRO
void printTSOneStep(FILE *pF);


void printsolToDof(FILE *pF);

//void printTSOneStepW();
//void printBuildRhs(FILE *pF);
void printsolToW();
//void printAdaptiveTSaveRestore(FILE *pF);

//void printTS(FILE *pF);

void printViewVtk();

void printSaveMeshes();

void printVtkViewWires();
//n is a number of polygone
void printFuncexp(FILE *pF,int n);
//n is a number of polygone
void printFunc(FILE *pF,int n);
void printInitialCond(FILE *pF,int IDCAS);


/*void randomizePoint(double amp){
  int ii;
  for (ii=0;ii<NNodes;ii++){
    Nx[ii]+=(amp*rand())/RAND_MAX;
    Ny[ii]+=(amp*rand())/RAND_MAX;
  }
    
  }*/
void printPluginVariable(FILE *pF);

void printXXYY(FILE *pF);
//just for demo
void printPlugin(FILE *pF);

void printVTKGENPY(FILE *pF);

int buildDirectories();

int buildDefOutput(FILE *pF);

int buildRunSGEFile(FILE *pF);

void printUserF();
#endif
