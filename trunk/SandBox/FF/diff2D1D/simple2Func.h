//#define WITH_GRADIENT_P
void initStructure(){
 NbSpecies=2;
 NDynFieldType=1;
 NDynEdgeType=1;
 nPerLength=3e-5;
 nEFinter=0;
 nEEinter=0;
 nParams=2;
  int ii,type;
  int wb=1;
  double s=40;
  double m=-20;
  double coef=1;
  //param model
  double mu=5;//1D to 2D
  double nu=10.0;//2D to 1D
  double D1d=10;
  double D2d=1e-6;
  FILE *edgesFile;
  char *line=NULL;
  reverseEdges=1;
  ssize_t read;
  size_t len = 0;
  NDynFieldType=1;
  NDynEdgeType=1;
  nPerLength=3e-5;
  nEFinter=0;
  nEEinter=0;
  nParams=2;
  NbSpecies=2;
  specNames[0]='A';
  specNames[1]='B';
  
  sprintf(GEODIR,"GEODATA/SIMPLE2");
  readPoints();
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/SIMPLE2FUNC/");
  //sprintf(POSTPRODIR,"/home/olivierb/solvers/trunk/SandBox/FF/Vacher/");
  //sprintf(FILES_COPY[0],"GRAD/*");
  
  readEdges();
  buildEdges();
  readPointsInEdges();
  for (ii=0;ii<NEdges;ii++)
     printEdge(EDGES+ii);
  readWires();
  printWires();
  
  allocStructs();
  
  //A
  aFieldPop[0].D2DX[0]=D2d;
  aFieldPop[0].D2DY[0]=D2d;
 
  printFieldPops(aFieldPop,Nwires);
  
}




