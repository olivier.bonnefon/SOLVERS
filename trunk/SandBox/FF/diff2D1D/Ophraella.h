int NbSpecies=1;
int NDynFieldType=1;
int NDynEdgeType=1;
//double nPerLength=16;
double nPerLength=2e-1;
int nEFinter=1;
int nEEinter=1;
int whithSource=0;
int whithControl=0;
int NFilesCopy=114+11;
char FILES_COPY[200][128];

#include "../Ophraella/meshesToCopy.h"

void initStructure(){
//  slowStart=1;
  int ii,type;
  int wb=1;
  double s=40;
  double m=-20;
  double coef=1;
  //param model
  double K=1.0;
  double r=1e-3;
  double mu=4300;//1D to 2D
  double nu=3.3e-2;//2D to 1D
  double D1d=1e3;
  //double D2d=3.98e-5;
  double D2d=69;
  FILE *edgesFile;
  char *line=NULL;
  ssize_t read;
  size_t len = 0;
  int withAllee=1;
  sprintf(OUTPUTDIR,"/mnt/bigcalcul2/biom/olivierB/solvers/trunk/SandBox/FF//RES_OPHRAELLA_DIRICHLET/");
  sprintf(POSTPRODIR,"/mnt/bigcalcul2/biom/olivierB/solvers/trunk/SandBox/FF//Ophraella/");
  preparMeshesCopy();
  sprintf(FILES_COPY[114+0],"IC/initOphraella.msh");
  sprintf(FILES_COPY[114+1],"IC/defIC.edp");
  if (withAllee)
    sprintf(FILES_COPY[114+2],"withAllee/postTraitementLoad.edp");
  else
    sprintf(FILES_COPY[114+2],"postTraitementLoad.edp");
  sprintf(FILES_COPY[114+3],"postTraitement.edp");
  sprintf(FILES_COPY[114+4],"postTraitementEnd.edp");
  sprintf(FILES_COPY[114+5],"main.edp");
  sprintf(FILES_COPY[114+6],"readDeptMesh.edp");
  sprintf(FILES_COPY[114+7],"OutborderMesh.msh");
  sprintf(FILES_COPY[114+8],"setDeptValue.edp");
  sprintf(FILES_COPY[114+9],"deptToAll.edp");
  sprintf(FILES_COPY[114+9],"defParamsGen.edp");
//  sprintf(FILES_COPY[3],"cas1.edp");
//  sprintf(GEODIR,"WTEST");
  sprintf(GEODIR,"OPHRAELLA");
 
  scalCoord=1e-3;
  printf("Coordonnate unit is supposed km.\n");
  readPoints();
  printBB();
  // sprintf(OUTPUTDIR,"/mnt/bigcalcul2/biom/olivierB/solvers/trunk/SandBox/FF/RES_OPHRAELLA_GAMMA_ONLY/");
   readEdges();
  buildEdges();
  readPointsInEdges();
  for (ii=0;ii<NEdges;ii++)
     printEdge(EDGES+ii);
  readWires();
  printWires();
  
  //exit(0);
  /*double scal=1;
  for (ii=0;ii<NEdges;ii++){
    Nx[ii]=scal* Nx[ii];
    Ny[ii]=scal* Ny[ii];
    }*/

 
  allocStructs();
  //Cmalthus is a fct domain depend
  aFieldPop[0].isFct[0][Cmalthus_I]=1;
  aFieldPop[0].isFieldDep[0][Cmalthus_I]=1;

  //K and R are fct
  aFieldPop[0].isFct[0][K_I]=1;
  aFieldPop[0].isFieldDep[0][K_I]=1;
  aFieldPop[0].isFct[0][R_I]=1;
  aFieldPop[0].isFieldDep[0][R_I]=1;
  aFieldPop[0].R[0]=1;
  aFieldPop[0].K[0]=1;
  aFieldPop[0].T[0]=0;
  aFieldPop[0].rho[0]=0.0;
  aFieldPop[0].U2[0]=0;
  
  if (withAllee){
    aFieldPop[0].isFct[0][Cmalthus_I]=1;
    aFieldPop[0].Cmalthus[0]=1;
    aFieldPop[0].source[0]=1;
  }else{
   aFieldPop[0].Cmalthus[0]=1;
   aFieldPop[0].source[0]=0.00;
  }
  
  aFieldPop[0].D2DX[0]=D2d;
  aFieldPop[0].D2DY[0]=D2d;
  
  
  
//  aFieldPop[1].T[0]=0;
  
//  aFieldPop[1].rho[0]=0.0;
  
  
  
  
  aEdgePop[0].D[0]=D1d;
  aEdgePop[0].indexEFInter=0;
  
  
    
  // 2D/1D flux
  aEdgeFieldInter[0].mu[0]=65*65;
  aEdgeFieldInter[0].nu[0]=0.18*0.18;
  aEdgeEdgeInter[0].alpha[0]=100;
  aEdgeEdgeInter[0].gamma[0]=100;
  
  edgesFile = openEdgesFile();
  ii=0;
  while ((read = getline(&line, &len, edgesFile)) != -1) { 
    sscanf(line,"%i",&type);
    //EDGES[ii].popType=1;
    // if (!type){
    //  EDGES[ii]._bcinf.type=ALONE;
    //  EDGES[ii]._bcsup.type=ALONE;
    //  EDGES[ii].popType=0;
    // }
    // if (type==2){
    //   EDGES[ii]._bcinf.type=DIRICHLET;
    //   EDGES[ii]._bcinf._value=1.0;
    // }
    if (type==3){
      EDGES[ii]._bcsup.type=DIRICHLET;
      EDGES[ii]._bcsup._value=1.0;
    }
    if (type==4){
      EDGES[ii]._bcinf.type=DIRICHLET;
      EDGES[ii]._bcinf._value=1.0;
    }
         //EDGES[ii].couple1D=type;
    
    ii++;
  }
  fclose(edgesFile);

  printFieldPops(aFieldPop,Nwires);
  
}

/*

pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;




pu1d0s38=1;
pu1d0s37=0;
pu1d0s36=0;
pu1d0s42=0;
pu1d0s41=1;
pu1d0s40=0;
pu1d0s39=0;
pu1d1s20=0;
pu1d1s21=1;
pu1d1s22=0;
pu1d1s23=0;
pu1d1s24=0;
pu1d1s35=1;
pu1d1s37=0;
pu1d1s38=0;
pu1d1s19=1;
pu1d2s0=0;
pu1d2s1=0;
pu1d2s2=1;
pu1d2s3=0;
pu1d2s4=0;
pu1d2s5=1;
pu1d2s6=0;
pu1d2s7=0;
pu1d2s8=1;
pu1d2s9=0;
pu1d2s10=0;
pu1d2s11=0;
pu1d2s12=1;
pu1d2s13=0;
pu1d2s14=0;
pu1d2s15=0;
pu1d2s16=0;
pu1d2s17=1;
pu1d2s18=0;
pu1d2s39=0;
pu1d2s40=0;
pu1d2s41=0;
pu1d2s42=1;
pu1d2s36=0;
pu1d2s35=0;
pu1d2s25=0;
pu1d2s26=0;
pu1d2s27=0;
pu1d2s28=0;
pu1d2s29=0;
pu1d2s30=0;
pu1d2s31=0;
pu1d2s32=0;
pu1d2s33=0;
pu1d2s34=0;
pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;*/
