#include "systemDescr.h"
int NbSpecies=2;
int NDynFieldType=2;
int NDynEdgeType=2;
int * FieldTypes=0;
int * EdgeTypes=0;

int NPoints=11;
double Px[]={5-5 ,10-5,10-5,5-5  ,5-5,0-5,0-5,0-5,10-5,17-5,14-5};
double Py[]={0 ,0 ,5 ,7.5,5,5,0,-5,-7.5,4,0};
//double Px[]={10., 0,  10, 0.,10};
//double Py[]={10., 10, 0,  0.,5};

int NEdges=15;
int EDGES[]={
             0,1,
             1,2,
             2,3,
             3,4,
             4,0,
             4,5,
             5,6,
             6,0,
             6,7,
             7,8,
             8,1,
             1,0,
             1,10,
             10,9,
             9,2
             };


int Nwires=4;//one wire per domain
int Wsizes[]={5,4,5,4};
int WIRES[]={0,1,2,3,4,
             5,6,7,-4,
             -7,8,9,10,11,
             12,13,14,-1};
//int ABSWIRES[]={0,1,2,3};

//  -1,4,5,6,7};

int nPerLength=2;
int nEFinter=2;
int nEEinter=1;
void initStructure(){
  int ii;
  double s=40;
  double m=-20;
  double coef=1.58;
  allocStructs();
  
  aFieldPop[0].D2DX[0]=5;
  aFieldPop[0].D2DY[0]=5;
  aFieldPop[0].K[0]=2;
  aFieldPop[0].R[0]=0;
  aFieldPop[0].Ccoup[0]=-1*coef;
  aFieldPop[0].Cmalthus[0]=1;
  aFieldPop[0].source[0]=0.00;

  aFieldPop[0].D2DX[1]=5;
  aFieldPop[0].D2DY[1]=5;
  aFieldPop[0].K[1]=1;
  aFieldPop[0].R[1]=0;
  aFieldPop[0].Ccoup[1]=1*coef;
  aFieldPop[0].Cmalthus[1]=-1;
  aFieldPop[0].source[1]=0.0;

  for (ii=0;ii<nEEinter;ii++){
    aEdgeEdgeInter[0].alpha[0]=0.0;
    aEdgeEdgeInter[0].alpha[1]=0.0;
  }
  for (ii=0;ii<nEFinter;ii++){
  //species [1] doesn't cross the edge.
    aEdgeFieldInter[ii].mu[0]=0;//1.0;
    aEdgeFieldInter[ii].nu[0]=0;//0.5;
    aEdgeFieldInter[ii].mu[1]=0;//1.0;
    aEdgeFieldInter[ii].nu[1]=0;//0.5;
  }

  //aEdgePop[0].pEFInter=&aEdgeFieldInter[1];
  // aEdgePop[0].source[1]=s;
  // aEdgePop[0].Cmalthus[1]=m;
  // aEdgePop[1].source[1]=s;
  // aEdgePop[1].Cmalthus[1]=m;

  printFieldPops(aFieldPop,Nwires);
}
