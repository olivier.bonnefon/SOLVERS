
#include "systemDescr.h"
int NbSpecies=1;
int NDynFieldType=1;
int NDynEdgeType=1;
int * FieldTypes=0;
int * EdgeTypes=0;



/*

                           

                           3_________________E3_________________2
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           | E0                                 |E2
                           |                                    |
                           |                                    |
                           |                                    |
                           |      4                             |
                           |                                    |
                           |                                    |
                           0                  E1                1
                           |                                    |
                           |                         5          |
                           |                                    |
                           |                                    |
                           |  E4                                |
                           |                                    |
                           |                                    |E6
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           |                                    |
                           6_________________E5________________7


pu1d0s1=0;
pu1d0s2(0:n1Ddofs0s2/2)=1;
pu1d0s3=0;
pu1d0s0=0;
pu1d1s4=0;
pu1d1s5=0;
pu1d1s6(1:n1Ddofs0s6/2)=5;
pu1d1s1=0;
*/


//The points:
int NPoints=8;
double PointsX[]={1,2,2,1,1.25,1.75,1,2};
double PointsY[]={0 ,0,1,1,0.7,-0.9,-1,-1};
//The nodes
int NNodes=6;
int Nodes[]={0,1,2,3,6,7};
//The edges
int NEdges=7;
int NodeEdges[]={3,0,
                 0,1,
                 1,2,
                 2,3,
                 0,6,
                 6,7,
                 7,1};
int NumberOfPointsInEdge[]={0,2,0,0,0,0,0};
//all edges are connected
int *lcouple1D=(int[]){1,1,1,1,1,1,1};
//The domains
int Nwires=2;//one wire per domain
int *Wsizes=(int[]){4,4};
int *WIRES=(int[]){1,2,3,0,
                   4,5,6,-1};

double nPerLength=15;
int nEFinter=1;
int nEEinter=1;
int whithSource=0;
int whithControl=0;

#include "edgeTools.h"

void initStructure(){
  int ii;
  int wb=1;
  double s=40;
  double m=-20;
  double coef=1;
  double scal=1;
  buildEdges();
  for (ii=0;ii<NEdges;ii++){
    EDGES[ii].couple1D=lcouple1D[ii];
  }
  // add 2 points in edge 0
  EDGES[1].geom.indicesPoints[1]=4;
  EDGES[1].geom.indicesPoints[2]=5;
  

  allocStructs();
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/RES_SIMPLE1BIS/");


  /*stationary state :
    u2dtm10=5;
    v2dtm10=(5-0.05)*(1-0.25);
  */
  aFieldPop[0].D2DX[0]=0.1;
  aFieldPop[0].D2DY[0]=0.1;
  aFieldPop[0].K[0]=1;
  aFieldPop[0].R[0]=0;
  aFieldPop[0].T[0]=0;
  aFieldPop[0].rho[0]=0.05;
  
  aFieldPop[0].Ccoup[0]=-1*coef;
  aFieldPop[0].Cmalthus[0]=0;
  aFieldPop[0].source[0]=0.00;

//  aEdgeEdgeInter[0].alpha[0]=0.0;
//  aEdgeEdgeInter[0].alpha[1]=wb?1.0:0;

  for (ii=0;ii<nEEinter;ii++){
    aEdgeEdgeInter[ii].alpha[0]=10;
  }
  for (ii=0;ii<nEFinter;ii++){
    //2D to 1D
    aEdgeFieldInter[ii].mu[0]=1.0;
    //1D to 2D
    aEdgeFieldInter[ii].nu[0]=0.5;
  }
  aEdgePop[0].D[0]=0.1;

  printFieldPops(aFieldPop,Nwires);
}



