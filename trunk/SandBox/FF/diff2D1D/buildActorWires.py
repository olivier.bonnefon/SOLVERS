points = vtk.vtkPoints()
points.SetNumberOfPoints(7)
RLine=1
GLine=1
BLine=1
widthLine=2.5

points.SetPoint(0, 0.000000e+00, 0.000000e+00, 0.1)
points.SetPoint(1, 5.000000e+00, 0.000000e+00, 0.1)
points.SetPoint(2, 5.000000e+00, 5.000000e+00, 0.1)
points.SetPoint(3, 0.000000e+00, 7.500000e+00, 0.1)
points.SetPoint(4, 0.000000e+00, 5.000000e+00, 0.1)
points.SetPoint(5, -5.000000e+00, 5.000000e+00, 0.1)
points.SetPoint(6, -5.000000e+00, 0.000000e+00, 0.1)

lines0 = vtk.vtkCellArray()
lines0.InsertNextCell(6)
lines0.InsertCellPoint(0);
lines0.InsertCellPoint(1);
lines0.InsertCellPoint(2);
lines0.InsertCellPoint(3);
lines0.InsertCellPoint(4);
lines0.InsertCellPoint(0);

polygon0 = vtk.vtkPolyData()
polygon0.SetPoints(points)
polygon0.SetLines(lines0)


polygonMapper0 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper0.SetInputConnection(polygon0.GetProducerPort())
else:
	polygonMapper0.SetInputData(polygon0)
	polygonMapper0.Update()
polygonActor0 = vtk.vtkActor()
polygonActor0.SetMapper(polygonMapper0)
polygonActor0.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor0.GetProperty().SetLineWidth(widthLine)

lines1 = vtk.vtkCellArray()
lines1.InsertNextCell(5)
lines1.InsertCellPoint(4);
lines1.InsertCellPoint(5);
lines1.InsertCellPoint(6);
lines1.InsertCellPoint(0);
lines1.InsertCellPoint(4);

polygon1 = vtk.vtkPolyData()
polygon1.SetPoints(points)
polygon1.SetLines(lines1)


polygonMapper1 = vtk.vtkPolyDataMapper()
if vtk.VTK_MAJOR_VERSION <= 5:
	polygonMapper1.SetInputConnection(polygon1.GetProducerPort())
else:
	polygonMapper1.SetInputData(polygon1)
	polygonMapper1.Update()
polygonActor1 = vtk.vtkActor()
polygonActor1.SetMapper(polygonMapper1)
polygonActor1.GetProperty().SetColor(RLine,GLine,BLine)
polygonActor1.GetProperty().SetLineWidth(widthLine)


