#include "timeStepping.h"
#include "gen2speciesNL.h"
#include <stdlib.h>
#include <stdio.h>

//#include "commun.h"
#include <unistd.h>
int isLinear=0;
double tm=1.0;
double dt=0.1;
double T0=0;
double Tf=1;
int maxMult=1;
double adaptiveRelErr=0.05;
double newtonCriteron=1e-2;
double newtonMaxResidu=10;
int newtonMinIt=1;
int newtonMaxIt=5;
int forcePositiveSol=1;

void _printBuildRhs(){
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"buildRhsGen.edp");
  FILE *pF = fopen(FILENAME, "w");
  int ii,jj,numSpecies;
  struct wireSide *pW=WIRES;
  int numEdge=0;
  struct edgePop* pedgePop=NULL;
  if (NDynEdgeType)
    pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
  
  if (NDynEdgeType){
    fprintf(pF,"\t//build rhs 1D\n");
    fprintf(pF,"\tmy1Dto2D;\n");
  
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      char charSpec=specNames[numSpecies];
      pW=WIRES;
      numEdge=0;
      
      pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
      for(ii=0;ii<Nwires;ii++){
	//fprintf(pF,"\trhs2d%i=varfRhs2D%i(0,Vh%i);\n",ii,ii,ii);
	for (jj=0;jj<Wsizes[ii];jj++){
	  pedgePop=&(aEdgePop[EDGES[pW->indexEdge].popType]);
	  fprintf(pF,"\n\t//spec %c domain %i edge %i\n",charSpec,ii,pW->indexEdge);
	  fprintf(pF,"\tbuildRHS2(varfRhs%c%is%i,rhs%c1d%is%i,n1Ddofs%is%i,zoi%is%ibis,Vh%is%i);\n",charSpec,
		  ii,pW->indexEdge,charSpec,ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge);
	  pW++;
	  numEdge++;
	}
      }
    }
    fprintf(pF,"\n");
    fprintf(pF,"\t//couple 1D rhs\n");
    //  fprintf(pF,"\tif (couple1D){\n");
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      char charSpec=specNames[numSpecies];
      pW=WIRES;
      for(ii=0;ii<Nwires;ii++){
	int next1D=1;
	for (jj=0;jj<Wsizes[ii];jj++){
	  
	  //if (EDGES[abs(pW[jj])]._bcsup.type==COUPLE1D && EDGES[abs(pW[next1D])]._bcsup.type==COUPLE1D){
	  if (((pW[jj].isReversed && EDGES[pW[jj].indexEdge]._bcinf.type==COUPLE1D) ||
         (!(pW[jj].isReversed) && EDGES[pW[jj].indexEdge]._bcsup.type==COUPLE1D) ) &&
	      ( pW[next1D].isReversed && EDGES[pW[next1D].indexEdge]._bcsup.type==COUPLE1D ||
          (!(pW[next1D].isReversed) && EDGES[pW[next1D].indexEdge]._bcinf.type==COUPLE1D))){
	    fprintf(pF,"\t\trhs%c1d%is%i(i%i%is%is%i)=rhs%c1d%is%i(i%i%is%is%i)+rhs%c1d%is%i(i%i%is%is%i);\n",
		    charSpec,
		    ii,pW[jj].indexEdge,
		    pW[jj].indexEdge,ii,pW[jj].indexEdge,pW[next1D].indexEdge,
		    charSpec,ii,pW[jj].indexEdge,
		    pW[jj].indexEdge,ii,pW[jj].indexEdge,pW[next1D].indexEdge,
		    charSpec,ii,pW[next1D].indexEdge,
		    pW[next1D].indexEdge,ii,pW[jj].indexEdge,pW[next1D].indexEdge);
	  }
	  next1D=(next1D+1)%(Wsizes[ii]);
	}
	pW=pW+Wsizes[ii];
      }
    }
    //  fprintf(pF,"\t}//fin 1D rhs\n");
    fprintf(pF,"//fin 1D rhs\n");
    
  }

fprintf(pF,"\t\t//build rhs 2D\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    for(ii=0;ii<Nwires;ii++){
      fprintf(pF,"\t\trhs%c2d%i=varfRhs%c2D%i(0,Vh%i);\n",specNames[numSpecies],ii,specNames[numSpecies],ii,ii);
    }
  }
  fprintf(pF,"\t\t//fin build rhs 2D\n");
  fprintf(pF,"\t\t//build all rhs \n");
  fprintf(pF,"\t\trhs=[");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    pW=WIRES;
    for(ii=0;ii<Nwires;ii++){
      fprintf(pF,"rhs%c2d%i",specNames[numSpecies],ii);
      if (NDynEdgeType){
        fprintf(pF,",");
        for (jj=0;jj<Wsizes[ii];jj++){
          fprintf(pF,"rhs%c1d%is%i",specNames[numSpecies],ii,pW->indexEdge);
          pW++;
          if (jj<Wsizes[ii]-1 || ii<Nwires-1 || (numSpecies+1)<NbSpecies)
            fprintf(pF,",");
        }
      }else{
        if (ii<Nwires-1 || numSpecies+1<NbSpecies)
          fprintf(pF,",");
      }
      if (ii<Nwires-1 || numSpecies<NbSpecies)
        fprintf(pF,"\n\t\t\t");
      
    }
  }
  fprintf(pF,"];\n");
  fprintf(pF,"\t\t//fin build all rhs \n");

  
  fclose(pF);
}



void _printMacro2Dto1D(FILE *pF){
  int numSpecies,ii,jj;
  if (NDynEdgeType){
    fprintf(pF,"macro my1Dto2D{\n");
    for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
      char charSpec=specNames[numSpecies];
      struct wireSide * pW=WIRES;
      for(ii=0;ii<Nwires;ii++){
        for (jj=0;jj<Wsizes[ii];jj++){
          fprintf(pF,"\tconvert1Dto2D2(p%c1dtm1%is%i,%c1D%is%itm1,n1Ddofs%is%i,zoi%is%ibis);\n",
                  charSpec,ii,pW->indexEdge,charSpec,ii,pW->indexEdge,ii,pW->indexEdge,ii,pW->indexEdge);
          pW++;
        }
      }
    }
    fprintf(pF,"}//\n");
  }
}

void _printSimuParams(FILE *pS){
  fprintf(pS,"//general ts params\n");
//  fprintf(pS,"int curStep=0;\n");
  fprintf(pS,"int isLinear=%i;\n",isLinear);
  fprintf(pS,"int int freqGradUpdate=1;\n");
  fprintf(pS,"real tm=%e;\n",tm);
  fprintf(pS,"real dt=%e;\n",dt);
  fprintf(pS,"real T0=%e;\n",T0);
  fprintf(pS,"real Tf=%e;\n",Tf);
  fprintf(pS,"real curT=%e;\n",T0);
  fprintf(pS,"real alpha=1/dt;\n");
  fprintf(pS,"int simuFailed=0;\n");
  fprintf(pS,"int numStep=0;\n");
  fprintf(pS,"//adaptive ts params\n");
  fprintf(pS,"real minStep=dt;\n");
  fprintf(pS,"int maxMult=%i;\n",maxMult);
  fprintf(pS,"real relError=%e;\n",adaptiveRelErr);
  fprintf(pS,"//NonLinear ts params\n");
  fprintf(pS,"real criteron=%e;\n",newtonCriteron);
  fprintf(pS,"real maxResiduPossible=%e;\n",newtonMaxResidu);

  fprintf(pS,"int minNewtonIt=%i;\n",newtonMinIt);
  fprintf(pS,"int maxNewtonIt=%i;\n",newtonMaxIt);
  fprintf(pS,"int forcePositiveSol=%i;\n",forcePositiveSol);
  
  
  
}
void _printAdaptiveTSaveRestore(FILE *pF){
  fprintf(pF,"macro adaptiveSaveState\n");
  fprintf(pF,"{\n");
  fprintf(pF,"    for (int im=0;im<NbSpecies;im++)\n");
  fprintf(pF,"      tabsave2d0[im]=tabtm12d0[im];\n");
  int numSpecies,ii,jj;
  
  for ( numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    struct wireSide* pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      for (jj=0;jj<Wsizes[ii];jj++){
        fprintf(pF,"   psav%c1d%is%i=p%c1dtm1%is%i;\n",
                specNames[numSpecies],ii,pW->indexEdge,
                specNames[numSpecies],ii,pW->indexEdge);
        pW++;
      }
    }
  }
  fprintf(pF,"  include \"userSaveState.edp\"\n");
  fprintf(pF,"  }//\n");


  fprintf(pF,"macro adaptiveRestore\n");
  fprintf(pF,"{\n");
  fprintf(pF,"   for (int im=0;im<NbSpecies;im++)\n");
  fprintf(pF,"    tabtm12d0[im]=tabsave2d0[im];\n");
  
  for ( numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    struct wireSide* pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      for (jj=0;jj<Wsizes[ii];jj++){
        fprintf(pF,"   p%c1dtm1%is%i=psav%c1d%is%i;\n",
                specNames[numSpecies],ii,pW->indexEdge,
                specNames[numSpecies],ii,pW->indexEdge);
        pW++;
      }
    }
  }
  fprintf(pF,"  include \"userRestoreState.edp\"\n");
  fprintf(pF," }//\n");
}

void _defSaveVtk(){
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defSaveVtk.edp");
  FILE *pVtk = fopen(FILENAME, "w");
  int numSpecies,ii,jj;
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=specNames[numSpecies];
    struct wireSide* pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      
      fprintf(pVtk,"\t\tsavevtk(OUTPUTDIR+\"D2/%c%is\"+string(numStep)+\".vtk\",Th%i,%ctm12d%i,dataname=\"UU\");\n",
                                             spec,ii,ii,spec,ii);
      if (NDynEdgeType){
        fprintf(pVtk,"\t\t{\n");
        fprintf(pVtk,"\t\t\tofstream pWire(OUTPUTDIR+\"D1/%c1d\"+string(numStep)+\"s%i.txt\");\n",spec,ii);
        for (jj=0;jj<Wsizes[ii];jj++){
          int notLast=1;
          int revers=0;
          if (pW->isReversed)
            revers=1;
          if (jj==Wsizes[ii]-1)
            notLast=0;
          fprintf(pVtk,"\t\t\tprintVect(pWire,%i,p%c1d%is%i,n1Ddofs%is%i-%i);\n",revers,spec,ii,pW->indexEdge,ii,pW->indexEdge,notLast);
          
          pW++;
       }
        fprintf(pVtk,"\t\t}\n");
        
      }
    }
  }
//  fprintf(pVtk,"\t}\n");
  
  fclose(pVtk);
}
void buildTS(){

  _defSaveVtk();
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"buildMatGen.edp");
  FILE *pMatsBuild = fopen(FILENAME, "w");
  buildMat(pMatsBuild);
  fclose(pMatsBuild);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defMatGen.edp");
  FILE *pMats = fopen(FILENAME, "w");
  printMat(pMats);
  fclose(pMats);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"saveRestoreState.edp");
  FILE *pToolAdap = fopen(FILENAME,"w");
  _printAdaptiveTSaveRestore(pToolAdap);
  fclose(pToolAdap);
  

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defSimuParamsGen.edp");
  if(access( FILENAME, F_OK ) == -1){
    FILE *pS=fopen(FILENAME,"w");
    _printSimuParams(pS);
    fclose(pS);
  }

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"macro2D1D.edp");
  FILE *pm2D1D = fopen(FILENAME, "w");
  _printMacro2Dto1D(pm2D1D);
  fclose(pm2D1D);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"solToDof.edp");
  FILE *pTimeStepSolToDof = fopen(FILENAME, "w");
  printsolToDof(pTimeStepSolToDof);
  fclose(pTimeStepSolToDof);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defICGen.edp");
  if(access( FILENAME, F_OK ) == -1){
    FILE *pIC = fopen(FILENAME, "w");
    printf("call IC\n");
    printInitialCond(pIC,1);
    fclose(pIC);
  }
  
  _printBuildRhs();
  
}
