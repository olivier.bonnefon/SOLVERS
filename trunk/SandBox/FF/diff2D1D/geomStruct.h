#ifndef GEOMDESCR_H
#define GEOMDESCR_H


/* Geometrical description of edges
 * nPoints: numbers of points of the Edges (>=2)
 * indicesPoints: indices of points.indicesPoints[0/nPoints-1] is the indice of the begining/ending node in the array Points
 * _abscCurv: abscisse curviline of the points
 */
struct edge_geom{
  int nPoints;
  int * indicesPoints;
  double *_abscCurv;//abscisse curviline
  double _length;
  int idInGeomFile;
};

enum edgeBcType {ALONE,COUPLE1D,DIRICHLET};
struct edgeBc{
  enum edgeBcType type;
  double _value;
};

/*
 *contain the edge description,
 * * geom: a pointer to edge_geom, the geometrical description.
 * * popType: index of pop dynamique: aEdgePop[popType] is the dynamic attached.
 * * couple1D: means this edge is agre to attached at his nearbourg.
 * * isShared: means the edge is shared by 2 wires.
 */
struct edge{
  struct edge_geom geom;
  int popType;
//  int couple1D;
  int _isShared;
  struct edgeBc _bcsup;
  struct edgeBc _bcinf;
  
};
struct wireSide{
  int indexEdge;
  int isReversed;
};

//struct edge* EDGES=NULL;

void printEdgeGeom(struct edge_geom *pE);

void printEdge(struct edge *pE);

#define GET_EDGE_NODE_BEGIN(I) EDGES[I].geom.indicesPoints[0]
#define GET_EDGE_NODE_END(I) EDGES[I].geom.indicesPoints[EDGES[I].geom.nPoints-1]

/*GEOMETRCICAL DESCRIPTION*/
//The number of points and its coordinates
//extern int NPoints;
//extern double Px[];
//extern double Py[];
//The number of geometrical edges.
//If a edge is shared beteween tow fields, it the same geometrical edge.
extern int NEdges;
//The couples of points defining edges
extern struct edge* EDGES;
//Number of wires, ie number of fields
extern int Nwires;
//Wsizes[i] is the number of edges of the field i
extern int *Wsizes;
//It contains the index of edge of the fields
extern struct wireSide *WIRES;

#endif
