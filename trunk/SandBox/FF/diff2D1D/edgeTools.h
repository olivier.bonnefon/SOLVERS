#ifndef EDGETOOLS_H
#define EDGETOOLS_H
#include "geomStruct.h"
#include <stdlib.h>
#include "geomParser.h"

void buildEdgeGeom(struct edge_geom* pE,int Nbeg,int Nend,int nPts);
void buildEdges();
void freeEdges();
int checkEdge(int ii);
void printWires();
#endif
