
#include "systemDescr.h"
int NbSpecies=1;
int NDynFieldType=2;
int NDynEdgeType=2;
int * FieldTypes=0;


//The points:
int NPoints=6;
//double PointsX[]={1,2,2,1,1.25,1.75};
//double PointsY[]={0 ,0,1,1,0.25,-0.25};
double PointsX[]={1,2,2,1,1.25,1.75};
double PointsY[]={0 ,0,1,1,0.25,-0.2};
//The nodes
int NNodes=4;
int Nodes[]={0,1,2,3};
//The edges
int NEdges=4;
int NodeEdges[]={0,1,
                1,2,
                2,3,
                3,0};
int NumberOfPointsInEdge[]={2,0,0,0};
//all edges are connected
int *lcouple1D=(int[]){1,1,1,1};
//The domains
int Nwires=1;//one wire per domain
int *Wsizes=(int[]){4};
int *WIRES=(int[]){0,1,2,3};

double nPerLength=15;
int nEFinter=2;
int nEEinter=0;
int whithSource=0;
int whithControl=0;

#include "edgeTools.h"

void initStructure(){
  int ii;
  int wb=1;
  double s=40;
  double m=-20;
  double coef=1;
  double scal=1;
  buildEdges();
  for (ii=0;ii<NEdges;ii++){
    EDGES[ii].couple1D=lcouple1D[ii];
  }

  // add 2 points in edge 0
  EDGES[0].geom.indicesPoints[1]=4;
  EDGES[0].geom.indicesPoints[2]=5;
  

  allocStructs();
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/RES_SIMPLE1/");


  /*stationary state :
    u2dtm10=5;
    v2dtm10=(5-0.05)*(1-0.25);
  */
  aFieldPop[0].IC[0]=5*1.2;
  aFieldPop[0].D2DX[0]=0.1;
  aFieldPop[0].D2DY[0]=0.1;
  aFieldPop[0].K[0]=1;
  aFieldPop[0].R[0]=0;
  aFieldPop[0].T[0]=0;
  aFieldPop[0].rho[0]=0.05;
  
  aFieldPop[0].Ccoup[0]=-1*coef;
  aFieldPop[0].Cmalthus[0]=0;
  aFieldPop[0].source[0]=0.00;

//  aEdgeEdgeInter[0].alpha[0]=0.0;
//  aEdgeEdgeInter[0].alpha[1]=wb?1.0:0;

 
  for (ii=0;ii<nEFinter;ii++){
    //1D to 2D
    aEdgeFieldInter[ii].mu[0]=.5;
//    aEdgeFieldInter[ii].mu[1]=wb?1.0:0;
    //2D to 1D
    aEdgeFieldInter[ii].nu[0]=.5;
//    aEdgeFieldInter[ii].nu[1]=wb?0.25:0;
  }
  aEdgePop[0].D[0]=15;

  printFieldPops(aFieldPop,Nwires);
}



