#include "geomStruct.h"
#include <stdio.h>
void printEdgeGeom(struct edge_geom *pE){
  int ii,npts;
  npts=pE->nPoints;
  printf("Edge geometrie:\n");
  printf(" id in geom=%i\n",pE->idInGeomFile);
  printf(" Nodes=%i %i\n",pE->indicesPoints[0],pE->indicesPoints[npts-1]);
  printf(" NbPoint=%i\n",pE->nPoints-2);
  for (ii=1;ii<pE->nPoints-1;ii++)
    printf("  %i ",pE->indicesPoints[ii]);
  printf(" \n");
}

void printEdge(struct edge *pE){
  printEdgeGeom(&(pE->geom));
  printf("The edge pop type is %i\n",pE->popType);
  printf(" agree to share his border? %i\n",pE->_bcinf.type);
}




/*GEOMETRCICAL DESCRIPTION*/
//The number of points and its coordinates
//extern int NPoints;
//extern double Px[];
//extern double Py[];
//The number of geometrical edges.
//If a edge is shared beteween tow fields, it the same geometrical edge.
int NEdges=0;
//The couples of points defining edges
struct edge* EDGES=NULL;
//Number of wires, ie number of fields
int Nwires=0;
//Wsizes[i] is the number of edges of the field i
int *Wsizes=0;
//It contains the index of edge of the fields
struct wireSide *WIRES=NULL;

