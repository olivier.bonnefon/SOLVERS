#include "edgeTools.h"
#include "geomStruct.h"

void buildEdgeGeom(struct edge_geom* pE,int Nbeg,int Nend,int nPts){
  pE->nPoints=nPts+2;
  pE->indicesPoints=(int*)malloc((pE->nPoints)*sizeof(int));
  pE->indicesPoints[0]=Nbeg;
  pE->indicesPoints[pE->nPoints-1]=Nend;
  pE->_abscCurv=(double*)malloc((pE->nPoints)*sizeof(double));
}
void buildEdges(){
  int iE,iP;
  EDGES=(struct edge*)malloc(NEdges*sizeof(struct edge));
  struct edge* curpE=EDGES;
  for (iE=0;iE<NEdges;iE++){
    buildEdgeGeom(&(curpE->geom),NodeEdges[2*iE],NodeEdges[2*iE+1],NumberOfPointsInEdge[iE]);
    curpE->popType=PopTypeEdges[iE];
    curpE->_bcinf.type=COUPLE1D;
    curpE->_bcsup.type=COUPLE1D;
    curpE->_isShared=0;
    curpE++;
  }
  
}
void freeEdges(){
  struct edge* curpE=EDGES;
  int iE;
  for (iE=0;iE<NEdges;iE++){
    free(curpE->geom.indicesPoints);
    free(curpE->geom._abscCurv);
    curpE++;
  }
  free(EDGES);
}
int checkEdge(int ii){
  double dirx,diry,yE,x,y;
  dirx=PointsX[GET_EDGE_NODE_END(ii)]-PointsX[GET_EDGE_NODE_BEGIN(ii)];
  diry=PointsY[GET_EDGE_NODE_END(ii)]-PointsY[GET_EDGE_NODE_BEGIN(ii)];
  int res=0;
  
  for (int numP=0;numP<EDGES[ii].geom.nPoints-2;numP++){
     double dx=PointsX[EDGES[ii].geom.indicesPoints[numP+1]]-PointsX[EDGES[ii].geom.indicesPoints[numP]];
     double dy=PointsY[EDGES[ii].geom.indicesPoints[numP+1]]-PointsY[EDGES[ii].geom.indicesPoints[numP]];
     if (dx*dirx+dy*diry < 0){
       printf("\nERREUR IN GEOMETRIE ABOUT EDGE %i from edge geometrie %i\n",ii,EDGES[ii].geom.idInGeomFile);
       printf("  vector between points %i %i is not in the right direction\n",numP,numP+1);
       res=1;
     }
  }
  return res;
}
void printWires(){
  int ii,jj;
  struct wireSide *pAux=WIRES;
  printf("There are %i domains.\n",Nwires);
  int errg=0;
  printf("[indexEdge idInGeomFile isReversed]\n");
  for (ii=0;ii<Nwires;ii++){
    printf("In domain %i,\nThere are %i edges:\n",ii,Wsizes[ii]);
    for (jj=0;jj<Wsizes[ii];jj++){
      errg+=checkEdge(pAux->indexEdge);
      printf("[%i (%i) %i]",pAux->indexEdge,EDGES[pAux->indexEdge].geom.idInGeomFile,pAux->isReversed);
      pAux++;
    }
    printf("\n");
  }
  if (errg){
    printf("cant go head because edges are bad\n");
    exit(0);
  }else{
    printf("Geometrie seems good\n");
  }
}

