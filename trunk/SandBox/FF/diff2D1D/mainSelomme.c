#include "gen2speciesNL.c"
//#define GEOM_DESCR "paysage20_bis.h"
//#define GEOM_DESCR "testFuncSimple.h"
//#define GEOM_DESCR "testFunc.h"
//#define GEOM_DESCR "vacher.h"
#define GEOM_DESCR "tessellation.h"
//#define GEOM_DESCR "simple3.h"
//#define GEOM_DESCR "tigerInter.h"
#include GEOM_DESCR



int main(int argc, char *argv[]){
  if (argc != 2){
    printf("usage: updateSelome /mnt/../SELOMMES\n");
    exit(1);
  }else{
    strcpy(OUTPUTDIR,argv[1]);
    printf("doing update in directory %s\n",OUTPUTDIR);
    sprintf(GEODIR,"%s",OUTPUTDIR);
    
  }
  //exit(0);
    
  int ii;
  initStructureForUpdate();
  buildDirectories();

  //exit(0);
  //randomizePoint(1.5);
  initMatStruct();
//  printViewVtk();
//  printVtkViewWires();
//  printSaveMeshes();
  
  printParams();

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defOutputGen.edp");
  FILE *pOut = fopen(FILENAME, "w");
  buildDefOutput(pOut);
  fclose(pOut);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defVarfGen.edp");
  FILE *pVarfs = fopen(FILENAME, "w");
  printVarfs(pVarfs);
  fclose(pVarfs);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"massInfo.txt");
  FILE *pMassInfo = fopen(FILENAME,"w");
  printMassInfo(pMassInfo);
  fclose(pMassInfo);


  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defXXYYGen.edp");
  FILE *pXY = fopen(FILENAME, "w");
  printXXYY(pXY);
  fclose(pXY);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defPluginGen.edp");
  FILE *pPlug = fopen(FILENAME, "w");
  printPlugin(pPlug);
  fclose(pPlug);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defICGen.edp");
  // if(access( FILENAME, F_OK ) == -1){
    FILE *pIC = fopen(FILENAME, "w");
    printf("call IC\n");
    printInitialCond(pIC,1);
    fclose(pIC);
//  }

  
  freeEdges();
  resetMatStruct();
  freeStructs();

  
}
