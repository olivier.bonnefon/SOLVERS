#include <stdio.h>
#include <math.h>
#include <stdlib.h>
/*
Vh0 uc=utm10s4+utm10s5+utm10s6+utm10s7+utm10s8;
    visuScal("2D1",Th0,uc,10);
Vh0 uc=utm10s0+utm10s1+utm10s2+utm10s3;

    cout<<"M1D0s3 "<<M1D0s3<<endl;


cout<<"M1D0s1 "<<M1D0s1<<endl;
    
 */
#include "simple8.h"



int curLab=1;
int LEDGES[512];
//vecteur unitaire des edges
double *pVE;
int maxDofs1d=-1;
char caux1[512];
char caux2[512];
char caux3[512];
char caux4[512];

int * pEdgeIsShared;

enum MatrixType{
  VARF2D,
  VARF1D,
  COUPLAGE_1Dto2D,
  COUPLAGE_2Dto1D,
  COUPLAGE_EDGE,
  COUPLAGE_DOMAIN
};

typedef struct{
  enum MatrixType type;
  int index2D1;
  int index1D1;
  int index2D2;
  int index1D2;
}MatrixStruct;


MatrixStruct **ppMS;
int NB_BLOCK_LINES=0;

void printMatrixName(FILE *pF, MatrixStruct *pMS){
  if (!pMS){
    fprintf(pF,"0");
    return;
  }
  switch (pMS->type)
  {
  case VARF2D:
    fprintf(pF,"M2D%i",pMS->index2D1);
    break;
  case VARF1D:
    fprintf(pF,"M1D%is%i",pMS->index2D1,pMS->index1D1);
    break;
  case COUPLAGE_1Dto2D:
    fprintf(pF,"MMNn%is%i",pMS->index2D1,pMS->index1D1);
    break;
  case COUPLAGE_2Dto1D:
    fprintf(pF,"MMnN%is%i",pMS->index2D1,pMS->index1D1);
    break;
  case COUPLAGE_EDGE:
    fprintf(pF,"C%is%is%i",pMS->index2D1,pMS->index1D1,pMS->index1D2);
    break;
  case COUPLAGE_DOMAIN:
    fprintf(pF,"C1DE%is%ionE%is%i",pMS->index2D1,pMS->index1D1,pMS->index2D2,pMS->index1D2);
    break;
  default :
    printf("default in printMatrixName\n");
    break;
  }
  
}

void printPoints(FILE *pF){
  int i;
  fprintf(pF,"//BEGIN POINTS\n");
  fprintf(pF,"int NPoints=%i;\n",NPoints);
  fprintf(pF,"real[int] Px=[\n");
  for (i=0;i<NPoints;i++){
    fprintf(pF,"\t%e",Px[i]);
    if (i<NPoints-1)
       fprintf(pF,",\n");
  }
  fprintf(pF,"];\n");
  fprintf(pF,"real[int] Py=[\n");
  for (i=0;i<NPoints;i++){
    fprintf(pF,"\t%e",Py[i]);
    if (i<NPoints-1)
       fprintf(pF,",\n");
  }
  fprintf(pF,"];\n");
  fprintf(pF,"//END POINTS\n");
}
void printEdges(FILE *pF){
  pVE=(double*)malloc(2*NEdges*sizeof(double));
  int ii;
  fprintf(pF,"//BEGIN LABEL EDGES\n");
  for (ii=0;ii<NEdges;ii++){
    fprintf(pF,"int labE%i=%i;\n",ii,curLab);
    curLab++;
  }
  fprintf(pF,"//END LABEL EDGES\n");
  fprintf(pF,"//BEGIN EDGES\n");
  fprintf(pF,"int NEdges=%i;\n",NEdges);
  for (ii=0;ii<NEdges;ii++){
    fprintf(pF,"border EDGE%i(t=0,1){x=(1-t)*%e + t*%e  ; y =(1-t)*%e + t*%e;label= labE%i;};\n",
           ii,Px[EDGES[2*ii]],Px[EDGES[2*ii+1]],Py[EDGES[2*ii]],Py[EDGES[2*ii+1]],ii);
  }
  fprintf(pF,"//END EDGES\n");

  fprintf(pF,"//BEGIN DECL CST FOR GET 1D DOFS\n");
  fprintf(pF,"//ELi for the length oh the edge i\n");
  fprintf(pF,"//EAi for the abscisse of the normalized edge vector\n");
  fprintf(pF,"//EAi for the ordonate of the normalized edge vector\n");
  fprintf(pF,"//\n");
  for (ii=0;ii<NEdges;ii++){
    double aux=sqrt((Px[EDGES[2*ii+1]]-Px[EDGES[2*ii]])*(Px[EDGES[2*ii+1]]-Px[EDGES[2*ii]])+
                    (Py[EDGES[2*ii+1]]-Py[EDGES[2*ii]])*(Py[EDGES[2*ii+1]]-Py[EDGES[2*ii]]));
    LEDGES[ii]=(int)aux+1;
    fprintf(pF,"real EL%i=%e;\n",ii,aux);
    pVE[2*ii]=(Px[EDGES[2*ii+1]]-Px[EDGES[2*ii]])/aux;
    pVE[2*ii+1]=(Py[EDGES[2*ii+1]]-Py[EDGES[2*ii]])/aux;
    fprintf(pF,"real EA%i=%e;\n",ii,(Px[EDGES[2*ii+1]]-Px[EDGES[2*ii]])/aux);
    fprintf(pF,"real EB%i=%e;\n",ii,(Py[EDGES[2*ii+1]]-Py[EDGES[2*ii]])/aux);
  }
  fprintf(pF,"//END DECL CST FOR GET 1D DOFS\n");
}
void printMeshes(FILE *pF){
  int ii,jj;
  fprintf(pF,"//BEGIN MESHES\n");
  fprintf(pF,"//node per unit length\n");
  fprintf(pF,"int n=%i;\n",nPerLength);
  fprintf(pF,"int Nwires=%i;\n",Nwires);
  fprintf(pF,"int[int] Wsizes=[\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t%i",Wsizes[ii]);
    if (ii<Nwires-1)
      fprintf(pF,",\n");
  }
  fprintf(pF,"\t];\n\n");
  fprintf(pF,"int[int] WIRES=[\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t%i,%i",WIRES[2*ii],WIRES[2*ii+1]);
    if (ii<Nwires-1)
      fprintf(pF,",\n");
  }
  fprintf(pF,"\t];\n\n");
  
  fprintf(pF,"include \"geomMacro.edp\"\n");

  int* pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"mesh Th%i=buildmesh(\n",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"\tEDGE%i(n*(%i))",abs(*pW),(*pW)>=0?LEDGES[*pW]:-LEDGES[-(*pW)]);
      if (nPerLength*LEDGES[abs(*pW)]>maxDofs1d)
        maxDofs1d=nPerLength*LEDGES[abs(*pW)];
      if (jj<Wsizes[ii]-1)
        fprintf(pF,"+\n");
      pW++;
    }
    fprintf(pF,",fixeborder=true);\n");
  }
  fprintf(pF,"plot(");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"Th%i",ii);
    if (ii<Nwires-1)
      fprintf(pF,",");
  }
  fprintf(pF,",wait=true);\n");
  fprintf(pF,"//END MESHES\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"fespace Vh%i(Th%i,P1);\n",ii,ii);
    fprintf(pF,"Vh%i u2d%i;\n",ii,ii);
    fprintf(pF,"Vh%i u2dtm1%i;\n",ii,ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"Vh%i utm1%is%i;\n",ii,ii,abs(*pW));
      pW++;
    }
  }
  
}
void printDofs(FILE *pF){
  int ii,jj;
  fprintf(pF,"//BEGIN DOFS DEF\n");
  fprintf(pF,"int[int] zoibuff(%i);\n",maxDofs1d+2);
  int* pW=WIRES;
  fprintf(pF,"\t//BEGIN DOFS declar\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"//Omega %i\n",ii);
    fprintf(pF,"int n2Ddofs%i=Vh%i.ndof;\n",ii,ii);
    fprintf(pF,"int ");
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"n1Ddofs%is%i",ii,abs(*pW));
      if (jj<Wsizes[ii]-1)
        fprintf(pF,",");
      pW++;
    }
    fprintf(pF,";\n");
  }
  fprintf(pF,"\t//END DOFS declar\n");
  pW=WIRES;
  fprintf(pF,"\t//BEGIN GET 1D DOFS index\n");
  for (ii=0;ii<Nwires;ii++){
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"boundarydofs(labE%i,EA%i*x+EB%i*y-100000,Th%i,Vh%i,zoibuff,n1Ddofs%is%i);\n",abs(*pW),abs(*pW),abs(*pW),ii,ii,ii,abs(*pW));
      fprintf(pF,"int[int] zoi%is%i(n1Ddofs%is%i);\n",ii,abs(*pW),ii,abs(*pW));
      fprintf(pF,"zoi%is%i=zoibuff(0:n1Ddofs%is%i);\n\n",ii,abs(*pW),ii,abs(*pW));
      pW++;
    }
  }
  fprintf(pF,"\t//END GET 1D DOFS index\n");

  fprintf(pF,"\t//BEGIN 1D DOFS declar\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"real[int] ");
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"rhs1d%is%i(n1Ddofs%is%i)",ii,abs(*pW),ii,abs(*pW));
      if (jj<Wsizes[ii]-1)
        fprintf(pF,",");
      pW++;
    }
    fprintf(pF,";\n");
  }
  fprintf(pF,"\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"real[int] ");
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"pu1d%is%i(n1Ddofs%is%i)",ii,abs(*pW),ii,abs(*pW));
      if (jj<Wsizes[ii]-1)
        fprintf(pF,",");
      pW++;
    }
    fprintf(pF,";\n");
  }

  fprintf(pF,"\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"real[int] ");
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"pu1dtm1%is%i(n1Ddofs%is%i)",ii,abs(*pW),ii,abs(*pW));
      if (jj<Wsizes[ii]-1)
        fprintf(pF,",");
      pW++;
    }
    fprintf(pF,";\n");
  }
  fprintf(pF,"\n");

  pW=WIRES;
  fprintf(pF,"real[int] sol(");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"n2Ddofs%i+",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"n1Ddofs%is%i",ii,abs(*pW));
      if (jj<Wsizes[ii]-1 || ii<Nwires-1)
        fprintf(pF,"+");
      pW++;
    }
    if(ii<Nwires-1)
      fprintf(pF,"\n");
  }
  fprintf(pF,");\n");

  pW=WIRES;
  fprintf(pF,"real[int] rhs(");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"n2Ddofs%i+",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"n1Ddofs%is%i",ii,abs(*pW));
      if (jj<Wsizes[ii]-1 || ii<Nwires-1)
        fprintf(pF,"+");
      pW++;
    }
    if(ii<Nwires-1)
      fprintf(pF,"\n");
  }
  fprintf(pF,");\n");
  fprintf(pF,"\n");
  fprintf(pF,"\t//END 1D DOFS declar\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"real[int] rhs2d%i(n2Ddofs%i);\n",ii,ii);
  }
  fprintf(pF,"\n");
  fprintf(pF,"//END DOFS DEF\n");
  fprintf(pF,"\n");
  
}

void printVarfs(FILE *pF){
  int ii,jj;
  int* pW=WIRES;
  fprintf(pF,"//BEGIN VARFS\n");
  fprintf(pF,"real D2DX=0.05;\nreal D2DY=0.05;\n");

  fprintf(pF,"real ux0=1;\nreal ux1=0;\nreal dux1=0;\nreal dux0=0;\n");

  fprintf(pF,"//\tBEGIN MATRIX DEC\n");
  fprintf(pF,"matrix A,MM,Maux;\n");
  fprintf(pF,"real D1D=10.0;\n");
  for (ii=0;ii<Nwires;ii++)
    fprintf(pF,"matrix M2D%i;\n",ii);
  pW=WIRES;
  fprintf(pF,"matrix ");
  for (ii=0;ii<Nwires;ii++){
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"M1D%is%i,",ii,abs(*pW));
      fprintf(pF,"MASSE1D%is%i,",ii,abs(*pW));
      fprintf(pF,"LAP1D%is%i",ii,abs(*pW));
      if (jj<Wsizes[ii]-1|| ii < Nwires-1)
        fprintf(pF,",");
      pW++;
    }
  }
  fprintf(pF,";\n");
  fprintf(pF,"//\tEND MATRIX DEC\n\n");

  fprintf(pF,"//begin 1Dx1D varf\n");
  pW=WIRES;
//  int numEdgeInWire=0;
  
  fprintf(pF,"real aux;\n");
  for (ii=0;ii<Nwires;ii++){
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"aux=EL%i/(n1Ddofs%is%i-1);\n",abs(*pW),ii,abs(*pW));
      fprintf(pF,"my1DvarfCSR(M1D%is%i,MASSE1D%is%i,LAP1D%is%i,n1Ddofs%is%i,aux,D1D,alpha,%s);\n",
              ii,abs(*pW),
              ii,abs(*pW),
              ii,abs(*pW),
              ii,abs(*pW),
              pEdgeIsShared[abs(*pW)]?"malpha":"mu");
      pW++;
//      numEdgeInWire++;
    }
    fprintf(pF,"\n");
  }
  fprintf(pF,"//\tend 1Dx1D varf\n\n");

  fprintf(pF,"//begin 1Dx1D domain couple\n");
  for(ii=0;ii<NB_BLOCK_LINES;ii++){
    for(jj=0;jj<NB_BLOCK_LINES;jj++){
      if (ppMS[ii+jj*NB_BLOCK_LINES] && (ppMS[ii+jj*NB_BLOCK_LINES])->type == COUPLAGE_DOMAIN){
        fprintf(pF,"matrix ");
        printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
        fprintf(pF,"=-1*alpha1D*MASSE1D%is%i;\n",
                (ppMS[ii+jj*NB_BLOCK_LINES])->index2D1,
                (ppMS[ii+jj*NB_BLOCK_LINES])->index1D1);
      }
    }
  }
  
  fprintf(pF,"//end 1Dx1D domain couple\n\n");

  
  fprintf(pF,"//\tbegin 2D ---> 1D varf\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"int[int] allLine%i(n2Ddofs%i);\n",ii,ii);
    fprintf(pF,"for (int ii=0; ii<n2Ddofs%i; ii++)\n",ii);
    fprintf(pF,"\tallLine%i[ii]=ii;\n",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"varf varfMat1D2D%is%i(v,w) = -int1d(Th%i,labE%i)(nu*v*w);\n",ii,abs(*pW),ii,abs(*pW));
      fprintf(pF,"Maux=varfMat1D2D%is%i(Vh%i,Vh%i);\n",ii,abs(*pW),ii,ii);
      fprintf(pF,"matrix MMnN%is%i=Maux(zoi%is%i,allLine%i);\n",ii,abs(*pW),ii,abs(*pW),ii);
      pW++;
    }
    fprintf(pF,"\n");
  }

  fprintf(pF,"//\tend 2D ---> 1D varf\n");
  fprintf(pF,"//\tbegin 2D  varf\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"varf varfMat2D%i(v,w)=int2d(Th%i)(alpha*v*w+D2DX*dx(v)*dx(w)+D2DY*dy(v)*dy(w))+\n",ii,ii);
    fprintf(pF,"\tint1d(Th%i,",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"labE%i",abs(*pW));
      if (jj<Wsizes[ii]-1 )
        fprintf(pF,",");
      pW++;
    }
    fprintf(pF,")(nu*v*w);\n");
    fprintf(pF,"varf varfRhs2D%i(v,w)= int2d(Th%i)(alpha*u2dtm1%i*w);\n",ii,ii,ii);
    fprintf(pF,"M2D%i=varfMat2D%i(Vh%i,Vh%i);\n",ii,ii,ii,ii);
  }

  fprintf(pF,"//\tend 2D  varf\n");

  fprintf(pF,"//\tbegin 1D ---> 2D  varf\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"varf varfMat2D1D%is%i(u,w)=-int1d(Th%i,labE%i)(mu*u*w);\n",ii,abs(*pW),ii,abs(*pW));
      fprintf(pF,"Maux=varfMat2D1D%is%i(Vh%i,Vh%i);\n",ii,abs(*pW),ii,ii);
      fprintf(pF,"matrix MMNn%is%i=Maux(allLine%i,zoi%is%i);\n",ii,abs(*pW),ii,ii,abs(*pW));
      pW++;
    }
    fprintf(pF,"\n");
  }
  
  fprintf(pF,"//\tend 1D ---> 2D  varf\n");
  fprintf(pF,"\n");



  
  fprintf(pF,"\n");
  fprintf(pF,"\n");
  fprintf(pF,"//END VARFS\n");
 
}
void printMatStruct(FILE *pF);
void printMat(FILE *pF){
  int ii,firstEdge,prevEdge,curEdge,jj,kk;
  int* pWsav,*pW=WIRES;
  int Nedges=0;
  int indexInI1I2=0;
  int NedgesMaxPerDom=0;
  for (ii=0;ii<Nwires;ii++){
    Nedges+=Wsizes[ii];
    if (Wsizes[ii]>NedgesMaxPerDom)
      NedgesMaxPerDom=Wsizes[ii];
  }
  
  
  
  fprintf(pF,"//BEGIN MAT DEF\n");
  fprintf(pF,"\n");
  fprintf(pF,"int[int] indexBeginMat(%i*%i);\n",Nwires,NedgesMaxPerDom);
  fprintf(pF,"int curIndexBlock=0;\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"curIndexBlock+=n2Ddofs%i;\n",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"indexBeginMat[%i*%i+%i]=curIndexBlock;\n",ii,NedgesMaxPerDom,jj);
      curEdge=abs(*pW);
      pW++;
      fprintf(pF,"curIndexBlock+=n1Ddofs%is%i;\n",ii,curEdge);
    }
//    indexBeginMat[NedgesMaxPerDom*ii]=curIndexBlock;
    fprintf(pF,"\n");
  }

  fprintf(pF,"int[int] I1I2(%i);\n",2*Nedges);
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    prevEdge=abs(*pW);
    firstEdge=prevEdge;
    pW++;
    for (jj=1;jj<Wsizes[ii]+1;jj++){
      if (jj < Wsizes[ii]){
        curEdge=abs(*pW);
        pW++;
      }else
        curEdge=firstEdge;
      sprintf(caux1,"i%i%is%is%i",prevEdge,ii,prevEdge,curEdge);
      sprintf(caux2,"i%i%is%is%i",curEdge,ii,prevEdge,curEdge);
      fprintf(pF,"int %s, %s;\n",caux1,caux2);
      //sprintf(caux3,"C%is%is%i",ii,prevEdge,curEdge);
      //sprintf(caux4,"C%is%is%i",ii,curEdge,prevEdge);
      //fprintf(pF,"matrix %s,%s;\n",caux3,caux4);
      fprintf(pF,"getCommunDof(zoi%is%i,zoi%is%i,%s,%s);\n",ii,prevEdge,ii,curEdge,caux1,caux2);
/*      fprintf(pF,"plugCSR(M1D%is%i,M1D%is%i,%s,%s,zoi%is%i,zoi%is%i,%s,%s);\n",
              ii,prevEdge,ii,curEdge,
              caux3,caux4,
              ii,prevEdge,ii,curEdge,
              caux1,caux2);*/
      fprintf(pF,"I1I2[2*%i]=%s+indexBeginMat[%i*%i+%i];\n",indexInI1I2,caux1,ii,NedgesMaxPerDom,jj-1);
      fprintf(pF,"I1I2[2*%i+1]=%s+indexBeginMat[%i*%i+%i];\n",indexInI1I2,caux2,ii,NedgesMaxPerDom,(jj < Wsizes[ii])?jj:0);
      indexInI1I2=indexInI1I2+1;
      /* fprintf(pF,"{\n"); */
      /* fprintf(pF,"\tmatrix lin;\n"); */
      /* fprintf(pF,"\tgetLineMatrice(MMnN%is%i,MMnN%is%i,lin,%s,%s);\n",ii,curEdge,ii,prevEdge,caux1,caux2); */
      /* fprintf(pF,"\tMMnN%is%i=MMnN%is%i+lin;\n",ii,prevEdge,ii,prevEdge); */
      /* fprintf(pF,"\t\n"); */
      /* fprintf(pF,"\t\n"); */
      /* fprintf(pF,"}\n"); */
      prevEdge=curEdge;
      fprintf(pF,"\n");
      
    }
    
    fprintf(pF,"\n\n");
  }

  printMatStruct(pF);
  fprintf(pF,"matrix M;\n");
  fprintf(pF,"OBtestCSR(M1,M,I1I2);\n");
  fprintf(pF,"set(M,solver=GMRES);\n");
  fprintf(pF,"//END MAT DEF\n");
}

void resetMatStruct(){
  int ii, jj;
  for(ii=0;ii<NB_BLOCK_LINES;ii++)
    for(jj=0;jj<NB_BLOCK_LINES;jj++)
      if (ppMS[ii+jj*NB_BLOCK_LINES])
        free(ppMS[ii+jj*NB_BLOCK_LINES]);
  free(ppMS);
  
}

void printMatStruct(FILE *pF){
  int ii, jj;
  fprintf(pF,"matrix M1=[\n");
  for(ii=0;ii<NB_BLOCK_LINES;ii++){
    fprintf(pF,"[");
    for(jj=0;jj<NB_BLOCK_LINES;jj++){
      printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
      if (jj<NB_BLOCK_LINES-1)
        fprintf(pF,",\t");
    }
    fprintf(pF,"]");
    if(ii<NB_BLOCK_LINES-1)
      fprintf(pF,",");
    fprintf(pF,"\n");
  }
  fprintf(pF,"];\n");
}
void initMatStruct(){
  int ii, jj;
  pEdgeIsShared=(int*)calloc(NEdges,sizeof(int));
  
  NB_BLOCK_LINES=Nwires;
  for (ii=0;ii<Nwires;ii++)
    NB_BLOCK_LINES+=Wsizes[ii];
  ppMS=( MatrixStruct**)calloc(NB_BLOCK_LINES*NB_BLOCK_LINES,sizeof( MatrixStruct**));

  //les blocs 2D
  int curIndex=0;
  for (ii=0;ii<Nwires;ii++){
    ppMS[curIndex+curIndex*NB_BLOCK_LINES]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
    ppMS[curIndex+curIndex*NB_BLOCK_LINES]->type=VARF2D;
    ppMS[curIndex+curIndex*NB_BLOCK_LINES]->index2D1=ii;
    curIndex=curIndex+1+Wsizes[ii];
  }
  //le blocs 1D
  curIndex=0;
  int *pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    curIndex++;
    for (jj=0;jj<Wsizes[ii];jj++){
      ppMS[curIndex+curIndex*NB_BLOCK_LINES]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
      ppMS[curIndex+curIndex*NB_BLOCK_LINES]->type=VARF1D;
      ppMS[curIndex+curIndex*NB_BLOCK_LINES]->index2D1=ii;
      ppMS[curIndex+curIndex*NB_BLOCK_LINES]->index1D1=abs(*pW);
      pW++;
      curIndex++;
      if ((*pW)<0){
        pEdgeIsShared[abs(*pW)]=1;
      }
    }
  }
  //1D --> 2D
  int line2D=0;
  int curCol=0;
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    curCol++;
    for (jj=0;jj<Wsizes[ii];jj++){
      ppMS[line2D+curCol*NB_BLOCK_LINES]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
      ppMS[line2D+curCol*NB_BLOCK_LINES]->type=COUPLAGE_1Dto2D;
      ppMS[line2D+curCol*NB_BLOCK_LINES]->index2D1=ii;
      ppMS[line2D+curCol*NB_BLOCK_LINES]->index1D1=abs(*pW);
      curCol++;
      pW++;
    }
    line2D=line2D+1+Wsizes[ii];
  }

  //2D --> 1D
  int col2D=0;
  int curLine=0;
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    curLine++;
    for (jj=0;jj<Wsizes[ii];jj++){
      ppMS[curLine+col2D*NB_BLOCK_LINES]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
      ppMS[curLine+col2D*NB_BLOCK_LINES]->type=COUPLAGE_2Dto1D;
      ppMS[curLine+col2D*NB_BLOCK_LINES]->index2D1=ii;
      ppMS[curLine+col2D*NB_BLOCK_LINES]->index1D1=abs(*pW);
      curLine++;
      pW++;
    }
    col2D=col2D+1+Wsizes[ii];
  }

  //couplage edge
  /*int line1D=0;
  curCol=0;
  line2D=0;
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    line1D++;
    int prev1D=Wsizes[ii]-1;
    int next1D=1;
    for (jj=0;jj<Wsizes[ii];jj++){
      curCol=line2D+1+prev1D;
      ppMS[line1D+curCol*NB_BLOCK_LINES]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
      ppMS[line1D+curCol*NB_BLOCK_LINES]->type=COUPLAGE_EDGE;
      ppMS[line1D+curCol*NB_BLOCK_LINES]->index2D1=ii;
      ppMS[line1D+curCol*NB_BLOCK_LINES]->index1D1=abs(pW[jj]);
      ppMS[line1D+curCol*NB_BLOCK_LINES]->index1D2=abs(pW[prev1D]);


      curCol=line2D+1+next1D;
      ppMS[line1D+curCol*NB_BLOCK_LINES]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
      ppMS[line1D+curCol*NB_BLOCK_LINES]->type=COUPLAGE_EDGE;
      ppMS[line1D+curCol*NB_BLOCK_LINES]->index2D1=ii;
      ppMS[line1D+curCol*NB_BLOCK_LINES]->index1D1=abs(pW[jj]);
      ppMS[line1D+curCol*NB_BLOCK_LINES]->index1D2=abs(pW[next1D]);
      
      line1D++;
      prev1D=(prev1D+1)%Wsizes[ii];
      next1D=(next1D+1)%Wsizes[ii];
    }
    pW=pW+Wsizes[ii];
    line2D=line2D+1+Wsizes[ii];
   
  }*/
  
  //couplage domain
  int *pWneg=WIRES;
  int lineNeg=0;
  int NumNegWire,NumNegEdge,NumPosWire,NumPosEdge;
  
  for (NumNegWire=0;NumNegWire<Nwires;NumNegWire++){
    lineNeg++;
    for (NumNegEdge=0;NumNegEdge<Wsizes[NumNegWire];NumNegEdge++){
      if (*pWneg<0){
        int colPos=0;
        int *pWpos=WIRES;
        for (NumPosWire=0;NumPosWire<Nwires;NumPosWire++){
          colPos++;
          for (NumPosEdge=0;NumPosEdge<Wsizes[NumPosWire];NumPosEdge++){
            if ((*pWpos)==abs(*pWneg)){
              ppMS[colPos+lineNeg*NB_BLOCK_LINES]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
              ppMS[colPos+lineNeg*NB_BLOCK_LINES]->type=COUPLAGE_DOMAIN;
              ppMS[colPos+lineNeg*NB_BLOCK_LINES]->index2D1=NumNegWire;
              ppMS[colPos+lineNeg*NB_BLOCK_LINES]->index1D1=abs(*pWneg);
              ppMS[colPos+lineNeg*NB_BLOCK_LINES]->index2D2=NumPosWire;
              ppMS[colPos+lineNeg*NB_BLOCK_LINES]->index1D2=*pWpos;


              ppMS[lineNeg+colPos*NB_BLOCK_LINES]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
              ppMS[lineNeg+colPos*NB_BLOCK_LINES]->type=COUPLAGE_DOMAIN;
              ppMS[lineNeg+colPos*NB_BLOCK_LINES]->index2D2=NumNegWire;
              ppMS[lineNeg+colPos*NB_BLOCK_LINES]->index1D2=abs(*pWneg);
              ppMS[lineNeg+colPos*NB_BLOCK_LINES]->index2D1=NumPosWire;
              ppMS[lineNeg+colPos*NB_BLOCK_LINES]->index1D1=*pWpos;


              
              NumPosEdge=Wsizes[NumPosWire];
              NumPosWire=Nwires;
            }
            colPos++;
            pWpos++;
          }
        }

      }
      lineNeg++;
      pWneg++;
    }
  }
}
#define WITHMASSECOMPUTE
void printTS(FILE *pF){
  int ii,jj;
  int *pW=WIRES;
  
  fprintf(pF,"//BEGIN TIME STEPPING\n");
  fprintf(pF,"for (int curStep=1;curStep<=nbSteps;curStep++){\n");
  fprintf(pF,"\t//build rhs\n");
  for(ii=0;ii<Nwires;ii++){
    fprintf(pF,"\trhs2d%i=varfRhs2D%i(0,Vh%i);\n",ii,ii,ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"\trhs1d%is%i=MASSE1D%is%i*pu1d%is%i;rhs1d%is%i*=alpha;\n",
              ii,abs(*pW),ii,abs(*pW),ii,abs(*pW),ii,abs(*pW));
      pW++;
    }
  }
  fprintf(pF,"\n");
  fprintf(pF,"\t//couple 1D rhs\n");
  pW=WIRES;
  for(ii=0;ii<Nwires;ii++){
    int next1D=1;
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"\trhs1d%is%i(i%i%is%is%i)=rhs1d%is%i(i%i%is%is%i)+rhs1d%is%i(i%i%is%is%i);\n",
              ii,abs(pW[jj]),
              abs(pW[jj]),ii,abs(pW[jj]),abs(pW[next1D]),
              ii,abs(pW[jj]),
              abs(pW[jj]),ii,abs(pW[jj]),abs(pW[next1D]),
              ii,abs(pW[next1D]),
              abs(pW[next1D]),ii,abs(pW[jj]),abs(pW[next1D]));
      next1D=(next1D+1)%(Wsizes[ii]);
    }
    pW=pW+Wsizes[ii];
  }
  pW=WIRES;
  fprintf(pF,"\trhs=[");
  for(ii=0;ii<Nwires;ii++){
    fprintf(pF,"rhs2d%i,",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"rhs1d%is%i",ii,abs(*pW));
      pW++;
      if (jj<Wsizes[ii]-1 || ii<Nwires-1)
        fprintf(pF,",");
    }
    if (ii<Nwires-1)
      fprintf(pF,"\n");
  }
  
  fprintf(pF,"\t];\n");

  fprintf(pF,"\tsol=M^-1*rhs;\n");
  pW=WIRES;
  fprintf(pF,"\tint curDof=0;\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"\tu2d%i[]=sol(curDof:curDof+n2Ddofs%i-1);\n",ii,ii);
    fprintf(pF,"\tcurDof=curDof+n2Ddofs%i;\n",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"\tpu1d%is%i=sol(curDof: curDof+n1Ddofs%is%i-1);\n",
              ii,abs(*pW),ii,abs(*pW));
      fprintf(pF,"\tcurDof=curDof+n1Ddofs%is%i;\n",ii,abs(*pW));
      pW++;
    }
  }
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"\tu2dtm1%i=u2d%i;\n",ii,ii);
  }
  /*pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"\tconvert1Dto2D2(pu1d%is%i,utm1%is%i,n1Ddofs%is%i,zoi%is%i);\n",
              ii,abs(*pW),ii,abs(*pW),ii,abs(*pW),ii,abs(*pW));
      pW++;
    }
    }*/
  
  fprintf(pF,"\tif (curStep%%freqVtk == 0){\n");
#ifdef WITHMASSECOMPUTE
#endif
#ifdef WITHMASSECOMPUTE
  fprintf(pF,"\t\treal masse2D=0;\n");
  fprintf(pF,"\t\treal masse1D=0;\n");
  fprintf(pF,"\t\treal masse1Dbuf=0;\n");
#endif
  
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
#ifdef WITHMASSECOMPUTE
    fprintf(pF,"\t\tmasse2D+=int2d(Th%i)(u2d%i);\n",ii,ii);
#endif
    fprintf(pF,"\t\tsavevtk(\"u%is\"+string(curStep)+\".vtk\",Th%i,u2d%i,dataname=\"UU\");\n",ii,ii,ii);
    fprintf(pF,"\t\t{\n");
    fprintf(pF,"\t\t\tofstream pWire(\"u1d\"+string(curStep)+\"s%i.txt\");\n",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      int notLast=1;
      int revers=0;
      if (*pW<0)
        revers=1;
      if (jj==Wsizes[ii]-1)
        notLast=0;
      fprintf(pF,"\t\t\tprintVect(pWire,%i,pu1d%is%i,n1Ddofs%is%i-%i);\n",revers,ii,abs(*pW),ii,abs(*pW),notLast);
#ifdef WITHMASSECOMPUTE
      fprintf(pF,"\t\t\tcomput1d(masse1Dbuf,(EL%i/(n1Ddofs%is%i-1)),pu1d%is%i,n1Ddofs%is%i);\n",abs(*pW),ii,abs(*pW),ii,abs(*pW),ii,abs(*pW));
      fprintf(pF,"\t\t\tmasse1D+=masse1Dbuf;\n");
#endif
    
      pW++;
    }
    fprintf(pF,"\t\t}\n");

  }
#ifdef WITHMASSECOMPUTE
    fprintf(pF,"\tcout<<\"masse2D=\"<<masse2D<<\" \"<<\"masse1D=\"<<masse1D<<\" \"<<\"masse=\"<<masse1D+masse2D<<endl;\n");
#endif
  fprintf(pF,"\t}\n");

  fprintf(pF,"\tif (curStep%%freqAff == 0){\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t\tvisuScal(\"2D%i\",Th%i,u2d%i,10);\n",ii,ii,ii);
  }
  fprintf(pF,"\t}\n");

  
  fprintf(pF,"\t\n");
  fprintf(pF,"\t\n");

  fprintf(pF,"}\n");
  fprintf(pF,"//END TIME STEPPING\n");
  
}

void printViewVtk(){
  int ii,kk;
  char filename[256];
  FILE *pmain=fopen("view.sh", "w");
  
  for(kk=0; kk<10;kk++){
    sprintf(filename,"view%i.py",kk);
    fprintf(pmain,"python %s\n",filename);
    FILE *pVtk = fopen(filename, "w");
    fprintf(pVtk,"from paraview.simple import *\n");
    fprintf(pVtk,"import sys\nbaseName=\"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/u\"\n");
    fprintf(pVtk,"freq=10\n");
    fprintf(pVtk,"minv=0.0003\n");
    fprintf(pVtk,"maxv=0.0005\n");

    fprintf(pVtk,"for num in range(%i,%i):\n",kk*20+1,(kk+1)*20+1);
    for(ii=0;ii<Nwires;ii++){
      fprintf(pVtk,"\tfileName%i=baseName+str(%i)+str(freq*num)+\".vtk\"\n",ii,ii);
      fprintf(pVtk,"\treader%i = OpenDataFile(fileName%i)\n",ii,ii);
    }
    fprintf(pVtk,"\tShow()\n");
    fprintf(pVtk,"\tview = GetActiveView()\n\tview.CameraPosition = [0,0,100]\n\tview.ViewSize = [1000, 1000]\n\t#view.CameraViewAngle = 90\n\t#Render()\n");

    for(ii=0;ii<Nwires;ii++){
      fprintf(pVtk,"\tdp%i = GetDisplayProperties(reader%i)\n",ii,ii);
      fprintf(pVtk,"\tdp%i.Representation = 'Surface'\n",ii);
      fprintf(pVtk,"\telev%i = Elevation(reader%i)\n",ii,ii);
      fprintf(pVtk,"\tdp%i.LookupTable = MakeBlueToRedLT(minv, maxv)\n",ii);
      fprintf(pVtk,"\tdp%i.ColorArrayName = 'UU'\n",ii);
    }
    fprintf(pVtk,"\t#Render()\n");
    fprintf(pVtk,"\tWriteImage(baseName+str(freq*num)+\".png\")\n");
    for(ii=0;ii<Nwires;ii++){
      fprintf(pVtk,"\tDelete(reader%i)\n\tdel reader%i\n",ii,ii);
    }
    fprintf(pVtk,"\tdel view");
    fclose(pVtk);
  }
  fclose(pmain);
}

void printVtkViewWires(){
  int ii,kk,numEdge;
  char filename[256];
  int *pW;
  FILE *pmain=fopen("viewWires.py", "w");
  FILE *pbuildActors=fopen("buildActorWires.py", "w");
  FILE *paddActors=fopen("addActorWires.py", "w");
  fprintf(pmain,"import vtk\n");
  fprintf(pmain,"execfile(\"buildActorWires.py\")\n");
  
  fprintf(pbuildActors,"points = vtk.vtkPoints()\n");
  fprintf(pbuildActors,"points.SetNumberOfPoints(%i)\n",NPoints);
  fprintf(pmain,"ren = vtk.vtkRenderer()\n");
  fprintf(pmain,"execfile(\"addActorWires.py\")\n");
  fprintf(pbuildActors,"RLine=1\nGLine=1\nBLine=1\nwidthLine=2.5\n");
  fprintf(pbuildActors,"\n");
  for(ii=0;ii<NPoints;ii++){
    fprintf(pbuildActors,"points.SetPoint(%i, %e, %e, 0.1)\n",ii,Px[ii],Py[ii]);
  }
  fprintf(pbuildActors,"\n");
  
  pW=WIRES;
  for(ii=0;ii<Nwires;ii++){
    fprintf(pbuildActors,"lines%i = vtk.vtkCellArray()\n",ii);
    fprintf(pbuildActors,"lines%i.InsertNextCell(%i)\n",ii,Wsizes[ii]+1);
    numEdge=abs(*pW);
    if (*pW>=0)
      fprintf(pbuildActors,"lines%i.InsertCellPoint(%i);\n",ii,EDGES[2*(numEdge)]);
    else
      fprintf(pbuildActors,"lines%i.InsertCellPoint(%i);\n",ii,EDGES[2*(numEdge)+1]);
      
    for(kk=0;kk<Wsizes[ii];kk++){
      numEdge=abs(*pW);
      if (*pW>=0){
        fprintf(pbuildActors,"lines%i.InsertCellPoint(%i);\n",ii,EDGES[2*(numEdge)+1]);
      }else
        fprintf(pbuildActors,"lines%i.InsertCellPoint(%i);\n",ii,EDGES[2*(numEdge)]);
      pW++;
    }
    fprintf(pbuildActors,"\n");
    fprintf(pbuildActors,"polygon%i = vtk.vtkPolyData()\n",ii);
    fprintf(pbuildActors,"polygon%i.SetPoints(points)\n",ii);
    fprintf(pbuildActors,"polygon%i.SetLines(lines%i)\n",ii,ii);
    fprintf(pbuildActors,"\n");

    fprintf(pbuildActors,"\n");
    
    fprintf(pbuildActors,"polygonMapper%i = vtk.vtkPolyDataMapper()\n",ii);
    fprintf(pbuildActors,"if vtk.VTK_MAJOR_VERSION <= 5:\n");
    fprintf(pbuildActors,"\tpolygonMapper%i.SetInputConnection(polygon%i.GetProducerPort())\n",ii,ii);
    fprintf(pbuildActors,"else:\n");
    fprintf(pbuildActors,"\tpolygonMapper%i.SetInputData(polygon%i)\n",ii,ii);
    fprintf(pbuildActors,"\tpolygonMapper%i.Update()\n",ii);
    fprintf(pbuildActors,"polygonActor%i = vtk.vtkActor()\n",ii);
    fprintf(pbuildActors,"polygonActor%i.SetMapper(polygonMapper%i)\n",ii,ii);

    fprintf(paddActors,"ren.AddActor(polygonActor%i)\n",ii);
    fprintf(pbuildActors,"polygonActor%i.GetProperty().SetColor(RLine,GLine,BLine)\n",ii);
    fprintf(pbuildActors,"polygonActor%i.GetProperty().SetLineWidth(widthLine)\n",ii);
    fprintf(pbuildActors,"\n");
    fprintf(pmain,"\n");
  }
  fprintf(pmain,"ren.SetBackground(0.1, 0.2, 0.4)\n");
  fprintf(pmain,"ren.ResetCamera()\n");
  fprintf(pmain,"renWin = vtk.vtkRenderWindow()\n");
  fprintf(pmain,"renWin.AddRenderer(ren)\n");
  fprintf(pmain,"renWin.SetSize(500, 500)\n");
  fprintf(pmain,"iren = vtk.vtkRenderWindowInteractor()\niren.SetRenderWindow(renWin)\niren.Initialize()\niren.Start()\n");
  fprintf(pbuildActors,"\n");
  fprintf(pmain,"\n");
  fclose(pmain);
  fclose(pbuildActors);
  fclose(paddActors);
}

void randomizePoint(double amp){
  int ii;
  for (ii=0;ii<NPoints;ii++){
    Px[ii]+=(amp*rand())/RAND_MAX;
    Py[ii]+=(amp*rand())/RAND_MAX;
  }
    
}
int main(){
  //randomizePoint(1.5);
  initMatStruct();
  //printViewVtk();
  //printVtkViewWires();
//  return 1;
  FILE *pGeom = fopen("defGeomGen.edp", "w");
  printPoints(pGeom);
  printEdges(pGeom);
  printMeshes(pGeom);
  fclose(pGeom);
  FILE *pDofs = fopen("defDofsGen.edp", "w");
  printDofs(pDofs);
  fclose(pDofs);

  FILE *pVarfs = fopen("defVarfGen.edp", "w");
  printVarfs(pVarfs);
  fclose(pVarfs);
  
  FILE *pMats = fopen("defMatGen.edp", "w");
  printMat(pMats);
  fclose(pMats);
  
  FILE *pTimeStep = fopen("timeSteppingGen.edp", "w");
  printTS(pTimeStep);
  fclose(pTimeStep);
  
  
}
