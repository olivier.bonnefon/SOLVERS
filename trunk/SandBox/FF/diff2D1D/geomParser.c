#include "geomParser.h"
#include <stdio.h>
#include "geomStruct.h"
#include "systemDescr.h"
#include <stdlib.h>
int * FieldTypes=0;
//int * EdgeTypes=0;

//The points:
int NPoints=0;
double *PointsX=NULL;
double *PointsY=NULL;
double scalCoord=1.0;
double scalXCoord=1.0;
double scalYCoord=1.0;
double Xmin=1e30;
double Xmax=-1e30;
double Ymin=1e30;
double Ymax=-1e30;

//The nodes
int NNodes=0;
int *Nodes=NULL;
//The edges
int *NodeEdges=NULL;
int *PopTypeEdges=NULL;
int *NumberOfPointsInEdge=NULL;
//int *lcouple1D=NULL;
//The domains
int* pEdgeIsReversedInWire=NULL;
int  BUILD_MESH;
int NFilesCopy;
char FILES_COPY[200][256];//max copy 200 files
char GEODIR[512];
//0 do nothing
//1 x <--> y
int symGeom=0;
FILE * openPointsFile(){
  char filename[256];
  sprintf(filename,"%s//%s",GEODIR,"Points.txt");
  FILE * res=fopen(filename,"r");
  if (!res)
    printf("openPointsFile failed with %s\n",filename);
  return res;
}

FILE * openEdgesFile(){
  char filename[256];
  sprintf(filename,"%s//%s",GEODIR,"Edges.txt");
  FILE * res=fopen(filename,"r");
  if (!res)
    printf("openPointsFile failed with %s\n",filename);
  return res;
}

FILE * openWiresFile(){
  char filename[256];
  sprintf(filename,"%s//%s",GEODIR,"Wires.txt");
  FILE * res=fopen(filename,"r");
  if (!res)
    printf("openPointsFile failed with %s\n",filename);
  return res;
}


void readPoints(){
  int c,id;
  FILE *pointsFile;
  char *line=NULL;
  double *px,*py,aux;
  ssize_t read;
  size_t len = 0;
  pointsFile = openPointsFile();
  while ((read = getline(&line, &len, pointsFile)) != -1) 
    NPoints++;
  printf("there are %i points\n",NPoints);
  PointsX=(double *)malloc(NPoints*sizeof(double));
  PointsY=(double *)malloc(NPoints*sizeof(double));
  fclose(pointsFile);
  pointsFile = openPointsFile();
  px=PointsX;py=PointsY;
  while ((read = getline(&line, &len, pointsFile)) != -1) {
    printf("%s", line);
    sscanf(line,"%i%le%le",&id,px,py);
    if (symGeom){
      aux=(*py)*scalCoord*scalXCoord;
      *py=(*px)*scalCoord*scalYCoord;
      *px=aux;
    }else{
      *px=(*px)*scalCoord*scalXCoord;
      *py=(*py)*scalCoord*scalYCoord;
    }
    printf("points %e %e\n",*px,*py);
    if (*px<Xmin)
      Xmin=*px;
    if (*px>Xmax)
      Xmax=*px;
    if (*py<Ymin)
      Ymin=*py;
    if (*py>Ymax)
      Ymax=*py;
    px++;py++;
  }
  free(line);
  fclose(pointsFile);
}
int *NodesNumber=NULL;
void printNodes(int n){
  int ii;
  printf("There are %i nodes:\n",n);
  for (ii=0;ii<n;ii++){
    printf("%i\n",NodesNumber[ii]);
  }
}
void printBB(){
  printf("The coordinate Bounding box is:\n");
  printf("-> x in %e \t %e\n",Xmin,Xmax);
  printf("-> y in %e \t %e\n",Ymin,Ymax);
  
}
void readEdges(){
  int c,id,nbeg,nend,ii,cmp,nPt;
  FILE *edgesFile;
  char *line=NULL;
  double *px,*py;
  ssize_t read;
  size_t len = 0;
  NodesNumber=(int*)malloc(NPoints*sizeof(int));
  for (ii=0;ii<NPoints;ii++)
    NodesNumber[ii]=-1;
  edgesFile = openEdgesFile();
  while ((read = getline(&line, &len, edgesFile)) != -1) {
    NEdges++;
    sscanf(line,"%i%i%i",&id,&nbeg,&nend);
    NodesNumber[nbeg-1]=1;
    NodesNumber[nend-1]=1;
  }
  fclose(edgesFile);
  printf("there are %i edges\n",NEdges);
//  lcouple1D=(int*)malloc(NEdges*sizeof(int));
  NodeEdges=(int*)malloc(2*NEdges*sizeof(int));
  PopTypeEdges=(int*)malloc(NEdges*sizeof(int));
  for (ii=0;ii<NPoints;ii++){
    if (NodesNumber[ii]>0){
      NodesNumber[ii]=NNodes;
      NNodes++;
    }
  }
  printNodes(NNodes);
  //now, if ii is a point that is also a node, NodeNumbers[ii] is the indice of the node
  printf("there are %i nodes\n",NNodes);
  Nodes=(int*)malloc(NNodes*sizeof(int));
  cmp=0;
  for (ii=0;ii<NPoints;ii++){
    if (NodesNumber[ii]>=0){
      Nodes[cmp]=ii;
      cmp++;
    }
  }
  NumberOfPointsInEdge=(int*)malloc(NEdges*sizeof(int));
  edgesFile = openEdgesFile();
  cmp=0;
  while ((read = getline(&line, &len, edgesFile)) != -1) {
    sscanf(line,"%i%i%i%i",&id,&nbeg,&nend,&nPt);
    NumberOfPointsInEdge[cmp]=nPt;
    if (id<NDynEdgeType)
      PopTypeEdges[cmp]=id;
    else
      PopTypeEdges[cmp]=0;
    cmp++;
  }
 
  for (ii=0;ii<NNodes;ii++)
    printf("%i is a node\n",Nodes[ii]);

  free(line);
  fclose(edgesFile);

}




void readPointsInEdges(){
  char label[512];
  FILE *edgesFile = openEdgesFile();
  int numPt;
  char *line=NULL;
  char *lineAux=NULL;
  size_t len = 0;
  ssize_t read;
  int c,id,nbeg,nend,ii,cmp=0,nPt;
  int offset;
  while ((read = getline(&line, &len, edgesFile)) != -1) {
    lineAux=line;
    sscanf(line,"%i%i%i%i%n",&id,&nbeg,&nend,&nPt, &offset);

//    lcouple1D[cmp]=id;
    printf("%i %i\n",nbeg,nend);
    NodeEdges[2*cmp]=NodesNumber[nbeg-1];
    NodeEdges[2*cmp+1]=NodesNumber[nend-1];
    line+=offset;
    EDGES[cmp].geom.indicesPoints[0]=nbeg-1;
    EDGES[cmp].geom.indicesPoints[nPt+1]=nend-1;
    printf("readPointsInEdges-> edge %i reading %i points.\n",cmp,nPt);
    for (ii=0;ii<nPt;ii++){
      sscanf(line,"%i%n",&numPt, &offset);
      line+=offset;
      printf("numPt=%i\n",numPt-1);
      EDGES[cmp].geom.indicesPoints[ii+1]=numPt-1;
    }
    sscanf(line,"%s%i",label,&(EDGES[cmp].geom.idInGeomFile));
    cmp++;
    line=lineAux;
  }
  free(line);
  fclose(edgesFile);
}

int readWires(){
  int numW,nEdges,ii,cmp=0,nTotalEdges=0;
  char *line=NULL;
  char *lineAux=NULL;
  size_t len = 0;
  ssize_t read;
  int offset;
  struct wireSide* pAux;
   //Get the number of domains
   FILE *wiresFile = openWiresFile();
   while ((read = getline(&line, &len, wiresFile)) != -1) {
     Nwires++;
     sscanf(line,"%i%i%n",&numW,&nEdges, &offset);
     nTotalEdges+=nEdges;
   }
   fclose(wiresFile);
   printf("There are %i wires\n",Nwires);
   Wsizes=(int*)malloc(Nwires*sizeof(int));
   WIRES=(struct wireSide*)malloc(nTotalEdges*sizeof(struct wireSide));
   //fill Wsizes
   wiresFile = openWiresFile();
   while ((read = getline(&line, &len, wiresFile)) != -1) {
     sscanf(line,"%i%i%n",&numW,&nEdges, &offset);
     Wsizes[cmp]=nEdges;
     cmp++;
   }
   fclose(wiresFile);
   //fil edges number
   pAux=WIRES;
   wiresFile = openWiresFile();
   cmp=0;
   while ((read = getline(&line, &len, wiresFile)) != -1) {
     lineAux=line;
     sscanf(line,"%i%i%n",&numW,&nEdges, &offset);
     //printf("cmp=%i\n",cmp);
     line+=offset;
     for (ii=0;ii<nEdges;ii++){
       int numE;
       sscanf(line,"%i%n",&numE,&offset);
       pAux->indexEdge=abs(numE)-1;
       pAux->isReversed=0;
       if (numE<0)
         pAux->isReversed=1;
       
       line+=offset;
       pAux++;
     }
     line=lineAux;
   }
   fclose(wiresFile);
   return nTotalEdges;
}


