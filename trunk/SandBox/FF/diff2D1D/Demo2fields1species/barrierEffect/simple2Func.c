#include "../../gen2speciesNL.h"
//#define WITH_GRADIENT_P
void initStructure(){
  int ii;
  NbSpecies=1;
  NDynFieldType=1;
  NDynEdgeType=2;//car 2 EF interaction
  nPerLength=10;
  scalXCoord=5;
  nEFinter=2;
  nEEinter=2;
  nParams=2;
  LinearSolver_2D1D=GMRES_2D1D;
  double D1d=10;
  double D2d=1e-6;
  reverseEdges=1;
  nParams=2;
  specNames[0]='A';
 
  sprintf(GEODIR,"%s/GEODATA/SIMPLE2",SRCDIR);
  readPoints();
  sprintf(OUTPUTDIR,"%s/../BARRIEREFFECT/",SRCDIR);
  //sprintf(POSTPRODIR,"/home/olivierb/solvers/trunk/SandBox/FF/Vacher/");
  NFilesCopy=11;
  sprintf(FILES_COPY[0],"%s/EDPSRC/myMacro.edp",SRCDIR);
  sprintf(FILES_COPY[1],"%s/EDPSRC/PREF50/freefem++.pref",SRCDIR);
  sprintf(FILES_COPY[2],"%s/EDPSRC/postTraitementLoad.edp",SRCDIR);
  sprintf(FILES_COPY[3],"%s/EDPSRC/defTargetLocs.edp",SRCDIR);
  sprintf(FILES_COPY[4],"%s/EDPSRC/defTargetDates.edp",SRCDIR);
  sprintf(FILES_COPY[5],"%s/EDPSRC/preTraitement.edp",SRCDIR);
  sprintf(FILES_COPY[6],"%s/EDPSRC/beforeOneStep.edp",SRCDIR);
  sprintf(FILES_COPY[7],"%s/Demo2fields1species/userFDef0.txt",SRCDIR);
  sprintf(FILES_COPY[8],"%s/Demo2fields1species/*.edp",SRCDIR);
  sprintf(FILES_COPY[9],"%s/EDPSRC/postTraitementEnd.edp",SRCDIR);
  sprintf(FILES_COPY[10],"%s/Demo2fields1species/barrierEffect/*.edp",SRCDIR);
 
  readEdges();
  buildEdges();
  readPointsInEdges();
  for (ii=0;ii<NEdges;ii++)
    printEdge(EDGES+ii);
  readWires();
  printWires();
 
  allocStructs();
 
  //A
  aFieldPop[0].D2DX[0]=D2d;
  aFieldPop[0].D2DY[0]=D2d;

  aEdgePop[1].indexEFInter=1;
  aEdgeFieldInter[1].nu[0]=0.001;
  aEdgeFieldInter[1].mu[0]=0.001;
  
  aEdgePop[1].indexEEInter=1;
  aEdgeEdgeInter[1].gamma[0]=1;
  
  aEdgePop[0].indexEFInter=0;
  aEdgeFieldInter[0].nu[0]=0;
  aEdgeFieldInter[0].mu[0]=0;
  printFieldPops(aFieldPop,Nwires);
  
}
