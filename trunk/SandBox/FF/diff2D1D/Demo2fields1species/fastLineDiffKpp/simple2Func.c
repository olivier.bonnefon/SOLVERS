#include "../../gen2speciesNL.h"
//#define WITH_GRADIENT_P
void initStructure(){
  int ii;
  NbSpecies=1;
  nEFinter=2;
  nEEinter=1;
  nParams=2;
  NDynFieldType=1;
  NDynEdgeType=2;
  nPerLength=5;
  scalYCoord=20;
  scalXCoord=10;

  LinearSolver_2D1D=GMRES_2D1D;
  reverseEdges=1;
 
  specNames[0]='A';
 
  sprintf(GEODIR,"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/GEODATA/SIMPLE2");
  readPoints();
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/FASTLINEDIFFKPP/");
  //sprintf(POSTPRODIR,"/home/olivierb/solvers/trunk/SandBox/FF/Vacher/");
  NFilesCopy=11;
  sprintf(FILES_COPY[0],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/EDPSRC/myMacro.edp");
  sprintf(FILES_COPY[1],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/EDPSRC/PREF50/freefem++.pref");
  sprintf(FILES_COPY[2],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/EDPSRC/postTraitementLoad.edp");
  sprintf(FILES_COPY[3],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/EDPSRC/defTargetLocs.edp");
  sprintf(FILES_COPY[4],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/EDPSRC/defTargetDates.edp");
  sprintf(FILES_COPY[5],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/EDPSRC/preTraitement.edp");
  sprintf(FILES_COPY[6],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/EDPSRC/beforeOneStep.edp");
  sprintf(FILES_COPY[7],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/Demo2fields1species/userFDef0.txt");
  sprintf(FILES_COPY[8],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/Demo2fields1species/*.edp");
  sprintf(FILES_COPY[9],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/EDPSRC/postTraitementEnd.edp");
  sprintf(FILES_COPY[10],"/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/Demo2fields1species/fastLineDiffKpp/*.edp");
  
  readEdges();
  buildEdges();
  readPointsInEdges();
  for (ii=0;ii<NEdges;ii++)
    printEdge(EDGES+ii);
  readWires();
  printWires();
 
  allocStructs();
 
  //A
  aFieldPop[0].D2DX[0]=5;
  aFieldPop[0].D2DY[0]=5;
 
  aEdgePop[1].indexEFInter=1;
  aEdgeFieldInter[0].mu[0]=0;
  aEdgeFieldInter[0].nu[0]=0;
  aEdgeFieldInter[1].mu[0]=1;
  aEdgeFieldInter[1].nu[0]=1;
 
 
 
  printFieldPops(aFieldPop,Nwires);
 
}




