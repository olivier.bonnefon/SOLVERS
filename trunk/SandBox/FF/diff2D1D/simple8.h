#include "systemDescr.h"
int NbSpecies=2;
int NDynFieldType=2;
int NDynEdgeType=2;
int * FieldTypes=0;
int * EdgeTypes=0;
int NPoints=21;

double Px[]={0 ,5,5,0  ,0,-5,-5,-5,5,12,9,8,5,1,-5,8,0,-9,-7,-8,-6.5};
double Py[]={0 ,0 ,5 ,7.5,5,5,0,-5,-7.5,4,0,-3.5,9,10,9,13,13.5,15,5,-6,-0.5};
//double Px[]={10., 0,  10, 0.,10};
//double Py[]={10., 10, 0,  0.,5};
int nPerLength=5;
int nEFinter=2;
int nEEinter=1;

int NEdges=33;
int EDGES[]={
             0,1,
             1,2,
             2,3,
             3,4,
             4,0,
             4,5,
             5,6,
             6,0,
             6,7,
             7,8,
             8,1,
             1,0,
             1,10,
             10,9,
             9,2,
             8,11,
             11,10,
             2,12,
             12,13,
             13,14,
             14,5,
             9,15,
             15,16,
             16,13,
             16,14,
             16,17,
             17,18,
             18,20,
             19,7,
             7,6,
             6,5,
             5,14,
             20,19
             };


int Nwires=9;//one wire per domain
int WiresType[]={2,2,2,2,2,2,2,2,2};
int Wsizes[]={5,4,5,4,4,7,6,3,9};
int WIRES[]={0,1,2,3,4,
             5,6,7,-4,
             -7,8,9,10,11,
             12,13,14,-1,
             15,16,-12,-10,
             17,18,19,20,-5,-3,-2,
             21,22,23,-18,-17,-14,
             24,-19,-23,
             25,26,27,32,28,29,30,31,-24};
//int ABSWIRES[]={0,1,2,3};

//  -1,4,5,6,7};
int whithSource=1;
int whithControl=1;
void initStructure(){
  int ii;
  int wb=0;
  double coef=1.2;
  double scal=0.2;
  for (ii=0;ii<NPoints;ii++){
    Px[ii]=scal* Px[ii];
    Py[ii]=scal* Py[ii];
  }

  allocStructs();
  
  sprintf(OUTPUTDIR,"/home/obonnefon/solvers1/trunk/SandBox/FF/diff2D1D/RESULT10Conv/");
  
  /*stationary state :
    u2dtm10=5;
    v2dtm10=(5-0.05)*(1-0.25);
  */
  aFieldPop[0].IC[0]=5;
  aFieldPop[0].D2DX[0]=1;
  aFieldPop[0].D2DY[0]=1;
  aFieldPop[0].K[0]=20;
  aFieldPop[0].R[0]=0;
  aFieldPop[0].T[0]=1;
  aFieldPop[0].rho[0]=0.05;
  
  aFieldPop[0].Ccoup[0]=-1*coef;
  aFieldPop[0].Cmalthus[0]=0;
  aFieldPop[0].source[0]=0.00;
  
  aFieldPop[0].IC[1]=(5-0.05)*(1-0.25);
  aFieldPop[0].D2DX[1]=1;
  aFieldPop[0].D2DY[1]=1;
  aFieldPop[0].K[1]=15;
  aFieldPop[0].R[1]=0;
  aFieldPop[0].Ccoup[1]=1*coef;
  aFieldPop[0].Cmalthus[1]=-5;
  aFieldPop[0].source[1]=0.0;
  
  aEdgeEdgeInter[0].alpha[0]=15.0;
  aEdgeEdgeInter[0].alpha[1]=0;

 
  for (ii=0;ii<nEFinter;ii++){
    //2D to 1D
    aEdgeFieldInter[ii].mu[0]=15.0;
    aEdgeFieldInter[ii].mu[1]=wb?1.0:0;
    //1D to 2D
    aEdgeFieldInter[ii].nu[0]=3.75;
    aEdgeFieldInter[ii].nu[1]=wb?0.25:0;
  }

  aEdgePop[0].source[1]=wb?5:0;
  aEdgePop[0].Cmalthus[1]=wb?-5:0;
  aEdgePop[0].source[0]=0;
  aEdgePop[0].Cmalthus[0]=0;
  //aEdgePop[0].pEFInter=&aEdgeFieldInter[1];
  // aEdgePop[0].source[1]=s;
  // aEdgePop[0].Cmalthus[1]=m;
  // aEdgePop[1].source[1]=s;
  // aEdgePop[1].Cmalthus[1]=m;

  printFieldPops(aFieldPop,Nwires);
}
