#include "systemDescr.h"
int NPoints=12;
double Px[]={5-5 ,10-5,10-5,5-5  ,5-5,0-5,0-5,0-5,10-5,17-5,14-5,8};
double Py[]={0 ,0 ,5 ,7.5,5,5,0,-5,-7.5,4,0,-3.5};
//double Px[]={10., 0,  10, 0.,10};
//double Py[]={10., 10, 0,  0.,5};

int NEdges=17;
int EDGES[]={
             0,1,
             1,2,
             2,3,
             3,4,
             4,0,
             4,5,
             5,6,
             6,0,
             6,7,
             7,8,
             8,1,
             1,0,
             1,10,
             10,9,
             9,2,
             8,11,
             11,10
             };


int Nwires=5;//one wire per domain
int Wsizes[]={5,4,5,4,4};
int WIRES[]={0,1,2,3,4,
             5,6,7,-4,
             -7,8,9,10,11,
             12,13,14,-1,
             15,16,-12,-10};
//int ABSWIRES[]={0,1,2,3};

//  -1,4,5,6,7};

int nPerLength=2;
int NbSpecies=2;

struct modelPop aModelPop[5];


void initStructure(){
  aModelPop[0].type=DIFF;
  aModelPop[0].aModel.mDiff.D2DX=0.5;
  aModelPop[0].aModel.mDiff.D2DY=0.5;
  
  aModelPop[1].type=DIFF;
  aModelPop[1].aModel.mDiff.D2DX=0.5;
  aModelPop[1].aModel.mDiff.D2DY=0.5;
  
  aModelPop[2].type=DIFF;
  aModelPop[2].aModel.mDiff.D2DX=0.5;
  aModelPop[2].aModel.mDiff.D2DY=0.5;
  aModelPop[2].type=DIFF;
  
  aModelPop[3].aModel.mDiff.D2DX=0.5;
  aModelPop[3].aModel.mDiff.D2DY=0.5;
  aModelPop[3].type=DIFF;
  
  aModelPop[3].type=KPP;
  aModelPop[3].aModel.mDiff.D2DX=0.5;
  aModelPop[3].aModel.mDiff.D2DY=0.5;
  aModelPop[3].aModel.mKpp.R[0]=10;
  aModelPop[3].aModel.mKpp.K[0]=1;
  aModelPop[3].aModel.mKpp.R[1]=5;
  aModelPop[3].aModel.mKpp.K[1]=1;
  
  aModelPop[4].type=KPP;
  aModelPop[4].aModel.mDiff.D2DX=0.5;
  aModelPop[4].aModel.mDiff.D2DY=0.5;
  aModelPop[4].aModel.mKpp.R[0]=10;
  aModelPop[4].aModel.mKpp.K[0]=1;
  aModelPop[4].aModel.mKpp.R[1]=1;
  aModelPop[4].aModel.mKpp.K[1]=1;
}
