
#include "systemDescr.h"
int NbSpecies=1;
int NDynFieldType=1;
int NDynEdgeType=1;
int * FieldTypes=0;


//The points:
int NPoints=4;
double PointsX[]={0,1,1,0};
double PointsY[]={0 ,0,1,1};
//The nodes
int NNodes=4;
int Nodes[]={0,1,2,3};
//The edges
int NEdges=4;
int NodeEdges[]={0,1,
                1,2,
                2,3,
                3,0};
int NumberOfPointsInEdge[]={0,0,0,0};
//all edges are connected
int *lcouple1D=(int[]){1,1,1,1};
//The domains
int Nwires=1;//one wire per domain
int *Wsizes=(int[]){4};
int *WIRES=(int[]){0,1,2,3};

double nPerLength=5;
int nEFinter=1;
int nEEinter=0;
int whithSource=0;
int whithControl=0;

#include "edgeTools.h"

void initStructure(){
  int ii;
  int wb=1;
  double s=40;
  double m=-20;
  double coef=1;
  double scal=1;
  buildEdges();
  for (ii=0;ii<NEdges;ii++){
    EDGES[ii].couple1D=lcouple1D[ii];
  }
  // add 2 points in edge 0
  

  allocStructs();
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/RES_DUMMY/");


  /*stationary state :
    u2dtm10=5;
    v2dtm10=(5-0.05)*(1-0.25);
  */
  aFieldPop[0].D2DX[0]=1;
  aFieldPop[0].D2DY[0]=1;
  aFieldPop[0].K[0]=1;
  aFieldPop[0].R[0]=0;
  aFieldPop[0].T[0]=0;
  aFieldPop[0].rho[0]=0.05;
  
  aFieldPop[0].Ccoup[0]=-1*coef;
  aFieldPop[0].Cmalthus[0]=0;
  aFieldPop[0].source[0]=0.00;

//  aEdgeEdgeInter[0].alpha[0]=0.0;
//  aEdgeEdgeInter[0].alpha[1]=wb?1.0:0;

 
  for (ii=0;ii<nEFinter;ii++){
    //2D to 1D
    aEdgeFieldInter[ii].mu[0]=1.0;
//    aEdgeFieldInter[ii].mu[1]=wb?1.0:0;
    //1D to 2D
    aEdgeFieldInter[ii].nu[0]=1.0;
//    aEdgeFieldInter[ii].nu[1]=wb?0.25:0;
  }
  aEdgePop[0].D[0]=1;

  printFieldPops(aFieldPop,Nwires);
}



