#ifndef TIMESTEPPING_H
#define TIMESTEPPING_H
extern int isLinear;
extern double tm;
extern double dt;
extern double T0;
extern double Tf;
extern int maxMult;
extern double adaptiveRelErr;
extern double newtonCriteron;
extern double newtonMaxResidu;
extern int newtonMinIt;
extern int newtonMaxIt;
extern int forcePositiveSol;

void buildTS();

#endif
