
#include "systemDescr.h"
int NbSpecies=1;
int NDynFieldType=1;
int NDynEdgeType=1;
int * FieldTypes=0;


//The points:
int NPoints=6;
double PointsX[]={0,1,1,0,2,2};
double PointsY[]={0 ,0,1,1,0,1};
//The nodes
int NNodes=6;
int Nodes[]={0,1,2,3,4,5};
//The edges
int NEdges=7;
int NodeEdges[]={0,1,
                 1,2,
                 2,3,
                 3,0,
                 1,4,
                 4,5,
                 5,2};
int NumberOfPointsInEdge[]={0,0,0,0,0,0,0};
//all edges are connected
int *lcouple1D=(int[]){1,1,1,1,1,1,1};
//The domains
int Nwires=2;//one wire per domain
int *Wsizes=(int[]){4,4};
int *WIRES=(int[]){0,1,2,3,
                   4,5,6,-1};

double nPerLength=10;
int nEFinter=1;
int nEEinter=1;
int whithSource=0;
int whithControl=0;

#include "edgeTools.h"

void initStructure(){
  int ii;
  int wb=1;
  double s=40;
  double m=-20;
  double coef=1;
  double scal=1;
  buildEdges();
  for (ii=0;ii<NEdges;ii++){
    EDGES[ii].couple1D=lcouple1D[ii];
  }
  // add 2 points in edge 0
  

  allocStructs();
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/RES_DUMMY2/");


  /*stationary state :
    u2dtm10=5;
    v2dtm10=(5-0.05)*(1-0.25);
  */
  aFieldPop[0].D2DX[0]=1;
  aFieldPop[0].D2DY[0]=1;
  aFieldPop[0].K[0]=1;
  aFieldPop[0].R[0]=0;
  aFieldPop[0].T[0]=0;
  aFieldPop[0].rho[0]=0.05;
  
  aFieldPop[0].Ccoup[0]=-1*coef;
  aFieldPop[0].Cmalthus[0]=0;
  aFieldPop[0].source[0]=0.00;

//  aEdgeEdgeInter[0].alpha[0]=0.0;
//  aEdgeEdgeInter[0].alpha[1]=wb?1.0:0;
  aEdgeEdgeInter[0].alpha[0]=5;
 
  for (ii=0;ii<nEFinter;ii++){
    //2D to 1D
    aEdgeFieldInter[ii].mu[0]=1.0;
//    aEdgeFieldInter[ii].mu[1]=wb?1.0:0;
    //1D to 2D
    aEdgeFieldInter[ii].nu[0]=1.0;
//    aEdgeFieldInter[ii].nu[1]=wb?0.25:0;
  }
  aEdgePop[0].D[0]=1;

  printFieldPops(aFieldPop,Nwires);
}

/*

//begin 1Dx1D domain couple
varf matC1s1(u,v)=int1d(Th1s1,1)((-1*(5.000000e-01))*u*v);
matrix UC1DE1s1onE0s1;
buildM2(matC1s1,UC1DE1s1onE0s1,zoi1s1bis,Vh1s1);
//matrix UC1DE1s1onE0s1=-1*(5.000000e-01)*MASSE1D1s1;

varf matC0s1(u,v)=int1d(Th0s1,1)((-1*(5.000000e-01))*u*v);
matrix UC1DE0s1onE1s1;
buildM2(matC0s1,UC1DE0s1onE1s1,zoi0s1bis,Vh1s1);
//matrix UC1DE0s1onE1s1=-1*(5.000000e-01)*MASSE1D0s1;
//end 1Dx1D domain couple

*/
