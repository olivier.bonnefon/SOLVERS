from paraview.simple import *

baseName="/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/u"

for num in range(0,200):
    fileName0=baseName+str(0)+str(10*num)+".vtk"
    reader0 = OpenDataFile(fileName0)
    Show()
    view = GetActiveView()
    view.ViewSize = [1000, 1000]
    view.CameraViewAngle = 90
    #view.CameraFocalPoint = [25, 50, 0]
    #view.CameraPosition = [0,0,100]
    Render()
    dp0 = GetDisplayProperties(reader0)
    dp0.Representation = 'Surface'
    elev0 = Elevation(reader0)
    dp0.LookupTable = MakeBlueToRedLT(0.0000, 0.5)
    dp0.ColorArrayName = 'UU'
    Render()
    WriteImage(baseName+str(num)+".png")
    Delete(reader0)
