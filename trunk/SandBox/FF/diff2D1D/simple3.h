#define WITH_GRADIENT
int NbSpecies=1;
int NDynFieldType=1;
int NDynEdgeType=2;
double nPerLength=300/1e6;
int nEFinter=2;
int nEEinter=1;
int whithSource=0;
int whithControl=0;
int NFilesCopy=0;
char FILES_COPY[200][128];
int nParams=0;
int BUILD_MESH=1;
void initStructure(){
    int ii;
  double coef=1.0;
  double scal=0.2;
  int wb=1;
  specNames[0]='X';
  reverseEdges=-1;
  //sprintf(GEODIR,"PAYSAGE20");
  sprintf(GEODIR,"FRA_DEP");
  readPoints();
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/FRA_DEP_BIS/");
  // sprintf(POSTPRODIR,"/home/olivierb/solvers/trunk/SandBox/FF/PAYSAGE20/");
  // sprintf(FILES_COPY[0],"postTraitement.edp");
  // sprintf(FILES_COPY[1],"postTraitement.edp");
  // sprintf(FILES_COPY[2],"postTraitementEnd.edp");
  // sprintf(FILES_COPY[3],"postTraitementLoad.edp");
  // sprintf(FILES_COPY[4],"cas1.edp");
  // sprintf(FILES_COPY[5],"defPluginVaraibleGen.edp");
  // sprintf(FILES_COPY[6],"defPluginGen.edp");
  // sprintf(FILES_COPY[7],"defICGen.edp");
  
  readEdges();
  buildEdges();
  readPointsInEdges();
  for (ii=0;ii<NEdges;ii++)
     printEdge(EDGES+ii);
  readWires();
  printWires();
  
  //exit(0);
  /*double scal=1;
  for (ii=0;ii<NEdges;ii++){
    Nx[ii]=scal* Nx[ii];
    Ny[ii]=scal* Ny[ii];
    }*/

 
  allocStructs();


  /*Fieldtype 0*/



  
  printFieldPops(aFieldPop,Nwires);
  
}

/*

pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;




pu1d0s38=1;
pu1d0s37=0;
pu1d0s36=0;
pu1d0s42=0;
pu1d0s41=1;
pu1d0s40=0;
pu1d0s39=0;
pu1d1s20=0;
pu1d1s21=1;
pu1d1s22=0;
pu1d1s23=0;
pu1d1s24=0;
pu1d1s35=1;
pu1d1s37=0;
pu1d1s38=0;
pu1d1s19=1;
pu1d2s0=0;
pu1d2s1=0;
pu1d2s2=1;
pu1d2s3=0;
pu1d2s4=0;
pu1d2s5=1;
pu1d2s6=0;
pu1d2s7=0;
pu1d2s8=1;
pu1d2s9=0;
pu1d2s10=0;
pu1d2s11=0;
pu1d2s12=1;
pu1d2s13=0;
pu1d2s14=0;
pu1d2s15=0;
pu1d2s16=0;
pu1d2s17=1;
pu1d2s18=0;
pu1d2s39=0;
pu1d2s40=0;
pu1d2s41=0;
pu1d2s42=1;
pu1d2s36=0;
pu1d2s35=0;
pu1d2s25=0;
pu1d2s26=0;
pu1d2s27=0;
pu1d2s28=0;
pu1d2s29=0;
pu1d2s30=0;
pu1d2s31=0;
pu1d2s32=0;
pu1d2s33=0;
pu1d2s34=0;
pu1d0s38(0:n1Ddofs0s38/2)=1;
pu1d0s36(0:n1Ddofs0s36/2)=1;
pu1d0s41(0:n1Ddofs0s41/2)=1;
pu1d0s39(0:n1Ddofs0s39/2)=1;*/
