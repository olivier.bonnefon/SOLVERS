#include "../gen2speciesNL.h"

//#define GEOM_DESCR "paysage20_bis.h"
//#define GEOM_DESCR "testFuncSimple.h"
//#define GEOM_DESCR "testFunc.h"
//#define GEOM_DESCR "vacher.h"
//#define GEOM_DESCR "tessellation.h"
//#define GEOM_DESCR "simple3.h"
//#define GEOM_DESCR "tigerInter.h"
//#define GEOM_DESCR "tiger.h"
#define GEOM_DESCR "DemoSimple2Func/simple2Func.h"
#include GEOM_DESCR



int main(){
  int ii;
  initStructure();
  buildDirectories();

   //FILE * pcheckF;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"checkdUF.edp");
  FILE * pcheckF= fopen(FILENAME, "w");
  printCheckdUF(pcheckF);
  fclose(pcheckF);
#ifdef WITH_GRADIENT_P
  printsolToW();
  printTSOneStepW();
  printOptimDef();
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"checkdPF.edp");
  pcheckF= fopen(FILENAME, "w");
  printCheckdPF(pcheckF);
  fclose(pcheckF);
  printRecorder();
#endif
  printUserF();
  
  printDefTargetDate();
  printDefTargetLoc();

  //exit(0);
  //randomizePoint(1.5);
  initMatStruct();
//  printViewVtk();
  printVtkViewWires();
  printSaveMeshes();
  
  printParams();
   
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defOutputGen.edp");
  FILE *pOut = fopen(FILENAME, "w");
  buildDefOutput(pOut);
  fclose(pOut);

//  return 1;
  if (BUILD_MESH){
    sprintf(FILENAME,"%s%s",OUTPUTDIR,"defGeomGen.edp");
    FILE *pGeom = fopen(FILENAME, "w");
//  printPoints(pGeom);
    printEdges(pGeom);
    printMeshes(pGeom);
    fclose(pGeom);
  }

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofsGen.edp");
  FILE *pDofs = fopen(FILENAME, "w");
  printDofs(pDofs);
  fclose(pDofs);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofs2DGen.edp");
  pDofs = fopen(FILENAME, "w");
  printDofs2D(pDofs);
  fclose(pDofs);
  if (NDynEdgeType){
    sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofs1DGen.edp");
    pDofs = fopen(FILENAME, "w");
    printDofs1D(pDofs);
    fclose(pDofs);
  }

  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defVarfGen.edp");
  FILE *pVarfs = fopen(FILENAME, "w");
  printVarfs(pVarfs);
  fclose(pVarfs);

 

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defMatGen.edp");
  FILE *pMats = fopen(FILENAME, "w");
  printMat(pMats);
  fclose(pMats);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"buildMatGen.edp");
  FILE *pMatsBuild = fopen(FILENAME, "w");
  buildMat(pMatsBuild);
  fclose(pMatsBuild);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"timeSteppingGen.edp");
  FILE *pTimeStep = fopen(FILENAME, "w");
  printTS(pTimeStep);
  fclose(pTimeStep);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"timeSteppingOneStepGen.edp");
  FILE *pTimeStepOneStep = fopen(FILENAME, "w");
  printTSOneStep(pTimeStepOneStep);
  fclose(pTimeStepOneStep);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"solToDof.edp");
  FILE *pTimeStepSolToDof = fopen(FILENAME, "w");
  printsolToDof(pTimeStepSolToDof);
  fclose(pTimeStepSolToDof);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defPluginVaraibleGen.edp");
  FILE *pPlugV = fopen(FILENAME, "w");
  if (withControl)
    printPluginVariable(pPlugV);
  fclose(pPlugV);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defPluginGen.edp");
  FILE *pPlug = fopen(FILENAME, "w");
  if (withControl)
    printPlugin(pPlug);
  fclose(pPlug);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defICGen.edp");
  if(access( FILENAME, F_OK ) == -1){
    FILE *pIC = fopen(FILENAME, "w");
    printf("call IC\n");
    printInitialCond(pIC,1);
    fclose(pIC);
  }

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"massInfo.txt");
  FILE *pMassInfo = fopen(FILENAME,"w");
  printMassInfo(pMassInfo);
  fclose(pMassInfo);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"vtktopngGen.edp");
  FILE *pVtkLim = fopen(FILENAME,"w");
  printVTKGENPY(pVtkLim);
  fclose(pVtkLim);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"run.sh");
  FILE *pRunsh = fopen(FILENAME,"w");
  buildRunSGEFile(pRunsh);
  fclose(pRunsh);
  freeEdges();
  resetMatStruct();
  freeStructs();

#ifdef WITH_POST_PRO
  for (ii=0;ii<NFilesCopy;ii++){
    sprintf(FILENAME,"cp %s%s %s",POSTPRODIR,FILES_COPY[ii],OUTPUTDIR);
    printf("doing :%s\n",FILENAME);
    system(FILENAME);
  }
#endif

  
}
