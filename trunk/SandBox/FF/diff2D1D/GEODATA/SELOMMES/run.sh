#ce script permet de transformé les fichiers provenant de Lite en des fichiers interpretable par le 2D1D
cp LITE/Points.txt PointsLITE.txt
sed 1d PointsLITE.txt -i 
awk '{print $1+1" "$2" "$3" pt"$1+1}' PointsLITE.txt > aux.txt
cp aux.txt Points.txt

cp LITE/Edges.txt EdgesLITE.txt
sed 1d EdgesLITE.txt -i
awk '{print "0 " $2+1" "$3+1" 0  ed"$1+1" "$1+1}' EdgesLITE.txt > aux.txt
cp aux.txt Edges.txt

cp LITE/Wires.txt WiresLITE.txt
sed 1d WiresLITE.txt -i
sed -i -e "s/NA//g" WiresLITE.txt
sed -i -e "s/\t/ /g" WiresLITE.txt
awk '{print $1+1" "NF-1" "}' WiresLITE.txt >part1.txt
cat WiresLITE.txt | cut -d " " -f 2- > part2.txt

awk '{for (i=1; i<=NF; i++) { if ($i >=0) {printf  $i+1" "}else{printf  $i-1" "} } printf "\n"}' part2.txt >part3.txt


paste part1.txt part3.txt > aux.txt
awk '{print $0 " W"$1}' aux.txt > Wires.txt
sed -i -e "s/ [ ]*/ /g" Wires.txt
