#ce script permet de transformé les fichiers provenant de Lite en des fichiers interpretable par le 2D1D
cp Points.txt PointsSav.txt
sed 1d Points.txt -i 
awk '{print $2" "$3" "$4" pt"$2}' Points.txt > aux.txt
cp aux.txt Points.txt

cp Edges.txt EdgesSav.txt
sed 1d Edges.txt -i
awk '{print "0 " $3" "$4" 0  pt"$2" "$2}' Edges.txt > aux.txt
cp aux.txt Edges.txt

cp Wires.txt WiresSav.txt
sed 1d Wires.txt -i
sed -i -e "s/NA//g" Wires.txt
awk '{print $2" "NF-2" "}' Wires.txt >part1.txt
cat Wires.txt | cut -d " " -f 3- > part2.txt
paste part1.txt part2.txt > aux.txt
awk '{print $0 " W"$1}' aux.txt > Wires.txt
