convention des fichiers Points.txt, Edges.txt, Wires.txt.

les index de points des aretes commence à 1, il n'y a pas de points d'index 0.

Chaque ligne du fichier Edeges.txt correspond à une arete, la ligne type du fichier Edges.txt est:
Ntype Ibeg Iend Ninter [Iinter]*  label id

Où
- Ntype permet de typer la dynamique de l'élément et sera exploiter par l'utilisateur
- Ibeg index de points débutant l'arete. cela désigne le Ibeg^iéme point du fichier Points.txt
- Iend index de points terminant l'arete. cela désigne le Iend^iéme point du fichier Points.txt
- Ninter designe le nombre de points intermédière
- Iinter les index de points intermédiaires
- label une chaine de caracteres (non utilisée par le simulateur, utile pour la visu)
- id (non utilisée par le simulateur, utile pour la visu)


Une ligne du fichier Points.txt correspond à un point de géométrie, la ligne type du fichier Points.txt

id x y label

Où

- id (non utilisée par le simulateur, utile pour la visu)
- x et y coordonné du point
- label une chaine de caracteres (non utilisée par le simulateur, utile pour la visu)


Chaque ligne du fichier Wires.txt correspond à un contour de domaine, la ligne typre du fichier Wires.txt

numW Nedges [Iedges]* label

Où

- numW (non utilisée par le simulateur, utile pour la visu)
- Nedges est le nombres d'aretes du contour
- Iedges peut etre négatif si l'arete doit etre retournée. abs(Iedges) désigne l'arete de la ligne abs(Iedges)^ieme du fichier Edges.txt
- label non utilisé


exemple:

Pt1--------e1-------------Pt2
|                           |
|                           |
|                           |
e4			    e2
|                             |
|                              |
|                               |
Pt4--------e3-----pt5---e3-----Pt3

Points.txt
1 0 1 pt1
2 1 1 pt2
3 1 0 pt3
4 0 0 pt4
5 -0.1 0.5 pt5



Edges.txt
0 1 2 0 e1
0 2 3 0 e2
0 3 4 1 5 e3
0 4 1 0 e4

Wires.txt

1 4 1 2 3 4 w1
