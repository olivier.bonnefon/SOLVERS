#include "gen2speciesNL.h"
#include "timeStepping.h"

int initStructure();
int defaultMain(int argc, char *argv[]){
  int ii;
  initStructure();
  buildDirectories();

   //FILE * pcheckF;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"checkdUF.edp");
  FILE * pcheckF= fopen(FILENAME, "w");
  printCheckdUF(pcheckF,1);
  fclose(pcheckF);

  printUserF();
  
  printDefTargetDate();
  printDefTargetLoc();

  //exit(0);
  //randomizePoint(1.5);
  initMatStruct();
//  printViewVtk();
  printVtkViewWires();
  printSaveMeshes();
  
  printParams();
   
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defOutputGen.edp");
  FILE *pOut = fopen(FILENAME, "w");
  buildDefOutput(pOut);
  fclose(pOut);

//  return 1;
  if (BUILD_MESH){
    sprintf(FILENAME,"%s%s",OUTPUTDIR,"defGeomGen.edp");
    FILE *pGeom = fopen(FILENAME, "w");
//  printPoints(pGeom);
    printEdges(pGeom);
    printMeshes(pGeom);
    fclose(pGeom);
  }

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofsGen.edp");
  FILE *pDofs = fopen(FILENAME, "w");
  printDofs(pDofs);
  fclose(pDofs);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofs2DGen.edp");
  pDofs = fopen(FILENAME, "w");
  printDofs2D(pDofs);
  fclose(pDofs);
  if (NDynEdgeType){
    sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofs1DGen.edp");
    pDofs = fopen(FILENAME, "w");
    printDofs1D(pDofs);
    fclose(pDofs);
  }

  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defVarfGen.edp");
  FILE *pVarfs = fopen(FILENAME, "w");
  printVarfs(pVarfs);
  fclose(pVarfs);
  

  buildTS();


  struct stat st = {0};
 

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defPluginVaraibleGen.edp");
  if (stat(OUTPUTDIR, &st) == -1) {
    FILE *pPlugV = fopen(FILENAME, "w");
    if (withControl)
      printPluginVariable(pPlugV);
    fclose(pPlugV);
  }

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defPluginGen.edp");
  if (stat(OUTPUTDIR, &st) == -1) {
    FILE *pPlug = fopen(FILENAME, "w");
    if (withControl)
      printPlugin(pPlug);
    fclose(pPlug);
  }


  sprintf(FILENAME,"%s%s",OUTPUTDIR,"massInfo.txt");
  FILE *pMassInfo = fopen(FILENAME,"w");
  printMassInfo(pMassInfo);
  fclose(pMassInfo);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"vtktopngGen.edp");
  FILE *pVtkLim = fopen(FILENAME,"w");
  printVTKGENPY(pVtkLim);
  fclose(pVtkLim);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"run.sh");
  FILE *pRunsh = fopen(FILENAME,"w");
  buildRunSGEFile(pRunsh);
  fclose(pRunsh);
  freeEdges();
  resetMatStruct();
  freeStructs();

  printModelParams();
 
  
  sprintf(FILENAME,"cd %s;chmod +x *.sh;./genSetFunc.sh",OUTPUTDIR);
  printf("doing %s\n",FILENAME);
  system(FILENAME);
  
}
