import matplotlib.mlab as mlab
from matplotlib.pyplot import figure, show, ylim
import numpy as np


listFile=["STEP0","STEP10","STEP100","STEP1000","STEP10000","STEP20000","STEP29999"]
for namefile in listFile:
    data = [line.strip().split() for line in open(namefile)]
    Nx=len(data)
    Msg=len(data[0])
    Tab=np.empty( (Nx,Msg), dtype=float)
    ii=0
    jj=0
    prev=0.0
    
    for element in data:
        if (ii>0):
            for I in element:
                Tab[ii,jj]=float(I)+prev
                if (jj > 1):
                    prev=Tab[ii,jj]
                jj=jj+1
        jj=0
        ii=ii+1
        prev=0.0




    x=Tab[:,0]
    ut=Tab[:,1]
    y1=Tab[:,3]
    y2=Tab[:,4]
    y3=Tab[:,5]
    y4=Tab[:,6]
#y5=Tab[:,7]
#y6=Tab[:,8]

    fig = figure()
    
    ax = fig.add_subplot(1,1,1)
    ax.plot(x,y1,x,y2,x,y3,x,y4,x,ut)
    ax.fill_between(x, ut, y4, where=ut>=y4, facecolor='grey', interpolate=True)
    ax.fill_between(x, y4, y3, where=y4>=y3, facecolor='green', interpolate=True)
    ax.fill_between(x, y3, y2, where=y3>=y2, facecolor='red', interpolate=True)
    ax.fill_between(x, y2, y1, where=y3>=y2, facecolor='blue', interpolate=True)
    ax.fill_between(x, y1, 0,facecolor='yellow', interpolate=True)
    ax.fill_between(x, y1, 0,facecolor='yellow', interpolate=True)
#    lims = ylim()
    ylim((-0.5,1.5)) 
    show()
    fig.savefig(namefile+'_py.eps')
