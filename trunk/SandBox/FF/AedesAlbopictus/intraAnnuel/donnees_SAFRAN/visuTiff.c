#include "tiffio.h"
#include "string.h"
#include "stdio.h"
#include <iostream>
#include <stdlib.h>
static int width=100;
static int height=100;
static int sampleperpixel = 4;
static char *image=NULL;
static TIFF* out=NULL;
static tsize_t linebytes=0;
static char *buf = NULL;
static int sNgen, snx, sny;
void visuTiff_Init(int Ngen,int nx,int ny){
  sNgen=Ngen;
  snx=nx;
  sny=ny;
  width=nx*Ngen;
  height=ny*Ngen;
//  image=new char [width*ny*sampleperpixel];
  image=new char [width*ny*sampleperpixel];
  out = TIFFOpen("resSimu.tif", "w");
  TIFFSetField (out, TIFFTAG_IMAGEWIDTH, width);
  TIFFSetField(out, TIFFTAG_IMAGELENGTH, height);
  TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, sampleperpixel);   // set number of channels per pixel
  TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 8);    // set the size of the channels
  TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
  //   Some other essential fields to set that you do not have to understand for now.
  TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
  TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
  linebytes = sampleperpixel * width;
  std::cout<<"buf "<<(void*)buf<<std::endl;
  buf =(char *)malloc(linebytes*sizeof(char));
  //if (TIFFScanlineSize(out)*linebytes)
  //  buf =(unsigned char *)_TIFFmalloc(linebytes);
  //else
  //  buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(out));
  std::cout<<"buf "<<(void*)buf<<std::endl;
  TIFFSetField(out, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(out, width*sampleperpixel));
  

   
/*
  for (uint32 row = 0; row < height/2; row++)
  {
    memcpy(buf, &image[row*linebytes], linebytes);    // check the index here, and figure out why not using h*linebytes
    if (TIFFWriteScanline(out, buf, row, 0) < 0){
      std::cout<<"visuTiff break"<<std::endl;
      break;
    }
    }*/
}


void visuTiff_Register(double * eta,int numGen){
  char * pI=NULL;
  int nnull=0;
  double *pEta=eta;
  for (uint32 ngen = 0; ngen < sNgen; ngen++){
//    pEta=eta+4*snx*sny;
    for (uint32 ii = 0; ii < sny; ii++){
      pEta=eta+4*ngen*snx*sny+ii*snx  ;
      for (uint32 jj = 0; jj < snx; jj++){
      
        
        double coef=*(pEta++);
        if (coef<1e-3)
          coef=0;
        if (coef>=1-1e-3){
          coef=1;
        }
        int line=ii;
        int col=sampleperpixel*(jj+ngen*snx);
        pI=&image[col+line*linebytes];
        *(pI++)=255*coef;
        //std::cout<<coef<<std::endl;
        *(pI++)=0;
        *(pI++)=255*(1-coef);
        *(pI++)=255;
        
      }
    }
  }
  //memcpy(buf, &image[0], linebytes);   
//  std::cout<<"nnull "<<nnull<<std::endl;
  for (uint32 row = 0; row < sny; row++){
    memcpy(buf, &image[row*linebytes], linebytes);    // check the index here, and figure out why not using h*linebytes
    uint32 nRow=row+numGen*sny;
    //  std::cout<<"writing line "<<nRow<<std::endl;
    if (TIFFWriteScanline(out, buf, row+numGen*sny, 0) < 0){
      std::cout<<"visuTiff break"<<std::endl;
      break;
    }
  }
}
void visuTiff_End(){
  TIFFClose(out);
}
