#include "stdio.h"
#include "string.h"
#include <stdlib.h>


#include "defRasterSafran.h"


//on construit un raster dont les cases sont centrées sur les (XMIN + i*DX,YMIN+j*DY)_{i,j}. 0=<i<=NX, 0=<j<=NY. 
//la case ij correspond à l'interval [XMIN + i*DX-DX/2,XMIN + i*DX+DX/2]X[YMIN + j*DY-DY/2,YMIN + j*DY+DY/2]
//le raster est de taille (NX+1)*(NY+1), range ligne par ligne, ie:
//la case ij est repéré par raster[i+j*(NX+1)] est contient l'index dans les donnees SAFRAN.
//
//#define DEBUGING
#ifdef DEBUGING
#include "visuTiff.h"
#endif
#define NB_PT_SAFRAN 9892
#define NB_TIME_SAFRAN 8760

#define PATH_TO_DATA_SAFRAN "/home/olivierb/Downloads/DATA_SAFRAN/"
#define PATH_TO_DATA_RASTER "/home/olivierb/Downloads/DATA_RASTER/"

int buildRasterIndex(int *pRasterIndex){
  FILE *fSafran,*fRasterIndex;
  int i,j,k;
  int nnx=NX+1;
  int nny=NY+1;
  int npts=nnx*nny;
  
  int *pRasterEnd=pRasterIndex+npts;
  int *pIaux=pRasterIndex;
  double x,y;
  for (i=0;i<npts;i++){
    *pIaux++=-1;
  }
  fSafran = fopen("latLongSafran.txt","r");
  for (k=0;k<NB_PT_SAFRAN;k++){
    fscanf(fSafran,"%lf%lf",&x,&y);
    
    i=(x-XMIN)/((1+1e-3)*DX);
    j=(y-YMIN)/((1+1e-3)*DY);
    printf("%i %i\n",i,j);
    pRasterIndex[i+j*nnx]=k;
  }
  fclose(fSafran);

  //on bouche les bord
  if(1){
    int *pBeginMinus1;
    pIaux=pRasterIndex;
    if (*pIaux==-1)
      pBeginMinus1=pIaux;
    else{
      while (*pIaux!=-1 && pIaux!=pRasterEnd)
        pIaux++;
      pBeginMinus1=pIaux;
    }
    while (pIaux!=pRasterEnd){
      while (*pIaux==-1 && pIaux!=pRasterEnd)
        pIaux++;
      if (pIaux==pRasterEnd){
        int v=*(pBeginMinus1-1);
        while (pBeginMinus1<pRasterEnd)
          *pBeginMinus1++=v;
        break;
      }
      while (pBeginMinus1<pIaux)
        *pBeginMinus1++=*pIaux;
    
      while (*pIaux!=-1 && pIaux!=pRasterEnd)
        pIaux++;
      if (pIaux!=pRasterEnd)
        pBeginMinus1=pIaux;
    }
  }
  fRasterIndex=fopen("rasterIndex.txt","w");
  pIaux=pRasterIndex;
  for (i=0;i<npts;i++)
    fprintf(fRasterIndex,"%i\n",*pIaux++);
  fclose(fRasterIndex);   
}



void safranToRasterTAIR( int *pRasterIndex){
  char pathData[256];
  char filename[256];
  double *pValT=(double*)malloc(NB_PT_SAFRAN*sizeof(double));
  double *pV=pValT;
  int npts=(NX+1)*(NY+1);
  for (int numA=2010;numA<2017;numA++){
    pV=pValT;
    sprintf(pathData,"%sTAIR_%i_%i/TAIR",PATH_TO_DATA_SAFRAN,numA,numA+1);
    int i=0,k;
  
    for (int i=0; i<NB_TIME_SAFRAN; i++){
      int * pI=pRasterIndex;
      //lecture des donnees SAFRAN, ie raster incomplet 
      sprintf(filename,"%s%i",pathData,i);
      FILE *fTempSafran=fopen(filename,"rb");
      int rfread=fread(pValT, sizeof(double), NB_PT_SAFRAN, fTempSafran);
      if (rfread!=NB_PT_SAFRAN){
        printf("can't read %i double in file %s, read only %i\n",NB_PT_SAFRAN,filename,rfread);
        return;
      }
      fclose(fTempSafran);
      //ouverture du fichier en ecriture pour le raster complet 
      sprintf(filename,"%s/TAIR_%i_%i/TAIR%i",PATH_TO_DATA_RASTER,numA,numA+1,i);
      FILE *fTAIRRaster=fopen(filename,"wb");
      for (k=0;k<npts;k++){
        double vv;
        if (*pI<0)
          vv=0;
        else
          vv=*(pValT+*pI);
        pI++;
        fwrite(&vv,sizeof(double),1,fTAIRRaster);
        //printf("%lf ",vv);
      
      }
      fclose(fTAIRRaster);
#ifdef DEBUGING
      {
        FILE *fTAIRRaster=fopen(filename,"rb");
        double *pValTaux=(double*)malloc(npts*sizeof(double));
        fread(pValTaux,sizeof(double), npts, fTAIRRaster);
        double dMax=0;
        double dMin=1000;
        int nP=0;

        for (k=0;k<npts;k++){
          if (pValTaux[k]>dMax) dMax=pValTaux[k];
          if (pValTaux[k]<dMin) dMin=pValTaux[k];
          if (pValTaux[k]>0) nP++;
          pValTaux[k]= pValTaux[k]/300.0;
        }
        printf("dmin=%lf dmax=%lf nP=%i\n",dMin,dMax,nP);
        fclose(fTAIRRaster);
        visuTiff_Init(1,NX+1,NY+1);
        visuTiff_Register(pValTaux,0);
        visuTiff_End();
      
      }
#endif
    }
  }
}

int main(){
  int i;
  int npts=(NX+1)*(NY+1);
  int *pRasterIndex=(int*)malloc(npts*sizeof(int));
  buildRasterIndex(pRasterIndex);
#ifdef DEBUGING
  {
    double * pD=(double*)malloc(npts*sizeof(double));
    int iMax=0;
    int iMin=1000;
    int nP=0;
    for (i=0;i<npts;i++){
      if (iMax<pRasterIndex[i])
        iMax=pRasterIndex[i];
      if (iMin>pRasterIndex[i])
        iMin=pRasterIndex[i];
      pD[i]=0;
      if (pRasterIndex[i]>-1){
        pD[i]=1;
        nP++;
      }
      visuTiff_Init(1,NX+1,NY+1);
      visuTiff_Register(pD,0);
      visuTiff_End();
    }
    printf("imax=%i iMin=%i nP=%i\n",iMax,iMin,nP);
  }
#endif

  safranToRasterTAIR(pRasterIndex);
  free(pRasterIndex);
  return 0;
}
