#ifndef VISU_TIFF_H
#define VISU_TIFF_H
void visuTiff_Init(int Ngen,int nx,int ny);
void visuTiff_Register(double * eta,int numGen);
void visuTiff_End();
#endif
