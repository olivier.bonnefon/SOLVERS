#include  <iostream>
#include <string>
#include <stdio.h>
#include <fstream>
#include <stdlib.h>
#include <string.h>

using namespace std;
#include "../defRasterSafran.h"
//#define DEBUGING
#ifdef DEBUGING
#include "visuTiff.h"
#endif

long int meshIndex[15]={6295,1636,2448,1116,2222,1968,1528,938,2246,1089,1058,1325,285,164,1069};

int readXY(double **pMeshX,double **pMeshY){
  long int dim;
  double *pY,*pX;

  ifstream xx("X",ios_base::binary);
  ifstream yy("Y",ios_base::binary);
  xx.read((char *) &dim, sizeof(long int));
  yy.read((char *) &dim, sizeof(long int));
  cout<<"reads dim="<<dim<<endl;
  pX=(double *)malloc(dim*sizeof(double));
  pY=(double *)malloc(dim*sizeof(double));
  *pMeshX=pX;*pMeshY=pY;
  for(long int i=0; i<dim; i++)
  {
    xx.read((char *) (pX++), sizeof(double));
    yy.read((char *) (pY++), sizeof(double));
  }
  return dim;
}
//project raster data to mesh
void readTemperatures(double *pData,double *meshX,double *meshY,int dim,int numA){
  char fileName[265];
  double *pDataAux;
  int h,k;
  int ncols=NX+1;
  int nrows=NY+1;
  double xllcorner=XMIN;
  double yllcorner=YMIN;
  double cellsize=DX;
  double val;
  double minT=1000;
  double maxT=0;
  pDataAux=pData;
  
  for (h=1;h<8760;h++){
    //lecture du raster
    sprintf(fileName,"/home/olivierb/Downloads/DATA_RASTER/TAIR_%i_%i/TAIR%i",numA,numA+1,h);
    double *pT=(double*)calloc(nrows*ncols,sizeof(double));
    double *pTaux=(double*)calloc(nrows*ncols,sizeof(double));
    FILE *infile=fopen(fileName,"rb");
    int nr=fread(pT, sizeof(double), ncols*nrows, infile);
    if (nr!=ncols*nrows){
      printf("Error fread return value= % instead of %i\n",nr,ncols*nrows);
      return;
    }
#ifdef DEBUGING
      memcpy(pTaux,pT,sizeof(double)* ncols*nrows);
      for (k=0;k<ncols*nrows;k++){
        if (pTaux[k]<minT)
          minT=pTaux[k];
        if (pTaux[k]>maxT)
          maxT=pTaux[k];
      }
      printf ("minT=%lf maxT=%lf\n",minT,maxT);
      for (k=0;k<ncols*nrows;k++)
        pTaux[k]=pTaux[k]/maxT;
      
      

      
      visuTiff_Init(1,ncols,nrows);
      visuTiff_Register(pTaux,0);
      visuTiff_End();
      break;
#endif
    
      
    fclose(infile);
    //creation du champ de temperature sur le maillage
    for(long int i=0; i<dim; i++)
    {
      double bufT;
      double Y=meshY[i];
      double X=meshX[i];
      int icol=(X-xllcorner)/((1+1e-6)*cellsize);
      int irow=(Y-yllcorner)/((1+1e-6)*cellsize);
      int index=icol+(irow)*ncols;
      if (index >= nrows*ncols || index <0){
        cout<<"bug "<<index<<" soulb be <"<<nrows*ncols;
        return ;
      }
      bufT=pT[index];
      *(pDataAux++)=bufT-273.15;
    }

    free(pT);
  }
    
}

void computeDegreJ(double T0,double Nd,double *pDataT,int dim, double * pDataDegJ){
  for (long int i=0;i<dim;i++)
    if ((*(pDataT++))>T0)
      *(pDataDegJ++)=1;
    else
      *(pDataDegJ++)=0;
  
  for (int h=1;h<365*24;h++){
    memcpy(pDataDegJ,pDataDegJ-dim,dim*sizeof(double));
    for (long int i=0;i<dim;i++){
      if ((*(pDataT++))>T0 && (*pDataDegJ) <Nd)
        (*pDataDegJ)+=1;
      pDataDegJ++;
    }
  }
  
}


void computeAverageT(double Period,double *pDataT,int dim,double *pDataAverage){
  double *pBuf=(double *)calloc(dim,sizeof(double));
  double *pAux=pBuf;
  double *pCur=pDataT;
  double *pAux2=pDataAverage;
  int h;
  for (h=0;h<Period;h++){
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pCur++);
      *(pDataAverage++)=0;
    }
  }
  double *pCurMinusT=pDataT;
  for(;h<24*365;h++){
    memcpy(pDataAverage,pBuf,dim*sizeof(double));
    pDataAverage+=dim;
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pCur++)-(*pCurMinusT++);
    }
  }
  free(pBuf);
  for(h=0;h<24*365*dim;h++){
    *pAux2=*pAux2/Period;
    pAux++;
  }
}
void computeAverageJanuaryT(double *pDataT,int dim, double *pBuf){
  
  double *pAux;
  for (int h=0;h<30*24;h++){
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pDataT++);
    }
  }
  pAux=pBuf;
  for (long int i=0;i<dim;i++){
    *pAux=*pAux/(24*30*1.0);
    pAux++;
  }
  

  
}
#include <sys/dir.h>
#include <sys/stat.h>
#include <sys/types.h>

  //chaque serie_numA contient 1 an de donnee commencant le 1 aout à 6h:
  //    annee numA:
  //    -----------
  //     nbr heure du 1 aout à 6h aux 31 decembre à minuit=152 j + 6 h=152*24 + (24-6)=3666
  //     correspondnat aux indices {8759-3666+1,...,8759}
  //     annee numA+1:
  //    -------------
  //     nbr heure du 1 janvier à 0h aux 1 aout à 6h= 8760-3666
  //     correspondnat aux indices {0,...,8759-3666}
void saveField( double *pDataAux,int numA,char * fieldString){
  char fileName[256];
  int h;
  int curYear=0;
  int curH=0;
  double bufT;
  sprintf(fileName,"DATA_%i",numA);mkdir(fileName,0755);
  sprintf(fileName,"DATA_%i/%s",numA,fieldString);mkdir(fileName,0755);
  sprintf(fileName,"DATA_%i",numA+1);mkdir(fileName,0755);
  sprintf(fileName,"DATA_%i/%s",numA+1,fieldString);mkdir(fileName,0755);

  for ( h=0;h<24*365;h++){
    if (h<3666){//aout à decembre
      curYear=numA;
      curH=8759-3666+1+h;
    }
    else{
      curYear=numA+1;
      curH=h-3666;
    }
//    int curIndexInDim=0;
//    pDataAux=pDatas+h*dim
    for (int nth=0;nth<15;nth++){
      long int auxdim=meshIndex[nth];
      sprintf(fileName,"DATA_%i/%s/mesh%is%d",curYear,fieldString,nth,curH);
      ofstream outfile (fileName,ios_base::binary);
      outfile.write ((char*) &auxdim, sizeof(long int));
      for(long int i=0; i<auxdim; i++){
        bufT=*(pDataAux++);
        outfile.write ((char*) &bufT, sizeof(double));
      }
//      curIndexInDim+=auxdim;
      outfile.close();
    }
  }
}
int main(){
  char fileName[256];
  int ncols;
  int nrows;
  double xllcorner;
  double yllcorner;
  double cellsize;
  double val;
  double minT=1000;
  double *pDatas,*pDatasDegJ,*pDatasAverage;
  double *pDataAux;
  long int dim;
  double *meshY,*meshX;
  double bufT;
  dim=readXY(&meshX,&meshY);
//temperature pDatas(num,h)=pDatas[num + h*dim];
  //ie, pDatas[num + h*dim]; est la température à l'heure h du sommet num
  pDatas=(double *)malloc(dim*24*365*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  pDatasDegJ=(double *)malloc(dim*24*365*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  int curYear=0;
  int curH=0;
  
  pDatasAverage=(double *)malloc(dim*24*365*sizeof(double));
  double *pBuf=(double *)calloc(dim,sizeof(double));
  for (int numA=2002;numA<2017;numA++){
    //mode_t mask =
    readTemperatures(pDatas,meshX,meshY,dim,numA);
    saveField(pDatas,numA,"T_BY_HEURE");
    
    computeDegreJ(17,240,pDatas,dim,pDatasDegJ);
    saveField(pDatasDegJ,numA,"DEGJ_BY_HEURE");

    computeAverageT(7*24,pDatas,dim,pDatasAverage);
    saveField(pDatasAverage,numA,"AVERAGE_BY_HEURE");

    //pDatas+3666*dim pour ce placer au 1 janvier
    computeAverageJanuaryT(pDatas+3666*dim,dim,pBuf);
    pDataAux=pBuf;
//    int curIndexInDim=0;
    for (int nth=0;nth<15;nth++){
      long int auxdim=meshIndex[nth];
      sprintf(fileName,"DATA_%i/januaryAverage_%is%i",numA+1,nth,numA+1);
      ofstream outfile (fileName,ios_base::binary);
      outfile.write ((char*) &auxdim, sizeof(long int));
      for(long int i=0; i<auxdim; i++){
        bufT=*(pDataAux++);
        outfile.write ((char*) &bufT, sizeof(double));
      }
      //    curIndexInDim+=auxdim;
      outfile.close();
    }
    /*
    sprintf(fileName,"DATA_%i/januaryAverage_%i",numA+1,numA+1);
    ofstream outfile (fileName,ios_base::binary);
    outfile.write ((char*) &dim, sizeof(long int));
    pDataAux=pBuf;
    for(long int i=0; i<dim; i++)
    {
      bufT=*(pDataAux++);
      outfile.write ((char*) &bufT, sizeof(double));
    }
    outfile.close();
    */
  }
  
  free(pBuf);
  free(pDatasAverage);
  free(pDatas);
  free(pDatasDegJ);
  return 0;
}
