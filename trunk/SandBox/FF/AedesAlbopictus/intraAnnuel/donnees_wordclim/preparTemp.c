#include  <iostream>
#include <string>
#include <stdio.h>
#include <fstream>
#include <stdlib.h>
#include <string.h>
using namespace std;

int readLatitudesLongitudes(double **pMeshLat,double **pMeshLong){
  long int dim;
  double *pLat,*pLong;

  ifstream latitudes("latitudes",ios_base::binary);
  ifstream longitudes("longitudes",ios_base::binary);
  latitudes.read((char *) &dim, sizeof(long int));
  longitudes.read((char *) &dim, sizeof(long int));
  cout<<"reads dim="<<dim<<endl;
  pLat=(double *)malloc(dim*sizeof(double));
  pLong=(double *)malloc(dim*sizeof(double));
  *pMeshLat=pLat;*pMeshLong=pLong;
  for(long int i=0; i<dim; i++)
  {
    latitudes.read((char *) (pLat++), sizeof(double));
    longitudes.read((char *) (pLong++), sizeof(double));
  }

  
  return dim;
}

void readTemperatures(double *pData,double *meshLong,double *meshLat,int dim){
  char fileName[265];
  double *pDataAux;
  int month;
  int ncols;
  int nrows;
  double xllcorner;
  double yllcorner;
  double cellsize;
  double val;
  double minT=1000;
  for (month=1;month<13;month++){
    pDataAux=pData+(month-1)*30*24*dim;
    sprintf(fileName,"temp_09GMT_%02d.asc",month);
    ifstream infile(fileName,ios_base::in);
    if(!infile){
      cerr << "Impossible d'ouvrir le fichier ! " << fileName<<endl;
      continue;
    }
    string chaine1;
    infile>>chaine1;
    infile>>ncols;
    cout<<chaine1<<ncols<<endl;
    infile>>chaine1;
    infile>>nrows;
    cout<<chaine1<<nrows<<endl;
    infile>>chaine1;
    infile>>xllcorner;
    cout<<chaine1<<xllcorner<<endl;
    infile>>chaine1;
    infile>>yllcorner;
    cout<<chaine1<<yllcorner<<endl;
    infile>>chaine1;
    infile>>cellsize;
    cout<<chaine1<<cellsize<<endl;
    
    infile>>chaine1;
    infile>>val;
    cout<<chaine1<<cellsize<<endl;
    cout<<chaine1<<cellsize<<endl;
    double *pT=(double*)calloc(nrows*ncols,sizeof(double));
    double *pTmp=pT;
    for (int i=0;i<nrows*ncols;i++)
      infile>>*pTmp++;
    
    for(long int i=0; i<dim; i++)
    {
      double centerLat=47.868;
      double centerLong=2.588;
      double bufT=-999;
      double coef=0.0;
      while (bufT<-100){
        double lat=(1.0-coef)*meshLat[i]+coef*centerLat;
        double longi=(1.0-coef)*meshLong[i]+coef*centerLat;
        int icol=(lat-xllcorner)/cellsize;
        int irow=(longi-yllcorner)/cellsize;
        int index=icol+((nrows-1)-irow)*ncols;
        bufT=pT[index];
        if (index >= nrows*ncols)
          cout<<"bug "<<index<<" soulb be <"<<nrows*ncols;
        //bufT=pT[irow+icol*nrows];
        //cout<<" index="<<icol+irow*ncols<<" lat "<<lat<<" longi "<<longi<<" icol "<<icol<<" irow "<<irow<<" T="<<bufT<<endl;
        if (bufT<-100){
          coef+=0.1;
          cout<<"try coef="<<coef<<endl;
        }
      }
      if (bufT<minT)
        minT=bufT;
      *(pDataAux++)=bufT;
    }

    free(pT);
  }
  double heureCoef=0;
  double *pDaux=pData;
  double *pPrev;
  double *pNext;
  
  for (int month=0; month<12; month++){
    cout<<"doing month="<<month<<endl;
    pPrev=pData+month*30*24*dim;
    pNext=pData+((month+1)%12)*30*24*dim;
    heureCoef=0;
    for (int jj=0;jj<30;jj++){
      for (int heure=0;heure<24;heure++){
        heureCoef+=1.0/(24*30.0);
        if (month==0 && jj==0 && heure==0){
          pDaux+=dim;
          continue;
        }
        for (long int i=0;i<dim;i++){
          *(pDaux++)=(1-heureCoef)*(pPrev[i])+heureCoef*(pNext[i]);
        }
      }
    }
  }
  pPrev=pDaux-dim;
  for (int h=0;h<365*24-30*12*24;h++){
    memcpy(pDaux,pPrev,dim*sizeof(double));
   
    pDaux+=dim;
  }
    
}

void computeDegreJ(double T0,double Nd,double *pDataT,int dim, double * pDataDegJ){
  for (long int i=0;i<dim;i++)
    if ((*(pDataT++))>T0)
      *(pDataDegJ++)=1;
    else
      *(pDataDegJ++)=0;
  
  for (int h=1;h<365*24;h++){
    memcpy(pDataDegJ,pDataDegJ-dim,dim*sizeof(double));
    for (long int i=0;i<dim;i++){
      if ((*(pDataT++))>T0 && (*pDataDegJ) <Nd)
        (*pDataDegJ)+=1;
      pDataDegJ++;
    }
  }
  
}


void computeAverageT(double Period,double *pDataT,int dim,double *pDataAverage){
  double *pBuf=(double *)calloc(dim,sizeof(double));
  double *pAux=pBuf;
  double *pCur=pDataT;
  double *pAux2=pDataAverage;
  int h;
  for (h=0;h<Period;h++){
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pCur++);
      *(pDataAverage++)=0;
    }
  }
  double *pCurMinusT=pDataT;
  for(;h<24*365;h++){
    memcpy(pDataAverage,pBuf,dim*sizeof(double));
    pDataAverage+=dim;
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pCur++)-(*pCurMinusT++);
    }
  }
  free(pBuf);
  for(h=0;h<24*365*dim;h++){
    *pAux2=*pAux2/Period;
    pAux++;
  }
}
void computeAverageJanuaryT(double *pDataT,int dim, double *pBuf){
  
  double *pAux;
  for (int h=0;h<30*24;h++){
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pDataT++);
    }
  }
  pAux=pBuf;
  for (long int i=0;i<dim;i++){
    *pAux=*pAux/(24*30*1.0);
    pAux++;
  }
  

  
}

int main(){
  char fileName[256];
  int ncols;
  int nrows;
  double xllcorner;
  double yllcorner;
  double cellsize;
  double val;
  double minT=1000;
  double *pDatas,*pDatasDegJ,*pDatasAverage;
  double *pDataAux;
  long int dim;
  double *meshLat,*meshLong;
  double bufT;
  dim=readLatitudesLongitudes(&meshLat,&meshLong);
//temperature pDatas(num,h)=pDatas[num + h*dim];
  //ie, pDatas[num + h*dim]; est la température à l'heure h du sommet num
  pDatas=(double *)malloc(dim*24*365*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  pDatasDegJ=(double *)malloc(dim*24*365*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  
  readTemperatures(pDatas,meshLong,meshLat,dim);
  pDataAux=pDatas;
  for (int h=0;h<24*365;h++){
    sprintf(fileName,"T_BY_HEURE/meshT%d",h);
    ofstream outfile (fileName,ios_base::binary);
    outfile.write ((char*) &dim, sizeof(long int));
    for(long int i=0; i<dim; i++){
      bufT=*(pDataAux++);
      outfile.write ((char*) &bufT, sizeof(double));
    }
    outfile.close();
  }
  computeDegreJ(17,240,pDatas,dim,pDatasDegJ);
  pDataAux=pDatasDegJ;
  for (int h=0;h<24*365;h++){
    sprintf(fileName,"DEGJ_BY_HEURE/meshDEGJ%d",h);
    ofstream outfile (fileName,ios_base::binary);
    outfile.write ((char*) &dim, sizeof(long int));
    for(long int i=0; i<dim; i++)
    {
      bufT=*(pDataAux++);
      outfile.write ((char*) &bufT, sizeof(double));
    }
    outfile.close();
  }
  pDatasAverage=(double *)malloc(dim*24*365*sizeof(double));
  computeAverageT(7*24,pDatas,dim,pDatasAverage);
  pDataAux=pDatasAverage;
  for (int h=0;h<24*365;h++){
    sprintf(fileName,"AVERAGE_BY_HEURE/meshAverage%d",h);
    ofstream outfile (fileName,ios_base::binary);
    outfile.write ((char*) &dim, sizeof(long int));
    for(long int i=0; i<dim; i++)
    {
      bufT=*(pDataAux++);
      outfile.write ((char*) &bufT, sizeof(double));
    }
    outfile.close();
  }

  double *pBuf=(double *)calloc(dim,sizeof(double));
  pDataAux=pBuf;
  computeAverageJanuaryT(pDatas,dim,pBuf);
  ofstream outfile ("januaryAverage",ios_base::binary);
  outfile.write ((char*) &dim, sizeof(long int));
  for(long int i=0; i<dim; i++)
    {
      bufT=*(pDataAux++);
      outfile.write ((char*) &bufT, sizeof(double));
    }
  outfile.close();
  free(pBuf);
  
  free(pDatasAverage);
  free(pDatas);
  free(pDatasDegJ);
  return 0;
}
