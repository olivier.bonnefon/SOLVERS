#include "stdio.h"

int main(){
  int N=113;
  int i,j;
  printf("load \"iovtk\"\n");
  printf("load \"resizeCSR\"\n");
  printf("include \"myMacro.edp\";\n");
  printf("include \"defGeomGen.edp\";\n");
  printf("include \"postTraitementLoad.edp\";\n");
  printf("\n");
  
  for (i=1;i<N+1;i++){
    printf("plot(");
    for(j=1;j<=i;j++){
      printf("ThDep%i,",j);
    }
    printf("wait=true,bb=[[-5,42],[9,52]],cmm=\"%i\");\n",i);
  }
}
