import matplotlib.mlab as mlab
from matplotlib.pyplot import figure, show, ylim, xlim, clf, rc, xticks, yticks
import numpy as np

PREFIX="LV"
listFile=["STEP0","STEP10","STEP99","STEP200","STEP400","STEP500","STEP600","STEP800","STEP900","STEP999","STEP1200","STEP1299","STEP1499"]
for namefile in listFile:
    data = [line.strip().split() for line in open(namefile)]
    Nx=len(data)
    Msg=len(data[0])
    Tab=np.empty( (Nx,Msg), dtype=float)
    ii=0
    jj=0
    prev=0.0
    
    for element in data:
        if (ii>0):
            for I in element:
                Tab[ii,jj]=float(I)+prev
                if (jj > 1):
                    prev=Tab[ii,jj]
                jj=jj+1
        jj=0
        ii=ii+1
        prev=0.0




    x=Tab[:,0]
    ut=Tab[:,1]
    y1=Tab[:,2]
    y2=Tab[:,3]
    y3=Tab[:,4]
    y4=Tab[:,5]
    y5=Tab[:,6]
#    y6=Tab[:,7]

    fig = figure()
    rc('xtick', labelsize=20);
    rc('ytick', labelsize=20);
    
    ax = fig.add_subplot(1,1,1)
    ax.plot(x,y1,x,y2,x,ut, color='black')
#    ax.fill_between(x, ut, y6, where=ut>=y6, facecolor='grey', interpolate=True)
    ax.fill_between(x, ut, y5, where=ut>=y5, facecolor='grey', interpolate=True)
    ax.fill_between(x, y5, y4, where=y5>=y4, facecolor='yellow', interpolate=True)
    ax.fill_between(x, y4, y3, where=y4>=y3, facecolor='brown', interpolate=True)
    ax.fill_between(x, y3, y2, where=y3>=y2, facecolor='orange', interpolate=True)
    ax.fill_between(x, y2, y1, where=y2>=y1, facecolor='red', interpolate=True)
    ax.fill_between(x, y1, 0,facecolor='blue', interpolate=True)
    ylim((-0.01,1.2))
    xlim((0,150)) 
    fig.savefig(PREFIX+namefile+'_py.eps')
    show()
    clf()
