from matplotlib.pyplot import figure, show, ylim, xlim, clf, rc, xticks, yticks,locator_params
import numpy as np
print 'DEBUT INFO a propos de doCumImg.py'
print '-Affiche les signaux avec des couleurs entre eux'
print '-necessite d avoir au moins deux signaux'
print 'FIN INFO a propos de doCumImg.py'
if (rd.nbSignals < 2):
    print '-Un seul signal: stop doCumImg.py'
    rd.sys.exit(0)

locator_params(axis = 'x', nbins = 3)
ut=np.empty(rd.n1Dofs, dtype=float)
y1=np.empty(rd.n1Dofs, dtype=float)
y2=np.empty(rd.n1Dofs, dtype=float)
y3=np.empty(rd.n1Dofs, dtype=float)
y4=np.empty(rd.n1Dofs, dtype=float)
y5=np.empty(rd.n1Dofs, dtype=float)
y6=np.empty(rd.n1Dofs, dtype=float)

for istep in rd.ll:
    fig = figure()
    #locator_params(axis = 'x', nbins = 5)
    #xticks(tick_range1, xtickval1 ,position=(0,0), fontsize=10)
    rc('xtick', labelsize=15);
    rc('ytick', labelsize=15);
    ax = fig.add_subplot(1,1,1)
    simStep=rd.data[istep*rd.n1Dofs][0]
    for isg in rd.llsng:
        for ii in range(0,rd.n1Dofs):
            value=0;
            if (ii < (rd.n1Dofs -100)):
                value=rd.data[istep*rd.n1Dofs+100+ii][2+isg]            
	    if (isg==0):
	       ut[ii]=value
            if (isg==1):
                y1[ii]=value
            if (isg==2):
                y2[ii]=value
                y2[ii]+=y1[ii]
            if (isg==3):
                y3[ii]=value
                y3[ii]+=y2[ii]
            if (isg==4):
                y4[ii]=value
                y4[ii]+=y3[ii]
            if (isg==5):
                y5[ii]=value
                y5[ii]+=y4[ii]
            if (isg==6):
                y6[ii]=value
                y6[ii]+=y5[ii]


    if (rd.nbSignals == 2):
        ax.plot(rd.x,y1,rd.x,ut, color='black')
        ax.fill_between(rd.x, y1, 0,facecolor='blue', interpolate=True)
        ax.fill_between(rd.x, ut, y1, where=ut>=y1, facecolor='grey', interpolate=True)

    if (rd.nbSignals == 3):
        ax.plot(rd.x,y2,rd.x,y1,rd.x,ut, color='black')
        ax.fill_between(rd.x, y1, 0,facecolor='blue', interpolate=True)
        ax.fill_between(rd.x, y2, y1, where=y2>=y1, facecolor='red', interpolate=True)
        ax.fill_between(rd.x, ut, y2, where=ut>=y2, facecolor='grey', interpolate=True)

    if (rd.nbSignals == 4):
        ax.plot(rd.x,y3,rd.x,y2,rd.x,y1,rd.x,ut, color='black')
        ax.fill_between(rd.x, y1, 0,facecolor='blue', interpolate=True)
        ax.fill_between(rd.x, y2, y1, where=y2>=y1, facecolor='red', interpolate=True)
        ax.fill_between(rd.x, y3, y2, where=y3>=y2, facecolor='orange', interpolate=True)
        ax.fill_between(rd.x, ut, y3, where=ut>=y3, facecolor='grey', interpolate=True)

    if (rd.nbSignals == 5):
        ax.plot(rd.x,y4,rd.x,y3,rd.x,y2,rd.x,y1,rd.x,ut, color='black')
        ax.fill_between(rd.x, y1, 0,facecolor='blue', interpolate=True)
        ax.fill_between(rd.x, y2, y1, where=y2>=y1, facecolor='red', interpolate=True)
        ax.fill_between(rd.x, y3, y2, where=y3>=y2, facecolor='orange', interpolate=True)
        ax.fill_between(rd.x, y4, y3, where=y4>=y3, facecolor='brown', interpolate=True)
        ax.fill_between(rd.x, ut, y4, where=ut>=y4, facecolor='grey', interpolate=True)

    if (rd.nbSignals == 6):
        ax.plot(rd.x,y5,rd.x,y4,rd.x,y3,rd.x,y2,rd.x,y1,rd.x,ut, color='black')
        ax.fill_between(rd.x, y1, 0,facecolor='blue', interpolate=True)
        ax.fill_between(rd.x, y2, y1, where=y2>=y1, facecolor='red', interpolate=True)
        ax.fill_between(rd.x, y3, y2, where=y3>=y2, facecolor='orange', interpolate=True)
        ax.fill_between(rd.x, y4, y3, where=y4>=y3, facecolor='brown', interpolate=True)
        ax.fill_between(rd.x, y5, y4, where=y5>=y4, facecolor='yellow', interpolate=True)
        ax.fill_between(rd.x, ut, y5, where=ut>=y5, facecolor='grey', interpolate=True)

    ylim((-0.1,1.2))
    xlim((rd.XMIN,rd.XMAX))
    if (rd.showmode):
        ax.set_title('Cumul group step '+str(rd.data[istep*rd.n1Dofs][0]))
        show()
    else:
        fig.savefig(rd.PREFIX+"_"+str(rd.data[istep*rd.n1Dofs][0])+'_py.eps')
        
    clf()
