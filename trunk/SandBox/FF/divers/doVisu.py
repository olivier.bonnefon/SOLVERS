import readData as rd
from matplotlib.pyplot import figure, show, ylim, xlim, clf, rc, xticks, yticks
print 'DEBUT INFO doVisu.py'
print '-Plot all signals to the steps.'
print 'END INFO doVisu.py'

for istep in rd.ll:
    fig = figure()
    rc('xtick', labelsize=20);
    rc('ytick', labelsize=20);
    ax = fig.add_subplot(1,1,1)
    for isg in rd.llsng:
        for ii in range(0,rd.n1Dofs):
            rd.y[ii]=rd.data[istep*rd.n1Dofs+ii][2+isg]
        ax.plot(rd.x,rd.y)
    ylim((-0.1,1.2))
    xlim((rd.XMIN,rd.XMAX))
    ax.set_title('STEP '+str(istep))
    if (rd.showmode):
        show()
    else:
        fig.savefig(rd.PREFIX+"_"+str(istep)+'_py.eps')
    clf()

