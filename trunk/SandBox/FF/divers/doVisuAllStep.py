import readData as rd
from matplotlib.pyplot import figure, show, ylim, xlim, clf, rc, xticks, yticks
print 'DEBUT INFO doVisuAllStep.py'
print '-Plot all signals of steps on the same figure.'
print 'END INFO doVisuAllStep.py'

fig = figure()
rc('xtick', labelsize=20);
rc('ytick', labelsize=20);
ax = fig.add_subplot(1,1,1)

    
for istep in rd.ll:
    for isg in rd.llsng:
        for ii in range(0,rd.n1Dofs):
            rd.y[ii]=rd.data[istep*rd.n1Dofs+ii][2+isg]
        ax.plot(rd.x,rd.y)

ylim((-0.1,1.2))
xlim((rd.XMIN,rd.XMAX))
ax.set_title('All STEP ')
if (rd.showmode):
    show()
else:
    fig.savefig(rd.PREFIX+"MultiStep_"+str(istep)+'_py.pdf')
clf()

