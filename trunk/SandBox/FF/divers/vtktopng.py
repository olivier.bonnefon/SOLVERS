import vtk
import os
from array import array
#baseName="/home/olivierb/VTK_PYTHON/diff/u"
species=array('c','uv')

#numStep=1998
#for num in range(1,150):
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(500,500)

minV=0.0005
maxV=1.0
nbStep=20
step=1
nbOmega=9
WinSize=500
finput = open("input.txt", 'w')
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(WinSize,WinSize)
rgb=[0,0,0]

for spec in species:
    baseName="/home/olivierb/solvers/trunk/SandBox/FF/diff2D1D/"+spec
    for numStep in range(1,nbStep):
        fenetre=vtk.vtkRenderWindow()
        fenetre.SetSize(WinSize,WinSize)
        ren=vtk.vtkRenderer()
        fenetre.AddRenderer(ren)
        execfile("buildActorWires.py")
        execfile("addActorWires.py")
    #execfile("buildActorWires2.py")
    #execfile("addActorWires2.py")
    #execfile("buildActorWires1.py")
    #execfile("addActorWires1.py")
        ren.SetBackground(0,0,0)
        for numOmega in range(0,nbOmega):
            fileName=baseName+str(numOmega)+"s"+str(numStep*step)+".vtk"
            r=vtk.vtkGenericDataObjectReader()
            r.SetFileName(fileName)
            r.Update()
            o=r.GetOutput()
            o.GetCellData().SetActiveScalars("UU")
            aPolyVertexMapper = vtk.vtkDataSetMapper()
            aPolyVertexMapper.SetInput(o)
            aPolyVertexMapper.SetScalarRange(minV,maxV)
            aPolyVertexMapper.UseLookupTableScalarRange=1
            lut=aPolyVertexMapper.GetLookupTable()
            lut.SetHueRange(251.0/360.0,0)
            lut.SetSaturationRange(1,1)
            lut.SetValueRange(1,1)
            lut.Build()
            aPolyVertexActor = vtk.vtkActor()
            aPolyVertexActor.SetMapper(aPolyVertexMapper)
            ren.AddActor(aPolyVertexActor)
        
    
    #iren=vtk.vtkRenderWindowInteractor()
    #iren.SetRenderWindow(fenetre)
    #iren.Initialize()
        fenetre.Render()
        image = vtk.vtkWindowToImageFilter()
        image.SetInput(fenetre)
        image.Update()
        fenetre.Start()
        fenetre.Render()
        fenetre.Finalize()
        image.UpdateInformation()
        image.Update()
    #iren.Render()
    #iren.Render()
    #iren.Render()
        png = vtk.vtkPNGWriter()
        fileStepName="step"+spec+str(numStep)+".png"
        png.SetFileName(fileStepName)
        png.SetInputConnection(image.GetOutputPort())
        png.Write()

for numStep in range(1,nbStep):
    pngfiles=""
    for spec in species:
        fileStepName="step"+spec+str(numStep)+".png"
        pngfiles=pngfiles+" "+fileStepName
    os.system("convert "+pngfiles+" "+"+append step"+str(numStep)+".png")
    finput.write("step"+str(numStep)+".png"+"\n")
    #iren.Start()

finput.close()
os.system("mencoder \"mf://@input.txt\" -mf fps=20 -o test.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800")
    
os.system("vlc test.avi")
   


