import math
from matplotlib.pyplot import figure, show, ylim, xlim, clf, rc, xticks, yticks
print 'DEBUT INFO doVisu.py'
print '-Plot all signals to the steps.'
print 'END INFO doVisu.py'
YMIN=1000
YMAX=-1000
c=0.734
d=1
a1=0.9
lplus=(c+math.sqrt(c*c+4.0*d*(a1-1)))/(2.0*d)
lmoins=(c-math.sqrt(c*c+4.0*d*(a1-1)))/(2.0*d)
for istep in rd.ll:
    fig = figure()
    rc('xtick', labelsize=20);
    rc('ytick', labelsize=20);
    ax = fig.add_subplot(1,1,1)
    for isg in rd.llsng:
        for ii in range(0,rd.n1Dofs):
            if (float(rd.data[istep*rd.n1Dofs+ii][2+isg]) >0):
                rd.y[ii]=math.log(float(rd.data[istep*rd.n1Dofs+ii][2+isg]))
                if (rd.y[ii]>YMAX):
                    YMAX=rd.y[ii]
                if (rd.y[ii]<YMIN):
                    YMIN=rd.y[ii]
            else:
                rd.y[ii]=0;
        ax.plot(rd.x,rd.y)
        break;
    ax.plot(rd.x,-lplus*(rd.x -0.25*(rd.XMIN+rd.XMAX)))
    ax.plot(rd.x,-lmoins*(rd.x -0.25*(rd.XMIN+rd.XMAX)))
    ylim((YMIN,YMAX))
    xlim((rd.XMIN,rd.XMAX))
    ax.set_title('STEP '+str(istep))
    if (rd.showmode):
        show()
    else:
        fig.savefig(rd.PREFIX+"Log_"+str(istep)+'_py.eps')
    clf()
