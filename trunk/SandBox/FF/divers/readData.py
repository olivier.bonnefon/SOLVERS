import matplotlib.mlab as mlab
import numpy as np
import sys
from subprocess import check_output
namefile="resSG3695"
showmode=1
onlysg=-1
dx=1.0
offsetX=0.0
maxStep=-1
freqStep=1
freqDX=1
nbImg=3
if (1):
    if (len(sys.argv)<2):
        print 'usage : python doVisu file [nbImg(3) show(1) onlysgn(-1) dx(1.0) offsetX(0.0) maxStep(-1) freqDX[1]]'
        print 'usage : nbImg pour le nombre d images a faire'
        print 'usage : show ecran ou fichier'
        print 'usage : onlysgn pour visualiser un signal en particulier'
        print 'usage : offsetX decalage de l abscisse'
        print 'usage : maxStep dernier pas pris en compte'
        print 'freqDX: frequence pour la prise en compte des pas d espace'
        sys.exit(2)
    if(len(sys.argv)>1):
        namefile=str(sys.argv[1])
    if(len(sys.argv)>2):
        nbImg=int(sys.argv[2])
    if(len(sys.argv)>3):
        showmode=int(sys.argv[3])
    if(len(sys.argv)>4):
        onlysg=int(sys.argv[4])
    if(len(sys.argv)>5):
        dx=float(sys.argv[5])
    if(len(sys.argv)>6):
        offsetX=float(sys.argv[6])
    if(len(sys.argv)>7):
        maxStep=int(sys.argv[7])
    if(len(sys.argv)>8):
        freqDX=int(sys.argv[8])

line = check_output(['tail', '-1', namefile])
aux = line.strip().split()
if (maxStep<0):
    maxStep=int(aux[0])

freqStep=maxStep/nbImg
print 'READING file ', namefile,' nbImg ',nbImg
data={}
i=0
nbSteps=0;
curNumSteps=-1;

with open(namefile, 'r') as dataFile:
    for line in dataFile:
        aux = line.strip().split()
        numStep=int(aux[0])
        numStepDX=int(aux[1])
        if (numStep%freqStep == 0 or numStep==maxStep):
            if (curNumSteps != numStep):
                nbSteps=nbSteps+1
                curNumSteps=numStep
            if (maxStep < 0 or numStep<=maxStep):
                if (numStepDX%freqDX == 0):
                    data[i]=aux
                    i=i+1
            else:
                break

n1Dofs=0;
while (data[n1Dofs][0] == data[0][0]):
    n1Dofs = n1Dofs+1


#data = [line.strip().split() for line in open(namefile) ]
Nx=len(data)
Msg=len(data[0])
Tab=np.empty( (Nx,Msg), dtype=float)
ii=0
jj=0
prev=0.0
print 'BEGIN INFO FROM READ DATA'
#n1Dofs=int(data[Nx-1][1])-int(data[0][1])+1

print '-nombre de dof:'+str(n1Dofs)
print '-nombre de steps a tracer:'+str(nbSteps)


nbSignals=Msg-2
print '-nombre de signaux:'+str(nbSignals)
PREFIX="img"+namefile
x=np.empty(n1Dofs, dtype=float)
y=np.empty(n1Dofs, dtype=float)
for ii in range(0,n1Dofs):
    x[ii]=data[ii][1]
    x[ii]=dx*x[ii]-offsetX
XMIN=x[0]
XMAX=x[n1Dofs-1]+1
    
ll=range(0,nbSteps-1,1)
#ll.append(nbSteps-1)
llsng=range(0,nbSignals)
if (onlysg != -1):
    llsng=range(onlysg,onlysg+1)

if (onlysg >= nbSignals):
    print 'no corresponding signal'
    sys.exit(2)
print 'FIN INFO FROM READ DATA'
