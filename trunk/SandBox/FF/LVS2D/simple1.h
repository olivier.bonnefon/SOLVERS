
#include "systemDescr.h"
int NbSpecies=3;
int NDynFieldType=2;
int * FieldTypes=0;

int NPoints=7;
double Px[]={0 ,5,5,0  ,0,-5,-5};
double Py[]={0 ,0 ,5 ,7.5,5,5,0};
//double Px[]={10., 0,  10, 0.,10};
//double Py[]={10., 10, 0,  0.,5};
//domaine 0:

/*
              
  
                (5,7.5)
                 |     .
                 |      ....
(0,5)---------(5,5)       ......(10,5) 
  |              |                  |
  |              |                  |
  |      1       |         0        |
  |     KPP      |      DIFF        |
  |              |                  |
  |              |                  |
(0,0)-----------(5,0)-------------(10,0)
*/


int NEdges=8;
int EDGES[]={
             0,1,
             1,2,
             2,3,
             3,4,
             4,0,
             4,5,
             5,6,
             6,0
             };


int Nwires=2;//one wire per domain
//1: diffusion
//2: kpp
//3: uv
//int WiresType[]={1,2};
int Wsizes[]={5,4};
int WIRES[]={0,1,2,3,4,
             5,6,7,-4};



/*int ABSWIRES[]={0,1,2,3};*/

//  -1,4,5,6,7};

int nPerLength=2;

void initStructure(){
  int ii;
  double coef=0.2;
  allocStructs();
  sprintf(OUTPUTDIR,"/home/olivierb/solvers/trunk/SandBox/FF/LVS2D/RES/");
  for (ii=0;ii<Nwires;ii++){
//field ii pop 0
  aFieldPop[ii].D2DX[0]=5;
  aFieldPop[ii].D2DY[0]=5;
  aFieldPop[ii].K[0]=10;
  aFieldPop[ii].R[0]=2;
  aFieldPop[ii].T[0]=0;
  aFieldPop[ii].rho[0]=0.01;
  
  aFieldPop[ii].Ccoup[0][1]=-1*coef;
  aFieldPop[ii].Ccoup[0][2]=-1*coef;
  aFieldPop[ii].Cmalthus[0]=0;
  aFieldPop[ii].source[0]=0.00;
//field ii pop 1
  aFieldPop[ii].D2DX[1]=5;
  aFieldPop[ii].D2DY[1]=5;
  aFieldPop[ii].K[1]=15;
  aFieldPop[ii].R[1]=0;
  aFieldPop[ii].Ccoup[1][0]=1*coef;
  aFieldPop[ii].Cmalthus[1]=-1;
  aFieldPop[ii].source[1]=0.0;

//field ii pop 2
  aFieldPop[ii].D2DX[2]=5;
  aFieldPop[ii].D2DY[2]=5;
  aFieldPop[ii].K[2]=15;
  aFieldPop[ii].R[2]=0;
  aFieldPop[ii].Ccoup[2][0]=1*coef;
  aFieldPop[ii].Cmalthus[2]=-1;
  aFieldPop[ii].source[2]=0.0;
  }
 
  printFieldPops(aFieldPop,Nwires);
  
}



