
#include "systemDescr.h"
int NbSpecies=4;
int NDynFieldType=1;
int * FieldTypes=0;
#include "usC2.h"

double yLegend=26;
double xLegend=-116;
double scaleLegend=5;

int nPerLength=1;

/*
  d_t P = D \laplac P +  AP +  BP +  CP - P 
  d_t A = D \laplac A + A(1-A) - AP - AB - AC
  d_t B = D \laplac B + B(1-B) - BP - BA - BC
  d_t C = D \laplac D + C(1-C) - CP - CA - CB
 */


void initStructure(){
  int ii;
  double coef=1.0;
  double gamma=1;
  double alpha=0.5;
  double D=5e-4;
  allocStructs();
  sprintf(OUTPUTDIR,"/home/biometrie/obonnefon/solvers/trunk/SandBox/FF/LVS2D/RESWORK/");
  for (ii=0;ii<Nwires;ii++){
//field ii pop 0 : predator C
  aFieldPop[ii].D2DX[0]=D;
  aFieldPop[ii].D2DY[0]=D;
  aFieldPop[ii].K[0]=0;
  aFieldPop[ii].R[0]=0;
  aFieldPop[ii].RIsFunc[0]=0;
  aFieldPop[ii].T[0]=0;
  aFieldPop[ii].rho[0]=0.01;
  aFieldPop[ii].Cmalthus[0]=-1;
  
  aFieldPop[ii].Ccoup[0][1]=1;
  aFieldPop[ii].Ccoup[0][2]=1;
  aFieldPop[ii].Ccoup[0][3]=1;
  aFieldPop[ii].source[0]=0.00;

  
//field ii pop 1 : H1
  aFieldPop[ii].D2DX[1]=D;
  aFieldPop[ii].D2DY[1]=D;
  aFieldPop[ii].K[1]=1;
  aFieldPop[ii].R[1]=1;
  aFieldPop[ii].RIsFunc[1]=0;
  aFieldPop[ii].T[1]=0;
  //H1 with C:
  aFieldPop[ii].Ccoup[1][0]=-1;
  //H1 with H2:
  aFieldPop[ii].Ccoup[1][2]=-1;
  //H1 with H3:
  aFieldPop[ii].Ccoup[1][3]=-1;
  
  aFieldPop[ii].Cmalthus[1]=0;
  aFieldPop[ii].source[1]=0.0;

//field ii pop 2 : H2
  aFieldPop[ii].D2DX[2]=D;
  aFieldPop[ii].D2DY[2]=D;
  aFieldPop[ii].K[2]=1;
  aFieldPop[ii].R[2]=1;
  aFieldPop[ii].RIsFunc[2]=0;
  aFieldPop[ii].T[2]=0;
  //H1 with C:
  aFieldPop[ii].Ccoup[2][0]=-1;
  //H1 with H2:
  aFieldPop[ii].Ccoup[2][1]=-1;
  //H1 with H3:
  aFieldPop[ii].Ccoup[2][3]=-1;
  
  aFieldPop[ii].Cmalthus[2]=0;
  aFieldPop[ii].source[2]=0.0;

//field ii pop 3 : H3
  aFieldPop[ii].D2DX[3]=D;
  aFieldPop[ii].D2DY[3]=D;
  aFieldPop[ii].K[3]=1;
  aFieldPop[ii].R[3]=1;
  aFieldPop[ii].RIsFunc[3]=0;
  aFieldPop[ii].T[3]=0;
  //H1 with V:
  aFieldPop[ii].Ccoup[3][0]=-1;
  //H1 with H2:
  aFieldPop[ii].Ccoup[3][1]=-1;
  //H1 with H3:
  aFieldPop[ii].Ccoup[3][2]=-1;
  
  aFieldPop[ii].Cmalthus[3]=0;
  aFieldPop[ii].source[3]=0.0;



  }
 
  printFieldPops(aFieldPop,Nwires);
  
}



