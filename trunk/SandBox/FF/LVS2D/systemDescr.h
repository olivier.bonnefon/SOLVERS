#ifndef SYSTEMDESCR_H
#define SYSTEMDESCR_H


#define MAX_NB_SPECS 5
extern char SPEC_NAME[];

/*
  d_t u= D[0] laplac u + R[0]u(1-u/K[0])+T[0](u/K[0]-rho)u(1-u/K[0])+Cmalthus[0]*u+Ccoup[0] u*v + source
  d_t v= D[1] laplac v + R[1]v(1-v/K[1])+Cmalthus[1]*v+Ccoup[1] u*v+ source
 */
struct  fieldPop{
  //Warning, for de Neumann BC, Diffusion is suposed isotrop
  double D2DX[MAX_NB_SPECS];
  double D2DY[MAX_NB_SPECS];
  double K[MAX_NB_SPECS];
  double R[MAX_NB_SPECS];
  int RIsFunc[MAX_NB_SPECS];
  double T[MAX_NB_SPECS];
  double rho[MAX_NB_SPECS];
  double Cmalthus[MAX_NB_SPECS];
  double Ccoup[MAX_NB_SPECS][MAX_NB_SPECS];//line,col
  double source[MAX_NB_SPECS];
};


/*GEOMETRCICAL DESCRIPTION*/
//The number of points and its coordinates
extern int NPoints;
extern double Px[];
extern double Py[];
//The number of geometrical edges.
//If a edge is shared beteween tow fields, it the same geometrical edge.
extern int NEdges;
//The couples of points defining geometrical edges
extern int EDGES[];
//Number of wires, ie number of fields
extern int Nwires;
//Wsizes[i] is the number of edges of the field i
extern int Wsizes[];
//It contains the index of edge of the fields
extern int WIRES[];
//Number of point on edge per unit of length
extern int nPerLength;




/*DYNAMICAL POPULATION DESCRIPTION*/
//Number of species
extern int NbSpecies;
//Number of field type
//ie Number of 'struct  fieldPop' allocated
//each field point on a 'struct  fieldPop'
extern int NDynFieldType;
extern int * FieldTypes;

//The array of field type.
struct fieldPop* aFieldPop;

//just print
void printFieldPops(struct fieldPop * pMod,int n){
  int ii,jj,numspec,numspecCol;
  int numEdge=0;

  printf("BEGIN PRINT DYNAMICS POPS\n");
  printf("\tAbout 2D dynamic type, there are %i type :\n",NDynFieldType);
  for (ii=0;ii<NDynFieldType;ii++){
    printf("\t\t->2D type %i\n",ii);
   for (numspec=0;numspec<NbSpecies;numspec++){
    printf("\t\t\tD2DX=%e\n",aFieldPop[ii].D2DX[numspec]);
    printf("\t\t\tD2DY=%e\n",aFieldPop[ii].D2DY[numspec]);
    printf("\t\t\tK=%e\n",aFieldPop[ii].K[numspec]);
    if (aFieldPop[ii].RIsFunc[numspec]){
      printf("\t\t\tR is a function.\n");
    }else{
      printf("\t\t\tR=%e\n",aFieldPop[ii].R[numspec]);
    }
    printf("\t\t\tT=%e\n",aFieldPop[ii].T[numspec]);
    printf("\t\t\trho=%e\n",aFieldPop[ii].rho[numspec]);
    printf("\t\t\tCmalthus=%e\n",aFieldPop[ii].Cmalthus[numspec]);
    for (numspecCol=0;numspecCol<NbSpecies;numspecCol++)
      printf("\t\t\tCcoup=%e\n",aFieldPop[ii].Ccoup[numspec][numspecCol]);
   }
  }
  printf("\tAbout dynamics per domain \n",ii);
  for (ii=0;ii<n;ii++){
    printf("\t\tDomain 2D %i has dynamic %i\n",ii,FieldTypes[ii]);
  }
  printf("END PRINT DYNAMICS POPS\n");
}


int isCoupled(int numDom, int numSpeciesLine,int numSpeciesCol){
  if (NbSpecies<2 || numSpeciesLine==numSpeciesCol)
    return 0;
  else{
    if (aFieldPop[FieldTypes[numDom]].Ccoup[numSpeciesLine][numSpeciesCol]!=0)
      return 1;
  }
  return 0;
}

int allocStructs(){
  int ii,numSpec,numSpecCol;
  //alloc memory for each type of fields dynamique
  aFieldPop=(struct fieldPop *)calloc(NDynFieldType,sizeof(struct fieldPop));
  for (ii=0;ii<NDynFieldType;ii++){
    for ( numSpec=0;numSpec<NbSpecies;numSpec++){
      aFieldPop[ii].D2DX[numSpec]=1;
      aFieldPop[ii].D2DY[numSpec]=1;
      aFieldPop[ii].T[numSpec]=1;
      aFieldPop[ii].rho[numSpec]=0.1;
      aFieldPop[ii].RIsFunc[numSpec]=0;
      for ( numSpecCol=0;numSpecCol<NbSpecies;numSpecCol++)
        aFieldPop[ii].Ccoup[numSpec][numSpecCol]=0;
    }
  }
  //alloc and init memory for Field type
  FieldTypes=(int*)calloc(Nwires,sizeof(int));

}


int freeStructs(){
  free(FieldTypes);
  free(aFieldPop);
}

#endif
