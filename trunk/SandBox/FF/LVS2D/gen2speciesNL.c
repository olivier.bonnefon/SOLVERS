#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/*
Vh0 uc=utm10s4+utm10s5+utm10s6+utm10s7+utm10s8;
    visuScal("2D1",Th0,uc,10);
Vh0 uc=utm10s0+utm10s1+utm10s2+utm10s3;

    cout<<"M1D0s3 "<<M1D0s3<<endl;


cout<<"M1D0s1 "<<M1D0s1<<endl;
    
*/
char OUTPUTDIR[512];
char FILENAME[512];

#include "simpleUS2.h"

int curLab=1;
int LEDGES[512];
//vecteur unitaire des edges
//double *pVE;
int maxDofs1d=-1;
char caux1[512];
char caux2[512];
char caux3[512];
char caux4[512];
char SPEC_NAME[10]={'P','A','B','C','D','E','F','G','H','I'};

enum MatrixType{
  VARF2D,
  COUPLAGE_SPECIES
};

typedef struct{
  enum MatrixType type;
  int index2D;//index of domain
  int numSpecies;//index of line spec
  int numSpeciesCoupled;//idex of coupled spec
}MatrixStruct;


MatrixStruct **ppMS;
int NB_BLOCK_LINES=0;
int NB_BLOCK_LINES_PER_SPECIES=0;

void printMatrixName(FILE *pF, MatrixStruct *pMS){
  if (!pMS){
    fprintf(pF,"0");
    return;
  }
  switch (pMS->type)
  {
  case VARF2D:
    fprintf(pF,"%cM2D%i",SPEC_NAME[pMS->numSpecies],pMS->index2D);
    break;
  case COUPLAGE_SPECIES:
    fprintf(pF,"%c%cMCoup2D%i",SPEC_NAME[pMS->numSpecies],SPEC_NAME[pMS->numSpeciesCoupled],pMS->index2D);
    break;
  default :
    printf("default in printMatrixName\n");
    break;
  }
  
}

void printPoints(FILE *pF){
  double xmin=100000;
  double xmax=-100000;
  double ymin=100000;
  double ymax=-100000;
  int i;
  fprintf(pF,"//BEGIN POINTS\n");
  fprintf(pF,"int NPoints=%i;\n",NPoints);
  fprintf(pF,"real[int] Px=[\n");
  for (i=0;i<NPoints;i++){
    if (Px[i]>xmax)
      xmax=Px[i];
    if (Px[i]<xmin)
      xmin=Px[i];
    if (Py[i]>ymax)
      ymax=Py[i];
    if (Py[i]<ymin)
      ymin=Py[i];
    fprintf(pF,"\t%e",Px[i]);
    if (i<NPoints-1)
       fprintf(pF,",\n");
  }
  fprintf(pF,"];\n");
  fprintf(pF,"real[int] Py=[\n");
  for (i=0;i<NPoints;i++){
    fprintf(pF,"\t%e",Py[i]);
    if (i<NPoints-1)
       fprintf(pF,",\n");
  }
  fprintf(pF,"];\n");
  fprintf(pF,"real xmin=%e;\n",xmin);
  fprintf(pF,"real xmax=%e;\n",xmax);
  fprintf(pF,"real ymin=%e;\n",ymin);
  fprintf(pF,"real ymax=%e;\n",ymax);
  fprintf(pF,"//END POINTS\n");
}

void printEdges(FILE *pF){
  double *pVE=(double*)malloc(2*NEdges*sizeof(double));
  int ii;
  fprintf(pF,"//BEGIN LABEL EDGES\n");
  for (ii=0;ii<NEdges;ii++){
    fprintf(pF,"int labE%i=%i;\n",ii,curLab);
    curLab++;
  }
  fprintf(pF,"//END LABEL EDGES\n");
  fprintf(pF,"//BEGIN EDGES\n");
  fprintf(pF,"int NEdges=%i;\n",NEdges);
  for (ii=0;ii<NEdges;ii++){
    fprintf(pF,"border EDGE%i(t=0,1){x=(1-t)*%14e + t*%14e  ; y =(1-t)*%14e + t*%14e;label= labE%i;};\n",
           ii,Px[EDGES[2*ii]],Px[EDGES[2*ii+1]],Py[EDGES[2*ii]],Py[EDGES[2*ii+1]],ii);
  }
  fprintf(pF,"//END EDGES\n");

  fprintf(pF,"//BEGIN DECL CST FOR GET 1D DOFS\n");
  fprintf(pF,"//ELi for the length oh the edge i\n");
  fprintf(pF,"//EAi for the abscisse of the normalized edge vector\n");
  fprintf(pF,"//EAi for the ordonate of the normalized edge vector\n");
  fprintf(pF,"//\n");
  for (ii=0;ii<NEdges;ii++){
    double aux=sqrt((Px[EDGES[2*ii+1]]-Px[EDGES[2*ii]])*(Px[EDGES[2*ii+1]]-Px[EDGES[2*ii]])+
                    (Py[EDGES[2*ii+1]]-Py[EDGES[2*ii]])*(Py[EDGES[2*ii+1]]-Py[EDGES[2*ii]]));
    LEDGES[ii]=(int)aux+1;
    fprintf(pF,"real EL%i=%e;\n",ii,aux);
    pVE[2*ii]=(Px[EDGES[2*ii+1]]-Px[EDGES[2*ii]])/aux;
    pVE[2*ii+1]=(Py[EDGES[2*ii+1]]-Py[EDGES[2*ii]])/aux;
    fprintf(pF,"real EA%i=%e;\n",ii,(Px[EDGES[2*ii+1]]-Px[EDGES[2*ii]])/aux);
    fprintf(pF,"real EB%i=%e;\n",ii,(Py[EDGES[2*ii+1]]-Py[EDGES[2*ii]])/aux);
  }
  fprintf(pF,"//END DECL CST FOR GET 1D DOFS\n");
  free(pVE);
}


void printMeshes(FILE *pF){
  int ii,jj;
  int numSpecies;
  fprintf(pF,"//BEGIN MESHES\n");
  fprintf(pF,"//node per unit length\n");
  fprintf(pF,"int n=%i;\n",nPerLength);
  fprintf(pF,"int Nwires=%i;\n",Nwires);
  fprintf(pF,"int[int] Wsizes=[\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t%i",Wsizes[ii]);
    if (ii<Nwires-1)
      fprintf(pF,",\n");
  }
  fprintf(pF,"\t];\n\n");
  fprintf(pF,"int[int] WIRES=[\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"\t%i,%i",WIRES[2*ii],WIRES[2*ii+1]);
    if (ii<Nwires-1)
      fprintf(pF,",\n");
  }
  fprintf(pF,"\t];\n\n");
  
//  fprintf(pF,"include \"geomMacro.edp\"\n");

  int* pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"mesh Th%i=buildmesh(\n",ii);
    for (jj=0;jj<Wsizes[ii];jj++){
      fprintf(pF,"\tEDGE%i(n*(%i))",abs(*pW),(*pW)>=0?LEDGES[*pW]:-LEDGES[-(*pW)]);
      if (nPerLength*LEDGES[abs(*pW)]>maxDofs1d)
        maxDofs1d=nPerLength*LEDGES[abs(*pW)];
      if (jj<Wsizes[ii]-1)
        fprintf(pF,"+\n");
      pW++;
    }
    fprintf(pF,",fixeborder=true);\n");
  }
  fprintf(pF,"plot(");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"Th%i",ii);
    if (ii<Nwires-1)
      fprintf(pF,",");
  }
  fprintf(pF,",wait=true);\n");
  fprintf(pF,"//END MESHES\n");
  for (ii=0;ii<Nwires;ii++)
    fprintf(pF,"fespace Vh%i(Th%i,P1);\n",ii,ii);
  for (ii=0;ii<Nwires;ii++)
    fprintf(pF,"real Surf%i=int2d(Th%i)(1.0);\n",ii,ii);
  
  for ( numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=SPEC_NAME[numSpecies];
    pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"Vh%i %c2d%i;\n",ii,spec,ii);
      fprintf(pF,"Vh%i %c2dtm1%i;\n",ii,spec,ii);
      fprintf(pF,"Vh%i %c2dk%i;\n",ii,spec,ii);
    }
  }
  
  
}
void printDofs(FILE *pF){
  int ii,jj,numSpecies;
  fprintf(pF,"\nint NbSpecies=%i;\n",NbSpecies);
  
  fprintf(pF,"//BEGIN DOFS DEF\n");
//  fprintf(pF,"int[int] zoibuff(%i);\n",maxDofs1d+2);
  int* pW=WIRES;
  fprintf(pF,"\t//BEGIN DOFS declar\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"//Omega %i\n",ii);
    fprintf(pF,"int n2Ddofs%i=Vh%i.ndof;\n",ii,ii);
  }
  fprintf(pF,"\t//END DOFS declar\n");
  pW=WIRES;
  fprintf(pF,"real[int] sol(%i*(",NbSpecies);
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"n2Ddofs%i",ii);
    if (ii<Nwires-1)
      fprintf(pF,"+");
  }
  fprintf(pF,"));\n");

  pW=WIRES;
  fprintf(pF,"real[int] rhs(%i*(",NbSpecies);
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"n2Ddofs%i",ii);
      if (ii<Nwires-1)
      fprintf(pF,"+");
  }
  fprintf(pF,"));\n");
  fprintf(pF,"\n");
  for ( numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"real[int] rhs%c2d%i(n2Ddofs%i);\n",SPEC_NAME[numSpecies],ii,ii);
    }
  fprintf(pF,"\n");
  }
  fprintf(pF,"//END DOFS DEF\n");
  fprintf(pF,"\n");
}

void printVarfs(FILE *pF){
  int ii,jj,numSpecies;
  int* pW=WIRES;
  fprintf(pF,"//BEGIN VARFS\n");
  //fprintf(pF,"real D2DX=0.05;\nreal D2DY=0.05;\n");

  //fprintf(pF,"real ux0=1;\nreal ux1=0;\nreal dux1=0;\nreal dux0=0;\n");

  fprintf(pF,"//\tBEGIN MATRIX DEC\n");
  fprintf(pF,"matrix A,MM,Maux;\n");
//  fprintf(pF,"real D1D=10.0;\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    for (ii=0;ii<Nwires;ii++)
      fprintf(pF,"matrix %cM2D%i;\n",SPEC_NAME[numSpecies],ii);
  }
  pW=WIRES;
  
  fprintf(pF,";\n");
  int numSpeciesCol=0;
  fprintf(pF,"//couplage UV matrices\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=SPEC_NAME[numSpecies];
    for (ii=0;ii<Nwires;ii++){
      for ( numSpeciesCol=0;numSpeciesCol<NbSpecies;numSpeciesCol++){
        if (isCoupled(ii,numSpecies,numSpeciesCol)){
          fprintf(pF,"matrix %c%cMCoup2D%i;\n",spec,SPEC_NAME[numSpeciesCol],ii);
        }
      }
    }
  }
  
  fprintf(pF,"//\tEND MATRIX DEC\n\n");

  fprintf(pF,"\n//\tbegin 2D varf\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    pW=WIRES;
    int nedge=0;
//    int curNedge=0;
    char spec=SPEC_NAME[numSpecies];
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"//VF 2D %c field %i\n",spec,ii);
      sprintf(caux1,"R%c%i",spec,ii);
      sprintf(caux2,"K%c%i",spec,ii);
      sprintf(caux3,"T%c%i",spec,ii);
      sprintf(caux4,"rho%c%i",spec,ii);
        
      fprintf(pF,"Vh%i D2X%c%i=%e;\n",ii,spec,ii,aFieldPop[FieldTypes[ii]].D2DX[numSpecies]);        
      fprintf(pF,"Vh%i D2Y%c%i=%e;\n",ii,spec,ii,aFieldPop[FieldTypes[ii]].D2DY[numSpecies]);
      if (aFieldPop[FieldTypes[ii]].RIsFunc[numSpecies]){
        fprintf(pF,"Vh%i %s=%s%c%i(x,y);\n",ii,caux1,"fctR",spec,ii);;
      }else{
        fprintf(pF,"Vh%i %s=%e;\n",ii,caux1,aFieldPop[FieldTypes[ii]].R[numSpecies]);
      }
      fprintf(pF,"Vh%i %s=%e;\n",ii,caux2,aFieldPop[FieldTypes[ii]].K[numSpecies]);
      fprintf(pF,"Vh%i %s=%e;\n",ii,caux3,aFieldPop[FieldTypes[ii]].T[numSpecies]);
      fprintf(pF,"Vh%i %s=%e;\n",ii,caux4,aFieldPop[FieldTypes[ii]].rho[numSpecies]);
      fprintf(pF,"Vh%i S%c%i=%e;\n",ii,spec,ii,aFieldPop[FieldTypes[ii]].source[numSpecies]);
      fprintf(pF,"Vh%i Mal%c%i=%e;\n",ii,spec,ii,aFieldPop[FieldTypes[ii]].Cmalthus[numSpecies]);
      for ( numSpeciesCol=0;numSpeciesCol<NbSpecies;numSpeciesCol++)
        if (isCoupled(ii,numSpecies,numSpeciesCol))
          fprintf(pF,"Vh%i C%c%c%i=%e;\n",ii,spec,SPEC_NAME[numSpeciesCol],ii,aFieldPop[FieldTypes[ii]].Ccoup[numSpecies][numSpeciesCol]);
      if (aFieldPop[FieldTypes[ii]].R[numSpecies]!=0){
        fprintf(pF,"varf varfMat%c2D%i(v,w)=",spec,ii);
        fprintf(pF,"int2d(Th%i)(tm*2*(%s/%s)*%c2dk%i*v*w-tm*%s*v*w)+\n\t",
                ii,caux1,caux2,spec,ii,caux1);
      }else{
        fprintf(pF,"varf varfMat%c2D%i(v,w)=",spec,ii);
      }
      //Alle effect
      if (aFieldPop[FieldTypes[ii]].T[numSpecies]!=0){
        fprintf(pF,"int2d(Th%i)(tm*%s*(-2)*(%s+1)*%c2dk%i*v*w/%s+tm*%s*3*%c2dk%i*%c2dk%i*v*w/(%s*%s)+tm*%s*%s*v*w)+\n\t",
                ii,caux3,
                caux4,
                spec,ii,caux2,caux3,
                spec,ii,spec,ii,caux2,caux2,caux3,caux4);

      }
      for ( numSpeciesCol=0;numSpeciesCol<NbSpecies;numSpeciesCol++){
        if (isCoupled(ii,numSpecies,numSpeciesCol)){
          fprintf(pF,"int2d(Th%i)(tm*(-1.0*C%c%c%i)*%c2dk%i*v*w)+\n\t",ii,spec,SPEC_NAME[numSpeciesCol],ii,
                  SPEC_NAME[numSpeciesCol],ii);
        }
      }
      
      if (aFieldPop[FieldTypes[ii]].Cmalthus[numSpecies]!=0){
        fprintf(pF,"int2d(Th%i)(tm*(-1.0)*Mal%c%i*v*w)+\n\t",ii,spec,ii);
      }
      
      fprintf(pF,"int2d(Th%i)(alpha*v*w+tm*D2X%c%i*dx(v)*dx(w)+tm*D2Y%c%i*dy(v)*dy(w));\n\t",
              ii,
              spec,ii,spec,ii);
       
      fprintf(pF,"\n");
      fprintf(pF,"varf varfRhs%c2D%i(v,w)=\n\t",spec,ii);
      fprintf(pF," int2d(Th%i)(\n\t\talpha*%c2dtm1%i*w",ii,spec,ii);
      for ( numSpeciesCol=0;numSpeciesCol<NbSpecies;numSpeciesCol++){
        if (isCoupled(ii,numSpecies,numSpeciesCol)){
          fprintf(pF,"+\n\t\ttm*(-1*C%c%c%i)*%c2dk%i*%c2dk%i*w",
                  spec,SPEC_NAME[numSpeciesCol],ii,
                  spec,ii,SPEC_NAME[numSpeciesCol],ii);
        }
      }
      if (aFieldPop[FieldTypes[ii]].R[numSpecies]!=0){
        fprintf(pF,"+\n\t\ttm*(%s/%s)*%c2dk%i*%c2dk%i*w",
                caux1,caux2,spec,ii,spec,ii);

      }
      if (aFieldPop[FieldTypes[ii]].T[numSpecies]!=0){
        fprintf(pF,"+\n\t\ttm*%s*%c2dk%i*%c2dk%i*(2*%c2dk%i/%s-%s-1)*w/%s",
                caux3,spec,ii,spec,ii,spec,ii,caux2,caux4,
                caux2);
      }
      fprintf(pF,"+\n\t\t(S%c%i)*w)",spec,ii);
#define WITH_THETA_METHOD
#ifdef WITH_THETA_METHOD
      fprintf(pF,"+\n",spec,ii);
      fprintf(pF,"\tint2d(Th%i)(\n",ii);
      fprintf(pF,"\t\tunmtm*D2X%c%i*dx(%c2dtm1%i)*dx(w)+unmtm*D2Y%c%i*dy(%c2dtm1%i)*dy(w)+\n",spec,ii,spec,ii,spec,ii,spec,ii);
      if (aFieldPop[FieldTypes[ii]].T[numSpecies]!=0){
        fprintf(pF,"\t\tunmtm*%s*(%c2dtm1%i/%s-%s)*%c2dtm1%i*(1-%c2dtm1%i/%s)*w+\n",
                caux3,spec,ii,caux2,caux4,
                spec,ii,spec,ii,caux2);
 
      }
      if (aFieldPop[FieldTypes[ii]].R[numSpecies]!=0){
        fprintf(pF,"\t\tunmtm*R%c%i*%c2dtm1%i*(1-%c2dtm1%i/K%c%i)*w+\n",spec,ii,spec,ii,spec,ii,spec,ii);
      }
      fprintf(pF,"\t\tunmtm*Mal%c%i*%c2dtm1%i*w",spec,ii,spec,ii);
      for ( numSpeciesCol=0;numSpeciesCol<NbSpecies;numSpeciesCol++){
        if (isCoupled(ii,numSpecies,numSpeciesCol)){
          fprintf(pF,"+\n\t\tunmtm*C%c%c%i*%c2dtm1%i*%c2dtm1%i*w",spec,SPEC_NAME[numSpeciesCol],ii,spec,ii,SPEC_NAME[numSpeciesCol],ii);
        }
      }
      fprintf(pF,")");      
#endif
      fprintf(pF,";\n");
#ifndef WITH_THETA_METHOD
      fprintf(pF,"if (tm!=1){\n");
      fprintf(pF,"\tcout<<\"theta method not allow\"<<endl;\n");
      fprintf(pF,"\texit(1);\n}\n");
#endif
      
    }
  }
  fprintf(pF,"//\tend 2D  varf\n");

  fprintf(pF,"\n//\tbegin 2D couplage varf\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=SPEC_NAME[numSpecies];
    for (ii=0;ii<Nwires;ii++){
      for ( numSpeciesCol=0;numSpeciesCol<NbSpecies;numSpeciesCol++){
        if (isCoupled(ii,numSpecies,numSpeciesCol)){
          fprintf(pF,"varf varfMat%c%cCoup2D%i(v,w)=",spec,SPEC_NAME[numSpeciesCol],ii);
          fprintf(pF," int2d(Th%i)(tm*(-1.0*C%c%c%i)*%c2dk%i*v*w);\n",
                  ii,
                  spec,SPEC_NAME[numSpeciesCol],ii,
                  SPEC_NAME[numSpecies],ii);
          
        }
      }
    }
  }
  fprintf(pF,"\n//\tend 2D couplage varf\n\n");

  
  fprintf(pF,"\n");

  
  fprintf(pF,"\n");
  fprintf(pF,"\n");
  fprintf(pF,"//END VARFS\n");
 
}
//void printMatCoup(FILE *pF,int);
void printMatStruct(FILE *pF){
  int ii, jj;
  fprintf(pF,"matrix M=[\n");
  for(ii=0;ii<NB_BLOCK_LINES;ii++){
    fprintf(pF,"[");
    for(jj=0;jj<NB_BLOCK_LINES;jj++){
      printMatrixName(pF,ppMS[ii+jj*NB_BLOCK_LINES]);
      if (jj<NB_BLOCK_LINES-1)
        fprintf(pF,",\t");
    }
    fprintf(pF,"]");
    if(ii<NB_BLOCK_LINES-1)
      fprintf(pF,",");
    fprintf(pF,"\n");
  }
  fprintf(pF,"];\n");
}
//void printMatStruct2(FILE *pF);
void buildMat(FILE *pF){
  int ii,numSpecies,numSpeciesCol;
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"%cM2D%i=varfMat%c2D%i(Vh%i,Vh%i);\n",SPEC_NAME[numSpecies],ii,SPEC_NAME[numSpecies],ii,ii,ii);
    }
  }
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=SPEC_NAME[numSpecies];
    for (ii=0;ii<Nwires;ii++){
      for ( numSpeciesCol=0;numSpeciesCol<NbSpecies;numSpeciesCol++){
        if (isCoupled(ii,numSpecies,numSpeciesCol)){
          fprintf(pF,"%c%cMCoup2D%i=varfMat%c%cCoup2D%i(Vh%i,Vh%i);\n",
                  SPEC_NAME[numSpecies],SPEC_NAME[numSpeciesCol],ii,
                  SPEC_NAME[numSpecies],SPEC_NAME[numSpeciesCol],ii,ii,ii);
        }
      }
    }
  }
  printMatStruct(pF);
  
//  fprintf(pF,"matrix M;\n");
  
  fprintf(pF,"set(M,solver=GMRES);\n");

}
/*void printMat(FILE *pF){
  int ii,jj,kk,numSpecies;
  int* pWsav,*pW=WIRES;
  
  
  
  fprintf(pF,"//BEGIN MAT DEF\n");
  fprintf(pF,"\n");
  fprintf(pF,"int curIndexBlock=0;\n");
  pW=WIRES;
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"curIndexBlock+=n2Ddofs%i;\n",ii);
//    indexBeginMat[NedgesMaxPerDom*ii]=curIndexBlock;
    fprintf(pF,"\n");
  }

  
//  buildMat(pF);
  fprintf(pF,"//END MAT DEF\n");
  }*/




void resetMatStruct(){
  int ii, jj;
  for(ii=0;ii<NB_BLOCK_LINES;ii++)
    for(jj=0;jj<NB_BLOCK_LINES;jj++)
      if (ppMS[ii+jj*NB_BLOCK_LINES])
        free(ppMS[ii+jj*NB_BLOCK_LINES]);
  free(ppMS);
}

// void initMatStruct(){
//   int ii, jj;
//   pEdgeIsShared=(int*)calloc(NEdges,sizeof(int));
  
//   NB_BLOCK_LINES=Nwires;
//   for (ii=0;ii<Nwires;ii++)
//     NB_BLOCK_LINES+=Wsizes[ii];
//   ppMS=( MatrixStruct**)calloc(NB_BLOCK_LINES*NB_BLOCK_LINES,sizeof( MatrixStruct**));

//   //les blocs 2D
//   int curIndex=0;
//   for (ii=0;ii<Nwires;ii++){
//     ppMS[curIndex+curIndex*Nwires]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
//     ppMS[curIndex+curIndex*Nwires]->type=VARF2D;
//     ppMS[curIndex+curIndex*Nwires]->index2D1=ii;
//     curIndex=curIndex+1+Wsizes[ii];
//   }
//   //le blocs 1D
//   curIndex=0;
//   int *pW=WIRES;
//   for (ii=0;ii<Nwires;ii++){
//     curIndex++;
//     for (jj=0;jj<Wsizes[ii];jj++){
//       ppMS[curIndex+curIndex*Nwires]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
//       ppMS[curIndex+curIndex*Nwires]->type=VARF1D;
//       ppMS[curIndex+curIndex*Nwires]->index2D1=ii;
//       ppMS[curIndex+curIndex*Nwires]->index1D1=abs(*pW);
//       pW++;
//       curIndex++;
//       if ((*pW)<0){
//         pEdgeIsShared[abs(*pW)]=1;
//       }
//     }
//   }
//   //1D --> 2D
//   int line2D=0;
//   int curCol=0;
//   pW=WIRES;
//   for (ii=0;ii<Nwires;ii++){
//     curCol++;
//     for (jj=0;jj<Wsizes[ii];jj++){
//       ppMS[line2D+curCol*Nwires]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
//       ppMS[line2D+curCol*Nwires]->type=COUPLAGE_1Dto2D;
//       ppMS[line2D+curCol*Nwires]->index2D1=ii;
//       ppMS[line2D+curCol*Nwires]->index1D1=abs(*pW);
//       curCol++;
//       pW++;
//     }
//     line2D=line2D+1+Wsizes[ii];
//   }

//   //2D --> 1D
//   int col2D=0;
//   int curLine=0;
//   pW=WIRES;
//   for (ii=0;ii<Nwires;ii++){
//     curLine++;
//     for (jj=0;jj<Wsizes[ii];jj++){
//       ppMS[curLine+col2D*Nwires]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
//       ppMS[curLine+col2D*Nwires]->type=COUPLAGE_2Dto1D;
//       ppMS[curLine+col2D*Nwires]->index2D1=ii;
//       ppMS[curLine+col2D*Nwires]->index1D1=abs(*pW);
//       curLine++;
//       pW++;
//     }
//     col2D=col2D+1+Wsizes[ii];
//   }

//   //couplage edge
//   /*int line1D=0;
//   curCol=0;
//   line2D=0;
//   pW=WIRES;
//   for (ii=0;ii<Nwires;ii++){
//     line1D++;
//     int prev1D=Wsizes[ii]-1;
//     int next1D=1;
//     for (jj=0;jj<Wsizes[ii];jj++){
//       curCol=line2D+1+prev1D;
//       ppMS[line1D+curCol*Nwires]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
//       ppMS[line1D+curCol*Nwires]->type=COUPLAGE_EDGE;
//       ppMS[line1D+curCol*Nwires]->index2D1=ii;
//       ppMS[line1D+curCol*Nwires]->index1D1=abs(pW[jj]);
//       ppMS[line1D+curCol*Nwires]->index1D2=abs(pW[prev1D]);


//       curCol=line2D+1+next1D;
//       ppMS[line1D+curCol*Nwires]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
//       ppMS[line1D+curCol*Nwires]->type=COUPLAGE_EDGE;
//       ppMS[line1D+curCol*Nwires]->index2D1=ii;
//       ppMS[line1D+curCol*Nwires]->index1D1=abs(pW[jj]);
//       ppMS[line1D+curCol*Nwires]->index1D2=abs(pW[next1D]);
      
//       line1D++;
//       prev1D=(prev1D+1)%Wsizes[ii];
//       next1D=(next1D+1)%Wsizes[ii];
//     }
//     pW=pW+Wsizes[ii];
//     line2D=line2D+1+Wsizes[ii];
   
//   }*/
  
//   //couplage domain
//   int *pWneg=WIRES;
//   int lineNeg=0;
//   int NumNegWire,NumNegEdge,NumPosWire,NumPosEdge;
  
//   for (NumNegWire=0;NumNegWire<Nwires;NumNegWire++){
//     lineNeg++;
//     for (NumNegEdge=0;NumNegEdge<Wsizes[NumNegWire];NumNegEdge++){
//       if (*pWneg<0){
//         int colPos=0;
//         int *pWpos=WIRES;
//         for (NumPosWire=0;NumPosWire<Nwires;NumPosWire++){
//           colPos++;
//           for (NumPosEdge=0;NumPosEdge<Wsizes[NumPosWire];NumPosEdge++){
//             if ((*pWpos)==abs(*pWneg)){
//               ppMS[colPos+lineNeg*Nwires]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
//               ppMS[colPos+lineNeg*Nwires]->type=COUPLAGE_DOMAIN;
//               ppMS[colPos+lineNeg*Nwires]->index2D1=NumNegWire;
//               ppMS[colPos+lineNeg*Nwires]->index1D1=abs(*pWneg);
//               ppMS[colPos+lineNeg*Nwires]->index2D2=NumPosWire;
//               ppMS[colPos+lineNeg*Nwires]->index1D2=*pWpos;


//               ppMS[lineNeg+colPos*Nwires]=( MatrixStruct *)malloc(sizeof( MatrixStruct));
//               ppMS[lineNeg+colPos*Nwires]->type=COUPLAGE_DOMAIN;
//               ppMS[lineNeg+colPos*Nwires]->index2D2=NumNegWire;
//               ppMS[lineNeg+colPos*Nwires]->index1D2=abs(*pWneg);
//               ppMS[lineNeg+colPos*Nwires]->index2D1=NumPosWire;
//               ppMS[lineNeg+colPos*Nwires]->index1D1=*pWpos;


              
//               NumPosEdge=Wsizes[NumPosWire];
//               NumPosWire=Nwires;
//             }
//             colPos++;
//             pWpos++;
//           }
//         }

//       }
//       lineNeg++;
//       pWneg++;
//     }
//   }
// }


/*

  Dofs structure:

 *<----dofs SPEC_NAME[0]----->...<----------------dofs SPEC_NAME[numSpecies]------------------>...<------dofs SPEC_NAME[NbSpecies]-------->
 *...............................<----Omega 0--->..<------Omega ii---->..<----Omega Nwires---->...........................................

NB_BLOCK_LINES number of block per line
 
 */



void initMatStruct(){
  int ii, jj,numSpecies;
  int nbBlockLines,nbBlockCols;
  MatrixStruct * pAux=0;
  NB_BLOCK_LINES_PER_SPECIES=Nwires;
  NB_BLOCK_LINES=NB_BLOCK_LINES_PER_SPECIES*NbSpecies;
//  nbBlockLines=NbSpecies*Nwires;
//  nbBlockCols=NbSpecies*Nwires;
  ppMS=( MatrixStruct**)calloc(NB_BLOCK_LINES*NB_BLOCK_LINES,sizeof( MatrixStruct**));
  printf("initMatStruct: NB_BLOCK_LINES=%i\n",NB_BLOCK_LINES);

  //colums of blocks about numSpecies
  //<--------dofs SPEC_NAME[0]------->...<---------dofs SPEC_NAME[numSpecies]----------->...
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    //les blocs 2D
    int offsetSpecies=numSpecies*NB_BLOCK_LINES_PER_SPECIES;
    int curIndex=offsetSpecies;
    //colums per domain
    //<------------------------------dofs SPEC_NAME[numSpecies]------------------------------->
    //<-----Omega 0-------->..<-----Omega ii-------->...
    for (ii=0;ii<Nwires;ii++){
      pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));
      ppMS[curIndex+curIndex*NB_BLOCK_LINES]=pAux;
      pAux->type=VARF2D;
      pAux->index2D=ii;
      pAux->numSpecies=numSpecies;
      
      //Infulence of SPEC_NAME[numSpecies] in domain Omega ii for other spec
      int colBlockIndex=curIndex;
      int numSpeciesLin;
      printf("initMatStruct: col=%i\n",colBlockIndex);
      for ( numSpeciesLin=0;numSpeciesLin<NbSpecies;numSpeciesLin++){
        int linBlockIndex=NB_BLOCK_LINES_PER_SPECIES*numSpeciesLin+ii;
        printf("initMatStruct: lin=%i\n",linBlockIndex);
       if (isCoupled(ii,numSpeciesLin,numSpecies)){
          pAux=( MatrixStruct *)malloc(sizeof( MatrixStruct));

          ppMS[linBlockIndex+colBlockIndex*NB_BLOCK_LINES]=pAux;
          pAux->type=COUPLAGE_SPECIES;
          pAux->index2D=ii;
          pAux->numSpecies=numSpeciesLin;
          pAux->numSpeciesCoupled=numSpecies;
        }
      }
      curIndex=curIndex+1;
    }
  }
}




#define WITHMASSECOMPUTE
void printMassInfo(FILE *pF){
  int ii,jj,numSpecies;
  int *pW=WIRES;
  fprintf(pF,"(1)curstep ");
  int cmpinfo=2;
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=numSpecies?'v':'u';
    pW=WIRES;
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"(%i)masse2D%c%i ",cmpinfo++,spec,ii);
    }
  }
}

void printTS(FILE *pF){
  int ii,jj,numSpecies;
  int *pW=WIRES;
  
  fprintf(pF,"//BEGIN TIME STEPPING\n");
#ifdef WITHMASSECOMPUTE
  fprintf(pF,"//Reset mass file\n");
  fprintf(pF,"{ofstream pMass(OUTPUTDIR+\"mass.txt\");}\n");
  for (ii=0;ii<Nwires;ii++){
    fprintf(pF,"{ofstream pTmp(OUTPUTDIR+\"control%i.txt\");}\n",ii);
  }
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char charSpec=SPEC_NAME[numSpecies];
    fprintf(pF,"real %cmaxDensity=0;\n",charSpec);
    fprintf(pF,"real %cminDensity=100000;\n",charSpec);
  }
#endif
fprintf(pF,"int curStep=0;\n");
#ifdef WITHMASSECOMPUTE
 fprintf(pF,"// BEGIN FUNC TO COMPUTE MASS\n");
 fprintf(pF,"func real computeMass(){\n\t\tofstream pMass(OUTPUTDIR+\"mass.txt\",append);\n");
 fprintf(pF,"\t\tpMass<<curStep<<\" \";\n");
 for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
   char spec=SPEC_NAME[numSpecies];
   fprintf(pF,"\t\treal masse2D%c=0;\n",spec);
 }
 
 for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
   char spec=SPEC_NAME[numSpecies];
   pW=WIRES;
   for (ii=0;ii<Nwires;ii++){
     fprintf(pF,"\t\t//compute 2D extremal density\n");
     fprintf(pF,"\t\t//compute 2D masse of %c in domain %i\n",spec,ii);
     fprintf(pF,"\t\tmasse2D%c=int2d(Th%i)(%c2dtm1%i);\n",spec,ii,spec,ii);
     fprintf(pF,"\t\tpMass<<masse2D%c<<\" \";\n",spec);
   }
 }
 fprintf(pF,"\t\tpMass<<endl;\n}\n");
 fprintf(pF,"// END FUNC TO COMPUTE MASS\n");
#endif
 fprintf(pF,"// BEGIN FUNC FOR VISU\n");
 fprintf(pF,"func real visuSpecies(){\n");
   for (numSpecies=0; numSpecies<NbSpecies; numSpecies++){
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"\t\tvisuScal(\"2D%i\",Th%i,%c2dtm1%i,10);\n",ii,ii,SPEC_NAME[numSpecies],ii);
    }
  }
 fprintf(pF,"}\n");
 fprintf(pF,"// END FUNC FOR VISU\n");
 
#ifdef WITHMASSECOMPUTE
 fprintf(pF,"computeMass();\n");
 fprintf(pF,"visuSpecies();\n");
#endif
  fprintf(pF,"for ( curStep=1;curStep<=nbSteps;curStep++){\n");
  fprintf(pF,"\tinclude \"updateParam.edp\";\n");
  fprintf(pF,"\treal criteron=1;\n");
  fprintf(pF,"\treal residu=criteron+1;\n");
  fprintf(pF,"\tint NewtonIt=0;\n");
  fprintf(pF,"\tint minNewtonIt=1;\n");
  fprintf(pF,"\tint maxNewtonIt=10;\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char charSpec=SPEC_NAME[numSpecies];
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"\t%c2dk%i=%c2dtm1%i;\n",charSpec,ii,charSpec,ii);
    }
  }
  fprintf(pF,"\treal dampNR=1.0;\n");
  fprintf(pF,"\twhile((residu>criteron || NewtonIt<minNewtonIt) && NewtonIt<maxNewtonIt){\n");
  fprintf(pF,"\t\t NewtonIt+=1;\n");
  fprintf(pF,"\t\t residu=0;\n");
  fprintf(pF,"\t\t//build rhs 2D\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    for(ii=0;ii<Nwires;ii++){
      fprintf(pF,"\t\trhs%c2d%i=varfRhs%c2D%i(0,Vh%i);\n",SPEC_NAME[numSpecies],ii,SPEC_NAME[numSpecies],ii,ii);
    }
  }
  
  fprintf(pF,"\t\trhs=[");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    pW=WIRES;
    for(ii=0;ii<Nwires;ii++){
      fprintf(pF,"rhs%c2d%i",SPEC_NAME[numSpecies],ii);
      if (ii<Nwires-1 || numSpecies<NbSpecies-1)
        fprintf(pF,",");
    }
  }
  
  fprintf(pF,"];\n");
  fprintf(pF,"\t\tinclude \"buildMatGen.edp\";\n");
  
  fprintf(pF,"\t\tsol=M^-1*rhs;\n");
  fprintf(pF,"\t\t    for (int ii=0;ii<sol.n;ii++)\n");
  fprintf(pF,"\t\t    if (sol[ii]<0)\n");
  fprintf(pF,"\t\t      sol[ii]=0;\n");

  
  fprintf(pF,"\t\tint curDof=0;\n");
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    pW=WIRES;
    char spec=SPEC_NAME[numSpecies];
    for (ii=0;ii<Nwires;ii++){
      fprintf(pF,"\t\t%c2d%i[]=sol(curDof:curDof+n2Ddofs%i-1);\n",spec,ii,ii);
      fprintf(pF,"\t\t%c2d%i[]-=%c2dk%i[];\n",spec,ii,spec,ii);
      fprintf(pF,"\t\tresidu=residu+%c2d%i[].l2;\n",spec,ii,spec,ii);
      fprintf(pF,"\t\t%c2dk%i[]=dampNR*sol(curDof:curDof+n2Ddofs%i-1)+(1-dampNR)*%c2dk%i[];\n",spec,ii,ii,spec,ii);
      fprintf(pF,"\t\tcurDof=curDof+n2Ddofs%i;\n",ii);

    }
    fprintf(pF,"\n");
  }
  fprintf(pF,"\t\tcout<<NewtonIt<<\" Newton residu=\"<<residu<<endl;\n");
  fprintf(pF,"\t}\n");
 fprintf(pF,"\tcout<<\"step=\"<<curStep<<\" Newton CV iteration=\"<<NewtonIt<<\" residu=\"<<residu<< endl;\n");
 for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
   char spec=SPEC_NAME[numSpecies];
   for (ii=0;ii<Nwires;ii++){
     fprintf(pF,"\t%c2d%i=%c2dk%i;\n",spec,ii,spec,ii);
   }
 }
 for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
   char spec=SPEC_NAME[numSpecies];
   for (ii=0;ii<Nwires;ii++){
     fprintf(pF,"\t%c2dtm1%i=%c2d%i;\n",spec,ii,spec,ii);
   }
 }


#ifdef WITHMASSECOMPUTE
 fprintf(pF,"\tcomputeMass();\n");
#endif
  
  fprintf(pF,"\tif (curStep%%freqVtk == 0){\n");

   for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
     char spec=SPEC_NAME[numSpecies];
     pW=WIRES;
     for (ii=0;ii<Nwires;ii++){
   
       fprintf(pF,"\t\tsavevtk(OUTPUTDIR+\"D2/%c%is\"+string(curStep)+\".vtk\",Th%i,%c2d%i,dataname=\"UU\");\n",spec,ii,ii,spec,ii);
     }
   }

  fprintf(pF,"\t}\n");

  fprintf(pF,"\tif (curStep%%freqAff == 0){\n");
  fprintf(pF,"\t\tvisuSpecies();\n");
  
  fprintf(pF,"\t}\n");

  
  fprintf(pF,"\t\n");
  fprintf(pF,"\t\n");

  fprintf(pF,"}\n");
  fprintf(pF,"//END TIME STEPPING\n");
  
}

void printViewVtk(){
  int ii,kk;
//  char filename[256];
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"view.sh");
  FILE *pmain=fopen(FILENAME, "w");
  
  for(kk=0; kk<10;kk++){
//    sprintf(filenameFILENAME,"view%i.py",kk);
    sprintf(FILENAME,"%sview%i.py",OUTPUTDIR,kk);
    fprintf(pmain,"python %s\n",FILENAME);
    FILE *pVtk = fopen(FILENAME, "w");
    fprintf(pVtk,"from paraview.simple import *\n");
    fprintf(pVtk,"import sys\nbaseName=\"/home/olivierb/solvers/trunk/SandBox/FF/LVS2D/u\"\n");
    fprintf(pVtk,"freq=10\n");
    fprintf(pVtk,"minv=0.0003\n");
    fprintf(pVtk,"maxv=0.0005\n");

    fprintf(pVtk,"for num in range(%i,%i):\n",kk*20+1,(kk+1)*20+1);
    for(ii=0;ii<Nwires;ii++){
      fprintf(pVtk,"\tfileName%i=baseName+str(%i)+str(freq*num)+\".vtk\"\n",ii,ii);
      fprintf(pVtk,"\treader%i = OpenDataFile(fileName%i)\n",ii,ii);
    }
    fprintf(pVtk,"\tShow()\n");
    fprintf(pVtk,"\tview = GetActiveView()\n\tview.CameraPosition = [0,0,100]\n\tview.ViewSize = [1000, 1000]\n\t#view.CameraViewAngle = 90\n\t#Render()\n");

    for(ii=0;ii<Nwires;ii++){
      fprintf(pVtk,"\tdp%i = GetDisplayProperties(reader%i)\n",ii,ii);
      fprintf(pVtk,"\tdp%i.Representation = 'Surface'\n",ii);
      fprintf(pVtk,"\telev%i = Elevation(reader%i)\n",ii,ii);
      fprintf(pVtk,"\tdp%i.LookupTable = MakeBlueToRedLT(minv, maxv)\n",ii);
      fprintf(pVtk,"\tdp%i.ColorArrayName = 'UU'\n",ii);
    }
    fprintf(pVtk,"\t#Render()\n");
    fprintf(pVtk,"\tWriteImage(baseName+str(freq*num)+\".png\")\n");
    for(ii=0;ii<Nwires;ii++){
      fprintf(pVtk,"\tDelete(reader%i)\n\tdel reader%i\n",ii,ii);
    }
    fprintf(pVtk,"\tdel view");
    fclose(pVtk);
  }
  fclose(pmain);
}

void printVtkViewWires(){
  int ii,kk,numEdge;
  char filename[256];
  int *pW;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"viewWires.py");
  FILE *pmain=fopen(FILENAME, "w");
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"buildActorWires.py");
  FILE *pbuildActors=fopen(FILENAME, "w");
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"addActorWires.py");
  FILE *paddActors=fopen(FILENAME, "w");
  fprintf(pmain,"import vtk\n");
  fprintf(pmain,"execfile(\"buildActorWires.py\")\n");
  
  fprintf(pbuildActors,"points = vtk.vtkPoints()\n");
  fprintf(pbuildActors,"points.SetNumberOfPoints(%i)\n",NPoints);
  fprintf(pmain,"ren = vtk.vtkRenderer()\n");
  fprintf(pmain,"execfile(\"addActorWires.py\")\n");
  fprintf(pbuildActors,"RLine=1\nGLine=1\nBLine=1\nwidthLine=2.5\n");
  fprintf(pbuildActors,"\n");
  for(ii=0;ii<NPoints;ii++){
    fprintf(pbuildActors,"points.SetPoint(%i, %e, %e, 0.1)\n",ii,Px[ii],Py[ii]);
  }
  fprintf(pbuildActors,"\n");
  
  pW=WIRES;
  for(ii=0;ii<Nwires;ii++){
    fprintf(pbuildActors,"lines%i = vtk.vtkCellArray()\n",ii);
    fprintf(pbuildActors,"lines%i.InsertNextCell(%i)\n",ii,Wsizes[ii]+1);
    numEdge=abs(*pW);
    if (*pW>=0)
      fprintf(pbuildActors,"lines%i.InsertCellPoint(%i);\n",ii,EDGES[2*(numEdge)]);
    else
      fprintf(pbuildActors,"lines%i.InsertCellPoint(%i);\n",ii,EDGES[2*(numEdge)+1]);
      
    for(kk=0;kk<Wsizes[ii];kk++){
      numEdge=abs(*pW);
      if (*pW>=0){
        fprintf(pbuildActors,"lines%i.InsertCellPoint(%i);\n",ii,EDGES[2*(numEdge)+1]);
      }else
        fprintf(pbuildActors,"lines%i.InsertCellPoint(%i);\n",ii,EDGES[2*(numEdge)]);
      pW++;
    }
    fprintf(pbuildActors,"\n");
    fprintf(pbuildActors,"polygon%i = vtk.vtkPolyData()\n",ii);
    fprintf(pbuildActors,"polygon%i.SetPoints(points)\n",ii);
    fprintf(pbuildActors,"polygon%i.SetLines(lines%i)\n",ii,ii);
    fprintf(pbuildActors,"\n");

    fprintf(pbuildActors,"\n");
    
    fprintf(pbuildActors,"polygonMapper%i = vtk.vtkPolyDataMapper()\n",ii);
    fprintf(pbuildActors,"if vtk.VTK_MAJOR_VERSION <= 5:\n");
    fprintf(pbuildActors,"\tpolygonMapper%i.SetInputConnection(polygon%i.GetProducerPort())\n",ii,ii);
    fprintf(pbuildActors,"else:\n");
    fprintf(pbuildActors,"\tpolygonMapper%i.SetInputData(polygon%i)\n",ii,ii);
    fprintf(pbuildActors,"\tpolygonMapper%i.Update()\n",ii);
    fprintf(pbuildActors,"polygonActor%i = vtk.vtkActor()\n",ii);
    fprintf(pbuildActors,"polygonActor%i.SetMapper(polygonMapper%i)\n",ii,ii);

    fprintf(paddActors,"ren.AddActor(polygonActor%i)\n",ii);
    fprintf(pbuildActors,"polygonActor%i.GetProperty().SetColor(RLine,GLine,BLine)\n",ii);
    fprintf(pbuildActors,"polygonActor%i.GetProperty().SetLineWidth(widthLine)\n",ii);
    fprintf(pbuildActors,"\n");
    fprintf(pmain,"\n");
  }
  fprintf(pmain,"ren.SetBackground(0.1, 0.2, 0.4)\n");
  fprintf(pmain,"ren.ResetCamera()\n");
  fprintf(pmain,"renWin = vtk.vtkRenderWindow()\n");
  fprintf(pmain,"renWin.AddRenderer(ren)\n");
  fprintf(pmain,"renWin.SetSize(500, 500)\n");
  fprintf(pmain,"iren = vtk.vtkRenderWindowInteractor()\niren.SetRenderWindow(renWin)\niren.Initialize()\niren.Start()\n");
  fprintf(pbuildActors,"\n");
  fprintf(pmain,"\n");
  fclose(pmain);
  fclose(pbuildActors);
  fclose(paddActors);
}
//n is a number of polygone
void printFuncexp(FILE *pF,int n){
  double x,y;
  int kk;
  int indInWIRES;
  double length;
  indInWIRES=0;
  for (kk=0;kk<n;kk++)
    indInWIRES+=Wsizes[kk];
  int iPt=EDGES[2*abs(WIRES[indInWIRES])];
  int iPt2=EDGES[2*abs(WIRES[indInWIRES])+1];
  length=(Px[iPt]-Px[iPt2])*(Px[iPt]-Px[iPt2])+(Py[iPt]-Py[iPt2])*(Py[iPt]-Py[iPt2]);
  //baricenter
  for(kk=0;kk<Wsizes[n];kk++){
    iPt=EDGES[2*abs(WIRES[indInWIRES])];
    x+=Px[iPt];
    y+=Py[iPt];
    indInWIRES++;
  }
  x=x/Wsizes[n];
  y=y/Wsizes[n];
  fprintf(pF,"//Fct %i\n",n);
  fprintf(pF,"func real fct%i(real v, real w){\n",n);
  fprintf(pF,"\tif ((%e-%c)*(%e-%c)+(%e-%c)*(%e-%c)>0.25*0.25*%e)\n",x,'v',x,'v',y,'w',y,'w',length);  
  fprintf(pF,"\t\treturn 0;\n");  
  fprintf(pF,"\treturn exp(-2.30*((%e-v)*(%e-v)+(%e-w)*(%e-w))/%e);\n",x,x,y,y,length);

  fprintf(pF,"}\n");
  fprintf(pF,"//indicatrice Fct %i\n",n);
  fprintf(pF,"func real fctI%i(real v, real w){\n",n);
  fprintf(pF,"\tif ((%e-%c)*(%e-%c)+(%e-%c)*(%e-%c)<0.25*0.25*%e)\n",x,'v',x,'v',y,'w',y,'w',length);  
  fprintf(pF,"\t\treturn 1;\n");  
  fprintf(pF,"\treturn 0;\n");

  fprintf(pF,"}\n");
  
}
//n is a number of polygone
void printFunc(FILE *pF,int n){
  double x,y;
  int kk;
  int indInWIRES;
  double length;
  indInWIRES=0;
  for (kk=0;kk<n;kk++)
    indInWIRES+=Wsizes[kk];
  int iPt=EDGES[2*abs(WIRES[indInWIRES])];
  int iPt2=EDGES[2*abs(WIRES[indInWIRES])+1];
  length=(Px[iPt]-Px[iPt2])*(Px[iPt]-Px[iPt2])+(Py[iPt]-Py[iPt2])*(Py[iPt]-Py[iPt2]);
  //baricenter
  for(kk=0;kk<Wsizes[n];kk++){
    iPt=EDGES[2*abs(WIRES[indInWIRES])];
    x+=Px[iPt];
    y+=Py[iPt];
    indInWIRES++;
  }
  x=x/Wsizes[n];
  y=y/Wsizes[n];
  fprintf(pF,"//Fct %i\n",n);
  fprintf(pF,"func real fct%i(real v, real w){\n",n);
  fprintf(pF,"\tif ((%e-%c)*(%e-%c)+(%e-%c)*(%e-%c)>0.25*0.25*%e)\n",x,'v',x,'v',y,'w',y,'w',length);  
  fprintf(pF,"\t\treturn 0;\n");  
  fprintf(pF,"\treturn exp(-2.30*((%e-v)*(%e-v)+(%e-w)*(%e-w))/%e);\n",x,x,y,y,length);

  fprintf(pF,"}\n");
  fprintf(pF,"//indicatrice Fct %i\n",n);
  fprintf(pF,"func real fctI%i(real v, real w){\n",n);
  fprintf(pF,"\treal aux=(%e-%c)*(%e-%c)+(%e-%c)*(%e-%c);\n",x,'v',x,'v',y,'w',y,'w');
  fprintf(pF,"\tif (aux<0.25*0.25*%e){\n",length);
  fprintf(pF,"\t\taux=aux/(0.25*0.25*%e);\n",length);
	fprintf(pF,"\t\t	return 1-aux;\n\t}\n");

  fprintf(pF,"\treturn 0;\n");

  fprintf(pF,"}\n");
  
}
void printInitialCond(FILE *pF){
  int numSpecies,ii,jj;
  int *pW;
  for (numSpecies=0;numSpecies<NbSpecies;numSpecies++){
    char spec=SPEC_NAME[numSpecies];
    for(ii=0;ii<Nwires;ii++){
      if (!numSpecies) printFunc(pF,ii);
      double kaux=aFieldPop[FieldTypes[ii]].K[numSpecies];
      //if (kaux!=0)
        //fprintf(pF,"%c2dtm1%i=%e*K%c%i*fct%i(x,y);\n",spec,ii,((double)rand())/((double)RAND_MAX),spec,ii,ii);
      fprintf(pF,"%c2dtm1%i=fctI%i(x,y);\n",spec,ii,ii);
        //else
        //fprintf(pF,"%c2dtm1%i=%e*0.5;\n",spec,ii,((double)rand())/((double)RAND_MAX));
    }
  }
}


void randomizePoint(double amp){
  int ii;
  for (ii=0;ii<NPoints;ii++){
    Px[ii]+=(amp*rand())/RAND_MAX;
    Py[ii]+=(amp*rand())/RAND_MAX;
  }
    
}
void printPluginVariable(FILE *pF){
  fprintf(pF,"real SourceDuration=1;\n");
  fprintf(pF,"real SourcePeriod=10;\n");
  fprintf(pF,"real TherholdPest=0.5;\n");
  fprintf(pF,"real TraiteDuration=10;\n");
  fprintf(pF,"real TraiteTRamp=10*dt;\n");
  fprintf(pF,"real Kt=0.01;\n");
  fprintf(pF,"real Knt=10;\n");
  fprintf(pF,"\n");
  int ii;
  for(ii=0;ii<Nwires;ii++){
    fprintf(pF,"//Field %i\n",ii);
    fprintf(pF,"int SourceState%i=0;\n",ii);
    fprintf(pF,"real SourceTstart%i=0.1;\n",ii);
    fprintf(pF,"real SourceTstop%i=0.1;\n",ii);
    fprintf(pF,"real SourceNextT%i=0.1;\n",ii);
    
    fprintf(pF,"int TraiteState%i=0;\n",ii);
    fprintf(pF,"real TraiteStart%i=0;\n",ii);
    fprintf(pF,"real TraiteStop%i=0;\n",ii);
    fprintf(pF,"\n");
  }
}
//just for demo
void printPlugin(FILE *pF){
  
  int ii;
  fprintf(pF,"real curT=T0+curStep*dt;\n");
  fprintf(pF,"//Control or not?\nif (1){\n",ii);
  for(ii=0;ii<Nwires;ii++){
    fprintf(pF,"//Field %i\n",ii);
    fprintf(pF,"if (TraiteState%i==0){\n",ii);
    fprintf(pF,"\treal IntegralPest=int2d(Th%i)(u2dtm1%i);\n",ii,ii,ii);
    fprintf(pF,"\treal Saux=int2d(Th%i)(1.0);\n",ii);
    fprintf(pF,"\tIntegralPest=IntegralPest/Saux;\n");
    
    fprintf(pF,"\t//declanchement du traitement?\n");
    fprintf(pF,"\tif (IntegralPest >TherholdPest ){\n");
    fprintf(pF,"\t\tcout<<\"control debut traitement %i\"<<endl;\n",ii);
    fprintf(pF,"\t\tTraiteState%i=1;\n",ii);
    fprintf(pF,"\t\tTraiteStart%i=curT ;\n",ii);
    fprintf(pF,"\t\tTraiteStop%i=curT+ TraiteDuration;\n",ii);
    fprintf(pF,"\t\tMalu%i=-1;;\n",ii);
    fprintf(pF,"\t\tofstream pTmp(OUTPUTDIR+\"control%i.txt\",append);;\n",ii);
    fprintf(pF,"\t\tpTmp<<\"traitement \"<<curT<<\" \"<<TraiteStop%i<<endl;\n",ii);
    fprintf(pF,"\t}\n");
    fprintf(pF,"}\n");

    
    fprintf(pF,"if (TraiteState%i == 1){\n",ii);
    fprintf(pF,"\tif (curT<TraiteStart%i+TraiteTRamp+0.0001){;\n",ii);
    fprintf(pF,"\t\treal taux=(TraiteStart%i+TraiteTRamp-curT)/TraiteTRamp;\n",ii);
    fprintf(pF,"\t\treal vKt=Kt*(1-taux)+Knt*taux;\n");
    fprintf(pF,"\t\tKu%i=vKt;\n",ii);
    fprintf(pF,"\t}\n");
    
    fprintf(pF,"if (curT>TraiteStop%i){\n",ii);
    fprintf(pF,"\t\tKu%i=Knt;\n",ii);
    fprintf(pF,"\t\tTraiteState%i=0;\n",ii);
    fprintf(pF,"\t\tMalu%i=0;\n",ii);
    fprintf(pF,"\t\tcout<<\"control fin traitement %i\"<<endl;\n",ii);
    fprintf(pF,"\t}\n");
    fprintf(pF,"}\n");
  }
  fprintf(pF,"}\n");

  fprintf(pF,"//Source or not?\nif (1){\n",ii);
  fprintf(pF,"real scaleSource=2;\n");
  for(ii=0;ii<Nwires;ii++){
    fprintf(pF,"//Source %i\n",ii);
    fprintf(pF,"if (SourceState%i==0){\n",ii);
    fprintf(pF,"\tif (curT>SourceNextT%i){\n",ii);
    fprintf(pF,"\t\tSourceState%i=1;\n",ii);
    fprintf(pF,"\t\tSourceTstart%i=curT;\n",ii);
    fprintf(pF,"\t\tSu%i=scaleSource*fct%i(x,y);\n",ii,ii);
    fprintf(pF,"\t\tSourceTstop%i=curT+SourceDuration*randreal1();\n",ii);
    fprintf(pF,"\t\tcout<<\"control source %i begin \"<<curT <<\" \"<<SourceTstop%i <<endl;\n",ii,ii);
    fprintf(pF,"\t\tofstream pTmp(OUTPUTDIR+\"control%i.txt\",append);\n",ii);
    fprintf(pF,"\t\tpTmp<<\"source \"<<SourceTstart%i<<\" \"<<SourceTstop%i<<endl;\n",ii,ii);
    fprintf(pF,"\t}\n");
    fprintf(pF,"}\n");

    fprintf(pF,"if (SourceState%i==1){\n",ii);
    fprintf(pF,"\tif (curT>SourceTstop%i){\n",ii);
    fprintf(pF,"\t\tSourceState%i=0;\n",ii);
    fprintf(pF,"\t\tSu%i=0;\n",ii);
    fprintf(pF,"\t\tSourceNextT%i=curT+SourcePeriod*randreal1();\n",ii);
    fprintf(pF,"\t\tcout<<\"control source %i end\"<<endl;\n",ii);
    fprintf(pF,"\t}\n");
   
    fprintf(pF,"}\n");
  }
  fprintf(pF,"}\n");

}

void printVTKGENPY(FILE *pF){
  int numspec=0;
  fprintf(pF," pyFile<<\"species=array(\'c\',\'\"");
  for (numspec=0;numspec<NbSpecies;numspec++){
    fprintf(pF," <<\"%c\"",SPEC_NAME[numspec]);
  }
  fprintf(pF," <<\"');\"<<endl;\n");
  for (numspec=0;numspec<NbSpecies;numspec++){
    fprintf(pF," pyFile<<\"%cmaxDensity=\"<<%cmaxDensity<<endl;\n",SPEC_NAME[numspec],SPEC_NAME[numspec]);
    fprintf(pF," pyFile<<\"%cminDensity=\"<<%cminDensity<<endl;\n",SPEC_NAME[numspec],SPEC_NAME[numspec]);
  }
  fprintf(pF," pyFile<<\"minV=array('d',[");
  for (numspec=0;numspec<NbSpecies;numspec++){
    fprintf(pF,"%cminDensity",SPEC_NAME[numspec]);
    if (numspec<NbSpecies-1)
      fprintf(pF,",");
  }
  fprintf(pF,"])\"<<endl;\n");
  fprintf(pF," pyFile<<\"maxV=array('d',[");
  for (numspec=0;numspec<NbSpecies;numspec++){
    fprintf(pF,"%cmaxDensity",SPEC_NAME[numspec]);
    if (numspec<NbSpecies-1)
      fprintf(pF,",");
  }
  fprintf(pF,"])\"<<endl;\n");
  fprintf(pF,"pyFile<<\"xLegend=%e\"<<endl;\n",xLegend);
  fprintf(pF,"pyFile<<\"yLegend=%e\"<<endl;\n",yLegend);
  fprintf(pF,"pyFile<<\"scaleLegend=%e\"<<endl;\n",scaleLegend);
  /*if (NbSpecies == 2){
    fprintf(pF," pyFile<<\"species=array(\'c\',\'uv\');\"<<endl;\n");
    fprintf(pF," pyFile<<\"umaxDensity=\"<<umaxDensity<<endl;\n");
    fprintf(pF," pyFile<<\"vmaxDensity=\"<<vmaxDensity<<endl;\n");
    fprintf(pF," pyFile<<\"uminDensity=\"<<uminDensity<<endl;\n");
    fprintf(pF," pyFile<<\"vminDensity=\"<<vminDensity<<endl;\n");
    fprintf(pF," pyFile<<\"minV=array('d',[uminDensity,vminDensity])\"<<endl;\n");
    fprintf(pF," pyFile<<\"maxV=array('d',[umaxDensity,vmaxDensity])\"<<endl;\n");
  }else{
    fprintf(pF," pyFile<<\"species=array(\'c\',\'u\');\"<<endl;\n");
    fprintf(pF,"  pyFile<<\"umaxDensity=\"<<umaxDensity<<endl;\n");
    fprintf(pF," pyFile<<\"uminDensity=\"<<uminDensity<<endl;\n");
    fprintf(pF," pyFile<<\"minV=array('d',[uminDensity])\"<<endl;\n");
    fprintf(pF," pyFile<<\"maxV=array('d',[umaxDensity])\"<<endl;\n");
    }*/
}

int buildDirectories(){

  struct stat st = {0};
  
  if (stat(OUTPUTDIR, &st) == -1) {
    mkdir(OUTPUTDIR, 0700);
  }
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"D2");
  if (stat(FILENAME, &st) == -1) {
    mkdir(FILENAME, 0700);
  }
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"D1");
  if (stat(FILENAME, &st) == -1) {
    mkdir(FILENAME, 0700);
  }
  sprintf(FILENAME,"%s%s","cp cas1.edp ",OUTPUTDIR);
  system(FILENAME);
  sprintf(FILENAME,"%s%s%s","cp freefem++.pref_output ",OUTPUTDIR,"freefem++.pref");
  system(FILENAME);
  sprintf(FILENAME,"%s%s","cp vtktopng.py ",OUTPUTDIR);
  system(FILENAME);
  sprintf(FILENAME,"%s%s","cp visu1D.py ",OUTPUTDIR);
  system(FILENAME);
  sprintf(FILENAME,"%s%s%s","cp defICGenRef.edp ",OUTPUTDIR,"defICGen.edp");
  system(FILENAME);
  sprintf(FILENAME,"%s%s%s","cp defPluginGenRef.edp ",OUTPUTDIR,"defPluginGen.edp");
  system(FILENAME);
  
  return 0;
}

int buildDefOutput(FILE *pF){
  fprintf(pF," string OUTPUTDIR=\"%s\";\n",OUTPUTDIR);
}


int main(){
  
  initStructure();
  buildDirectories();
  
  
  //randomizePoint(1.5);
  initMatStruct();
  printViewVtk();
  printVtkViewWires();

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defOutputGen.edp");
  FILE *pOut = fopen(FILENAME, "w");
  buildDefOutput(pOut);
  fclose(pOut);

//  return 1;
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defGeomGen.edp");
  FILE *pGeom = fopen(FILENAME, "w");
  printPoints(pGeom);
  printEdges(pGeom);
  printMeshes(pGeom);
  fclose(pGeom);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofsGen.edp");
  FILE *pDofs = fopen(FILENAME, "w");
  printDofs(pDofs);
  fclose(pDofs);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defVarfGen.edp");
  FILE *pVarfs = fopen(FILENAME, "w");
  printVarfs(pVarfs);
  fclose(pVarfs);

  /*sprintf(FILENAME,"%s%s",OUTPUTDIR,"defMatGen.edp");
  FILE *pMats = fopen(FILENAME, "w");
  printMat(pMats);
  fclose(pMats);
  */
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"buildMatGen.edp");
  FILE *pMatsBuild = fopen(FILENAME, "w");
  buildMat(pMatsBuild);
  fclose(pMatsBuild);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"timeSteppingGen.edp");
  FILE *pTimeStep = fopen(FILENAME, "w");
  printTS(pTimeStep);
  fclose(pTimeStep);

/*  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defPluginVaraibleGen.edp");
  FILE *pPlugV = fopen(FILENAME, "w");
  printPluginVariable(pPlugV);
  fclose(pPlugV);*/

  //sprintf(FILENAME,"%s%s",OUTPUTDIR,"defPluginGen.edp");
  //FILE *pPlug = fopen(FILENAME, "w");
  //printPlugin(pPlug);
  //fclose(pPlug);

/*  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defICGen.edp");
  FILE *pIC = fopen(FILENAME, "w");
  printInitialCond(pIC);
  fclose(pIC);*/

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"massInfo.txt");
  FILE *pMassInfo = fopen(FILENAME,"w");
  printMassInfo(pMassInfo);
  fclose(pMassInfo);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"vtktopngGen.edp");
  FILE *pVtkLim = fopen(FILENAME,"w");
  printVTKGENPY(pVtkLim);
  fclose(pVtkLim);
  
  resetMatStruct();
  freeStructs();
}
