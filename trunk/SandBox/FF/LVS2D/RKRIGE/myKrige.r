#library("FeedbackTS")
#library("maps")
library("sp")
library("geoR")

krige=function(coordinates,statistic,variog.param,grid,krige.param,plots=TRUE){
    input=list(coordinates=coordinates,statistic=statistic,
      variog.param=variog.param,grid=grid,krige.param=krige.param)
    ## projection and scaling of coordinates and statistic
    if(length(grid$proj)>0){
      coord.proj=project(coordinates,proj=grid$proj,degrees=grid$degrees)
      coord.proj=cbind(coord.proj$x,coord.proj$y)
    } else {
      coord.proj=coordinates
    }
    coord.scaling=variog.param$coordinates.scaling
    stat.scaling=variog.param$statistic.scaling
    coord.proj=coord.proj/coord.scaling
    statistic=statistic/stat.scaling
    variog.param$cov.pars=variog.param$cov.pars/c(stat.scaling^2,coord.scaling)
    variog.param$nugget=variog.param$nugget/stat.scaling^2
    ## variogram fitting
    aa=as.geodata(cbind(coord.proj,statistic),coords.col=1:2,data.col=3)
    maxdist=max(dist(aa$coord))*variog.param$keep.distance
    cloud1=variog(aa, option = "cloud")
    bin1=variog(aa,uvec=seq(0,maxdist,l=variog.param$nb.bin),bin.cloud = T)
    ols.n=variofit(bin1, ini.cov.pars=c(variog.param$cov.pars[1],variog.param$cov.pars[2]),
      nugget = variog.param$nugget,fix.nugget=variog.param$fix.nugget,
      weights = "equal")
    wls.n=variofit(bin1, ini.cov.pars=ols.n$cov.pars,nugget=ols.n$nugget,
      fix.nugget=variog.param$fix.nugget)
    if(plots){	
      xlab="distance"
      ylab="semivariance"
      if(length(grid$proj)>0){
        xlab=paste(xlab,"in projection space")
      }
      if(coord.scaling!=1){
        xlab=paste("scaled",xlab)
      }
      if(stat.scaling!=1){
        ylab=paste("scaled",ylab)
      }
      plot(bin1,xlab=xlab,ylab=ylab)
      lines(wls.n, lty = 1, lwd = 2, max.dist = maxdist)
      ## variogram envelopes (under randomness and under the model)
      env.mc <- variog.mc.env(aa, obj.variog = bin1)
      env.model <- variog.model.env(aa, obj.variog = bin1,model.pars = wls.n)
      plot(bin1, envelope = env.mc,xlab=xlab,ylab=ylab)
      plot(bin1, envelope = env.model,xlab=xlab,ylab=ylab)
      plot(coord.proj[,1],coord.proj[,2])
    }
    ## grid shaping
    gr=grid$coords
    in.region=0
    in.region=point.in.polygon(gr[,1],gr[,2],grid$borderPoly[,1],grid$borderPoly[,2])
    grIn=gr[in.region>0,1:2]
    if (plots){
      plot(grIn[,1],grIn[,2],main="krige grid in")
    }
    ## kriging
    if(length(grid$proj)>0){
      pred.grid=project(gr,proj=grid$proj,degrees=grid$degrees)
      pred.grid=cbind(pred.grid$x,pred.grid$y)
    } else {
      pred.grid=gr
    }
    scaled.pred.grid=pred.grid/coord.scaling
    kc=krige.conv(geodata=aa,locations=scaled.pred.grid,
      krige=krige.control(obj.model=wls.n,type.krige=krige.param$type.krige,
        trend.d=krige.param$trend.d))
    kc$predict=kc$predict*stat.scaling
    kc$krige.var=kc$krige.var*stat.scaling^2
    kc$predict[!in.region]=NA
    kc$krige.var[!in.region]=NA
    if(plots){
      boxplot(cbind(kc$predict,sqrt(kc$krige.var)),axes=FALSE,ylab="statistic")
      box()
      axis(1,1:2,labels=c("prediction","sd"))
      axis(2)
      image(kc,loc=gr,col=gray(seq(1,0.1,l=100)),xlab="latitude",ylab="longitude",
            main="kriging prediction")
      contour(kc,loc=gr,add=TRUE)
      #map('worldHires', grid$border,xlim=range(grid$x),ylim=range(grid$y),col=1,add=TRUE)	
      image(kc,val=sqrt(kc$krige.var),loc=gr,col=gray(seq(1,0.1,l=100)),
            xlab="latitude",ylab="longitude",main="kriging standard error")
      contour(kc,val=sqrt(kc$krige.var),loc=gr,add=TRUE)
      #map('worldHires', grid$border,xlim=range(grid$x),ylim=range(grid$y),col=1,add=TRUE)	
    }
    list(input=input,in.region=in.region,variofit.wls=wls.n,grid=gr,
         krige=kc)
}



#lecture des fichiers d'entrés
##Le polygone de bordure
boundaryTable=read.table("usBoundaryCoarse.txt",header=T)
##Les points de mesures
coordStatsT=read.table("stats.txt",header=T)
##La grille a kriger
coordsGrid=read.table("grid.txt",header=T)

boundary = as.matrix(boundaryTable)
coordStats=as.matrix(coordStatsT)

nodes=coordStats[,1:2]
stats=coordStats[,3]

xmin=min(c(boundary[,1],nodes[,1]))
ymin=min(c(boundary[,2],nodes[,2]))
xmax=max(c(boundary[,1],nodes[,1]))
ymax=max(c(boundary[,2],nodes[,2]))
par(mfrow=c(3,4), mar=c(5.1,4.1,4.1,4.1))

plot(nodes[,1],nodes[,2],xlim=c(xmin,xmax),ylim=c(ymin,ymax),main="input stats nodes")
lines(boundary[,1],boundary[,2],type='l')

in.region=0
in.region=point.in.polygon(nodes[,1],nodes[,2],boundary[,1],boundary[,2])
nodesIn=nodes[in.region>0,1:2]
statsIn=stats[in.region>0]
plot(nodesIn[,1],nodesIn[,2],xlim=c(xmin,xmax),ylim=c(ymin,ymax),main="input stats nodes in")
lines(boundary[,1],boundary[,2],type='l')

xmin=min(c(boundary[,1],nodesIn[,1]))
ymin=min(c(boundary[,2],nodesIn[,2]))
xmax=max(c(boundary[,1],nodesIn[,1]))
ymax=max(c(boundary[,2],nodesIn[,2]))

plot(coordsGrid[,1],coordsGrid[,2],xlim=c(xmin,xmax),ylim=c(ymin,ymax),main="input grid ")
#kr1=krige(nodesIn,
#      statsIn,
#      grid=list(x=seq(xmin,xmax,1),y=seq(ymin,ymax,1),borderPoly=USboundaryC,degrees=TRUE),
#      variog.param=list(keep.distance=2/3,nb.bin=15,
#        cov.pars=c(0.0008,10^6),
#        nugget=0.0001,
#        fix.nugget=FALSE,
#        coordinates.scaling=10^5,
#        statistic.scaling=0.01),
#      krige.param=list(type.krige="ok",trend.d="1st"),
#      plots=TRUE)
kr1=krige(nodesIn,
      statsIn,
      grid=list(coords=coordsGrid,borderPoly=boundary,degrees=TRUE),
      variog.param=list(keep.distance=2/3,nb.bin=15,
        cov.pars=c(0.0008,10^6),
        nugget=0.0001,
        fix.nugget=FALSE,
        coordinates.scaling=1,
        statistic.scaling=0.01),
      krige.param=list(type.krige="ok",trend.d="1st"),
      plots=TRUE)
