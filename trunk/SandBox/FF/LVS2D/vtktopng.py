import vtk
import os
from array import array
#baseName="/home/olivierb/VTK_PYTHON/diff/u"
#species=array('c','uv')

#numStep=1998
#for num in range(1,150):
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(500,500)
nbStep=50
step=20
nbOmega=2
WinSize=500
firstStep=1
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(WinSize,WinSize)
rgb=[0,0,0]
execfile("vtktopngGen.py")
os.chdir(OUTPUTDIR+"/D2");
finput = open("input.txt", 'w')

numSpec=0
for spec in species:
    baseName=OUTPUTDIR+"/D2/"+spec
    for numStep in range(firstStep,nbStep):
        fenetre=vtk.vtkRenderWindow()
        fenetre.SetSize(WinSize,WinSize)
        ren=vtk.vtkRenderer()
        fenetre.AddRenderer(ren)
        execfile("../buildActorWires.py")
        execfile("../addActorWires.py")
    #execfile("buildActorWires2.py")
    #execfile("addActorWires2.py")
    #execfile("buildActorWires1.py")
    #execfile("addActorWires1.py")
        ren.SetBackground(0,0,0)
        for numOmega in range(0,nbOmega):
            fileName=baseName+str(numOmega)+"s"+str(numStep*step)+".vtk"
            r=vtk.vtkGenericDataObjectReader()
            r.SetFileName(fileName)
            r.Update()
            o=r.GetOutput()
            o.GetCellData().SetActiveScalars("UU")
            aPolyVertexMapper = vtk.vtkDataSetMapper()
            aPolyVertexMapper.SetInput(o)
            aPolyVertexMapper.SetScalarRange(minV[numSpec],maxV[numSpec])
            aPolyVertexMapper.UseLookupTableScalarRange=1
            lut=aPolyVertexMapper.GetLookupTable()
            lut.SetHueRange(251.0/360.0,0)
            lut.SetSaturationRange(1,1)
            lut.SetValueRange(1,1)
            lut.Build()
            aPolyVertexActor = vtk.vtkActor()
            aPolyVertexActor.SetMapper(aPolyVertexMapper)
            ren.AddActor(aPolyVertexActor)
            atext = vtk.vtkVectorText()
            atext.SetText(str(spec))
            textMapper = vtk.vtkPolyDataMapper()
            textMapper.SetInputConnection(atext.GetOutputPort())
            textActor = vtk.vtkFollower()
            textActor.SetMapper(textMapper)
            textActor.SetScale(scaleLegend,scaleLegend ,scaleLegend )
            textActor.AddPosition(-116, 25, 0)
            ren.AddActor(textActor)
    
    #iren=vtk.vtkRenderWindowInteractor()
    #iren.SetRenderWindow(fenetre)
    #iren.Initialize()
        fenetre.Render()
        image = vtk.vtkWindowToImageFilter()
        image.SetInput(fenetre)
        image.Update()
        fenetre.Start()
        fenetre.Render()
        fenetre.Finalize()
        image.UpdateInformation()
        image.Update()
    #iren.Render()
    #iren.Render()
    #iren.Render()
        jpg = vtk.vtkJPEGWriter()
        fileStepName="step"+spec+str(numStep)+".jpg"
        jpg.SetFileName(fileStepName)
        jpg.SetInputConnection(image.GetOutputPort())
        jpg.Write()
    numSpec+=1

for numStep in range(firstStep,nbStep):
    jpgfiles=""
    for spec in species:
        fileStepName="step"+spec+str(numStep)+".jpg"
        jpgfiles=jpgfiles+" "+fileStepName
    os.system("convert "+jpgfiles+" "+"+append step"+str(numStep)+".jpg")
    finput.write("step"+str(numStep)+".jpg"+"\n")
    #iren.Start()

finput.close()
#os.system("mencoder \"mf://@input.txt\" -mf fps=20 -o test.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800")
print "mencoder \"mf://@input.txt\" -mf fps=20 -o test.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800"
#os.system("vlc test.avi")
   


