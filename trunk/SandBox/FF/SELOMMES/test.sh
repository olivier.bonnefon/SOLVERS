#!/bin/bash
#$ -S /bin/bash
#$ -N EmiSt
#$ -V
#$ -cwd

echo "lot $KK par $JOB_ID "

OUTPUTDIR=/mnt/bigcalcul2/biom/olivierB/solvers/trunk/SandBox/FF/SELOMMES_$KK"/"
mkdir $OUTPUTDIR
cp -R /mnt/bigcalcul2/biom/olivierB/solvers/trunk/SandBox/FF/SELOMMES_REF/* $OUTPUTDIR
cd $OUTPUTDIR

for ((i=2;i<3;i++));do
        
    #TODO: modifier la geometrie en tenant compte de KK et de i
    #     -> modifier $OUTPUTDIR/SELOMMES/edges.txt
    #     -> modifier $OUTPUTDIR/SELOMMES/cultures.txt
    /opt/SELOMMES/updateSelommes $OUTPUTDIR

    
    #TODO :-> modifier $OUTPUTDIR/events.txt en tenant compte de KK et de i
    #      -> modifier $OUTPUTDIR/summaryParams.edp
    /opt/freefem++-3.58/src/nw/FreeFem++-nw cas1.edp

    #TODO :-> sauvegarder mass.txt et control*

done
